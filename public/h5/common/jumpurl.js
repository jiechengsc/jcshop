//接口请求方法
import http from '@/common/http.js';
import store from '@/store'
import basejson from '@/static/base.json';
import {
	getjuris
} from '@/common/jurisdiction.js'
import {
	getnavbar
} from "@/common/juris_navbar.js"
import {
	publicShare
} from '@/common/baseutil'
import util from 'common/util'
export const is_canurl = async (link) => {
	return store.state.pagelist.findIndex(_ => {
		return _.page == link
	}) > -1
}
export const Jumpurling = async (link, type) => {
	if (basejson.addons_pages.findIndex(_ => {
			return link == _.page
		}) > -1) {
		let a = await is_canurl(link)
		if (!a) {
			uni.showModal({
				title: '无权访问！',
			})
			return false
		}
	}
	
	//在线客服
	if (link.indexOf('customer/customer') > -1) {
		// #ifdef H5
		// let customerService = store.state.customerService
		getjuris('customerService').then(a => {
			if (a && a.is_open) {
				window.location.href = 'https://yzf.qq.com/xv/web/static/chat/index.html?sign=' + a
					.wechat
			} else {
				uni.showModal({
					title: '提示',
					content: '未开启在线客服',
					showCancel: false,
					success(e) {}
				})
			}
		})
		return false
		// #endif
		// #ifdef MP-WEIXIN
		
		var link = link.replace('..', '/applicationpage')
		// #endif
	}
	
	
	
	// var link = link.replace('..', '/pages')
	if (link.indexOf('../order') > -1 || link.indexOf('../groupdetial') > -1) {
		var link = link.replace('..', '/orderpage')
	} else if (link.indexOf('../agent') > -1) {
		var link = link.replace('..', '/agentpage')
	} else if (link.indexOf('../marketing') > -1 || link.indexOf('../prize') > -1) {
		var link = link.replace('..', '/marketpage')
	} else if (link.indexOf('../user') > -1 || link.indexOf('../rewardList') > -1 || link.indexOf(
		'../welfare') > -1 || link.indexOf('../buymember') > -1 || link.indexOf('../pay') > -1 || link.indexOf(
			'../afterpay') > -1 || link.indexOf('../record') > -1 || link.indexOf('../recordDetails') > -1) {
		var link = link.replace('..', '/userpage')
	} else if (link.indexOf('../goods') > -1) {
		var link = link.replace('..', '/pointsmallpage')
	} else {
		var link = link.replace('..', '/pages')
	}
	//计算pv
	// let sys=this.$u.sys()
	var deviceId = uni.getStorageSync('__DC_STAT_UUID')
	let data = {
		path: link,
		deviceId: deviceId
	};
	// #ifdef H5
	let mall_id = uni.getStorageSync('mall_id');
	if (link.indexOf('?') == -1) {
		link = link + '?mall_id=' + mall_id
	} else {
		link = link + '&mall_id=' + mall_id
	}
	// #endif
	http('/total/uvpv', data, 'POST').then(res => {
		//成功执行跳转
	});
	if (type == 'switch') {
		uni.reLaunch({
			url: link,
			success() {
				//设置分享
				// #ifdef H5
				publicShare()
				// #endif	
				// #ifdef MP-WEIXIN
				getnavbar(link)
				// #endif
			},
			fail(e) {
				console.log(e)
			},
			complete(e) {}
		})
	} else if (type == 'redirect') {
		uni.redirectTo({
			url: link,
			success() {
				//设置分享
				// #ifdef H5
				if (type != 'noshare') {
					publicShare()
				}
				// #endif
			},
			fail(e) {
				console.log(e)
			},
			complete(e) {}
		})
	} else {
		uni.navigateTo({
			url: link,
			success() {
				//设置分享
				// #ifdef H5
				if (type != 'noshare') {
					publicShare()
				}
				// #endif
				// #ifdef MP-WEIXIN
				getnavbar(link)
				// #endif
			},
			fail(e) {
				console.log(e)
			},
			complete(e) {}
		})
	}
}
//页面跳转
export const Jumpurl = (item, type) => {
	// #ifndef MP-WEIXIN
	let text = uni.getStorageSync('withholdPassword');
	if(text){
		util.uniCopy({
			content: text,
			success: res => {
				uni.removeStorageSync('withholdPassword');
			},
			error: e => {}
		});
	}
	
	// #endif
	if (!item) {
		return
	}
	if (item.type == 1) {
		let token = uni.getStorageSync('token')
		if (item.link.indexOf('index/index') > -1) {
			type = 'switch'
		}
		if (token) {
			Jumpurling(item.link, type)
		} else {
			if (item.login_type == 2) {
				store.commit('setlogin', false);
			} else {
				Jumpurling(item.link, type)
			}
		}
	} else if (item.type == 2) {
		if (item.id) {
			Jumpurling('../detail/detail?id=' + item.id, 'noshare')
		}
	} else if (item.type == 4) {
		if (item.id) {
			Jumpurling('../conpon/detail?id=' + item.id, 'share')
		}
	} else if (item.type == 5) {
		if (item.id) {
			Jumpurling('../custompage/custompage?id=' + item.id, 'share')
		}
	} else if (item.type == 6) {
		if (item.id == 1) {
			// #ifdef MP-WEIXIN
			let url = encodeURI(item.link.link)
			Jumpurling('../webview/webview?url=' + url)
			// #endif
			// #ifndef MP-WEIXIN
			window.location.href = item.link.link;
			// #endif
		} else if (item.id == 2) {
			uni.makePhoneCall({
				phoneNumber: item.link.mobile
			})
		} else if (item.id == 3) {
			uni.navigateToMiniProgram({
				appId: item.link.appid,
				path: item.link.pages,
				success(res) {
					// 打开成功
				}
			})
		}
		// if (item.id) {
		// 	Jumpurling('../list/list?id=' + item.id, 'share')
		// }
	} else if (item.type == 7) {
		if (item.id) {
			Jumpurling('../list/list?id=' + item.id, 'share')
		}
	} else if (item.type == 8) {
		http('/wx/cs/' + item.id, {}, 'get').then(res => {
			let wx_customer = res.data.msg
			// #ifndef MP-WEIXIN
			window.location.href = wx_customer.url;
			// #endif
			// #ifdef MP-WEIXIN
			wx.openCustomerServiceChat({
				extInfo: {
					url: wx_customer.url
				},
				corpId: wx_customer.corp_id,
				success(res) {},
				fail(e) {
					console.log(e);
				}
			});
			// #endif
		})
	}
}
module.exports = {
	Jumpurl: Jumpurl
}
