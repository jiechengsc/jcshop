import store from '@/store'
import http from '@/common/http.js'
export const getnavbar=async (page) =>{
	let BottomMenu = store.state.BottomMenu;
	if (BottomMenu && BottomMenu.tablist) {
		let a_id=BottomMenu.tablist.findIndex(a=>{
			let b = a.linkdata.link.replace('../', '');
			return page.indexOf(b) > -1
		})
		if(a_id>-1){
			
			store.commit('sethastabbar', true);
	
		}else{
			store.commit('sethastabbar', false);
		}
	
		return a_id
	}else{
		return false
	}
}

module.exports = {
	getnavbar: getnavbar,
}