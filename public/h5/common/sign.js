import md5 from "./md5";

/**
 * 签名算法
 * @param appKey
 * @param appSecret
 * @param token
 * @param _t
 * @param params
 * @returns {*}
 */
export default function sign(appKey, appSecret, token, _t, params) {
    try {
        if (appKey && appSecret && _t) {
            // 签名头
            var str1 = "app_key=" + appKey + "|app_secret=" + appSecret + "|token=" + token + "|_t=" + _t;

            // 签名体
            var str2 = "";
            var keys = Object.keys(params).sort();
            keys.forEach((value) => {
                str2 += value + "|";
            });
            str2 = str2.substring(0, str2.length - 1);   // 去掉末尾的“|”

            // MD5计算
            var sign = md5(str1 + "&" + str2);
            return sign;
        }
    } catch (e) {
        this.logger.error("生成APP接口签名失败：" + e);
    }
    return "";
}