module.exports = {
  getM : (date) => {
    let ary = date.split('-'),
      m = ary[1].replace(/$0/gi,'')
    return Number(m)
  },
  getY: (date) => {
    let ary = date.split('-')
    return ary[0]
  },
  sendM : (m) => {
    if(m < 10) return '0' + m
    else return m
  }
}