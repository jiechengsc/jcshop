import http from '@/common/http.js';
import store from "@/store"
export const base = {
	integralBalance: store.state.integralBalance,
	mallBaseinfo: store.state.mallBaseinfo,
	mallPayMode: store.state.mallPayMode,
	tradeSetting: store.state.tradeSetting,
	customerService: store.state.customerService,
	agentExplain: store.state.agentExplain,
	addons: store.state.addons,
	mallBase: store.state.mallBase,
	oss: store.state.oss,
}
export const getjuris = async (type) => {
	if (store.state[type]&&type=='mallBase') {
		return store.state[type]
	} else {
		
		let url="/configuration/"
		if(type=='ossConfig'){
			url="/Configuration/"
		}
		if(type=='addons'){
			url='/'
		}
		let Baseinfo = await http( url+ type, {}, 'GET').then(async res => {
			store.commit('set' + type, res.data.msg);
			return res.data.msg
		})
		return Baseinfo
	}
}
export const getjuris_arry = async (type) => {
	let a = {}
	if (Array.isArray(type)) {
		for (var i = 0; i < type.length; i++) {
			let c = type[i]
			a[c] = await getjuris(c)
		}
	} else {
		if (type == 'addons') {
			a = await getjuris_addons()
		} else {
			a = await getjuris(type)
		}
	}
	return a
}
export const getjuris_addons = async (arr) => {
	let a = {}
	let addons = await getjuris('addons');
	if (Array.isArray(arr)) {
		for (var i = 0; i < arr.length; i++) {
			let c = arr[i]
			a[c] = addons.findIndex(_ => _ == c) > -1
		}
	} else {
		a = addons.findIndex(_ => _ == arr) > -1
	}
	return a
}
module.exports = {
	getjuris: getjuris_arry,
	getjuris_addons: getjuris_addons
}
