'use strict'
const path = require('path')
const defaultSettings = require('./src/settings.js')
let glob = require('glob') // 用于筛选文件

// 工厂函数 - 配置pages实现多页面获取某文件夹下的html与js
function handleEntry(entry) {
  let entries = {}
  let entryBaseName = ''
  let entryPathName = ''
  let entryTemplate = ''
  let applicationName = ''

  glob.sync(entry).forEach(item => {
    console.log('!!!', item)
    entryBaseName = path.basename(item, path.extname(item))
    console.log('entryBaseName:', entryBaseName)
    entryTemplate = item.split('/').splice(-3)
    console.log('entryTemplate:', entryTemplate)
    entryPathName = entryBaseName // 正确输出js和html的路径
    console.log('entryPathName', entryPathName)

    entries[entryPathName] = {
      entry: 'src/' + entryTemplate[0] + '/' + entryTemplate[1] + '/' + entryTemplate[1] + '.js',
      template: 'src/' + entryTemplate[0] + '/' + entryTemplate[1] + '/' + entryTemplate[2],
      title: entryTemplate[2],
      filename: entryTemplate[2]
    }
  })

  return entries
}

let pages = handleEntry('./src/applications/**?/*.html')
// let pages = handleEntry('./src/applications/**?/*.html')
function resolve(dir) {
  return path.join(__dirname, dir)
}
const name = defaultSettings.title || '管理平台' // page title
// If your port is set to 80,
// use administrator privileges to execute the command line.
// For example, Mac: sudo npm run
// You can change the port by the following methods:
// port = 9528 npm run dev OR npm run dev --port = 9528
const port = process.env.port || process.env.npm_config_port || 9528 // dev port
// All configuration item explanations can be find in https://cli.vuejs.org/config/
module.exports = {
  /**
   * You will need to set publicPath if you plan to deploy your site under a sub path,
   * for example GitHub Pages. If you plan to deploy your site to https://foo.github.io/bar/,
   * then publicPath should be set to "/bar/".
   * In most cases please use '/' !!!
   * Detail: https://cli.vuejs.org/config/#publicpath
   */
  publicPath: '/',
  outputDir: 'dist',
  assetsDir: 'static',
  pages,
  //lintOnSave: process.env.NODE_ENV === 'development',
  lintOnSave: false,
  productionSourceMap: false,
  transpileDependencies: ['element-ui', 'vue-elementui-skeleton', '@amap', '@antv'],
  devServer: {
    port: port,
    open: true,
    overlay: {
      warnings: false,
      errors: false
    },
    proxy: {
      // change dev-api/login => /login
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      //index.html#/saas_login https://x.jc362.com
      [process.env.VUE_APP_BASE_API]: {
        // target: "https://x.jc362.com",
        // target: "https://www.jingzhe365.com",
        target: "https://cs.jc362.com",
        changeOrigin: true,
        pathRewrite: {
          ['^' + process.env.VUE_APP_BASE_API]: ''
        }
      }
    },
    // before: require('./mock/mock-server.js')
  },
  configureWebpack: {
    // provide the app's title in webpack's name field, so that
    // it can be accessed in index.html to inject the correct title.
    name: name,
    resolve: {
      alias: {
        '@': resolve('src'),
        "d3-regression": "d3-regression/dist/d3-regression.js" // 或者 "d3-regression/dist/d3-regression.min.js"
      }
    }
  },
  // chainWebpack(config) {
  //   // it can improve the speed of the first screen, it is recommended to turn on preload
  //   config.plugin('preload').tap(() => [{
  //     rel: 'preload',
  //     // to ignore runtime.js
  //     // https://github.com/vuejs/vue-cli/blob/dev/packages/@vue/cli-service/lib/config/app.js#L171
  //     fileBlacklist: [/\.map$/, /hot-update\.js$/, /runtime\..*\.js$/],
  //     include: 'initial'
  //   }])
  //   // when there are many pages, it will cause too many meaningless requests
  //   config.plugins.delete('prefetch')
  //   // set svg-sprite-loader
  //   config.module.rule('svg').exclude.add(resolve('src/icons')).end()
  //   config.module.rule('icons').test(/\.svg$/).include.add(resolve('src/icons')).end().use('svg-sprite-loader')
  //     .loader('svg-sprite-loader').options({
  //       symbolId: 'icon-[name]'
  //     }).end()
  //   // set preserveWhitespace
  //   config.module.rule('vue').use('vue-loader').loader('vue-loader').tap(options => {
  //     options.compilerOptions.preserveWhitespace = true
  //     return options
  //   }).end()
  //   config.when(process.env.NODE_ENV !== 'development', config => {
  //     config.plugin('ScriptExtHtmlWebpackPlugin').after('html').use('script-ext-html-webpack-plugin', [{
  //       // `runtime` must same as runtimeChunk name. default is `runtime`
  //       inline: /runtime\..*\.js$/
  //     }]).end()
  //     config.optimization.splitChunks({
  //       chunks: 'all',
  //       cacheGroups: {
  //         libs: {
  //           name: 'chunk-libs',
  //           test: /[\\/]node_modules[\\/]/,
  //           priority: 10,
  //           chunks: 'initial' // only package third parties that are initially dependent
  //         },
  //         elementUI: {
  //           name: 'chunk-elementUI', // split elementUI into a single package
  //           priority: 20, // the weight needs to be larger than libs and app or it will be packaged into libs or app
  //           test: /[\\/]node_modules[\\/]_?element-ui(.*)/ // in order to adapt to cnpm
  //         },
  //         commons: {
  //           name: 'chunk-commons',
  //           test: resolve('src/components'), // can customize your rules
  //           minChunks: 3, //  minimum common number
  //           priority: 5,
  //           reuseExistingChunk: true
  //         }
  //       }
  //     })
  //     // https:// webpack.js.org/configuration/optimization/#optimizationruntimechunk
  //     config.optimization.runtimeChunk('single')
  //   })
  // },

  /*  pages: {
      index: {
        // page 的入口
        entry: 'src/main.js',
        // 模板来源
        template: 'public/index.html',
        // 在 dist/index.html 的输出
        filename: 'index.html',
        // 当使用 title 选项时，
        // template 中的 title 标签需要是 <title><%= htmlWebpackPlugin.options.title %></title>
        title: 'Index Page',
        // 在这个页面中包含的块，默认情况下会包含
        // 提取出来的通用 chunk 和 vendor chunk。
        chunks: ['chunk-vendors', 'chunk-common', 'index']
      },
      // 当使用只有入口的字符串格式时，
      // 模板会被推导为 `public/subpage.html`
      // 并且如果找不到的话，就回退到 `public/index.html`。
      // 输出文件名会被推导为 `subpage.html`。
      // subpage: 'src/subpage/main.js'
    } */

}
