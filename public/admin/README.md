
```
# 安装依赖
$ npm install

# 启动项目(本地开发环境)
$ npm run dev

# 打包项目
$ npm run build
```

# <span id="project-structure">项目结构</span>
```
|-- single
    |-- public
        |-- iconfont //图标
        |-- tinymce //文本编辑器
    |-- src 
    |   |-- api //接口文件夹
    |   |-- applications //路由文件夹 
    |   |-- components
    
    |-- view
    |   |-- login
    |   |   |-- mall_login.vue
    |   |   |-- shop_login.vue
    |   |-- mall
    |   |   |-- activity //新人送礼
    |   |   |-- addons //插件列表
    |   |   |-- admin //管理员
    |   |   |-- agent //分销
    |   |   |-- application //应用
    |   |   |-- article //文章
    |   |   |-- assettransfer //转赠
    |   |   |-- commission //达标/考核
    |   |   |   |-- agent //分销达标
    |   |   |   |-- assessment //分销考核
    |   |   |-- copyright //版权
    |   |   |-- customer //腾讯智服
    |   |   |-- customForm //自定义表单
    |   |   |-- deduction 
    |   |   |   |-- index.vue //抵扣设置
    |   |   |   |-- return.vue //积分使用限制
    |   |   |-- finance //财务 
    |   |   |-- groupon //拼团  
    |   |   |-- guesslike //猜你喜欢
    |   |   |-- integralgoods //积分商品
    |   |   |-- liver //直播
    |   |   |-- loglist //操作记录
    |   |   |-- mall_index //首页
    |   |   |-- merchant //商户
    |   |   |-- multiple //多端
    |   |   |-- notice //消息通知
    |   |   |-- page //页面管理
    |   |   |-- pay //支付设置
    |   |   |-- presell //商品预售
    |   |   |-- product //商品管理
    |   |   |-- protocol //协议管理
    |   |   |-- quick //快速注册小程序
    |   |   |-- recharge //充值奖励
    |   |   |-- role //角色
    |   |   |-- scrapy //采集助手
    |   |   |-- seckill //商品秒杀
    |   |   |-- setting //设置
    |   |   |-- signin //签到
    |   |   |-- sms //短信设置
    |   |   |-- systemform //系统表单
    |   |   |-- template //模板中心
    |   |   |-- user //用户管理
    |   |   |-- vipcenter //会员中心
    |   |   |-- wxurllink //小程序外部跳转
    |   |-- shop
    |   |   |-- admin 管理员
    |   |   |-- announcement //公告
    |   |   |-- app //应用
    |   |   |-- coupon //优惠券
    |   |   |-- distribution //同城
    |   |   |-- dndlist //DIY
    |   |   |-- finance //财务
    |   |   |-- freight //物流设置
    |   |   |-- information //基本信息
    |   |   |-- lottery //抽奖
    |   |   |-- order //订单
    |   |   |-- print //小票助手
    |   |   |-- product//商品
    |   |   |-- return //退货地址
    |   |   |-- role //角色
    |   |   |-- shop_index //首页
    |   |   |-- store //门店
   
    |-- util	//js
        |-- auth.js	//
        |-- btnPermissions.js //按钮级权限
        |-- entry.js //渠道权限
        |-- index.js //工具js
        |-- jurisdiction.js 插件权限
        |-- loading.js
        |-- lodop.js //打印插件
        |-- lodop_print.js
        |-- request.js 请求封装
        |-- validate.js 验证
```