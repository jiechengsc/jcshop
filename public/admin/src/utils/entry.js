import Vue from 'vue'
import mallstore from '@/applications/mall/store'
import shopstore from '@/applications/shop/store'
import {
  logintype_byurl
} from '@/utils/auth'
const whiteList = ['deliver', "refund", "delete", "audit"]
/**权限指令**/
const entry = Vue.directive('entry', {
  inserted: function(el, binding, vnode) {
    // 获取页面按钮权限
    let type = logintype_byurl()
    let store;
    if (type == 'mall') {
      store = mallstore
    } else if (type == 'shop') {
      store = shopstore
    }
    var entry = []
    if(store.state.common.entry){
      entry = store.state.common.entry
      let btnPermissionsArr = '';
      if (binding.value && entry) {
        if (entry.indexOf(binding.value) == -1) {
          el.parentNode.removeChild(el);
        }
      } else {
        el.parentNode.removeChild(el);
      }
    }else{
      store.dispatch('common/getlesseeentry').then(a => {
        entry = a
        let btnPermissionsArr = '';
        if (binding.value && entry) {
          if (entry.indexOf(binding.value) == -1) {
            el.parentNode.removeChild(el);
          }
        } else {
          el.parentNode.removeChild(el);
        }
      });
    }

  }
});
export {
  entry
}
