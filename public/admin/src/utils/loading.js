import {
  Loading
} from 'element-ui';
let loadingCount = 0;
let loading;
const startLoading = (text) => {
  loading = Loading.service({
    target: '.app-main',
    lock: true,
    text: text ? text : '请求中……',
    background: "rgba(255, 255, 255, 1)"
  });
};
const endLoading = () => {
  loading.close();
};
export const showLoading = (text) => {
  if (loadingCount === 0) {
    startLoading(text);
  }
  loadingCount += 1;
};
export const hideLoading = () => {
  if (loadingCount <= 0) {
    return;
  }
  loadingCount -= 1;
  if (loadingCount === 0) {
    endLoading();
  }
};
