import Cookies from 'js-cookie'
const TokenKey = 'vue_admin_template_token'
import mallrouter from '@/applications/mall/router'
import shoprouter from '@/applications/shop/router'
import mallstore from '@/applications/mall/store'
import shopstore from '@/applications/shop/store'
export function getToken() {
  let TokenKey = logintype_byurl() + 'Token'
  return Cookies.get(TokenKey)
}
export async function setToken(token) {
  let TokenKey = logintype_byurl() + 'Token'
  return Cookies.set(TokenKey, token)
}
export function removeToken() {
  let TokenKey = logintype_byurl() + 'Token'
  return Cookies.remove(TokenKey)
}
export function getMallToken() {
  return Cookies.get('mallToken')
}
export function setMallToken(token) {
  return Cookies.set('mallToken', token)
}
export function setSaasToken(token) {
  return Cookies.set('saasToken', token)
}
export function setUserToken(token) {
  return Cookies.set('userToken', token)
}
export function setsToken(token) {
  return Cookies.set('shopToken', token)
}
export function removeMallToken() {
  return Cookies.remove('mallToken')
}
//设置/读取shop_token
//场景:商城端(mall)调用商户端(shop)功能时需要用
export function setShopToken(token) {
  return Cookies.set('mshopToken', token);
};
export function getShopToken(token) {
  return Cookies.get('mshopToken', token);
};
export function removeShopToken() {
  return Cookies.remove('mshopToken');
};
//判断登录类型
export function getLoginType(type) {
  let token = getToken()
  if (token) {
    let a = token.substr(0, 4)
    let login_type = logintype_byurl()
    if (type == 2) {
      return login_type
    } else if (type == 1) {
      return a
    }
    if (a != login_type) {
      login_type = a + '_' + login_type
    }
    return login_type
  } else {
    return ''
  }
  // if (login_type.indexOf('_') == -1) {
  //   return login_type
  // } else {
  //   let a = login_type.split("_");
  //   if (type == 1) {
  //     return a[0]
  //   } else if (type == 2) {
  //     return a[a.length - 1]
  //   }
  // }
}
export function logintype_byurl(type) {
  let url = window.location.pathname;
  if (url.indexOf('saas.html') > -1) {
    return 'saas'
  } else if (url.indexOf('mall.html') > -1) {
    return 'mall'
  } else if (url.indexOf('shop.html') > -1) {
    return 'shop'
  } else if (url.indexOf('user.html') > -1) {
    return 'user'
  }
}
export function getroute(link) {
  return new Promise((resolve, reject) => {

  })
}
