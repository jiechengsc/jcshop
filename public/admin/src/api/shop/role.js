import request from '@/utils/request'
export function getrulelist(token) {
  return request({
    url: '/shop/auth/rule/value',
    method: 'get',
  })
}
export function getrolelist(page, data) {
  return request({
    url: '/shop/auth/role/list?page=' + page + '&size=10',
    method: 'post',
    data
  })
}
export function saverole(data) {
  return request({
    url: '/shop/auth/role',
    method: 'post',
    data
  })
}
export function getroledetail(data) {
  return request({
    url: '/shop/auth/role/' + data.id,
    method: 'get',
  })
}
export function editrole(data) {
  return request({
    url: '/shop/auth/role/' + data.id,
    method: 'put',
    data
  })
}
export function delrole(data) {
  return request({
    url: '/shop/auth/role/' + data.id,
    method: 'delete',
    data
  })
}
