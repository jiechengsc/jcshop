import request from '@/utils/request';
var qs = require('qs');
//**********以下接口为装修中心调用**********
//获取商品列表
export function getProList(data, page, size) {
  return request({
    url: '/shop/spu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
//获取商品分类列表
export function getCateList() {
  return request({
    url: '/shop/classify',
    method: 'get'
  });
};
//图片列表
export function getpicList(data, page, size) {
  data = qs.stringify(data);
  return request({
    url: '/shop/pic/picList?' + data,
    method: 'get',
  });
};
export function groupList() {
  return request({
    url: '/shop/pic/groupList',
    method: 'get'
  });
};
//添加分组
export function addGroup(data) {
  return request({
    url: '/shop/pic/addGroup',
    method: 'post',
    data
  });
};
//编辑分组
export function editGroup(data) {
  return request({
    url: '/shop/pic/editGroup',
    method: 'put',
    data
  });
};
export function delPic(data) {
  return request({
    url: '/shop/pic/delPic/' + data.id,
    method: 'delete',
    data
  })
}
export function delGroup(data) {
  return request({
    url: '/shop/pic/' + data.id,
    method: 'delete',
    data
  })
}

