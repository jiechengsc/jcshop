import request from '@/utils/request';
//**********以下接口为站点配置模块**********
//增加站点配置(单条)
export function addSiteConfig(data){
  return request({
    url: '/shop/configuration',
    method: 'post',
    data
  });
};

//修改站点配置(单条)
export function editSiteConfig(data){
  return request({
    url: '/shop/configuration/' + data.type,
    method: 'put',
    data
  });
};

//获取站点配置(单条)
export function getSiteConfig(type){
  return request({
    url: '/shop/configuration/' + type,
    method: 'get'
  });
};
