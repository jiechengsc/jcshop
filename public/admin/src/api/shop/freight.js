import request from '@/utils/request'

//运费模板列表
export function getfreightlist(page,data) {
  return request({
    url: '/shop/freight/temp/list?page=' + page + '&size=10' ,
    method: 'post',
    data
  })
}
//add
export function savefreight(data) {
  return request({
    url: '/shop/freight/temp',
    method: 'post',
    data
  })
}

//detail
export function getfreightdetail(data) {
  return request({
    url: '/shop/freight/temp/' + data.id,
    method: 'get',
  })
}
//update
export function editfreight(data) {
  return request({
    url: '/shop/freight/temp/' + data.id,
    method: 'put',
    data
  })
}
// delete
export function delfreight(data) {
  return request({
    url: '/shop/freight/temp/' + data.id,
    method: 'delete',
    data
  })
}


//快递

export function getexpress(){
  return request({
    url: '/shop/freight/express',
    method: 'get'
  });
};
