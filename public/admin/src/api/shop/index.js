import request from '@/utils/request';
//**********以下接口为商城端首页数据统计**********
//获取统计数据 - 头部
export function getDataTop() {
  return request({
    url: '/shop/statistic/index',
    method: 'post'
  });
};


//获取统计数据 - 总数
export function getDataTotal(data) {
  return request({
    url: '/shop/statistic/total',
    method: 'post',
    data
  });
};


//获取统计数据 - 图表
export function getDataCount(data) {
  return request({
    url: '/shop/statistic/count',
    method: 'post',
    data
  });
};

//获取统计数据 - 分销
export function getDataAgent() {
  return request({
    url: '/shop/statistic/agent',
    method: 'post'
  });
};

//获取统计数据 - 用户消费排行
export function getDataUserRank() {
  return request({
    url: '/shop/statistic/consumption_ranking',
    method: 'post'
  });
};

//获取统计数据 - 商品销量排行
export function getDataProductRank() {
  return request({
    url: '/shop/statistic/sales_ranking',
    method: 'post'
  });
};

//获取统计数据 - 今日数据
export function getDataunifiedcount() {
  return request({
    url: '/shop/statistic/unified/count',
    method: 'post'
  });
};
