import request from '@/utils/request';

// 获取发票列表
export function getInvoiceList(data) {
  return request({
    url: `/shop/invoice/manage/invoiceList?page=${data.page}&size=${data.size}&inoice_type=${data.inoice_type}&status=${data.status}&start_time=${data.start_time}&end_time=${data.end_time}`,
    method: 'get',
    data
  });
};
// export function getInvoiceList(data) {
//   return request({
//     url: '/shop/invoice/manage/invoiceList?page=' + data.page + '&size=' + data.size ,
//     method: 'GET',
//     data
//   });
// };
// 修改发票信息（审核）
export function editInvoiceLister(data) {
  return request({
    url: '/shop/invoice/manage/examine/',
    method: 'put',
    data
  });
};
// 获取发票详细信息
export function getInvoiceRead(data) {
  return request({
    url: `/shop/invoice/manage/read?id=` + data.id,
    method: 'get',
  });
};