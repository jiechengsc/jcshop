import request from '@/utils/request';
//**********以下接口为订单部分调用**********
//删除订单
export function delOrder(id) {
  return request({
    url: '/shop/order/' + id,
    method: 'delete'
  });
};
//编辑订单
// export function editOrder(data){
//   return request({
//     url: '/shop/order/save',
//     method: 'post',
//     data
//   });
// };
//编辑订单(发货)
export function expressOrder(data) {
  return request({
    url: '/shop/order/saveOrderStatus',
    method: 'put',
    data
  });
};
//编辑订单(修改收货地址)
export function addressOrder(data) {
  return request({
    url: '/shop/order/saveConsignee',
    method: 'put',
    data
  });
};
//编辑订单(修改状态,临时测试用)
export function orderState(data) {
  return request({
    url: '/shop/order/modifyOrderState',
    method: 'put',
    data
  });
};
//编辑订单(取消发货)
export function cancelExpress(data) {
  return request({
    url: '/shop/order/cancelOrder',
    method: 'post',
    data
  });
};
//退款
export function orderdrawback(data) {
  return request({
    url: '/shop/order/drawback',
    method: 'post',
    data
  });
};
//关闭订单
export function orderClose(data) {
  return request({
    url: `shop/order/orderClose`,
    method: 'put',
    data
  });
};
//获取单个订单
export function getOrder(id) {
  return request({
    url: `/shop/order/` + id,
    method: 'get'
  });
};
//获取订单列表
export function getOrderList(data) {
  return request({
    url: '/shop/order/list',
    method: 'post',
    data
  });
};
//获取订单对应商品的门店列表
export function getOrderStoreList(id) {
  return request({
    url: '/shop/order/storeList?id=' + id,
    method: 'get'
  });
};
//订单改价
export function changeMoney(data) {
  return request({
    url: `shop/order/changeMoney`,
    method: 'put',
    data
  });
};
//改价明细
export function changeMoneyList(data) {
  return request({
    url: `/shop/order/changeMoneyList?page=${data.page}&size=${data.size}&order_no=${data.order_id}`,
    method: 'get',
    data
  });
};
// /shop/order/
//**********以下接口为售后部分调用**********
//编辑售后订单记录
export function editAfter(data) {
  return request({
    url: `/shop/aftersale/upAfterState`,
    method: 'put',
    data
  });
};
//获取售后订单记录
export function getAfter(id) {
  return request({
    url: `/shop/aftersale/` + id,
    method: 'get'
  });
};
//获取售后订单列表
export function getAfterList(data, page, size) {
  return request({
    url: `/shop/aftersale/afterSaleList?
    type=${data.type}&
    keyword=${data.keyword}&
    pay_type=${data.pay_type}&
    after_type=${data.after_type}&
    start_time=${data.start_time}&
    end_time=${data.end_time}&
    status=${data.status}&
    after_status=${data.after_status}&
    order_type=${data.order_type}&
    page=${page}&
    size=${size}`,
    method: 'get'
  });
};
//导出订单
export function exportorder(id) {
  return request({
    url: `/shop/export`,
    method: 'get'
  });
};
//批量发货
export function postOrderExportData(data) {
  return request({
    url: '/shop/order/exportData',
    method: 'post',
    data
  });
};
export function getDistribution(id) {
  return request({
    url: `/shop/order/getDistribution?oid=` + id,
    method: 'get'
  });
};
export function localDelivery(data) {
  return request({
    url: '/shop/order/localDelivery',
    method: 'post',
    data
  });
};
export function dadaPay(data) {
  return request({
    url: '/shop/dada/pay',
    method: 'post',
    data
  });
};
export function getcityCode(data) {
  return request({
    url: '/shop/dada/cityCode?oid=' + data.oid,
    method: 'get',
    data
  });
};
export function getdadareasons(data) {
  return request({
    url: '/shop/dada/reasons?oid=' + data.oid,
    method: 'get',
    data
  });
};
export function formalCancel(data) {
  return request({
    url: '/shop/dada/formalCancel',
    method: 'post',
    data
  });
};
