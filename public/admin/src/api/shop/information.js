import request from '@/utils/request';
//获取商户详情列表
export function getMerchantList() {
    return request({
      url: '/shop/merchant',
      method: 'get',
    });
  };
//修改商户详情信息
export function putMerchantList(data) {
    return request({
      url: '/shop/merchant',
      method: 'put',
      data
    });
  };