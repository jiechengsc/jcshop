import request from '@/utils/request'

export function getskulist(data) {
  return request({
    url: '/shop/sku/attr/' + data.id,
    method: 'get',
  })
}


//update
export function savesku(data) {
  return request({
    url: '/shop/sku/attr',
    method: 'post',
    data
  })
}