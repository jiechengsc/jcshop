import request from '@/utils/request';
//**********以下接口为商城端_营销_优惠券功能**********
//增加优惠券
export function addCoupon(data) {
  return request({
    url: `/shop/coupon`,
    method: 'post',
    data
  });
};

//删除优惠券
export function delCoupon(id) {
  return request({
    url: `/shop/coupon/` + id,
    method: 'delete'
  });
};

//修改优惠券
export function editCoupon(data) {
  return request({
    url: `/shop/coupon/` + data.id,
    method: 'put',
    data
  });
};

//获取优惠券
export function getCoupon(id) {
  return request({
    url: `/shop/coupon/` + id,
    method: 'get'
  });
};

//mall端获取优惠券
export function getMallCoupon(id) {
  return request({
    url: `/madmin/coupon/` + id,
    method: 'get'
  });
};

//获取优惠券列表
export function getCouponList(data,page,size) {
  return request({
    url: `/shop/coupon/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};

//发送优惠券
export function sendCoupon(data) {
  return request({
    url: `/shop/coupon/send/` + data.id,
    method: 'post',
    data
  });
};
//优惠券获取列表
export function getuserCouponList(data,page,size) {
  return request({
    url: `/shop/user/coupon/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};

//mall端优惠券获取列表
export function getMadminCouponList(data,page,size) {
  return request({
    url: `/madmin/coupon/list?page=${page}&size=` + size,
    method: 'post',
    data
  });
};
//mall端获取优惠券详情
export function getMadminCouponLister(data) {
  return request({
    url: `/madmin/coupon/` + data.id,
    method: 'get',
    data
  });
};
//mall端审核优惠券
export function putMadminCouponList(data) {
  return request({
    url: `/madmin/coupon/` + data.id,
    method: 'put',
    data
  });
};