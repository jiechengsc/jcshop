import request from '@/utils/request'

export function getuserlist(page,data) {
  return request({
    url: '/shop/admin/list?page=' + page + '&size=10' ,
    method: 'post',
    data
  })
}
export function saveadminuser(data) {
  return request({
    url: '/shop/admin',
    method: 'post',
    data
  })
}
export function getuserdetail(data) {
  return request({
    url: '/shop/admin/' + data.id,
    method: 'get',
  })
}
export function editadminuser(data) {
  return request({
    url: '/shop/admin/' + data.id,
    method: 'put',
    data
  })
}

export function deladminuser(data) {
  return request({
    url: '/shop/admin/' + data.id,
    method: 'delete',
    data
  })
}
