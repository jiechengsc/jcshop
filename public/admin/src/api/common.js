import request from '@/utils/request'
import {
  getLoginType
} from "@/utils/auth";
var qs = require('qs');
export function geturl(t) {
  let type = getLoginType(2)
  if (type) {
    if (type == 'mall') {
      if (t) {
        return 'madmin'
      } else {
        return 'shop'
      }
    } else if (type == 'shop') {
      return 'shop'
    } else if (type == 'saas') {
      return 'admin'
    } else if (type == 'user') {
      return 'suser'
    }
  }
}
export function getpicgeturl(data) {
  return request({
    url: '/shop/pic/geturl',
    method: 'get',
    data
  })
}
export function getsaaspicurl(data) {
  return request({
    url: '/admin/pic/geturl',
    method: 'get',
    data
  })
}
export function getAddonsList(data) {
  return request({
    url: '/' + geturl(1) + '/addons',
    method: 'get',
    data
  })
}
export function getallroleslist(token) {
  return request({
    url: '/' + geturl(1) + '/auth/role',
    method: 'get',
  })
}
export function getlesseeentry(data) {
  return request({
    url: '/' + data.url + '/lessee/entry',
    method: 'get',
    data
  })
}
//saas
export function saas_login(data) {
  return request({
    url: '/admin/operation/login',
    method: 'post',
    data
  })
}
export function saas_logout() {
  let data = {
    n_load: 1
  }
  return request({
    url: '/admin/operation/logout',
    method: 'post',
    data
  })
}
// mall
export function mall_login(data) {
  return request({
    url: '/madmin/operation/login',
    method: 'post',
    data
  })
}
export function mall_logout() {
  let data = {
    n_load: 1
  }
  return request({
    url: '/madmin/operation/logout',
    method: 'post',
    data
  })
}
//shop
export function shop_login(data) {
  return request({
    url: '/shop/operation/login',
    method: 'post',
    data
  })
}
export function shop_logout() {
  let data = {
    n_load: 1
  }
  return request({
    url: '/shop/operation/logout',
    method: 'post',
    data
  })
}
/*图传*/
//图片列表
export function getpicList(data, page, size) {
  data = qs.stringify(data);
  return request({
    url: '/' + geturl() + '/pic/picList?' + data,
    method: 'get',
  });
};
export function groupList() {
  return request({
    url: '/' + geturl() + '/pic/groupList',
    method: 'get'
  });
};
//添加分组
export function addGroup(data) {
  return request({
    url: '/' + geturl() + '/pic/addGroup',
    method: 'post',
    data
  });
};
//编辑分组
export function editGroup(data) {
  return request({
    url: '/' + geturl() + '/pic/editGroup',
    method: 'put',
    data
  });
};
export function delPic(data) {
  return request({
    url: '/' + geturl() + '/pic/delPic/' + data.id,
    method: 'delete',
    data
  })
}
export function delGroup(data) {
  return request({
    url: '/' + geturl() + '/pic/' + data.id,
    method: 'delete',
    data
  })
}
export function information(data) {
  return request({
    url: '/foreign/index/information',
    method: 'get',
    data
  })
}

export function agentrank(data) {
  return request({
    url: '/madmin/agent/rank',
    method: 'get',
    data
  })
}
export function clearCache(data) {
  return request({
    url: '/admin/addons/clearCache',
    method: 'get',
    data
  })
}
