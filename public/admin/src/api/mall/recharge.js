import request from '@/utils/request'
import qs from 'qs'
//列表
export function getrechargelist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/recharge?${params}`,
    method: 'get'
  })
}
export function getrecharge(data) {
  return request({
    url: '/madmin/recharge/' + data.id,
    method: 'get',
    data
  })
}
export function delrecharge(data) {
  return request({
    url: '/madmin/recharge/' + data.id,
    method: 'delete',
  })
}
export function stoprecharge(data) {
  return request({
    url: '/madmin/recharge/' + data.id,
    method: 'post',
  })
}
export function addrecharge(data) {
  return request({
    url: '/madmin/recharge',
    method: 'post',
    data
  })
}
export function editrecharge(data) {
  return request({
    url: '/madmin/recharge/' + data.id,
    method: 'put',
    data
  })
}
