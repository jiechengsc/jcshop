import request from '@/utils/request'

export function getgiftlist(data) {

  return request({
    url: '/madmin/new_kid_gift/list',
    method: 'post',
    data
  })
}
export function savegift(data) {

  return request({
    url: '/madmin/new_kid_gift' ,
    method: 'post',
    data
  })
}
export function giftdetail(data) {

  return request({
    url: '/madmin/new_kid_gift/'+data.id,
    method: 'get',
    data
  })
}
export function editgift(data) {

  return request({
    url: '/madmin/new_kid_gift/'+data.id,
    method: 'put',
    data
  })
}

export function delgift(data) {
  return request({
    url: '/madmin/new_kid_gift/' + data.id,
    method: 'delete',
    data
  })
}

export function getreceivelist(data) {

  return request({
    url: '/madmin/new_kid_gift/receive' ,
    method: 'post',
    data
  })
}
