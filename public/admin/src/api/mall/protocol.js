import request from '@/utils/request';


//**********以下接口为商城端协议功能**********
//增加协议
export function addProtocol(data) {
  return request({
    url: '/madmin/protocol',
    method: 'post',
    data
  });
};

//删除协议
export function delProtocol(id) {
  return request({
    url: '/madmin/protocol/' + id,
    method: 'delete'
  });
};

//更新协议
export function editProtocol(data) {
  return request({
    url: '/madmin/protocol/' + data.id,
    method: 'put',
    data
  });
};

//获取协议
export function getProtocol(id) {
  return request({
    url: '/madmin/protocol/' + id,
    method: 'get'
  });
};

//获取协议列表
export function getProtocols(data,page,size) {
  return request({
    url: '/madmin/protocol/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
