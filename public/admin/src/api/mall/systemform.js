import request from '@/utils/request';
export function formcreate(data) {
  return request({
    url: '/madmin/order/form',
    method: 'post',
    data
  })
}
export function formtemplatelist(data) {
  return request({
    url: `/madmin/order/form?page=${data.page}&&size=${data.size}&source=${data.source}&status=${data.status}`,
    method: 'get',
    data
  });
};
export function getform(id) {
  return request({
    url: '/madmin/order/form/' + id,
    method: 'get'
  });
};
export function editform(data) {
  return request({
    url: '/madmin/order/form/' + data.id,
    method: 'put',
    data
  })
}
export function delform(id) {
  return request({
    url: '/madmin/order/form/' + id,
    method: 'delete'
  });
};
export function  answerdetail(data) {
  return request({
    url: '/madmin/order/form/answer/' + data.id,
    method: 'get'
  });
}
export function answerlist(id, page, size) {
  let data = {}
  data.page = page;
  data.size = size
  return request({
    url: '/madmin/order/form/template/' + id,
    method: 'get',
    data
  });
};

export function formstatus(data) {
  return request({
    url: '/madmin/order/form/status',
    method: 'post',
    data
  })
}

//form//:id
