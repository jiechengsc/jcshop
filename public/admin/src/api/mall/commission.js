import request from '@/utils/request'
import qs from 'qs'
//列表
export function getactivity(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/activity/list?${params}`,
    method: 'get'
  })
}
export function addactivity(data) {
  return request({
    url: '/madmin/commission/activity/add',
    method: 'post',
    data
  })
}

export function getdetail(data) {
  return request({
    url: '/madmin/commission/activity/detail/' + data.id,
    method: 'get',
    data
  })
}
export function activitystatus(data) {
  return request({
    url: '/madmin/commission/activity/status',
    method: 'post',
    data
  })
}

export function delactivity(data) {
  return request({
    url: '/madmin/commission/activity/delete/' + data.id,
    method: 'delete',
  })
}

//列表
export function getawardlist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/activity/award/list?${params}`,
    method: 'get'
  })
}
export function activitysend(data) {
  return request({
    url: '/madmin/commission/activity/send',
    method: 'post',
    data
  })
}
export function activityissue(data) {
  return request({
    url: '/madmin/commission/activity/issue',
    method: 'post',
    data
  })
}


/*分销考核*/
export function getassessmentlist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/assessment/list?${params}`,
    method: 'get'
  })
}
export function getassessmentorder(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/assessment/order?${params}`,
    method: 'get'
  })
}
export function getassessmentdegrade(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/assessment/degrade?${params}`,
    method: 'get'
  })
}

export function addassessment(data) {
  return request({
    url: '/madmin/commission/assessment/add',
    method: 'post',
    data
  })
}

export function getassessment(data) {
  return request({
    url: '/madmin/commission/assessment/detail/' + data.id,
    method: 'get',
    data
  })
}

export function assessmentstatus(data) {
  return request({
    url: '/madmin/commission/assessment/status',
    method: 'post',
    data
  })
}

export function delassessment(data) {
  return request({
    url: '/madmin/commission/assessment/delete/' + data.id,
    method: 'delete',
  })
}

export function getindex(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/commission/assessment/index?${params}`,
    method: 'get'
  })
}

//**********以下接口为分销商额外提现列表模块**********
//获取分销商提现列表
export function getWithdrawList(data, page, size) {
  return request({
    url: '/madmin/commission/assessment/withdraw/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除分销商提现记录
export function delWithdraw(id) {
  return request({
    url: '/madmin/commission/assessment/cash/' + id,
    method: 'delete'
  });
};

//编辑分销商提现记录(通过/拒绝/删除)
export function operateWithdraw(data) {
  return request({
    url: '/madmin/commission/assessment/withdraw/' + data.id,
    method: 'put',
    data
  });
};

//获取单条分销商提现记录
export function getWithdraw(id) {
  return request({
    url: '/madmin/commission/assessment/withdraw/' + id,
    method: 'get'
  });
};
