import request from '@/utils/request';
export function formcreate(data) {
  return request({
    url: '/madmin/form/template',
    method: 'post',
    data
  })
}
export function formtemplatelist(data, page, size) {
  data.page = page;
  data.size = size
  return request({
    url: '/madmin/form/template/list',
    method: 'post',
    data
  });
};
export function getform(id) {
  return request({
    url: '/madmin/form/template/' + id,
    method: 'get'
  });
};
export function editform(data) {
  return request({
    url: '/madmin/form/template/' + data.id,
    method: 'put',
    data
  })
}
export function delform(id) {
  return request({
    url: '/madmin/form/template/' + id,
    method: 'delete'
  });
};
export function answerlist(id, page, size) {
  let data={}
  data.page = page;
  data.size = size
  return request({
    url: '/madmin/form/answer/' + id,
    method: 'post',
    data
  });
};
//form//:id
