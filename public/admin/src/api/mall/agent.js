import request from '@/utils/request'

//列表
export function getagentlist(page, data) {

  return request({
    url: '/madmin/agent/list?page=' + page + '&size=10',
    method: 'post',
    data
  })
}

export function saveagent(data) {
  return request({
    url: '/madmin/agent',
    method: 'post',
    data
  })
}
export function getagentdetail(data) {
  return request({
    url: '/madmin/agent/' + data.id,
    method: 'get',
  })
}
export function editagent(data) {
  return request({
    url: '/madmin/agent/' + data.id,
    method: 'put',
    data
  })
}

export function delagent(data) {
  return request({
    url: '/madmin/agent/' + data.id,
    method: 'delete',
    data
  })
}



//申请
//列表
export function agent_applylist(page, data) {

  return request({
    url: '/madmin/agent/apply/list?page=' + page + '&size=10',
    method: 'post',
    data
  })
}


export function edit_agent_apply(data) {
  return request({
    url: '/madmin/agent/apply/' + data.id,
    method: 'put',
    data
  })
}

export function del_agent_apply(data) {
  return request({
    url: '/madmin/agent/apply/' + data.id,
    method: 'delete',
    data
  })
}


//佣金记录
//列表
export function agent_commissionlist(page, data) {

  return request({
    url: '/madmin/agent/commission/list?page=' + page + '&size=10',
    method: 'post',
    data
  })
}

export function edit_agent_commission(data) {
  return request({
    url: '/madmin/agent/commission/' + data.id,
    method: 'put',
    data
  })
}
//推广订单
//列表
export function agent_orderlist(data) {

  return request({
    url: '/madmin/agent/order',
    method: 'post',
    data
  })
}

// 获取分销关系
export function getAgentLevel(data) {
  return request({
    url: '/madmin/agent/level?user_id=' + data.user_id + '&level=' + data.level + '&page=' + data.page + '&size=' + data.size,
    method: 'get',
    data
  })
}


export function agent_gradelist(data) {

  return request({
    url: '/madmin/agent/grade/list',
    method: 'post',
    data
  })
}

export function save_agent_grade(data) {
  return request({
    url: '/madmin/agent/grade/' + data.id,
    method: 'put',
    data
  })
}

export function get_agentgrade_detail(data) {
  return request({
    url: '/madmin/agent/grade/' + data.id,
    method: 'get',
  })
}


export function del_agent_grade(data) {
  return request({
    url: '/madmin/agent/grade/' + data.id,
    method: 'delete',
    data
  })
}