import request from '@/utils/request'

export function classify_list(data) {
  return request({
    url: '/madmin/classify',
    method: 'get',
  })
}

export function save_classify(data) {
  return request({
    url: '/madmin/classify',
    method: 'post',
    data
  })
}

export function del_classify(data) {
  return request({
    url: '/madmin/classify/' + data.id,
    method: 'delete',
    data
  })
}

//设置分类层级
export function editLevel(data) {
  return request({
    url: '/madmin/classify/level',
    method: 'put',
    data
  })
}

//**********以下接口为商城端商品审核功能**********
//获取待审核商品列表
export function getAuditProList(data, page, size) {
  return request({
    url: '/madmin/spu/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//审核商品
export function auditPro(data) {
  return request({
    url: '/madmin/spu/' + data.id,
    method: 'put',
    data
  });
};

//获取待审核商品
export function getAuditPro(id) {
  return request({
    url: '/madmin/spu/' + id,
    method: 'get'
  });
};
//获取商品
export function getPro(id) {
  return request({
    url: '/madmin/spu/' + id,
    method: 'get'
  });
};

export function sortPro(data) {
  return request({
    url: '/madmin/spu/sort/' + data.id,
    method: 'put',
    data
  });
};
export function setName(data){
  return request({
    url: '/madmin/spu/name/' + data.id,
    method: 'put',
    data
  });
};

// 获取关键字列表
export function getKeyWordList() {
  return request({
    url: '/madmin/search/keyword',
    method: 'get',
  })
}

// 添加关键字
export function postKeyWordList(data) {
  return request({
    url: '/madmin/search/keyword',
    method: 'post',
    data
  })
}


// 修改关键字
export function editKeyWordList(data) {
  return request({
    url: '/madmin/search/keyword/' + data.id,
    method: 'put',
    data
  })
}
// 删除关键字
export function deleteKeyWordList(data) {
  return request({
    url: '/madmin/search/keyword/' + data.id,
    method: 'delete',
    data
  })
}