import request from '@/utils/request';
//**********以下接口为消息通知模块 - 发送类型配置**********
//保存发送设置配置
export function addNoticeConfig(data){
  return request({
    url: '/madmin/msg/config',
    method: 'post',
    data
  });
};

//修改发送设置配置
export function editNoticeConfig(data){
  return request({
    url: '/madmin/msg/config/' + data.id,
    method: 'put',
    data
  });
};

//获取所有配置类型
export function getNoticeConfigAll(type){
  return request({
    url: '/madmin/msg/config?type=' + encodeURI(type),
    method: 'get'
  });
};

//获取所有场景变量
export function getNoticeSceneAll(){
  return request({
    url: '/madmin/msg/config/scene',
    method: 'get'
  });
};


