import request from '@/utils/request'
//获取列表的接口
export function getMembershipList(page, size, data) {
  return request({
    url: '/madmin/membership/level/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  })
}
// 添加
export function addMembership(data) {
  return request({
    url: '/madmin/membership/level',
    method: 'post',
    data
  })
}
// 获取
export function getMembership(data) {
  return request({
    url: '/madmin/membership/level/' + data.id,
    method: 'get',
  })
}
// 删除
export function deleteMembership(data) {
  return request({
    url: '/madmin/membership/level/' + data.id,
    method: 'delete',
  })
}
// 修改
export function editMembership(data) {
  return request({
    url: '/madmin/membership/level/' + data.id,
    method: 'put',
    data
  })
}
// 获取开通记录列表
export function getRecordList(data) {
  return request({
    url: '/madmin/membership/record?page=' + data.page + '&&size=10',
    method: 'get',
  })
}
export function getRecordListDetails(data) {
  return request({
    url: '/madmin/membership/record/' + data.id,
    method: 'get',
  })
}
// /* 会员权益 */
export function contentlist(data) {
  return request({
    url: '/madmin/membership/level/content/list',
    method: 'post',
    data
  })
}
export function addcontent(data) {
  return request({
    url: '/madmin/membership/level/content',
    method: 'post',
    data
  })
}
export function deletecontent(data) {
  return request({
    url: '/madmin/membership/level/content/' + data.id,
    method: 'delete',
  })
}
// 获取
export function getcontent(data) {
  return request({
    url: '/madmin/membership/level/content/' + data.id,
    method: 'get',
    data
  })
}
export function editcontent(data) {
  return request({
    url: '/madmin/membership/level/content/' + data.id,
    method: 'put',
    data
  })
}
