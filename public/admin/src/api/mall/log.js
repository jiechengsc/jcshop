import request from '@/utils/request'
import qs from 'qs'
export function getLogList(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/log/list?${params}`,
    method: 'get'
  })
}