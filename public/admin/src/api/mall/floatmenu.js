import request from '@/utils/request';

// 获取推荐商品配置
export function getFloatConfig() {
    return request({
        url: '/madmin/float/get_config',
        method: 'get',
    })
}
// 创建更新 商品配置
export function saveFloatConfig(data) {
    return request({
        url: '/madmin/float/save',
        method: 'POST',
        data
    })
}
