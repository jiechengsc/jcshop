import request from '@/utils/request'
import qs from 'qs'
//列表
export function getlist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/wx/urllink/list?${params}`,
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: `/madmin/wx/urllink/add`,
    method: 'post',
    data
  })
}
export function del(data) {
  return request({
    url: `/madmin/wx/urllink/delete/${data.id}`,
    method: 'delete',
    data
  })
}
