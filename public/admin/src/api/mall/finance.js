import request from '@/utils/request';
//**********以下接口为商户提现列表模块**********
//获取商户提现列表
export function getMerchantWithdrawList(data, page, size) {
  return request({
    url: '/madmin/merchant/withdraw/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除商户提现记录
export function delMerchantWithdraw(id) {
  return request({
    url: '/madmin/merchant/cash/' + id,
    method: 'delete'
  });
};

//编辑商户提现记录(通过/拒绝/删除)
export function operateMerchantWithdraw(data) {
  return request({
    url: '/madmin/merchant/withdraw/' + data.id,
    method: 'put',
    data
  });
};

//获取单条商户提现记录
export function getMerchantWithdraw(id) {
  return request({
    url: '/madmin/merchant/withdraw/' + id,
    method: 'get'
  });
};

//**********以下接口为分销商提现列表模块**********
//获取分销商提现列表
export function getAgentWithdrawList(data, page, size) {
  return request({
    url: '/madmin/agent/withdraw/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除分销商提现记录
export function delAgentWithdraw(id) {
  return request({
    url: '/madmin/agent/cash/' + id,
    method: 'delete'
  });
};

//编辑分销商提现记录(通过/拒绝/删除)
export function operateAgentWithdraw(data) {
  return request({
    url: '/madmin/agent/withdraw/' + data.id,
    method: 'put',
    data
  });
};

//获取单条分销商提现记录
export function getAgentWithdraw(id) {
  return request({
    url: '/madmin/agent/withdraw/' + id,
    method: 'get'
  });
};

//**********以下接口为用户提现列表模块**********
//获取用户提现列表
export function getUserWithdrawList(data, page, size) {
  return request({
    url: '/madmin/user/withdraw/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除用户提现记录
export function delUserWithdraw(id) {
  return request({
    url: '/madmin/user/cash/' + id,
    method: 'delete'
  });
};

//编辑用户提现记录(通过/拒绝/删除)
export function operateUserWithdraw(data) {
  return request({
    url: '/madmin/user/withdraw/' + data.id,
    method: 'put',
    data
  });
};

//获取单条用户提现记录
export function getUserWithdraw(id) {
  return request({
    url: '/madmin/user/withdraw/' + id,
    method: 'get'
  });
};

//**********以下接口为用户总金额列表模块**********
//获取分销商提现列表
export function getUserCashList(data, page, size) {
  return request({
    url: '/madmin/user/cash/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//删除用户总金额记录
export function delUserCash(user_id) {
  return request({
    url: '/madmin/user/cash/' + user_id,
    method: 'delete'
  });
};

//编辑用户总金额记录
export function editUserCash(data) {
  return request({
    url: '/madmin/user/cash/' + data.user_id,
    method: 'put',
    data
  });
};

//获取单条用户总金额记录
export function getUserCash(user_id) {
  return request({
    url: '/madmin/user/cash/' + user_id,
    method: 'get'
  });
};

export function getRecordList(data, page, size) {
  return request({
    url: '/madmin/recharge/record/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};
export function getIntegralList(data, page, size) {
  data.page = page;
  data.size = size
  return request({
    url: '/madmin/integral/list',
    method: 'post',
    data
  });
};

//record

// 获取商户余额信息
export function getUserMerchantList(data) {
  return request({
    url: '/madmin/user/cash/merchant?page=' + data.page + '&size=' + data.size + '&start_time=' + data.start_time + '&end_time=' + data.end_time,
    method: 'post',
    data
  });
};

//获取商户 入账详情
export function setMalllementList(data, page, size) {
  return request({
    url: '/madmin/merchant/settlement/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  })
}