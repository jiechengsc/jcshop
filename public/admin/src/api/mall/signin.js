import request from '@/utils/request'

export function getsignin(type){
  return request({
    url: '/madmin/signin',
    method: 'get'
  });
};

export function savesignin(data) {

  return request({
    url: '/madmin/signin' ,
    method: 'put',
    data
  })
}