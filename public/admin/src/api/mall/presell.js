import request from '@/utils/request'
import qs from 'qs'
//列表
export function getpreselllist(data) {
  let params = qs.stringify(data)
  return request({
    url: `/madmin/presell?${params}`,
    method: 'get'
  })
}
export function getpresell(data) {
  return request({
    url: '/madmin/presell/' + data.id,
    method: 'get',
    data
  })
}
export function delpresell(data) {
  return request({
    url: '/madmin/presell/' + data.id,
    method: 'delete',
  })
}
export function stoppresell(data) {
  return request({
    url: '/madmin/presell/' + data.id,
    method: 'post',
  })
}
export function addpresell(data) {
  return request({
    url: '/madmin/presell',
    method: 'post',
    data
  })
}
export function editpresell(data) {
  return request({
    url: '/madmin/presell/' + data.id,
    method: 'put',
    data
  })
}