import request from '@/utils/request';
//**********以下接口为商城端首页数据统计**********
//获取统计数据 - 头部
export function getDataTop() {
  return request({
    url: '/madmin/statistic/top',
    method: 'post'
  });
};


//获取统计数据 - 总数
export function getDataTotal(data) {
  return request({
    url: '/madmin/statistic/total',
    method: 'post',
    data
  });
};

//获取统计数据 - 图表
export function getDataCount(data) {
  return request({
    url: '/madmin/statistic/count',
    method: 'post',
    data
  });
};

//获取统计数据 - 分销
export function getDataAgent() {
  return request({
    url: '/madmin/statistic/agent',
    method: 'post'
  });
};

//获取统计数据 - 用户消费排行
export function getDataUserRank() {
  return request({
    url: '/madmin/statistic/consumption_ranking',
    method: 'post'
  });
};

//获取统计数据 - 商品销量排行
export function getDataProductRank() {
  return request({
    url: '/madmin/statistic/sales_ranking',
    method: 'post'
  });
};

