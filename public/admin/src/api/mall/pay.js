import request from '@/utils/request';
//**********以下接口为支付模板**********
//获取支付模板列表(所有)
export function getPayAll(){
  return request({
    url: '/madmin/pay/mode',
    method: 'get'
  });
};

//获取支付模板列表
export function getPayList(data,page,size) {
  return request({
    url: '/madmin/pay/mode/list?page=' + page + '&size=' + size,
    method: 'post',
    data
  });
};

//新增支付模板
export function addPay(data){
  return request({
    url: '/madmin/pay/mode',
    method: 'post',
    data,
    headers:{
      'Content-Type':'multipart/form-data'
    }
  });
};

//删除支付模板
export function delPay(id){
    return request({
      url: '/madmin/pay/mode/' + id,
      method: 'delete'
    });
};

//编辑支付模板
export function editPay(data,id){
  return request({
    url: '/madmin/pay/mode/' + id,
    method: 'post',
    data,
    headers:{
      'Content-Type':'multipart/form-data'
    }
  });
};

//获取支付模板
export function getPay(id){
  return request({
    url: '/madmin/pay/mode/' + id,
    method: 'get'
  });
};
