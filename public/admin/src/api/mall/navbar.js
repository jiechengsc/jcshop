import request from '@/utils/request'

//修改用户信息
export function allEditUser(data) {
    return request({
        url: '/' + data.typer + '/admin/selfUp/' + data.id,
        method: 'PUT',
        data
    })
}
