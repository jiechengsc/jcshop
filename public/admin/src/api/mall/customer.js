import request from '@/utils/request';


export function getwxcompany(data) {
    return request({
        url: `/madmin/wx/company?page=${data.page}&&size=10`,
        method: 'get',
    })
}

export function savewxcompany(data) {
  return request({
    url: '/madmin/wx/company/save',
    method: 'post',
    data
  })
}
export function getwxcompanydetail(data) {
  return request({
    url: '/madmin/wx/company/read/'+data.id,
    method: 'get',
    data
  })
}
export function getwxcompanycustomer(data) {
  return request({
    url: '/madmin/wx/company/customer?id='+data.id,
    method: 'get',
    data
  })
}

export function editwxcompany(data) {
  return request({
    url: '/madmin/wx/company/update',
    method: 'put',
    data
  })
}

export function deladwxcompany(data) {
  return request({
    url: '/madmin/wx/company/delete/'+data.id,
    method: 'delete',
    data
  })
}


export function getwxcustomer(data) {
    return request({
        url: `/madmin/wx/customer?page=${data.page}&&size=10`,
        method: 'get',
    })
}

export function savewxcustomer(data) {
  return request({
    url: '/madmin/wx/customer/save',
    method: 'post',
    data
  })
}
export function getwxcustomerdetail(data) {
  return request({
    url: '/madmin/wx/customer/read/'+data.id,
    method: 'get',
    data
  })
}
export function editwxcustomer(data) {
  return request({
    url: '/madmin/wx/customer/update',
    method: 'put',
    data
  })
}

export function deladwxcustomer(data) {
  return request({
    url: '/madmin/wx/customer/delete/'+data.id,
    method: 'delete',
    data
  })
}

export function getwxcs(data) {
    return request({
        url: `/shop/wx/cs?page=${data.page}&&size=10`,
        method: 'get',
    })
}
