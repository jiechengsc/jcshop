import request from '@/utils/request'

export function getuserlist(page,data,size) {

  return request({
    url: '/madmin/user/list?page=' + page + '&size=' + size ,
    method: 'post',
    data
  })
}

export function saveadminuser(data) {
  return request({
    url: '/madmin/user',
    method: 'post',
    data
  })
}
export function getuserdetail(data) {
  return request({
    url: '/madmin/user/' + data.id,
    method: 'get',
  })
}
export function editadminuser(data) {
  return request({
    url: '/madmin/user/' + data.id,
    method: 'put',
    data
  })
}

export function deladminuser(data) {
  return request({
    url: '/madmin/user/' + data.id,
    method: 'delete',
    data
  })
}

//获取用户余额列表(临时,原接口意思财务模块中)
// export function userMoneyList(data,page,size) {
//   return request({
//     url: `/madmin/user/cash/total/list?page=${page}&size=` + size,
//     method: 'post',
//     data
//   })
// };

//获取用户积分列表
export function userScoreList(data,page,size) {
	data.page = page;
	data.size = size
  return request({
    url: `/madmin/integral/list`,
    method: 'post',
    data
  })
};
export function userMoneyList(data,page,size) {
  return request({
    url: `/madmin/recharge/record/list?page=${page}&size=` + size,
    method: 'post',
    data
  })
};

//修改用户余额
export function editUserMoney(data) {
  return request({
    url: `/madmin/user/cash/recharge/` + data.user_id,
    method: 'put',
    data
  })
};

//修改用户积分
export function editUserScore(data) {
  return request({
    url: `/madmin/user/cash/integral/` + data.user_id,
    method: 'put',
    data
  })
};
export function editUserLevel(data) {
  return request({
    url: `/madmin/membership/level/user/` + data.id,
    method: 'put',
    data
  })
};
export function userCouponList(data,page,size) {
  return request({
    url: `/madmin/user/coupon/list?page=${page}&size=` + size,
    method: 'post',
    data
  })
};
export function getuserstatistic(data) {
  return request({
    url: '/shop/statistic/user/total/' + data.id,
    method: 'get',
  })
}
///
