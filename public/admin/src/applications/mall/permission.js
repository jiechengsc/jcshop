import router from './router'
import store from './store'
import {
  Message,
  MessageBox
} from 'element-ui'
import NProgress from 'nprogress' // progress bar
import 'nprogress/nprogress.css' // progress bar style
import {
  getToken,
  getLoginType
} from '@/utils/auth' // get token from cookie
NProgress.configure({
  showSpinner: false
}) // NProgress Configuration
const whiteList = ['/login'] // no redirect whitelist
router.beforeEach(async (to, from, next) => {
  // start progress bar
  if (to.path == '/404') {
    next(false)
  } else {
    NProgress.start()
    // if (!store.getters.info) {
    //   var info = await store.dispatch('common/getinformation');
    //  if(info&&info.icon){
    //    var link = document.querySelector("link[rel*='icon']") || document.createElement('link');
    //    link.type = 'image/x-icon';
    //    link.rel = 'shortcut icon';
    //    link.href =info.icon;
    //    document.getElementsByTagName('head')[0].appendChild(link)
    //  }
    // }
    // // set page title
    
    // determine whether the user has logged in
    const hasToken = getToken()
    if (hasToken) {
      if (to.path === '/login') {
        // if is logged in, redirect to the home page
        next({
          path: '/'
        })
        NProgress.done()
      } else {
        // determine whether the user has obtained his permission roles through getInfo
        const hasRoles = store.getters.ruleList && store.getters.ruleList.length > 0
        if (hasRoles) {
          next()
        } else {
          try {
            // get user info
            // note: roles must be a object array! such as: ['admin'] or ,['developer','editor']
            await store.dispatch('common/getAddonsList')
            const ruleList = await store.dispatch('user/getrule')
            // generate accessible routes map based on roles
            // const ruleList = store.getters.ruleList
            // store.commit('user/SET_ROLET', '2')
            const accessRoutes = await store.dispatch('permission/generateRoutes', ruleList)
            // dynamically add accessible routes
            router.addRoutes(accessRoutes)
            // hack method to ensure that addRoutes is complete
            // set the replace: true, so the navigation will not leave a history record
            next({
              ...to,
              replace: true
            })
          } catch (error) {
            // remove token and go to login page to re-login
            await store.dispatch('user/resetToken')
            Message.error(error || 'Has Error')
            next(`/login`)
            NProgress.done()
          }
        }
      }
    } else {
      /* has no token*/
      if (whiteList.indexOf(to.path) !== -1) {
        // in the free login whitelist, go directly
        next()
      } else {
        next(`/login`)
        NProgress.done()
      }
    }
  }
})
router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
