import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
/* Layout */
import Layout from '@/layout'
import layoutEmpty from '@/layout/layoutEmpty'
/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */
/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [{
  path: '/redirect',
  component: Layout,
  hidden: true,
  children: [{
    path: '/redirect/:path(.*)',
    component: () => import('@/views/redirect/index')
  }]
}, {
  path: '/login',
  component: () => import('@/views/login/mall_login'),
  hidden: true
}, {
  path: '/dndlist',
  component: () => import('@/views/shop/dndlist'),
  hidden: true,
  meta: {
    id: 14
  }
}, {
  path: '/custommenu',
  component: () => import('@/views/shop/dndlist/custommenu'),
  hidden: true,
  meta: {
    id: 16
  }
}, {
  path: '/404',
  component: () => import('@/views/404'),
  hidden: true
}, {
  path: '/',
  component: Layout,
  redirect: '/dashboard',
  hidden: true,
  children: [{
    path: 'dashboard',
    name: '首页',
    component: () => import('@/views/dashboard/index'),
    meta: {
      title: '首页',
      icon: 'dashboard'
    }
  }]
}]
/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const mall_asyncRoutes = [{
    path: '/mall',
    component: Layout,
    redirect: '/mall/index',
    meta: {
      title: '首页',
      icon: 'el-sc-shouyexuanzhong',
      id: 1
    },
    children: [{
      path: 'index',
      name: '商户首页',
      component: () => import('@/views/mall/mall_index/index'),
      meta: {
        title: '首页',
        icon: 'dashboard',
        id: 1
      }
    }]
  }, {
    path: '/product',
    component: Layout,
    name: 'product',
    redirect: '/product/list/sell',
    meta: {
      title: '商品',
      icon: 'el-sc-shangpin1',
      id: [2, 3, 4, 5, 6, 7]
    },
    children: [{
      path: 'category',
      name: 'category',
      component: () => import('@/views/mall/product/categoryindex'),
      meta: {
        title: '分类管理',
        icon: 'dashboard',
        level: 2,
        parents: 'product',
        id: 2
      }
    }, {
      path: 'list',
      name: 'list',
      component: layoutEmpty,
      meta: {
        title: '商品列表',
        icon: 'dashboard',
        level: 2,
        parents: 'product',
        id: [3, 4]
      },
      children: [{
        path: 'sell',
        name: 'productList',
        component: () => import('@/views/shop/product/list'),
        meta: {
          icon: 'dashboard',
          title: '出售中',
          level: 3,
          parents: 'product',
          id: 3
        }
      }, {
        path: 'listStock',
        name: 'productListStock',
        component: () => import('@/views/shop/product/list'),
        meta: {
          icon: 'dashboard',
          title: '仓库中',
          level: 3,
          parents: 'product',
          id: 3
        }
      }, {
        path: 'sellout',
        name: 'productselloutlist',
        component: () => import('@/views/shop/product/selloutlist'),
        meta: {
          icon: 'dashboard',
          title: '已售罄',
          level: 3,
          parents: 'product',
          id: 3
        }
      }, {
        path: 'listAudit',
        name: 'productListAudit',
        component: () => import('@/views/mall/product/listAudit'),
        meta: {
          icon: 'dashboard',
          title: '审核商品',
          level: 3,
          parents: 'product',
          id: 4
        }
      }, ]
    }, {
      path: 'freight',
      name: 'freight',
      component: layoutEmpty,
      meta: {
        title: '物流设置',
        icon: 'dashboard',
        level: 2,
        parents: 'product',
        id: [5, 6, 7]
      },
      children: [{
        path: 'list',
        name: 'freightlist',
        component: () => import('@/views/shop/freight/index'),
        meta: {
          icon: 'dashboard',
          title: '运费模板',
          level: 3,
          parents: 'freight',
          id: 5
        }
      }, {
        path: 'returnAddresslist',
        name: 'settingLogisticsReturnList',
        component: () => import('@/views/shop/return/returnAddress'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '退货地址',
          level: 3,
          parents: 'settingLogisticsReturn',
          id: 6
          //activeMenu: '/notice/sms/template'
        }
      }, {
        path: 'alicloud',
        name: 'settingAlicloud',
        component: () => import('@/views/mall/setting/alicloud/index'),
        meta: {
          icon: 'dashboard',
          title: '物流设置',
          level: 3,
          parents: 'SystemSetting',
          id: 7
        }
      }, {
        path: 'item',
        name: 'settingLogisticsReturnItem',
        component: () => import('@/views/shop/return/returnAddressItem'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '操作数据',
          level: 3,
          parents: 'settingLogisticsReturn',
          activeMenu: '/setting/shop/return/list',
          id: 6
        },
        hidden: true
      }]
    }, {
      path: 'item',
      name: 'productItem',
      component: () => import('@/views/shop/product/product'),
      meta: {
        icon: 'dashboard',
        title: '商品操作',
        level: 2,
        parents: 'product',
        id: 3
      },
      hidden: true
    }, {
      path: 'keyword',
      name: 'settingKeyword',
      component: () => import('@/views/mall/product/keyword'),
      meta: {
        title: '关键字管理',
        icon: 'dashboard',
        level: 2,
        parents: 'product',
        id: 56
      }
    }, ]
  }, {
    path: '/order',
    component: Layout,
    name: 'order',
    redirect: '/order/list/all',
    meta: {
      title: '订单',
      icon: 'el-sc-51',
      level: 1,
      id: [8, 9, 10, 11, 12, 13]
    },
    children: [{
      path: 'list',
      name: 'orderlist',
      component: layoutEmpty,
      meta: {
        title: '订单列表',
        icon: 'dashboard',
        level: 2,
        parents: 'product',
        id: [8, 9, 10]
      },
      children: [{
        path: 'all',
        name: 'orderList',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '全部订单',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'unpaid',
        name: 'Unpaid',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '待支付',
          status: '1',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'todelivered',
        name: 'Todelivered',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '待发货',
          status: '2',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'towritten',
        name: 'Towritten',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '待核销',
          status: '13',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'delivered',
        name: 'Delivered',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '已发货',
          status: '3',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'received',
        name: 'Received',
        component: () => import('@/views/shop/order/list'),
        meta: {
          icon: 'dashboard',
          title: '已收货',
          status: '8',
          level: 3,
          parents: 'order',
          id: 8
        }
      }, {
        path: 'comment',
        name: 'commentList',
        component: () => import('@/views/shop/order/comment'),
        meta: {
          icon: 'dashboard',
          title: '订单评论',
          level: 3,
          parents: 'order',
          id: 9
        }
      }, {
        path: 'item',
        name: 'orderItem',
        component: () => import('@/views/shop/order/item'),
        meta: {
          icon: 'dashboard',
          title: '订单详情',
          level: 3,
          parents: 'order',
          activeMenu: '/order/list',
          id: 8
        },
        hidden: true
      }, {
        path: 'c_m_list',
        name: 'changeMoneyList',
        component: () => import('@/views/shop/order/changeMoneyList'),
        meta: {
          icon: 'dashboard',
          title: '改价详情',
          level: 3,
          parents: 'order',
          activeMenu: '/order/list',
          id: 8
        },
        hidden: true
      }, {
        path: 'after',
        name: 'afterList',
        component: () => import('@/views/shop/order/after'),
        meta: {
          icon: 'dashboard',
          title: '维权订单',
          level: 3,
          parents: 'order',
          id: 10
        }
      }, {
        path: 'afterItem',
        name: 'afterItem',
        component: () => import('@/views/shop/order/afterItem'),
        meta: {
          icon: 'dashboard',
          title: '维权详情',
          level: 3,
          parents: 'order',
          activeMenu: '/order/after',
          id: 10
        },
        hidden: true
      }, {
        path: 'bulkdelivery',
        name: 'bulkdelivery',
        component: () => import('@/views/shop/order/bulkdelivery'),
        meta: {
          icon: 'dashboard',
          title: '批量发货',
          level: 2,
          parents: 'order',
          id: 11
        }
      }, ]
    }, {
      path: 'invoice',
      name: 'invoice',
      component: () => import('@/views/shop/order/invoice'),
      meta: {
        icon: 'dashboard',
        title: '发票列表',
        level: 2,
        parents: 'order',
        id: 12
      }
    }, {
      path: 'setting',
      name: 'setting',
      component: layoutEmpty,
      meta: {
        title: '交易设置',
        icon: 'dashboard',
        level: 2,
        parents: 'order',
        id: [12, 13]
      },
      children: [{
        path: 'deal',
        name: 'settingDeal',
        component: () => import('@/views/mall/setting/deal/deal'),
        meta: {
          icon: 'dashboard',
          title: '交易设置',
          level: 3,
          parents: 'SystemSetting',
          id: 12
        }
      }, {
        path: 'safeguard',
        name: 'settingSafeguard',
        component: () => import('@/views/mall/setting/safeguard/safeguard'),
        meta: {
          icon: 'dashboard',
          title: '维权设置',
          level: 3,
          parents: 'SystemSetting',
          id: 13
        }
      }, ]
    }, ],
  }, {
    path: '/stores',
    component: Layout,
    name: 'stores',
    redirect: '/stores/renovation',
    meta: {
      title: 'DIY',
      icon: 'el-sc-sheji',
      level: 1,
      id: [14, 15, 16]
    },
    children: [{
      path: 'renovation',
      name: 'renovation',
      component: () => import('@/views/shop/dndlist/home'),
      meta: {
        title: '店铺装修',
        icon: 'dashboard',
        level: 2,
        parents: 'stores',
        id: 14
      }
    }, {
      path: 'template',
      name: 'template',
      component: () => import('@/views/mall/template/index'),
      meta: {
        title: '模板中心',
        icon: 'dashboard',
        level: 2,
        parents: 'stores',
        id: 15
      }
    }, {
      path: 'custommenulist',
      name: 'custommenulist',
      component: () => import('@/views/shop/dndlist/custommenulist'),
      meta: {
        title: '底部菜单',
        icon: 'dashboard',
        level: 2,
        parents: 'stores',
        id: 16
      }
    }, {
      path: 'floatmenu',
      name: 'floatmenu',
      component: () => import('@/views/shop/dndlist/floatmenu'),
      meta: {
        title: '悬浮菜单',
        icon: 'dashboard',
        level: 2,
        parents: 'stores',
        id: 68
      }
    }, {
      path: 'theme',
      name: 'Theme',
      component: () => import('@/views/shop/dndlist/theme'),
      meta: {
        icon: 'dashboard',
        title: '主题色',
        level: 2,
        parents: 'stores',
        id: 69
      }
    }, ]
  }, {
    path: '/user',
    component: Layout,
    name: 'useradmin',
    redirect: '/user/user',
    meta: {
      title: '用户',
      icon: 'el-sc-yonghu',
      level: 1,
      id: [17]
    },
    children: [{
      path: 'user',
      name: 'userlist',
      component: () => import('@/views/mall/user/index'),
      meta: {
        icon: 'dashboard',
        title: '用户',
        level: 2,
        parents: 'user',
        id: 17
      }
    }, {
      path: 'item',
      name: 'userItem',
      component: () => import('@/views/mall/user/item'),
      meta: {
        icon: 'dashboard',
        title: '用户详情',
        level: 2,
        parents: 'user',
        activeMenu: '/user/user',
        id: 17
      },
      hidden: true
    }]
  }, {
    path: '/finance',
    component: Layout,
    name: 'Finance',
    redirect: '/finance/withdrawal',
    meta: {
      title: '财务',
      icon: 'el-sc-caiwu4',
      level: 1,
      id: [18, 19, 20, 21, 22, 23, 57, 58]
    },
    children: [{
      path: 'withdrawal',
      name: 'financeWithdrawal',
      component: () => import('@/views/mall/finance/withdrawal'),
      meta: {
        icon: 'dashboard',
        title: '提现申请',
        level: 2,
        parents: 'finance',
        id: 18
      }
    }, {
      path: 'balance',
      name: 'financeBalance',
      component: () => import('@/views/mall/finance/balance'),
      meta: {
        icon: 'dashboard',
        title: '金额明细',
        level: 2,
        parents: 'finance',
        id: 19
      }
    }, {
      path: 'integral',
      name: 'financeIntegral',
      component: () => import('@/views/mall/finance/integral'),
      meta: {
        icon: 'dashboard',
        title: '积分明细',
        level: 2,
        parents: 'finance',
        id: 20,
        a_id: 7
      }
    }, {
      path: 'usermerchant',
      name: 'usermerchantScore',
      component: () => import('@/views/mall/finance/usermerchant'),
      meta: {
        icon: 'dashboard',
        title: '商户明细',
        level: 2,
        parents: 'SystemSetting',
        id: 57,
        a_id: 12
      }
    }, {
      path: 'score',
      name: 'settingScore',
      component: () => import('@/views/mall/setting/score/score'),
      meta: {
        icon: 'dashboard',
        title: '积分余额',
        level: 2,
        parents: 'SystemSetting',
        id: 21,
        // a_id: 7
      }
    }, {
      path: 'pay',
      name: 'settingPay',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        title: '支付设置',
        icon: 'nested',
        level: 2,
        id: [22, 23]
      },
      children: [{
          path: 'mode',
          name: 'settingPayMode',
          component: () => import('@/views/mall/setting/payment/mode'),
          meta: {
            icon: 'el-icon-chat-dot-round',
            title: '支付方式',
            level: 3,
            parents: 'settingPay',
            id: 22
          }
        },
        // {
        //   path: 'payment',
        //   name: 'settingPayPayment',
        //   component: () => import('@/views/mall/setting/payment/payment'),
        //   meta: {
        //     icon: 'el-icon-chat-dot-round',
        //     title: '打款设置',
        //     level: 3,
        //     parents: 'settingPay'
        //     //activeMenu: '/notice/sms/template'
        //   }
        // },
        {
          path: 'payTemplate',
          name: 'settingPayTemplate',
          component: () => import('@/views/mall/pay/payList'),
          meta: {
            icon: 'el-icon-chat-dot-round',
            title: '支付模板',
            level: 3,
            parents: 'settingPay',
            id: 23
            //activeMenu: '/notice/sms/template'
          }
        }, {
          path: 'payTemplateItem',
          name: 'settingPayTemplateItem',
          component: () => import('@/views/mall/pay/payItem'),
          meta: {
            icon: 'el-icon-chat-dot-round',
            title: '支付模板',
            level: 3,
            parents: 'settingPay',
            id: 23
            //activeMenu: '/notice/sms/template'
          },
          hidden: true
        }
      ]
    }, {
      path: 'entry',
      name: 'financeEntry',
      component: () => import('@/views/shop/finance/entry'),
      meta: {
        icon: 'dashboard',
        title: '入账详情',
        level: 2,
        parents: 'finance',
        id: 58,
      }
    }]
  }, {
    path: '/agent',
    component: Layout,
    name: 'agent',
    redirect: '/agent/list',
    meta: {
      title: '分销',
      icon: 'el-sc-fenxiaoshang',
      level: 1,
      id: [24, 25, 26, 27, 70]
    },
    children: [{
      path: 'list',
      name: 'agentindex',
      component: () => import('@/views/mall/agent/index'),
      meta: {
        icon: 'dashboard',
        title: '分销商',
        level: 2,
        parents: 'agent',
        id: 24
      }
    }, {
      path: 'apply',
      name: 'agentapply',
      component: () => import('@/views/mall/agent/apply'),
      meta: {
        icon: 'dashboard',
        title: '申请列表',
        level: 2,
        parents: 'agent',
        id: 25
      }
    }, {
      path: 'order',
      name: 'agentorder',
      component: () => import('@/views/mall/agent/order'),
      meta: {
        icon: 'dashboard',
        title: '分销订单',
        level: 2,
        parents: 'agent',
        id: 26
      }
    }, {
      path: 'agentsetting',
      name: 'agentsetting',
      component: () => import('@/views/mall/setting/agent/agent'),
      meta: {
        icon: 'dashboard',
        title: '分销设置',
        level: 2,
        parents: 'agent',
        id: 27
      }
    }, {
      path: 'agentgrade',
      name: 'agentgrade',
      component: () => import('@/views/mall/agent/grade'),
      meta: {
        icon: 'dashboard',
        title: '分销等级',
        level: 2,
        parents: 'agent',
        id: 70
      }
    }, ]
  }, {
    path: '/multiple',
    component: Layout,
    name: 'multiple',
    redirect: '/multiple/setting',
    meta: {
      title: '多端',
      icon: 'el-sc-ziyuanxhdpi',
      level: 1,
      id: 28
    },
    children: [{
      path: 'setting',
      name: 'multipleSetting',
      component: () => import('@/views/mall/multiple/multipleList'),
      meta: {
        icon: 'dashboard',
        title: '多端设置',
        level: 2,
        parents: 'multiple',
        id: 28
      }
    }, {
      path: 'wx',
      name: 'multipleWx',
      component: () => import('@/views/mall/multiple/multipleItemHKey'),
      meta: {
        icon: 'dashboard',
        title: '公众号设置',
        level: 2,
        parents: 'multiple',
        activeMenu: '/multiple/setting',
        id: 28
      },
      hidden: true
    }, {
      path: 'mp',
      name: 'multipleMp',
      component: () => import('@/views/mall/multiple/multipleItemMp'),
      meta: {
        icon: 'dashboard',
        title: '小程序设置',
        level: 2,
        parents: 'multiple',
        activeMenu: '/multiple/setting',
        id: 28
      },
      hidden: true
    }, {
      path: 'mpKey',
      name: 'multipleMpKey',
      component: () => import('@/views/mall/multiple/multipleItemMpKey'),
      meta: {
        icon: 'dashboard',
        title: '小程序Key',
        level: 2,
        parents: 'multiple',
        activeMenu: '/multiple/setting',
        id: 28
      },
      hidden: true
    }]
  }, {
    path: '/setting',
    component: Layout,
    redirect: '/setting/shop/base',
    name: 'SystemSetting',
    meta: {
      title: '系统',
      icon: 'el-sc-shezhi10',
      level: 1,
      id: [29, 30, 31, 32, 33, 34, 35, 64, 77,88,89]
    },
    children: [{
      path: 'shop',
      name: 'settingShop',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        icon: 'dashboard',
        title: '商城设置',
        level: 2,
        parents: 'setting',
        id: [29, 30, 31, 32, 64,88,89]
      },
      children: [{
        path: 'base',
        name: 'settingShopBase',
        component: () => import('@/views/mall/setting/base/base'),
        meta: {
          icon: 'dashboard',
          title: '基础设置',
          level: 3,
          parents: 'setting',
          id: 29
        }
      }, {
        path: 'share',
        name: 'settingShopShare',
        component: () => import('@/views/mall/setting/base/share'),
        meta: {
          icon: 'dashboard',
          title: '分享设置',
          level: 3,
          parents: 'settingShop',
          id: 30
        }
      }, {
        path: 'lottery',
        name: 'settingShopLottery',
        component: () => import('@/views/mall/setting/prize'),
        meta: {
          icon: 'dashboard',
          title: '抽奖设置',
          level: 3,
          id: 66,
          a_id: [17, 18],
          parents: 'setting',
        },
      }, {
        path: 'announcement',
        name: 'settingShopAnnouncementList',
        component: () => import('@/views/shop/announcement/announcement'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '公告列表',
          level: 3,
          parents: 'settingShopAnnouncement',
          id: 31
          //activeMenu: '/notice/sms/template'
        }
      }, {
        path: 'item',
        name: 'announcementItem',
        component: () => import('@/views/shop/announcement/announcementItem'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '操作数据',
          level: 3,
          parents: 'settingShopAnnouncement',
          activeMenu: '/setting/shop/announcement/list',
          id: 31
        },
        hidden: true
      }, {
        path: 'contact',
        name: 'contact',
        component: () => import('@/views/mall/setting/base/contact'),
        meta: {
          icon: 'dashboard',
          title: '联系我们',
          level: 3,
          parents: 'settingShop',
          id: 32
        }
      }, {
        path: 'pages',
        name: 'settingShopContact',
        component: () => import('@/views/mall/page/index'),
        meta: {
          icon: 'dashboard',
          title: '页面管理',
          level: 3,
          parents: 'settingShop',
          id: 65
        }
      }, ]
    }, {
      path: 'protocol',
      component: layoutEmpty,
      name: 'protocol',
      redirect: 'noRedirect',
      meta: {
        title: '协议列表',
        icon: 'nested',
        level: 2,
        id: 33
      },
      children: [{
        path: 'list',
        name: 'protocolList',
        component: () => import('@/views/mall/protocol/list'),
        meta: {
          icon: 'dashboard',
          title: '协议列表',
          level: 3,
          parents: 'protocol',
          id: 33
        }
      }, {
        path: 'item',
        name: 'protocolItem',
        component: () => import('@/views/mall/protocol/item'),
        meta: {
          icon: 'dashboard',
          title: '协议操作操作',
          level: 3,
          parents: 'protocol',
          id: 33
        },
        hidden: true
      }]
    },
         // {
         //      path: 'addons',
         //      component: layoutEmpty,
         //      name: 'addons',
         //      redirect: 'noRedirect',
         //      meta: {
         //        title: '插件列表',
         //        icon: 'nested',
         //        level: 2,
         //        id: 88
         //      },
         //      children: [{
         //        path: 'list',
         //        name: 'addonsList',
         //        component: () => import('@/views/mall/addons/index'),
         //        meta: {
         //          icon: 'dashboard',
         //          title: '插件列表',
         //          level: 3,
         //          parents: 'addons',
         //          id: 88
         //        }
         //      }]
         //    },


            {
              path: 'copyright',
              component: layoutEmpty,
              name: 'copyright',
              redirect: 'noRedirect',
              meta: {
                title: '版权',
                icon: 'nested',
                level: 2,
                id: 89
              },
              children: [{
                path: 'list',
                name: 'copyrightList',
                component: () => import('@/views/mall/copyright/index'),
                meta: {
                  icon: 'dashboard',
                  title: '版权',
                  level: 3,
                  parents: 'copyright',
                  id: 89
                }
              }]
            },

    {
      path: 'sysRight',
      name: 'settingSysRight',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        title: '系统权限',
        icon: 'nested',
        level: 2,
        id: [34, 35]
      },
      children: [{
        path: 'role',
        name: 'settingSysRightRole',
        component: () => import('@/views/mall/role/index'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '角色管理',
          level: 3,
          parents: 'settingSysRight',
          id: 34
        }
      }, {
        path: 'admin',
        name: 'settingSysRightAdmin',
        component: () => import('@/views/mall/admin/index'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '管理员管理',
          level: 3,
          parents: 'settingSysRight',
          id: 35
          //activeMenu: '/notice/sms/template'
        }
      }, {
        path: 'log',
        name: 'settingSysRightLog',
        component: () => import('@/views/mall/loglist/index'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '操作日志',
          level: 3,
          parents: 'settingSysRight',
          id: 77
          //activeMenu: '/notice/sms/template'
        },
        // hidden: true
      }]
    }, ]
  }, {
    path: '/application',
    component: Layout,
    name: 'application',
    redirect: '/application/index',
    meta: {
      title: '应用',
      icon: 'el-sc-yingyong',
      level: 1,
      id: [36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64,
        67, 78, 79, 80, 81, 82, 83
      ]
    },
    children: [{
      path: 'index',
      component: () => import('@/views/mall/application/index'),
      name: 'index',
      meta: {
        title: '应用',
        icon: 'nested',
        id: [36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 55, 56, 57, 58, 59, 60, 61, 62,
          63, 64, 67, 78, 79, 80, 81, 82, 83
        ]
      },
    }]
  }, {
    path: '/signin',
    name: 'signin',
    component: Layout,
    redirect: '/signin/index',
    hidden: true,
    meta: {
      icon: 'el-sc-qiandao5',
      title: '签到',
      des: '促进访客量、增加用户粘性',
      level: 1,
      isapp: true,
      parents: 'settingShop',
      apptype: 2,
      a_id: 5,
      id: 41
    },
    children: [{
      path: 'index',
      name: 'signinindex',
      component: () => import('@/views/mall/signin/index'),
      meta: {
        icon: 'dashboard',
        title: '签到管理',
        level: 2,
        parents: 'settingShop',
        id: 41
      },
    }]
  }, {
    path: '/activity',
    name: 'activity',
    component: Layout,
    redirect: '/activity/gift',
    hidden: true,
    meta: {
      icon: 'el-sc-xinren',
      title: '新人送礼',
      des: '新人专享好礼 提高转化',
      level: 1,
      isapp: true,
      parents: 'activity',
      apptype: 1,
      a_id: 1,
      id: 37
    },
    children: [{
      path: 'gift',
      name: 'gift',
      component: () => import('@/views/mall/activity/gift'),
      meta: {
        icon: 'dashboard',
        title: '新人送礼',
        level: 2,
        parents: 'activity',
        id: 37
      },
    }, {
      path: 'receive',
      name: 'receive',
      component: () => import('@/views/mall/activity/receive'),
      meta: {
        icon: 'dashboard',
        title: '领取列表',
        level: 2,
        parents: 'activity',
        id: 55
      },
    }, {
      path: 'editgift',
      name: 'editgift',
      component: () => import('@/views/mall/activity/editgift'),
      meta: {
        icon: 'dashboard',
        title: '编辑',
        level: 2,
        parents: 'activity',
        id: 37
      },
      hidden: true,
    }, ]
  }, {
    path: '/ossdetial',
    name: 'ossdetial',
    component: Layout,
    redirect: '/ossdetial/index',
    hidden: true,
    meta: {
      icon: 'el-sc-yuanchengcaokong',
      title: '远程附件',
      des: '本地图片存储到oss',
      level: 1,
      isapp: true,
      parents: 'settingShop',
      apptype: 3,
      a_id: 8,
      id: 45
    },
    children: [{
      path: 'index',
      name: 'ossindex',
      component: () => import('@/views/mall/setting/oss/index'),
      meta: {
        icon: 'dashboard',
        title: '远程附件',
        level: 2,
        parents: 'settingShop',
        id: 45
      },
    }]
  }, {
    path: '/customerservice',
    name: 'customerservice',
    component: Layout,
    redirect: '/customerservice/index',
    hidden: true,
    meta: {
      icon: 'el-sc-kefufuwu',
      title: '腾讯智服',
      des: '多渠道合一打通客服渠道',
      level: 1,
      isapp: true,
      parents: 'settingShop',
      apptype: 3,
      a_id: 9,
      id: 46
    },
    children: [{
      path: 'index',
      name: 'customerserviceindex',
      component: () => import('@/views/mall/setting/customerservice/index'),
      meta: {
        icon: 'dashboard',
        title: '客服设置',
        level: 2,
        parents: 'settingShop',
        id: 46
      },
    }]
  }, {
    path: '/integral',
    name: 'integral',
    component: Layout,
    redirect: '/integral/index',
    hidden: true,
    meta: {
      icon: 'el-sc-jifenguizeguankong2',
      des: '积分使用限制',
      title: '积分规划',
      level: 1,
      isapp: true,
      parents: 'SystemSetting',
      apptype: 2,
      a_id: 6,
      id: 42
    },
    children: [{
      path: 'index',
      name: 'scoreintegral',
      component: () => import('@/views/mall/setting/score/integral'),
      meta: {
        icon: 'dashboard',
        title: '积分规划',
        level: 2,
        parents: 'SystemSetting',
        id: 42
      },
    }]
  }, {
    path: '/deduction',
    name: 'deductionreturn',
    component: Layout,
    redirect: '/deduction/return',
    hidden: true,
    meta: {
      icon: 'el-sc-jifenguizeguankong2',
      des: '积分使用限制',
      title: '消费返积分',
      level: 1,
      isapp: true,
      parents: 'SystemSetting',
      apptype: 2,
      a_id: 15,
      id: 43
    },
    children: [{
      path: 'return',
      name: 'deductionitem',
      component: () => import('@/views/mall/deduction/return'),
      meta: {
        icon: 'dashboard',
        title: '消费返积分',
        level: 2,
        parents: 'SystemSetting',
        id: 43
      },
    }]
  }, {
    path: '/receipt',
    name: 'receipt',
    component: Layout,
    redirect: '/receipt/index',
    hidden: true,
    meta: {
      icon: 'el-sc-xiaopiaodayin',
      title: '小票助手',
      des: '快速打印小票清单',
      level: 1,
      isapp: true,
      parents: 'templatePrint',
      apptype: 3,
      a_id: 10,
      id: 47
    },
    children: [{
      path: 'index',
      name: 'receipt',
      component: () => import('@/views/shop/print/tempalteReceipt'),
      meta: {
        icon: 'dashboard',
        title: '小票',
        level: 2,
        id: 47
      },
      hidden: true
    }, {
      path: 'receiptItem',
      name: 'receiptItem',
      component: () => import('@/views/shop/print/tempalteReceiptItem'),
      meta: {
        icon: 'dashboard',
        title: '模板操作',
        level: 3,
        parents: 'templatePrint',
        activeMenu: '/print/template/receipt',
        id: 47
      },
      hidden: true
    }, {
      path: 'receiptPrinter',
      name: 'receiptPrinter',
      component: () => import('@/views/shop/print/receiptPrinter'),
      meta: {
        icon: 'dashboard',
        title: '小票打印机',
        level: 3,
        parents: 'templatePrint',
        activeMenu: '/print/template/receipt',
        id: 47
      },
      hidden: true
    }, {
      path: 'receiptTask',
      name: 'receiptTask',
      component: () => import('@/views/shop/print/receiptTask'),
      meta: {
        icon: 'dashboard',
        title: '打印计划',
        level: 3,
        parents: 'templatePrint',
        activeMenu: '/print/template/receipt',
        id: 47
      },
      hidden: true
    }, ]
  }, {
    path: '/store',
    component: Layout,
    name: 'store',
    redirect: '/store/storeList',
    hidden: true,
    meta: {
      title: '门店',
      icon: 'el-sc-mendian4',
      des: '到店自提附近门店选择',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 11,
      id: 48
    },
    children: [{
      path: 'storeList',
      name: 'storeList',
      component: () => import('@/views/shop/store/store'),
      meta: {
        title: '门店列表',
        icon: 'dashboard',
        parents: 'store',
        level: 2,
        id: 48
      }
    }, {
      path: 'storeItem',
      name: 'storeItem',
      component: () => import('@/views/shop/store/store_item'),
      meta: {
        title: '门店操作',
        icon: 'dashboard',
        parents: 'store',
        level: 2,
        id: 48
      },
      hidden: true
    }, {
      path: 'employeeList',
      name: 'employeeList',
      component: () => import('@/views/shop/store/employee'),
      meta: {
        title: '店员核销',
        icon: 'dashboard',
        parents: 'store',
        level: 2,
        id: 48
      }
    }, ]
  }, {
    path: '/merchant',
    component: Layout,
    name: 'merchant',
    redirect: '/merchant/list',
    hidden: true,
    meta: {
      title: '商户',
      des: '商家入驻平台',
      icon: 'el-sc-shanghu2',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 12,
      id: [49, 50]
    },
    children: [{
      path: 'list',
      name: 'merchantList',
      component: () => import('@/views/mall/merchant/list'),
      meta: {
        icon: 'dashboard',
        title: '商户列表',
        level: 2,
        parents: 'merchant',
        id: 49
      }
    }, {
      path: 'item',
      name: 'merchantItem',
      component: () => import('@/views/mall/merchant/item'),
      meta: {
        icon: 'dashboard',
        title: '商户操作',
        level: 2,
        parents: 'merchant',
        activeMenu: '/merchant/list',
        id: 49
      },
      hidden: true
    }, {
      path: 'apply',
      name: 'merchantApply',
      component: () => import('@/views/mall/merchant/apply'),
      meta: {
        icon: 'dashboard',
        title: '申请列表',
        level: 2,
        parents: 'merchant',
        id: 50
      }
    }, ]
  }, {
    path: '/groupon',
    component: Layout,
    name: 'groupon',
    redirect: '/groupon/list',
    hidden: true,
    meta: {
      title: '限时拼团',
      des: '多种拼团,提升用户转化率',
      icon: 'el-sc-pintuan4',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 24,
      id: 78
    },
    children: [{
      path: 'list',
      name: 'grouponList',
      component: () => import('@/views/mall/groupon/index'),
      meta: {
        icon: 'dashboard',
        title: '拼团列表',
        level: 2,
        parents: 'groupon',
        id: 78
      }
    }, {
      path: 'order',
      name: 'grouponList',
      component: () => import('@/views/mall/groupon/order'),
      meta: {
        icon: 'dashboard',
        title: '拼团订单',
        level: 2,
        parents: 'groupon',
        id: 78
      }
    }, {
      path: 'setting',
      name: 'grouponsetting',
      component: () => import('@/views/mall/setting/group/group'),
      meta: {
        icon: 'dashboard',
        title: '拼团设置',
        level: 2,
        parents: 'groupon',
        id: 78
      }
    }, {
      path: 'item',
      name: 'grouponItem',
      component: () => import('@/views/mall/groupon/item'),
      meta: {
        icon: 'dashboard',
        title: '拼团操作',
        level: 2,
        parents: 'groupon',
        activeMenu: '/groupon/list',
        id: 78
      },
      hidden: true
    }, {
      path: 'detail',
      name: 'grouponItem',
      component: () => import('@/views/mall/groupon/detail'),
      meta: {
        icon: 'dashboard',
        title: '订单详情',
        level: 2,
        parents: 'groupon',
        activeMenu: '/groupon/list',
        id: 78
      },
      hidden: true
    }, ]
  }, {
    path: '/seckill',
    component: Layout,
    name: 'seckill',
    redirect: '/seckill/list',
    hidden: true,
    meta: {
      title: '限时秒杀',
      des: '低价、限购，吸引更多用户',
      icon: 'el-sc-tsm_0076',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 25,
      id: 79
    },
    children: [{
      path: 'list',
      name: 'seckillList',
      component: () => import('@/views/mall/seckill/index'),
      meta: {
        icon: 'dashboard',
        title: '秒杀列表',
        level: 2,
        parents: 'seckill',
        id: 79
      }
    }, {
      path: 'setting',
      name: 'seckillsetting',
      component: () => import('@/views/mall/setting/seckill/seckill'),
      meta: {
        icon: 'dashboard',
        title: '秒杀设置',
        level: 2,
        parents: 'seckill',
        id: 79
      }
    }, {
      path: 'item',
      name: 'seckillItem',
      component: () => import('@/views/mall/seckill/item'),
      meta: {
        icon: 'dashboard',
        title: '秒杀操作',
        level: 2,
        parents: 'seckill',
        activeMenu: '/seckill/list',
        id: 79
      },
      hidden: true
    }, {
      path: 'detail',
      name: 'seckillItem',
      component: () => import('@/views/mall/seckill/detail'),
      meta: {
        icon: 'dashboard',
        title: '订单详情',
        level: 2,
        parents: 'seckill',
        activeMenu: '/seckill/list',
        id: 79
      },
      hidden: true
    }, ]
  }, {
    path: '/presell',
    component: Layout,
    name: 'presell',
    redirect: '/presell/list',
    hidden: true,
    meta: {
      title: '商品预售',
      des: '有效规避生产存在风险',
      icon: 'el-sc-tsm_0076',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 34,
      id: 84
    },
    children: [{
      path: 'list',
      name: 'presellList',
      component: () => import('@/views/mall/presell/index'),
      meta: {
        icon: 'dashboard',
        title: '活动列表',
        level: 2,
        parents: 'presell',
        id: 84
      }
    }, {
      path: 'item',
      name: 'presellItem',
      component: () => import('@/views/mall/presell/item'),
      meta: {
        icon: 'dashboard',
        title: '活动操作',
        level: 2,
        parents: 'presell',
        activeMenu: '/presell/list',
        id: 84
      },
      hidden: true
    }]
  }, {
    path: '/assettransfer',
    component: Layout,
    name: 'assettransfer',
    redirect: '/assettransfer/index',
    hidden: true,
    meta: {
      title: '资产转赠',
      des: '提升用户互动和拉新',
      icon: 'el-sc-tsm_0076',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 35,
      id: 86
    },
    children: [{
      path: 'index',
      name: 'assettransferList',
      component: () => import('@/views/mall/assettransfer/index'),
      meta: {
        icon: 'dashboard',
        title: '转赠设置',
        level: 2,
        parents: 'assettransfer',
        id: 86
      }
    }, {
      path: 'list',
      name: 'assettransferItem',
      component: () => import('@/views/mall/assettransfer/list'),
      meta: {
        icon: 'dashboard',
        title: '转赠记录',
        level: 2,
        parents: 'assettransfer',
        id: 86
      },
    }, ]
  },
  {
    path: '/recharge',
    component: Layout,
    name: 'recharge',
    redirect: '/recharge/index',
    hidden: true,
    meta: {
      title: '充值奖励',
      des: '提升用户充值率',
      icon: 'el-sc-tsm_0076',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 36,
      id: 87
    },
    children: [{
      path: 'index',
      name: 'rechargeList',
      component: () => import('@/views/mall/recharge/index'),
      meta: {
        icon: 'dashboard',
        title: '充值奖励',
        level: 2,
        parents: 'recharge',
        id: 87
      }
    }, {
      path: 'item',
      name: 'rechargeItem',
      component: () => import('@/views/mall/recharge/item'),
      meta: {
        icon: 'dashboard',
        title: '编辑充值奖励',
        level: 2,
        parents: 'recharge',
        activeMenu: '/recharge/index',
        id: 87
      },
       hidden: true
    },

    ]
  },

  {
    path: '/liver',
    component: Layout,
    name: 'liver',
    redirect: '/liver/list',
    hidden: true,
    meta: {
      title: '小程序直播',
      des: '引导消费形成销售转化',
      icon: 'el-sc-zhibo1',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 27,
      id: 75
    },
    children: [{
      path: 'list',
      name: 'liverList',
      component: () => import('@/views/mall/liver/index'),
      meta: {
        icon: 'dashboard',
        title: '直播列表',
        level: 2,
        parents: 'liver',
        id: 75
      }
    }, {
      path: 'item',
      name: 'liverItem',
      component: () => import('@/views/mall/liver/item'),
      meta: {
        icon: 'dashboard',
        title: '直播操作',
        level: 2,
        parents: 'liver',
        activeMenu: '/liver/list',
        id: 75
      },
      hidden: true
    }, {
      path: 'operate',
      name: 'liverItem',
      component: () => import('@/views/mall/liver/operate'),
      meta: {
        icon: 'dashboard',
        title: '直播操作',
        level: 2,
        parents: 'liver',
        activeMenu: '/liver/list',
        id: 75
      },
      hidden: true
    }, {
      path: 'product',
      name: 'liverList',
      component: layoutEmpty,
      meta: {
        icon: 'dashboard',
        title: '商品列表',
        level: 2,
        parents: 'liver',
        id: 75
      },
      children: [{
        path: 'list',
        name: 'liverList',
        component: () => import('@/views/mall/liver/product'),
        meta: {
          icon: 'dashboard',
          title: '全部商品',
          level: 2,
          parents: 'liver',
          id: 75
        },
      }, {
        path: 'auditlist',
        name: 'liverList',
        component: () => import('@/views/mall/liver/auditlist'),
        meta: {
          icon: 'dashboard',
          title: '已审核商品',
          level: 2,
          parents: 'liver',
          id: 75
        },
      }]
    }, ]
  }, {
    path: '/scrapy',
    component: Layout,
    name: 'scrapy',
    redirect: '/scrapy/list',
    hidden: true,
    meta: {
      title: '采集助手',
      icon: 'el-sc-caiji',
      des: '一键采集 快速铺货',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 26,
      id: 74
    },
    children: [{
      path: 'setting',
      name: 'scrapysetting',
      component: () => import('@/views/mall/scrapy/setting'),
      meta: {
        icon: 'dashboard',
        title: '基础设置',
        level: 2,
        parents: 'scrapy',
        id: 74
      }
    }, {
      path: 'list',
      name: 'scrapyList',
      component: () => import('@/views/mall/scrapy/list'),
      meta: {
        icon: 'dashboard',
        title: '采集列表',
        level: 2,
        parents: 'scrapy',
        id: 74
      }
    }, {
      path: 'item',
      name: 'scrapyItem',
      component: () => import('@/views/mall/scrapy/item'),
      meta: {
        icon: 'dashboard',
        title: '采集商品',
        level: 2,
        parents: 'scrapy',
        id: 74
      },
    }, ]
  }, {
    path: '/quick',
    component: Layout,
    name: 'quick',
    redirect: '/quick/register',
    hidden: true,
    meta: {
      title: '快速注册小程序',
      icon: 'el-sc-xiaochengxu',
      des: '小程序注册免300认证',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 28,
      id: 76
    },
    children: [{
      path: 'setting',
      name: 'quicksetting',
      component: () => import('@/views/mall/quick/setting'),
      meta: {
        icon: 'dashboard',
        title: '基础设置',
        level: 2,
        parents: 'quick',
        id: 76
      }
    }, {
      path: 'register',
      name: 'quickregister',
      component: () => import('@/views/mall/quick/register'),
      meta: {
        icon: 'dashboard',
        title: '快速注册小程序',
        level: 2,
        parents: 'quick',
        id: 76
      },
    }, ]
  }, {
    path: '/withholdPassword',
    component: Layout,
    name: 'withholdPassword',
    redirect: '/withholdPassword/setting',
    hidden: true,
    meta: {
      title: '截留口令',
      icon: 'el-sc-xiaochengxu',
      des: '第三方进行倒流',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 33,
      id: 85
    },
    children: [{
      path: 'setting',
      name: 'quicksetting',
      component: () => import('@/views/mall/setting/withholdPassword'),
      meta: {
        icon: 'dashboard',
        title: '截留口令',
        level: 2,
        parents: 'quick',
        id: 85
      }
    }, ]
  }, {
    path: '/article',
    component: Layout,
    name: 'article',
    redirect: '/article/list',
    hidden: true,
    meta: {
      title: '文章',
      icon: 'el-sc-wenzhangzhongxin',
      des: '丰富内容信息',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 2,
      id: 38
    },
    children: [{
      path: 'list',
      name: 'articleList',
      component: () => import('@/views/mall/article/listArticle'),
      meta: {
        icon: 'dashboard',
        title: '文章列表',
        level: 2,
        parents: 'article',
        id: 38
      }
    }, {
      path: 'item',
      name: 'articleItem',
      component: () => import('@/views/mall/article/itemArticle'),
      meta: {
        icon: 'dashboard',
        title: '文章操作',
        level: 2,
        parents: 'article',
        id: 38
      },
      hidden: true
    }, {
      path: 'category',
      name: 'articleCategory',
      component: () => import('@/views/mall/article/listCategory'),
      meta: {
        icon: 'dashboard',
        title: '文章分类',
        level: 2,
        parents: 'article',
        id: 38
      }
    }]
  }, {
    path: '/customform',
    component: Layout,
    name: 'customform',
    redirect: '/customform/list',
    hidden: true,
    meta: {
      title: '自定义表单',
      icon: 'el-sc-17',
      des: ' 页面自由编辑，信息快速获取',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 13,
      id: 51
    },
    children: [{
      path: 'list',
      name: 'customformList',
      component: () => import('@/views/mall/customForm/index'),
      meta: {
        icon: 'dashboard',
        title: '自定义表单列表',
        level: 2,
        parents: 'customform',
        id: 51
      }
    }, {
      path: 'item',
      name: 'customformItem',
      component: () => import('@/views/mall/customForm/edit'),
      meta: {
        icon: 'dashboard',
        title: '自定义表单操作',
        level: 2,
        parents: 'customform',
        id: 51
      },
      hidden: true
    }, {
      path: 'answer',
      name: 'customformItem',
      component: () => import('@/views/mall/customForm/answer'),
      meta: {
        icon: 'dashboard',
        title: '表单提交列表',
        level: 2,
        parents: 'customform',
        id: 51
      },
      hidden: true
    }, ]
  }, {
    path: '/systemform',
    component: Layout,
    name: 'systemform',
    redirect: '/systemform/list',
    hidden: true,
    meta: {
      title: '系统表单',
      icon: 'el-sc-17',
      des: ' 多场景收集信息',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 23,
      id: 71
    },
    children: [{
      path: 'list',
      name: 'systemformList',
      component: () => import('@/views/mall/systemform/index'),
      meta: {
        icon: 'dashboard',
        title: '系统表单列表',
        level: 2,
        parents: 'customform',
        id: 71
      }
    }, {
      path: 'item',
      name: 'systemformItem',
      component: () => import('@/views/mall/systemform/edit'),
      meta: {
        icon: 'dashboard',
        title: '系统表单操作',
        level: 2,
        parents: 'systemform',
        id: 71
      },
      hidden: true
    }, {
      path: 'answer',
      name: 'systemformItem',
      component: () => import('@/views/mall/systemform/answer'),
      meta: {
        icon: 'dashboard',
        title: '表单提交列表',
        level: 2,
        parents: 'systemform',
        id: 71
      },
      hidden: true
    }, ]
  }, {
    path: '/deduction',
    name: 'deduction',
    component: Layout,
    redirect: '/deduction/index',
    hidden: true,
    meta: {
      icon: 'el-sc-mian',
      title: '抵扣设置',
      isapp: true,
      apptype: 2,
      des: '积分抵扣使用比例',
      a_id: 7,
      id: 44
    },
    children: [{
      path: 'index',
      name: 'deductionindex',
      component: () => import('@/views/mall/deduction/index'),
      meta: {
        icon: 'dashboard',
        title: '抵扣设置',
        id: 44
      },
    }]
  }, {
    path: '/coupon',
    name: 'coupon',
    redirect: '/coupon/list',
    hidden: true,
    component: Layout,
    meta: {
      icon: 'el-sc-Vector-10',
      title: '优惠券',
      des: '向用户发放商品优惠卷',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 3,
      id: 39
    },
    children: [{
      path: 'list',
      name: 'couponList',
      component: () => import('@/views/shop/coupon/list'),
      meta: {
        icon: 'dashboard',
        title: '全部优惠券',
        level: 2,
        parents: 'coupon',
        id: 39
      }
    }, {
      path: 'item',
      name: 'couponItem',
      component: () => import('@/views/shop/coupon/item'),
      meta: {
        icon: 'dashboard',
        title: '操作优惠券',
        level: 2,
        parents: 'coupon',
        activeMenu: '/marketing/coupon/list',
        id: 39
      },
      hidden: true
    }, {
      path: 'record',
      name: 'recordList',
      component: () => import('@/views/shop/coupon/records'),
      meta: {
        icon: 'dashboard',
        title: '发券记录',
        level: 2,
        parents: 'coupon',
        id: 39
      },
      hidden: true
    }, {
      path: 'setting',
      name: 'settingCoupon',
      component: () => import('@/views/shop/coupon/setting'),
      meta: {
        icon: 'dashboard',
        title: '其他设置',
        level: 2,
        parents: 'coupon',
        id: 39
      }
    }, {
      path: 'examine',
      name: 'examineCoupon',
      component: () => import('@/views/shop/coupon/examine'),
      meta: {
        icon: 'dashboard',
        title: '优惠券审核',
        level: 2,
        parents: 'coupon',
        id: 39
      }
    }]
  }, {
    path: '/print',
    component: Layout,
    name: 'print',
    redirect: '/print/list',
    hidden: true,
    meta: {
      title: '快递助手',
      icon: 'el-sc-kuaidi1',
      des: '面单打印提升发货效率',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 14,
      id: 52
    },
    children: [{
      path: 'list',
      name: 'printOrderList',
      component: () => import('@/views/shop/print/printOrderList'),
      meta: {
        icon: 'dashboard',
        title: '打印列表',
        level: 2,
        parents: 'print',
        id: 52
      }
    }, {
      path: 'template',
      name: 'templatePrint',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        icon: 'dashboard',
        title: '模板管理',
        level: 2,
        parents: 'print',
        id: 52
      },
      children: [{
        path: 'electronics',
        name: 'electronics',
        component: () => import('@/views/shop/print/tempalteElectronics'),
        meta: {
          icon: 'dashboard',
          title: '电子面单',
          level: 3,
          parents: 'templatePrint',
          id: 52
        }
      }, {
        path: 'express_bill',
        name: 'express_bill',
        component: () => import('@/views/shop/print/express_bill'),
        meta: {
          icon: 'dashboard',
          title: '电子面单设置',
          level: 3,
          parents: 'templatePrint',
          id: 52
        }
      }, {
        path: 'electronicsItem',
        name: 'electronicsItem',
        component: () => import('@/views/shop/print/tempalteElectronicsItem'),
        meta: {
          icon: 'dashboard',
          title: '模板操作',
          level: 3,
          parents: 'templatePrint',
          activeMenu: '/print/template/electronics',
          id: 52
        },
        hidden: true
      }, {
        path: 'invoice',
        name: 'invoice',
        component: () => import('@/views/shop/print/tempalteInvoice'),
        meta: {
          icon: 'dashboard',
          title: '发货单',
          level: 3,
          parents: 'templatePrint',
          id: 52
        },
        // hidden: true
      }, {
        path: 'editInvoice',
        name: 'editInvoice',
        component: () => import('@/views/shop/print/tempalteInvoiceItem'),
        meta: {
          icon: 'dashboard',
          title: '模板操作',
          level: 3,
          parents: 'templatePrint',
          activeMenu: '/print/template/electronics',
          id: 52
        },
        hidden: true
      }, ]
    }]
  }, {
    path: '/vipcenter',
    component: Layout,
    name: 'vipcenter',
    redirect: '/vipcenter/list',
    hidden: true,
    meta: {
      title: '会员中心',
      icon: 'el-sc-huiyuan5',
      des: '会员中心',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 20,
      id: 64
    },
    children: [{
      path: 'list',
      name: 'vipCenterList',
      component: () => import('@/views/mall/vipcenter/list'),
      meta: {
        icon: 'dashboard',
        title: '会员列表',
        level: 2,
        parents: 'vipcenter',
        id: 64
      }
    }, {
      path: 'index',
      name: 'vipCenterIndex',
      component: () => import('@/views/mall/vipcenter/index'),
      meta: {
        icon: 'dashboard',
        title: '会员中心',
        level: 2,
        parents: 'vipcenter',
        id: 64
      }
    }, {
      path: 'vipuser',
      name: 'settingShopVipUser',
      component: () => import('@/views/mall/setting/vipuser'),
      meta: {
        icon: 'dashboard',
        title: '会员中心设置',
        level: 3,
        id: 64,
        parents: 'vipcenter',
      },
    }, {
      path: 'rights',
      name: 'vipCenterRights',
      component: () => import('@/views/mall/vipcenter/rights'),
      meta: {
        icon: 'dashboard',
        title: '权益列表',
        level: 2,
        parents: 'vipcenter',
        id: 64
      }
    }, {
      path: 'openlist',
      name: 'vipCenterOpenList',
      component: () => import('@/views/mall/vipcenter/openlist'),
      meta: {
        icon: 'dashboard',
        title: '开通记录',
        level: 2,
        parents: 'vipcenter',
        id: 64
      }
    }, ]
  }, {
    path: '/guesslike',
    component: Layout,
    name: 'guesslike',
    redirect: '/guesslike/index',
    hidden: true,
    meta: {
      title: '猜你喜欢',
      icon: 'el-sc-cainixihuan1',
      des: '自定义推荐猜你喜欢',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 21,
      id: 63
    },
    children: [{
      path: 'index',
      name: 'guesslikeList',
      component: () => import('@/views/mall/guesslike/index'),
      meta: {
        icon: 'dashboard',
        title: '猜你喜欢',
        level: 2,
        parents: 'print',
        id: 63
      }
    }]
  }, {
    path: '/notice',
    component: Layout,
    redirect: '/notice/buyer/base',
    name: 'notice',
    hidden: true,
    meta: {
      title: '消息通知',
      icon: 'el-sc-xiaoxihui',
      des: '消息订阅推送配置',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 4,
      id: 40
    },
    children: [{
      path: 'buyer',
      name: 'noticeBuyer',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        icon: 'dashboard',
        title: '买家通知',
        level: 2,
        parents: 'notice',
        id: 40
      },
      children: [{
        path: 'base',
        name: 'msgBaseList',
        component: () => import('@/views/mall/notice/noticeList'),
        meta: {
          icon: 'dashboard',
          title: '基础通知',
          level: 3,
          parents: 'noticeBuyer',
          id: 40
        }
      }, {
        path: 'market',
        name: 'msgMarketList',
        component: () => import('@/views/mall/notice/noticeList'),
        meta: {
          icon: 'dashboard',
          title: '营销通知',
          level: 3,
          parents: 'noticeBuyer',
          id: 40
        }
      }, {
        path: 'vcode',
        name: 'msgVcodeList',
        component: () => import('@/views/mall/notice/noticeList'),
        meta: {
          icon: 'dashboard',
          title: '验证码',
          level: 3,
          parents: 'noticeBuyer',
          id: 40
        }
      }]
    }, {
      path: 'seller',
      name: 'noticeSeller',
      component: () => import('@/views/mall/notice/noticeList'),
      meta: {
        icon: 'dashboard',
        title: '卖家通知',
        level: 2,
        parents: 'notice',
        id: 40
      }
    }, {
      path: 'sms',
      name: 'sms',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        title: '短信设置',
        icon: 'nested',
        level: 2,
        id: 40
      },
      children: [{
        path: 'template',
        name: 'smsTemplate',
        component: () => import('@/views/mall/sms/smsTemplate'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '短信模板',
          level: 3,
          parents: 'sms',
          id: 40
        }
      }, {
        path: 'smsItem',
        name: 'smsItem',
        component: () => import('@/views/mall/sms/smsItem'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '设置模板',
          level: 3,
          parents: 'sms',
          activeMenu: '/notice/sms/template',
          id: 40
        },
        hidden: true
      }]
    }, {
      path: 'wx',
      name: 'wx',
      redirect: 'noRedirect',
      component: layoutEmpty,
      meta: {
        title: '微信设置',
        icon: 'nested',
        level: 2,
        id: 40
      },
      children: [{
        path: 'template',
        name: 'wxTemplate',
        component: () => import('@/views/mall/sms/wxTemplate'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '微信模板',
          level: 3,
          parents: 'wx',
          id: 40
        }
      }, {
        path: 'wxItem',
        name: 'wxItem',
        component: () => import('@/views/mall/sms/wxItem'),
        meta: {
          icon: 'el-icon-chat-dot-round',
          title: '设置模板',
          level: 3,
          parents: 'wx',
          activeMenu: '/notice/wx/template',
          id: 40
        },
        hidden: true
      }]
    }, {
      path: 'setting',
      name: 'smsSetting',
      component: () => import('@/views/mall/sms/smsSetting'),
      meta: {
        icon: 'el-icon-setting',
        title: '接口设置',
        level: 2,
        parents: 'notice',
        id: 40
      }
    }, {
      path: 'noticeItem',
      name: 'noticeItem',
      component: () => import('@/views/mall/notice/noticeItem'),
      meta: {
        icon: 'dashboard',
        title: '编辑配置',
        level: 2,
        parents: 'notice',
        id: 40
      },
      hidden: true
    }]
  }, {
    path: '/grid',
    component: Layout,
    name: 'grid',
    redirect: '/grid/list',
    hidden: true,
    meta: {
      title: '九宫格抽奖',
      icon: 'el-sc-iconfontzhizuobiaozhun0247',
      des: '发布九宫格抽奖系统',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 17,
      id: 59
    },
    children: [{
      path: 'list',
      name: 'gridList',
      component: () => import('@/views/shop/lottery/grid/list'),
      meta: {
        icon: 'dashboard',
        title: '抽奖列表',
        level: 2,
        parents: 'grid',
        id: 59
      }
    }, {
      path: 'item',
      name: 'gridItem',
      component: () => import('@/views/shop/lottery/grid/item'),
      meta: {
        icon: 'dashboard',
        title: '新增抽奖',
        level: 2,
        parents: 'grid',
        id: 59
      },
      hidden: true
    }, {
      path: 'winning',
      name: 'winningList',
      component: () => import('@/views/shop/lottery/winning'),
      props: {
        win_style: 1
      },
      meta: {
        icon: 'dashboard',
        title: '中奖列表',
        level: 2,
        parents: 'grid',
        id: 59
      }
    }]
  }, {
    path: '/scratch',
    component: Layout,
    name: 'scratch',
    redirect: '/scratch/list',
    hidden: true,
    meta: {
      title: '刮刮乐抽奖',
      icon: 'el-sc-a-ziyuan1280',
      des: '发布刮刮乐抽奖系统',
      level: 1,
      isapp: true,
      apptype: 1,
      a_id: 18,
      id: 60
    },
    children: [{
      path: 'list',
      name: 'scratchList',
      component: () => import('@/views/shop/lottery/scratch/list'),
      meta: {
        icon: 'dashboard',
        title: '抽奖列表',
        level: 2,
        parents: 'scratch',
        id: 60
      }
    }, {
      path: 'item',
      name: 'scratchItem',
      component: () => import('@/views/shop/lottery/scratch/item'),
      meta: {
        icon: 'dashboard',
        title: '新增抽奖',
        level: 2,
        parents: 'grid',
        id: 60
      },
      hidden: true
    }, {
      path: 'winning',
      name: 'scratchwinning',
      component: () => import('@/views/shop/lottery/winning'),
      props: {
        win_style: 2
      },
      meta: {
        icon: 'dashboard',
        title: '中奖列表',
        level: 2,
        parents: 'grid',
        id: 60
      }
    }]
  }, {
    path: '/distribution',
    component: Layout,
    name: 'distribution',
    redirect: '/distribution/list',
    hidden: true,
    meta: {
      title: '同城配送',
      icon: 'el-sc-kuaidi1',
      des: ' 支持商家配送和第三方配送',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 16,
      id: 62
    },
    children: [{
      path: 'list',
      name: 'distributionList',
      component: () => import('@/views/shop/distribution/list'),
      meta: {
        icon: 'dashboard',
        title: '同城配送',
        level: 2,
        parents: 'distribution',
        id: 62
      }
    }, {
      path: 'item',
      name: 'distributionItem',
      component: () => import('@/views/shop/distribution/item'),
      meta: {
        icon: 'dashboard',
        title: '同城配送',
        level: 2,
        parents: 'distribution',
        id: 62
      },
      hidden: true
    }]
  }, {
    path: '/customer',
    component: Layout,
    name: 'customer',
    redirect: '/customer/list',
    hidden: true,
    meta: {
      title: '微信客服',
      icon: 'el-sc-kefudianhua_weixinkefu',
      des: ' 打通企业微信，形成私域闭环',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 22,
      id: 67
    },
    children: [{
      path: 'list',
      name: 'customerList',
      component: () => import('@/views/mall/customer/index'),
      meta: {
        icon: 'dashboard',
        title: '微信客服',
        level: 2,
        parents: 'distribution',
        id: 67
      }
    }]
  }, {
    path: '/integralgoods',
    component: Layout,
    name: 'integralgoods',
    redirect: '/integralgoods/goodslist',
    hidden: true,
    meta: {
      title: '积分商城',
      icon: 'el-sc-jifenshangcheng',
      des: '积分购买商品，形成用户黏性',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 19,
      id: 61
    },
    children: [{
      path: 'classifylist',
      name: 'classifylist',
      component: () => import('@/views/mall/integralgoods/classify/index'),
      meta: {
        icon: 'dashboard',
        title: '分类列表',
        level: 2,
        parents: 'integralgoods',
        id: 61
      }
    }, {
      path: 'goodslist',
      name: 'goodslist',
      component: () => import('@/views/mall/integralgoods/goods/index'),
      meta: {
        icon: 'dashboard',
        title: '商品列表',
        level: 2,
        parents: 'integralgoods',
        id: 61
      }
    }, {
      path: 'goodsitem',
      name: 'goodsitem',
      component: () => import('@/views/mall/integralgoods/goods/item'),
      meta: {
        icon: 'dashboard',
        title: '商品详情',
        level: 2,
        parents: 'integralgoods',
        id: 61
      },
      hidden: true
    }, {
      path: 'orderlist',
      name: 'orderlist',
      component: () => import('@/views/mall/integralgoods/order/index'),
      meta: {
        icon: 'dashboard',
        title: '订单列表',
        level: 2,
        parents: 'integralgoods',
        id: 61
      }
    }, {
      path: 'orderitem',
      name: 'orderitem',
      component: () => import('@/views/mall/integralgoods/order/item'),
      meta: {
        icon: 'dashboard',
        title: '订单详情',
        level: 2,
        parents: 'integralgoods',
        id: 61
      },
      hidden: true
    }, ]
  }, {
    path: '/activitys',
    component: Layout,
    name: 'activity',
    redirect: '/activitys/list',
    hidden: true,
    meta: {
      title: '分销达标奖',
      icon: 'el-sc-a-ziyuan1280',
      des: '差异化模式，阶梯式达标奖励',
      level: 1,
      isapp: true,
      apptype: 2,
      a_id: 30,
      id: 80
    },
    children: [{
      path: 'list',
      name: 'activityList',
      component: () => import('@/views/mall/commission/agent/list'),
      meta: {
        icon: 'dashboard',
        title: '活动列表',
        level: 2,
        parents: 'activity',
        id: 80
      }
    }, {
      path: 'item',
      name: 'activityItem',
      component: () => import('@/views/mall/commission/agent/item'),
      meta: {
        icon: 'dashboard',
        title: '新增活动',
        level: 2,
        parents: 'activity',
        id: 80
      },
      hidden: true
    }, {
      path: 'award',
      name: 'awardList',
      component: () => import('@/views/mall/commission/agent/award'),
      meta: {
        icon: 'dashboard',
        title: '奖励记录',
        level: 2,
        parents: 'activity',
        id: 80
      }
    }, ]
  }, {
    path: '/assessment',
    component: Layout,
    name: 'assessment',
    redirect: '/assessment/index',
    hidden: true,
    meta: {
      title: '分销商业绩考核',
      icon: 'el-sc-a-ziyuan1280',
      des: '考核等级升降模式，分销激励玩法',
      level: 1,
      isapp: true,
      apptype: 2,
      a_id: 31,
      id: [81, 82]
    },
    children: [{
      path: 'index',
      name: 'assessmentList',
      component: () => import('@/views/mall/commission/assessment/index'),
      meta: {
        icon: 'dashboard',
        title: '统计数据',
        level: 2,
        parents: 'assessment',
        id: 81
      }
    }, {
      path: 'list',
      name: 'assessmentList',
      component: () => import('@/views/mall/commission/assessment/list'),
      meta: {
        icon: 'dashboard',
        title: '考核管理',
        level: 2,
        parents: 'assessment',
        id: 81
      }
    }, {
      path: 'order',
      name: 'assessmentorder',
      component: () => import('@/views/mall/commission/assessment/order'),
      meta: {
        icon: 'dashboard',
        title: '订单记录',
        level: 2,
        parents: 'assessment',
        id: 81
      }
    }, {
      path: 'degrade',
      name: 'assessmentdegrade',
      component: () => import('@/views/mall/commission/assessment/degrade'),
      meta: {
        icon: 'dashboard',
        title: '降级记录',
        level: 2,
        parents: 'assessment',
        id: 81
      }
    }, {
      path: 'withdrawal',
      name: 'assessmentwithdrawal',
      component: () => import('@/views/mall/commission/assessment/withdrawal'),
      meta: {
        icon: 'dashboard',
        title: '提现审核',
        level: 2,
        parents: 'assessment',
        id: 82
      }
    }, {
      path: 'item',
      name: 'assessmentItem',
      component: () => import('@/views/mall/commission/assessment/item'),
      meta: {
        icon: 'dashboard',
        title: '新增规则',
        level: 2,
        parents: 'assessment',
        id: 81
      },
      hidden: true
    }, ]
  }, {
    path: '/wxurllink',
    component: Layout,
    name: 'wxurllink',
    redirect: '/wxurllink/list',
    hidden: true,
    meta: {
      title: '小程序外部跳转',
      icon: 'el-sc-a-ziyuan1280',
      des: '通过H5链接跳转小程序，高效引流',
      level: 1,
      isapp: true,
      apptype: 3,
      a_id: 32,
      id: 83
    },
    children: [{
      path: 'list',
      name: 'wxurllinkList',
      component: () => import('@/views/mall/wxurllink/list'),
      meta: {
        icon: 'dashboard',
        title: '链接管理',
        level: 2,
        parents: 'wxurllink',
        id: 83
      }
    }, {
      path: 'item',
      name: 'wxurllinkItem',
      component: () => import('@/views/mall/wxurllink/item'),
      meta: {
        icon: 'dashboard',
        title: '新增链接',
        level: 2,
        parents: 'wxurllink',
        id: 83
      },
      hidden: true
    }, ]
  },
  // 404 page must be placed at the end !!!
];
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})
const router = createRouter()
// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
