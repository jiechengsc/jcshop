import Vue from 'vue'
import VueElementUISkeleton from 'vue-elementui-skeleton'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
// import 'element-ui/lib/theme-chalk/index.css'
// import locale from 'element-ui/lib/locale/lang/en' // lang i18n

import '../../styles/element-variables.scss'
import '@/styles/index.scss' // global css
// import  ''
// import "@/assets/icon/iconfont.css";  //全局字体图标
// import 'https://at.alicdn.com/t/font_2040199_rbpw9cheay.css'
//import AmapVue from '@amap/amap-vue'; //map

import App from './mall.vue'
import store from './store'
import router from './router'

import '@/icons' // icon
import './permission' // permission control

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// if (process.env.NODE_ENV === 'production') {
//   const { mockXHR } = require('../mock')
//   mockXHR()
// }

import VueClipboard from 'vue-clipboard2'
Vue.use(VueClipboard)


//引入并挂载momont
import moment from 'moment';
// moment.lang('zh-cn');
Vue.prototype.$moment = moment;
import {jur_addons} from '@/utils/jurisdiction'
Vue.prototype.$jur_addons=jur_addons

Vue.prototype.isLoading = false
Vue.prototype.$edition_type = 'single';
// set ElementUI lang to EN
// Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
Vue.use(VueElementUISkeleton)
Vue.use(ElementUI)
import has from '@/utils/btnPermissions.js';
import entry from '@/utils/entry.js';
Vue.prototype.globalClick = function (callback) {
    document.getElementById('app').onclick = function () {
        callback();
    };
};
import vimage from '@/components/vimage/index.vue';
Vue.component("vimage", vimage);
import ufrom from '@/components/Ufrom/index.vue';
Vue.component("ufrom", ufrom);

// AmapVue.config.version = '2.0'; // 默认2.0，这里可以不修改
// // AmapVue.config.key = '55303a123a32fe178eab98ead425b132';
//AmapVue.config.key = '20121b013150acd202060edc3371780d';
// AmapVue.config.plugins = [
//   'AMap.ToolBar',
//   'AMap.MoveAnimation',
//   // 'AMap.PlaceSearch',
//   // 'AMap.AutoComplete',
//   // 在此配置你需要预加载的插件，如果不配置，在使用到的时候会自动异步加载
// ];
//Vue.use(AmapVue);

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
