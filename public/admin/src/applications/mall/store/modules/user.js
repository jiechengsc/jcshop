import {
  mall_login,
  mall_logout,
  shop_login
} from '@/api/common'
import {
  getToken,
  setToken,
  setShopToken,
  setsToken,
  removeToken,
  removeShopToken,
  getLoginType,
  setSaasToken,
  setUserToken
} from '@/utils/auth'
import {
  resetRouter
} from '@/applications/mall/router'

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    ruleList: [],
    license: false
  }
}
const state = getDefaultState()
const ruleList = ''
const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_ROLELIST: (state, ruleList) => {
    state.ruleList = ruleList
  },
  SET_LICENSE: (state, license) => {
    state.license = license
  }
}
const actions = {
  // user login
  login({
    commit,
    ruleList
  }, userInfo) {
    const {
      username,
      password
    } = userInfo
    let login;
    localStorage.setItem('login_type', userInfo.type)
    return new Promise((resolve, reject) => {
      mall_login({
        username: username.trim(),
        password: password
      }).then(async response => {
        let data = response.msg
        commit('SET_TOKEN', data.token)
        setToken(data.token);
        let user = {
          site: data.msg.site,
          user: data.msg.user
        }
        localStorage.setItem('mall_id', user.user.mall_id)
        localStorage.setItem('mall_user', JSON.stringify(user))
        localStorage.setItem('mall_ruleList', JSON.stringify(data.ruleList))
        // commit('SET_ROLELIST', data.ruleList)
        //设置shop_token
        if (data.shop_token) {
          setShopToken(data.shop_token);
        };
        if (data.license == false) {
          commit('SET_LICENSE', !data.license)
          return false
        }
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  //mtos_login 静默登录
  silent_login({
    commit,
    ruleList
  }, userInfo) {
    const {
      id,
    } = userInfo
    let data = {
      merchant_id: id
    };
    return new Promise((resolve, reject) => {
      shop_login(data).then(data => {
        let token = getToken()
        setsToken(token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  //静默跳回
  silent_back({
    commit,
  }, userInfo) {
    const {
      type
    } = userInfo
    localStorage.setItem('login_type', type)
    return new Promise((resolve, reject) => {
      let token = getToken()
      removeToken()
      if (token.indexOf('suser') > -1) {
        setUserToken(token)
      } else {
        setSaasToken(token)
      }
      commit('SET_ROLELIST', null)
      resolve()
    })
  },
  // get user getrule
  getrule({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      let ruleList = JSON.parse(localStorage.getItem('mall_ruleList'))
      if (!ruleList) {
        ruleList = [{}, {}]
      }
      commit('SET_ROLELIST', ruleList)
      resolve(ruleList)
    })
  },
  setrule({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      localStorage.setItem('mall_ruleList', [{}, {}]);
      commit('SET_ROLELIST', [{}, {}])
      resolve()
    })
  },
  // user logout
  logout({
    commit,
    state
  }) {
    return new Promise((resolve, reject) => {
      mall_logout(state.token).then(() => {
        removeToken() // must remove  token  first
        //移除mall端的shop_token
        removeShopToken();
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },
  // remove token
  resetToken({
    commit
  }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  },
  setlicense({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_LICENSE', true)
      resolve()
    })
  },
  resetsetlicense({
    commit
  }) {
    return new Promise(resolve => {
      commit('SET_LICENSE', false)
      resolve()
    })
  },
}
export default {
  namespaced: true,
  state,
  mutations,
  actions
}
