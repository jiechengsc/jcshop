const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  avatar: state => state.user.avatar,
  name: state => state.user.name,
  roles: state => state.user.roles,
  ruleList: state => state.user.ruleList,
  permission_routes: state => state.permission.routes,
  allroleslist: state => state.common.allroleslist,
  license: state => state.user.license,
  addons: state => state.common.addons,
  addRoutes: state => state.permission.addRoutes,
  info: state => state.common.info,
  entry: state => state.common.entry,
  picurl: state => state.common.picurl,
}
export default getters
