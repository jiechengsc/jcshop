import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
/* Layout */
import Layout from '@/layout'
import layoutEmpty from '@/layout/layoutEmpty'
/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */
/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    children: [{
      path: '/redirect/:path(.*)',
      component: () => import('@/views/redirect/index')
    }]
  }, {
    path: '/login',
    component: () => import('@/views/login/shop_login'),
    hidden: true
  }, {
    path: '/dndlist',
    component: () => import('@/views/shop/dndlist'),
    hidden: true
  }, {
    path: '/custommenu',
    component: () => import('@/views/shop/dndlist/custommenu'),
    hidden: true
  }, {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  }, {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    hidden: true,
    children: [{
      path: 'dashboard',
      name: '首页',
      component: () => import('@/views/dashboard/index'),
      meta: {
        title: '首页',
        icon: 'dashboard'
      }
    }]
  },
]
/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const shop_asyncRoutes = [{
  path: '/shopHome',
  component: Layout,
  redirect: '/shopHome/index',
  meta: {
    title: '首页',
    icon: 'el-sc-shouyexuanzhong',
    id: 1
  },
  children: [{
    path: 'index',
    name: '商户首页',
    component: () => import('@/views/shop/shop_index/index'),
    meta: {
      title: '首页',
      icon: 'el-sc-shouyexuanzhong',
      id: 1
    }
  }]
}, {
  path: '/product',
  component: Layout,
  name: 'product',
  redirect: '/product/list/sell',
  meta: {
    title: '商品',
    icon: 'el-sc-shangpin1',
    id: [2, 3, 4]
  },
  children: [{
    path: 'list',
    name: 'list',
    component: layoutEmpty,
    meta: {
      title: '商品列表',
      icon: 'dashboard',
      level: 2,
      parents: 'product',
      id: 2
    },
    children: [{
      path: 'sell',
      name: 'productList',
      component: () => import('@/views/shop/product/list'),
      meta: {
        icon: 'dashboard',
        title: '出售中',
        level: 3,
        parents: 'product',
        id: 2
      }
    }, {
      path: 'listStock',
      name: 'productListStock',
      component: () => import('@/views/shop/product/list'),
      meta: {
        icon: 'dashboard',
        title: '仓库中',
        level: 3,
        parents: 'product',
        id: 2
      }
    }, {
      path: 'sellout',
      name: 'productselloutlist',
      component: () => import('@/views/shop/product/selloutlist'),
      meta: {
        icon: 'dashboard',
        title: '已售罄',
        level: 3,
        parents: 'product',
        id: 2
      }
    }, {
      path: 'listAudit',
      name: 'productListAudit',
      component: () => import('@/views/shop/product/listAudit'),
      meta: {
        icon: 'dashboard',
        title: '审核商品',
        level: 3,
        parents: 'product',
        id: 2
      }
    }, ]
  }, {
    path: 'freight',
    name: 'list',
    component: layoutEmpty,
    meta: {
      title: '物流设置',
      icon: 'dashboard',
      level: 2,
      parents: 'freight',
      id: [3, 4]
    },
    children: [{
      path: 'list',
      name: 'freightlist',
      component: () => import('@/views/shop/freight/index'),
      meta: {
        icon: 'dashboard',
        title: '运费模板',
        level: 3,
        parents: 'freight',
        id: 3
      }
    }, {
      path: 'returnAddresslist',
      name: 'settingLogisticsReturnList',
      component: () => import('@/views/shop/return/returnAddress'),
      meta: {
        icon: 'el-icon-chat-dot-round',
        title: '退货地址',
        level: 3,
        parents: 'settingLogisticsReturn',
        id: 4
        //activeMenu: '/notice/sms/template'
      }
    }, {
      path: 'item',
      name: 'settingLogisticsReturnItem',
      component: () => import('@/views/shop/return/returnAddressItem'),
      meta: {
        icon: 'el-icon-chat-dot-round',
        title: '操作数据',
        level: 3,
        parents: 'settingLogisticsReturn',
        activeMenu: '/setting/shop/return/list',
        id: 4
      },
      hidden: true
    }]
  }, {
    path: 'item',
    name: 'productItem',
    component: () => import('@/views/shop/product/product'),
    meta: {
      icon: 'dashboard',
      title: '商品操作',
      level: 2,
      parents: 'product',
      id: 2
    },
    hidden: true
  }]
}, {
  path: '/order',
  component: Layout,
  name: 'order',
  redirect: '/order/list/all',
  meta: {
    title: '订单',
    icon: 'el-sc-51',
    level: 1,
    id: [5, 6, 7, 8, 9]
  },
  children: [{
    path: 'list',
    name: 'orderList',
    component: layoutEmpty,
    meta: {
      icon: 'dashboard',
      title: '全部订单',
      level: 2,
      parents: 'order',
      id: [5, 6, 7, 8, 9]
    },
    children: [{
      path: 'all',
      name: 'orderList',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '全部订单',
        level: 2,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'unpaid',
      name: 'Unpaid',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '待支付',
        status: '1',
        level: 2,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'todelivered',
      name: 'Todelivered',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '待发货',
        status: '2',
        level: 2,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'towritten',
      name: 'Towritten',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '待核销',
        status: '13',
        level: 3,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'delivered',
      name: 'Delivered',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '已发货',
        status: '3',
        level: 2,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'received',
      name: 'Received',
      component: () => import('@/views/shop/order/list'),
      meta: {
        icon: 'dashboard',
        title: '已收货',
        status: '8',
        level: 3,
        parents: 'order',
        id: 5
      }
    }, {
      path: 'comment',
      name: 'commentList',
      component: () => import('@/views/shop/order/comment'),
      meta: {
        icon: 'dashboard',
        title: '订单评论',
        level: 2,
        parents: 'order',
        id: 6
      }
    }, {
      path: 'item',
      name: 'orderItem',
      component: () => import('@/views/shop/order/item'),
      meta: {
        icon: 'dashboard',
        title: '订单详情',
        level: 2,
        parents: 'order',
        activeMenu: '/order/list',
        id: 5
      },
      hidden: true
    }, {
      path: 'after',
      name: 'afterList',
      component: () => import('@/views/shop/order/after'),
      meta: {
        icon: 'dashboard',
        title: '维权订单',
        level: 2,
        parents: 'order',
        id: 7
      }
    }, {
      path: 'afterItem',
      name: 'afterItem',
      component: () => import('@/views/shop/order/afterItem'),
      meta: {
        icon: 'dashboard',
        title: '维权详情',
        level: 2,
        parents: 'order',
        activeMenu: '/order/after',
        id: 7
      },
      hidden: true
    }, {
      path: 'bulkdelivery',
      name: 'bulkdelivery',
      component: () => import('@/views/shop/order/bulkdelivery'),
      meta: {
        icon: 'dashboard',
        title: '批量发货',
        level: 2,
        parents: 'order',
        id: 8
      }
    }, {
      path: 'invoice',
      name: 'invoice',
      component: () => import('@/views/shop/order/invoice'),
      meta: {
        icon: 'dashboard',
        title: '发票列表',
        level: 2,
        parents: 'order',
        id: 9
      }
    }, ]
  }]
}, {
  path: '/finance',
  component: Layout,
  name: 'Finance',
  redirect: '/finance/withdrawal',
  meta: {
    title: '财务',
    icon: 'el-sc-caiwu4',
    level: 1,
    id: 10
  },
  children: [{
    path: 'withdrawal',
    name: 'financeWithdrawal',
    component: () => import('@/views/shop/finance/index'),
    meta: {
      icon: 'dashboard',
      title: '提现申请',
      level: 2,
      parents: 'finance',
      id: 10
    }
  }, {
    path: 'entry',
    name: 'financeEntry',
    component: () => import('@/views/shop/finance/entry'),
    meta: {
      icon: 'dashboard',
      title: '入账详情',
      level: 2,
      parents: 'finance',
      id: 19
    }
  }]
}, {
  path: '/app',
  component: Layout,
  name: 'app',
  redirect: '/app/list',
  meta: {
    title: '应用',
    icon: 'el-sc-yingyong',
    level: 1,
    id: [11, 12, 13, 14, 15]
  },
  // hidden: true,
  children: [{
    path: 'list',
    name: 'appList',
    hidden: true,
    component: () => import('@/views/shop/app/appList'),
    meta: {
      icon: 'dashboard',
      title: '应用中心',
      level: 2,
      parents: 'app',
      id: [11, 12, 13, 14, 15,20]
    }
  }, ]
}, {
  path: '/receipt',
  name: 'receipt',
  component: Layout,
  redirect: '/receipt/index',
  hidden: true,
  meta: {
    icon: 'el-sc-xiaopiao',
    title: '小票助手',
    des: '快速打印小票清单',
    level: 1,
    isapp: true,
    parents: 'templatePrint',
    apptype: 3,
    id: 13,
    a_id: 10,
  },
  children: [{
    path: 'index',
    name: 'receipt',
    component: () => import('@/views/shop/print/tempalteReceipt'),
    meta: {
      icon: 'dashboard',
      title: '小票',
      level: 2,
      id: 13,
    },
    hidden: true
  }, {
    path: 'receiptItem',
    name: 'receiptItem',
    component: () => import('@/views/shop/print/tempalteReceiptItem'),
    meta: {
      icon: 'dashboard',
      title: '模板操作',
      level: 3,
      parents: 'templatePrint',
      activeMenu: '/print/template/receipt',
      id: 13
    },
    hidden: true
  }, {
    path: 'receiptPrinter',
    name: 'receiptPrinter',
    component: () => import('@/views/shop/print/receiptPrinter'),
    meta: {
      icon: 'dashboard',
      title: '小票打印机',
      level: 3,
      parents: 'templatePrint',
      activeMenu: '/print/template/receipt',
      id: 13
    },
    hidden: true
  }, {
    path: 'receiptTask',
    name: 'receiptTask',
    component: () => import('@/views/shop/print/receiptTask'),
    meta: {
      icon: 'dashboard',
      title: '打印计划',
      level: 3,
      parents: 'templatePrint',
      activeMenu: '/print/template/receipt',
      id: 13
    },
    hidden: true
  }, ]
}, {
  path: '/store',
  component: Layout,
  name: 'store',
  redirect: '/store/storeList',
  hidden: true,
  meta: {
    title: '门店',
    icon: 'el-sc-mendian1',
    level: 1,
    des: '到店自提附近门店选择',
    isapp: true,
    apptype: 3,
    id: 14,
    a_id: 11,
  },
  children: [{
    path: 'storeList',
    name: 'storeList',
    component: () => import('@/views/shop/store/store'),
    meta: {
      title: '门店列表',
      icon: 'dashboard',
      parents: 'store',
      level: 2,
      id: 14
    }
  }, {
    path: 'storeItem',
    name: 'storeItem',
    component: () => import('@/views/shop/store/store_item'),
    meta: {
      title: '门店操作',
      icon: 'dashboard',
      parents: 'store',
      level: 2,
      id: 14
    },
    hidden: true
  }, {
    path: 'employeeList',
    name: 'employeeList',
    component: () => import('@/views/shop/store/employee'),
    meta: {
      title: '店员核销',
      icon: 'dashboard',
      parents: 'store',
      level: 2,
      id: 14
    }
  }, ]
}, {
  path: '/distribution',
  component: Layout,
  name: 'distribution',
  redirect: '/distribution/list',
  hidden: true,
  meta: {
    title: '同城配送',
    icon: 'el-sc-wenzhang',
    des: ' 页面自由编辑，信息快速获取',
    level: 1,
    isapp: true,
    apptype: 3,
    id: 20,
    a_id: 16,
  },
  children: [{
    path: 'list',
    name: 'distributionList',
    component: () => import('@/views/shop/distribution/list'),
    meta: {
      icon: 'dashboard',
      title: '同城配送',
      level: 2,
      parents: 'distribution',
      id: 20
    }
  }, {
    path: 'item',
    name: 'distributionItem',
    component: () => import('@/views/shop/distribution/item'),
    meta: {
      icon: 'dashboard',
      title: '同城配送',
      level: 2,
      parents: 'distribution',
      id: 20
    },
    hidden: true
  }]
}, {
  path: '/coupon',
  name: 'coupon',
  redirect: '/coupon/list',
  hidden: true,
  component: Layout,
  meta: {
    icon: 'el-sc-youhuiquan1',
    title: '优惠券',
    level: 1,
    isapp: true,
    des: '向用户发放商品优惠卷',
    apptype: 1,
    id: 12,
    a_id: 3,
  },
  children: [{
    path: 'list',
    name: 'couponList',
    component: () => import('@/views/shop/coupon/list'),
    meta: {
      icon: 'dashboard',
      title: '全部优惠券',
      level: 2,
      parents: 'coupon',
      id: 12
    }
  }, {
    path: 'item',
    name: 'couponItem',
    component: () => import('@/views/shop/coupon/item'),
    meta: {
      icon: 'dashboard',
      title: '操作优惠券',
      level: 2,
      parents: 'coupon',
      activeMenu: '/marketing/coupon/list',
      id: 12
    },
    hidden: true
  }, {
    path: 'record',
    name: 'recordList',
    component: () => import('@/views/shop/coupon/records'),
    meta: {
      icon: 'dashboard',
      title: '发券记录',
      level: 2,
      parents: 'coupon',
      id: 12
    },
    hidden: true
  }, {
    path: 'setting',
    name: 'settingCoupon',
    component: () => import('@/views/shop/coupon/setting'),
    meta: {
      icon: 'dashboard',
      title: '其他设置',
      level: 2,
      parents: 'coupon',
      id: 12
    }
  }, {
    path: 'examine',
    name: 'examineCoupon',
    component: () => import('@/views/shop/coupon/examine'),
    meta: {
      icon: 'dashboard',
      title: '优惠券审核',
      level: 2,
      parents: 'coupon',
      id: 12
    }
  }]
}, {
  path: '/print',
  component: Layout,
  name: 'print',
  redirect: '/print/list',
  hidden: true,
  meta: {
    title: '快递助手',
    icon: 'el-sc-kuaidi',
    level: 1,
    isapp: true,
    apptype: 3,
    des: '面单打印提升发货效率',
    id: 15,
    a_id: 14,
  },
  children: [{
    path: 'list',
    name: 'printOrderList',
    component: () => import('@/views/shop/print/printOrderList'),
    meta: {
      icon: 'dashboard',
      title: '打印列表',
      level: 2,
      parents: 'print',
      id: 15
    }
  }, {
    path: 'template',
    name: 'templatePrint',
    redirect: 'noRedirect',
    component: layoutEmpty,
    meta: {
      icon: 'dashboard',
      title: '模板管理',
      level: 2,
      parents: 'print',
      id: 15
    },
    children: [{
      path: 'electronics',
      name: 'electronics',
      component: () => import('@/views/shop/print/tempalteElectronics'),
      meta: {
        icon: 'dashboard',
        title: '电子面单',
        level: 3,
        parents: 'templatePrint',
        id: 15
      }
    }, {
      path: 'express_bill',
      name: 'express_bill',
      component: () => import('@/views/shop/print/express_bill'),
      meta: {
        icon: 'dashboard',
        title: '电子面单设置',
        level: 3,
        parents: 'templatePrint',
        id: 15
      }
    }, {
      path: 'electronicsItem',
      name: 'electronicsItem',
      component: () => import('@/views/shop/print/tempalteElectronicsItem'),
      meta: {
        icon: 'dashboard',
        title: '模板操作',
        level: 3,
        parents: 'templatePrint',
        activeMenu: '/print/template/electronics',
        id: 15
      },
      hidden: true
    }, {
      path: 'invoice',
      name: 'invoice',
      component: () => import('@/views/shop/print/tempalteInvoice'),
      meta: {
        icon: 'dashboard',
        title: '发货单',
        level: 3,
        parents: 'templatePrint',
        id: 15
      },
      // hidden: true
    }, {
      path: 'editInvoice',
      name: 'editInvoice',
      component: () => import('@/views/shop/print/tempalteInvoiceItem'),
      meta: {
        icon: 'dashboard',
        title: '模板操作',
        level: 3,
        parents: 'templatePrint',
        activeMenu: '/print/template/electronics',
        id: 15
      },
      hidden: true
    }, ]
  }]
}, {
  path: '/shopsetting',
  component: Layout,
  redirect: '/shopsetting/shopsysRight/shoprole',
  name: 'SystemSetting',
  meta: {
    title: '系统',
    icon: 'el-sc-shezhi10',
    level: 1,
    id: [16, 17, 18]
  },
  children: [{
    path: 'shopsysRight',
    name: 'settingSysRight',
    redirect: 'noRedirect',
    component: layoutEmpty,
    meta: {
      title: '系统权限',
      icon: 'nested',
      level: 2,
      id: [16, 17, 18]
    },
    children: [{
      path: 'shoprole',
      name: 'settingSysRightRole',
      component: () => import('@/views/shop/role/index'),
      meta: {
        icon: 'el-icon-chat-dot-round',
        title: '角色管理',
        level: 3,
        parents: 'settingSysRight',
        id: 16
      }
    }, {
      path: 'shopadmin',
      name: 'settingSysRightAdmin',
      component: () => import('@/views/shop/admin/index'),
      meta: {
        icon: 'el-icon-chat-dot-round',
        title: '管理员管理',
        level: 3,
        parents: 'settingSysRight',
        id: 17
        //activeMenu: '/notice/sms/template'
      }
    }, {
      path: 'shopinformation',
      name: 'settingSysRightInformation',
      component: () => import('@/views/shop/information/index'),
      meta: {
        icon: 'el-icon-chat-dot-round',
        title: '基本信息',
        level: 3,
        parents: 'settingSysRight',
        id: 18
        //activeMenu: '/notice/sms/template'
      }
    }]
  }]
}, {
  path: '*',
  redirect: '/404',
  hidden: true
}, ];
const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({
    y: 0
  }),
  routes: constantRoutes
})
const router = createRouter()
// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}
export default router
