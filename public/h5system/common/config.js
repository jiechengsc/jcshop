let baseUrl=''
if (process.env.NODE_ENV === 'development') {
	// 开发环境
	baseUrl = "https://666.wzseo365.com";
} else if (process.env.NODE_ENV === 'production') {
	// 生产环境
	// #ifdef MP-WEIXIN
	baseUrl = "https://z.jingzhe365.com";
	// #endif
	// #ifndef MP-WEIXIN
	baseUrl = window.location.origin;
	// #endif
	
}
module.exports = {
	baseUrl: baseUrl
}
