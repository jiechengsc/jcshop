import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex) // vue的插件机制
// Vuex.Store 构造器选项
const store = new Vuex.Store({
	// 为了不和页面或组件的data中的造成混淆，state中的变量前面建议加上$符号
	state: {
		// 用户信息
		$userInfo: {},
		$Token: '',
		$SystemInfo: {},
		$RuleList: [],
		$BasicsUrl: '',
		$SelectData: {
			// '1': {
			// 	imgList: [],
			// 	Limit: 1
			// },
			// '2': {
			// 	imgList: [],
			// 	Limit: 10
			// },
			// '3': {
			// 	imgList: [],
			// 	Limit: 1
			// }
		},
		$Addons:[],
		$Agentrank:0,
	},
	mutations: {
		setuserinfo(state, data) {
			state.$userInfo = data
			uni.setStorageSync('$userInfo', data)
		},
		setToken(state, data) {
			state.$Token = data
			uni.setStorageSync('$Token', data)
		},
		setRuleList(state, data) {
			state.$RuleList = data
			uni.setStorageSync('$RuleList', data)
		},
		setSystemInfo(state, data) {
			state.$SystemInfo = data
		},
		setBasicsUrl(state, data) {
			state.$BasicsUrl = data
		},
		setSelectData(state, data) {
			state.$SelectData = data
			uni.setStorageSync('$SelectData', data)
		},
		setAddons(state, data) {
			state.$Addons = data
			uni.setStorageSync('$Addons', data)
		},
		setAgentrank(state, data) {
			state.$Agentrank = data
			uni.setStorageSync('$Agentrank', data)
		},
	
		
	}
})
for (var item in store.state) {
	uni.getStorageSync(item) ? store.state[item] = uni.getStorageSync(item) : false;
}
export default store
