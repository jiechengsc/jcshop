/**
 * 响应拦截
 * @param {Object} http 
 */
module.exports = (vm) => {
	uni.$u.http.interceptors.response.use((response) => {
		/* 对响应成功做点什么 可使用async await 做异步操作*/
		const data = response
		// 自定义参数
		const custom = response.config?.custom
		if (data.statusCode !== 200 &&data.statusCode !== 201&&data.statusCode !== 204) { // 服务端返回的状态码不等于200，则reject()
			// 如果没有显式定义custom的toast参数为false的话，默认对报错进行toast弹出提示
			if (data.statusCode == 401||data.statusCode == 403) {
				uni.$u.route({
					url:'/pages/login/login',
					type:'reLaunch'
				});
				
			}
			if (custom.toast !== false) {
				uni.$u.toast(data.message)
			}
			// 如果需要catch返回，则进行reject
			if (custom?.catch) {
				return Promise.reject(data)
			} else {
				// 否则返回一个pending中的promise
				return new Promise(() => {})
			}
		}
		return data.data || {}
	}, (response) => {
		/*  对响应错误做点什么 （statusCode !== 200）*/
		if (response.statusCode == 401||response.statusCode == 403) {
			uni.$u.route({
				url:'/pages/login/login',
				type:'reLaunch'
			});
		} else {
			uni.$u.toast(response.data.error_msg || response.data.msg)
			return Promise.reject(response)
		}
	})
}
