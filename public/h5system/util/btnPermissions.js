import Vue from 'vue'
import store from '@/store'
const whiteList = ['deliver', "refund", "delete", "audit"]
/**权限指令**/
const has = Vue.directive('has', {
	inserted: function(el, binding, vnode) {
		// 获取页面按钮权限
		let btnPermissionsArr = '';
		if (binding.value) {
			let data = binding.value
			if (!Vue.prototype.$_has(data.id, data.type || "update") && el.parentNode) {
				el.parentNode.removeChild(el);
			}
		}
		// if (binding.value) {
		//   if ((binding.value == 'refund' || binding.value == 'send') && type == 'shop'&&el.parentNode) {
		//     el.parentNode.removeChild(el)
		//     return false
		//   }
		//   if (whiteList.indexOf(binding.value) > -1) {
		//     btnPermissionsArr = vnode.context.$route.meta.id;
		//     if (!Vue.prototype.$_has(btnPermissionsArr, binding.value)&&el.parentNode) {
		//       el.parentNode.removeChild(el);
		//     }
		//   } else {
		//     if (type == 'saas') {
		//       var store = saasstore
		//     } else if (type == 'mall') {
		//       var store = mallstore
		//     } else if (type == 'shop') {
		//       var store = shopstore
		//     }
		//     // 如果指令传值，获取指令参数，判断插件权限
		//     let addons = store.state.common.addons
		//     if (binding.value != -1 && addons && addons.indexOf(binding.value) == -1&&el.parentNode) {
		//       el.parentNode.removeChild(el);
		//     }
		//   }
		// } else {
		//   // 否则获取路由中的参数，根据路由的btnPermissionsArr和当前登录人按钮权限做比较。
		//   btnPermissionsArr = vnode.context.$route.meta.id;
		//   if (!Vue.prototype.$_has(btnPermissionsArr, "update")&&el.parentNode) {
		//     el.parentNode.removeChild(el);
		//   }
		// }
	}
});
// 权限检查方法
Vue.prototype.$_has = function(value, value_type) {
	let isExist = false;
	let rulelist = store.state.$RuleList
	// 获取用户按钮权限、
	if (rulelist == undefined || rulelist == null) {
		return false;
	}
	if (rulelist.findIndex(_ => _.level == value && _.resource == value_type) > -1) {
		isExist = true;
	}
	return isExist;
};
export {
	has
}
