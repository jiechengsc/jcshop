SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for jiecheng_accept_record
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_accept_record`;
CREATE TABLE `jiecheng_accept_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `order_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `commodity` json NULL DEFAULT NULL,
  `assistant` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `assistant_user_id` int(11) NULL DEFAULT NULL,
  `store` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `accept_time` datetime NULL DEFAULT NULL,
  `type` int(2) DEFAULT '1' COMMENT '1-订单 2-抽奖 3-积分',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='核销记录表';

-- ----------------------------
-- Table structure for jiecheng_addons
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_addons`;
CREATE TABLE `jiecheng_addons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `addons_id` int(11) NOT NULL DEFAULT '0',
  `new_version` varchar(255) NOT NULL DEFAULT '',
  `old_version` varchar(255) NOT NULL DEFAULT '',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00',
  `name` varchar(32) NOT NULL DEFAULT '',
  `img` varchar(255) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0:为授权,1:未启用,2:启用',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `token`  varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_addons` (`addons_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='插件授权';


DROP TABLE IF EXISTS `jiecheng_command_log`;
CREATE TABLE `jiecheng_command_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `content` varchar(1000) NOT NULL DEFAULT '',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;


DROP TABLE IF EXISTS `jiecheng_addons_log`;
CREATE TABLE `jiecheng_addons_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addons_id` tinyint(1) NOT NULL DEFAULT '0',
  `type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '类型:0:安装 1:更新 2卸载',
  `old_versions` varchar(64) DEFAULT '',
  `versions` varchar(64) NOT NULL DEFAULT '',
  `content` varchar(1000) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态:0=未开始 1=成功 2=失败',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='插件安装记录';



DROP TABLE IF EXISTS `jiecheng_update_record`;
CREATE TABLE `jiecheng_update_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `addons_id`  int(11) NOT NULL DEFAULT 0,
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '类型:1:更新,2:回滚',
  `old_versions` varchar(64) NOT NULL DEFAULT '',
  `versions` varchar(64) DEFAULT '',
  `content` varchar(1000) NOT NULL DEFAULT '',
  `update` varchar(64) NOT NULL DEFAULT '',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态:0:未更新,1:文件已下载,2:更新中,3:更新完成,4:更新失败',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='更新记录';


DROP TABLE IF EXISTS `jiecheng_copyright`;
CREATE TABLE `jiecheng_copyright` (
  `name` varchar(32) NOT NULL DEFAULT '',
  `mobile` varchar(32) NOT NULL DEFAULT '',
  `logo` varchar(255) NOT NULL DEFAULT '',
  `record_number` varchar(255) NOT NULL DEFAULT '',
  `token` varchar(255) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'pc版权',
  `link_info` json NULL DEFAULT NULL COMMENT '小程序版权',
  `record_number1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '公安备案号' ,
  PRIMARY KEY (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='版权';

-- ----------------------------
-- Table structure for jiecheng_admin
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_admin`;
CREATE TABLE `jiecheng_admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `mall_id` int(11) NULL DEFAULT NULL,
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `role_id` int(10) NOT NULL COMMENT '角色id',
  `is_root` bit(1) NOT NULL DEFAULT b'0',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态: 0:未启用,1:正常,2:限制,3:删除',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `ac_token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '登录token',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录ip',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_username` (`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='后台管理员';

-- ----------------------------
-- Table structure for jiecheng_advert
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_advert`;
CREATE TABLE `jiecheng_advert` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `content` text,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `mall_id` (`mall_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商场广告';

-- ----------------------------
-- Table structure for jiecheng_after_config
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_after_config`;
CREATE TABLE `jiecheng_after_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `explain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款说明',
  `is_retreat` tinyint(1) NULL DEFAULT NULL COMMENT '单品退换:1-开启 2-关闭',
  `days` tinyint(4) NULL DEFAULT NULL COMMENT '订单完成多少天允许维权',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='维权配置';

-- ----------------------------
-- Table structure for jiecheng_after_order_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_after_order_commodity`;
CREATE TABLE `jiecheng_after_order_commodity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_after_id` int(11) NULL DEFAULT NULL,
  `ocid` int(11) NULL DEFAULT NULL COMMENT 'order_commodity表ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='维权商品';

-- ----------------------------
-- Table structure for jiecheng_after_process
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_after_process`;
CREATE TABLE `jiecheng_after_process` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_after_id` int(11) NULL DEFAULT NULL COMMENT 'order_after表ID',
  `content` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '驳回备注',
  `step` tinyint(1) NULL DEFAULT NULL COMMENT '流程步骤',
  `step_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '流程名称',
  `handle_time` datetime NULL DEFAULT NULL COMMENT '处理时间',
  `state` tinyint(1) DEFAULT '2' COMMENT '1-已同意 2-未处理 3-已驳回',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='退货处理流程表';

-- ----------------------------
-- Table structure for jiecheng_agent
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_agent`;
CREATE TABLE `jiecheng_agent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `grade_id` int(11) DEFAULT '0' COMMENT '等级',
  `pid` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `apply_id` int(11) DEFAULT '0',
  `status` tinyint(2) DEFAULT '1',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index_pid` (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='推广员';

-- ----------------------------
-- Table structure for jiecheng_agent_apply
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_agent_apply`;
CREATE TABLE `jiecheng_agent_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户',
  `pid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态: 0:申请中,1:审核通过,2:审核失败',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `pk_agent_apply_user_id` (`user_id`) USING BTREE,
  CONSTRAINT `pk_agent_apply_user` FOREIGN KEY (`user_id`) REFERENCES `jiecheng_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='推广员申请入驻';

-- ----------------------------
-- Table structure for jiecheng_agent_bill_temporary
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_agent_bill_temporary`;
CREATE TABLE `jiecheng_agent_bill_temporary` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `source_user_id` int(11) NOT NULL COMMENT '来源用户ID',
  `tree` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '层级',
  `steps` int(1) NOT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单号',
  `commodity_id` int(11) NULL DEFAULT NULL,
  `order_commodity_id` int(11) NULL DEFAULT NULL COMMENT '订单商品表ID',
  `sku_id` int(11) NULL DEFAULT NULL,
  `amount` decimal(11,2) NULL DEFAULT NULL COMMENT '推广费',
  `number` int(11) NULL DEFAULT NULL COMMENT '数量',
  `figure_type` bit(1) DEFAULT b'0' COMMENT '推销费模式 0-比例 1-固定',
  `figure` decimal(8,6) NULL DEFAULT NULL COMMENT '推广费比例',
  `money` decimal(11,2) NULL DEFAULT NULL COMMENT '金额',
  `status` tinyint(1) DEFAULT '0' COMMENT '0-未结算 1-已结算 2-已取消,3-到账取消',
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `commission_rate` float NOT NULL DEFAULT 0 COMMENT '考核佣金比例',
  `commission` float NOT NULL DEFAULT 0 COMMENT '考核额外佣金',
  `is_assessment` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否考核 0=否 1=是',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `is_self` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否自购 0=否 1=是',
  `assessment_status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '结算状态 0=未结算 1=结算',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `order_id` (`order_no`) USING BTREE,
  KEY `uniqid` (`uniqid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='佣金记录';

-- ----------------------------
-- Table structure for jiecheng_agent_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_agent_withdraw`;
CREATE TABLE `jiecheng_agent_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `withdraw` decimal(10,2) NOT NULL,
  `service_charge` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `is_online` tinyint(2) DEFAULT '0' COMMENT '是否线上,1:线上,0:线下',
  `type` int(10) NOT NULL COMMENT '类型:1-微信,2-零钱',
  `card` json NULL DEFAULT NULL COMMENT '线下卡片数据',
  `serial_num` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态:0:申请中,1:通过,2:取消',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_time` datetime NULL DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `receipt_img` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_agent_withdraw` (`agent_id`) USING BTREE,
  CONSTRAINT `pk_agent_withdraw` FOREIGN KEY (`agent_id`) REFERENCES `jiecheng_agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='推广员提现记录';

-- ----------------------------
-- Table structure for jiecheng_agent_grade
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_agent_grade`;
CREATE TABLE `jiecheng_agent_grade` (
  `id` int(11) NOT NULL COMMENT '1-10级',
  `mall_id` int(11) NOT NULL,
  `name` varchar(32) NOT NULL DEFAULT '' COMMENT '等级名称',
  `l1` decimal(10,2) NOT NULL DEFAULT '0.00',
  `l2` decimal(10,2) NOT NULL DEFAULT '0.00',
  `l3` decimal(10,2) NOT NULL DEFAULT '0.00',
  `users` int(11) NOT NULL DEFAULT '0' COMMENT '下级用户人数',
  `commission` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '佣金总额',
  `draw` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '已提现佣金总额',
  `payment` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '下级用户消费金额',
  `agents` int(11) NOT NULL DEFAULT '0' COMMENT '下级分销商人数',
  `status` tinyint(2) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='分销等级';

-- ----------------------------
-- Table structure for jiecheng_ali_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_ali_sms_template`;
CREATE TABLE `jiecheng_ali_sms_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `scene` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '场景',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `sign_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `template_code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `attr` json NULL DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:不启用,1:启用',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `Index_scene` (`scene`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='短信模版';

-- ----------------------------
-- Table structure for jiecheng_area
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_area`;
CREATE TABLE `jiecheng_area` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_code` int(6) NOT NULL,
  `area_code` int(6) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_area` (`area_code`) USING BTREE,
  KEY `Index_parent` (`parent_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='地区';

-- ----------------------------
-- Table structure for jiecheng_article
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_article`;
CREATE TABLE `jiecheng_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `headimg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `subhead` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `fk_type` (`type`) USING BTREE,
  CONSTRAINT `fk_type` FOREIGN KEY (`type`) REFERENCES `jiecheng_article_type` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='文章';

-- ----------------------------
-- Table structure for jiecheng_article_type
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_article_type`;
CREATE TABLE `jiecheng_article_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='文章分类';

-- ----------------------------
-- Table structure for jiecheng_attach_name
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_attach_name`;
CREATE TABLE `jiecheng_attach_name` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '附加属性名称',
  `need`INT(10) NOT NULL DEFAULT '0' COMMENT '选择下限',
  `toplimit` int(11) NOT NULL DEFAULT '1' COMMENT '上限最大值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='附加属性表';

-- ----------------------------
-- Table structure for jiecheng_attach_value
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_attach_value`;
CREATE TABLE `jiecheng_attach_value` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `attach_name_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '规格名',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '附加价格',
  `stock` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '库存',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='附加属性表';

-- ----------------------------
-- Table structure for jiecheng_attr_name
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_attr_name`;
CREATE TABLE `jiecheng_attr_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `has_pic` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否开启图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='属性名';

-- ----------------------------
-- Table structure for jiecheng_attr_value
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_attr_value`;
CREATE TABLE `jiecheng_attr_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `attr_name_id` int(11) NULL DEFAULT NULL,
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='属性值';

-- ----------------------------
-- Table structure for jiecheng_base_configuration
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_base_configuration`;
CREATE TABLE `jiecheng_base_configuration` (
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `configuration` json NULL DEFAULT NULL,
  PRIMARY KEY (`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='配置';

-- ----------------------------
-- Table structure for jiecheng_bottom_menu
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_bottom_menu`;
CREATE TABLE `jiecheng_bottom_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `data` json NULL DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='底部菜单';

-- ----------------------------
-- Table structure for jiecheng_carry_mode_num
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_carry_mode_num`;
CREATE TABLE `jiecheng_carry_mode_num` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `ft_id` int(11) NOT NULL COMMENT '模板id',
  `district_value` mediumtext COMMENT '配送区域值',
  `district` text COMMENT '配送区域',
  `first_piece` int(11) NOT NULL DEFAULT '0' COMMENT '首件数量',
  `first_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '首件费用',
  `second_piece` int(11) NOT NULL DEFAULT '0' COMMENT '续件',
  `second_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '续费',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_freight_carry` (`ft_id`) USING BTREE,
  CONSTRAINT `pk_freight_carry` FOREIGN KEY (`ft_id`) REFERENCES `jiecheng_freight_temp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='运送方式';

-- ----------------------------
-- Table structure for jiecheng_carry_mode_weight
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_carry_mode_weight`;
CREATE TABLE `jiecheng_carry_mode_weight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `ft_id` int(11) NOT NULL COMMENT '模板id',
  `district_value` mediumtext COMMENT '区域值',
  `district` text COMMENT '配送区域',
  `first_weight` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '首件重量',
  `first_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '首件费用',
  `second_weight` decimal(18,2) NOT NULL DEFAULT '0.00' COMMENT '续重',
  `second_amount` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '续费',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_freight_carry` (`ft_id`) USING BTREE,
  CONSTRAINT `jiecheng_carry_mode_weight_ibfk_1` FOREIGN KEY (`ft_id`) REFERENCES `jiecheng_freight_temp` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='运送方式';

-- ----------------------------
-- Table structure for jiecheng_classify
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_classify`;
CREATE TABLE `jiecheng_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pid` int(11) NOT NULL DEFAULT '0',
  `status` bit(1) DEFAULT b'1',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_pid` (`pid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='分类';

-- ----------------------------
-- Table structure for jiecheng_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commodity`;
CREATE TABLE `jiecheng_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL COMMENT '商户',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '0:实体商品,1:虚拟商品',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
  `code` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '商品编码',
  `subtitle` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `label` json NULL DEFAULT NULL COMMENT '标签',
  `master` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '主图',
  `slideshow` json NULL DEFAULT NULL COMMENT '轮播图',
  `share` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分享图',
  `classify_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类',
  `classify_value` text,
  `weight` double NULL DEFAULT NULL COMMENT '重量',
  `deliver` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '3' COMMENT '提取方式:1:快递,2:自提,3:三者都支持，4:同城配送',
  `ft_type` int(2) NOT NULL DEFAULT '0' COMMENT '配送方式:0:包邮,1:配送模板,2:统一价格',
  `ft_id` int(11) NOT NULL DEFAULT '0' COMMENT '配送模板',
  `ft_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '统一配送价格',
  `is_limitation` tinyint(2) DEFAULT '0' COMMENT '是否限购:0:不限,1:限购',
  `limitation` int(11) NULL DEFAULT NULL COMMENT '限购数量',
  `limitation_time` datetime NULL DEFAULT NULL,
  `limitation_type` tinyint(1) DEFAULT '0' COMMENT '限购类型: 1-单笔限购 2-历史限购 ',
  `inventory_type` tinyint(2) DEFAULT '1' COMMENT '库存类型:1:下单前减库存,2:永不减库存,3:付款减库存',
  `total` int(11) unsigned NOT NULL COMMENT '数量',
  `sell` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '售出',
  `is_original_price` int(1) NOT NULL DEFAULT '0' COMMENT '是否开启原价:0:不开启,1:开启',
  `original_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '原价',
  `sell_price` decimal(10,2) NOT NULL COMMENT '销售价',
  `min_price` decimal(10,2) NULL DEFAULT NULL COMMENT '最小价',
  `max_price` decimal(10,2) NULL DEFAULT NULL COMMENT '最大价',
  `cost_price` decimal(10,2) NOT NULL DEFAULT '0' COMMENT '成本价',
  `content` text COMMENT '详情',
  `sort` int(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `has_parameter` int(1) NOT NULL DEFAULT '0' COMMENT '是否拥有参数,0:不拥有,1:拥有',
  `parameter` json NULL DEFAULT NULL COMMENT '参数',
  `is_virtual` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否开启虚拟销量:0:不开启,1:开启',
  `virtual_sell` int(10) DEFAULT '0' COMMENT '虚拟销量',
  `is_distribution` int(1) NOT NULL DEFAULT '0' COMMENT '是否参与分销:0:不参与,1:参与',
  `is_base_distribution` int(1) NOT NULL DEFAULT '0' COMMENT '启用全局配置:0:是,1:不是',
  `distribution` json NULL DEFAULT NULL COMMENT '分销',
  `has_sku` int(1) NOT NULL DEFAULT '0' COMMENT '是否拥有sku',
  `sku_value` json NULL DEFAULT NULL COMMENT 'sku值,前台显示用',
  `audit` tinyint(2) NOT NULL DEFAULT '0',
  `audit_remark` text,
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `integral_deduction` int(1) NOT NULL DEFAULT '0' COMMENT '是否开启积分抵扣',
  `integral_maximum` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '积分最大扣除额度',
  `is_integral` int(1) NOT NULL DEFAULT '0' COMMENT '是否积分商品',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `fraction` decimal(12,4) DEFAULT '0.0000' COMMENT '分数',
  `retreat` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退换货类型 1-退款 2-退货退款 3-换货',
  `is_open_member_price` int(1) DEFAULT '0' COMMENT '是否开启会员价:0-不开启,1-开启',
  `member_price_type` tinyint(2) DEFAULT '1' COMMENT '会员价类型:1-系统默认,2-折扣,3-固定价格',
  `member_price` decimal(10,2) NULL DEFAULT NULL COMMENT '折扣价或者最终价',
  `is_intra_city` tinyint(2) DEFAULT '0' COMMENT '是否同城配送:0-不开启,1:开启',
  `level_price` json NULL DEFAULT NULL,
  `has_attach` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否拥有附加属性',
  `attach_value` json NULL DEFAULT NULL COMMENT '附加属性值',
  `form_open` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=不开启 1=开启',
  `template_id` int(11) NOT NULL DEFAULT 0 COMMENT '商品表单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='spu';

-- ----------------------------
-- Table structure for jiecheng_commodity_total
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commodity_total`;
CREATE TABLE `jiecheng_commodity_total` (
  `commodity_id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `click` int(11) DEFAULT '0',
  PRIMARY KEY (`commodity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='浏览量';

-- ----------------------------
-- Table structure for jiecheng_configuration
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_configuration`;
CREATE TABLE `jiecheng_configuration` (
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `configuration` json NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`type`,`mall_id`) USING BTREE,
  UNIQUE KEY `Index_mall_type` (`mall_id`,`type`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='配置';

-- ----------------------------
-- Table structure for jiecheng_coupon
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_coupon`;
CREATE TABLE `jiecheng_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `used_type` tinyint(2) DEFAULT '1' COMMENT '使用位置:0:通用,1:店铺',
  `denomination` double(11,2) NOT NULL COMMENT '面额',
  `conditions` double(11,2) NOT NULL COMMENT '使用条件',
  `time_limit_type` bit(1) NOT NULL DEFAULT b'0' COMMENT '使用时间限制',
  `limit_time` int(11) NULL DEFAULT NULL COMMENT '限制天数,type为0',
  `limit_indate_begin` datetime NULL DEFAULT NULL COMMENT '有效期开始时间,type为1',
  `limit_indate_end` datetime NULL DEFAULT NULL COMMENT '有效期结束时间,type为1',
  `total_limit_type` bit(1) NOT NULL DEFAULT b'0' COMMENT '发放总量限制类型:0:不限制,1:限制',
  `total_limit` int(11) NULL DEFAULT NULL COMMENT '总量',
  `user_limit_type` bit(1) NOT NULL DEFAULT b'0' COMMENT '每人限领:0:不限,1:限制',
  `user_limit` int(11) NULL DEFAULT NULL COMMENT '每人限领张数',
  `commodity_limit_type` tinyint(2) NOT NULL COMMENT '使用限制:0:不限制,1:允许以下商品,2:限制以下商品,3:允许一下分类',
  `commodity_limit` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '限制条件,用","拼接',
  `limit_value` text COMMENT '限制条件的中文值',
  `use_limit_type` tinyint(2) NOT NULL COMMENT '使用限制:0:不限制,1:不可与其他折扣同时使用',
  `get_type` tinyint(2) NULL DEFAULT NULL COMMENT '领取方式:0:领券中心,1:链接领取,2:活动领取',
  `sort` smallint(5) DEFAULT '1' COMMENT '排序规则,越小越靠前',
  `explain_type` tinyint(2) DEFAULT '0',
  `instructions` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '使用说明',
  `used` int(11) DEFAULT '0' COMMENT '已领取数量',
  `audit` tinyint(1) DEFAULT '0' COMMENT '审核状态:0:未审核,1:已审核',
  `audit_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核不通过原因',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态:0:未开始,1:开始中,2:领完,3:过期',
  `is_show` tinyint(1) DEFAULT '0' NOT NULL COMMENT '0=不展示 1=展示',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='优惠券';

-- ----------------------------
-- Table structure for jiecheng_dada
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_dada`;
CREATE TABLE `jiecheng_dada` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `distance` double(11,2) NULL DEFAULT NULL COMMENT '配送距离(单位：米)',
  `fee` double(11,2) NULL DEFAULT NULL COMMENT '实际运费(单位：元)，运费减去优惠券费用',
  `deliver_fee` double(11,2) NULL DEFAULT NULL COMMENT '运费(单位：元)',
  `coupon_fee` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优惠券费用(单位：元)',
  `order_id` int(11) NULL DEFAULT NULL COMMENT '订单号',
  `order_status` tinyint(1) NULL DEFAULT NULL COMMENT '订单状态(待接单＝1,待取货＝2,配送中＝3,已完成＝4,已取消＝5, 指派单=8,妥投异常之物品返回中=9, 妥投异常之物品返回完成=10, 骑士到店=100,创建达达运单失败=1000 可参考文末的状态说明）',
  `cancel_reason` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单取消原因,其他状态下默认值为空字符串',
  `cancel_from` tinyint(1) NULL DEFAULT NULL COMMENT '订单取消原因来源(1:达达配送员取消；2:商家主动取消；3:系统或客服取消；0:默认值)',
  `dm_id` int(11) NULL DEFAULT NULL COMMENT '达达配送员id，接单以后会传',
  `dm_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配送员姓名，接单以后会传',
  `dm_mobile` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配送员手机号，接单以后会传',
  `finish_code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货码',
  `insurance_fee` double(11,2) NULL DEFAULT NULL COMMENT '保价费(单位：元)',
  `tips` double(11,2) NULL DEFAULT NULL COMMENT '小费(单位：元)',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `client_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '达达订单',
  `mall_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='达达';

-- ----------------------------
-- Table structure for jiecheng_default_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_default_template`;
CREATE TABLE `jiecheng_default_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `page_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面名称',
  `page_type` tinyint(1) NULL DEFAULT NULL COMMENT ' 1-商城首页 2-商品详情 3-会员中心 4- 自定义页面',
  `status` tinyint(4) DEFAULT '1' COMMENT '1-未应用 2-应用中 ',
  `small_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩列图',
  `page_key` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'DIY配置key',
  `qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '二维码路径',
  `page_config` text COMMENT '页面属性',
  `source` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '来源: 1- 小程序 2-公众号',
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime NULL DEFAULT NULL,
  `configure` longtext COMMENT '配置',
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='DIY配置';

-- ----------------------------
-- Table structure for jiecheng_department
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_department`;
CREATE TABLE `jiecheng_department` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门名',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(2) NULL DEFAULT NULL COMMENT '状态: 0:未启用,1:正常,2:限制,3:删除',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='部门';

-- ----------------------------
-- Table structure for jiecheng_distribution
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_distribution`;
CREATE TABLE `jiecheng_distribution` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '1-商家配送 2-第三方配送',
  `third_party` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方配送，1-美团配送 2-达达配送',
  `config` text COMMENT '第三方配送方式配置',
  `shop_address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺详细地址',
  `shop_region` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商铺所属区域',
  `area` tinyint(1) NULL DEFAULT NULL COMMENT '1-按不同区域 2-按不同距离 3-按行政区域',
  `divide_config` text COMMENT '商家配送-配置',
  `billing_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '计费方式',
  `renewal_charge` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '续重收费',
  `divide_type` tinyint(1) DEFAULT '1' COMMENT '1-按半径划分 2-自定义',
  `gaude_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '高德KEY',
  `is_range` tinyint(1) DEFAULT '1' COMMENT '是否使用快递：1-是 2-否',
  `contacts` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系人',
  `contact_phone_one` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话1',
  `contact_phone_two` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话2',
  `coordinate` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '店铺经纬度',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `area_data` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '行政区域-可配送区域数组',
  `store_id` int(11) NULL DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `area_config` text NOT NULL COMMENT '行政配送-配置',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='同城配送表';

-- ----------------------------
-- Table structure for jiecheng_express
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_express`;
CREATE TABLE `jiecheng_express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `code` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `ali_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `r_datas` json NULL DEFAULT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='快递';

-- ----------------------------
-- Table structure for jiecheng_express_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_express_template`;
CREATE TABLE `jiecheng_express_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `trans_type` int(1) DEFAULT '1' COMMENT '1-陆运 2-空运',
  `pay_type` int(1) DEFAULT '1' COMMENT '支付方式 1-现付 2-到付 3-月结 4-第三方支付',
  `exp_type` int(1) NULL DEFAULT NULL,
  `is_return_sign_bill` int(1) DEFAULT '2' COMMENT '是否要求回单 1-是 2-否',
  `operate_require` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签回单操作要求(如：签名、盖章、身份证复印件等)',
  `is_notice` int(1) DEFAULT '1' COMMENT '是否通知快递员揽件 0-通知 1-不通知',
  `is_return_print_template` int(1) DEFAULT '1' COMMENT '是否返回电子面单模板 0-不返回 1-返回',
  `is_send_message` int(1) DEFAULT '0' COMMENT '是否订阅短信 0-不需要 1-需要',
  `customer_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户账号',
  `customer_pwd` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客户秘密',
  `send_site` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送站点',
  `send_staff` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '派遣工作人员',
  `month_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '月份代码',
  `ware_house_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发货仓编码',
  `company` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人公司',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人姓名',
  `tel` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人电话号码',
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人手机号码',
  `post_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人邮编',
  `province_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人省',
  `city_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `exp_area_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人县',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发件人详细地址',
  `shipper_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司变换',
  `merchant_id` int(11) DEFAULT '0' COMMENT '商户ID',
  `delete_time` datetime NULL DEFAULT NULL,
  `default` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='快递模版';

-- ----------------------------
-- Table structure for jiecheng_form_answer
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_form_answer`;
CREATE TABLE `jiecheng_form_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `template_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `answer` json NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `form_template_id` (`template_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='自定义表单';

-- ----------------------------
-- Table structure for jiecheng_form_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_form_template`;
CREATE TABLE `jiecheng_form_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `config` json NULL DEFAULT NULL COMMENT '配置',
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片',
  `status` int(1) NULL DEFAULT NULL COMMENT '状态',
  `tries_limit` int(2) NULL DEFAULT NULL COMMENT '限制次数',
  `merchant_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='表单模版';

-- ----------------------------
-- Table structure for jiecheng_freight_temp
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_freight_temp`;
CREATE TABLE `jiecheng_freight_temp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `express_id` int(11) NOT NULL COMMENT '快递id',
  `carry_mode_type` bit(1) NOT NULL DEFAULT b'0' COMMENT '计费方式:0:按件,1:按重量',
  `no_carry` text COMMENT '不可配送区域,","拼接',
  `no_carry_value` text COMMENT '不可配送区域中文值',
  `is_default` bit(1) DEFAULT b'0',
  `sort` int(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` bit(1) NOT NULL DEFAULT b'1',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='运费模板';

-- ----------------------------
-- Table structure for jiecheng_integral_attr_name
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_attr_name`;
CREATE TABLE `jiecheng_integral_attr_name` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `has_pic` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否开启图片',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='属性名';

-- ----------------------------
-- Table structure for jiecheng_integral_attr_value
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_attr_value`;
CREATE TABLE `jiecheng_integral_attr_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `attr_name_id` int(11) NULL DEFAULT NULL,
  `value` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='属性值';

-- ----------------------------
-- Table structure for jiecheng_integral_classify
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_classify`;
CREATE TABLE `jiecheng_integral_classify` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` bit(1) DEFAULT b'1',
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='分类';

-- ----------------------------
-- Table structure for jiecheng_integral_goods
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_goods`;
CREATE TABLE `jiecheng_integral_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商品名称',
  `subtitle` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '副标题',
  `master` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '主图',
  `slideshow` json NULL DEFAULT NULL COMMENT '轮播图',
  `classify_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '分类',
  `total` int(11) unsigned NOT NULL COMMENT '数量',
  `sell` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '销量',
  `sell_price` decimal(10) NOT NULL COMMENT '销售价',
  `min_price` int(11) NULL DEFAULT NULL,
  `max_price` int(10) NULL DEFAULT NULL,
  `content` text COMMENT '详情',
  `sort` int(4) NOT NULL DEFAULT '0' COMMENT '排序',
  `has_sku` int(1) NOT NULL DEFAULT '0' COMMENT '是否拥有sku',
  `sku_value` json NULL DEFAULT NULL COMMENT 'sku值,前台显示用',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `status` int(1) DEFAULT '0',
  `type` int(1) DEFAULT '0' COMMENT '0-实体 1-虚拟',
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='spu';

-- ----------------------------
-- Table structure for jiecheng_integral_order
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_order`;
CREATE TABLE `jiecheng_integral_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单表',
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_no` char(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '下单客户ID',
  `status` tinyint(1) DEFAULT '1' COMMENT '订单状态:1-待发货 2-已发货 3-完成 4-关闭',
  `money` int(11) NULL DEFAULT NULL COMMENT '订单总额',
  `type` tinyint(1) DEFAULT '1',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `consignee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人',
  `remake` text COMMENT '备注',
  `iphone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人电话号码',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `pay_time` datetime NULL DEFAULT NULL COMMENT '付款时间',
  `order_type` tinyint(1) DEFAULT '1' COMMENT '1-快递 2-自提 3-同城配送',
  `store_id` int(11) DEFAULT '0' COMMENT '门店ID',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `order_no` (`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='积分订单';

-- ----------------------------
-- Table structure for jiecheng_integral_order_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_order_commodity`;
CREATE TABLE `jiecheng_integral_order_commodity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL COMMENT '订单-商品关联表',
  `commodity_id` int(11) NULL DEFAULT NULL,
  `count` int(11) NULL DEFAULT NULL COMMENT '下单数量',
  `status` tinyint(1) DEFAULT '1' COMMENT '订单状态: 1-待发货 2-已发货 3-已收货',
  `money` decimal(8,2) NULL DEFAULT NULL COMMENT '原来金额',
  `sku_id` int(11) NULL DEFAULT NULL COMMENT 'skuID',
  `logistics_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `express_company` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `type` tinyint(4) DEFAULT '1' COMMENT '1-快递 2-自提',
  `receiving_time` datetime NULL DEFAULT NULL COMMENT '收货时间',
  `deliver_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '商品单价',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `commodity_id` (`commodity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='积分子订单';

-- ----------------------------
-- Table structure for jiecheng_integral_record
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_record`;
CREATE TABLE `jiecheng_integral_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `commodity_id` int(11) NULL DEFAULT NULL COMMENT '商品ID',
  `sku_id` int(11) DEFAULT '0',
  `number` int(11) DEFAULT '1' COMMENT '数量',
  `type` int(2) NULL DEFAULT NULL COMMENT '积分类型1-订单获得 2-下单消费 3-分享获得 4-后台添加 5-后台减少',
  `status` int(1) NULL DEFAULT NULL COMMENT '0-未处理 1-已经处理',
  `integral` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=748 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='积分记录';

-- ----------------------------
-- Table structure for jiecheng_integral_sku
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_sku`;
CREATE TABLE `jiecheng_integral_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `commodity_id` int(11) NOT NULL,
  `weight` double NULL DEFAULT NULL COMMENT '重量',
  `pvs` text NOT NULL,
  `pvs_value` text,
  `pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='sku';

-- ----------------------------
-- Table structure for jiecheng_integral_sku_inventory
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_integral_sku_inventory`;
CREATE TABLE `jiecheng_integral_sku_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `sku_id` int(11) NOT NULL,
  `original_price` int(10) DEFAULT '0',
  `sell_price` int(10) NOT NULL,
  `total` int(11) NOT NULL,
  `sell` int(11) unsigned DEFAULT '0',
  `status` bit(1) NOT NULL DEFAULT b'0',
  `level_price` json NULL DEFAULT NULL COMMENT '会员梯度价格',
  PRIMARY KEY (`id`),
  KEY `integral_sku_inventory_ibfk_1` (`sku_id`) USING BTREE,
  CONSTRAINT `jiecheng_integral_sku_id` FOREIGN KEY (`sku_id`) REFERENCES `jiecheng_integral_sku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='商品库存';

-- ----------------------------
-- Table structure for jiecheng_invoice
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_invoice`;
CREATE TABLE `jiecheng_invoice` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL COMMENT '订单ID',
  `duty_paragraph` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '税号',
  `address` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `deposit_bank` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行账户',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态: 1-未审核 2-已通过 3-已驳回',
  `rise` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发票抬头',
  `invoice_type` tinyint(1) DEFAULT '1' COMMENT '抬头类型:1-个人 2-单位',
  `remake` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '驳回备注',
  `money` decimal(11,2) DEFAULT '0.00' COMMENT '金额',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否默认: 1-是 0-否',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL COMMENT '发票管理',
  `mailbox` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='发票';

-- ----------------------------
-- Table structure for jiecheng_invoice_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_invoice_template`;
CREATE TABLE `jiecheng_invoice_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_520_ci,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL COMMENT '标题',
  `express_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COLLATE utf8mb4_unicode_520_ci NULL DEFAULT NULL,
  `config` json NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci ROW_FORMAT=DYNAMIC COMMENT='发票模版';

-- ----------------------------
-- Table structure for jiecheng_jump_wxa
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_jump_wxa`;
CREATE TABLE `jiecheng_jump_wxa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `appid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='小程序跳转';

-- ----------------------------
-- Table structure for jiecheng_lottery_config
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_lottery_config`;
CREATE TABLE `jiecheng_lottery_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `config` json NULL DEFAULT NULL COMMENT '配置',
  `other` json NULL DEFAULT NULL,
  `start_time` datetime NULL DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `style` int(1) DEFAULT '1' COMMENT '样式 1-九宫格 2-刮刮乐',
  `integral` int(11) DEFAULT '0' COMMENT '消耗积分',
  `limit` int(4) DEFAULT '0' COMMENT '每日上限',
  `number` int(11) DEFAULT '0' COMMENT '次数',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `status` bit(1) DEFAULT b'0' COMMENT '0-不启用 1-启用',
  `type` int(1) DEFAULT '0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='小程序抽奖';

-- ----------------------------
-- Table structure for jiecheng_lottery_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_lottery_log`;
CREATE TABLE `jiecheng_lottery_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int(1) DEFAULT '0',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '状态 1-未领取 2-已领取 3-已放弃',
  `info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '说明',
  `receive` int(1) DEFAULT '1' COMMENT '1-已经领取 2-未领取',
  `style` int(1) NULL DEFAULT NULL,
  `number` int(11) DEFAULT '1' COMMENT '数量',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `lottery_id` (`uniqid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='抽奖记录';

-- ----------------------------
-- Table structure for jiecheng_lottery_logistics
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_lottery_logistics`;
CREATE TABLE `jiecheng_lottery_logistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `lottery_log_id` int(11) NULL DEFAULT NULL,
  `type` int(1) NULL DEFAULT NULL COMMENT '1-快递 2-自提',
  `goods_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `address` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `logistics` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递单号',
  `express_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司CODE',
  `status` int(1) DEFAULT '1' COMMENT '1-待发货 2-待收货 3-已收货',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `store_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `lottery_log_id` (`lottery_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='抽奖记录';

-- ----------------------------
-- Table structure for jiecheng_lottery_prize
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_lottery_prize`;
CREATE TABLE `jiecheng_lottery_prize` (
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `limit` int(10) NULL DEFAULT NULL,
  `count` int(10) NULL DEFAULT NULL,
  `number` int(10) NULL DEFAULT NULL,
  `integral` int(10) NULL DEFAULT NULL,
  `amount` decimal(12,2) NULL DEFAULT NULL,
  `commodity` json NULL DEFAULT NULL,
  `value` json NULL DEFAULT NULL,
  `other` json NULL DEFAULT NULL,
  PRIMARY KEY (`uniqid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='抽奖价格';

-- ----------------------------
-- Table structure for jiecheng_mall_base
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_mall_base`;
CREATE TABLE `jiecheng_mall_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `mall_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商场默认数据';

-- ----------------------------
-- Table structure for jiecheng_mall_shop_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_mall_shop_rule`;
CREATE TABLE `jiecheng_mall_shop_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NULL DEFAULT NULL,
  `rule_list` json NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商场权限';

-- ----------------------------
-- Table structure for jiecheng_meituan
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_meituan`;
CREATE TABLE `jiecheng_meituan` (
  `id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `delivery_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mt_peisong_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '美团内部订单号',
  `delivery_distance` int(11) NULL DEFAULT NULL COMMENT '订单配送距离',
  `delivery_fee` double(11,2) NULL DEFAULT NULL COMMENT '运费',
  `pay_amount` double(11,2) NULL DEFAULT NULL COMMENT '实付金额',
  `settlement_mode_code` tinyint(4) NULL DEFAULT NULL COMMENT '结算方式，1、实时结算，2、账期结算；账期结算下包含的支付方式为账期；实时结算下包含的支付方式为余额；后续会扩展多种支付方式',
  `coupon_fee` double(11,1) NULL DEFAULT NULL COMMENT '优惠金额',
  `status` tinyint(1) NULL DEFAULT NULL,
  `courier_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配送员姓名',
  `courier_phone` int(11) NULL DEFAULT NULL COMMENT '配送员电话',
  `cancel_reason_id` int(5) NULL DEFAULT NULL COMMENT '取消原因id',
  `cancel_reason` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '取消原因',
  `predict_delivery_time` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '预计送达时间',
  `exception_id` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '异常ID',
  `exception_code` int(11) NULL DEFAULT NULL COMMENT '订单异常代码',
  `exception_descr` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单异常详细信息',
  `exception_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='美团';

-- ----------------------------
-- Table structure for jiecheng_membership_level
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_membership_level`;
CREATE TABLE `jiecheng_membership_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `level` int(11) NOT NULL COMMENT '会员等级:1-100',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `indate` int(11) NOT NULL COMMENT '有效时长',
  `discount` double(3,1) NOT NULL COMMENT '会员折扣:0.1-9.9, 0:免费',
  `is_cash_upgrade` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否开始累积金额升级',
  `reach_cash` double(10,2) NULL DEFAULT NULL COMMENT '达标金额',
  `is_buy_upgrade` tinyint(2) NOT NULL DEFAULT '0' COMMENT '是否可以购买升级',
  `buy_price` decimal(10,2) NULL DEFAULT NULL COMMENT '购买价格',
  `is_present_money` tinyint(2) NOT NULL DEFAULT '0' COMMENT '开通是否赠送金额',
  `present_money` decimal(10,2) NULL DEFAULT NULL COMMENT '赠送金额',
  `is_present_integer` tinyint(2) NOT NULL DEFAULT '0' COMMENT '开通是否赠送积分',
  `present_integer` int(10) NULL DEFAULT NULL COMMENT '赠送积分',
  `is_present_coupon` tinyint(2) NOT NULL DEFAULT '0' COMMENT '开通是否赠送优惠券',
  `present_coupon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '优惠券id. '',''拼接',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` mediumtext COMMENT '内容',
  `status` tinyint(10) DEFAULT '0' COMMENT '状态:0-未启用,1-启用',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='会员等级';

-- ----------------------------
-- Table structure for jiecheng_membership_level_content
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_membership_level_content`;
CREATE TABLE `jiecheng_membership_level_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `image` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `text` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `is_open` int(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='会员等级内容';

-- ----------------------------
-- Table structure for jiecheng_membership_level_content_relation
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_membership_level_content_relation`;
CREATE TABLE `jiecheng_membership_level_content_relation` (
  `mid` int(11) NOT NULL,
  `cid` int(11) NOT NULL,
  PRIMARY KEY (`mid`,`cid`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='会员关系';

-- ----------------------------
-- Table structure for jiecheng_membership_present
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_membership_present`;
CREATE TABLE `jiecheng_membership_present` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `record_id` int(11) NULL DEFAULT NULL,
  `level` int(11) NULL DEFAULT NULL,
  `present_money` decimal(10,2) NULL DEFAULT NULL,
  `present_integer` int(11) NULL DEFAULT NULL,
  `present_coupon` json NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='会员变更流水';

-- ----------------------------
-- Table structure for jiecheng_membership_record
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_membership_record`;
CREATE TABLE `jiecheng_membership_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `base_in` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更位置:admin(后台),wechata(小程序),wechat(公众号),web',
  `source_type` tinyint(2) NULL DEFAULT NULL COMMENT '来源类型:1-后台变更,2-消费升级,3-购买升级,4-到期',
  `source` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `old_level` int(10) NULL DEFAULT NULL COMMENT '变更前等级',
  `new_level` int(10) NULL DEFAULT NULL COMMENT '变更后等级',
  `level_id` int(11) NULL DEFAULT NULL COMMENT '会员等级id',
  `money` decimal(10,2) NULL DEFAULT NULL COMMENT '支付金额',
  `trade` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内部单号',
  `pay_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '支付流水号',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态:0:未变更,1:已变更',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='会员变更流水';

-- ----------------------------
-- Table structure for jiecheng_merchant
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_merchant`;
CREATE TABLE `jiecheng_merchant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '商户名',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `company` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '所属公司',
  `district` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `longitude` double(10,6) NULL DEFAULT NULL,
  `latitude` double(10,6) NULL DEFAULT NULL,
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `content` text COMMENT '详情',
  `business_license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '营业执照',
  `indate` bigint(20) NULL DEFAULT NULL COMMENT '有效期时间戳: 0:永久有效',
  `type` bit(1) DEFAULT b'0' COMMENT '单多门店: 0:单门店,1:多门店',
  `is_commodity_limit` tinyint(2) DEFAULT '0' COMMENT '是否限制商品发布数量:0:不限制,1:限制',
  `commodity_limit` int(11) DEFAULT '0' COMMENT '限制商品发布数量',
  `apply_id` int(11) DEFAULT '0',
  `audit_type` bit(1) DEFAULT b'0' COMMENT '免审核:0:不免,1:免审核',
  `is_self` tinyint(2) DEFAULT '0' COMMENT '是否自营:0:不是,1:是',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态: 0:未启用,1:正常,2限制,3:删除',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `customer_service_type` int(1) DEFAULT '1' COMMENT '客服类型 1-官方客服 2-手机号码 3-二维码',
  `customer_service_mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '客服电话',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `wx_customer` text COMMENT '微信客服',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_merchant_user` (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户';

-- ----------------------------
-- Table structure for jiecheng_merchant_apply
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_merchant_apply`;
CREATE TABLE `jiecheng_merchant_apply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户',
  `logo` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '手机',
  `company` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '公司',
  `business_license` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `district` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域',
  `address` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `longitude` double(10,6) NULL DEFAULT NULL COMMENT '经度',
  `latitude` double(10,6) NULL DEFAULT NULL COMMENT '纬度',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '审核结果备注',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态: 0:申请中,1:审核通过,2:审核失败',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_merchant_apply_user` (`user_id`) USING BTREE,
  CONSTRAINT `pk_merchant_apply_user` FOREIGN KEY (`user_id`) REFERENCES `jiecheng_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户申请入驻';

-- ----------------------------
-- Table structure for jiecheng_merchant_cash
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_merchant_cash`;
CREATE TABLE `jiecheng_merchant_cash` (
  `merchant_id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `total` decimal(10,2) DEFAULT '0.00',
  `recharge` decimal(10,2) DEFAULT '0.00',
  `withdraw` decimal(10,2) DEFAULT '0.00',
  PRIMARY KEY (`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户金额';

-- ----------------------------
-- Table structure for jiecheng_merchant_customer_service
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_merchant_customer_service`;
CREATE TABLE `jiecheng_merchant_customer_service` (
  `id` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `merchant_id` int(11) NOT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '',
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `merchant_id` (`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户客服';

-- ----------------------------
-- Table structure for jiecheng_merchant_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_merchant_withdraw`;
CREATE TABLE `jiecheng_merchant_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `withdraw` decimal(10,2) NOT NULL,
  `service_charge` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `is_online` tinyint(2) DEFAULT '0' COMMENT '是否线上:0:线上,1:线下',
  `type` int(10) NOT NULL,
  `card` json NULL DEFAULT NULL COMMENT '卡片',
  `serial_num` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_time` datetime NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_merchant_withdraw` (`merchant_id`) USING BTREE,
  CONSTRAINT `pk_merchant_withdraw` FOREIGN KEY (`merchant_id`) REFERENCES `jiecheng_merchant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户提现记录';

-- ----------------------------
-- Table structure for jiecheng_msg_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_msg_template`;
CREATE TABLE `jiecheng_msg_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11)  NOT NULL  DEFAULT '0',
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `channel` tinyint(2) NOT NULL,
  `timing` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `is_base` bit(1) NOT NULL DEFAULT b'0',
  `template_id` int(11) NULL DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_type_channel` (`mall_id`,`type`,`channel`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模版消息';

-- ----------------------------
-- Table structure for jiecheng_my_collect
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_my_collect`;
CREATE TABLE `jiecheng_my_collect` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `commodity_id` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_user_commodity` (`user_id`,`commodity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='团队';

-- ----------------------------
-- Table structure for jiecheng_mytemplate
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_mytemplate`;
CREATE TABLE `jiecheng_mytemplate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `page_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `page_type` tinyint(1) NULL DEFAULT NULL COMMENT ' 1-商城首页 2-商品详情 3-会员中心 4- 自定义页面',
  `page_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `small_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `page_config` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面属性',
  `tag` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `source` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '来源: 1- 小程序 2-公众号',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `class_name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `configure` mediumtext,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户模板';

-- ----------------------------
-- Table structure for jiecheng_new_kid_gift
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_new_kid_gift`;
CREATE TABLE `jiecheng_new_kid_gift` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `new_kid_gift_id` int(11) NULL DEFAULT NULL,
  `amount` decimal(14,2) DEFAULT '0.00',
  `integral` int(10) NULL DEFAULT NULL,
  `coupon_list` json NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `new_kid_gift_id` (`new_kid_gift_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='新人礼物表';

-- ----------------------------
-- Table structure for jiecheng_new_kid_gift_config
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_new_kid_gift_config`;
CREATE TABLE `jiecheng_new_kid_gift_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `config` json NULL DEFAULT NULL,
  `start_time` datetime NULL DEFAULT NULL,
  `end_time` datetime NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `status` int(2) DEFAULT '1' COMMENT '1-开启 2-关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='新人礼物配置';

-- ----------------------------
-- Table structure for jiecheng_notice
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_notice`;
CREATE TABLE `jiecheng_notice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `title` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `type` tinyint(2) NOT NULL COMMENT '展现方式:1:内容,2:链接',
  `content` text NOT NULL,
  `sort` smallint(5) NOT NULL DEFAULT '0',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '0:隐藏,1:显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='消息';

-- ----------------------------
-- Table structure for jiecheng_order
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order`;
CREATE TABLE `jiecheng_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '订单表',
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `order_no` char(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '订单编号',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '下单客户ID',
  `user_address_id` int(11) NULL DEFAULT NULL COMMENT '客户地址',
  `pay_id` tinyint(1) DEFAULT '2' COMMENT '支付方式 1-余额 2-微信 3-支付宝 4-后台支付',
  `status` tinyint(1) DEFAULT '1' COMMENT '订单状态: 1-待支付 2-待发货 3-已发货 4-已完结 5-已评价   6-删除 7-部分发货 8-已收货 9-部分收货 10-维权中 11-维权完成 12-已关闭   ',
  `pay_no` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '第三方支付流水号',
  `money` decimal(11,2) NULL DEFAULT NULL COMMENT '商品总价',
  `type` tinyint(1) DEFAULT '1' COMMENT '1-普通订单 2-维权订单 ',
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `consignee` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人',
  `remake` text COMMENT '备注',
  `iphone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收货人电话号码',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `pay_time` datetime NULL DEFAULT NULL COMMENT '付款时间',
  `unified_money` decimal(11,2) NULL DEFAULT NULL,
  `unified_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内部统一订单号',
  `base_in` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '登录环境:wechat,wechata,web',
  `after_status` tinyint(1) DEFAULT '3' COMMENT '维权状态: 1- 维权中 2-维权完成 3-未维权',
  `after_type` tinyint(1) NULL DEFAULT NULL COMMENT '1-整单维权 2-单品维权',
  `order_type` tinyint(1) DEFAULT '1' COMMENT '1-快递 2-自提 3-同城配送',
  `is_print` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否已经打印',
  `deliver_type` tinyint(1) NULL DEFAULT NULL COMMENT '1-整单发货 2-分包裹发货 ',
  `discount` decimal(11,2) DEFAULT '0.00' COMMENT '优惠金额',
  `discount_amount` decimal(11,2) DEFAULT '0.00' COMMENT '抵扣积分',
  `source` tinyint(1) unsigned DEFAULT '2' COMMENT '来源:1-小程序 2-公众号',
  `is_evaluate` tinyint(4) DEFAULT '0' COMMENT '未评价-0，已评价-1',
  `settlement` tinyint(1) DEFAULT '0' COMMENT '是否入账:0:订单待支付,1:未入账,2:已入账,3:取消',
  `is_rebate` tinyint(1) DEFAULT '0' COMMENT '是否部分退款，0-否，1-是',
  `has_attach` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否含有附加属性',
  `distribution_fee` float NOT NULL DEFAULT '0' COMMENT '同城配送费',
  `discount_level_money` decimal(14,2) DEFAULT '0.00' COMMENT '会员价折扣金额',
  `delivery_time` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '自提时间',
  `order_from` int(11) NOT NULL DEFAULT '0' COMMENT '0普通1拼团订单',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `order_no` (`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单';

-- ----------------------------
-- Table structure for jiecheng_order_after
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_after`;
CREATE TABLE `jiecheng_order_after` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `after_no` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '售后编号',
  `order_id` int(11) NULL DEFAULT NULL,
  `type` tinyint(1) DEFAULT '2' COMMENT '1-仅退款 2-退货退款 3-换货',
  `explain` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款说明',
  `reason` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退款原因',
  `money` double(11,2) NULL DEFAULT NULL COMMENT '退款金额',
  `imgs` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退货凭证',
  `express_num` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '买家退货快递单号',
  `express_company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `return_time` datetime NULL DEFAULT NULL COMMENT '退货时间',
  `reamke` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `state` tinyint(1) DEFAULT '1' COMMENT '1-等待商家处理 2-同意申请 3-驳回申请 4-商家已发货，等待客户收货 5-手动退款  6-确认发货 7-退款退货完成 8-退款完成 9-换货完成 10-退回物品,等待退款  11-同意退款 12-等待买家退回商品 13-买家退回物品，等待退款 14-买家退回物品，等待商家重新发货 ',
  `deliver_num` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商家发货快递号',
  `deliver_company` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商家发货快递公司',
  `deliver_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `after_address_id` tinyint(1) NULL DEFAULT NULL COMMENT '退货地址ID',
  `reject_remake` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '驳回备注',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='售后';

-- ----------------------------
-- Table structure for jiecheng_order_agent_profit
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_agent_profit`;
CREATE TABLE `jiecheng_order_agent_profit` (
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `order_no` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `commodity_id` int(11) NULL DEFAULT NULL,
  `agent1` int(11) NULL DEFAULT NULL,
  `agent2` int(11) NULL DEFAULT NULL,
  `agent3` int(11) NULL DEFAULT NULL,
  `amount1` decimal(11,2) NULL DEFAULT NULL,
  `amount2` decimal(11,2) NULL DEFAULT NULL,
  `amount3` decimal(11,2) NULL DEFAULT NULL,
  PRIMARY KEY (`uniqid`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单佣金';

-- ----------------------------
-- Table structure for jiecheng_order_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_commodity`;
CREATE TABLE `jiecheng_order_commodity` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL COMMENT '订单-商品关联表',
  `commodity_id` int(11) NULL DEFAULT NULL,
  `count` int(11) NULL DEFAULT NULL COMMENT '下单数量',
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `status` tinyint(1) DEFAULT '1' COMMENT '订单状态: 1-待支付 2-待发货 3-已发货 4-已完结 5-已评价   6-删除 7-已收货 8-维权中 9-已维权 10-维权驳回 11-已关闭   ',
  `money` decimal(8,2) NULL DEFAULT NULL COMMENT '原来金额',
  `discount` decimal(8,2) NULL DEFAULT NULL COMMENT '实际支付金额',
  `sku_id` int(11) NULL DEFAULT NULL COMMENT 'skuID',
  `store_id` int(11) NULL DEFAULT NULL COMMENT '门店ID',
  `logistics_no` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '物流单号',
  `express_company` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '快递公司',
  `type` tinyint(4) DEFAULT '1' COMMENT '1-快递 2-自提',
  `receiving_time` datetime NULL DEFAULT NULL COMMENT '收货时间',
  `deliver_time` datetime NULL DEFAULT NULL COMMENT '发货时间',
  `return_amount` decimal(8,2) DEFAULT '0.00' COMMENT '退货金额',
  `is_out_sell` tinyint(2) DEFAULT '0' COMMENT '是否超出限购:0:没超出,1:超出',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `after_type` tinyint(4) NULL DEFAULT NULL COMMENT '维权类型:1-仅退款 2-退货退款 3-换货',
  `after_status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '3' COMMENT '维权状态: 1- 维权中 2-已维权 3-未维权 4-维权已驳回',
  `price` decimal(10,2) DEFAULT '0.00' COMMENT '商品单价',
  `is_print` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否已经打印',
  `discount_amount` decimal(11,2) DEFAULT '0.00' COMMENT '抵扣积分',
  `retreat` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '退换货类型 1-退款 2-退货退款 3-换货',
  `is_evaluate` tinyint(4) DEFAULT '0' COMMENT '未评价-0，已评价-1',
  `before_member_price` decimal(10,2) NULL DEFAULT NULL COMMENT '计算会员价前的价格',
  `attach_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '附加属性值',
  `group` int(10) NOT NULL DEFAULT '-1' COMMENT '属性分组',
  `gc_id` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT 'group_commodity的id,层级',
  `collage_id` int(11) NOT NULL DEFAULT '0' COMMENT '团id',
  `collage_item_id` int(11) NOT NULL DEFAULT '0' COMMENT '团员id',
  `sc_id` int(11) NOT NULL DEFAULT '0' COMMENT 'seckill_commodity的id',
  `pc_id` int(11) NOT NULL DEFAULT '0' COMMENT 'presell_commodity的id',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `commodity_id` (`commodity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='子订单';

-- ----------------------------
-- Table structure for jiecheng_order_commodity_attach
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_commodity_attach`;
CREATE TABLE `jiecheng_order_commodity_attach` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL COMMENT '订单-商品关联表',
  `sku_id` int(11) DEFAULT '0',
  `group` int(11) NOT NULL DEFAULT '0' COMMENT '分组',
  `commodity_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `attach_id` int(11) NULL DEFAULT NULL COMMENT '附加属性值id',
  `attach_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '附加属性名',
  `num` int(11) NOT NULL DEFAULT '1' COMMENT '购买数量',
  `attach_price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '附加属性单价',
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '0未付款1已付款',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE,
  KEY `commodity_id` (`commodity_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='子订单属性';

-- ----------------------------
-- Table structure for jiecheng_order_evaluate
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_evaluate`;
CREATE TABLE `jiecheng_order_evaluate` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `commodity_id` int(11) NULL DEFAULT NULL,
  `content` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评价内容',
  `img_url` text COMMENT '图片地址',
  `state` tinyint(1) DEFAULT '1' COMMENT '1-未审核 2-已审核 3-驳回',
  `score` int(11) DEFAULT '10' COMMENT '评分 1-10分',
  `remake` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `reply` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '掌柜回复',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `sku` int(11) NULL DEFAULT NULL COMMENT 'sku表ID',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='评价';

-- ----------------------------
-- Table structure for jiecheng_order_freight
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_freight`;
CREATE TABLE `jiecheng_order_freight` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `order_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `freight` decimal(8,2) NULL DEFAULT NULL COMMENT '运费',
  `cids` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '商品ID组合-1,2,3,4',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `order_id` (`order_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单运费';

-- ----------------------------
-- Table structure for jiecheng_pay_mode
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_pay_mode`;
CREATE TABLE `jiecheng_pay_mode` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mode` tinyint(2) NOT NULL COMMENT '支付方式:1:微信,2:支付宝',
  `type` tinyint(2) NOT NULL COMMENT '支付类型: 微信:1:微信支付,2:微信支付子商户,3:借用微信支付,4:借用微信子商户,支付宝:1:支付宝2.0',
  `configuration` json NOT NULL COMMENT '配置信息详情',
  `cert` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `wxkey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `rootca` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `M_cert` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `M_wxkey` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='支付';

-- ----------------------------
-- Table structure for jiecheng_pay_type
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_pay_type`;
CREATE TABLE `jiecheng_pay_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` tinyint(2) NULL DEFAULT NULL COMMENT '状态: 0:未启用,1:正常,2:删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='支付方式字典';

-- ----------------------------
-- Table structure for jiecheng_pic
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_pic`;
CREATE TABLE `jiecheng_pic` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `pic_group_id` int(11) NULL DEFAULT NULL,
  `pic_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片昵称',
  `pic_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图片路径',
  `pic_size` double NULL DEFAULT NULL COMMENT '图片大小 M',
  `width` int(11) NULL DEFAULT NULL,
  `height` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='图片限量';

-- ----------------------------
-- Table structure for jiecheng_pic_count
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_pic_count`;
CREATE TABLE `jiecheng_pic_count` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `count` decimal(11,2) NULL DEFAULT NULL COMMENT '以M为单位',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `merchant_id` (`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='图片总量';

-- ----------------------------
-- Table structure for jiecheng_pic_group
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_pic_group`;
CREATE TABLE `jiecheng_pic_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `count` int(11) DEFAULT '0' COMMENT '上传图片数量',
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='图片分组';

-- ----------------------------
-- Table structure for jiecheng_protocol
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_protocol`;
CREATE TABLE `jiecheng_protocol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '协议类型:1:分销协议',
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `subhead` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `content` longtext NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='文章';

-- ----------------------------
-- Table structure for jiecheng_receipts_printer
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_receipts_printer`;
CREATE TABLE `jiecheng_receipts_printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `printer_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `printer_secret` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '打印机秘钥',
  `client_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `client_secret` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL COMMENT '删除时间',
  `default` int(1) DEFAULT '0' COMMENT '默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='打印';

-- ----------------------------
-- Table structure for jiecheng_receipts_task
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_receipts_task`;
CREATE TABLE `jiecheng_receipts_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `store_id` int(11) NULL DEFAULT NULL COMMENT '门店ID',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` int(11) NOT NULL,
  `configure` json NULL DEFAULT NULL,
  `template_id` int(11) NULL DEFAULT NULL,
  `printer_id` int(11) NULL DEFAULT NULL,
  `number` int(2) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='门店打印';

-- ----------------------------
-- Table structure for jiecheng_receipts_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_receipts_template`;
CREATE TABLE `jiecheng_receipts_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '名称',
  `config` json NULL DEFAULT NULL COMMENT '配置',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `default` int(1) DEFAULT '0' COMMENT '默认',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='打印模版';

-- ----------------------------
-- Table structure for jiecheng_recharge_record
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_recharge_record`;
CREATE TABLE `jiecheng_recharge_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `base_in` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '变更位置:admin(后台),wechata(小程序),wechat(公众号),web',
  `source_type` tinyint(2) NULL DEFAULT NULL COMMENT '来源类型:0:消费,1:充值,2:提现,3:分销提现至余额,4:商户提现至余额,5:后台变更,6:退款 7签到获得 8:领取新人礼物 9:开通会员等级,10分销达标,13资产转赠',
  `source` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '来源',
  `is_online` tinyint(2) DEFAULT '0' COMMENT '是否线上:0:是,1:不是',
  `type` tinyint(1) DEFAULT '0' COMMENT '类型:0:新增,1:减少,2:变更为',
  `card` json NULL DEFAULT NULL COMMENT '线下卡片',
  `money` decimal(10,2) DEFAULT '0.00' COMMENT '变更金额',
  `service_charge` decimal(10,2) DEFAULT '0.00' COMMENT '手续费',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `trade` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `old_cash` decimal(10,2) NULL DEFAULT NULL COMMENT '变更前余额',
  `new_cash` decimal(10,2) NULL DEFAULT NULL COMMENT '变更后余额',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态:0:未变更,1:已变更',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='金额变更';

-- ----------------------------
-- Table structure for jiecheng_recommend
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_recommend`;
CREATE TABLE `jiecheng_recommend` (
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mall_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `config_id` int(11) NOT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '1-启用 2-停止',
  PRIMARY KEY (`uniqid`) USING BTREE,
  KEY `commodity` (`commodity_id`) USING BTREE,
  KEY `mall_id` (`mall_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='推荐';

-- ----------------------------
-- Table structure for jiecheng_recommend_config
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_recommend_config`;
CREATE TABLE `jiecheng_recommend_config` (
  `uniqid` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `number` int(11) DEFAULT '0',
  `is_repeat` int(11) DEFAULT '1' COMMENT '1-重复 2-不重复',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(1) DEFAULT '1' COMMENT '1-启动 2-禁用',
  PRIMARY KEY (`uniqid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='推荐配置';

-- ----------------------------
-- Table structure for jiecheng_recommend_goods
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_recommend_goods`;
CREATE TABLE `jiecheng_recommend_goods` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `content` text,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='推荐商品';

-- ----------------------------
-- Table structure for jiecheng_recommend_menu
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_recommend_menu`;
CREATE TABLE `jiecheng_recommend_menu` (
   `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
   `mall_id` int(11) DEFAULT NULL,
   `content` text COMMENT '设置的内容',
   `create_time` datetime DEFAULT NULL,
   `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='悬浮菜单设置';

-- ----------------------------
-- Table structure for jiecheng_renovation
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_renovation`;
CREATE TABLE `jiecheng_renovation` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL COMMENT '商户ID',
  `page_name` varchar(120) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面名称',
  `page_type` tinyint(1) NULL DEFAULT NULL COMMENT ' 1-商城首页 2-商品详情 3-会员中心 4- 自定义页面',
  `status` tinyint(4) DEFAULT '1' COMMENT '1-未应用 2-应用中 ',
  `small_img` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩列图',
  `page_key` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'DIY配置key',
  `qrcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '二维码路径',
  `page_config` text COMMENT '页面属性',
  `source` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '来源: 1- 小程序 2-公众号',
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `create_time` datetime NULL DEFAULT NULL,
  `configure` mediumtext COMMENT '页面装修数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商城DIY配置';

-- ----------------------------
-- Table structure for jiecheng_return_address
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_return_address`;
CREATE TABLE `jiecheng_return_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `linkman` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `phone` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `postcode` int(8) NULL DEFAULT NULL,
  `district` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='地址';

-- ----------------------------
-- Table structure for jiecheng_rise
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_rise`;
CREATE TABLE `jiecheng_rise` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `rise` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '抬头',
  `is_default` tinyint(1) DEFAULT '0' COMMENT '是否默认,1是默认,0普通',
  `duty_paragraph` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '税号',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话号码',
  `deposit_bank` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '开户银行',
  `bank_account` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '银行账户',
  `inoice_type` tinyint(1) DEFAULT '1' COMMENT '抬头类型:1-个人 2-单位',
  `mailbox` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='缴税';

-- ----------------------------
-- Table structure for jiecheng_role
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_role`;
CREATE TABLE `jiecheng_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `department_id` int(10) NULL DEFAULT NULL COMMENT '部门id',
  `is_root` bit(1) NOT NULL DEFAULT b'0',
  `status` bit(1) DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色';

-- ----------------------------
-- Table structure for jiecheng_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_role_rule`;
CREATE TABLE `jiecheng_role_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_rule` (`rule_id`) USING BTREE,
  KEY `pk_role` (`role_id`) USING BTREE,
  CONSTRAINT `jiecheng_role_rule_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `jiecheng_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jiecheng_role_rule_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `jiecheng_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色-权限对应';

-- ----------------------------
-- Table structure for jiecheng_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_rule`;
CREATE TABLE `jiecheng_rule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名',
  `group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限分组',
  `resource_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名',
  `resource` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源',
  `level` int(11) NULL DEFAULT NULL,
  `up` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` bit(1) DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限';

-- ----------------------------
-- Table structure for jiecheng_rule_extend
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_rule_extend`;
CREATE TABLE `jiecheng_rule_extend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NULL DEFAULT NULL,
  `rule_route` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限';


-- ----------------------------
-- Table structure for jiecheng_lessee
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_lessee`;
CREATE TABLE `jiecheng_lessee` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `site` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '站点名称',
  `meal_id` int(11) NULL DEFAULT NULL,
  `addons` json NULL DEFAULT NULL,
  `binding` datetime NULL DEFAULT NULL COMMENT '限制到期',
  `type` tinyint(2) DEFAULT '0' COMMENT '有效期类型:0:限制,1:永久有效',
  `indate` datetime NULL DEFAULT NULL COMMENT '有效期',
  `apply_id` int(11) DEFAULT '0',
  `versions` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '版本号',
  `entry_type` json NULL DEFAULT NULL COMMENT '前端数组：1-小程序，2-公众号，3-h5',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态: 0:未启用,1:正常,2:过期,3:限制,4:删除',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `oss_size` int(11) DEFAULT '5242880' COMMENT '可上传oss大小,单位为Kb',
  `withhold_password_switch` tinyint(1) NOT NULL DEFAULT 1 COMMENT '1=开启 0=关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='租户';

-- ----------------------------
-- Table structure for jiecheng_search_keyword
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_search_keyword`;
CREATE TABLE `jiecheng_search_keyword` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='热搜';

-- ----------------------------
-- Table structure for jiecheng_shop_admin
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_admin`;
CREATE TABLE `jiecheng_shop_admin` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '管理员id',
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL COMMENT '商户id',
  `username` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机',
  `role_id` int(10) NOT NULL COMMENT '角色id',
  `is_root` bit(1) NOT NULL DEFAULT b'0',
  `status` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '状态: 0:未启用,1:正常,2:限制,3:删除',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '姓名',
  `ac_token` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '登录token',
  `login_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '登录ip',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_username` (`username`) USING BTREE,
  UNIQUE KEY `Index_mobile` (`mobile`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='后台管理员';

-- ----------------------------
-- Table structure for jiecheng_shop_assistant
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_assistant`;
CREATE TABLE `jiecheng_shop_assistant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `store_id` int(11) NOT NULL COMMENT '门店id',
  `serial_num` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '店员编号',
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_store_user` (`mall_id`,`merchant_id`,`user_id`,`store_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='店员';

-- ----------------------------
-- Table structure for jiecheng_shop_configuration
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_configuration`;
CREATE TABLE `jiecheng_shop_configuration` (
  `type` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `configuration` json NOT NULL,
  `status` bit(1) NOT NULL DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`type`,`merchant_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户配置';

-- ----------------------------
-- Table structure for jiecheng_shop_department
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_department`;
CREATE TABLE `jiecheng_shop_department` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名',
  `merchant_id` int(11) NOT NULL,
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态: 0:未启用,1:正常,2:限制,3:删除',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='部门';

-- ----------------------------
-- Table structure for jiecheng_shop_role
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_role`;
CREATE TABLE `jiecheng_shop_role` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名',
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `department_id` int(10) NULL DEFAULT NULL COMMENT '部门id',
  `is_root` bit(1) NOT NULL DEFAULT b'0',
  `status` bit(1) DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色';

-- ----------------------------
-- Table structure for jiecheng_shop_role_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_role_rule`;
CREATE TABLE `jiecheng_shop_role_rule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `rule_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_rule` (`rule_id`) USING BTREE,
  KEY `pk_role` (`role_id`) USING BTREE,
  CONSTRAINT `jiecheng_shop_role_rule_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `jiecheng_shop_role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `jiecheng_shop_role_rule_ibfk_2` FOREIGN KEY (`rule_id`) REFERENCES `jiecheng_shop_rule` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='角色-权限对应';

-- ----------------------------
-- Table structure for jiecheng_shop_rule
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_rule`;
CREATE TABLE `jiecheng_shop_rule` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限名',
  `group` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限分组',
  `resource_name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源名',
  `resource` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '资源',
  `level` int(11) NULL DEFAULT NULL,
  `up` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` bit(1) DEFAULT b'1' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='权限';

-- ----------------------------
-- Table structure for jiecheng_shop_rule_extend
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_shop_rule_extend`;
CREATE TABLE `jiecheng_shop_rule_extend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rule_id` int(11) NULL DEFAULT NULL,
  `rule_route` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `method` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户权限';

-- ----------------------------
-- Table structure for jiecheng_sign_in
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_sign_in`;
CREATE TABLE `jiecheng_sign_in` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `integral` int(12) DEFAULT '0' COMMENT '积分',
  `amount` decimal(14,2) DEFAULT '0.00' COMMENT '余额',
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `continuity` int(11) DEFAULT '0' COMMENT '连续天数',
  `mall_id` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC COMMENT='积分';

-- ----------------------------
-- Table structure for jiecheng_sku
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_sku`;
CREATE TABLE `jiecheng_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `commodity_id` int(11) NOT NULL,
  `code` VARCHAR(255) NOT NULL DEFAULT '' COMMENT '商品编码',
  `weight` double NULL DEFAULT NULL COMMENT '重量',
  `pvs` text NOT NULL,
  `pvs_value` text,
  `pic` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='sku';

-- ----------------------------
-- Table structure for jiecheng_sku_inventory
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_sku_inventory`;
CREATE TABLE `jiecheng_sku_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `sku_id` int(11) NOT NULL,
  `original_price` decimal(10,2) DEFAULT '0.00',
  `sell_price` decimal(10,2) NOT NULL,
  `cost_price` DECIMAL(10,2) NOT NULL DEFAULT '0' COMMENT '成本价',
  `total` int(11) NOT NULL,
  `sell` int(11) unsigned DEFAULT '0',
  `status` bit(1) NOT NULL DEFAULT b'0',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `level_price` json NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_sku_inventory` (`sku_id`) USING BTREE,
  CONSTRAINT `pk_sku_inventory` FOREIGN KEY (`sku_id`) REFERENCES `jiecheng_sku` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商品库存';

-- ----------------------------
-- Table structure for jiecheng_store
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_store`;
CREATE TABLE `jiecheng_store` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NOT NULL COMMENT '归属商户',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '门店名',
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机',
  `district` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '区域',
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '地址',
  `longitude` double(10,6) NOT NULL COMMENT '经度',
  `latitude` double(10,6) NOT NULL COMMENT '纬度',
  `business_hours` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `remark` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `audit` tinyint(1) NOT NULL DEFAULT '0' COMMENT '审核状态:0:申请中,1:通过,2:不通过',
  `audit_remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `status` bit(1) NOT NULL DEFAULT b'0' COMMENT '状态: 0:未启用,1:正常',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='门店';

-- ----------------------------
-- Table structure for jiecheng_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_template`;
CREATE TABLE `jiecheng_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `page_type` tinyint(1) NULL DEFAULT NULL COMMENT ' 1-商城首页 2-商品详情 3-会员中心 4- 自定义页面',
  `small_img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '缩略图',
  `page_key` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '存储key',
  `page_config` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '页面属性',
  `source` varchar(150) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '2' COMMENT '来源: 1- 小程序 2-公众号',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '分类名称',
  `configure` mediumtext COMMENT '页面装修数据',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模板市场';

-- ----------------------------
-- Table structure for jiecheng_total
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_total`;
CREATE TABLE `jiecheng_total` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `uv` int(11) NULL DEFAULT NULL,
  `pv` int(11) NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='浏览';

-- ----------------------------
-- Table structure for jiecheng_transport
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_transport`;
CREATE TABLE `jiecheng_transport` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='同城配送';

-- ----------------------------
-- Table structure for jiecheng_user
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user`;
CREATE TABLE `jiecheng_user` (
  `id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `nickname` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `picurl` text,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mobile` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `agent_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `status` tinyint(2) DEFAULT '1' COMMENT '1-正常 2-拉黑 3-删除',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `pid` int(11) DEFAULT '0' COMMENT '父ID',
  `is_merge` tinyint(1) NULL DEFAULT NULL COMMENT '状态: 0-',
  `subscribe` tinyint(1) NULL DEFAULT NULL COMMENT '1-关注 0-未关注',
  `is_success` tinyint(1) NULL DEFAULT NULL COMMENT '是否已合并：0-未合并 1-已合并',
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'H5登录密码',
  `level` int(11) DEFAULT '0' COMMENT '会员等级',
  `level_time` datetime NULL DEFAULT NULL COMMENT '会员时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `mall_id` (`mall_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户';

-- ----------------------------
-- Table structure for jiecheng_user_address
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_address`;
CREATE TABLE `jiecheng_user_address` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `address` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `consignee` varchar(80) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '收件人',
  `iphone` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '电话',
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `type` tinyint(1) DEFAULT '1' COMMENT '设置默认地址:2默认 1普通',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='地址';

-- ----------------------------
-- Table structure for jiecheng_user_card
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_card`;
CREATE TABLE `jiecheng_user_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `resource` json NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  `receipt_img` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='会员卡';

-- ----------------------------
-- Table structure for jiecheng_user_cash
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_cash`;
CREATE TABLE `jiecheng_user_cash` (
  `user_id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `total` decimal(10,2) DEFAULT '0.00' COMMENT '余额',
  `recharge` decimal(10,2) DEFAULT '0.00' COMMENT '充值金额',
  `expense` decimal(10,2) DEFAULT '0.00' COMMENT '消费金额',
  `withdraw` decimal(10,2) DEFAULT '0.00' COMMENT '已提现金额',
  `agent_total` decimal(10,2) DEFAULT '0.00' COMMENT '分销总金额',
  `agent` decimal(10,2) DEFAULT '0.00' COMMENT '分销未提现金额',
  `agent_withdraw` decimal(10,2) DEFAULT '0.00' COMMENT '分销已提现金额',
  `status` bit(1) DEFAULT b'1',
  `integral_total` int(11) DEFAULT '0' COMMENT '总积分',
  `integral` int(11) DEFAULT '0' COMMENT '积分',
  `integral_withdraw` int(11) DEFAULT '0' COMMENT '未消费积分',
  `assessment_total` decimal(10,2) NULL DEFAULT 0.00 COMMENT '考核总金额',
  `assessment` decimal(10,2) NULL DEFAULT 0.00 COMMENT '考核未提现金额',
  `assessment_withdraw` decimal(10,2) NULL DEFAULT 0.00 COMMENT '考核已提现金额',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  CONSTRAINT `pk_user_cash` FOREIGN KEY (`user_id`) REFERENCES `jiecheng_user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='账户';

-- ----------------------------
-- Table structure for jiecheng_user_coupon
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_coupon`;
CREATE TABLE `jiecheng_user_coupon` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `coupon_id` int(11) NULL DEFAULT NULL,
  `order_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `unified_order_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `used_type` tinyint(2) DEFAULT '1',
  `type` tinyint(2) DEFAULT '0' COMMENT '获取方式:0-前台领取,1-后台发放',
  `status` tinyint(2) DEFAULT '0' COMMENT '状态:0:未使用,1:已使用,2:已过期',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='用户优惠券';

-- ----------------------------
-- Table structure for jiecheng_user_lottery
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_lottery`;
CREATE TABLE `jiecheng_user_lottery` (
  `user_id` int(11) NOT NULL,
  `lottery_config_id` int(11) NOT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `number` int(11) DEFAULT '0' COMMENT '次数',
  `get_number` int(11) DEFAULT '0' COMMENT '已抽奖次数',
  `added_number` int(11) DEFAULT '0' COMMENT '额外次数',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `end_time` datetime NULL DEFAULT NULL,
  `status` int(1) DEFAULT '1' COMMENT '1-未使用 2-已使用 3-已失效',
  PRIMARY KEY (`user_id`,`lottery_config_id`) USING BTREE,
  KEY `mall_id` (`mall_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='用户抽奖';

-- ----------------------------
-- Table structure for jiecheng_user_lottery_record
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_lottery_record`;
CREATE TABLE `jiecheng_user_lottery_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `lottery_id` int(11) NOT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `number` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='用户抽奖记录';

-- ----------------------------
-- Table structure for jiecheng_user_wx
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_user_wx`;
CREATE TABLE `jiecheng_user_wx` (
  `user_id` int(11) NULL DEFAULT NULL,
  `mall_id` int(11) NULL DEFAULT NULL,
  `openid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `unionid` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` tinyint(1) DEFAULT '1' COMMENT '1-小程序  2-公众号',
  `sex` tinyint(1) NULL DEFAULT NULL COMMENT '性别: 1-男 2-女',
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='微信openid';

-- ----------------------------
-- Table structure for jiecheng_uvpv
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_uvpv`;
CREATE TABLE `jiecheng_uvpv` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) DEFAULT '0',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL,
  `details` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='配置';

-- ----------------------------
-- Table structure for jiecheng_visit
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_visit`;
CREATE TABLE `jiecheng_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `agent_id` int(11) NULL DEFAULT NULL,
  `ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `mode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `merchant_id` int(11) NULL DEFAULT NULL,
  `is_fission` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='访问';

-- ----------------------------
-- Table structure for jiecheng_wechat_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wechat_template`;
CREATE TABLE `jiecheng_wechat_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `scene` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `template_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '微信模板id',
  `first` json NOT NULL COMMENT '头部对象',
  `attr` json NULL DEFAULT NULL COMMENT '变量对象',
  `remark` json NULL DEFAULT NULL COMMENT '尾部对象',
  `status` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模版';

-- ----------------------------
-- Table structure for jiecheng_wechata_plugins
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wechata_plugins`;
CREATE TABLE `jiecheng_wechata_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `plugins` text,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='小程序插件';

-- ----------------------------
-- Table structure for jiecheng_wechata_qr
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wechata_qr`;
CREATE TABLE `jiecheng_wechata_qr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `wea_key` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `qr_data` json NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='小程序码';

-- ----------------------------
-- Table structure for jiecheng_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_withdraw`;
CREATE TABLE `jiecheng_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `withdraw` decimal(10,2) NOT NULL,
  `service_charge` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `is_online` tinyint(2) DEFAULT '0' COMMENT '是否线上:1:线上,0:线下',
  `type` int(10) NOT NULL,
  `card` json NULL DEFAULT NULL COMMENT '卡片',
  `serial_num` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_no` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pay_time` datetime NULL DEFAULT NULL,
  `receipt_img` varchar(500) NOT NULL DEFAULT '',
  `create_time` datetime NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='商户提现记录';

-- ----------------------------
-- Table structure for jiecheng_wx_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wx_template`;
CREATE TABLE `jiecheng_wx_template` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `channel` int(10) NOT NULL DEFAULT '1' COMMENT '1:微信，2:小程序',
  `value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `kid_list` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '1,2,3,4,5' COMMENT '模板关键词列表',
  `example` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='模版';

-- ----------------------------
-- Table structure for jiecheng_wx_company
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wx_company`;
CREATE TABLE `jiecheng_wx_company` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业名',
  `corp_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '企业id',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='微信企业设置';

-- ----------------------------
-- Table structure for jiecheng_wx_customer
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wx_customer`;
CREATE TABLE `jiecheng_wx_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服名',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '客服链接',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 COMMENT='微信企业客服设置';
-- ----------------------------
-- Table structure for jiecheng_wxa_message
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_wxa_message`;
CREATE TABLE `jiecheng_wxa_message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `template_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `count` int(11) DEFAULT '1',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `Index_user_template` (`user_id`,`template_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='小程序消息';

-- ----------------------------
-- Table structure for jiecheng_mall_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_mall_log`;
CREATE TABLE `jiecheng_mall_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL DEFAULT '' COMMENT '报错地址',
  `data` text,
  `message` text NOT NULL COMMENT '报错内容',
  `time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='mall端日志';

-- ----------------------------
-- Table structure for jiecheng_order_form_template
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_form_template`;
CREATE TABLE `jiecheng_order_form_template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
  `config` json NOT NULL COMMENT '配置',
  `image` varchar(255) NOT NULL DEFAULT '' COMMENT '图片',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '状态 0= 未启用 1=启用',
  `source` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=下单表单 1=商品表单',
  `merchant_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单表单';

-- ----------------------------
-- Table structure for jiecheng_order_form_answer
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_form_answer`;
CREATE TABLE `jiecheng_order_form_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `template_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `answer` json NOT NULL,
  `order_id` int(11) NOT NULL DEFAULT '0',
  `source` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=下单表单 1=商品表单',
  `commodity_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `user_id` (`user_id`) USING BTREE,
  KEY `form_template_id` (`template_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='订单自定义表单';

-- ----------------------------
-- Table structure for jiecheng_order_money_change
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_order_money_change`;
CREATE TABLE `jiecheng_order_money_change` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `merchant_id` int(11) NOT NULL DEFAULT '0',
  `order_no` varchar(50) NOT NULL DEFAULT '',
  `order_id` int(11) NOT NULL DEFAULT '0',
  `money` float(11,2) NOT NULL DEFAULT '0.00' COMMENT '变动金额',
  `old_money` float(11,2) NOT NULL DEFAULT '0.00' COMMENT '改价前金额',
  `new_money` float(11,2) NOT NULL DEFAULT '0.00' COMMENT '改动后金额',
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `type` tinyint(11) NOT NULL DEFAULT '0',
  `time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='改价';

-- ----------------------------
-- Table structure for jiecheng_live_good
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_live_good`;
CREATE TABLE `jiecheng_live_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播商品库id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '商品id',
  `audit_id` int(11) NOT NULL DEFAULT '0' COMMENT '审核id',
  `cover_img_url` varchar(255) NOT NULL DEFAULT '' COMMENT '封面',
  `cover_img_media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '封面media id',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '审核状态 0：未审核。1：审核中，2：审核通过，3：审核驳回',
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `price_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '价格类型，1：一口价（只需要传入price，price2不传） 2：价格区间（price字段为左边界，price2字段为右边界，price和price2必传） 3：显示折扣价（price字段为原价，price2字段为现价， price和price2必传）',
  `price` decimal(11,2) NOT NULL DEFAULT '0.00',
  `price2` decimal(11,2) NOT NULL DEFAULT '0.00',
  `update_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`,`room_goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='直播';

-- ----------------------------
-- Table structure for jiecheng_live_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_live_log`;
CREATE TABLE `jiecheng_live_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `room_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播商品库id',
  `desc` json DEFAULT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`,`room_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='直播日志';

-- ----------------------------
-- Table structure for jiecheng_live_room
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_live_room`;
CREATE TABLE `jiecheng_live_room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播间id',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '直播间标题',
  `anchor_name` varchar(255) NOT NULL DEFAULT '' COMMENT '主播昵称',
  `start_time` datetime DEFAULT NULL COMMENT '开始时间',
  `end_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '结束时间',
  `status` int(11) NOT NULL DEFAULT '102' COMMENT '状态 101：直播中   102：未开始   103：已结束  104：禁播   105：暂停  106：异常   107：已过期',
  `cover_img` varchar(255) NOT NULL DEFAULT '' COMMENT '背景图',
  `media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '背景图 media id',
  `anchor_wechat` varchar(255) NOT NULL DEFAULT '' COMMENT '主播微信号',
  `share_img` varchar(255) NOT NULL DEFAULT '' COMMENT '分享图',
  `share_img_media_id` varchar(255) NOT NULL DEFAULT '' COMMENT '分享图media id',
  `room_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '直播类型 0手机直播 1推流',
  `screen_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '横屏、竖屏 【1：横屏，0：竖屏】（横屏：视频宽高比为16:9、4:3、1.85:1 ；竖屏：视频宽高比为9:16、2:3）',
  `close_like` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭点赞 1是0否',
  `close_goods` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭货架 1是0否',
  `close_comment` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭评论 1是0否',
  `is_delete` tinyint(1) NOT NULL DEFAULT '0',
  `is_hide` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `is_feeds_public` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否开启官方收录 【1: 开启，0：关闭】',
  `close_replay` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭回放 【0：开启，1：关闭】',
  `close_share` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否关闭分享',
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`,`room_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='直播间';

-- ----------------------------
-- Table structure for jiecheng_live_room_has_good
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_live_room_has_good`;
CREATE TABLE `jiecheng_live_room_has_good` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播商品库表id',
  `goods_id` int(11) NOT NULL DEFAULT '0' COMMENT '直播商品ID 是第三方的id',
  `room_id` int(11) NOT NULL DEFAULT '0' COMMENT 'live_room表id',
  `pv` int(11) NOT NULL DEFAULT '0',
  `is_sale` tinyint(1) NOT NULL DEFAULT '1' COMMENT '上下架 【0：下架，1：上架】',
  `mall_id` int(11) NOT NULL,
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`,`room_goods_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='直播间配置';

-- ----------------------------
-- Table structure for jiecheng_scrapy_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_scrapy_log`;
CREATE TABLE `jiecheng_scrapy_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `desc` json DEFAULT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='采集记录';

-- ----------------------------
-- Table structure for jiecheng_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_log`;
CREATE TABLE `jiecheng_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '请求的地址',
  `action` varchar(255) NOT NULL DEFAULT '' COMMENT '动作',
  `data` json DEFAULT NULL COMMENT '返回的数据',
  `method` varchar(255) NOT NULL DEFAULT '',
  `message` text COMMENT '报错内容',
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `admin_name` varchar(255) NOT NULL DEFAULT '' COMMENT '操作员',
  `request_param` json DEFAULT NULL COMMENT '请求的数据',
  `code` int(11) NOT NULL DEFAULT '200' COMMENT '错误码',
  `title` varchar(255) NOT NULL DEFAULT '',
  `platform` varchar(255) NOT NULL DEFAULT '',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  `ip` varchar(500) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='mall端日志';

-- ----------------------------
-- Table structure for jiecheng_group
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_group`;
CREATE TABLE `jiecheng_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `client_type` varchar(20) NOT NULL DEFAULT '' COMMENT '1微信公众号2小程序3h5',
  `type` int(10) NOT NULL DEFAULT '0' COMMENT '1普通拼团2阶梯拼团',
  `rules` text NOT NULL COMMENT '拼团配置',
  `close_time` datetime DEFAULT NULL COMMENT '手动停止时间',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='拼团';

-- ----------------------------
-- Table structure for jiecheng_group_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_group_commodity`;
CREATE TABLE `jiecheng_group_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL COMMENT '拼团Id',
  `commodity_id` int(11) NOT NULL COMMENT '商品id',
  `has_sku` int(1) NOT NULL DEFAULT '0' COMMENT '1有',
  `activity_stock` int(10) NOT NULL COMMENT '库存',
  `activity_price` varchar(255) NOT NULL DEFAULT '0.00' COMMENT '拼团价',
  `leader_price` decimal(10,2) NOT NULL DEFAULT '-1.00' COMMENT '团长价',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  `type` int(1) NOT NULL DEFAULT '1' COMMENT '1普通2阶梯',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `success_num` varchar(25) NOT NULL DEFAULT '' COMMENT '普通团；[1,2,3...]阶梯团',
  `limit_time` int(50) NOT NULL DEFAULT '0' COMMENT '有效时间',
  `virtual_success` varchar(25) NOT NULL DEFAULT '' COMMENT '模拟人数',
  `use_agent` int(11) NOT NULL DEFAULT '0' COMMENT '分销',
  `use_coupon` int(11) NOT NULL COMMENT '优惠卷',
  `limit_type` varchar(50) NOT NULL DEFAULT '' COMMENT '[选项，活动期内每人最多购买, 活动期内每人每天最多购买]',
  `use_single` int(1) NOT NULL DEFAULT '1' COMMENT '商品单购',
  `show_time` datetime DEFAULT NULL COMMENT '预热时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='拼团商品';

-- ----------------------------
-- Table structure for jiecheng_group_commodity_sku
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_group_commodity_sku`;
CREATE TABLE `jiecheng_group_commodity_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `gc_id` int(11) NOT NULL COMMENT 'group_commodity的id',
  `sku_id` int(11) NOT NULL DEFAULT '0',
  `activity_stock` decimal(10,2) NOT NULL COMMENT '库存',
  `activity_price` varchar(255) NOT NULL DEFAULT '0.00' COMMENT '拼团价',
  `leader_price` decimal(10,2) NOT NULL DEFAULT '-1.00' COMMENT '团长价',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='拼团商品';

-- ----------------------------
-- Table structure for jiecheng_collage
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_collage`;
CREATE TABLE `jiecheng_collage` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `gc_id` int(11) NOT NULL COMMENT 'group_commodity',
  `user_id` int(11) NOT NULL,
  `commodity_id` int(11) NOT NULL,
  `sku_id` int(11) DEFAULT NULL,
  `oc_id` int(11) NOT NULL COMMENT 'order_commodity订单详情id',
  `success_num` int(11) NOT NULL COMMENT '团员人数',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '成团人数',
  `create_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL COMMENT '截团时间',
  `status` int(2) NOT NULL DEFAULT '0' COMMENT '1开团中 2成功 11关闭',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='拼团';

-- ----------------------------
-- Table structure for jiecheng_collage_item
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_collage_item`;
CREATE TABLE `jiecheng_collage_item` (
 `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
 `mall_id` int(11) NOT NULL,
 `gc_id` int(11) NOT NULL COMMENT 'group_commodity',
 `collage_id` int(11) NOT NULL COMMENT 'collage',
 `user_id` varchar(255) NOT NULL DEFAULT '',
 `commodity_id` int(11) NOT NULL,
 `sku_id` int(11) DEFAULT NULL,
 `oc_id` int(11) NOT NULL DEFAULT '0' COMMENT 'order_commodity订单详情id',
 `create_time` datetime DEFAULT NULL,
 `is_robot` int(11) NOT NULL DEFAULT '0' COMMENT '1是机器人凑数',
 PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='团';

-- ----------------------------
-- Table structure for jiecheng_commission_activity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_activity`;
CREATE TABLE `jiecheng_commission_activity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `mall_id` int(11) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `level_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=全部分销商 1=指定分销商',
  `levels` json NOT NULL,
  `condition` json NOT NULL,
  `thumb` varchar(500) NOT NULL DEFAULT '' COMMENT '活动封面',
  `content` text COMMENT '规则介绍',
  `award_stage` json NOT NULL COMMENT '奖励规则及内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=未开始 1=开启 2=暂停 3=结束 4=关闭',
  `create_time` datetime DEFAULT NULL,
  `delete_time` datetime DEFAULT NULL,
  `stop_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='达标';

-- ----------------------------
-- Table structure for jiecheng_commission_activity_award
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_activity_award`;
CREATE TABLE `jiecheng_commission_activity_award` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `activity_id` int(11) NOT NULL DEFAULT '0' COMMENT '活动id',
  `mall_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL DEFAULT '0',
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '分销商id',
  `content` json DEFAULT NULL COMMENT '奖励内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=待发放 1=已发放 2=已失效',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='达标奖励';

-- ----------------------------
-- Table structure for jiecheng_commission_assessment
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_assessment`;
CREATE TABLE `jiecheng_commission_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '',
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `level_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=全部分销商 1=指定分销商',
  `levels` json NOT NULL,
  `is_degrade` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否降级 0=不降级 1=降级',
  `assessment_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '考核依据 0=分销商订单总额 1=分销订单总数 ',
  `thumb` varchar(500) NOT NULL DEFAULT '' COMMENT '活动封面',
  `content` text COMMENT '规则介绍',
  `role` json NOT NULL COMMENT '奖励规则及内容',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '考核状态 0=未开始 1=进行中 2=手动停止 3=已结束 ',
  `stop_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='考核';

-- ----------------------------
-- Table structure for jiecheng_commission_assessment_degrade
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_assessment_degrade`;
CREATE TABLE `jiecheng_commission_assessment_degrade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `mall_id` int(11) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '分销商id',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT '用户id',
  `old_level` int(11) NOT NULL DEFAULT '0' COMMENT '降级之前等级',
  `assessment_id` int(11) NOT NULL DEFAULT '0',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='考核降级记录';

-- ----------------------------
-- Table structure for jiecheng_commission_assessment_order
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_assessment_order`;
CREATE TABLE `jiecheng_commission_assessment_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `mall_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL DEFAULT '0' COMMENT '分销商id',
  `status` tinyint(4) NOT NULL COMMENT '发放状态 0=待发放 1=已发放 2=已失效',
  `money` float(11,2) NOT NULL DEFAULT '0.00' COMMENT '一级佣金',
  `money_1` float(11,2) NOT NULL DEFAULT '0.00' COMMENT '阶梯佣金',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='考核订单';

-- ----------------------------
-- Table structure for jiecheng_commission_assessment_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_commission_assessment_withdraw`;
CREATE TABLE `jiecheng_commission_assessment_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) DEFAULT NULL,
  `agent_id` int(11) NOT NULL,
  `withdraw` decimal(10,2) NOT NULL,
  `service_charge` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `is_online` tinyint(2) DEFAULT '0' COMMENT '是否线上,1:线上,0:线下',
  `type` int(10) NOT NULL COMMENT '类型:1-微信,2-零钱',
  `card` json DEFAULT NULL COMMENT '线下卡片数据',
  `serial_num` varchar(64) NOT NULL DEFAULT '0',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '状态:0:申请中,1:通过,2:取消',
  `remark` varchar(255) DEFAULT NULL,
  `pay_no` varchar(64) DEFAULT NULL,
  `pay_time` datetime DEFAULT NULL,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `pk_agent_withdraw` (`agent_id`) USING BTREE,
  CONSTRAINT `jiecheng_commission_assessment_withdraw_ibfk_1` FOREIGN KEY (`agent_id`) REFERENCES `jiecheng_agent` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='考核提现记录';

DROP TABLE IF EXISTS `jiecheng_link`;
CREATE TABLE `jiecheng_link` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL COMMENT '页面名称',
  `url_link` varchar(500) NOT NULL DEFAULT '' COMMENT '链接',
  `wx_url_path` varchar(500) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=正常 1=过期',
  `path` varchar(500) NOT NULL,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `is_forever` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否永久 0=否 1=永久',
  `expired_time` datetime DEFAULT NULL COMMENT '过期时间',
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`,`url_link`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='外部跳转链接';

DROP TABLE IF EXISTS `jiecheng_link_click`;
CREATE TABLE `jiecheng_link_click` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` varchar(255) NOT NULL DEFAULT '',
  `link_id` int(11) NOT NULL DEFAULT '0',
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `delete_time` datetime DEFAULT NULL,
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_mall_type` (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='外部跳转链接点击记录';

-- ----------------------------
-- Table structure for jiecheng_seckill
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_seckill`;
CREATE TABLE `jiecheng_seckill` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `client_type` varchar(20) NOT NULL DEFAULT '' COMMENT '1微信公众号2小程序3h5',
  `rules` text NOT NULL COMMENT '拼团配置',
  `close_time` datetime DEFAULT NULL COMMENT '手动停止时间',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='秒杀活动';

-- ----------------------------
-- Table structure for jiecheng_seckill_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_seckill_commodity`;
CREATE TABLE `jiecheng_seckill_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `seckill_id` int(11) NOT NULL COMMENT 'Id',
  `commodity_id` int(11) NOT NULL COMMENT '商品id',
  `has_sku` int(1) NOT NULL DEFAULT '0' COMMENT '1有',
  `activity_stock` int(10) NOT NULL COMMENT '库存',
  `activity_price` varchar(255) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `use_agent` int(11) NOT NULL DEFAULT '0' COMMENT '分销',
  `limit_type` varchar(50) NOT NULL DEFAULT '' COMMENT '[选项，活动期内每人最多购买, 活动期内每人每天最多购买]',
  `show_time` datetime DEFAULT NULL COMMENT '预热时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='活动商品';

-- ----------------------------
-- Table structure for jiecheng_seckill_commodity_sku
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_seckill_commodity_sku`;
CREATE TABLE `jiecheng_seckill_commodity_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `sc_id` int(11) NOT NULL COMMENT 'seckill_commodity的id',
  `sku_id` int(11) NOT NULL DEFAULT '0',
  `activity_stock` int(10) NOT NULL COMMENT '库存',
  `activity_price` varchar(255) NOT NULL DEFAULT '0.00' COMMENT '秒杀价',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='秒杀商品';

-- ----------------------------
-- Table structure for jiecheng_presell
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_presell`;
CREATE TABLE `jiecheng_presell` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `client_type` varchar(20) NOT NULL DEFAULT '' COMMENT '1微信公众号2小程序3h5',
  `rules` text NOT NULL COMMENT '配置',
  `close_time` datetime DEFAULT NULL COMMENT '手动停止时间',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='活动';

-- ----------------------------
-- Table structure for jiecheng_presell_commodity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_presell_commodity`;
CREATE TABLE `jiecheng_presell_commodity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `presell_id` int(11) NOT NULL COMMENT '活动Id',
  `commodity_id` int(11) NOT NULL COMMENT '商品id',
  `has_sku` int(1) NOT NULL DEFAULT '0' COMMENT '1有',
  `activity_stock` int(10) NOT NULL COMMENT '库存',
  `activity_price` varchar(25) NOT NULL DEFAULT '0.00' COMMENT '价格',
  `status` int(11) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `use_agent` int(11) NOT NULL DEFAULT '0' COMMENT '分销',
  `send_at` varchar(50) NOT NULL DEFAULT '' COMMENT '[选项，特定时间, 付款成功]',
  `limit_type` varchar(50) NOT NULL DEFAULT '' COMMENT '[选项，活动期内每人最多购买, 活动期内每人每天最多购买]',
  `cancel_time` int(11) NOT NULL DEFAULT '0' COMMENT '0跟随系统设置，取消时间分钟',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='预售商品';

-- ----------------------------
-- Table structure for jiecheng_presell_commodity_sku
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_presell_commodity_sku`;
CREATE TABLE `jiecheng_presell_commodity_sku` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `pc_id` int(11) NOT NULL COMMENT 'presell_commodity的id',
  `sku_id` int(11) NOT NULL DEFAULT '0',
  `activity_stock` int(10) NOT NULL COMMENT '库存',
  `activity_price` varchar(255) NOT NULL DEFAULT '0.00' COMMENT '价格',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='预售商品';

-- ----------------------------
-- Table structure for jiecheng_activity
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_activity`;
CREATE TABLE `jiecheng_activity` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1充值',
  `title` varchar(255) NOT NULL DEFAULT '' COMMENT '活动名称',
  `start_time` datetime NOT NULL COMMENT '开始时间',
  `end_time` datetime NOT NULL COMMENT '结束时间',
  `client_type` varchar(20) NOT NULL DEFAULT '' COMMENT '1微信公众号2小程序3h5',
  `rules` text NOT NULL COMMENT '配置',
  `close_time` datetime DEFAULT NULL COMMENT '手动停止时间',
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `delete_time` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='活动';

-- ----------------------------
-- Table structure for jiecheng_activity_recharge
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_activity_recharge`;
CREATE TABLE `jiecheng_activity_recharge` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL DEFAULT '0',
  `aid` int(11) NOT NULL COMMENT 'activity的id',
  `single_recharge` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '充值金额',
  `balance` decimal(11,2) NOT NULL DEFAULT '0.00' COMMENT '赠送余额',
  `rewards_type` varchar(50) NOT NULL DEFAULT '' COMMENT '选择',
  `coupon` varchar(100) NOT NULL DEFAULT '' COMMENT '优惠券',
  `credit` int(11) NOT NULL DEFAULT '0' COMMENT '积分',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0未开始1开始2停止',
  `end_time` datetime NOT NULL,
  `delete_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='充值活动';

-- ----------------------------
-- Table structure for jiecheng_assettransfer_log
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_assettransfer_log`;
CREATE TABLE `jiecheng_assettransfer_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mall_id` int(11) NOT NULL,
  `type` tinyint(255) NOT NULL DEFAULT '0' COMMENT '0=余额转赠 1=积分转赠',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `gifted_user_id` int(11) NOT NULL DEFAULT '0' COMMENT '被赠用户',
  `money` float(10,2) NOT NULL DEFAULT '0.00',
  `gifted_money` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '被赠积分或余额',
  `service_charge` float(10,2) NOT NULL DEFAULT '0.00' COMMENT '手续费',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='资产转赠';

-- Table structure for jiecheng_saas_version
-- ----------------------------
DROP TABLE IF EXISTS `jiecheng_version`;
CREATE TABLE `jiecheng_version` (
  `id` int(11) NOT NULL,
  `old_version` varchar(255) DEFAULT NULL,
  `new_version` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC COMMENT='版本';


INSERT INTO `jiecheng_area` VALUES ('1', '86', '110000', '北京市');
INSERT INTO `jiecheng_area` VALUES ('2', '86', '120000', '天津市');
INSERT INTO `jiecheng_area` VALUES ('3', '86', '130000', '河北省');
INSERT INTO `jiecheng_area` VALUES ('4', '86', '140000', '山西省');
INSERT INTO `jiecheng_area` VALUES ('5', '86', '150000', '内蒙古自治区');
INSERT INTO `jiecheng_area` VALUES ('6', '86', '210000', '辽宁省');
INSERT INTO `jiecheng_area` VALUES ('7', '86', '220000', '吉林省');
INSERT INTO `jiecheng_area` VALUES ('8', '86', '230000', '黑龙江省');
INSERT INTO `jiecheng_area` VALUES ('9', '86', '310000', '上海市');
INSERT INTO `jiecheng_area` VALUES ('10', '86', '320000', '江苏省');
INSERT INTO `jiecheng_area` VALUES ('11', '86', '330000', '浙江省');
INSERT INTO `jiecheng_area` VALUES ('12', '86', '340000', '安徽省');
INSERT INTO `jiecheng_area` VALUES ('13', '86', '350000', '福建省');
INSERT INTO `jiecheng_area` VALUES ('14', '86', '360000', '江西省');
INSERT INTO `jiecheng_area` VALUES ('15', '86', '370000', '山东省');
INSERT INTO `jiecheng_area` VALUES ('16', '86', '410000', '河南省');
INSERT INTO `jiecheng_area` VALUES ('17', '86', '420000', '湖北省');
INSERT INTO `jiecheng_area` VALUES ('18', '86', '430000', '湖南省');
INSERT INTO `jiecheng_area` VALUES ('19', '86', '440000', '广东省');
INSERT INTO `jiecheng_area` VALUES ('20', '86', '450000', '广西壮族自治区');
INSERT INTO `jiecheng_area` VALUES ('21', '86', '460000', '海南省');
INSERT INTO `jiecheng_area` VALUES ('22', '86', '500000', '重庆市');
INSERT INTO `jiecheng_area` VALUES ('23', '86', '510000', '四川省');
INSERT INTO `jiecheng_area` VALUES ('24', '86', '520000', '贵州省');
INSERT INTO `jiecheng_area` VALUES ('25', '86', '530000', '云南省');
INSERT INTO `jiecheng_area` VALUES ('26', '86', '540000', '西藏自治区');
INSERT INTO `jiecheng_area` VALUES ('27', '86', '610000', '陕西省');
INSERT INTO `jiecheng_area` VALUES ('28', '86', '620000', '甘肃省');
INSERT INTO `jiecheng_area` VALUES ('29', '86', '630000', '青海省');
INSERT INTO `jiecheng_area` VALUES ('30', '86', '640000', '宁夏回族自治区');
INSERT INTO `jiecheng_area` VALUES ('31', '86', '650000', '新疆维吾尔自治区');
INSERT INTO `jiecheng_area` VALUES ('32', '86', '710000', '台湾省');
INSERT INTO `jiecheng_area` VALUES ('33', '86', '810000', '香港特别行政区');
INSERT INTO `jiecheng_area` VALUES ('34', '86', '820000', '澳门特别行政区');
INSERT INTO `jiecheng_area` VALUES ('35', '110000', '110100', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('36', '110100', '110101', '东城区');
INSERT INTO `jiecheng_area` VALUES ('37', '110100', '110102', '西城区');
INSERT INTO `jiecheng_area` VALUES ('38', '110100', '110105', '朝阳区');
INSERT INTO `jiecheng_area` VALUES ('39', '110100', '110106', '丰台区');
INSERT INTO `jiecheng_area` VALUES ('40', '110100', '110107', '石景山区');
INSERT INTO `jiecheng_area` VALUES ('41', '110100', '110108', '海淀区');
INSERT INTO `jiecheng_area` VALUES ('42', '110100', '110109', '门头沟区');
INSERT INTO `jiecheng_area` VALUES ('43', '110100', '110111', '房山区');
INSERT INTO `jiecheng_area` VALUES ('44', '110100', '110112', '通州区');
INSERT INTO `jiecheng_area` VALUES ('45', '110100', '110113', '顺义区');
INSERT INTO `jiecheng_area` VALUES ('46', '110100', '110114', '昌平区');
INSERT INTO `jiecheng_area` VALUES ('47', '110100', '110115', '大兴区');
INSERT INTO `jiecheng_area` VALUES ('48', '110100', '110116', '怀柔区');
INSERT INTO `jiecheng_area` VALUES ('49', '110100', '110117', '平谷区');
INSERT INTO `jiecheng_area` VALUES ('50', '110100', '110118', '密云区');
INSERT INTO `jiecheng_area` VALUES ('51', '110100', '110119', '延庆区');
INSERT INTO `jiecheng_area` VALUES ('52', '120000', '120100', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('53', '120100', '120101', '和平区');
INSERT INTO `jiecheng_area` VALUES ('54', '120100', '120102', '河东区');
INSERT INTO `jiecheng_area` VALUES ('55', '120100', '120103', '河西区');
INSERT INTO `jiecheng_area` VALUES ('56', '120100', '120104', '南开区');
INSERT INTO `jiecheng_area` VALUES ('57', '120100', '120105', '河北区');
INSERT INTO `jiecheng_area` VALUES ('58', '120100', '120106', '红桥区');
INSERT INTO `jiecheng_area` VALUES ('59', '120100', '120110', '东丽区');
INSERT INTO `jiecheng_area` VALUES ('60', '120100', '120111', '西青区');
INSERT INTO `jiecheng_area` VALUES ('61', '120100', '120112', '津南区');
INSERT INTO `jiecheng_area` VALUES ('62', '120100', '120113', '北辰区');
INSERT INTO `jiecheng_area` VALUES ('63', '120100', '120114', '武清区');
INSERT INTO `jiecheng_area` VALUES ('64', '120100', '120115', '宝坻区');
INSERT INTO `jiecheng_area` VALUES ('65', '120100', '120116', '滨海新区');
INSERT INTO `jiecheng_area` VALUES ('66', '120100', '120117', '宁河区');
INSERT INTO `jiecheng_area` VALUES ('67', '120100', '120118', '静海区');
INSERT INTO `jiecheng_area` VALUES ('68', '120100', '120119', '蓟州区');
INSERT INTO `jiecheng_area` VALUES ('69', '130000', '130100', '石家庄市');
INSERT INTO `jiecheng_area` VALUES ('70', '130000', '130200', '唐山市');
INSERT INTO `jiecheng_area` VALUES ('71', '130000', '130300', '秦皇岛市');
INSERT INTO `jiecheng_area` VALUES ('72', '130000', '130400', '邯郸市');
INSERT INTO `jiecheng_area` VALUES ('73', '130000', '130500', '邢台市');
INSERT INTO `jiecheng_area` VALUES ('74', '130000', '130600', '保定市');
INSERT INTO `jiecheng_area` VALUES ('75', '130000', '130700', '张家口市');
INSERT INTO `jiecheng_area` VALUES ('76', '130000', '130800', '承德市');
INSERT INTO `jiecheng_area` VALUES ('77', '130000', '130900', '沧州市');
INSERT INTO `jiecheng_area` VALUES ('78', '130000', '131000', '廊坊市');
INSERT INTO `jiecheng_area` VALUES ('79', '130000', '131100', '衡水市');
INSERT INTO `jiecheng_area` VALUES ('80', '130100', '130101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('81', '130100', '130102', '长安区');
INSERT INTO `jiecheng_area` VALUES ('82', '130100', '130104', '桥西区');
INSERT INTO `jiecheng_area` VALUES ('83', '130100', '130105', '新华区');
INSERT INTO `jiecheng_area` VALUES ('84', '130100', '130107', '井陉矿区');
INSERT INTO `jiecheng_area` VALUES ('85', '130100', '130108', '裕华区');
INSERT INTO `jiecheng_area` VALUES ('86', '130100', '130109', '藁城区');
INSERT INTO `jiecheng_area` VALUES ('87', '130100', '130110', '鹿泉区');
INSERT INTO `jiecheng_area` VALUES ('88', '130100', '130111', '栾城区');
INSERT INTO `jiecheng_area` VALUES ('89', '130100', '130121', '井陉县');
INSERT INTO `jiecheng_area` VALUES ('90', '130100', '130123', '正定县');
INSERT INTO `jiecheng_area` VALUES ('91', '130100', '130125', '行唐县');
INSERT INTO `jiecheng_area` VALUES ('92', '130100', '130126', '灵寿县');
INSERT INTO `jiecheng_area` VALUES ('93', '130100', '130127', '高邑县');
INSERT INTO `jiecheng_area` VALUES ('94', '130100', '130128', '深泽县');
INSERT INTO `jiecheng_area` VALUES ('95', '130100', '130129', '赞皇县');
INSERT INTO `jiecheng_area` VALUES ('96', '130100', '130130', '无极县');
INSERT INTO `jiecheng_area` VALUES ('97', '130100', '130131', '平山县');
INSERT INTO `jiecheng_area` VALUES ('98', '130100', '130132', '元氏县');
INSERT INTO `jiecheng_area` VALUES ('99', '130100', '130133', '赵县');
INSERT INTO `jiecheng_area` VALUES ('100', '130100', '130171', '石家庄高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('101', '130100', '130172', '石家庄循环化工园区');
INSERT INTO `jiecheng_area` VALUES ('102', '130100', '130181', '辛集市');
INSERT INTO `jiecheng_area` VALUES ('103', '130100', '130183', '晋州市');
INSERT INTO `jiecheng_area` VALUES ('104', '130100', '130184', '新乐市');
INSERT INTO `jiecheng_area` VALUES ('105', '130200', '130201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('106', '130200', '130202', '路南区');
INSERT INTO `jiecheng_area` VALUES ('107', '130200', '130203', '路北区');
INSERT INTO `jiecheng_area` VALUES ('108', '130200', '130204', '古冶区');
INSERT INTO `jiecheng_area` VALUES ('109', '130200', '130205', '开平区');
INSERT INTO `jiecheng_area` VALUES ('110', '130200', '130207', '丰南区');
INSERT INTO `jiecheng_area` VALUES ('111', '130200', '130208', '丰润区');
INSERT INTO `jiecheng_area` VALUES ('112', '130200', '130209', '曹妃甸区');
INSERT INTO `jiecheng_area` VALUES ('113', '130200', '130224', '滦南县');
INSERT INTO `jiecheng_area` VALUES ('114', '130200', '130225', '乐亭县');
INSERT INTO `jiecheng_area` VALUES ('115', '130200', '130227', '迁西县');
INSERT INTO `jiecheng_area` VALUES ('116', '130200', '130229', '玉田县');
INSERT INTO `jiecheng_area` VALUES ('117', '130200', '130271', '河北唐山芦台经济开发区');
INSERT INTO `jiecheng_area` VALUES ('118', '130200', '130272', '唐山市汉沽管理区');
INSERT INTO `jiecheng_area` VALUES ('119', '130200', '130273', '唐山高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('120', '130200', '130274', '河北唐山海港经济开发区');
INSERT INTO `jiecheng_area` VALUES ('121', '130200', '130281', '遵化市');
INSERT INTO `jiecheng_area` VALUES ('122', '130200', '130283', '迁安市');
INSERT INTO `jiecheng_area` VALUES ('123', '130200', '130284', '滦州市');
INSERT INTO `jiecheng_area` VALUES ('124', '130300', '130301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('125', '130300', '130302', '海港区');
INSERT INTO `jiecheng_area` VALUES ('126', '130300', '130303', '山海关区');
INSERT INTO `jiecheng_area` VALUES ('127', '130300', '130304', '北戴河区');
INSERT INTO `jiecheng_area` VALUES ('128', '130300', '130306', '抚宁区');
INSERT INTO `jiecheng_area` VALUES ('129', '130300', '130321', '青龙满族自治县');
INSERT INTO `jiecheng_area` VALUES ('130', '130300', '130322', '昌黎县');
INSERT INTO `jiecheng_area` VALUES ('131', '130300', '130324', '卢龙县');
INSERT INTO `jiecheng_area` VALUES ('132', '130300', '130371', '秦皇岛市经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('133', '130300', '130372', '北戴河新区');
INSERT INTO `jiecheng_area` VALUES ('134', '130400', '130401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('135', '130400', '130402', '邯山区');
INSERT INTO `jiecheng_area` VALUES ('136', '130400', '130403', '丛台区');
INSERT INTO `jiecheng_area` VALUES ('137', '130400', '130404', '复兴区');
INSERT INTO `jiecheng_area` VALUES ('138', '130400', '130406', '峰峰矿区');
INSERT INTO `jiecheng_area` VALUES ('139', '130400', '130407', '肥乡区');
INSERT INTO `jiecheng_area` VALUES ('140', '130400', '130408', '永年区');
INSERT INTO `jiecheng_area` VALUES ('141', '130400', '130423', '临漳县');
INSERT INTO `jiecheng_area` VALUES ('142', '130400', '130424', '成安县');
INSERT INTO `jiecheng_area` VALUES ('143', '130400', '130425', '大名县');
INSERT INTO `jiecheng_area` VALUES ('144', '130400', '130426', '涉县');
INSERT INTO `jiecheng_area` VALUES ('145', '130400', '130427', '磁县');
INSERT INTO `jiecheng_area` VALUES ('146', '130400', '130430', '邱县');
INSERT INTO `jiecheng_area` VALUES ('147', '130400', '130431', '鸡泽县');
INSERT INTO `jiecheng_area` VALUES ('148', '130400', '130432', '广平县');
INSERT INTO `jiecheng_area` VALUES ('149', '130400', '130433', '馆陶县');
INSERT INTO `jiecheng_area` VALUES ('150', '130400', '130434', '魏县');
INSERT INTO `jiecheng_area` VALUES ('151', '130400', '130435', '曲周县');
INSERT INTO `jiecheng_area` VALUES ('152', '130400', '130471', '邯郸经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('153', '130400', '130473', '邯郸冀南新区');
INSERT INTO `jiecheng_area` VALUES ('154', '130400', '130481', '武安市');
INSERT INTO `jiecheng_area` VALUES ('155', '130500', '130501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('156', '130500', '130502', '桥东区');
INSERT INTO `jiecheng_area` VALUES ('157', '130500', '130503', '桥西区');
INSERT INTO `jiecheng_area` VALUES ('158', '130500', '130521', '邢台县');
INSERT INTO `jiecheng_area` VALUES ('159', '130500', '130522', '临城县');
INSERT INTO `jiecheng_area` VALUES ('160', '130500', '130523', '内丘县');
INSERT INTO `jiecheng_area` VALUES ('161', '130500', '130524', '柏乡县');
INSERT INTO `jiecheng_area` VALUES ('162', '130500', '130525', '隆尧县');
INSERT INTO `jiecheng_area` VALUES ('163', '130500', '130526', '任县');
INSERT INTO `jiecheng_area` VALUES ('164', '130500', '130527', '南和县');
INSERT INTO `jiecheng_area` VALUES ('165', '130500', '130528', '宁晋县');
INSERT INTO `jiecheng_area` VALUES ('166', '130500', '130529', '巨鹿县');
INSERT INTO `jiecheng_area` VALUES ('167', '130500', '130530', '新河县');
INSERT INTO `jiecheng_area` VALUES ('168', '130500', '130531', '广宗县');
INSERT INTO `jiecheng_area` VALUES ('169', '130500', '130532', '平乡县');
INSERT INTO `jiecheng_area` VALUES ('170', '130500', '130533', '威县');
INSERT INTO `jiecheng_area` VALUES ('171', '130500', '130534', '清河县');
INSERT INTO `jiecheng_area` VALUES ('172', '130500', '130535', '临西县');
INSERT INTO `jiecheng_area` VALUES ('173', '130500', '130571', '河北邢台经济开发区');
INSERT INTO `jiecheng_area` VALUES ('174', '130500', '130581', '南宫市');
INSERT INTO `jiecheng_area` VALUES ('175', '130500', '130582', '沙河市');
INSERT INTO `jiecheng_area` VALUES ('176', '130600', '130601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('177', '130600', '130602', '竞秀区');
INSERT INTO `jiecheng_area` VALUES ('178', '130600', '130606', '莲池区');
INSERT INTO `jiecheng_area` VALUES ('179', '130600', '130607', '满城区');
INSERT INTO `jiecheng_area` VALUES ('180', '130600', '130608', '清苑区');
INSERT INTO `jiecheng_area` VALUES ('181', '130600', '130609', '徐水区');
INSERT INTO `jiecheng_area` VALUES ('182', '130600', '130623', '涞水县');
INSERT INTO `jiecheng_area` VALUES ('183', '130600', '130624', '阜平县');
INSERT INTO `jiecheng_area` VALUES ('184', '130600', '130626', '定兴县');
INSERT INTO `jiecheng_area` VALUES ('185', '130600', '130627', '唐县');
INSERT INTO `jiecheng_area` VALUES ('186', '130600', '130628', '高阳县');
INSERT INTO `jiecheng_area` VALUES ('187', '130600', '130629', '容城县');
INSERT INTO `jiecheng_area` VALUES ('188', '130600', '130630', '涞源县');
INSERT INTO `jiecheng_area` VALUES ('189', '130600', '130631', '望都县');
INSERT INTO `jiecheng_area` VALUES ('190', '130600', '130632', '安新县');
INSERT INTO `jiecheng_area` VALUES ('191', '130600', '130633', '易县');
INSERT INTO `jiecheng_area` VALUES ('192', '130600', '130634', '曲阳县');
INSERT INTO `jiecheng_area` VALUES ('193', '130600', '130635', '蠡县');
INSERT INTO `jiecheng_area` VALUES ('194', '130600', '130636', '顺平县');
INSERT INTO `jiecheng_area` VALUES ('195', '130600', '130637', '博野县');
INSERT INTO `jiecheng_area` VALUES ('196', '130600', '130638', '雄县');
INSERT INTO `jiecheng_area` VALUES ('197', '130600', '130671', '保定高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('198', '130600', '130672', '保定白沟新城');
INSERT INTO `jiecheng_area` VALUES ('199', '130600', '130681', '涿州市');
INSERT INTO `jiecheng_area` VALUES ('200', '130600', '130682', '定州市');
INSERT INTO `jiecheng_area` VALUES ('201', '130600', '130683', '安国市');
INSERT INTO `jiecheng_area` VALUES ('202', '130600', '130684', '高碑店市');
INSERT INTO `jiecheng_area` VALUES ('203', '130700', '130701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('204', '130700', '130702', '桥东区');
INSERT INTO `jiecheng_area` VALUES ('205', '130700', '130703', '桥西区');
INSERT INTO `jiecheng_area` VALUES ('206', '130700', '130705', '宣化区');
INSERT INTO `jiecheng_area` VALUES ('207', '130700', '130706', '下花园区');
INSERT INTO `jiecheng_area` VALUES ('208', '130700', '130708', '万全区');
INSERT INTO `jiecheng_area` VALUES ('209', '130700', '130709', '崇礼区');
INSERT INTO `jiecheng_area` VALUES ('210', '130700', '130722', '张北县');
INSERT INTO `jiecheng_area` VALUES ('211', '130700', '130723', '康保县');
INSERT INTO `jiecheng_area` VALUES ('212', '130700', '130724', '沽源县');
INSERT INTO `jiecheng_area` VALUES ('213', '130700', '130725', '尚义县');
INSERT INTO `jiecheng_area` VALUES ('214', '130700', '130726', '蔚县');
INSERT INTO `jiecheng_area` VALUES ('215', '130700', '130727', '阳原县');
INSERT INTO `jiecheng_area` VALUES ('216', '130700', '130728', '怀安县');
INSERT INTO `jiecheng_area` VALUES ('217', '130700', '130730', '怀来县');
INSERT INTO `jiecheng_area` VALUES ('218', '130700', '130731', '涿鹿县');
INSERT INTO `jiecheng_area` VALUES ('219', '130700', '130732', '赤城县');
INSERT INTO `jiecheng_area` VALUES ('220', '130700', '130771', '张家口经济开发区');
INSERT INTO `jiecheng_area` VALUES ('221', '130700', '130772', '张家口市察北管理区');
INSERT INTO `jiecheng_area` VALUES ('222', '130700', '130773', '张家口市塞北管理区');
INSERT INTO `jiecheng_area` VALUES ('223', '130800', '130801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('224', '130800', '130802', '双桥区');
INSERT INTO `jiecheng_area` VALUES ('225', '130800', '130803', '双滦区');
INSERT INTO `jiecheng_area` VALUES ('226', '130800', '130804', '鹰手营子矿区');
INSERT INTO `jiecheng_area` VALUES ('227', '130800', '130821', '承德县');
INSERT INTO `jiecheng_area` VALUES ('228', '130800', '130822', '兴隆县');
INSERT INTO `jiecheng_area` VALUES ('229', '130800', '130824', '滦平县');
INSERT INTO `jiecheng_area` VALUES ('230', '130800', '130825', '隆化县');
INSERT INTO `jiecheng_area` VALUES ('231', '130800', '130826', '丰宁满族自治县');
INSERT INTO `jiecheng_area` VALUES ('232', '130800', '130827', '宽城满族自治县');
INSERT INTO `jiecheng_area` VALUES ('233', '130800', '130828', '围场满族蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('234', '130800', '130871', '承德高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('235', '130800', '130881', '平泉市');
INSERT INTO `jiecheng_area` VALUES ('236', '130900', '130901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('237', '130900', '130902', '新华区');
INSERT INTO `jiecheng_area` VALUES ('238', '130900', '130903', '运河区');
INSERT INTO `jiecheng_area` VALUES ('239', '130900', '130921', '沧县');
INSERT INTO `jiecheng_area` VALUES ('240', '130900', '130922', '青县');
INSERT INTO `jiecheng_area` VALUES ('241', '130900', '130923', '东光县');
INSERT INTO `jiecheng_area` VALUES ('242', '130900', '130924', '海兴县');
INSERT INTO `jiecheng_area` VALUES ('243', '130900', '130925', '盐山县');
INSERT INTO `jiecheng_area` VALUES ('244', '130900', '130926', '肃宁县');
INSERT INTO `jiecheng_area` VALUES ('245', '130900', '130927', '南皮县');
INSERT INTO `jiecheng_area` VALUES ('246', '130900', '130928', '吴桥县');
INSERT INTO `jiecheng_area` VALUES ('247', '130900', '130929', '献县');
INSERT INTO `jiecheng_area` VALUES ('248', '130900', '130930', '孟村回族自治县');
INSERT INTO `jiecheng_area` VALUES ('249', '130900', '130971', '河北沧州经济开发区');
INSERT INTO `jiecheng_area` VALUES ('250', '130900', '130972', '沧州高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('251', '130900', '130973', '沧州渤海新区');
INSERT INTO `jiecheng_area` VALUES ('252', '130900', '130981', '泊头市');
INSERT INTO `jiecheng_area` VALUES ('253', '130900', '130982', '任丘市');
INSERT INTO `jiecheng_area` VALUES ('254', '130900', '130983', '黄骅市');
INSERT INTO `jiecheng_area` VALUES ('255', '130900', '130984', '河间市');
INSERT INTO `jiecheng_area` VALUES ('256', '131000', '131001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('257', '131000', '131002', '安次区');
INSERT INTO `jiecheng_area` VALUES ('258', '131000', '131003', '广阳区');
INSERT INTO `jiecheng_area` VALUES ('259', '131000', '131022', '固安县');
INSERT INTO `jiecheng_area` VALUES ('260', '131000', '131023', '永清县');
INSERT INTO `jiecheng_area` VALUES ('261', '131000', '131024', '香河县');
INSERT INTO `jiecheng_area` VALUES ('262', '131000', '131025', '大城县');
INSERT INTO `jiecheng_area` VALUES ('263', '131000', '131026', '文安县');
INSERT INTO `jiecheng_area` VALUES ('264', '131000', '131028', '大厂回族自治县');
INSERT INTO `jiecheng_area` VALUES ('265', '131000', '131071', '廊坊经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('266', '131000', '131081', '霸州市');
INSERT INTO `jiecheng_area` VALUES ('267', '131000', '131082', '三河市');
INSERT INTO `jiecheng_area` VALUES ('268', '131100', '131101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('269', '131100', '131102', '桃城区');
INSERT INTO `jiecheng_area` VALUES ('270', '131100', '131103', '冀州区');
INSERT INTO `jiecheng_area` VALUES ('271', '131100', '131121', '枣强县');
INSERT INTO `jiecheng_area` VALUES ('272', '131100', '131122', '武邑县');
INSERT INTO `jiecheng_area` VALUES ('273', '131100', '131123', '武强县');
INSERT INTO `jiecheng_area` VALUES ('274', '131100', '131124', '饶阳县');
INSERT INTO `jiecheng_area` VALUES ('275', '131100', '131125', '安平县');
INSERT INTO `jiecheng_area` VALUES ('276', '131100', '131126', '故城县');
INSERT INTO `jiecheng_area` VALUES ('277', '131100', '131127', '景县');
INSERT INTO `jiecheng_area` VALUES ('278', '131100', '131128', '阜城县');
INSERT INTO `jiecheng_area` VALUES ('279', '131100', '131171', '河北衡水高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('280', '131100', '131172', '衡水滨湖新区');
INSERT INTO `jiecheng_area` VALUES ('281', '131100', '131182', '深州市');
INSERT INTO `jiecheng_area` VALUES ('282', '140000', '140100', '太原市');
INSERT INTO `jiecheng_area` VALUES ('283', '140000', '140200', '大同市');
INSERT INTO `jiecheng_area` VALUES ('284', '140000', '140300', '阳泉市');
INSERT INTO `jiecheng_area` VALUES ('285', '140000', '140400', '长治市');
INSERT INTO `jiecheng_area` VALUES ('286', '140000', '140500', '晋城市');
INSERT INTO `jiecheng_area` VALUES ('287', '140000', '140600', '朔州市');
INSERT INTO `jiecheng_area` VALUES ('288', '140000', '140700', '晋中市');
INSERT INTO `jiecheng_area` VALUES ('289', '140000', '140800', '运城市');
INSERT INTO `jiecheng_area` VALUES ('290', '140000', '140900', '忻州市');
INSERT INTO `jiecheng_area` VALUES ('291', '140000', '141000', '临汾市');
INSERT INTO `jiecheng_area` VALUES ('292', '140000', '141100', '吕梁市');
INSERT INTO `jiecheng_area` VALUES ('293', '140100', '140101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('294', '140100', '140105', '小店区');
INSERT INTO `jiecheng_area` VALUES ('295', '140100', '140106', '迎泽区');
INSERT INTO `jiecheng_area` VALUES ('296', '140100', '140107', '杏花岭区');
INSERT INTO `jiecheng_area` VALUES ('297', '140100', '140108', '尖草坪区');
INSERT INTO `jiecheng_area` VALUES ('298', '140100', '140109', '万柏林区');
INSERT INTO `jiecheng_area` VALUES ('299', '140100', '140110', '晋源区');
INSERT INTO `jiecheng_area` VALUES ('300', '140100', '140121', '清徐县');
INSERT INTO `jiecheng_area` VALUES ('301', '140100', '140122', '阳曲县');
INSERT INTO `jiecheng_area` VALUES ('302', '140100', '140123', '娄烦县');
INSERT INTO `jiecheng_area` VALUES ('303', '140100', '140171', '山西转型综合改革示范区');
INSERT INTO `jiecheng_area` VALUES ('304', '140100', '140181', '古交市');
INSERT INTO `jiecheng_area` VALUES ('305', '140200', '140201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('306', '140200', '140212', '新荣区');
INSERT INTO `jiecheng_area` VALUES ('307', '140200', '140213', '平城区');
INSERT INTO `jiecheng_area` VALUES ('308', '140200', '140214', '云冈区');
INSERT INTO `jiecheng_area` VALUES ('309', '140200', '140215', '云州区');
INSERT INTO `jiecheng_area` VALUES ('310', '140200', '140221', '阳高县');
INSERT INTO `jiecheng_area` VALUES ('311', '140200', '140222', '天镇县');
INSERT INTO `jiecheng_area` VALUES ('312', '140200', '140223', '广灵县');
INSERT INTO `jiecheng_area` VALUES ('313', '140200', '140224', '灵丘县');
INSERT INTO `jiecheng_area` VALUES ('314', '140200', '140225', '浑源县');
INSERT INTO `jiecheng_area` VALUES ('315', '140200', '140226', '左云县');
INSERT INTO `jiecheng_area` VALUES ('316', '140200', '140271', '山西大同经济开发区');
INSERT INTO `jiecheng_area` VALUES ('317', '140300', '140301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('318', '140300', '140302', '城区');
INSERT INTO `jiecheng_area` VALUES ('319', '140300', '140303', '矿区');
INSERT INTO `jiecheng_area` VALUES ('320', '140300', '140311', '郊区');
INSERT INTO `jiecheng_area` VALUES ('321', '140300', '140321', '平定县');
INSERT INTO `jiecheng_area` VALUES ('322', '140300', '140322', '盂县');
INSERT INTO `jiecheng_area` VALUES ('323', '140400', '140401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('324', '140400', '140403', '潞州区');
INSERT INTO `jiecheng_area` VALUES ('325', '140400', '140404', '上党区');
INSERT INTO `jiecheng_area` VALUES ('326', '140400', '140405', '屯留区');
INSERT INTO `jiecheng_area` VALUES ('327', '140400', '140406', '潞城区');
INSERT INTO `jiecheng_area` VALUES ('328', '140400', '140423', '襄垣县');
INSERT INTO `jiecheng_area` VALUES ('329', '140400', '140425', '平顺县');
INSERT INTO `jiecheng_area` VALUES ('330', '140400', '140426', '黎城县');
INSERT INTO `jiecheng_area` VALUES ('331', '140400', '140427', '壶关县');
INSERT INTO `jiecheng_area` VALUES ('332', '140400', '140428', '长子县');
INSERT INTO `jiecheng_area` VALUES ('333', '140400', '140429', '武乡县');
INSERT INTO `jiecheng_area` VALUES ('334', '140400', '140430', '沁县');
INSERT INTO `jiecheng_area` VALUES ('335', '140400', '140431', '沁源县');
INSERT INTO `jiecheng_area` VALUES ('336', '140400', '140471', '山西长治高新技术产业园区');
INSERT INTO `jiecheng_area` VALUES ('337', '140500', '140501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('338', '140500', '140502', '城区');
INSERT INTO `jiecheng_area` VALUES ('339', '140500', '140521', '沁水县');
INSERT INTO `jiecheng_area` VALUES ('340', '140500', '140522', '阳城县');
INSERT INTO `jiecheng_area` VALUES ('341', '140500', '140524', '陵川县');
INSERT INTO `jiecheng_area` VALUES ('342', '140500', '140525', '泽州县');
INSERT INTO `jiecheng_area` VALUES ('343', '140500', '140581', '高平市');
INSERT INTO `jiecheng_area` VALUES ('344', '140600', '140601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('345', '140600', '140602', '朔城区');
INSERT INTO `jiecheng_area` VALUES ('346', '140600', '140603', '平鲁区');
INSERT INTO `jiecheng_area` VALUES ('347', '140600', '140621', '山阴县');
INSERT INTO `jiecheng_area` VALUES ('348', '140600', '140622', '应县');
INSERT INTO `jiecheng_area` VALUES ('349', '140600', '140623', '右玉县');
INSERT INTO `jiecheng_area` VALUES ('350', '140600', '140671', '山西朔州经济开发区');
INSERT INTO `jiecheng_area` VALUES ('351', '140600', '140681', '怀仁市');
INSERT INTO `jiecheng_area` VALUES ('352', '140700', '140701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('353', '140700', '140702', '榆次区');
INSERT INTO `jiecheng_area` VALUES ('354', '140700', '140721', '榆社县');
INSERT INTO `jiecheng_area` VALUES ('355', '140700', '140722', '左权县');
INSERT INTO `jiecheng_area` VALUES ('356', '140700', '140723', '和顺县');
INSERT INTO `jiecheng_area` VALUES ('357', '140700', '140724', '昔阳县');
INSERT INTO `jiecheng_area` VALUES ('358', '140700', '140725', '寿阳县');
INSERT INTO `jiecheng_area` VALUES ('359', '140700', '140726', '太谷县');
INSERT INTO `jiecheng_area` VALUES ('360', '140700', '140727', '祁县');
INSERT INTO `jiecheng_area` VALUES ('361', '140700', '140728', '平遥县');
INSERT INTO `jiecheng_area` VALUES ('362', '140700', '140729', '灵石县');
INSERT INTO `jiecheng_area` VALUES ('363', '140700', '140781', '介休市');
INSERT INTO `jiecheng_area` VALUES ('364', '140800', '140801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('365', '140800', '140802', '盐湖区');
INSERT INTO `jiecheng_area` VALUES ('366', '140800', '140821', '临猗县');
INSERT INTO `jiecheng_area` VALUES ('367', '140800', '140822', '万荣县');
INSERT INTO `jiecheng_area` VALUES ('368', '140800', '140823', '闻喜县');
INSERT INTO `jiecheng_area` VALUES ('369', '140800', '140824', '稷山县');
INSERT INTO `jiecheng_area` VALUES ('370', '140800', '140825', '新绛县');
INSERT INTO `jiecheng_area` VALUES ('371', '140800', '140826', '绛县');
INSERT INTO `jiecheng_area` VALUES ('372', '140800', '140827', '垣曲县');
INSERT INTO `jiecheng_area` VALUES ('373', '140800', '140828', '夏县');
INSERT INTO `jiecheng_area` VALUES ('374', '140800', '140829', '平陆县');
INSERT INTO `jiecheng_area` VALUES ('375', '140800', '140830', '芮城县');
INSERT INTO `jiecheng_area` VALUES ('376', '140800', '140881', '永济市');
INSERT INTO `jiecheng_area` VALUES ('377', '140800', '140882', '河津市');
INSERT INTO `jiecheng_area` VALUES ('378', '140900', '140901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('379', '140900', '140902', '忻府区');
INSERT INTO `jiecheng_area` VALUES ('380', '140900', '140921', '定襄县');
INSERT INTO `jiecheng_area` VALUES ('381', '140900', '140922', '五台县');
INSERT INTO `jiecheng_area` VALUES ('382', '140900', '140923', '代县');
INSERT INTO `jiecheng_area` VALUES ('383', '140900', '140924', '繁峙县');
INSERT INTO `jiecheng_area` VALUES ('384', '140900', '140925', '宁武县');
INSERT INTO `jiecheng_area` VALUES ('385', '140900', '140926', '静乐县');
INSERT INTO `jiecheng_area` VALUES ('386', '140900', '140927', '神池县');
INSERT INTO `jiecheng_area` VALUES ('387', '140900', '140928', '五寨县');
INSERT INTO `jiecheng_area` VALUES ('388', '140900', '140929', '岢岚县');
INSERT INTO `jiecheng_area` VALUES ('389', '140900', '140930', '河曲县');
INSERT INTO `jiecheng_area` VALUES ('390', '140900', '140931', '保德县');
INSERT INTO `jiecheng_area` VALUES ('391', '140900', '140932', '偏关县');
INSERT INTO `jiecheng_area` VALUES ('392', '140900', '140971', '五台山风景名胜区');
INSERT INTO `jiecheng_area` VALUES ('393', '140900', '140981', '原平市');
INSERT INTO `jiecheng_area` VALUES ('394', '141000', '141001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('395', '141000', '141002', '尧都区');
INSERT INTO `jiecheng_area` VALUES ('396', '141000', '141021', '曲沃县');
INSERT INTO `jiecheng_area` VALUES ('397', '141000', '141022', '翼城县');
INSERT INTO `jiecheng_area` VALUES ('398', '141000', '141023', '襄汾县');
INSERT INTO `jiecheng_area` VALUES ('399', '141000', '141024', '洪洞县');
INSERT INTO `jiecheng_area` VALUES ('400', '141000', '141025', '古县');
INSERT INTO `jiecheng_area` VALUES ('401', '141000', '141026', '安泽县');
INSERT INTO `jiecheng_area` VALUES ('402', '141000', '141027', '浮山县');
INSERT INTO `jiecheng_area` VALUES ('403', '141000', '141028', '吉县');
INSERT INTO `jiecheng_area` VALUES ('404', '141000', '141029', '乡宁县');
INSERT INTO `jiecheng_area` VALUES ('405', '141000', '141030', '大宁县');
INSERT INTO `jiecheng_area` VALUES ('406', '141000', '141031', '隰县');
INSERT INTO `jiecheng_area` VALUES ('407', '141000', '141032', '永和县');
INSERT INTO `jiecheng_area` VALUES ('408', '141000', '141033', '蒲县');
INSERT INTO `jiecheng_area` VALUES ('409', '141000', '141034', '汾西县');
INSERT INTO `jiecheng_area` VALUES ('410', '141000', '141081', '侯马市');
INSERT INTO `jiecheng_area` VALUES ('411', '141000', '141082', '霍州市');
INSERT INTO `jiecheng_area` VALUES ('412', '141100', '141101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('413', '141100', '141102', '离石区');
INSERT INTO `jiecheng_area` VALUES ('414', '141100', '141121', '文水县');
INSERT INTO `jiecheng_area` VALUES ('415', '141100', '141122', '交城县');
INSERT INTO `jiecheng_area` VALUES ('416', '141100', '141123', '兴县');
INSERT INTO `jiecheng_area` VALUES ('417', '141100', '141124', '临县');
INSERT INTO `jiecheng_area` VALUES ('418', '141100', '141125', '柳林县');
INSERT INTO `jiecheng_area` VALUES ('419', '141100', '141126', '石楼县');
INSERT INTO `jiecheng_area` VALUES ('420', '141100', '141127', '岚县');
INSERT INTO `jiecheng_area` VALUES ('421', '141100', '141128', '方山县');
INSERT INTO `jiecheng_area` VALUES ('422', '141100', '141129', '中阳县');
INSERT INTO `jiecheng_area` VALUES ('423', '141100', '141130', '交口县');
INSERT INTO `jiecheng_area` VALUES ('424', '141100', '141181', '孝义市');
INSERT INTO `jiecheng_area` VALUES ('425', '141100', '141182', '汾阳市');
INSERT INTO `jiecheng_area` VALUES ('426', '150000', '150100', '呼和浩特市');
INSERT INTO `jiecheng_area` VALUES ('427', '150000', '150200', '包头市');
INSERT INTO `jiecheng_area` VALUES ('428', '150000', '150300', '乌海市');
INSERT INTO `jiecheng_area` VALUES ('429', '150000', '150400', '赤峰市');
INSERT INTO `jiecheng_area` VALUES ('430', '150000', '150500', '通辽市');
INSERT INTO `jiecheng_area` VALUES ('431', '150000', '150600', '鄂尔多斯市');
INSERT INTO `jiecheng_area` VALUES ('432', '150000', '150700', '呼伦贝尔市');
INSERT INTO `jiecheng_area` VALUES ('433', '150000', '150800', '巴彦淖尔市');
INSERT INTO `jiecheng_area` VALUES ('434', '150000', '150900', '乌兰察布市');
INSERT INTO `jiecheng_area` VALUES ('435', '150000', '152200', '兴安盟');
INSERT INTO `jiecheng_area` VALUES ('436', '150000', '152500', '锡林郭勒盟');
INSERT INTO `jiecheng_area` VALUES ('437', '150000', '152900', '阿拉善盟');
INSERT INTO `jiecheng_area` VALUES ('438', '150100', '150101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('439', '150100', '150102', '新城区');
INSERT INTO `jiecheng_area` VALUES ('440', '150100', '150103', '回民区');
INSERT INTO `jiecheng_area` VALUES ('441', '150100', '150104', '玉泉区');
INSERT INTO `jiecheng_area` VALUES ('442', '150100', '150105', '赛罕区');
INSERT INTO `jiecheng_area` VALUES ('443', '150100', '150121', '土默特左旗');
INSERT INTO `jiecheng_area` VALUES ('444', '150100', '150122', '托克托县');
INSERT INTO `jiecheng_area` VALUES ('445', '150100', '150123', '和林格尔县');
INSERT INTO `jiecheng_area` VALUES ('446', '150100', '150124', '清水河县');
INSERT INTO `jiecheng_area` VALUES ('447', '150100', '150125', '武川县');
INSERT INTO `jiecheng_area` VALUES ('448', '150100', '150171', '呼和浩特金海工业园区');
INSERT INTO `jiecheng_area` VALUES ('449', '150100', '150172', '呼和浩特经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('450', '150200', '150201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('451', '150200', '150202', '东河区');
INSERT INTO `jiecheng_area` VALUES ('452', '150200', '150203', '昆都仑区');
INSERT INTO `jiecheng_area` VALUES ('453', '150200', '150204', '青山区');
INSERT INTO `jiecheng_area` VALUES ('454', '150200', '150205', '石拐区');
INSERT INTO `jiecheng_area` VALUES ('455', '150200', '150206', '白云鄂博矿区');
INSERT INTO `jiecheng_area` VALUES ('456', '150200', '150207', '九原区');
INSERT INTO `jiecheng_area` VALUES ('457', '150200', '150221', '土默特右旗');
INSERT INTO `jiecheng_area` VALUES ('458', '150200', '150222', '固阳县');
INSERT INTO `jiecheng_area` VALUES ('459', '150200', '150223', '达尔罕茂明安联合旗');
INSERT INTO `jiecheng_area` VALUES ('460', '150200', '150271', '包头稀土高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('461', '150300', '150301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('462', '150300', '150302', '海勃湾区');
INSERT INTO `jiecheng_area` VALUES ('463', '150300', '150303', '海南区');
INSERT INTO `jiecheng_area` VALUES ('464', '150300', '150304', '乌达区');
INSERT INTO `jiecheng_area` VALUES ('465', '150400', '150401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('466', '150400', '150402', '红山区');
INSERT INTO `jiecheng_area` VALUES ('467', '150400', '150403', '元宝山区');
INSERT INTO `jiecheng_area` VALUES ('468', '150400', '150404', '松山区');
INSERT INTO `jiecheng_area` VALUES ('469', '150400', '150421', '阿鲁科尔沁旗');
INSERT INTO `jiecheng_area` VALUES ('470', '150400', '150422', '巴林左旗');
INSERT INTO `jiecheng_area` VALUES ('471', '150400', '150423', '巴林右旗');
INSERT INTO `jiecheng_area` VALUES ('472', '150400', '150424', '林西县');
INSERT INTO `jiecheng_area` VALUES ('473', '150400', '150425', '克什克腾旗');
INSERT INTO `jiecheng_area` VALUES ('474', '150400', '150426', '翁牛特旗');
INSERT INTO `jiecheng_area` VALUES ('475', '150400', '150428', '喀喇沁旗');
INSERT INTO `jiecheng_area` VALUES ('476', '150400', '150429', '宁城县');
INSERT INTO `jiecheng_area` VALUES ('477', '150400', '150430', '敖汉旗');
INSERT INTO `jiecheng_area` VALUES ('478', '150500', '150501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('479', '150500', '150502', '科尔沁区');
INSERT INTO `jiecheng_area` VALUES ('480', '150500', '150521', '科尔沁左翼中旗');
INSERT INTO `jiecheng_area` VALUES ('481', '150500', '150522', '科尔沁左翼后旗');
INSERT INTO `jiecheng_area` VALUES ('482', '150500', '150523', '开鲁县');
INSERT INTO `jiecheng_area` VALUES ('483', '150500', '150524', '库伦旗');
INSERT INTO `jiecheng_area` VALUES ('484', '150500', '150525', '奈曼旗');
INSERT INTO `jiecheng_area` VALUES ('485', '150500', '150526', '扎鲁特旗');
INSERT INTO `jiecheng_area` VALUES ('486', '150500', '150571', '通辽经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('487', '150500', '150581', '霍林郭勒市');
INSERT INTO `jiecheng_area` VALUES ('488', '150600', '150601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('489', '150600', '150602', '东胜区');
INSERT INTO `jiecheng_area` VALUES ('490', '150600', '150603', '康巴什区');
INSERT INTO `jiecheng_area` VALUES ('491', '150600', '150621', '达拉特旗');
INSERT INTO `jiecheng_area` VALUES ('492', '150600', '150622', '准格尔旗');
INSERT INTO `jiecheng_area` VALUES ('493', '150600', '150623', '鄂托克前旗');
INSERT INTO `jiecheng_area` VALUES ('494', '150600', '150624', '鄂托克旗');
INSERT INTO `jiecheng_area` VALUES ('495', '150600', '150625', '杭锦旗');
INSERT INTO `jiecheng_area` VALUES ('496', '150600', '150626', '乌审旗');
INSERT INTO `jiecheng_area` VALUES ('497', '150600', '150627', '伊金霍洛旗');
INSERT INTO `jiecheng_area` VALUES ('498', '150700', '150701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('499', '150700', '150702', '海拉尔区');
INSERT INTO `jiecheng_area` VALUES ('500', '150700', '150703', '扎赉诺尔区');
INSERT INTO `jiecheng_area` VALUES ('501', '150700', '150721', '阿荣旗');
INSERT INTO `jiecheng_area` VALUES ('502', '150700', '150722', '莫力达瓦达斡尔族自治旗');
INSERT INTO `jiecheng_area` VALUES ('503', '150700', '150723', '鄂伦春自治旗');
INSERT INTO `jiecheng_area` VALUES ('504', '150700', '150724', '鄂温克族自治旗');
INSERT INTO `jiecheng_area` VALUES ('505', '150700', '150725', '陈巴尔虎旗');
INSERT INTO `jiecheng_area` VALUES ('506', '150700', '150726', '新巴尔虎左旗');
INSERT INTO `jiecheng_area` VALUES ('507', '150700', '150727', '新巴尔虎右旗');
INSERT INTO `jiecheng_area` VALUES ('508', '150700', '150781', '满洲里市');
INSERT INTO `jiecheng_area` VALUES ('509', '150700', '150782', '牙克石市');
INSERT INTO `jiecheng_area` VALUES ('510', '150700', '150783', '扎兰屯市');
INSERT INTO `jiecheng_area` VALUES ('511', '150700', '150784', '额尔古纳市');
INSERT INTO `jiecheng_area` VALUES ('512', '150700', '150785', '根河市');
INSERT INTO `jiecheng_area` VALUES ('513', '150800', '150801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('514', '150800', '150802', '临河区');
INSERT INTO `jiecheng_area` VALUES ('515', '150800', '150821', '五原县');
INSERT INTO `jiecheng_area` VALUES ('516', '150800', '150822', '磴口县');
INSERT INTO `jiecheng_area` VALUES ('517', '150800', '150823', '乌拉特前旗');
INSERT INTO `jiecheng_area` VALUES ('518', '150800', '150824', '乌拉特中旗');
INSERT INTO `jiecheng_area` VALUES ('519', '150800', '150825', '乌拉特后旗');
INSERT INTO `jiecheng_area` VALUES ('520', '150800', '150826', '杭锦后旗');
INSERT INTO `jiecheng_area` VALUES ('521', '150900', '150901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('522', '150900', '150902', '集宁区');
INSERT INTO `jiecheng_area` VALUES ('523', '150900', '150921', '卓资县');
INSERT INTO `jiecheng_area` VALUES ('524', '150900', '150922', '化德县');
INSERT INTO `jiecheng_area` VALUES ('525', '150900', '150923', '商都县');
INSERT INTO `jiecheng_area` VALUES ('526', '150900', '150924', '兴和县');
INSERT INTO `jiecheng_area` VALUES ('527', '150900', '150925', '凉城县');
INSERT INTO `jiecheng_area` VALUES ('528', '150900', '150926', '察哈尔右翼前旗');
INSERT INTO `jiecheng_area` VALUES ('529', '150900', '150927', '察哈尔右翼中旗');
INSERT INTO `jiecheng_area` VALUES ('530', '150900', '150928', '察哈尔右翼后旗');
INSERT INTO `jiecheng_area` VALUES ('531', '150900', '150929', '四子王旗');
INSERT INTO `jiecheng_area` VALUES ('532', '150900', '150981', '丰镇市');
INSERT INTO `jiecheng_area` VALUES ('533', '152200', '152201', '乌兰浩特市');
INSERT INTO `jiecheng_area` VALUES ('534', '152200', '152202', '阿尔山市');
INSERT INTO `jiecheng_area` VALUES ('535', '152200', '152221', '科尔沁右翼前旗');
INSERT INTO `jiecheng_area` VALUES ('536', '152200', '152222', '科尔沁右翼中旗');
INSERT INTO `jiecheng_area` VALUES ('537', '152200', '152223', '扎赉特旗');
INSERT INTO `jiecheng_area` VALUES ('538', '152200', '152224', '突泉县');
INSERT INTO `jiecheng_area` VALUES ('539', '152500', '152501', '二连浩特市');
INSERT INTO `jiecheng_area` VALUES ('540', '152500', '152502', '锡林浩特市');
INSERT INTO `jiecheng_area` VALUES ('541', '152500', '152522', '阿巴嘎旗');
INSERT INTO `jiecheng_area` VALUES ('542', '152500', '152523', '苏尼特左旗');
INSERT INTO `jiecheng_area` VALUES ('543', '152500', '152524', '苏尼特右旗');
INSERT INTO `jiecheng_area` VALUES ('544', '152500', '152525', '东乌珠穆沁旗');
INSERT INTO `jiecheng_area` VALUES ('545', '152500', '152526', '西乌珠穆沁旗');
INSERT INTO `jiecheng_area` VALUES ('546', '152500', '152527', '太仆寺旗');
INSERT INTO `jiecheng_area` VALUES ('547', '152500', '152528', '镶黄旗');
INSERT INTO `jiecheng_area` VALUES ('548', '152500', '152529', '正镶白旗');
INSERT INTO `jiecheng_area` VALUES ('549', '152500', '152530', '正蓝旗');
INSERT INTO `jiecheng_area` VALUES ('550', '152500', '152531', '多伦县');
INSERT INTO `jiecheng_area` VALUES ('551', '152500', '152571', '乌拉盖管委会');
INSERT INTO `jiecheng_area` VALUES ('552', '152900', '152921', '阿拉善左旗');
INSERT INTO `jiecheng_area` VALUES ('553', '152900', '152922', '阿拉善右旗');
INSERT INTO `jiecheng_area` VALUES ('554', '152900', '152923', '额济纳旗');
INSERT INTO `jiecheng_area` VALUES ('555', '152900', '152971', '内蒙古阿拉善经济开发区');
INSERT INTO `jiecheng_area` VALUES ('556', '210000', '210100', '沈阳市');
INSERT INTO `jiecheng_area` VALUES ('557', '210000', '210200', '大连市');
INSERT INTO `jiecheng_area` VALUES ('558', '210000', '210300', '鞍山市');
INSERT INTO `jiecheng_area` VALUES ('559', '210000', '210400', '抚顺市');
INSERT INTO `jiecheng_area` VALUES ('560', '210000', '210500', '本溪市');
INSERT INTO `jiecheng_area` VALUES ('561', '210000', '210600', '丹东市');
INSERT INTO `jiecheng_area` VALUES ('562', '210000', '210700', '锦州市');
INSERT INTO `jiecheng_area` VALUES ('563', '210000', '210800', '营口市');
INSERT INTO `jiecheng_area` VALUES ('564', '210000', '210900', '阜新市');
INSERT INTO `jiecheng_area` VALUES ('565', '210000', '211000', '辽阳市');
INSERT INTO `jiecheng_area` VALUES ('566', '210000', '211100', '盘锦市');
INSERT INTO `jiecheng_area` VALUES ('567', '210000', '211200', '铁岭市');
INSERT INTO `jiecheng_area` VALUES ('568', '210000', '211300', '朝阳市');
INSERT INTO `jiecheng_area` VALUES ('569', '210000', '211400', '葫芦岛市');
INSERT INTO `jiecheng_area` VALUES ('570', '210100', '210101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('571', '210100', '210102', '和平区');
INSERT INTO `jiecheng_area` VALUES ('572', '210100', '210103', '沈河区');
INSERT INTO `jiecheng_area` VALUES ('573', '210100', '210104', '大东区');
INSERT INTO `jiecheng_area` VALUES ('574', '210100', '210105', '皇姑区');
INSERT INTO `jiecheng_area` VALUES ('575', '210100', '210106', '铁西区');
INSERT INTO `jiecheng_area` VALUES ('576', '210100', '210111', '苏家屯区');
INSERT INTO `jiecheng_area` VALUES ('577', '210100', '210112', '浑南区');
INSERT INTO `jiecheng_area` VALUES ('578', '210100', '210113', '沈北新区');
INSERT INTO `jiecheng_area` VALUES ('579', '210100', '210114', '于洪区');
INSERT INTO `jiecheng_area` VALUES ('580', '210100', '210115', '辽中区');
INSERT INTO `jiecheng_area` VALUES ('581', '210100', '210123', '康平县');
INSERT INTO `jiecheng_area` VALUES ('582', '210100', '210124', '法库县');
INSERT INTO `jiecheng_area` VALUES ('583', '210100', '210181', '新民市');
INSERT INTO `jiecheng_area` VALUES ('584', '210200', '210201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('585', '210200', '210202', '中山区');
INSERT INTO `jiecheng_area` VALUES ('586', '210200', '210203', '西岗区');
INSERT INTO `jiecheng_area` VALUES ('587', '210200', '210204', '沙河口区');
INSERT INTO `jiecheng_area` VALUES ('588', '210200', '210211', '甘井子区');
INSERT INTO `jiecheng_area` VALUES ('589', '210200', '210212', '旅顺口区');
INSERT INTO `jiecheng_area` VALUES ('590', '210200', '210213', '金州区');
INSERT INTO `jiecheng_area` VALUES ('591', '210200', '210214', '普兰店区');
INSERT INTO `jiecheng_area` VALUES ('592', '210200', '210224', '长海县');
INSERT INTO `jiecheng_area` VALUES ('593', '210200', '210281', '瓦房店市');
INSERT INTO `jiecheng_area` VALUES ('594', '210200', '210283', '庄河市');
INSERT INTO `jiecheng_area` VALUES ('595', '210300', '210301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('596', '210300', '210302', '铁东区');
INSERT INTO `jiecheng_area` VALUES ('597', '210300', '210303', '铁西区');
INSERT INTO `jiecheng_area` VALUES ('598', '210300', '210304', '立山区');
INSERT INTO `jiecheng_area` VALUES ('599', '210300', '210311', '千山区');
INSERT INTO `jiecheng_area` VALUES ('600', '210300', '210321', '台安县');
INSERT INTO `jiecheng_area` VALUES ('601', '210300', '210323', '岫岩满族自治县');
INSERT INTO `jiecheng_area` VALUES ('602', '210300', '210381', '海城市');
INSERT INTO `jiecheng_area` VALUES ('603', '210400', '210401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('604', '210400', '210402', '新抚区');
INSERT INTO `jiecheng_area` VALUES ('605', '210400', '210403', '东洲区');
INSERT INTO `jiecheng_area` VALUES ('606', '210400', '210404', '望花区');
INSERT INTO `jiecheng_area` VALUES ('607', '210400', '210411', '顺城区');
INSERT INTO `jiecheng_area` VALUES ('608', '210400', '210421', '抚顺县');
INSERT INTO `jiecheng_area` VALUES ('609', '210400', '210422', '新宾满族自治县');
INSERT INTO `jiecheng_area` VALUES ('610', '210400', '210423', '清原满族自治县');
INSERT INTO `jiecheng_area` VALUES ('611', '210500', '210501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('612', '210500', '210502', '平山区');
INSERT INTO `jiecheng_area` VALUES ('613', '210500', '210503', '溪湖区');
INSERT INTO `jiecheng_area` VALUES ('614', '210500', '210504', '明山区');
INSERT INTO `jiecheng_area` VALUES ('615', '210500', '210505', '南芬区');
INSERT INTO `jiecheng_area` VALUES ('616', '210500', '210521', '本溪满族自治县');
INSERT INTO `jiecheng_area` VALUES ('617', '210500', '210522', '桓仁满族自治县');
INSERT INTO `jiecheng_area` VALUES ('618', '210600', '210601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('619', '210600', '210602', '元宝区');
INSERT INTO `jiecheng_area` VALUES ('620', '210600', '210603', '振兴区');
INSERT INTO `jiecheng_area` VALUES ('621', '210600', '210604', '振安区');
INSERT INTO `jiecheng_area` VALUES ('622', '210600', '210624', '宽甸满族自治县');
INSERT INTO `jiecheng_area` VALUES ('623', '210600', '210681', '东港市');
INSERT INTO `jiecheng_area` VALUES ('624', '210600', '210682', '凤城市');
INSERT INTO `jiecheng_area` VALUES ('625', '210700', '210701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('626', '210700', '210702', '古塔区');
INSERT INTO `jiecheng_area` VALUES ('627', '210700', '210703', '凌河区');
INSERT INTO `jiecheng_area` VALUES ('628', '210700', '210711', '太和区');
INSERT INTO `jiecheng_area` VALUES ('629', '210700', '210726', '黑山县');
INSERT INTO `jiecheng_area` VALUES ('630', '210700', '210727', '义县');
INSERT INTO `jiecheng_area` VALUES ('631', '210700', '210781', '凌海市');
INSERT INTO `jiecheng_area` VALUES ('632', '210700', '210782', '北镇市');
INSERT INTO `jiecheng_area` VALUES ('633', '210800', '210801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('634', '210800', '210802', '站前区');
INSERT INTO `jiecheng_area` VALUES ('635', '210800', '210803', '西市区');
INSERT INTO `jiecheng_area` VALUES ('636', '210800', '210804', '鲅鱼圈区');
INSERT INTO `jiecheng_area` VALUES ('637', '210800', '210811', '老边区');
INSERT INTO `jiecheng_area` VALUES ('638', '210800', '210881', '盖州市');
INSERT INTO `jiecheng_area` VALUES ('639', '210800', '210882', '大石桥市');
INSERT INTO `jiecheng_area` VALUES ('640', '210900', '210901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('641', '210900', '210902', '海州区');
INSERT INTO `jiecheng_area` VALUES ('642', '210900', '210903', '新邱区');
INSERT INTO `jiecheng_area` VALUES ('643', '210900', '210904', '太平区');
INSERT INTO `jiecheng_area` VALUES ('644', '210900', '210905', '清河门区');
INSERT INTO `jiecheng_area` VALUES ('645', '210900', '210911', '细河区');
INSERT INTO `jiecheng_area` VALUES ('646', '210900', '210921', '阜新蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('647', '210900', '210922', '彰武县');
INSERT INTO `jiecheng_area` VALUES ('648', '211000', '211001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('649', '211000', '211002', '白塔区');
INSERT INTO `jiecheng_area` VALUES ('650', '211000', '211003', '文圣区');
INSERT INTO `jiecheng_area` VALUES ('651', '211000', '211004', '宏伟区');
INSERT INTO `jiecheng_area` VALUES ('652', '211000', '211005', '弓长岭区');
INSERT INTO `jiecheng_area` VALUES ('653', '211000', '211011', '太子河区');
INSERT INTO `jiecheng_area` VALUES ('654', '211000', '211021', '辽阳县');
INSERT INTO `jiecheng_area` VALUES ('655', '211000', '211081', '灯塔市');
INSERT INTO `jiecheng_area` VALUES ('656', '211100', '211101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('657', '211100', '211102', '双台子区');
INSERT INTO `jiecheng_area` VALUES ('658', '211100', '211103', '兴隆台区');
INSERT INTO `jiecheng_area` VALUES ('659', '211100', '211104', '大洼区');
INSERT INTO `jiecheng_area` VALUES ('660', '211100', '211122', '盘山县');
INSERT INTO `jiecheng_area` VALUES ('661', '211200', '211201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('662', '211200', '211202', '银州区');
INSERT INTO `jiecheng_area` VALUES ('663', '211200', '211204', '清河区');
INSERT INTO `jiecheng_area` VALUES ('664', '211200', '211221', '铁岭县');
INSERT INTO `jiecheng_area` VALUES ('665', '211200', '211223', '西丰县');
INSERT INTO `jiecheng_area` VALUES ('666', '211200', '211224', '昌图县');
INSERT INTO `jiecheng_area` VALUES ('667', '211200', '211281', '调兵山市');
INSERT INTO `jiecheng_area` VALUES ('668', '211200', '211282', '开原市');
INSERT INTO `jiecheng_area` VALUES ('669', '211300', '211301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('670', '211300', '211302', '双塔区');
INSERT INTO `jiecheng_area` VALUES ('671', '211300', '211303', '龙城区');
INSERT INTO `jiecheng_area` VALUES ('672', '211300', '211321', '朝阳县');
INSERT INTO `jiecheng_area` VALUES ('673', '211300', '211322', '建平县');
INSERT INTO `jiecheng_area` VALUES ('674', '211300', '211324', '喀喇沁左翼蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('675', '211300', '211381', '北票市');
INSERT INTO `jiecheng_area` VALUES ('676', '211300', '211382', '凌源市');
INSERT INTO `jiecheng_area` VALUES ('677', '211400', '211401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('678', '211400', '211402', '连山区');
INSERT INTO `jiecheng_area` VALUES ('679', '211400', '211403', '龙港区');
INSERT INTO `jiecheng_area` VALUES ('680', '211400', '211404', '南票区');
INSERT INTO `jiecheng_area` VALUES ('681', '211400', '211421', '绥中县');
INSERT INTO `jiecheng_area` VALUES ('682', '211400', '211422', '建昌县');
INSERT INTO `jiecheng_area` VALUES ('683', '211400', '211481', '兴城市');
INSERT INTO `jiecheng_area` VALUES ('684', '220000', '220100', '长春市');
INSERT INTO `jiecheng_area` VALUES ('685', '220000', '220200', '吉林市');
INSERT INTO `jiecheng_area` VALUES ('686', '220000', '220300', '四平市');
INSERT INTO `jiecheng_area` VALUES ('687', '220000', '220400', '辽源市');
INSERT INTO `jiecheng_area` VALUES ('688', '220000', '220500', '通化市');
INSERT INTO `jiecheng_area` VALUES ('689', '220000', '220600', '白山市');
INSERT INTO `jiecheng_area` VALUES ('690', '220000', '220700', '松原市');
INSERT INTO `jiecheng_area` VALUES ('691', '220000', '220800', '白城市');
INSERT INTO `jiecheng_area` VALUES ('692', '220000', '222400', '延边朝鲜族自治州');
INSERT INTO `jiecheng_area` VALUES ('693', '220100', '220101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('694', '220100', '220102', '南关区');
INSERT INTO `jiecheng_area` VALUES ('695', '220100', '220103', '宽城区');
INSERT INTO `jiecheng_area` VALUES ('696', '220100', '220104', '朝阳区');
INSERT INTO `jiecheng_area` VALUES ('697', '220100', '220105', '二道区');
INSERT INTO `jiecheng_area` VALUES ('698', '220100', '220106', '绿园区');
INSERT INTO `jiecheng_area` VALUES ('699', '220100', '220112', '双阳区');
INSERT INTO `jiecheng_area` VALUES ('700', '220100', '220113', '九台区');
INSERT INTO `jiecheng_area` VALUES ('701', '220100', '220122', '农安县');
INSERT INTO `jiecheng_area` VALUES ('702', '220100', '220171', '长春经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('703', '220100', '220172', '长春净月高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('704', '220100', '220173', '长春高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('705', '220100', '220174', '长春汽车经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('706', '220100', '220182', '榆树市');
INSERT INTO `jiecheng_area` VALUES ('707', '220100', '220183', '德惠市');
INSERT INTO `jiecheng_area` VALUES ('708', '220200', '220201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('709', '220200', '220202', '昌邑区');
INSERT INTO `jiecheng_area` VALUES ('710', '220200', '220203', '龙潭区');
INSERT INTO `jiecheng_area` VALUES ('711', '220200', '220204', '船营区');
INSERT INTO `jiecheng_area` VALUES ('712', '220200', '220211', '丰满区');
INSERT INTO `jiecheng_area` VALUES ('713', '220200', '220221', '永吉县');
INSERT INTO `jiecheng_area` VALUES ('714', '220200', '220271', '吉林经济开发区');
INSERT INTO `jiecheng_area` VALUES ('715', '220200', '220272', '吉林高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('716', '220200', '220273', '吉林中国新加坡食品区');
INSERT INTO `jiecheng_area` VALUES ('717', '220200', '220281', '蛟河市');
INSERT INTO `jiecheng_area` VALUES ('718', '220200', '220282', '桦甸市');
INSERT INTO `jiecheng_area` VALUES ('719', '220200', '220283', '舒兰市');
INSERT INTO `jiecheng_area` VALUES ('720', '220200', '220284', '磐石市');
INSERT INTO `jiecheng_area` VALUES ('721', '220300', '220301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('722', '220300', '220302', '铁西区');
INSERT INTO `jiecheng_area` VALUES ('723', '220300', '220303', '铁东区');
INSERT INTO `jiecheng_area` VALUES ('724', '220300', '220322', '梨树县');
INSERT INTO `jiecheng_area` VALUES ('725', '220300', '220323', '伊通满族自治县');
INSERT INTO `jiecheng_area` VALUES ('726', '220300', '220381', '公主岭市');
INSERT INTO `jiecheng_area` VALUES ('727', '220300', '220382', '双辽市');
INSERT INTO `jiecheng_area` VALUES ('728', '220400', '220401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('729', '220400', '220402', '龙山区');
INSERT INTO `jiecheng_area` VALUES ('730', '220400', '220403', '西安区');
INSERT INTO `jiecheng_area` VALUES ('731', '220400', '220421', '东丰县');
INSERT INTO `jiecheng_area` VALUES ('732', '220400', '220422', '东辽县');
INSERT INTO `jiecheng_area` VALUES ('733', '220500', '220501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('734', '220500', '220502', '东昌区');
INSERT INTO `jiecheng_area` VALUES ('735', '220500', '220503', '二道江区');
INSERT INTO `jiecheng_area` VALUES ('736', '220500', '220521', '通化县');
INSERT INTO `jiecheng_area` VALUES ('737', '220500', '220523', '辉南县');
INSERT INTO `jiecheng_area` VALUES ('738', '220500', '220524', '柳河县');
INSERT INTO `jiecheng_area` VALUES ('739', '220500', '220581', '梅河口市');
INSERT INTO `jiecheng_area` VALUES ('740', '220500', '220582', '集安市');
INSERT INTO `jiecheng_area` VALUES ('741', '220600', '220601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('742', '220600', '220602', '浑江区');
INSERT INTO `jiecheng_area` VALUES ('743', '220600', '220605', '江源区');
INSERT INTO `jiecheng_area` VALUES ('744', '220600', '220621', '抚松县');
INSERT INTO `jiecheng_area` VALUES ('745', '220600', '220622', '靖宇县');
INSERT INTO `jiecheng_area` VALUES ('746', '220600', '220623', '长白朝鲜族自治县');
INSERT INTO `jiecheng_area` VALUES ('747', '220600', '220681', '临江市');
INSERT INTO `jiecheng_area` VALUES ('748', '220700', '220701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('749', '220700', '220702', '宁江区');
INSERT INTO `jiecheng_area` VALUES ('750', '220700', '220721', '前郭尔罗斯蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('751', '220700', '220722', '长岭县');
INSERT INTO `jiecheng_area` VALUES ('752', '220700', '220723', '乾安县');
INSERT INTO `jiecheng_area` VALUES ('753', '220700', '220771', '吉林松原经济开发区');
INSERT INTO `jiecheng_area` VALUES ('754', '220700', '220781', '扶余市');
INSERT INTO `jiecheng_area` VALUES ('755', '220800', '220801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('756', '220800', '220802', '洮北区');
INSERT INTO `jiecheng_area` VALUES ('757', '220800', '220821', '镇赉县');
INSERT INTO `jiecheng_area` VALUES ('758', '220800', '220822', '通榆县');
INSERT INTO `jiecheng_area` VALUES ('759', '220800', '220871', '吉林白城经济开发区');
INSERT INTO `jiecheng_area` VALUES ('760', '220800', '220881', '洮南市');
INSERT INTO `jiecheng_area` VALUES ('761', '220800', '220882', '大安市');
INSERT INTO `jiecheng_area` VALUES ('762', '222400', '222401', '延吉市');
INSERT INTO `jiecheng_area` VALUES ('763', '222400', '222402', '图们市');
INSERT INTO `jiecheng_area` VALUES ('764', '222400', '222403', '敦化市');
INSERT INTO `jiecheng_area` VALUES ('765', '222400', '222404', '珲春市');
INSERT INTO `jiecheng_area` VALUES ('766', '222400', '222405', '龙井市');
INSERT INTO `jiecheng_area` VALUES ('767', '222400', '222406', '和龙市');
INSERT INTO `jiecheng_area` VALUES ('768', '222400', '222424', '汪清县');
INSERT INTO `jiecheng_area` VALUES ('769', '222400', '222426', '安图县');
INSERT INTO `jiecheng_area` VALUES ('770', '230000', '230100', '哈尔滨市');
INSERT INTO `jiecheng_area` VALUES ('771', '230000', '230200', '齐齐哈尔市');
INSERT INTO `jiecheng_area` VALUES ('772', '230000', '230300', '鸡西市');
INSERT INTO `jiecheng_area` VALUES ('773', '230000', '230400', '鹤岗市');
INSERT INTO `jiecheng_area` VALUES ('774', '230000', '230500', '双鸭山市');
INSERT INTO `jiecheng_area` VALUES ('775', '230000', '230600', '大庆市');
INSERT INTO `jiecheng_area` VALUES ('776', '230000', '230700', '伊春市');
INSERT INTO `jiecheng_area` VALUES ('777', '230000', '230800', '佳木斯市');
INSERT INTO `jiecheng_area` VALUES ('778', '230000', '230900', '七台河市');
INSERT INTO `jiecheng_area` VALUES ('779', '230000', '231000', '牡丹江市');
INSERT INTO `jiecheng_area` VALUES ('780', '230000', '231100', '黑河市');
INSERT INTO `jiecheng_area` VALUES ('781', '230000', '231200', '绥化市');
INSERT INTO `jiecheng_area` VALUES ('782', '230000', '232700', '大兴安岭地区');
INSERT INTO `jiecheng_area` VALUES ('783', '230100', '230101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('784', '230100', '230102', '道里区');
INSERT INTO `jiecheng_area` VALUES ('785', '230100', '230103', '南岗区');
INSERT INTO `jiecheng_area` VALUES ('786', '230100', '230104', '道外区');
INSERT INTO `jiecheng_area` VALUES ('787', '230100', '230108', '平房区');
INSERT INTO `jiecheng_area` VALUES ('788', '230100', '230109', '松北区');
INSERT INTO `jiecheng_area` VALUES ('789', '230100', '230110', '香坊区');
INSERT INTO `jiecheng_area` VALUES ('790', '230100', '230111', '呼兰区');
INSERT INTO `jiecheng_area` VALUES ('791', '230100', '230112', '阿城区');
INSERT INTO `jiecheng_area` VALUES ('792', '230100', '230113', '双城区');
INSERT INTO `jiecheng_area` VALUES ('793', '230100', '230123', '依兰县');
INSERT INTO `jiecheng_area` VALUES ('794', '230100', '230124', '方正县');
INSERT INTO `jiecheng_area` VALUES ('795', '230100', '230125', '宾县');
INSERT INTO `jiecheng_area` VALUES ('796', '230100', '230126', '巴彦县');
INSERT INTO `jiecheng_area` VALUES ('797', '230100', '230127', '木兰县');
INSERT INTO `jiecheng_area` VALUES ('798', '230100', '230128', '通河县');
INSERT INTO `jiecheng_area` VALUES ('799', '230100', '230129', '延寿县');
INSERT INTO `jiecheng_area` VALUES ('800', '230100', '230183', '尚志市');
INSERT INTO `jiecheng_area` VALUES ('801', '230100', '230184', '五常市');
INSERT INTO `jiecheng_area` VALUES ('802', '230200', '230201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('803', '230200', '230202', '龙沙区');
INSERT INTO `jiecheng_area` VALUES ('804', '230200', '230203', '建华区');
INSERT INTO `jiecheng_area` VALUES ('805', '230200', '230204', '铁锋区');
INSERT INTO `jiecheng_area` VALUES ('806', '230200', '230205', '昂昂溪区');
INSERT INTO `jiecheng_area` VALUES ('807', '230200', '230206', '富拉尔基区');
INSERT INTO `jiecheng_area` VALUES ('808', '230200', '230207', '碾子山区');
INSERT INTO `jiecheng_area` VALUES ('809', '230200', '230208', '梅里斯达斡尔族区');
INSERT INTO `jiecheng_area` VALUES ('810', '230200', '230221', '龙江县');
INSERT INTO `jiecheng_area` VALUES ('811', '230200', '230223', '依安县');
INSERT INTO `jiecheng_area` VALUES ('812', '230200', '230224', '泰来县');
INSERT INTO `jiecheng_area` VALUES ('813', '230200', '230225', '甘南县');
INSERT INTO `jiecheng_area` VALUES ('814', '230200', '230227', '富裕县');
INSERT INTO `jiecheng_area` VALUES ('815', '230200', '230229', '克山县');
INSERT INTO `jiecheng_area` VALUES ('816', '230200', '230230', '克东县');
INSERT INTO `jiecheng_area` VALUES ('817', '230200', '230231', '拜泉县');
INSERT INTO `jiecheng_area` VALUES ('818', '230200', '230281', '讷河市');
INSERT INTO `jiecheng_area` VALUES ('819', '230300', '230301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('820', '230300', '230302', '鸡冠区');
INSERT INTO `jiecheng_area` VALUES ('821', '230300', '230303', '恒山区');
INSERT INTO `jiecheng_area` VALUES ('822', '230300', '230304', '滴道区');
INSERT INTO `jiecheng_area` VALUES ('823', '230300', '230305', '梨树区');
INSERT INTO `jiecheng_area` VALUES ('824', '230300', '230306', '城子河区');
INSERT INTO `jiecheng_area` VALUES ('825', '230300', '230307', '麻山区');
INSERT INTO `jiecheng_area` VALUES ('826', '230300', '230321', '鸡东县');
INSERT INTO `jiecheng_area` VALUES ('827', '230300', '230381', '虎林市');
INSERT INTO `jiecheng_area` VALUES ('828', '230300', '230382', '密山市');
INSERT INTO `jiecheng_area` VALUES ('829', '230400', '230401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('830', '230400', '230402', '向阳区');
INSERT INTO `jiecheng_area` VALUES ('831', '230400', '230403', '工农区');
INSERT INTO `jiecheng_area` VALUES ('832', '230400', '230404', '南山区');
INSERT INTO `jiecheng_area` VALUES ('833', '230400', '230405', '兴安区');
INSERT INTO `jiecheng_area` VALUES ('834', '230400', '230406', '东山区');
INSERT INTO `jiecheng_area` VALUES ('835', '230400', '230407', '兴山区');
INSERT INTO `jiecheng_area` VALUES ('836', '230400', '230421', '萝北县');
INSERT INTO `jiecheng_area` VALUES ('837', '230400', '230422', '绥滨县');
INSERT INTO `jiecheng_area` VALUES ('838', '230500', '230501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('839', '230500', '230502', '尖山区');
INSERT INTO `jiecheng_area` VALUES ('840', '230500', '230503', '岭东区');
INSERT INTO `jiecheng_area` VALUES ('841', '230500', '230505', '四方台区');
INSERT INTO `jiecheng_area` VALUES ('842', '230500', '230506', '宝山区');
INSERT INTO `jiecheng_area` VALUES ('843', '230500', '230521', '集贤县');
INSERT INTO `jiecheng_area` VALUES ('844', '230500', '230522', '友谊县');
INSERT INTO `jiecheng_area` VALUES ('845', '230500', '230523', '宝清县');
INSERT INTO `jiecheng_area` VALUES ('846', '230500', '230524', '饶河县');
INSERT INTO `jiecheng_area` VALUES ('847', '230600', '230601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('848', '230600', '230602', '萨尔图区');
INSERT INTO `jiecheng_area` VALUES ('849', '230600', '230603', '龙凤区');
INSERT INTO `jiecheng_area` VALUES ('850', '230600', '230604', '让胡路区');
INSERT INTO `jiecheng_area` VALUES ('851', '230600', '230605', '红岗区');
INSERT INTO `jiecheng_area` VALUES ('852', '230600', '230606', '大同区');
INSERT INTO `jiecheng_area` VALUES ('853', '230600', '230621', '肇州县');
INSERT INTO `jiecheng_area` VALUES ('854', '230600', '230622', '肇源县');
INSERT INTO `jiecheng_area` VALUES ('855', '230600', '230623', '林甸县');
INSERT INTO `jiecheng_area` VALUES ('856', '230600', '230624', '杜尔伯特蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('857', '230600', '230671', '大庆高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('858', '230700', '230701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('859', '230700', '230717', '伊美区');
INSERT INTO `jiecheng_area` VALUES ('860', '230700', '230718', '乌翠区');
INSERT INTO `jiecheng_area` VALUES ('861', '230700', '230719', '友好区');
INSERT INTO `jiecheng_area` VALUES ('862', '230700', '230722', '嘉荫县');
INSERT INTO `jiecheng_area` VALUES ('863', '230700', '230723', '汤旺县');
INSERT INTO `jiecheng_area` VALUES ('864', '230700', '230724', '丰林县');
INSERT INTO `jiecheng_area` VALUES ('865', '230700', '230725', '大箐山县');
INSERT INTO `jiecheng_area` VALUES ('866', '230700', '230726', '南岔县');
INSERT INTO `jiecheng_area` VALUES ('867', '230700', '230751', '金林区');
INSERT INTO `jiecheng_area` VALUES ('868', '230700', '230781', '铁力市');
INSERT INTO `jiecheng_area` VALUES ('869', '230800', '230801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('870', '230800', '230803', '向阳区');
INSERT INTO `jiecheng_area` VALUES ('871', '230800', '230804', '前进区');
INSERT INTO `jiecheng_area` VALUES ('872', '230800', '230805', '东风区');
INSERT INTO `jiecheng_area` VALUES ('873', '230800', '230811', '郊区');
INSERT INTO `jiecheng_area` VALUES ('874', '230800', '230822', '桦南县');
INSERT INTO `jiecheng_area` VALUES ('875', '230800', '230826', '桦川县');
INSERT INTO `jiecheng_area` VALUES ('876', '230800', '230828', '汤原县');
INSERT INTO `jiecheng_area` VALUES ('877', '230800', '230881', '同江市');
INSERT INTO `jiecheng_area` VALUES ('878', '230800', '230882', '富锦市');
INSERT INTO `jiecheng_area` VALUES ('879', '230800', '230883', '抚远市');
INSERT INTO `jiecheng_area` VALUES ('880', '230900', '230901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('881', '230900', '230902', '新兴区');
INSERT INTO `jiecheng_area` VALUES ('882', '230900', '230903', '桃山区');
INSERT INTO `jiecheng_area` VALUES ('883', '230900', '230904', '茄子河区');
INSERT INTO `jiecheng_area` VALUES ('884', '230900', '230921', '勃利县');
INSERT INTO `jiecheng_area` VALUES ('885', '231000', '231001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('886', '231000', '231002', '东安区');
INSERT INTO `jiecheng_area` VALUES ('887', '231000', '231003', '阳明区');
INSERT INTO `jiecheng_area` VALUES ('888', '231000', '231004', '爱民区');
INSERT INTO `jiecheng_area` VALUES ('889', '231000', '231005', '西安区');
INSERT INTO `jiecheng_area` VALUES ('890', '231000', '231025', '林口县');
INSERT INTO `jiecheng_area` VALUES ('891', '231000', '231071', '牡丹江经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('892', '231000', '231081', '绥芬河市');
INSERT INTO `jiecheng_area` VALUES ('893', '231000', '231083', '海林市');
INSERT INTO `jiecheng_area` VALUES ('894', '231000', '231084', '宁安市');
INSERT INTO `jiecheng_area` VALUES ('895', '231000', '231085', '穆棱市');
INSERT INTO `jiecheng_area` VALUES ('896', '231000', '231086', '东宁市');
INSERT INTO `jiecheng_area` VALUES ('897', '231100', '231101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('898', '231100', '231102', '爱辉区');
INSERT INTO `jiecheng_area` VALUES ('899', '231100', '231123', '逊克县');
INSERT INTO `jiecheng_area` VALUES ('900', '231100', '231124', '孙吴县');
INSERT INTO `jiecheng_area` VALUES ('901', '231100', '231181', '北安市');
INSERT INTO `jiecheng_area` VALUES ('902', '231100', '231182', '五大连池市');
INSERT INTO `jiecheng_area` VALUES ('903', '231100', '231183', '嫩江市');
INSERT INTO `jiecheng_area` VALUES ('904', '231200', '231201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('905', '231200', '231202', '北林区');
INSERT INTO `jiecheng_area` VALUES ('906', '231200', '231221', '望奎县');
INSERT INTO `jiecheng_area` VALUES ('907', '231200', '231222', '兰西县');
INSERT INTO `jiecheng_area` VALUES ('908', '231200', '231223', '青冈县');
INSERT INTO `jiecheng_area` VALUES ('909', '231200', '231224', '庆安县');
INSERT INTO `jiecheng_area` VALUES ('910', '231200', '231225', '明水县');
INSERT INTO `jiecheng_area` VALUES ('911', '231200', '231226', '绥棱县');
INSERT INTO `jiecheng_area` VALUES ('912', '231200', '231281', '安达市');
INSERT INTO `jiecheng_area` VALUES ('913', '231200', '231282', '肇东市');
INSERT INTO `jiecheng_area` VALUES ('914', '231200', '231283', '海伦市');
INSERT INTO `jiecheng_area` VALUES ('915', '232700', '232701', '漠河市');
INSERT INTO `jiecheng_area` VALUES ('916', '232700', '232721', '呼玛县');
INSERT INTO `jiecheng_area` VALUES ('917', '232700', '232722', '塔河县');
INSERT INTO `jiecheng_area` VALUES ('918', '232700', '232761', '加格达奇区');
INSERT INTO `jiecheng_area` VALUES ('919', '232700', '232762', '松岭区');
INSERT INTO `jiecheng_area` VALUES ('920', '232700', '232763', '新林区');
INSERT INTO `jiecheng_area` VALUES ('921', '232700', '232764', '呼中区');
INSERT INTO `jiecheng_area` VALUES ('922', '310000', '310100', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('923', '310100', '310101', '黄浦区');
INSERT INTO `jiecheng_area` VALUES ('924', '310100', '310104', '徐汇区');
INSERT INTO `jiecheng_area` VALUES ('925', '310100', '310105', '长宁区');
INSERT INTO `jiecheng_area` VALUES ('926', '310100', '310106', '静安区');
INSERT INTO `jiecheng_area` VALUES ('927', '310100', '310107', '普陀区');
INSERT INTO `jiecheng_area` VALUES ('928', '310100', '310109', '虹口区');
INSERT INTO `jiecheng_area` VALUES ('929', '310100', '310110', '杨浦区');
INSERT INTO `jiecheng_area` VALUES ('930', '310100', '310112', '闵行区');
INSERT INTO `jiecheng_area` VALUES ('931', '310100', '310113', '宝山区');
INSERT INTO `jiecheng_area` VALUES ('932', '310100', '310114', '嘉定区');
INSERT INTO `jiecheng_area` VALUES ('933', '310100', '310115', '浦东新区');
INSERT INTO `jiecheng_area` VALUES ('934', '310100', '310116', '金山区');
INSERT INTO `jiecheng_area` VALUES ('935', '310100', '310117', '松江区');
INSERT INTO `jiecheng_area` VALUES ('936', '310100', '310118', '青浦区');
INSERT INTO `jiecheng_area` VALUES ('937', '310100', '310120', '奉贤区');
INSERT INTO `jiecheng_area` VALUES ('938', '310100', '310151', '崇明区');
INSERT INTO `jiecheng_area` VALUES ('939', '320000', '320100', '南京市');
INSERT INTO `jiecheng_area` VALUES ('940', '320000', '320200', '无锡市');
INSERT INTO `jiecheng_area` VALUES ('941', '320000', '320300', '徐州市');
INSERT INTO `jiecheng_area` VALUES ('942', '320000', '320400', '常州市');
INSERT INTO `jiecheng_area` VALUES ('943', '320000', '320500', '苏州市');
INSERT INTO `jiecheng_area` VALUES ('944', '320000', '320600', '南通市');
INSERT INTO `jiecheng_area` VALUES ('945', '320000', '320700', '连云港市');
INSERT INTO `jiecheng_area` VALUES ('946', '320000', '320800', '淮安市');
INSERT INTO `jiecheng_area` VALUES ('947', '320000', '320900', '盐城市');
INSERT INTO `jiecheng_area` VALUES ('948', '320000', '321000', '扬州市');
INSERT INTO `jiecheng_area` VALUES ('949', '320000', '321100', '镇江市');
INSERT INTO `jiecheng_area` VALUES ('950', '320000', '321200', '泰州市');
INSERT INTO `jiecheng_area` VALUES ('951', '320000', '321300', '宿迁市');
INSERT INTO `jiecheng_area` VALUES ('952', '320100', '320101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('953', '320100', '320102', '玄武区');
INSERT INTO `jiecheng_area` VALUES ('954', '320100', '320104', '秦淮区');
INSERT INTO `jiecheng_area` VALUES ('955', '320100', '320105', '建邺区');
INSERT INTO `jiecheng_area` VALUES ('956', '320100', '320106', '鼓楼区');
INSERT INTO `jiecheng_area` VALUES ('957', '320100', '320111', '浦口区');
INSERT INTO `jiecheng_area` VALUES ('958', '320100', '320113', '栖霞区');
INSERT INTO `jiecheng_area` VALUES ('959', '320100', '320114', '雨花台区');
INSERT INTO `jiecheng_area` VALUES ('960', '320100', '320115', '江宁区');
INSERT INTO `jiecheng_area` VALUES ('961', '320100', '320116', '六合区');
INSERT INTO `jiecheng_area` VALUES ('962', '320100', '320117', '溧水区');
INSERT INTO `jiecheng_area` VALUES ('963', '320100', '320118', '高淳区');
INSERT INTO `jiecheng_area` VALUES ('964', '320200', '320201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('965', '320200', '320205', '锡山区');
INSERT INTO `jiecheng_area` VALUES ('966', '320200', '320206', '惠山区');
INSERT INTO `jiecheng_area` VALUES ('967', '320200', '320211', '滨湖区');
INSERT INTO `jiecheng_area` VALUES ('968', '320200', '320213', '梁溪区');
INSERT INTO `jiecheng_area` VALUES ('969', '320200', '320214', '新吴区');
INSERT INTO `jiecheng_area` VALUES ('970', '320200', '320281', '江阴市');
INSERT INTO `jiecheng_area` VALUES ('971', '320200', '320282', '宜兴市');
INSERT INTO `jiecheng_area` VALUES ('972', '320300', '320301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('973', '320300', '320302', '鼓楼区');
INSERT INTO `jiecheng_area` VALUES ('974', '320300', '320303', '云龙区');
INSERT INTO `jiecheng_area` VALUES ('975', '320300', '320305', '贾汪区');
INSERT INTO `jiecheng_area` VALUES ('976', '320300', '320311', '泉山区');
INSERT INTO `jiecheng_area` VALUES ('977', '320300', '320312', '铜山区');
INSERT INTO `jiecheng_area` VALUES ('978', '320300', '320321', '丰县');
INSERT INTO `jiecheng_area` VALUES ('979', '320300', '320322', '沛县');
INSERT INTO `jiecheng_area` VALUES ('980', '320300', '320324', '睢宁县');
INSERT INTO `jiecheng_area` VALUES ('981', '320300', '320371', '徐州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('982', '320300', '320381', '新沂市');
INSERT INTO `jiecheng_area` VALUES ('983', '320300', '320382', '邳州市');
INSERT INTO `jiecheng_area` VALUES ('984', '320400', '320401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('985', '320400', '320402', '天宁区');
INSERT INTO `jiecheng_area` VALUES ('986', '320400', '320404', '钟楼区');
INSERT INTO `jiecheng_area` VALUES ('987', '320400', '320411', '新北区');
INSERT INTO `jiecheng_area` VALUES ('988', '320400', '320412', '武进区');
INSERT INTO `jiecheng_area` VALUES ('989', '320400', '320413', '金坛区');
INSERT INTO `jiecheng_area` VALUES ('990', '320400', '320481', '溧阳市');
INSERT INTO `jiecheng_area` VALUES ('991', '320500', '320501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('992', '320500', '320505', '虎丘区');
INSERT INTO `jiecheng_area` VALUES ('993', '320500', '320506', '吴中区');
INSERT INTO `jiecheng_area` VALUES ('994', '320500', '320507', '相城区');
INSERT INTO `jiecheng_area` VALUES ('995', '320500', '320508', '姑苏区');
INSERT INTO `jiecheng_area` VALUES ('996', '320500', '320509', '吴江区');
INSERT INTO `jiecheng_area` VALUES ('997', '320500', '320571', '苏州工业园区');
INSERT INTO `jiecheng_area` VALUES ('998', '320500', '320581', '常熟市');
INSERT INTO `jiecheng_area` VALUES ('999', '320500', '320582', '张家港市');
INSERT INTO `jiecheng_area` VALUES ('1000', '320500', '320583', '昆山市');
INSERT INTO `jiecheng_area` VALUES ('1001', '320500', '320585', '太仓市');
INSERT INTO `jiecheng_area` VALUES ('1002', '320600', '320601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1003', '320600', '320602', '崇川区');
INSERT INTO `jiecheng_area` VALUES ('1004', '320600', '320611', '港闸区');
INSERT INTO `jiecheng_area` VALUES ('1005', '320600', '320612', '通州区');
INSERT INTO `jiecheng_area` VALUES ('1006', '320600', '320623', '如东县');
INSERT INTO `jiecheng_area` VALUES ('1007', '320600', '320671', '南通经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1008', '320600', '320681', '启东市');
INSERT INTO `jiecheng_area` VALUES ('1009', '320600', '320682', '如皋市');
INSERT INTO `jiecheng_area` VALUES ('1010', '320600', '320684', '海门市');
INSERT INTO `jiecheng_area` VALUES ('1011', '320600', '320685', '海安市');
INSERT INTO `jiecheng_area` VALUES ('1012', '320700', '320701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1013', '320700', '320703', '连云区');
INSERT INTO `jiecheng_area` VALUES ('1014', '320700', '320706', '海州区');
INSERT INTO `jiecheng_area` VALUES ('1015', '320700', '320707', '赣榆区');
INSERT INTO `jiecheng_area` VALUES ('1016', '320700', '320722', '东海县');
INSERT INTO `jiecheng_area` VALUES ('1017', '320700', '320723', '灌云县');
INSERT INTO `jiecheng_area` VALUES ('1018', '320700', '320724', '灌南县');
INSERT INTO `jiecheng_area` VALUES ('1019', '320700', '320771', '连云港经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1020', '320700', '320772', '连云港高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1021', '320800', '320801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1022', '320800', '320803', '淮安区');
INSERT INTO `jiecheng_area` VALUES ('1023', '320800', '320804', '淮阴区');
INSERT INTO `jiecheng_area` VALUES ('1024', '320800', '320812', '清江浦区');
INSERT INTO `jiecheng_area` VALUES ('1025', '320800', '320813', '洪泽区');
INSERT INTO `jiecheng_area` VALUES ('1026', '320800', '320826', '涟水县');
INSERT INTO `jiecheng_area` VALUES ('1027', '320800', '320830', '盱眙县');
INSERT INTO `jiecheng_area` VALUES ('1028', '320800', '320831', '金湖县');
INSERT INTO `jiecheng_area` VALUES ('1029', '320800', '320871', '淮安经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1030', '320900', '320901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1031', '320900', '320902', '亭湖区');
INSERT INTO `jiecheng_area` VALUES ('1032', '320900', '320903', '盐都区');
INSERT INTO `jiecheng_area` VALUES ('1033', '320900', '320904', '大丰区');
INSERT INTO `jiecheng_area` VALUES ('1034', '320900', '320921', '响水县');
INSERT INTO `jiecheng_area` VALUES ('1035', '320900', '320922', '滨海县');
INSERT INTO `jiecheng_area` VALUES ('1036', '320900', '320923', '阜宁县');
INSERT INTO `jiecheng_area` VALUES ('1037', '320900', '320924', '射阳县');
INSERT INTO `jiecheng_area` VALUES ('1038', '320900', '320925', '建湖县');
INSERT INTO `jiecheng_area` VALUES ('1039', '320900', '320971', '盐城经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1040', '320900', '320981', '东台市');
INSERT INTO `jiecheng_area` VALUES ('1041', '321000', '321001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1042', '321000', '321002', '广陵区');
INSERT INTO `jiecheng_area` VALUES ('1043', '321000', '321003', '邗江区');
INSERT INTO `jiecheng_area` VALUES ('1044', '321000', '321012', '江都区');
INSERT INTO `jiecheng_area` VALUES ('1045', '321000', '321023', '宝应县');
INSERT INTO `jiecheng_area` VALUES ('1046', '321000', '321071', '扬州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1047', '321000', '321081', '仪征市');
INSERT INTO `jiecheng_area` VALUES ('1048', '321000', '321084', '高邮市');
INSERT INTO `jiecheng_area` VALUES ('1049', '321100', '321101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1050', '321100', '321102', '京口区');
INSERT INTO `jiecheng_area` VALUES ('1051', '321100', '321111', '润州区');
INSERT INTO `jiecheng_area` VALUES ('1052', '321100', '321112', '丹徒区');
INSERT INTO `jiecheng_area` VALUES ('1053', '321100', '321171', '镇江新区');
INSERT INTO `jiecheng_area` VALUES ('1054', '321100', '321181', '丹阳市');
INSERT INTO `jiecheng_area` VALUES ('1055', '321100', '321182', '扬中市');
INSERT INTO `jiecheng_area` VALUES ('1056', '321100', '321183', '句容市');
INSERT INTO `jiecheng_area` VALUES ('1057', '321200', '321201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1058', '321200', '321202', '海陵区');
INSERT INTO `jiecheng_area` VALUES ('1059', '321200', '321203', '高港区');
INSERT INTO `jiecheng_area` VALUES ('1060', '321200', '321204', '姜堰区');
INSERT INTO `jiecheng_area` VALUES ('1061', '321200', '321271', '泰州医药高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1062', '321200', '321281', '兴化市');
INSERT INTO `jiecheng_area` VALUES ('1063', '321200', '321282', '靖江市');
INSERT INTO `jiecheng_area` VALUES ('1064', '321200', '321283', '泰兴市');
INSERT INTO `jiecheng_area` VALUES ('1065', '321300', '321301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1066', '321300', '321302', '宿城区');
INSERT INTO `jiecheng_area` VALUES ('1067', '321300', '321311', '宿豫区');
INSERT INTO `jiecheng_area` VALUES ('1068', '321300', '321322', '沭阳县');
INSERT INTO `jiecheng_area` VALUES ('1069', '321300', '321323', '泗阳县');
INSERT INTO `jiecheng_area` VALUES ('1070', '321300', '321324', '泗洪县');
INSERT INTO `jiecheng_area` VALUES ('1071', '321300', '321371', '宿迁经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1072', '330000', '330100', '杭州市');
INSERT INTO `jiecheng_area` VALUES ('1073', '330000', '330200', '宁波市');
INSERT INTO `jiecheng_area` VALUES ('1074', '330000', '330300', '温州市');
INSERT INTO `jiecheng_area` VALUES ('1075', '330000', '330400', '嘉兴市');
INSERT INTO `jiecheng_area` VALUES ('1076', '330000', '330500', '湖州市');
INSERT INTO `jiecheng_area` VALUES ('1077', '330000', '330600', '绍兴市');
INSERT INTO `jiecheng_area` VALUES ('1078', '330000', '330700', '金华市');
INSERT INTO `jiecheng_area` VALUES ('1079', '330000', '330800', '衢州市');
INSERT INTO `jiecheng_area` VALUES ('1080', '330000', '330900', '舟山市');
INSERT INTO `jiecheng_area` VALUES ('1081', '330000', '331000', '台州市');
INSERT INTO `jiecheng_area` VALUES ('1082', '330000', '331100', '丽水市');
INSERT INTO `jiecheng_area` VALUES ('1083', '330100', '330101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1084', '330100', '330102', '上城区');
INSERT INTO `jiecheng_area` VALUES ('1085', '330100', '330103', '下城区');
INSERT INTO `jiecheng_area` VALUES ('1086', '330100', '330104', '江干区');
INSERT INTO `jiecheng_area` VALUES ('1087', '330100', '330105', '拱墅区');
INSERT INTO `jiecheng_area` VALUES ('1088', '330100', '330106', '西湖区');
INSERT INTO `jiecheng_area` VALUES ('1089', '330100', '330108', '滨江区');
INSERT INTO `jiecheng_area` VALUES ('1090', '330100', '330109', '萧山区');
INSERT INTO `jiecheng_area` VALUES ('1091', '330100', '330110', '余杭区');
INSERT INTO `jiecheng_area` VALUES ('1092', '330100', '330111', '富阳区');
INSERT INTO `jiecheng_area` VALUES ('1093', '330100', '330112', '临安区');
INSERT INTO `jiecheng_area` VALUES ('1094', '330100', '330122', '桐庐县');
INSERT INTO `jiecheng_area` VALUES ('1095', '330100', '330127', '淳安县');
INSERT INTO `jiecheng_area` VALUES ('1096', '330100', '330182', '建德市');
INSERT INTO `jiecheng_area` VALUES ('1097', '330200', '330201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1098', '330200', '330203', '海曙区');
INSERT INTO `jiecheng_area` VALUES ('1099', '330200', '330205', '江北区');
INSERT INTO `jiecheng_area` VALUES ('1100', '330200', '330206', '北仑区');
INSERT INTO `jiecheng_area` VALUES ('1101', '330200', '330211', '镇海区');
INSERT INTO `jiecheng_area` VALUES ('1102', '330200', '330212', '鄞州区');
INSERT INTO `jiecheng_area` VALUES ('1103', '330200', '330213', '奉化区');
INSERT INTO `jiecheng_area` VALUES ('1104', '330200', '330225', '象山县');
INSERT INTO `jiecheng_area` VALUES ('1105', '330200', '330226', '宁海县');
INSERT INTO `jiecheng_area` VALUES ('1106', '330200', '330281', '余姚市');
INSERT INTO `jiecheng_area` VALUES ('1107', '330200', '330282', '慈溪市');
INSERT INTO `jiecheng_area` VALUES ('1108', '330300', '330301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1109', '330300', '330302', '鹿城区');
INSERT INTO `jiecheng_area` VALUES ('1110', '330300', '330303', '龙湾区');
INSERT INTO `jiecheng_area` VALUES ('1111', '330300', '330304', '瓯海区');
INSERT INTO `jiecheng_area` VALUES ('1112', '330300', '330305', '洞头区');
INSERT INTO `jiecheng_area` VALUES ('1113', '330300', '330324', '永嘉县');
INSERT INTO `jiecheng_area` VALUES ('1114', '330300', '330326', '平阳县');
INSERT INTO `jiecheng_area` VALUES ('1115', '330300', '330327', '苍南县');
INSERT INTO `jiecheng_area` VALUES ('1116', '330300', '330328', '文成县');
INSERT INTO `jiecheng_area` VALUES ('1117', '330300', '330329', '泰顺县');
INSERT INTO `jiecheng_area` VALUES ('1118', '330300', '330371', '温州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1119', '330300', '330381', '瑞安市');
INSERT INTO `jiecheng_area` VALUES ('1120', '330300', '330382', '乐清市');
INSERT INTO `jiecheng_area` VALUES ('1121', '330300', '330383', '龙港市');
INSERT INTO `jiecheng_area` VALUES ('1122', '330400', '330401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1123', '330400', '330402', '南湖区');
INSERT INTO `jiecheng_area` VALUES ('1124', '330400', '330411', '秀洲区');
INSERT INTO `jiecheng_area` VALUES ('1125', '330400', '330421', '嘉善县');
INSERT INTO `jiecheng_area` VALUES ('1126', '330400', '330424', '海盐县');
INSERT INTO `jiecheng_area` VALUES ('1127', '330400', '330481', '海宁市');
INSERT INTO `jiecheng_area` VALUES ('1128', '330400', '330482', '平湖市');
INSERT INTO `jiecheng_area` VALUES ('1129', '330400', '330483', '桐乡市');
INSERT INTO `jiecheng_area` VALUES ('1130', '330500', '330501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1131', '330500', '330502', '吴兴区');
INSERT INTO `jiecheng_area` VALUES ('1132', '330500', '330503', '南浔区');
INSERT INTO `jiecheng_area` VALUES ('1133', '330500', '330521', '德清县');
INSERT INTO `jiecheng_area` VALUES ('1134', '330500', '330522', '长兴县');
INSERT INTO `jiecheng_area` VALUES ('1135', '330500', '330523', '安吉县');
INSERT INTO `jiecheng_area` VALUES ('1136', '330600', '330601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1137', '330600', '330602', '越城区');
INSERT INTO `jiecheng_area` VALUES ('1138', '330600', '330603', '柯桥区');
INSERT INTO `jiecheng_area` VALUES ('1139', '330600', '330604', '上虞区');
INSERT INTO `jiecheng_area` VALUES ('1140', '330600', '330624', '新昌县');
INSERT INTO `jiecheng_area` VALUES ('1141', '330600', '330681', '诸暨市');
INSERT INTO `jiecheng_area` VALUES ('1142', '330600', '330683', '嵊州市');
INSERT INTO `jiecheng_area` VALUES ('1143', '330700', '330701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1144', '330700', '330702', '婺城区');
INSERT INTO `jiecheng_area` VALUES ('1145', '330700', '330703', '金东区');
INSERT INTO `jiecheng_area` VALUES ('1146', '330700', '330723', '武义县');
INSERT INTO `jiecheng_area` VALUES ('1147', '330700', '330726', '浦江县');
INSERT INTO `jiecheng_area` VALUES ('1148', '330700', '330727', '磐安县');
INSERT INTO `jiecheng_area` VALUES ('1149', '330700', '330781', '兰溪市');
INSERT INTO `jiecheng_area` VALUES ('1150', '330700', '330782', '义乌市');
INSERT INTO `jiecheng_area` VALUES ('1151', '330700', '330783', '东阳市');
INSERT INTO `jiecheng_area` VALUES ('1152', '330700', '330784', '永康市');
INSERT INTO `jiecheng_area` VALUES ('1153', '330800', '330801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1154', '330800', '330802', '柯城区');
INSERT INTO `jiecheng_area` VALUES ('1155', '330800', '330803', '衢江区');
INSERT INTO `jiecheng_area` VALUES ('1156', '330800', '330822', '常山县');
INSERT INTO `jiecheng_area` VALUES ('1157', '330800', '330824', '开化县');
INSERT INTO `jiecheng_area` VALUES ('1158', '330800', '330825', '龙游县');
INSERT INTO `jiecheng_area` VALUES ('1159', '330800', '330881', '江山市');
INSERT INTO `jiecheng_area` VALUES ('1160', '330900', '330901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1161', '330900', '330902', '定海区');
INSERT INTO `jiecheng_area` VALUES ('1162', '330900', '330903', '普陀区');
INSERT INTO `jiecheng_area` VALUES ('1163', '330900', '330921', '岱山县');
INSERT INTO `jiecheng_area` VALUES ('1164', '330900', '330922', '嵊泗县');
INSERT INTO `jiecheng_area` VALUES ('1165', '331000', '331001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1166', '331000', '331002', '椒江区');
INSERT INTO `jiecheng_area` VALUES ('1167', '331000', '331003', '黄岩区');
INSERT INTO `jiecheng_area` VALUES ('1168', '331000', '331004', '路桥区');
INSERT INTO `jiecheng_area` VALUES ('1169', '331000', '331022', '三门县');
INSERT INTO `jiecheng_area` VALUES ('1170', '331000', '331023', '天台县');
INSERT INTO `jiecheng_area` VALUES ('1171', '331000', '331024', '仙居县');
INSERT INTO `jiecheng_area` VALUES ('1172', '331000', '331081', '温岭市');
INSERT INTO `jiecheng_area` VALUES ('1173', '331000', '331082', '临海市');
INSERT INTO `jiecheng_area` VALUES ('1174', '331000', '331083', '玉环市');
INSERT INTO `jiecheng_area` VALUES ('1175', '331100', '331101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1176', '331100', '331102', '莲都区');
INSERT INTO `jiecheng_area` VALUES ('1177', '331100', '331121', '青田县');
INSERT INTO `jiecheng_area` VALUES ('1178', '331100', '331122', '缙云县');
INSERT INTO `jiecheng_area` VALUES ('1179', '331100', '331123', '遂昌县');
INSERT INTO `jiecheng_area` VALUES ('1180', '331100', '331124', '松阳县');
INSERT INTO `jiecheng_area` VALUES ('1181', '331100', '331125', '云和县');
INSERT INTO `jiecheng_area` VALUES ('1182', '331100', '331126', '庆元县');
INSERT INTO `jiecheng_area` VALUES ('1183', '331100', '331127', '景宁畲族自治县');
INSERT INTO `jiecheng_area` VALUES ('1184', '331100', '331181', '龙泉市');
INSERT INTO `jiecheng_area` VALUES ('1185', '340000', '340100', '合肥市');
INSERT INTO `jiecheng_area` VALUES ('1186', '340000', '340200', '芜湖市');
INSERT INTO `jiecheng_area` VALUES ('1187', '340000', '340300', '蚌埠市');
INSERT INTO `jiecheng_area` VALUES ('1188', '340000', '340400', '淮南市');
INSERT INTO `jiecheng_area` VALUES ('1189', '340000', '340500', '马鞍山市');
INSERT INTO `jiecheng_area` VALUES ('1190', '340000', '340600', '淮北市');
INSERT INTO `jiecheng_area` VALUES ('1191', '340000', '340700', '铜陵市');
INSERT INTO `jiecheng_area` VALUES ('1192', '340000', '340800', '安庆市');
INSERT INTO `jiecheng_area` VALUES ('1193', '340000', '341000', '黄山市');
INSERT INTO `jiecheng_area` VALUES ('1194', '340000', '341100', '滁州市');
INSERT INTO `jiecheng_area` VALUES ('1195', '340000', '341200', '阜阳市');
INSERT INTO `jiecheng_area` VALUES ('1196', '340000', '341300', '宿州市');
INSERT INTO `jiecheng_area` VALUES ('1197', '340000', '341500', '六安市');
INSERT INTO `jiecheng_area` VALUES ('1198', '340000', '341600', '亳州市');
INSERT INTO `jiecheng_area` VALUES ('1199', '340000', '341700', '池州市');
INSERT INTO `jiecheng_area` VALUES ('1200', '340000', '341800', '宣城市');
INSERT INTO `jiecheng_area` VALUES ('1201', '340100', '340101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1202', '340100', '340102', '瑶海区');
INSERT INTO `jiecheng_area` VALUES ('1203', '340100', '340103', '庐阳区');
INSERT INTO `jiecheng_area` VALUES ('1204', '340100', '340104', '蜀山区');
INSERT INTO `jiecheng_area` VALUES ('1205', '340100', '340111', '包河区');
INSERT INTO `jiecheng_area` VALUES ('1206', '340100', '340121', '长丰县');
INSERT INTO `jiecheng_area` VALUES ('1207', '340100', '340122', '肥东县');
INSERT INTO `jiecheng_area` VALUES ('1208', '340100', '340123', '肥西县');
INSERT INTO `jiecheng_area` VALUES ('1209', '340100', '340124', '庐江县');
INSERT INTO `jiecheng_area` VALUES ('1210', '340100', '340171', '合肥高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1211', '340100', '340172', '合肥经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1212', '340100', '340173', '合肥新站高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1213', '340100', '340181', '巢湖市');
INSERT INTO `jiecheng_area` VALUES ('1214', '340200', '340201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1215', '340200', '340202', '镜湖区');
INSERT INTO `jiecheng_area` VALUES ('1216', '340200', '340203', '弋江区');
INSERT INTO `jiecheng_area` VALUES ('1217', '340200', '340207', '鸠江区');
INSERT INTO `jiecheng_area` VALUES ('1218', '340200', '340208', '三山区');
INSERT INTO `jiecheng_area` VALUES ('1219', '340200', '340221', '芜湖县');
INSERT INTO `jiecheng_area` VALUES ('1220', '340200', '340222', '繁昌县');
INSERT INTO `jiecheng_area` VALUES ('1221', '340200', '340223', '南陵县');
INSERT INTO `jiecheng_area` VALUES ('1222', '340200', '340225', '无为县');
INSERT INTO `jiecheng_area` VALUES ('1223', '340200', '340271', '芜湖经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1224', '340200', '340272', '安徽芜湖长江大桥经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1225', '340300', '340301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1226', '340300', '340302', '龙子湖区');
INSERT INTO `jiecheng_area` VALUES ('1227', '340300', '340303', '蚌山区');
INSERT INTO `jiecheng_area` VALUES ('1228', '340300', '340304', '禹会区');
INSERT INTO `jiecheng_area` VALUES ('1229', '340300', '340311', '淮上区');
INSERT INTO `jiecheng_area` VALUES ('1230', '340300', '340321', '怀远县');
INSERT INTO `jiecheng_area` VALUES ('1231', '340300', '340322', '五河县');
INSERT INTO `jiecheng_area` VALUES ('1232', '340300', '340323', '固镇县');
INSERT INTO `jiecheng_area` VALUES ('1233', '340300', '340371', '蚌埠市高新技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1234', '340300', '340372', '蚌埠市经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1235', '340400', '340401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1236', '340400', '340402', '大通区');
INSERT INTO `jiecheng_area` VALUES ('1237', '340400', '340403', '田家庵区');
INSERT INTO `jiecheng_area` VALUES ('1238', '340400', '340404', '谢家集区');
INSERT INTO `jiecheng_area` VALUES ('1239', '340400', '340405', '八公山区');
INSERT INTO `jiecheng_area` VALUES ('1240', '340400', '340406', '潘集区');
INSERT INTO `jiecheng_area` VALUES ('1241', '340400', '340421', '凤台县');
INSERT INTO `jiecheng_area` VALUES ('1242', '340400', '340422', '寿县');
INSERT INTO `jiecheng_area` VALUES ('1243', '340500', '340501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1244', '340500', '340503', '花山区');
INSERT INTO `jiecheng_area` VALUES ('1245', '340500', '340504', '雨山区');
INSERT INTO `jiecheng_area` VALUES ('1246', '340500', '340506', '博望区');
INSERT INTO `jiecheng_area` VALUES ('1247', '340500', '340521', '当涂县');
INSERT INTO `jiecheng_area` VALUES ('1248', '340500', '340522', '含山县');
INSERT INTO `jiecheng_area` VALUES ('1249', '340500', '340523', '和县');
INSERT INTO `jiecheng_area` VALUES ('1250', '340600', '340601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1251', '340600', '340602', '杜集区');
INSERT INTO `jiecheng_area` VALUES ('1252', '340600', '340603', '相山区');
INSERT INTO `jiecheng_area` VALUES ('1253', '340600', '340604', '烈山区');
INSERT INTO `jiecheng_area` VALUES ('1254', '340600', '340621', '濉溪县');
INSERT INTO `jiecheng_area` VALUES ('1255', '340700', '340701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1256', '340700', '340705', '铜官区');
INSERT INTO `jiecheng_area` VALUES ('1257', '340700', '340706', '义安区');
INSERT INTO `jiecheng_area` VALUES ('1258', '340700', '340711', '郊区');
INSERT INTO `jiecheng_area` VALUES ('1259', '340700', '340722', '枞阳县');
INSERT INTO `jiecheng_area` VALUES ('1260', '340800', '340801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1261', '340800', '340802', '迎江区');
INSERT INTO `jiecheng_area` VALUES ('1262', '340800', '340803', '大观区');
INSERT INTO `jiecheng_area` VALUES ('1263', '340800', '340811', '宜秀区');
INSERT INTO `jiecheng_area` VALUES ('1264', '340800', '340822', '怀宁县');
INSERT INTO `jiecheng_area` VALUES ('1265', '340800', '340825', '太湖县');
INSERT INTO `jiecheng_area` VALUES ('1266', '340800', '340826', '宿松县');
INSERT INTO `jiecheng_area` VALUES ('1267', '340800', '340827', '望江县');
INSERT INTO `jiecheng_area` VALUES ('1268', '340800', '340828', '岳西县');
INSERT INTO `jiecheng_area` VALUES ('1269', '340800', '340871', '安徽安庆经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1270', '340800', '340881', '桐城市');
INSERT INTO `jiecheng_area` VALUES ('1271', '340800', '340882', '潜山市');
INSERT INTO `jiecheng_area` VALUES ('1272', '341000', '341001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1273', '341000', '341002', '屯溪区');
INSERT INTO `jiecheng_area` VALUES ('1274', '341000', '341003', '黄山区');
INSERT INTO `jiecheng_area` VALUES ('1275', '341000', '341004', '徽州区');
INSERT INTO `jiecheng_area` VALUES ('1276', '341000', '341021', '歙县');
INSERT INTO `jiecheng_area` VALUES ('1277', '341000', '341022', '休宁县');
INSERT INTO `jiecheng_area` VALUES ('1278', '341000', '341023', '黟县');
INSERT INTO `jiecheng_area` VALUES ('1279', '341000', '341024', '祁门县');
INSERT INTO `jiecheng_area` VALUES ('1280', '341100', '341101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1281', '341100', '341102', '琅琊区');
INSERT INTO `jiecheng_area` VALUES ('1282', '341100', '341103', '南谯区');
INSERT INTO `jiecheng_area` VALUES ('1283', '341100', '341122', '来安县');
INSERT INTO `jiecheng_area` VALUES ('1284', '341100', '341124', '全椒县');
INSERT INTO `jiecheng_area` VALUES ('1285', '341100', '341125', '定远县');
INSERT INTO `jiecheng_area` VALUES ('1286', '341100', '341126', '凤阳县');
INSERT INTO `jiecheng_area` VALUES ('1287', '341100', '341171', '苏滁现代产业园');
INSERT INTO `jiecheng_area` VALUES ('1288', '341100', '341172', '滁州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1289', '341100', '341181', '天长市');
INSERT INTO `jiecheng_area` VALUES ('1290', '341100', '341182', '明光市');
INSERT INTO `jiecheng_area` VALUES ('1291', '341200', '341201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1292', '341200', '341202', '颍州区');
INSERT INTO `jiecheng_area` VALUES ('1293', '341200', '341203', '颍东区');
INSERT INTO `jiecheng_area` VALUES ('1294', '341200', '341204', '颍泉区');
INSERT INTO `jiecheng_area` VALUES ('1295', '341200', '341221', '临泉县');
INSERT INTO `jiecheng_area` VALUES ('1296', '341200', '341222', '太和县');
INSERT INTO `jiecheng_area` VALUES ('1297', '341200', '341225', '阜南县');
INSERT INTO `jiecheng_area` VALUES ('1298', '341200', '341226', '颍上县');
INSERT INTO `jiecheng_area` VALUES ('1299', '341200', '341271', '阜阳合肥现代产业园区');
INSERT INTO `jiecheng_area` VALUES ('1300', '341200', '341272', '阜阳经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1301', '341200', '341282', '界首市');
INSERT INTO `jiecheng_area` VALUES ('1302', '341300', '341301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1303', '341300', '341302', '埇桥区');
INSERT INTO `jiecheng_area` VALUES ('1304', '341300', '341321', '砀山县');
INSERT INTO `jiecheng_area` VALUES ('1305', '341300', '341322', '萧县');
INSERT INTO `jiecheng_area` VALUES ('1306', '341300', '341323', '灵璧县');
INSERT INTO `jiecheng_area` VALUES ('1307', '341300', '341324', '泗县');
INSERT INTO `jiecheng_area` VALUES ('1308', '341300', '341371', '宿州马鞍山现代产业园区');
INSERT INTO `jiecheng_area` VALUES ('1309', '341300', '341372', '宿州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1310', '341500', '341501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1311', '341500', '341502', '金安区');
INSERT INTO `jiecheng_area` VALUES ('1312', '341500', '341503', '裕安区');
INSERT INTO `jiecheng_area` VALUES ('1313', '341500', '341504', '叶集区');
INSERT INTO `jiecheng_area` VALUES ('1314', '341500', '341522', '霍邱县');
INSERT INTO `jiecheng_area` VALUES ('1315', '341500', '341523', '舒城县');
INSERT INTO `jiecheng_area` VALUES ('1316', '341500', '341524', '金寨县');
INSERT INTO `jiecheng_area` VALUES ('1317', '341500', '341525', '霍山县');
INSERT INTO `jiecheng_area` VALUES ('1318', '341600', '341601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1319', '341600', '341602', '谯城区');
INSERT INTO `jiecheng_area` VALUES ('1320', '341600', '341621', '涡阳县');
INSERT INTO `jiecheng_area` VALUES ('1321', '341600', '341622', '蒙城县');
INSERT INTO `jiecheng_area` VALUES ('1322', '341600', '341623', '利辛县');
INSERT INTO `jiecheng_area` VALUES ('1323', '341700', '341701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1324', '341700', '341702', '贵池区');
INSERT INTO `jiecheng_area` VALUES ('1325', '341700', '341721', '东至县');
INSERT INTO `jiecheng_area` VALUES ('1326', '341700', '341722', '石台县');
INSERT INTO `jiecheng_area` VALUES ('1327', '341700', '341723', '青阳县');
INSERT INTO `jiecheng_area` VALUES ('1328', '341800', '341801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1329', '341800', '341802', '宣州区');
INSERT INTO `jiecheng_area` VALUES ('1330', '341800', '341821', '郎溪县');
INSERT INTO `jiecheng_area` VALUES ('1331', '341800', '341823', '泾县');
INSERT INTO `jiecheng_area` VALUES ('1332', '341800', '341824', '绩溪县');
INSERT INTO `jiecheng_area` VALUES ('1333', '341800', '341825', '旌德县');
INSERT INTO `jiecheng_area` VALUES ('1334', '341800', '341871', '宣城市经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1335', '341800', '341881', '宁国市');
INSERT INTO `jiecheng_area` VALUES ('1336', '341800', '341882', '广德市');
INSERT INTO `jiecheng_area` VALUES ('1337', '350000', '350100', '福州市');
INSERT INTO `jiecheng_area` VALUES ('1338', '350000', '350200', '厦门市');
INSERT INTO `jiecheng_area` VALUES ('1339', '350000', '350300', '莆田市');
INSERT INTO `jiecheng_area` VALUES ('1340', '350000', '350400', '三明市');
INSERT INTO `jiecheng_area` VALUES ('1341', '350000', '350500', '泉州市');
INSERT INTO `jiecheng_area` VALUES ('1342', '350000', '350600', '漳州市');
INSERT INTO `jiecheng_area` VALUES ('1343', '350000', '350700', '南平市');
INSERT INTO `jiecheng_area` VALUES ('1344', '350000', '350800', '龙岩市');
INSERT INTO `jiecheng_area` VALUES ('1345', '350000', '350900', '宁德市');
INSERT INTO `jiecheng_area` VALUES ('1346', '350100', '350101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1347', '350100', '350102', '鼓楼区');
INSERT INTO `jiecheng_area` VALUES ('1348', '350100', '350103', '台江区');
INSERT INTO `jiecheng_area` VALUES ('1349', '350100', '350104', '仓山区');
INSERT INTO `jiecheng_area` VALUES ('1350', '350100', '350105', '马尾区');
INSERT INTO `jiecheng_area` VALUES ('1351', '350100', '350111', '晋安区');
INSERT INTO `jiecheng_area` VALUES ('1352', '350100', '350112', '长乐区');
INSERT INTO `jiecheng_area` VALUES ('1353', '350100', '350121', '闽侯县');
INSERT INTO `jiecheng_area` VALUES ('1354', '350100', '350122', '连江县');
INSERT INTO `jiecheng_area` VALUES ('1355', '350100', '350123', '罗源县');
INSERT INTO `jiecheng_area` VALUES ('1356', '350100', '350124', '闽清县');
INSERT INTO `jiecheng_area` VALUES ('1357', '350100', '350125', '永泰县');
INSERT INTO `jiecheng_area` VALUES ('1358', '350100', '350128', '平潭县');
INSERT INTO `jiecheng_area` VALUES ('1359', '350100', '350181', '福清市');
INSERT INTO `jiecheng_area` VALUES ('1360', '350200', '350201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1361', '350200', '350203', '思明区');
INSERT INTO `jiecheng_area` VALUES ('1362', '350200', '350205', '海沧区');
INSERT INTO `jiecheng_area` VALUES ('1363', '350200', '350206', '湖里区');
INSERT INTO `jiecheng_area` VALUES ('1364', '350200', '350211', '集美区');
INSERT INTO `jiecheng_area` VALUES ('1365', '350200', '350212', '同安区');
INSERT INTO `jiecheng_area` VALUES ('1366', '350200', '350213', '翔安区');
INSERT INTO `jiecheng_area` VALUES ('1367', '350300', '350301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1368', '350300', '350302', '城厢区');
INSERT INTO `jiecheng_area` VALUES ('1369', '350300', '350303', '涵江区');
INSERT INTO `jiecheng_area` VALUES ('1370', '350300', '350304', '荔城区');
INSERT INTO `jiecheng_area` VALUES ('1371', '350300', '350305', '秀屿区');
INSERT INTO `jiecheng_area` VALUES ('1372', '350300', '350322', '仙游县');
INSERT INTO `jiecheng_area` VALUES ('1373', '350400', '350401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1374', '350400', '350402', '梅列区');
INSERT INTO `jiecheng_area` VALUES ('1375', '350400', '350403', '三元区');
INSERT INTO `jiecheng_area` VALUES ('1376', '350400', '350421', '明溪县');
INSERT INTO `jiecheng_area` VALUES ('1377', '350400', '350423', '清流县');
INSERT INTO `jiecheng_area` VALUES ('1378', '350400', '350424', '宁化县');
INSERT INTO `jiecheng_area` VALUES ('1379', '350400', '350425', '大田县');
INSERT INTO `jiecheng_area` VALUES ('1380', '350400', '350426', '尤溪县');
INSERT INTO `jiecheng_area` VALUES ('1381', '350400', '350427', '沙县');
INSERT INTO `jiecheng_area` VALUES ('1382', '350400', '350428', '将乐县');
INSERT INTO `jiecheng_area` VALUES ('1383', '350400', '350429', '泰宁县');
INSERT INTO `jiecheng_area` VALUES ('1384', '350400', '350430', '建宁县');
INSERT INTO `jiecheng_area` VALUES ('1385', '350400', '350481', '永安市');
INSERT INTO `jiecheng_area` VALUES ('1386', '350500', '350501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1387', '350500', '350502', '鲤城区');
INSERT INTO `jiecheng_area` VALUES ('1388', '350500', '350503', '丰泽区');
INSERT INTO `jiecheng_area` VALUES ('1389', '350500', '350504', '洛江区');
INSERT INTO `jiecheng_area` VALUES ('1390', '350500', '350505', '泉港区');
INSERT INTO `jiecheng_area` VALUES ('1391', '350500', '350521', '惠安县');
INSERT INTO `jiecheng_area` VALUES ('1392', '350500', '350524', '安溪县');
INSERT INTO `jiecheng_area` VALUES ('1393', '350500', '350525', '永春县');
INSERT INTO `jiecheng_area` VALUES ('1394', '350500', '350526', '德化县');
INSERT INTO `jiecheng_area` VALUES ('1395', '350500', '350527', '金门县');
INSERT INTO `jiecheng_area` VALUES ('1396', '350500', '350581', '石狮市');
INSERT INTO `jiecheng_area` VALUES ('1397', '350500', '350582', '晋江市');
INSERT INTO `jiecheng_area` VALUES ('1398', '350500', '350583', '南安市');
INSERT INTO `jiecheng_area` VALUES ('1399', '350600', '350601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1400', '350600', '350602', '芗城区');
INSERT INTO `jiecheng_area` VALUES ('1401', '350600', '350603', '龙文区');
INSERT INTO `jiecheng_area` VALUES ('1402', '350600', '350622', '云霄县');
INSERT INTO `jiecheng_area` VALUES ('1403', '350600', '350623', '漳浦县');
INSERT INTO `jiecheng_area` VALUES ('1404', '350600', '350624', '诏安县');
INSERT INTO `jiecheng_area` VALUES ('1405', '350600', '350625', '长泰县');
INSERT INTO `jiecheng_area` VALUES ('1406', '350600', '350626', '东山县');
INSERT INTO `jiecheng_area` VALUES ('1407', '350600', '350627', '南靖县');
INSERT INTO `jiecheng_area` VALUES ('1408', '350600', '350628', '平和县');
INSERT INTO `jiecheng_area` VALUES ('1409', '350600', '350629', '华安县');
INSERT INTO `jiecheng_area` VALUES ('1410', '350600', '350681', '龙海市');
INSERT INTO `jiecheng_area` VALUES ('1411', '350700', '350701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1412', '350700', '350702', '延平区');
INSERT INTO `jiecheng_area` VALUES ('1413', '350700', '350703', '建阳区');
INSERT INTO `jiecheng_area` VALUES ('1414', '350700', '350721', '顺昌县');
INSERT INTO `jiecheng_area` VALUES ('1415', '350700', '350722', '浦城县');
INSERT INTO `jiecheng_area` VALUES ('1416', '350700', '350723', '光泽县');
INSERT INTO `jiecheng_area` VALUES ('1417', '350700', '350724', '松溪县');
INSERT INTO `jiecheng_area` VALUES ('1418', '350700', '350725', '政和县');
INSERT INTO `jiecheng_area` VALUES ('1419', '350700', '350781', '邵武市');
INSERT INTO `jiecheng_area` VALUES ('1420', '350700', '350782', '武夷山市');
INSERT INTO `jiecheng_area` VALUES ('1421', '350700', '350783', '建瓯市');
INSERT INTO `jiecheng_area` VALUES ('1422', '350800', '350801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1423', '350800', '350802', '新罗区');
INSERT INTO `jiecheng_area` VALUES ('1424', '350800', '350803', '永定区');
INSERT INTO `jiecheng_area` VALUES ('1425', '350800', '350821', '长汀县');
INSERT INTO `jiecheng_area` VALUES ('1426', '350800', '350823', '上杭县');
INSERT INTO `jiecheng_area` VALUES ('1427', '350800', '350824', '武平县');
INSERT INTO `jiecheng_area` VALUES ('1428', '350800', '350825', '连城县');
INSERT INTO `jiecheng_area` VALUES ('1429', '350800', '350881', '漳平市');
INSERT INTO `jiecheng_area` VALUES ('1430', '350900', '350901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1431', '350900', '350902', '蕉城区');
INSERT INTO `jiecheng_area` VALUES ('1432', '350900', '350921', '霞浦县');
INSERT INTO `jiecheng_area` VALUES ('1433', '350900', '350922', '古田县');
INSERT INTO `jiecheng_area` VALUES ('1434', '350900', '350923', '屏南县');
INSERT INTO `jiecheng_area` VALUES ('1435', '350900', '350924', '寿宁县');
INSERT INTO `jiecheng_area` VALUES ('1436', '350900', '350925', '周宁县');
INSERT INTO `jiecheng_area` VALUES ('1437', '350900', '350926', '柘荣县');
INSERT INTO `jiecheng_area` VALUES ('1438', '350900', '350981', '福安市');
INSERT INTO `jiecheng_area` VALUES ('1439', '350900', '350982', '福鼎市');
INSERT INTO `jiecheng_area` VALUES ('1440', '360000', '360100', '南昌市');
INSERT INTO `jiecheng_area` VALUES ('1441', '360000', '360200', '景德镇市');
INSERT INTO `jiecheng_area` VALUES ('1442', '360000', '360300', '萍乡市');
INSERT INTO `jiecheng_area` VALUES ('1443', '360000', '360400', '九江市');
INSERT INTO `jiecheng_area` VALUES ('1444', '360000', '360500', '新余市');
INSERT INTO `jiecheng_area` VALUES ('1445', '360000', '360600', '鹰潭市');
INSERT INTO `jiecheng_area` VALUES ('1446', '360000', '360700', '赣州市');
INSERT INTO `jiecheng_area` VALUES ('1447', '360000', '360800', '吉安市');
INSERT INTO `jiecheng_area` VALUES ('1448', '360000', '360900', '宜春市');
INSERT INTO `jiecheng_area` VALUES ('1449', '360000', '361000', '抚州市');
INSERT INTO `jiecheng_area` VALUES ('1450', '360000', '361100', '上饶市');
INSERT INTO `jiecheng_area` VALUES ('1451', '360100', '360101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1452', '360100', '360102', '东湖区');
INSERT INTO `jiecheng_area` VALUES ('1453', '360100', '360103', '西湖区');
INSERT INTO `jiecheng_area` VALUES ('1454', '360100', '360104', '青云谱区');
INSERT INTO `jiecheng_area` VALUES ('1455', '360100', '360105', '湾里区');
INSERT INTO `jiecheng_area` VALUES ('1456', '360100', '360111', '青山湖区');
INSERT INTO `jiecheng_area` VALUES ('1457', '360100', '360112', '新建区');
INSERT INTO `jiecheng_area` VALUES ('1458', '360100', '360121', '南昌县');
INSERT INTO `jiecheng_area` VALUES ('1459', '360100', '360123', '安义县');
INSERT INTO `jiecheng_area` VALUES ('1460', '360100', '360124', '进贤县');
INSERT INTO `jiecheng_area` VALUES ('1461', '360200', '360201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1462', '360200', '360202', '昌江区');
INSERT INTO `jiecheng_area` VALUES ('1463', '360200', '360203', '珠山区');
INSERT INTO `jiecheng_area` VALUES ('1464', '360200', '360222', '浮梁县');
INSERT INTO `jiecheng_area` VALUES ('1465', '360200', '360281', '乐平市');
INSERT INTO `jiecheng_area` VALUES ('1466', '360300', '360301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1467', '360300', '360302', '安源区');
INSERT INTO `jiecheng_area` VALUES ('1468', '360300', '360313', '湘东区');
INSERT INTO `jiecheng_area` VALUES ('1469', '360300', '360321', '莲花县');
INSERT INTO `jiecheng_area` VALUES ('1470', '360300', '360322', '上栗县');
INSERT INTO `jiecheng_area` VALUES ('1471', '360300', '360323', '芦溪县');
INSERT INTO `jiecheng_area` VALUES ('1472', '360400', '360401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1473', '360400', '360402', '濂溪区');
INSERT INTO `jiecheng_area` VALUES ('1474', '360400', '360403', '浔阳区');
INSERT INTO `jiecheng_area` VALUES ('1475', '360400', '360404', '柴桑区');
INSERT INTO `jiecheng_area` VALUES ('1476', '360400', '360423', '武宁县');
INSERT INTO `jiecheng_area` VALUES ('1477', '360400', '360424', '修水县');
INSERT INTO `jiecheng_area` VALUES ('1478', '360400', '360425', '永修县');
INSERT INTO `jiecheng_area` VALUES ('1479', '360400', '360426', '德安县');
INSERT INTO `jiecheng_area` VALUES ('1480', '360400', '360428', '都昌县');
INSERT INTO `jiecheng_area` VALUES ('1481', '360400', '360429', '湖口县');
INSERT INTO `jiecheng_area` VALUES ('1482', '360400', '360430', '彭泽县');
INSERT INTO `jiecheng_area` VALUES ('1483', '360400', '360481', '瑞昌市');
INSERT INTO `jiecheng_area` VALUES ('1484', '360400', '360482', '共青城市');
INSERT INTO `jiecheng_area` VALUES ('1485', '360400', '360483', '庐山市');
INSERT INTO `jiecheng_area` VALUES ('1486', '360500', '360501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1487', '360500', '360502', '渝水区');
INSERT INTO `jiecheng_area` VALUES ('1488', '360500', '360521', '分宜县');
INSERT INTO `jiecheng_area` VALUES ('1489', '360600', '360601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1490', '360600', '360602', '月湖区');
INSERT INTO `jiecheng_area` VALUES ('1491', '360600', '360603', '余江区');
INSERT INTO `jiecheng_area` VALUES ('1492', '360600', '360681', '贵溪市');
INSERT INTO `jiecheng_area` VALUES ('1493', '360700', '360701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1494', '360700', '360702', '章贡区');
INSERT INTO `jiecheng_area` VALUES ('1495', '360700', '360703', '南康区');
INSERT INTO `jiecheng_area` VALUES ('1496', '360700', '360704', '赣县区');
INSERT INTO `jiecheng_area` VALUES ('1497', '360700', '360722', '信丰县');
INSERT INTO `jiecheng_area` VALUES ('1498', '360700', '360723', '大余县');
INSERT INTO `jiecheng_area` VALUES ('1499', '360700', '360724', '上犹县');
INSERT INTO `jiecheng_area` VALUES ('1500', '360700', '360725', '崇义县');
INSERT INTO `jiecheng_area` VALUES ('1501', '360700', '360726', '安远县');
INSERT INTO `jiecheng_area` VALUES ('1502', '360700', '360727', '龙南县');
INSERT INTO `jiecheng_area` VALUES ('1503', '360700', '360728', '定南县');
INSERT INTO `jiecheng_area` VALUES ('1504', '360700', '360729', '全南县');
INSERT INTO `jiecheng_area` VALUES ('1505', '360700', '360730', '宁都县');
INSERT INTO `jiecheng_area` VALUES ('1506', '360700', '360731', '于都县');
INSERT INTO `jiecheng_area` VALUES ('1507', '360700', '360732', '兴国县');
INSERT INTO `jiecheng_area` VALUES ('1508', '360700', '360733', '会昌县');
INSERT INTO `jiecheng_area` VALUES ('1509', '360700', '360734', '寻乌县');
INSERT INTO `jiecheng_area` VALUES ('1510', '360700', '360735', '石城县');
INSERT INTO `jiecheng_area` VALUES ('1511', '360700', '360781', '瑞金市');
INSERT INTO `jiecheng_area` VALUES ('1512', '360800', '360801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1513', '360800', '360802', '吉州区');
INSERT INTO `jiecheng_area` VALUES ('1514', '360800', '360803', '青原区');
INSERT INTO `jiecheng_area` VALUES ('1515', '360800', '360821', '吉安县');
INSERT INTO `jiecheng_area` VALUES ('1516', '360800', '360822', '吉水县');
INSERT INTO `jiecheng_area` VALUES ('1517', '360800', '360823', '峡江县');
INSERT INTO `jiecheng_area` VALUES ('1518', '360800', '360824', '新干县');
INSERT INTO `jiecheng_area` VALUES ('1519', '360800', '360825', '永丰县');
INSERT INTO `jiecheng_area` VALUES ('1520', '360800', '360826', '泰和县');
INSERT INTO `jiecheng_area` VALUES ('1521', '360800', '360827', '遂川县');
INSERT INTO `jiecheng_area` VALUES ('1522', '360800', '360828', '万安县');
INSERT INTO `jiecheng_area` VALUES ('1523', '360800', '360829', '安福县');
INSERT INTO `jiecheng_area` VALUES ('1524', '360800', '360830', '永新县');
INSERT INTO `jiecheng_area` VALUES ('1525', '360800', '360881', '井冈山市');
INSERT INTO `jiecheng_area` VALUES ('1526', '360900', '360901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1527', '360900', '360902', '袁州区');
INSERT INTO `jiecheng_area` VALUES ('1528', '360900', '360921', '奉新县');
INSERT INTO `jiecheng_area` VALUES ('1529', '360900', '360922', '万载县');
INSERT INTO `jiecheng_area` VALUES ('1530', '360900', '360923', '上高县');
INSERT INTO `jiecheng_area` VALUES ('1531', '360900', '360924', '宜丰县');
INSERT INTO `jiecheng_area` VALUES ('1532', '360900', '360925', '靖安县');
INSERT INTO `jiecheng_area` VALUES ('1533', '360900', '360926', '铜鼓县');
INSERT INTO `jiecheng_area` VALUES ('1534', '360900', '360981', '丰城市');
INSERT INTO `jiecheng_area` VALUES ('1535', '360900', '360982', '樟树市');
INSERT INTO `jiecheng_area` VALUES ('1536', '360900', '360983', '高安市');
INSERT INTO `jiecheng_area` VALUES ('1537', '361000', '361001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1538', '361000', '361002', '临川区');
INSERT INTO `jiecheng_area` VALUES ('1539', '361000', '361003', '东乡区');
INSERT INTO `jiecheng_area` VALUES ('1540', '361000', '361021', '南城县');
INSERT INTO `jiecheng_area` VALUES ('1541', '361000', '361022', '黎川县');
INSERT INTO `jiecheng_area` VALUES ('1542', '361000', '361023', '南丰县');
INSERT INTO `jiecheng_area` VALUES ('1543', '361000', '361024', '崇仁县');
INSERT INTO `jiecheng_area` VALUES ('1544', '361000', '361025', '乐安县');
INSERT INTO `jiecheng_area` VALUES ('1545', '361000', '361026', '宜黄县');
INSERT INTO `jiecheng_area` VALUES ('1546', '361000', '361027', '金溪县');
INSERT INTO `jiecheng_area` VALUES ('1547', '361000', '361028', '资溪县');
INSERT INTO `jiecheng_area` VALUES ('1548', '361000', '361030', '广昌县');
INSERT INTO `jiecheng_area` VALUES ('1549', '361100', '361101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1550', '361100', '361102', '信州区');
INSERT INTO `jiecheng_area` VALUES ('1551', '361100', '361103', '广丰区');
INSERT INTO `jiecheng_area` VALUES ('1552', '361100', '361104', '广信区');
INSERT INTO `jiecheng_area` VALUES ('1553', '361100', '361123', '玉山县');
INSERT INTO `jiecheng_area` VALUES ('1554', '361100', '361124', '铅山县');
INSERT INTO `jiecheng_area` VALUES ('1555', '361100', '361125', '横峰县');
INSERT INTO `jiecheng_area` VALUES ('1556', '361100', '361126', '弋阳县');
INSERT INTO `jiecheng_area` VALUES ('1557', '361100', '361127', '余干县');
INSERT INTO `jiecheng_area` VALUES ('1558', '361100', '361128', '鄱阳县');
INSERT INTO `jiecheng_area` VALUES ('1559', '361100', '361129', '万年县');
INSERT INTO `jiecheng_area` VALUES ('1560', '361100', '361130', '婺源县');
INSERT INTO `jiecheng_area` VALUES ('1561', '361100', '361181', '德兴市');
INSERT INTO `jiecheng_area` VALUES ('1562', '370000', '370100', '济南市');
INSERT INTO `jiecheng_area` VALUES ('1563', '370000', '370200', '青岛市');
INSERT INTO `jiecheng_area` VALUES ('1564', '370000', '370300', '淄博市');
INSERT INTO `jiecheng_area` VALUES ('1565', '370000', '370400', '枣庄市');
INSERT INTO `jiecheng_area` VALUES ('1566', '370000', '370500', '东营市');
INSERT INTO `jiecheng_area` VALUES ('1567', '370000', '370600', '烟台市');
INSERT INTO `jiecheng_area` VALUES ('1568', '370000', '370700', '潍坊市');
INSERT INTO `jiecheng_area` VALUES ('1569', '370000', '370800', '济宁市');
INSERT INTO `jiecheng_area` VALUES ('1570', '370000', '370900', '泰安市');
INSERT INTO `jiecheng_area` VALUES ('1571', '370000', '371000', '威海市');
INSERT INTO `jiecheng_area` VALUES ('1572', '370000', '371100', '日照市');
INSERT INTO `jiecheng_area` VALUES ('1573', '370000', '371300', '临沂市');
INSERT INTO `jiecheng_area` VALUES ('1574', '370000', '371400', '德州市');
INSERT INTO `jiecheng_area` VALUES ('1575', '370000', '371500', '聊城市');
INSERT INTO `jiecheng_area` VALUES ('1576', '370000', '371600', '滨州市');
INSERT INTO `jiecheng_area` VALUES ('1577', '370000', '371700', '菏泽市');
INSERT INTO `jiecheng_area` VALUES ('1578', '370100', '370101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1579', '370100', '370102', '历下区');
INSERT INTO `jiecheng_area` VALUES ('1580', '370100', '370103', '市中区');
INSERT INTO `jiecheng_area` VALUES ('1581', '370100', '370104', '槐荫区');
INSERT INTO `jiecheng_area` VALUES ('1582', '370100', '370105', '天桥区');
INSERT INTO `jiecheng_area` VALUES ('1583', '370100', '370112', '历城区');
INSERT INTO `jiecheng_area` VALUES ('1584', '370100', '370113', '长清区');
INSERT INTO `jiecheng_area` VALUES ('1585', '370100', '370114', '章丘区');
INSERT INTO `jiecheng_area` VALUES ('1586', '370100', '370115', '济阳区');
INSERT INTO `jiecheng_area` VALUES ('1587', '370100', '370116', '莱芜区');
INSERT INTO `jiecheng_area` VALUES ('1588', '370100', '370117', '钢城区');
INSERT INTO `jiecheng_area` VALUES ('1589', '370100', '370124', '平阴县');
INSERT INTO `jiecheng_area` VALUES ('1590', '370100', '370126', '商河县');
INSERT INTO `jiecheng_area` VALUES ('1591', '370100', '370171', '济南高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1592', '370200', '370201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1593', '370200', '370202', '市南区');
INSERT INTO `jiecheng_area` VALUES ('1594', '370200', '370203', '市北区');
INSERT INTO `jiecheng_area` VALUES ('1595', '370200', '370211', '黄岛区');
INSERT INTO `jiecheng_area` VALUES ('1596', '370200', '370212', '崂山区');
INSERT INTO `jiecheng_area` VALUES ('1597', '370200', '370213', '李沧区');
INSERT INTO `jiecheng_area` VALUES ('1598', '370200', '370214', '城阳区');
INSERT INTO `jiecheng_area` VALUES ('1599', '370200', '370215', '即墨区');
INSERT INTO `jiecheng_area` VALUES ('1600', '370200', '370271', '青岛高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1601', '370200', '370281', '胶州市');
INSERT INTO `jiecheng_area` VALUES ('1602', '370200', '370283', '平度市');
INSERT INTO `jiecheng_area` VALUES ('1603', '370200', '370285', '莱西市');
INSERT INTO `jiecheng_area` VALUES ('1604', '370300', '370301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1605', '370300', '370302', '淄川区');
INSERT INTO `jiecheng_area` VALUES ('1606', '370300', '370303', '张店区');
INSERT INTO `jiecheng_area` VALUES ('1607', '370300', '370304', '博山区');
INSERT INTO `jiecheng_area` VALUES ('1608', '370300', '370305', '临淄区');
INSERT INTO `jiecheng_area` VALUES ('1609', '370300', '370306', '周村区');
INSERT INTO `jiecheng_area` VALUES ('1610', '370300', '370321', '桓台县');
INSERT INTO `jiecheng_area` VALUES ('1611', '370300', '370322', '高青县');
INSERT INTO `jiecheng_area` VALUES ('1612', '370300', '370323', '沂源县');
INSERT INTO `jiecheng_area` VALUES ('1613', '370400', '370401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1614', '370400', '370402', '市中区');
INSERT INTO `jiecheng_area` VALUES ('1615', '370400', '370403', '薛城区');
INSERT INTO `jiecheng_area` VALUES ('1616', '370400', '370404', '峄城区');
INSERT INTO `jiecheng_area` VALUES ('1617', '370400', '370405', '台儿庄区');
INSERT INTO `jiecheng_area` VALUES ('1618', '370400', '370406', '山亭区');
INSERT INTO `jiecheng_area` VALUES ('1619', '370400', '370481', '滕州市');
INSERT INTO `jiecheng_area` VALUES ('1620', '370500', '370501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1621', '370500', '370502', '东营区');
INSERT INTO `jiecheng_area` VALUES ('1622', '370500', '370503', '河口区');
INSERT INTO `jiecheng_area` VALUES ('1623', '370500', '370505', '垦利区');
INSERT INTO `jiecheng_area` VALUES ('1624', '370500', '370522', '利津县');
INSERT INTO `jiecheng_area` VALUES ('1625', '370500', '370523', '广饶县');
INSERT INTO `jiecheng_area` VALUES ('1626', '370500', '370571', '东营经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1627', '370500', '370572', '东营港经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1628', '370600', '370601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1629', '370600', '370602', '芝罘区');
INSERT INTO `jiecheng_area` VALUES ('1630', '370600', '370611', '福山区');
INSERT INTO `jiecheng_area` VALUES ('1631', '370600', '370612', '牟平区');
INSERT INTO `jiecheng_area` VALUES ('1632', '370600', '370613', '莱山区');
INSERT INTO `jiecheng_area` VALUES ('1633', '370600', '370634', '长岛县');
INSERT INTO `jiecheng_area` VALUES ('1634', '370600', '370671', '烟台高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1635', '370600', '370672', '烟台经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1636', '370600', '370681', '龙口市');
INSERT INTO `jiecheng_area` VALUES ('1637', '370600', '370682', '莱阳市');
INSERT INTO `jiecheng_area` VALUES ('1638', '370600', '370683', '莱州市');
INSERT INTO `jiecheng_area` VALUES ('1639', '370600', '370684', '蓬莱市');
INSERT INTO `jiecheng_area` VALUES ('1640', '370600', '370685', '招远市');
INSERT INTO `jiecheng_area` VALUES ('1641', '370600', '370686', '栖霞市');
INSERT INTO `jiecheng_area` VALUES ('1642', '370600', '370687', '海阳市');
INSERT INTO `jiecheng_area` VALUES ('1643', '370700', '370701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1644', '370700', '370702', '潍城区');
INSERT INTO `jiecheng_area` VALUES ('1645', '370700', '370703', '寒亭区');
INSERT INTO `jiecheng_area` VALUES ('1646', '370700', '370704', '坊子区');
INSERT INTO `jiecheng_area` VALUES ('1647', '370700', '370705', '奎文区');
INSERT INTO `jiecheng_area` VALUES ('1648', '370700', '370724', '临朐县');
INSERT INTO `jiecheng_area` VALUES ('1649', '370700', '370725', '昌乐县');
INSERT INTO `jiecheng_area` VALUES ('1650', '370700', '370772', '潍坊滨海经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1651', '370700', '370781', '青州市');
INSERT INTO `jiecheng_area` VALUES ('1652', '370700', '370782', '诸城市');
INSERT INTO `jiecheng_area` VALUES ('1653', '370700', '370783', '寿光市');
INSERT INTO `jiecheng_area` VALUES ('1654', '370700', '370784', '安丘市');
INSERT INTO `jiecheng_area` VALUES ('1655', '370700', '370785', '高密市');
INSERT INTO `jiecheng_area` VALUES ('1656', '370700', '370786', '昌邑市');
INSERT INTO `jiecheng_area` VALUES ('1657', '370800', '370801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1658', '370800', '370811', '任城区');
INSERT INTO `jiecheng_area` VALUES ('1659', '370800', '370812', '兖州区');
INSERT INTO `jiecheng_area` VALUES ('1660', '370800', '370826', '微山县');
INSERT INTO `jiecheng_area` VALUES ('1661', '370800', '370827', '鱼台县');
INSERT INTO `jiecheng_area` VALUES ('1662', '370800', '370828', '金乡县');
INSERT INTO `jiecheng_area` VALUES ('1663', '370800', '370829', '嘉祥县');
INSERT INTO `jiecheng_area` VALUES ('1664', '370800', '370830', '汶上县');
INSERT INTO `jiecheng_area` VALUES ('1665', '370800', '370831', '泗水县');
INSERT INTO `jiecheng_area` VALUES ('1666', '370800', '370832', '梁山县');
INSERT INTO `jiecheng_area` VALUES ('1667', '370800', '370871', '济宁高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1668', '370800', '370881', '曲阜市');
INSERT INTO `jiecheng_area` VALUES ('1669', '370800', '370883', '邹城市');
INSERT INTO `jiecheng_area` VALUES ('1670', '370900', '370901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1671', '370900', '370902', '泰山区');
INSERT INTO `jiecheng_area` VALUES ('1672', '370900', '370911', '岱岳区');
INSERT INTO `jiecheng_area` VALUES ('1673', '370900', '370921', '宁阳县');
INSERT INTO `jiecheng_area` VALUES ('1674', '370900', '370923', '东平县');
INSERT INTO `jiecheng_area` VALUES ('1675', '370900', '370982', '新泰市');
INSERT INTO `jiecheng_area` VALUES ('1676', '370900', '370983', '肥城市');
INSERT INTO `jiecheng_area` VALUES ('1677', '371000', '371001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1678', '371000', '371002', '环翠区');
INSERT INTO `jiecheng_area` VALUES ('1679', '371000', '371003', '文登区');
INSERT INTO `jiecheng_area` VALUES ('1680', '371000', '371071', '威海火炬高技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1681', '371000', '371072', '威海经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1682', '371000', '371073', '威海临港经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1683', '371000', '371082', '荣成市');
INSERT INTO `jiecheng_area` VALUES ('1684', '371000', '371083', '乳山市');
INSERT INTO `jiecheng_area` VALUES ('1685', '371100', '371101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1686', '371100', '371102', '东港区');
INSERT INTO `jiecheng_area` VALUES ('1687', '371100', '371103', '岚山区');
INSERT INTO `jiecheng_area` VALUES ('1688', '371100', '371121', '五莲县');
INSERT INTO `jiecheng_area` VALUES ('1689', '371100', '371122', '莒县');
INSERT INTO `jiecheng_area` VALUES ('1690', '371100', '371171', '日照经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1691', '371300', '371301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1692', '371300', '371302', '兰山区');
INSERT INTO `jiecheng_area` VALUES ('1693', '371300', '371311', '罗庄区');
INSERT INTO `jiecheng_area` VALUES ('1694', '371300', '371312', '河东区');
INSERT INTO `jiecheng_area` VALUES ('1695', '371300', '371321', '沂南县');
INSERT INTO `jiecheng_area` VALUES ('1696', '371300', '371322', '郯城县');
INSERT INTO `jiecheng_area` VALUES ('1697', '371300', '371323', '沂水县');
INSERT INTO `jiecheng_area` VALUES ('1698', '371300', '371324', '兰陵县');
INSERT INTO `jiecheng_area` VALUES ('1699', '371300', '371325', '费县');
INSERT INTO `jiecheng_area` VALUES ('1700', '371300', '371326', '平邑县');
INSERT INTO `jiecheng_area` VALUES ('1701', '371300', '371327', '莒南县');
INSERT INTO `jiecheng_area` VALUES ('1702', '371300', '371328', '蒙阴县');
INSERT INTO `jiecheng_area` VALUES ('1703', '371300', '371329', '临沭县');
INSERT INTO `jiecheng_area` VALUES ('1704', '371300', '371371', '临沂高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1705', '371300', '371372', '临沂经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1706', '371300', '371373', '临沂临港经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1707', '371400', '371401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1708', '371400', '371402', '德城区');
INSERT INTO `jiecheng_area` VALUES ('1709', '371400', '371403', '陵城区');
INSERT INTO `jiecheng_area` VALUES ('1710', '371400', '371422', '宁津县');
INSERT INTO `jiecheng_area` VALUES ('1711', '371400', '371423', '庆云县');
INSERT INTO `jiecheng_area` VALUES ('1712', '371400', '371424', '临邑县');
INSERT INTO `jiecheng_area` VALUES ('1713', '371400', '371425', '齐河县');
INSERT INTO `jiecheng_area` VALUES ('1714', '371400', '371426', '平原县');
INSERT INTO `jiecheng_area` VALUES ('1715', '371400', '371427', '夏津县');
INSERT INTO `jiecheng_area` VALUES ('1716', '371400', '371428', '武城县');
INSERT INTO `jiecheng_area` VALUES ('1717', '371400', '371471', '德州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1718', '371400', '371472', '德州运河经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1719', '371400', '371481', '乐陵市');
INSERT INTO `jiecheng_area` VALUES ('1720', '371400', '371482', '禹城市');
INSERT INTO `jiecheng_area` VALUES ('1721', '371500', '371501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1722', '371500', '371502', '东昌府区');
INSERT INTO `jiecheng_area` VALUES ('1723', '371500', '371503', '茌平区');
INSERT INTO `jiecheng_area` VALUES ('1724', '371500', '371521', '阳谷县');
INSERT INTO `jiecheng_area` VALUES ('1725', '371500', '371522', '莘县');
INSERT INTO `jiecheng_area` VALUES ('1726', '371500', '371524', '东阿县');
INSERT INTO `jiecheng_area` VALUES ('1727', '371500', '371525', '冠县');
INSERT INTO `jiecheng_area` VALUES ('1728', '371500', '371526', '高唐县');
INSERT INTO `jiecheng_area` VALUES ('1729', '371500', '371581', '临清市');
INSERT INTO `jiecheng_area` VALUES ('1730', '371600', '371601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1731', '371600', '371602', '滨城区');
INSERT INTO `jiecheng_area` VALUES ('1732', '371600', '371603', '沾化区');
INSERT INTO `jiecheng_area` VALUES ('1733', '371600', '371621', '惠民县');
INSERT INTO `jiecheng_area` VALUES ('1734', '371600', '371622', '阳信县');
INSERT INTO `jiecheng_area` VALUES ('1735', '371600', '371623', '无棣县');
INSERT INTO `jiecheng_area` VALUES ('1736', '371600', '371625', '博兴县');
INSERT INTO `jiecheng_area` VALUES ('1737', '371600', '371681', '邹平市');
INSERT INTO `jiecheng_area` VALUES ('1738', '371700', '371701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1739', '371700', '371702', '牡丹区');
INSERT INTO `jiecheng_area` VALUES ('1740', '371700', '371703', '定陶区');
INSERT INTO `jiecheng_area` VALUES ('1741', '371700', '371721', '曹县');
INSERT INTO `jiecheng_area` VALUES ('1742', '371700', '371722', '单县');
INSERT INTO `jiecheng_area` VALUES ('1743', '371700', '371723', '成武县');
INSERT INTO `jiecheng_area` VALUES ('1744', '371700', '371724', '巨野县');
INSERT INTO `jiecheng_area` VALUES ('1745', '371700', '371725', '郓城县');
INSERT INTO `jiecheng_area` VALUES ('1746', '371700', '371726', '鄄城县');
INSERT INTO `jiecheng_area` VALUES ('1747', '371700', '371728', '东明县');
INSERT INTO `jiecheng_area` VALUES ('1748', '371700', '371771', '菏泽经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1749', '371700', '371772', '菏泽高新技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1750', '410000', '410100', '郑州市');
INSERT INTO `jiecheng_area` VALUES ('1751', '410000', '410200', '开封市');
INSERT INTO `jiecheng_area` VALUES ('1752', '410000', '410300', '洛阳市');
INSERT INTO `jiecheng_area` VALUES ('1753', '410000', '410400', '平顶山市');
INSERT INTO `jiecheng_area` VALUES ('1754', '410000', '410500', '安阳市');
INSERT INTO `jiecheng_area` VALUES ('1755', '410000', '410600', '鹤壁市');
INSERT INTO `jiecheng_area` VALUES ('1756', '410000', '410700', '新乡市');
INSERT INTO `jiecheng_area` VALUES ('1757', '410000', '410800', '焦作市');
INSERT INTO `jiecheng_area` VALUES ('1758', '410000', '410900', '濮阳市');
INSERT INTO `jiecheng_area` VALUES ('1759', '410000', '411000', '许昌市');
INSERT INTO `jiecheng_area` VALUES ('1760', '410000', '411100', '漯河市');
INSERT INTO `jiecheng_area` VALUES ('1761', '410000', '411200', '三门峡市');
INSERT INTO `jiecheng_area` VALUES ('1762', '410000', '411300', '南阳市');
INSERT INTO `jiecheng_area` VALUES ('1763', '410000', '411400', '商丘市');
INSERT INTO `jiecheng_area` VALUES ('1764', '410000', '411500', '信阳市');
INSERT INTO `jiecheng_area` VALUES ('1765', '410000', '411600', '周口市');
INSERT INTO `jiecheng_area` VALUES ('1766', '410000', '411700', '驻马店市');
INSERT INTO `jiecheng_area` VALUES ('1767', '410000', '419000', '省直辖县级行政区划');
INSERT INTO `jiecheng_area` VALUES ('1768', '410100', '410101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1769', '410100', '410102', '中原区');
INSERT INTO `jiecheng_area` VALUES ('1770', '410100', '410103', '二七区');
INSERT INTO `jiecheng_area` VALUES ('1771', '410100', '410104', '管城回族区');
INSERT INTO `jiecheng_area` VALUES ('1772', '410100', '410105', '金水区');
INSERT INTO `jiecheng_area` VALUES ('1773', '410100', '410106', '上街区');
INSERT INTO `jiecheng_area` VALUES ('1774', '410100', '410108', '惠济区');
INSERT INTO `jiecheng_area` VALUES ('1775', '410100', '410122', '中牟县');
INSERT INTO `jiecheng_area` VALUES ('1776', '410100', '410171', '郑州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1777', '410100', '410172', '郑州高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1778', '410100', '410173', '郑州航空港经济综合实验区');
INSERT INTO `jiecheng_area` VALUES ('1779', '410100', '410181', '巩义市');
INSERT INTO `jiecheng_area` VALUES ('1780', '410100', '410182', '荥阳市');
INSERT INTO `jiecheng_area` VALUES ('1781', '410100', '410183', '新密市');
INSERT INTO `jiecheng_area` VALUES ('1782', '410100', '410184', '新郑市');
INSERT INTO `jiecheng_area` VALUES ('1783', '410100', '410185', '登封市');
INSERT INTO `jiecheng_area` VALUES ('1784', '410200', '410201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1785', '410200', '410202', '龙亭区');
INSERT INTO `jiecheng_area` VALUES ('1786', '410200', '410203', '顺河回族区');
INSERT INTO `jiecheng_area` VALUES ('1787', '410200', '410204', '鼓楼区');
INSERT INTO `jiecheng_area` VALUES ('1788', '410200', '410205', '禹王台区');
INSERT INTO `jiecheng_area` VALUES ('1789', '410200', '410212', '祥符区');
INSERT INTO `jiecheng_area` VALUES ('1790', '410200', '410221', '杞县');
INSERT INTO `jiecheng_area` VALUES ('1791', '410200', '410222', '通许县');
INSERT INTO `jiecheng_area` VALUES ('1792', '410200', '410223', '尉氏县');
INSERT INTO `jiecheng_area` VALUES ('1793', '410200', '410225', '兰考县');
INSERT INTO `jiecheng_area` VALUES ('1794', '410300', '410301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1795', '410300', '410302', '老城区');
INSERT INTO `jiecheng_area` VALUES ('1796', '410300', '410303', '西工区');
INSERT INTO `jiecheng_area` VALUES ('1797', '410300', '410304', '瀍河回族区');
INSERT INTO `jiecheng_area` VALUES ('1798', '410300', '410305', '涧西区');
INSERT INTO `jiecheng_area` VALUES ('1799', '410300', '410306', '吉利区');
INSERT INTO `jiecheng_area` VALUES ('1800', '410300', '410311', '洛龙区');
INSERT INTO `jiecheng_area` VALUES ('1801', '410300', '410322', '孟津县');
INSERT INTO `jiecheng_area` VALUES ('1802', '410300', '410323', '新安县');
INSERT INTO `jiecheng_area` VALUES ('1803', '410300', '410324', '栾川县');
INSERT INTO `jiecheng_area` VALUES ('1804', '410300', '410325', '嵩县');
INSERT INTO `jiecheng_area` VALUES ('1805', '410300', '410326', '汝阳县');
INSERT INTO `jiecheng_area` VALUES ('1806', '410300', '410327', '宜阳县');
INSERT INTO `jiecheng_area` VALUES ('1807', '410300', '410328', '洛宁县');
INSERT INTO `jiecheng_area` VALUES ('1808', '410300', '410329', '伊川县');
INSERT INTO `jiecheng_area` VALUES ('1809', '410300', '410371', '洛阳高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1810', '410300', '410381', '偃师市');
INSERT INTO `jiecheng_area` VALUES ('1811', '410400', '410401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1812', '410400', '410402', '新华区');
INSERT INTO `jiecheng_area` VALUES ('1813', '410400', '410403', '卫东区');
INSERT INTO `jiecheng_area` VALUES ('1814', '410400', '410404', '石龙区');
INSERT INTO `jiecheng_area` VALUES ('1815', '410400', '410411', '湛河区');
INSERT INTO `jiecheng_area` VALUES ('1816', '410400', '410421', '宝丰县');
INSERT INTO `jiecheng_area` VALUES ('1817', '410400', '410422', '叶县');
INSERT INTO `jiecheng_area` VALUES ('1818', '410400', '410423', '鲁山县');
INSERT INTO `jiecheng_area` VALUES ('1819', '410400', '410425', '郏县');
INSERT INTO `jiecheng_area` VALUES ('1820', '410400', '410471', '平顶山高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1821', '410400', '410472', '平顶山市城乡一体化示范区');
INSERT INTO `jiecheng_area` VALUES ('1822', '410400', '410481', '舞钢市');
INSERT INTO `jiecheng_area` VALUES ('1823', '410400', '410482', '汝州市');
INSERT INTO `jiecheng_area` VALUES ('1824', '410500', '410501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1825', '410500', '410502', '文峰区');
INSERT INTO `jiecheng_area` VALUES ('1826', '410500', '410503', '北关区');
INSERT INTO `jiecheng_area` VALUES ('1827', '410500', '410505', '殷都区');
INSERT INTO `jiecheng_area` VALUES ('1828', '410500', '410506', '龙安区');
INSERT INTO `jiecheng_area` VALUES ('1829', '410500', '410522', '安阳县');
INSERT INTO `jiecheng_area` VALUES ('1830', '410500', '410523', '汤阴县');
INSERT INTO `jiecheng_area` VALUES ('1831', '410500', '410526', '滑县');
INSERT INTO `jiecheng_area` VALUES ('1832', '410500', '410527', '内黄县');
INSERT INTO `jiecheng_area` VALUES ('1833', '410500', '410571', '安阳高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1834', '410500', '410581', '林州市');
INSERT INTO `jiecheng_area` VALUES ('1835', '410600', '410601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1836', '410600', '410602', '鹤山区');
INSERT INTO `jiecheng_area` VALUES ('1837', '410600', '410603', '山城区');
INSERT INTO `jiecheng_area` VALUES ('1838', '410600', '410611', '淇滨区');
INSERT INTO `jiecheng_area` VALUES ('1839', '410600', '410621', '浚县');
INSERT INTO `jiecheng_area` VALUES ('1840', '410600', '410622', '淇县');
INSERT INTO `jiecheng_area` VALUES ('1841', '410600', '410671', '鹤壁经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1842', '410700', '410701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1843', '410700', '410702', '红旗区');
INSERT INTO `jiecheng_area` VALUES ('1844', '410700', '410703', '卫滨区');
INSERT INTO `jiecheng_area` VALUES ('1845', '410700', '410704', '凤泉区');
INSERT INTO `jiecheng_area` VALUES ('1846', '410700', '410711', '牧野区');
INSERT INTO `jiecheng_area` VALUES ('1847', '410700', '410721', '新乡县');
INSERT INTO `jiecheng_area` VALUES ('1848', '410700', '410724', '获嘉县');
INSERT INTO `jiecheng_area` VALUES ('1849', '410700', '410725', '原阳县');
INSERT INTO `jiecheng_area` VALUES ('1850', '410700', '410726', '延津县');
INSERT INTO `jiecheng_area` VALUES ('1851', '410700', '410727', '封丘县');
INSERT INTO `jiecheng_area` VALUES ('1852', '410700', '410771', '新乡高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1853', '410700', '410772', '新乡经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1854', '410700', '410773', '新乡市平原城乡一体化示范区');
INSERT INTO `jiecheng_area` VALUES ('1855', '410700', '410781', '卫辉市');
INSERT INTO `jiecheng_area` VALUES ('1856', '410700', '410782', '辉县市');
INSERT INTO `jiecheng_area` VALUES ('1857', '410700', '410783', '长垣市');
INSERT INTO `jiecheng_area` VALUES ('1858', '410800', '410801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1859', '410800', '410802', '解放区');
INSERT INTO `jiecheng_area` VALUES ('1860', '410800', '410803', '中站区');
INSERT INTO `jiecheng_area` VALUES ('1861', '410800', '410804', '马村区');
INSERT INTO `jiecheng_area` VALUES ('1862', '410800', '410811', '山阳区');
INSERT INTO `jiecheng_area` VALUES ('1863', '410800', '410821', '修武县');
INSERT INTO `jiecheng_area` VALUES ('1864', '410800', '410822', '博爱县');
INSERT INTO `jiecheng_area` VALUES ('1865', '410800', '410823', '武陟县');
INSERT INTO `jiecheng_area` VALUES ('1866', '410800', '410825', '温县');
INSERT INTO `jiecheng_area` VALUES ('1867', '410800', '410871', '焦作城乡一体化示范区');
INSERT INTO `jiecheng_area` VALUES ('1868', '410800', '410882', '沁阳市');
INSERT INTO `jiecheng_area` VALUES ('1869', '410800', '410883', '孟州市');
INSERT INTO `jiecheng_area` VALUES ('1870', '410900', '410901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1871', '410900', '410902', '华龙区');
INSERT INTO `jiecheng_area` VALUES ('1872', '410900', '410922', '清丰县');
INSERT INTO `jiecheng_area` VALUES ('1873', '410900', '410923', '南乐县');
INSERT INTO `jiecheng_area` VALUES ('1874', '410900', '410926', '范县');
INSERT INTO `jiecheng_area` VALUES ('1875', '410900', '410927', '台前县');
INSERT INTO `jiecheng_area` VALUES ('1876', '410900', '410928', '濮阳县');
INSERT INTO `jiecheng_area` VALUES ('1877', '410900', '410971', '河南濮阳工业园区');
INSERT INTO `jiecheng_area` VALUES ('1878', '410900', '410972', '濮阳经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1879', '411000', '411001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1880', '411000', '411002', '魏都区');
INSERT INTO `jiecheng_area` VALUES ('1881', '411000', '411003', '建安区');
INSERT INTO `jiecheng_area` VALUES ('1882', '411000', '411024', '鄢陵县');
INSERT INTO `jiecheng_area` VALUES ('1883', '411000', '411025', '襄城县');
INSERT INTO `jiecheng_area` VALUES ('1884', '411000', '411071', '许昌经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1885', '411000', '411081', '禹州市');
INSERT INTO `jiecheng_area` VALUES ('1886', '411000', '411082', '长葛市');
INSERT INTO `jiecheng_area` VALUES ('1887', '411100', '411101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1888', '411100', '411102', '源汇区');
INSERT INTO `jiecheng_area` VALUES ('1889', '411100', '411103', '郾城区');
INSERT INTO `jiecheng_area` VALUES ('1890', '411100', '411104', '召陵区');
INSERT INTO `jiecheng_area` VALUES ('1891', '411100', '411121', '舞阳县');
INSERT INTO `jiecheng_area` VALUES ('1892', '411100', '411122', '临颍县');
INSERT INTO `jiecheng_area` VALUES ('1893', '411100', '411171', '漯河经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('1894', '411200', '411201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1895', '411200', '411202', '湖滨区');
INSERT INTO `jiecheng_area` VALUES ('1896', '411200', '411203', '陕州区');
INSERT INTO `jiecheng_area` VALUES ('1897', '411200', '411221', '渑池县');
INSERT INTO `jiecheng_area` VALUES ('1898', '411200', '411224', '卢氏县');
INSERT INTO `jiecheng_area` VALUES ('1899', '411200', '411271', '河南三门峡经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1900', '411200', '411281', '义马市');
INSERT INTO `jiecheng_area` VALUES ('1901', '411200', '411282', '灵宝市');
INSERT INTO `jiecheng_area` VALUES ('1902', '411300', '411301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1903', '411300', '411302', '宛城区');
INSERT INTO `jiecheng_area` VALUES ('1904', '411300', '411303', '卧龙区');
INSERT INTO `jiecheng_area` VALUES ('1905', '411300', '411321', '南召县');
INSERT INTO `jiecheng_area` VALUES ('1906', '411300', '411322', '方城县');
INSERT INTO `jiecheng_area` VALUES ('1907', '411300', '411323', '西峡县');
INSERT INTO `jiecheng_area` VALUES ('1908', '411300', '411324', '镇平县');
INSERT INTO `jiecheng_area` VALUES ('1909', '411300', '411325', '内乡县');
INSERT INTO `jiecheng_area` VALUES ('1910', '411300', '411326', '淅川县');
INSERT INTO `jiecheng_area` VALUES ('1911', '411300', '411327', '社旗县');
INSERT INTO `jiecheng_area` VALUES ('1912', '411300', '411328', '唐河县');
INSERT INTO `jiecheng_area` VALUES ('1913', '411300', '411329', '新野县');
INSERT INTO `jiecheng_area` VALUES ('1914', '411300', '411330', '桐柏县');
INSERT INTO `jiecheng_area` VALUES ('1915', '411300', '411371', '南阳高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1916', '411300', '411372', '南阳市城乡一体化示范区');
INSERT INTO `jiecheng_area` VALUES ('1917', '411300', '411381', '邓州市');
INSERT INTO `jiecheng_area` VALUES ('1918', '411400', '411401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1919', '411400', '411402', '梁园区');
INSERT INTO `jiecheng_area` VALUES ('1920', '411400', '411403', '睢阳区');
INSERT INTO `jiecheng_area` VALUES ('1921', '411400', '411421', '民权县');
INSERT INTO `jiecheng_area` VALUES ('1922', '411400', '411422', '睢县');
INSERT INTO `jiecheng_area` VALUES ('1923', '411400', '411423', '宁陵县');
INSERT INTO `jiecheng_area` VALUES ('1924', '411400', '411424', '柘城县');
INSERT INTO `jiecheng_area` VALUES ('1925', '411400', '411425', '虞城县');
INSERT INTO `jiecheng_area` VALUES ('1926', '411400', '411426', '夏邑县');
INSERT INTO `jiecheng_area` VALUES ('1927', '411400', '411471', '豫东综合物流产业聚集区');
INSERT INTO `jiecheng_area` VALUES ('1928', '411400', '411472', '河南商丘经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1929', '411400', '411481', '永城市');
INSERT INTO `jiecheng_area` VALUES ('1930', '411500', '411501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1931', '411500', '411502', '浉河区');
INSERT INTO `jiecheng_area` VALUES ('1932', '411500', '411503', '平桥区');
INSERT INTO `jiecheng_area` VALUES ('1933', '411500', '411521', '罗山县');
INSERT INTO `jiecheng_area` VALUES ('1934', '411500', '411522', '光山县');
INSERT INTO `jiecheng_area` VALUES ('1935', '411500', '411523', '新县');
INSERT INTO `jiecheng_area` VALUES ('1936', '411500', '411524', '商城县');
INSERT INTO `jiecheng_area` VALUES ('1937', '411500', '411525', '固始县');
INSERT INTO `jiecheng_area` VALUES ('1938', '411500', '411526', '潢川县');
INSERT INTO `jiecheng_area` VALUES ('1939', '411500', '411527', '淮滨县');
INSERT INTO `jiecheng_area` VALUES ('1940', '411500', '411528', '息县');
INSERT INTO `jiecheng_area` VALUES ('1941', '411500', '411571', '信阳高新技术产业开发区');
INSERT INTO `jiecheng_area` VALUES ('1942', '411600', '411601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1943', '411600', '411602', '川汇区');
INSERT INTO `jiecheng_area` VALUES ('1944', '411600', '411603', '淮阳区');
INSERT INTO `jiecheng_area` VALUES ('1945', '411600', '411621', '扶沟县');
INSERT INTO `jiecheng_area` VALUES ('1946', '411600', '411622', '西华县');
INSERT INTO `jiecheng_area` VALUES ('1947', '411600', '411623', '商水县');
INSERT INTO `jiecheng_area` VALUES ('1948', '411600', '411624', '沈丘县');
INSERT INTO `jiecheng_area` VALUES ('1949', '411600', '411625', '郸城县');
INSERT INTO `jiecheng_area` VALUES ('1950', '411600', '411627', '太康县');
INSERT INTO `jiecheng_area` VALUES ('1951', '411600', '411628', '鹿邑县');
INSERT INTO `jiecheng_area` VALUES ('1952', '411600', '411671', '河南周口经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1953', '411600', '411681', '项城市');
INSERT INTO `jiecheng_area` VALUES ('1954', '411700', '411701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1955', '411700', '411702', '驿城区');
INSERT INTO `jiecheng_area` VALUES ('1956', '411700', '411721', '西平县');
INSERT INTO `jiecheng_area` VALUES ('1957', '411700', '411722', '上蔡县');
INSERT INTO `jiecheng_area` VALUES ('1958', '411700', '411723', '平舆县');
INSERT INTO `jiecheng_area` VALUES ('1959', '411700', '411724', '正阳县');
INSERT INTO `jiecheng_area` VALUES ('1960', '411700', '411725', '确山县');
INSERT INTO `jiecheng_area` VALUES ('1961', '411700', '411726', '泌阳县');
INSERT INTO `jiecheng_area` VALUES ('1962', '411700', '411727', '汝南县');
INSERT INTO `jiecheng_area` VALUES ('1963', '411700', '411728', '遂平县');
INSERT INTO `jiecheng_area` VALUES ('1964', '411700', '411729', '新蔡县');
INSERT INTO `jiecheng_area` VALUES ('1965', '411700', '411771', '河南驻马店经济开发区');
INSERT INTO `jiecheng_area` VALUES ('1966', '419000', '419001', '济源市');
INSERT INTO `jiecheng_area` VALUES ('1967', '420000', '420100', '武汉市');
INSERT INTO `jiecheng_area` VALUES ('1968', '420000', '420200', '黄石市');
INSERT INTO `jiecheng_area` VALUES ('1969', '420000', '420300', '十堰市');
INSERT INTO `jiecheng_area` VALUES ('1970', '420000', '420500', '宜昌市');
INSERT INTO `jiecheng_area` VALUES ('1971', '420000', '420600', '襄阳市');
INSERT INTO `jiecheng_area` VALUES ('1972', '420000', '420700', '鄂州市');
INSERT INTO `jiecheng_area` VALUES ('1973', '420000', '420800', '荆门市');
INSERT INTO `jiecheng_area` VALUES ('1974', '420000', '420900', '孝感市');
INSERT INTO `jiecheng_area` VALUES ('1975', '420000', '421000', '荆州市');
INSERT INTO `jiecheng_area` VALUES ('1976', '420000', '421100', '黄冈市');
INSERT INTO `jiecheng_area` VALUES ('1977', '420000', '421200', '咸宁市');
INSERT INTO `jiecheng_area` VALUES ('1978', '420000', '421300', '随州市');
INSERT INTO `jiecheng_area` VALUES ('1979', '420000', '422800', '恩施土家族苗族自治州');
INSERT INTO `jiecheng_area` VALUES ('1980', '420000', '429000', '省直辖县级行政区划');
INSERT INTO `jiecheng_area` VALUES ('1981', '420100', '420101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1982', '420100', '420102', '江岸区');
INSERT INTO `jiecheng_area` VALUES ('1983', '420100', '420103', '江汉区');
INSERT INTO `jiecheng_area` VALUES ('1984', '420100', '420104', '硚口区');
INSERT INTO `jiecheng_area` VALUES ('1985', '420100', '420105', '汉阳区');
INSERT INTO `jiecheng_area` VALUES ('1986', '420100', '420106', '武昌区');
INSERT INTO `jiecheng_area` VALUES ('1987', '420100', '420107', '青山区');
INSERT INTO `jiecheng_area` VALUES ('1988', '420100', '420111', '洪山区');
INSERT INTO `jiecheng_area` VALUES ('1989', '420100', '420112', '东西湖区');
INSERT INTO `jiecheng_area` VALUES ('1990', '420100', '420113', '汉南区');
INSERT INTO `jiecheng_area` VALUES ('1991', '420100', '420114', '蔡甸区');
INSERT INTO `jiecheng_area` VALUES ('1992', '420100', '420115', '江夏区');
INSERT INTO `jiecheng_area` VALUES ('1993', '420100', '420116', '黄陂区');
INSERT INTO `jiecheng_area` VALUES ('1994', '420100', '420117', '新洲区');
INSERT INTO `jiecheng_area` VALUES ('1995', '420200', '420201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('1996', '420200', '420202', '黄石港区');
INSERT INTO `jiecheng_area` VALUES ('1997', '420200', '420203', '西塞山区');
INSERT INTO `jiecheng_area` VALUES ('1998', '420200', '420204', '下陆区');
INSERT INTO `jiecheng_area` VALUES ('1999', '420200', '420205', '铁山区');
INSERT INTO `jiecheng_area` VALUES ('2000', '420200', '420222', '阳新县');
INSERT INTO `jiecheng_area` VALUES ('2001', '420200', '420281', '大冶市');
INSERT INTO `jiecheng_area` VALUES ('2002', '420300', '420301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2003', '420300', '420302', '茅箭区');
INSERT INTO `jiecheng_area` VALUES ('2004', '420300', '420303', '张湾区');
INSERT INTO `jiecheng_area` VALUES ('2005', '420300', '420304', '郧阳区');
INSERT INTO `jiecheng_area` VALUES ('2006', '420300', '420322', '郧西县');
INSERT INTO `jiecheng_area` VALUES ('2007', '420300', '420323', '竹山县');
INSERT INTO `jiecheng_area` VALUES ('2008', '420300', '420324', '竹溪县');
INSERT INTO `jiecheng_area` VALUES ('2009', '420300', '420325', '房县');
INSERT INTO `jiecheng_area` VALUES ('2010', '420300', '420381', '丹江口市');
INSERT INTO `jiecheng_area` VALUES ('2011', '420500', '420501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2012', '420500', '420502', '西陵区');
INSERT INTO `jiecheng_area` VALUES ('2013', '420500', '420503', '伍家岗区');
INSERT INTO `jiecheng_area` VALUES ('2014', '420500', '420504', '点军区');
INSERT INTO `jiecheng_area` VALUES ('2015', '420500', '420505', '猇亭区');
INSERT INTO `jiecheng_area` VALUES ('2016', '420500', '420506', '夷陵区');
INSERT INTO `jiecheng_area` VALUES ('2017', '420500', '420525', '远安县');
INSERT INTO `jiecheng_area` VALUES ('2018', '420500', '420526', '兴山县');
INSERT INTO `jiecheng_area` VALUES ('2019', '420500', '420527', '秭归县');
INSERT INTO `jiecheng_area` VALUES ('2020', '420500', '420528', '长阳土家族自治县');
INSERT INTO `jiecheng_area` VALUES ('2021', '420500', '420529', '五峰土家族自治县');
INSERT INTO `jiecheng_area` VALUES ('2022', '420500', '420581', '宜都市');
INSERT INTO `jiecheng_area` VALUES ('2023', '420500', '420582', '当阳市');
INSERT INTO `jiecheng_area` VALUES ('2024', '420500', '420583', '枝江市');
INSERT INTO `jiecheng_area` VALUES ('2025', '420600', '420601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2026', '420600', '420602', '襄城区');
INSERT INTO `jiecheng_area` VALUES ('2027', '420600', '420606', '樊城区');
INSERT INTO `jiecheng_area` VALUES ('2028', '420600', '420607', '襄州区');
INSERT INTO `jiecheng_area` VALUES ('2029', '420600', '420624', '南漳县');
INSERT INTO `jiecheng_area` VALUES ('2030', '420600', '420625', '谷城县');
INSERT INTO `jiecheng_area` VALUES ('2031', '420600', '420626', '保康县');
INSERT INTO `jiecheng_area` VALUES ('2032', '420600', '420682', '老河口市');
INSERT INTO `jiecheng_area` VALUES ('2033', '420600', '420683', '枣阳市');
INSERT INTO `jiecheng_area` VALUES ('2034', '420600', '420684', '宜城市');
INSERT INTO `jiecheng_area` VALUES ('2035', '420700', '420701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2036', '420700', '420702', '梁子湖区');
INSERT INTO `jiecheng_area` VALUES ('2037', '420700', '420703', '华容区');
INSERT INTO `jiecheng_area` VALUES ('2038', '420700', '420704', '鄂城区');
INSERT INTO `jiecheng_area` VALUES ('2039', '420800', '420801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2040', '420800', '420802', '东宝区');
INSERT INTO `jiecheng_area` VALUES ('2041', '420800', '420804', '掇刀区');
INSERT INTO `jiecheng_area` VALUES ('2042', '420800', '420822', '沙洋县');
INSERT INTO `jiecheng_area` VALUES ('2043', '420800', '420881', '钟祥市');
INSERT INTO `jiecheng_area` VALUES ('2044', '420800', '420882', '京山市');
INSERT INTO `jiecheng_area` VALUES ('2045', '420900', '420901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2046', '420900', '420902', '孝南区');
INSERT INTO `jiecheng_area` VALUES ('2047', '420900', '420921', '孝昌县');
INSERT INTO `jiecheng_area` VALUES ('2048', '420900', '420922', '大悟县');
INSERT INTO `jiecheng_area` VALUES ('2049', '420900', '420923', '云梦县');
INSERT INTO `jiecheng_area` VALUES ('2050', '420900', '420981', '应城市');
INSERT INTO `jiecheng_area` VALUES ('2051', '420900', '420982', '安陆市');
INSERT INTO `jiecheng_area` VALUES ('2052', '420900', '420984', '汉川市');
INSERT INTO `jiecheng_area` VALUES ('2053', '421000', '421001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2054', '421000', '421002', '沙市区');
INSERT INTO `jiecheng_area` VALUES ('2055', '421000', '421003', '荆州区');
INSERT INTO `jiecheng_area` VALUES ('2056', '421000', '421022', '公安县');
INSERT INTO `jiecheng_area` VALUES ('2057', '421000', '421023', '监利县');
INSERT INTO `jiecheng_area` VALUES ('2058', '421000', '421024', '江陵县');
INSERT INTO `jiecheng_area` VALUES ('2059', '421000', '421071', '荆州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('2060', '421000', '421081', '石首市');
INSERT INTO `jiecheng_area` VALUES ('2061', '421000', '421083', '洪湖市');
INSERT INTO `jiecheng_area` VALUES ('2062', '421000', '421087', '松滋市');
INSERT INTO `jiecheng_area` VALUES ('2063', '421100', '421101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2064', '421100', '421102', '黄州区');
INSERT INTO `jiecheng_area` VALUES ('2065', '421100', '421121', '团风县');
INSERT INTO `jiecheng_area` VALUES ('2066', '421100', '421122', '红安县');
INSERT INTO `jiecheng_area` VALUES ('2067', '421100', '421123', '罗田县');
INSERT INTO `jiecheng_area` VALUES ('2068', '421100', '421124', '英山县');
INSERT INTO `jiecheng_area` VALUES ('2069', '421100', '421125', '浠水县');
INSERT INTO `jiecheng_area` VALUES ('2070', '421100', '421126', '蕲春县');
INSERT INTO `jiecheng_area` VALUES ('2071', '421100', '421127', '黄梅县');
INSERT INTO `jiecheng_area` VALUES ('2072', '421100', '421171', '龙感湖管理区');
INSERT INTO `jiecheng_area` VALUES ('2073', '421100', '421181', '麻城市');
INSERT INTO `jiecheng_area` VALUES ('2074', '421100', '421182', '武穴市');
INSERT INTO `jiecheng_area` VALUES ('2075', '421200', '421201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2076', '421200', '421202', '咸安区');
INSERT INTO `jiecheng_area` VALUES ('2077', '421200', '421221', '嘉鱼县');
INSERT INTO `jiecheng_area` VALUES ('2078', '421200', '421222', '通城县');
INSERT INTO `jiecheng_area` VALUES ('2079', '421200', '421223', '崇阳县');
INSERT INTO `jiecheng_area` VALUES ('2080', '421200', '421224', '通山县');
INSERT INTO `jiecheng_area` VALUES ('2081', '421200', '421281', '赤壁市');
INSERT INTO `jiecheng_area` VALUES ('2082', '421300', '421301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2083', '421300', '421303', '曾都区');
INSERT INTO `jiecheng_area` VALUES ('2084', '421300', '421321', '随县');
INSERT INTO `jiecheng_area` VALUES ('2085', '421300', '421381', '广水市');
INSERT INTO `jiecheng_area` VALUES ('2086', '422800', '422801', '恩施市');
INSERT INTO `jiecheng_area` VALUES ('2087', '422800', '422802', '利川市');
INSERT INTO `jiecheng_area` VALUES ('2088', '422800', '422822', '建始县');
INSERT INTO `jiecheng_area` VALUES ('2089', '422800', '422823', '巴东县');
INSERT INTO `jiecheng_area` VALUES ('2090', '422800', '422825', '宣恩县');
INSERT INTO `jiecheng_area` VALUES ('2091', '422800', '422826', '咸丰县');
INSERT INTO `jiecheng_area` VALUES ('2092', '422800', '422827', '来凤县');
INSERT INTO `jiecheng_area` VALUES ('2093', '422800', '422828', '鹤峰县');
INSERT INTO `jiecheng_area` VALUES ('2094', '429000', '429004', '仙桃市');
INSERT INTO `jiecheng_area` VALUES ('2095', '429000', '429005', '潜江市');
INSERT INTO `jiecheng_area` VALUES ('2096', '429000', '429006', '天门市');
INSERT INTO `jiecheng_area` VALUES ('2097', '429000', '429021', '神农架林区');
INSERT INTO `jiecheng_area` VALUES ('2098', '430000', '430100', '长沙市');
INSERT INTO `jiecheng_area` VALUES ('2099', '430000', '430200', '株洲市');
INSERT INTO `jiecheng_area` VALUES ('2100', '430000', '430300', '湘潭市');
INSERT INTO `jiecheng_area` VALUES ('2101', '430000', '430400', '衡阳市');
INSERT INTO `jiecheng_area` VALUES ('2102', '430000', '430500', '邵阳市');
INSERT INTO `jiecheng_area` VALUES ('2103', '430000', '430600', '岳阳市');
INSERT INTO `jiecheng_area` VALUES ('2104', '430000', '430700', '常德市');
INSERT INTO `jiecheng_area` VALUES ('2105', '430000', '430800', '张家界市');
INSERT INTO `jiecheng_area` VALUES ('2106', '430000', '430900', '益阳市');
INSERT INTO `jiecheng_area` VALUES ('2107', '430000', '431000', '郴州市');
INSERT INTO `jiecheng_area` VALUES ('2108', '430000', '431100', '永州市');
INSERT INTO `jiecheng_area` VALUES ('2109', '430000', '431200', '怀化市');
INSERT INTO `jiecheng_area` VALUES ('2110', '430000', '431300', '娄底市');
INSERT INTO `jiecheng_area` VALUES ('2111', '430000', '433100', '湘西土家族苗族自治州');
INSERT INTO `jiecheng_area` VALUES ('2112', '430100', '430101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2113', '430100', '430102', '芙蓉区');
INSERT INTO `jiecheng_area` VALUES ('2114', '430100', '430103', '天心区');
INSERT INTO `jiecheng_area` VALUES ('2115', '430100', '430104', '岳麓区');
INSERT INTO `jiecheng_area` VALUES ('2116', '430100', '430105', '开福区');
INSERT INTO `jiecheng_area` VALUES ('2117', '430100', '430111', '雨花区');
INSERT INTO `jiecheng_area` VALUES ('2118', '430100', '430112', '望城区');
INSERT INTO `jiecheng_area` VALUES ('2119', '430100', '430121', '长沙县');
INSERT INTO `jiecheng_area` VALUES ('2120', '430100', '430181', '浏阳市');
INSERT INTO `jiecheng_area` VALUES ('2121', '430100', '430182', '宁乡市');
INSERT INTO `jiecheng_area` VALUES ('2122', '430200', '430201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2123', '430200', '430202', '荷塘区');
INSERT INTO `jiecheng_area` VALUES ('2124', '430200', '430203', '芦淞区');
INSERT INTO `jiecheng_area` VALUES ('2125', '430200', '430204', '石峰区');
INSERT INTO `jiecheng_area` VALUES ('2126', '430200', '430211', '天元区');
INSERT INTO `jiecheng_area` VALUES ('2127', '430200', '430212', '渌口区');
INSERT INTO `jiecheng_area` VALUES ('2128', '430200', '430223', '攸县');
INSERT INTO `jiecheng_area` VALUES ('2129', '430200', '430224', '茶陵县');
INSERT INTO `jiecheng_area` VALUES ('2130', '430200', '430225', '炎陵县');
INSERT INTO `jiecheng_area` VALUES ('2131', '430200', '430271', '云龙示范区');
INSERT INTO `jiecheng_area` VALUES ('2132', '430200', '430281', '醴陵市');
INSERT INTO `jiecheng_area` VALUES ('2133', '430300', '430301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2134', '430300', '430302', '雨湖区');
INSERT INTO `jiecheng_area` VALUES ('2135', '430300', '430304', '岳塘区');
INSERT INTO `jiecheng_area` VALUES ('2136', '430300', '430321', '湘潭县');
INSERT INTO `jiecheng_area` VALUES ('2137', '430300', '430371', '湖南湘潭高新技术产业园区');
INSERT INTO `jiecheng_area` VALUES ('2138', '430300', '430372', '湘潭昭山示范区');
INSERT INTO `jiecheng_area` VALUES ('2139', '430300', '430373', '湘潭九华示范区');
INSERT INTO `jiecheng_area` VALUES ('2140', '430300', '430381', '湘乡市');
INSERT INTO `jiecheng_area` VALUES ('2141', '430300', '430382', '韶山市');
INSERT INTO `jiecheng_area` VALUES ('2142', '430400', '430401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2143', '430400', '430405', '珠晖区');
INSERT INTO `jiecheng_area` VALUES ('2144', '430400', '430406', '雁峰区');
INSERT INTO `jiecheng_area` VALUES ('2145', '430400', '430407', '石鼓区');
INSERT INTO `jiecheng_area` VALUES ('2146', '430400', '430408', '蒸湘区');
INSERT INTO `jiecheng_area` VALUES ('2147', '430400', '430412', '南岳区');
INSERT INTO `jiecheng_area` VALUES ('2148', '430400', '430421', '衡阳县');
INSERT INTO `jiecheng_area` VALUES ('2149', '430400', '430422', '衡南县');
INSERT INTO `jiecheng_area` VALUES ('2150', '430400', '430423', '衡山县');
INSERT INTO `jiecheng_area` VALUES ('2151', '430400', '430424', '衡东县');
INSERT INTO `jiecheng_area` VALUES ('2152', '430400', '430426', '祁东县');
INSERT INTO `jiecheng_area` VALUES ('2153', '430400', '430471', '衡阳综合保税区');
INSERT INTO `jiecheng_area` VALUES ('2154', '430400', '430472', '湖南衡阳高新技术产业园区');
INSERT INTO `jiecheng_area` VALUES ('2155', '430400', '430473', '湖南衡阳松木经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2156', '430400', '430481', '耒阳市');
INSERT INTO `jiecheng_area` VALUES ('2157', '430400', '430482', '常宁市');
INSERT INTO `jiecheng_area` VALUES ('2158', '430500', '430501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2159', '430500', '430502', '双清区');
INSERT INTO `jiecheng_area` VALUES ('2160', '430500', '430503', '大祥区');
INSERT INTO `jiecheng_area` VALUES ('2161', '430500', '430511', '北塔区');
INSERT INTO `jiecheng_area` VALUES ('2162', '430500', '430522', '新邵县');
INSERT INTO `jiecheng_area` VALUES ('2163', '430500', '430523', '邵阳县');
INSERT INTO `jiecheng_area` VALUES ('2164', '430500', '430524', '隆回县');
INSERT INTO `jiecheng_area` VALUES ('2165', '430500', '430525', '洞口县');
INSERT INTO `jiecheng_area` VALUES ('2166', '430500', '430527', '绥宁县');
INSERT INTO `jiecheng_area` VALUES ('2167', '430500', '430528', '新宁县');
INSERT INTO `jiecheng_area` VALUES ('2168', '430500', '430529', '城步苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2169', '430500', '430581', '武冈市');
INSERT INTO `jiecheng_area` VALUES ('2170', '430500', '430582', '邵东市');
INSERT INTO `jiecheng_area` VALUES ('2171', '430600', '430601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2172', '430600', '430602', '岳阳楼区');
INSERT INTO `jiecheng_area` VALUES ('2173', '430600', '430603', '云溪区');
INSERT INTO `jiecheng_area` VALUES ('2174', '430600', '430611', '君山区');
INSERT INTO `jiecheng_area` VALUES ('2175', '430600', '430621', '岳阳县');
INSERT INTO `jiecheng_area` VALUES ('2176', '430600', '430623', '华容县');
INSERT INTO `jiecheng_area` VALUES ('2177', '430600', '430624', '湘阴县');
INSERT INTO `jiecheng_area` VALUES ('2178', '430600', '430626', '平江县');
INSERT INTO `jiecheng_area` VALUES ('2179', '430600', '430671', '岳阳市屈原管理区');
INSERT INTO `jiecheng_area` VALUES ('2180', '430600', '430681', '汨罗市');
INSERT INTO `jiecheng_area` VALUES ('2181', '430600', '430682', '临湘市');
INSERT INTO `jiecheng_area` VALUES ('2182', '430700', '430701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2183', '430700', '430702', '武陵区');
INSERT INTO `jiecheng_area` VALUES ('2184', '430700', '430703', '鼎城区');
INSERT INTO `jiecheng_area` VALUES ('2185', '430700', '430721', '安乡县');
INSERT INTO `jiecheng_area` VALUES ('2186', '430700', '430722', '汉寿县');
INSERT INTO `jiecheng_area` VALUES ('2187', '430700', '430723', '澧县');
INSERT INTO `jiecheng_area` VALUES ('2188', '430700', '430724', '临澧县');
INSERT INTO `jiecheng_area` VALUES ('2189', '430700', '430725', '桃源县');
INSERT INTO `jiecheng_area` VALUES ('2190', '430700', '430726', '石门县');
INSERT INTO `jiecheng_area` VALUES ('2191', '430700', '430771', '常德市西洞庭管理区');
INSERT INTO `jiecheng_area` VALUES ('2192', '430700', '430781', '津市市');
INSERT INTO `jiecheng_area` VALUES ('2193', '430800', '430801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2194', '430800', '430802', '永定区');
INSERT INTO `jiecheng_area` VALUES ('2195', '430800', '430811', '武陵源区');
INSERT INTO `jiecheng_area` VALUES ('2196', '430800', '430821', '慈利县');
INSERT INTO `jiecheng_area` VALUES ('2197', '430800', '430822', '桑植县');
INSERT INTO `jiecheng_area` VALUES ('2198', '430900', '430901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2199', '430900', '430902', '资阳区');
INSERT INTO `jiecheng_area` VALUES ('2200', '430900', '430903', '赫山区');
INSERT INTO `jiecheng_area` VALUES ('2201', '430900', '430921', '南县');
INSERT INTO `jiecheng_area` VALUES ('2202', '430900', '430922', '桃江县');
INSERT INTO `jiecheng_area` VALUES ('2203', '430900', '430923', '安化县');
INSERT INTO `jiecheng_area` VALUES ('2204', '430900', '430971', '益阳市大通湖管理区');
INSERT INTO `jiecheng_area` VALUES ('2205', '430900', '430972', '湖南益阳高新技术产业园区');
INSERT INTO `jiecheng_area` VALUES ('2206', '430900', '430981', '沅江市');
INSERT INTO `jiecheng_area` VALUES ('2207', '431000', '431001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2208', '431000', '431002', '北湖区');
INSERT INTO `jiecheng_area` VALUES ('2209', '431000', '431003', '苏仙区');
INSERT INTO `jiecheng_area` VALUES ('2210', '431000', '431021', '桂阳县');
INSERT INTO `jiecheng_area` VALUES ('2211', '431000', '431022', '宜章县');
INSERT INTO `jiecheng_area` VALUES ('2212', '431000', '431023', '永兴县');
INSERT INTO `jiecheng_area` VALUES ('2213', '431000', '431024', '嘉禾县');
INSERT INTO `jiecheng_area` VALUES ('2214', '431000', '431025', '临武县');
INSERT INTO `jiecheng_area` VALUES ('2215', '431000', '431026', '汝城县');
INSERT INTO `jiecheng_area` VALUES ('2216', '431000', '431027', '桂东县');
INSERT INTO `jiecheng_area` VALUES ('2217', '431000', '431028', '安仁县');
INSERT INTO `jiecheng_area` VALUES ('2218', '431000', '431081', '资兴市');
INSERT INTO `jiecheng_area` VALUES ('2219', '431100', '431101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2220', '431100', '431102', '零陵区');
INSERT INTO `jiecheng_area` VALUES ('2221', '431100', '431103', '冷水滩区');
INSERT INTO `jiecheng_area` VALUES ('2222', '431100', '431121', '祁阳县');
INSERT INTO `jiecheng_area` VALUES ('2223', '431100', '431122', '东安县');
INSERT INTO `jiecheng_area` VALUES ('2224', '431100', '431123', '双牌县');
INSERT INTO `jiecheng_area` VALUES ('2225', '431100', '431124', '道县');
INSERT INTO `jiecheng_area` VALUES ('2226', '431100', '431125', '江永县');
INSERT INTO `jiecheng_area` VALUES ('2227', '431100', '431126', '宁远县');
INSERT INTO `jiecheng_area` VALUES ('2228', '431100', '431127', '蓝山县');
INSERT INTO `jiecheng_area` VALUES ('2229', '431100', '431128', '新田县');
INSERT INTO `jiecheng_area` VALUES ('2230', '431100', '431129', '江华瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2231', '431100', '431171', '永州经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('2232', '431100', '431172', '永州市金洞管理区');
INSERT INTO `jiecheng_area` VALUES ('2233', '431100', '431173', '永州市回龙圩管理区');
INSERT INTO `jiecheng_area` VALUES ('2234', '431200', '431201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2235', '431200', '431202', '鹤城区');
INSERT INTO `jiecheng_area` VALUES ('2236', '431200', '431221', '中方县');
INSERT INTO `jiecheng_area` VALUES ('2237', '431200', '431222', '沅陵县');
INSERT INTO `jiecheng_area` VALUES ('2238', '431200', '431223', '辰溪县');
INSERT INTO `jiecheng_area` VALUES ('2239', '431200', '431224', '溆浦县');
INSERT INTO `jiecheng_area` VALUES ('2240', '431200', '431225', '会同县');
INSERT INTO `jiecheng_area` VALUES ('2241', '431200', '431226', '麻阳苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2242', '431200', '431227', '新晃侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2243', '431200', '431228', '芷江侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2244', '431200', '431229', '靖州苗族侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2245', '431200', '431230', '通道侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2246', '431200', '431271', '怀化市洪江管理区');
INSERT INTO `jiecheng_area` VALUES ('2247', '431200', '431281', '洪江市');
INSERT INTO `jiecheng_area` VALUES ('2248', '431300', '431301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2249', '431300', '431302', '娄星区');
INSERT INTO `jiecheng_area` VALUES ('2250', '431300', '431321', '双峰县');
INSERT INTO `jiecheng_area` VALUES ('2251', '431300', '431322', '新化县');
INSERT INTO `jiecheng_area` VALUES ('2252', '431300', '431381', '冷水江市');
INSERT INTO `jiecheng_area` VALUES ('2253', '431300', '431382', '涟源市');
INSERT INTO `jiecheng_area` VALUES ('2254', '433100', '433101', '吉首市');
INSERT INTO `jiecheng_area` VALUES ('2255', '433100', '433122', '泸溪县');
INSERT INTO `jiecheng_area` VALUES ('2256', '433100', '433123', '凤凰县');
INSERT INTO `jiecheng_area` VALUES ('2257', '433100', '433124', '花垣县');
INSERT INTO `jiecheng_area` VALUES ('2258', '433100', '433125', '保靖县');
INSERT INTO `jiecheng_area` VALUES ('2259', '433100', '433126', '古丈县');
INSERT INTO `jiecheng_area` VALUES ('2260', '433100', '433127', '永顺县');
INSERT INTO `jiecheng_area` VALUES ('2261', '433100', '433130', '龙山县');
INSERT INTO `jiecheng_area` VALUES ('2262', '433100', '433173', '湖南永顺经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2263', '440000', '440100', '广州市');
INSERT INTO `jiecheng_area` VALUES ('2264', '440000', '440200', '韶关市');
INSERT INTO `jiecheng_area` VALUES ('2265', '440000', '440300', '深圳市');
INSERT INTO `jiecheng_area` VALUES ('2266', '440000', '440400', '珠海市');
INSERT INTO `jiecheng_area` VALUES ('2267', '440000', '440500', '汕头市');
INSERT INTO `jiecheng_area` VALUES ('2268', '440000', '440600', '佛山市');
INSERT INTO `jiecheng_area` VALUES ('2269', '440000', '440700', '江门市');
INSERT INTO `jiecheng_area` VALUES ('2270', '440000', '440800', '湛江市');
INSERT INTO `jiecheng_area` VALUES ('2271', '440000', '440900', '茂名市');
INSERT INTO `jiecheng_area` VALUES ('2272', '440000', '441200', '肇庆市');
INSERT INTO `jiecheng_area` VALUES ('2273', '440000', '441300', '惠州市');
INSERT INTO `jiecheng_area` VALUES ('2274', '440000', '441400', '梅州市');
INSERT INTO `jiecheng_area` VALUES ('2275', '440000', '441500', '汕尾市');
INSERT INTO `jiecheng_area` VALUES ('2276', '440000', '441600', '河源市');
INSERT INTO `jiecheng_area` VALUES ('2277', '440000', '441700', '阳江市');
INSERT INTO `jiecheng_area` VALUES ('2278', '440000', '441800', '清远市');
INSERT INTO `jiecheng_area` VALUES ('2279', '440000', '441900', '东莞市');
INSERT INTO `jiecheng_area` VALUES ('2280', '440000', '442000', '中山市');
INSERT INTO `jiecheng_area` VALUES ('2281', '440000', '445100', '潮州市');
INSERT INTO `jiecheng_area` VALUES ('2282', '440000', '445200', '揭阳市');
INSERT INTO `jiecheng_area` VALUES ('2283', '440000', '445300', '云浮市');
INSERT INTO `jiecheng_area` VALUES ('2284', '440100', '440101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2285', '440100', '440103', '荔湾区');
INSERT INTO `jiecheng_area` VALUES ('2286', '440100', '440104', '越秀区');
INSERT INTO `jiecheng_area` VALUES ('2287', '440100', '440105', '海珠区');
INSERT INTO `jiecheng_area` VALUES ('2288', '440100', '440106', '天河区');
INSERT INTO `jiecheng_area` VALUES ('2289', '440100', '440111', '白云区');
INSERT INTO `jiecheng_area` VALUES ('2290', '440100', '440112', '黄埔区');
INSERT INTO `jiecheng_area` VALUES ('2291', '440100', '440113', '番禺区');
INSERT INTO `jiecheng_area` VALUES ('2292', '440100', '440114', '花都区');
INSERT INTO `jiecheng_area` VALUES ('2293', '440100', '440115', '南沙区');
INSERT INTO `jiecheng_area` VALUES ('2294', '440100', '440117', '从化区');
INSERT INTO `jiecheng_area` VALUES ('2295', '440100', '440118', '增城区');
INSERT INTO `jiecheng_area` VALUES ('2296', '440200', '440201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2297', '440200', '440203', '武江区');
INSERT INTO `jiecheng_area` VALUES ('2298', '440200', '440204', '浈江区');
INSERT INTO `jiecheng_area` VALUES ('2299', '440200', '440205', '曲江区');
INSERT INTO `jiecheng_area` VALUES ('2300', '440200', '440222', '始兴县');
INSERT INTO `jiecheng_area` VALUES ('2301', '440200', '440224', '仁化县');
INSERT INTO `jiecheng_area` VALUES ('2302', '440200', '440229', '翁源县');
INSERT INTO `jiecheng_area` VALUES ('2303', '440200', '440232', '乳源瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2304', '440200', '440233', '新丰县');
INSERT INTO `jiecheng_area` VALUES ('2305', '440200', '440281', '乐昌市');
INSERT INTO `jiecheng_area` VALUES ('2306', '440200', '440282', '南雄市');
INSERT INTO `jiecheng_area` VALUES ('2307', '440300', '440301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2308', '440300', '440303', '罗湖区');
INSERT INTO `jiecheng_area` VALUES ('2309', '440300', '440304', '福田区');
INSERT INTO `jiecheng_area` VALUES ('2310', '440300', '440305', '南山区');
INSERT INTO `jiecheng_area` VALUES ('2311', '440300', '440306', '宝安区');
INSERT INTO `jiecheng_area` VALUES ('2312', '440300', '440307', '龙岗区');
INSERT INTO `jiecheng_area` VALUES ('2313', '440300', '440308', '盐田区');
INSERT INTO `jiecheng_area` VALUES ('2314', '440300', '440309', '龙华区');
INSERT INTO `jiecheng_area` VALUES ('2315', '440300', '440310', '坪山区');
INSERT INTO `jiecheng_area` VALUES ('2316', '440300', '440311', '光明区');
INSERT INTO `jiecheng_area` VALUES ('2317', '440400', '440401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2318', '440400', '440402', '香洲区');
INSERT INTO `jiecheng_area` VALUES ('2319', '440400', '440403', '斗门区');
INSERT INTO `jiecheng_area` VALUES ('2320', '440400', '440404', '金湾区');
INSERT INTO `jiecheng_area` VALUES ('2321', '440500', '440501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2322', '440500', '440507', '龙湖区');
INSERT INTO `jiecheng_area` VALUES ('2323', '440500', '440511', '金平区');
INSERT INTO `jiecheng_area` VALUES ('2324', '440500', '440512', '濠江区');
INSERT INTO `jiecheng_area` VALUES ('2325', '440500', '440513', '潮阳区');
INSERT INTO `jiecheng_area` VALUES ('2326', '440500', '440514', '潮南区');
INSERT INTO `jiecheng_area` VALUES ('2327', '440500', '440515', '澄海区');
INSERT INTO `jiecheng_area` VALUES ('2328', '440500', '440523', '南澳县');
INSERT INTO `jiecheng_area` VALUES ('2329', '440600', '440601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2330', '440600', '440604', '禅城区');
INSERT INTO `jiecheng_area` VALUES ('2331', '440600', '440605', '南海区');
INSERT INTO `jiecheng_area` VALUES ('2332', '440600', '440606', '顺德区');
INSERT INTO `jiecheng_area` VALUES ('2333', '440600', '440607', '三水区');
INSERT INTO `jiecheng_area` VALUES ('2334', '440600', '440608', '高明区');
INSERT INTO `jiecheng_area` VALUES ('2335', '440700', '440701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2336', '440700', '440703', '蓬江区');
INSERT INTO `jiecheng_area` VALUES ('2337', '440700', '440704', '江海区');
INSERT INTO `jiecheng_area` VALUES ('2338', '440700', '440705', '新会区');
INSERT INTO `jiecheng_area` VALUES ('2339', '440700', '440781', '台山市');
INSERT INTO `jiecheng_area` VALUES ('2340', '440700', '440783', '开平市');
INSERT INTO `jiecheng_area` VALUES ('2341', '440700', '440784', '鹤山市');
INSERT INTO `jiecheng_area` VALUES ('2342', '440700', '440785', '恩平市');
INSERT INTO `jiecheng_area` VALUES ('2343', '440800', '440801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2344', '440800', '440802', '赤坎区');
INSERT INTO `jiecheng_area` VALUES ('2345', '440800', '440803', '霞山区');
INSERT INTO `jiecheng_area` VALUES ('2346', '440800', '440804', '坡头区');
INSERT INTO `jiecheng_area` VALUES ('2347', '440800', '440811', '麻章区');
INSERT INTO `jiecheng_area` VALUES ('2348', '440800', '440823', '遂溪县');
INSERT INTO `jiecheng_area` VALUES ('2349', '440800', '440825', '徐闻县');
INSERT INTO `jiecheng_area` VALUES ('2350', '440800', '440881', '廉江市');
INSERT INTO `jiecheng_area` VALUES ('2351', '440800', '440882', '雷州市');
INSERT INTO `jiecheng_area` VALUES ('2352', '440800', '440883', '吴川市');
INSERT INTO `jiecheng_area` VALUES ('2353', '440900', '440901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2354', '440900', '440902', '茂南区');
INSERT INTO `jiecheng_area` VALUES ('2355', '440900', '440904', '电白区');
INSERT INTO `jiecheng_area` VALUES ('2356', '440900', '440981', '高州市');
INSERT INTO `jiecheng_area` VALUES ('2357', '440900', '440982', '化州市');
INSERT INTO `jiecheng_area` VALUES ('2358', '440900', '440983', '信宜市');
INSERT INTO `jiecheng_area` VALUES ('2359', '441200', '441201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2360', '441200', '441202', '端州区');
INSERT INTO `jiecheng_area` VALUES ('2361', '441200', '441203', '鼎湖区');
INSERT INTO `jiecheng_area` VALUES ('2362', '441200', '441204', '高要区');
INSERT INTO `jiecheng_area` VALUES ('2363', '441200', '441223', '广宁县');
INSERT INTO `jiecheng_area` VALUES ('2364', '441200', '441224', '怀集县');
INSERT INTO `jiecheng_area` VALUES ('2365', '441200', '441225', '封开县');
INSERT INTO `jiecheng_area` VALUES ('2366', '441200', '441226', '德庆县');
INSERT INTO `jiecheng_area` VALUES ('2367', '441200', '441284', '四会市');
INSERT INTO `jiecheng_area` VALUES ('2368', '441300', '441301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2369', '441300', '441302', '惠城区');
INSERT INTO `jiecheng_area` VALUES ('2370', '441300', '441303', '惠阳区');
INSERT INTO `jiecheng_area` VALUES ('2371', '441300', '441322', '博罗县');
INSERT INTO `jiecheng_area` VALUES ('2372', '441300', '441323', '惠东县');
INSERT INTO `jiecheng_area` VALUES ('2373', '441300', '441324', '龙门县');
INSERT INTO `jiecheng_area` VALUES ('2374', '441400', '441401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2375', '441400', '441402', '梅江区');
INSERT INTO `jiecheng_area` VALUES ('2376', '441400', '441403', '梅县区');
INSERT INTO `jiecheng_area` VALUES ('2377', '441400', '441422', '大埔县');
INSERT INTO `jiecheng_area` VALUES ('2378', '441400', '441423', '丰顺县');
INSERT INTO `jiecheng_area` VALUES ('2379', '441400', '441424', '五华县');
INSERT INTO `jiecheng_area` VALUES ('2380', '441400', '441426', '平远县');
INSERT INTO `jiecheng_area` VALUES ('2381', '441400', '441427', '蕉岭县');
INSERT INTO `jiecheng_area` VALUES ('2382', '441400', '441481', '兴宁市');
INSERT INTO `jiecheng_area` VALUES ('2383', '441500', '441501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2384', '441500', '441502', '城区');
INSERT INTO `jiecheng_area` VALUES ('2385', '441500', '441521', '海丰县');
INSERT INTO `jiecheng_area` VALUES ('2386', '441500', '441523', '陆河县');
INSERT INTO `jiecheng_area` VALUES ('2387', '441500', '441581', '陆丰市');
INSERT INTO `jiecheng_area` VALUES ('2388', '441600', '441601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2389', '441600', '441602', '源城区');
INSERT INTO `jiecheng_area` VALUES ('2390', '441600', '441621', '紫金县');
INSERT INTO `jiecheng_area` VALUES ('2391', '441600', '441622', '龙川县');
INSERT INTO `jiecheng_area` VALUES ('2392', '441600', '441623', '连平县');
INSERT INTO `jiecheng_area` VALUES ('2393', '441600', '441624', '和平县');
INSERT INTO `jiecheng_area` VALUES ('2394', '441600', '441625', '东源县');
INSERT INTO `jiecheng_area` VALUES ('2395', '441700', '441701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2396', '441700', '441702', '江城区');
INSERT INTO `jiecheng_area` VALUES ('2397', '441700', '441704', '阳东区');
INSERT INTO `jiecheng_area` VALUES ('2398', '441700', '441721', '阳西县');
INSERT INTO `jiecheng_area` VALUES ('2399', '441700', '441781', '阳春市');
INSERT INTO `jiecheng_area` VALUES ('2400', '441800', '441801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2401', '441800', '441802', '清城区');
INSERT INTO `jiecheng_area` VALUES ('2402', '441800', '441803', '清新区');
INSERT INTO `jiecheng_area` VALUES ('2403', '441800', '441821', '佛冈县');
INSERT INTO `jiecheng_area` VALUES ('2404', '441800', '441823', '阳山县');
INSERT INTO `jiecheng_area` VALUES ('2405', '441800', '441825', '连山壮族瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2406', '441800', '441826', '连南瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2407', '441800', '441881', '英德市');
INSERT INTO `jiecheng_area` VALUES ('2408', '441800', '441882', '连州市');
INSERT INTO `jiecheng_area` VALUES ('2409', '441900', '441900003', '东城街道');
INSERT INTO `jiecheng_area` VALUES ('2410', '441900', '441900004', '南城街道');
INSERT INTO `jiecheng_area` VALUES ('2411', '441900', '441900005', '万江街道');
INSERT INTO `jiecheng_area` VALUES ('2412', '441900', '441900006', '莞城街道');
INSERT INTO `jiecheng_area` VALUES ('2413', '441900', '441900101', '石碣镇');
INSERT INTO `jiecheng_area` VALUES ('2414', '441900', '441900102', '石龙镇');
INSERT INTO `jiecheng_area` VALUES ('2415', '441900', '441900103', '茶山镇');
INSERT INTO `jiecheng_area` VALUES ('2416', '441900', '441900104', '石排镇');
INSERT INTO `jiecheng_area` VALUES ('2417', '441900', '441900105', '企石镇');
INSERT INTO `jiecheng_area` VALUES ('2418', '441900', '441900106', '横沥镇');
INSERT INTO `jiecheng_area` VALUES ('2419', '441900', '441900107', '桥头镇');
INSERT INTO `jiecheng_area` VALUES ('2420', '441900', '441900108', '谢岗镇');
INSERT INTO `jiecheng_area` VALUES ('2421', '441900', '441900109', '东坑镇');
INSERT INTO `jiecheng_area` VALUES ('2422', '441900', '441900110', '常平镇');
INSERT INTO `jiecheng_area` VALUES ('2423', '441900', '441900111', '寮步镇');
INSERT INTO `jiecheng_area` VALUES ('2424', '441900', '441900112', '樟木头镇');
INSERT INTO `jiecheng_area` VALUES ('2425', '441900', '441900113', '大朗镇');
INSERT INTO `jiecheng_area` VALUES ('2426', '441900', '441900114', '黄江镇');
INSERT INTO `jiecheng_area` VALUES ('2427', '441900', '441900115', '清溪镇');
INSERT INTO `jiecheng_area` VALUES ('2428', '441900', '441900116', '塘厦镇');
INSERT INTO `jiecheng_area` VALUES ('2429', '441900', '441900117', '凤岗镇');
INSERT INTO `jiecheng_area` VALUES ('2430', '441900', '441900118', '大岭山镇');
INSERT INTO `jiecheng_area` VALUES ('2431', '441900', '441900119', '长安镇');
INSERT INTO `jiecheng_area` VALUES ('2432', '441900', '441900121', '虎门镇');
INSERT INTO `jiecheng_area` VALUES ('2433', '441900', '441900122', '厚街镇');
INSERT INTO `jiecheng_area` VALUES ('2434', '441900', '441900123', '沙田镇');
INSERT INTO `jiecheng_area` VALUES ('2435', '441900', '441900124', '道滘镇');
INSERT INTO `jiecheng_area` VALUES ('2436', '441900', '441900125', '洪梅镇');
INSERT INTO `jiecheng_area` VALUES ('2437', '441900', '441900126', '麻涌镇');
INSERT INTO `jiecheng_area` VALUES ('2438', '441900', '441900127', '望牛墩镇');
INSERT INTO `jiecheng_area` VALUES ('2439', '441900', '441900128', '中堂镇');
INSERT INTO `jiecheng_area` VALUES ('2440', '441900', '441900129', '高埗镇');
INSERT INTO `jiecheng_area` VALUES ('2441', '441900', '441900401', '松山湖');
INSERT INTO `jiecheng_area` VALUES ('2442', '441900', '441900402', '东莞港');
INSERT INTO `jiecheng_area` VALUES ('2443', '441900', '441900403', '东莞生态园');
INSERT INTO `jiecheng_area` VALUES ('2444', '442000', '442000001', '石岐街道');
INSERT INTO `jiecheng_area` VALUES ('2445', '442000', '442000002', '东区街道');
INSERT INTO `jiecheng_area` VALUES ('2446', '442000', '442000003', '中山港街道');
INSERT INTO `jiecheng_area` VALUES ('2447', '442000', '442000004', '西区街道');
INSERT INTO `jiecheng_area` VALUES ('2448', '442000', '442000005', '南区街道');
INSERT INTO `jiecheng_area` VALUES ('2449', '442000', '442000006', '五桂山街道');
INSERT INTO `jiecheng_area` VALUES ('2450', '442000', '442000100', '小榄镇');
INSERT INTO `jiecheng_area` VALUES ('2451', '442000', '442000101', '黄圃镇');
INSERT INTO `jiecheng_area` VALUES ('2452', '442000', '442000102', '民众镇');
INSERT INTO `jiecheng_area` VALUES ('2453', '442000', '442000103', '东凤镇');
INSERT INTO `jiecheng_area` VALUES ('2454', '442000', '442000104', '东升镇');
INSERT INTO `jiecheng_area` VALUES ('2455', '442000', '442000105', '古镇镇');
INSERT INTO `jiecheng_area` VALUES ('2456', '442000', '442000106', '沙溪镇');
INSERT INTO `jiecheng_area` VALUES ('2457', '442000', '442000107', '坦洲镇');
INSERT INTO `jiecheng_area` VALUES ('2458', '442000', '442000108', '港口镇');
INSERT INTO `jiecheng_area` VALUES ('2459', '442000', '442000109', '三角镇');
INSERT INTO `jiecheng_area` VALUES ('2460', '442000', '442000110', '横栏镇');
INSERT INTO `jiecheng_area` VALUES ('2461', '442000', '442000111', '南头镇');
INSERT INTO `jiecheng_area` VALUES ('2462', '442000', '442000112', '阜沙镇');
INSERT INTO `jiecheng_area` VALUES ('2463', '442000', '442000113', '南朗镇');
INSERT INTO `jiecheng_area` VALUES ('2464', '442000', '442000114', '三乡镇');
INSERT INTO `jiecheng_area` VALUES ('2465', '442000', '442000115', '板芙镇');
INSERT INTO `jiecheng_area` VALUES ('2466', '442000', '442000116', '大涌镇');
INSERT INTO `jiecheng_area` VALUES ('2467', '442000', '442000117', '神湾镇');
INSERT INTO `jiecheng_area` VALUES ('2468', '445100', '445101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2469', '445100', '445102', '湘桥区');
INSERT INTO `jiecheng_area` VALUES ('2470', '445100', '445103', '潮安区');
INSERT INTO `jiecheng_area` VALUES ('2471', '445100', '445122', '饶平县');
INSERT INTO `jiecheng_area` VALUES ('2472', '445200', '445201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2473', '445200', '445202', '榕城区');
INSERT INTO `jiecheng_area` VALUES ('2474', '445200', '445203', '揭东区');
INSERT INTO `jiecheng_area` VALUES ('2475', '445200', '445222', '揭西县');
INSERT INTO `jiecheng_area` VALUES ('2476', '445200', '445224', '惠来县');
INSERT INTO `jiecheng_area` VALUES ('2477', '445200', '445281', '普宁市');
INSERT INTO `jiecheng_area` VALUES ('2478', '445300', '445301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2479', '445300', '445302', '云城区');
INSERT INTO `jiecheng_area` VALUES ('2480', '445300', '445303', '云安区');
INSERT INTO `jiecheng_area` VALUES ('2481', '445300', '445321', '新兴县');
INSERT INTO `jiecheng_area` VALUES ('2482', '445300', '445322', '郁南县');
INSERT INTO `jiecheng_area` VALUES ('2483', '445300', '445381', '罗定市');
INSERT INTO `jiecheng_area` VALUES ('2484', '450000', '450100', '南宁市');
INSERT INTO `jiecheng_area` VALUES ('2485', '450000', '450200', '柳州市');
INSERT INTO `jiecheng_area` VALUES ('2486', '450000', '450300', '桂林市');
INSERT INTO `jiecheng_area` VALUES ('2487', '450000', '450400', '梧州市');
INSERT INTO `jiecheng_area` VALUES ('2488', '450000', '450500', '北海市');
INSERT INTO `jiecheng_area` VALUES ('2489', '450000', '450600', '防城港市');
INSERT INTO `jiecheng_area` VALUES ('2490', '450000', '450700', '钦州市');
INSERT INTO `jiecheng_area` VALUES ('2491', '450000', '450800', '贵港市');
INSERT INTO `jiecheng_area` VALUES ('2492', '450000', '450900', '玉林市');
INSERT INTO `jiecheng_area` VALUES ('2493', '450000', '451000', '百色市');
INSERT INTO `jiecheng_area` VALUES ('2494', '450000', '451100', '贺州市');
INSERT INTO `jiecheng_area` VALUES ('2495', '450000', '451200', '河池市');
INSERT INTO `jiecheng_area` VALUES ('2496', '450000', '451300', '来宾市');
INSERT INTO `jiecheng_area` VALUES ('2497', '450000', '451400', '崇左市');
INSERT INTO `jiecheng_area` VALUES ('2498', '450100', '450101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2499', '450100', '450102', '兴宁区');
INSERT INTO `jiecheng_area` VALUES ('2500', '450100', '450103', '青秀区');
INSERT INTO `jiecheng_area` VALUES ('2501', '450100', '450105', '江南区');
INSERT INTO `jiecheng_area` VALUES ('2502', '450100', '450107', '西乡塘区');
INSERT INTO `jiecheng_area` VALUES ('2503', '450100', '450108', '良庆区');
INSERT INTO `jiecheng_area` VALUES ('2504', '450100', '450109', '邕宁区');
INSERT INTO `jiecheng_area` VALUES ('2505', '450100', '450110', '武鸣区');
INSERT INTO `jiecheng_area` VALUES ('2506', '450100', '450123', '隆安县');
INSERT INTO `jiecheng_area` VALUES ('2507', '450100', '450124', '马山县');
INSERT INTO `jiecheng_area` VALUES ('2508', '450100', '450125', '上林县');
INSERT INTO `jiecheng_area` VALUES ('2509', '450100', '450126', '宾阳县');
INSERT INTO `jiecheng_area` VALUES ('2510', '450100', '450127', '横县');
INSERT INTO `jiecheng_area` VALUES ('2511', '450200', '450201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2512', '450200', '450202', '城中区');
INSERT INTO `jiecheng_area` VALUES ('2513', '450200', '450203', '鱼峰区');
INSERT INTO `jiecheng_area` VALUES ('2514', '450200', '450204', '柳南区');
INSERT INTO `jiecheng_area` VALUES ('2515', '450200', '450205', '柳北区');
INSERT INTO `jiecheng_area` VALUES ('2516', '450200', '450206', '柳江区');
INSERT INTO `jiecheng_area` VALUES ('2517', '450200', '450222', '柳城县');
INSERT INTO `jiecheng_area` VALUES ('2518', '450200', '450223', '鹿寨县');
INSERT INTO `jiecheng_area` VALUES ('2519', '450200', '450224', '融安县');
INSERT INTO `jiecheng_area` VALUES ('2520', '450200', '450225', '融水苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2521', '450200', '450226', '三江侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2522', '450300', '450301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2523', '450300', '450302', '秀峰区');
INSERT INTO `jiecheng_area` VALUES ('2524', '450300', '450303', '叠彩区');
INSERT INTO `jiecheng_area` VALUES ('2525', '450300', '450304', '象山区');
INSERT INTO `jiecheng_area` VALUES ('2526', '450300', '450305', '七星区');
INSERT INTO `jiecheng_area` VALUES ('2527', '450300', '450311', '雁山区');
INSERT INTO `jiecheng_area` VALUES ('2528', '450300', '450312', '临桂区');
INSERT INTO `jiecheng_area` VALUES ('2529', '450300', '450321', '阳朔县');
INSERT INTO `jiecheng_area` VALUES ('2530', '450300', '450323', '灵川县');
INSERT INTO `jiecheng_area` VALUES ('2531', '450300', '450324', '全州县');
INSERT INTO `jiecheng_area` VALUES ('2532', '450300', '450325', '兴安县');
INSERT INTO `jiecheng_area` VALUES ('2533', '450300', '450326', '永福县');
INSERT INTO `jiecheng_area` VALUES ('2534', '450300', '450327', '灌阳县');
INSERT INTO `jiecheng_area` VALUES ('2535', '450300', '450328', '龙胜各族自治县');
INSERT INTO `jiecheng_area` VALUES ('2536', '450300', '450329', '资源县');
INSERT INTO `jiecheng_area` VALUES ('2537', '450300', '450330', '平乐县');
INSERT INTO `jiecheng_area` VALUES ('2538', '450300', '450332', '恭城瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2539', '450300', '450381', '荔浦市');
INSERT INTO `jiecheng_area` VALUES ('2540', '450400', '450401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2541', '450400', '450403', '万秀区');
INSERT INTO `jiecheng_area` VALUES ('2542', '450400', '450405', '长洲区');
INSERT INTO `jiecheng_area` VALUES ('2543', '450400', '450406', '龙圩区');
INSERT INTO `jiecheng_area` VALUES ('2544', '450400', '450421', '苍梧县');
INSERT INTO `jiecheng_area` VALUES ('2545', '450400', '450422', '藤县');
INSERT INTO `jiecheng_area` VALUES ('2546', '450400', '450423', '蒙山县');
INSERT INTO `jiecheng_area` VALUES ('2547', '450400', '450481', '岑溪市');
INSERT INTO `jiecheng_area` VALUES ('2548', '450500', '450501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2549', '450500', '450502', '海城区');
INSERT INTO `jiecheng_area` VALUES ('2550', '450500', '450503', '银海区');
INSERT INTO `jiecheng_area` VALUES ('2551', '450500', '450512', '铁山港区');
INSERT INTO `jiecheng_area` VALUES ('2552', '450500', '450521', '合浦县');
INSERT INTO `jiecheng_area` VALUES ('2553', '450600', '450601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2554', '450600', '450602', '港口区');
INSERT INTO `jiecheng_area` VALUES ('2555', '450600', '450603', '防城区');
INSERT INTO `jiecheng_area` VALUES ('2556', '450600', '450621', '上思县');
INSERT INTO `jiecheng_area` VALUES ('2557', '450600', '450681', '东兴市');
INSERT INTO `jiecheng_area` VALUES ('2558', '450700', '450701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2559', '450700', '450702', '钦南区');
INSERT INTO `jiecheng_area` VALUES ('2560', '450700', '450703', '钦北区');
INSERT INTO `jiecheng_area` VALUES ('2561', '450700', '450721', '灵山县');
INSERT INTO `jiecheng_area` VALUES ('2562', '450700', '450722', '浦北县');
INSERT INTO `jiecheng_area` VALUES ('2563', '450800', '450801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2564', '450800', '450802', '港北区');
INSERT INTO `jiecheng_area` VALUES ('2565', '450800', '450803', '港南区');
INSERT INTO `jiecheng_area` VALUES ('2566', '450800', '450804', '覃塘区');
INSERT INTO `jiecheng_area` VALUES ('2567', '450800', '450821', '平南县');
INSERT INTO `jiecheng_area` VALUES ('2568', '450800', '450881', '桂平市');
INSERT INTO `jiecheng_area` VALUES ('2569', '450900', '450901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2570', '450900', '450902', '玉州区');
INSERT INTO `jiecheng_area` VALUES ('2571', '450900', '450903', '福绵区');
INSERT INTO `jiecheng_area` VALUES ('2572', '450900', '450921', '容县');
INSERT INTO `jiecheng_area` VALUES ('2573', '450900', '450922', '陆川县');
INSERT INTO `jiecheng_area` VALUES ('2574', '450900', '450923', '博白县');
INSERT INTO `jiecheng_area` VALUES ('2575', '450900', '450924', '兴业县');
INSERT INTO `jiecheng_area` VALUES ('2576', '450900', '450981', '北流市');
INSERT INTO `jiecheng_area` VALUES ('2577', '451000', '451001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2578', '451000', '451002', '右江区');
INSERT INTO `jiecheng_area` VALUES ('2579', '451000', '451003', '田阳区');
INSERT INTO `jiecheng_area` VALUES ('2580', '451000', '451022', '田东县');
INSERT INTO `jiecheng_area` VALUES ('2581', '451000', '451023', '平果县');
INSERT INTO `jiecheng_area` VALUES ('2582', '451000', '451024', '德保县');
INSERT INTO `jiecheng_area` VALUES ('2583', '451000', '451026', '那坡县');
INSERT INTO `jiecheng_area` VALUES ('2584', '451000', '451027', '凌云县');
INSERT INTO `jiecheng_area` VALUES ('2585', '451000', '451028', '乐业县');
INSERT INTO `jiecheng_area` VALUES ('2586', '451000', '451029', '田林县');
INSERT INTO `jiecheng_area` VALUES ('2587', '451000', '451030', '西林县');
INSERT INTO `jiecheng_area` VALUES ('2588', '451000', '451031', '隆林各族自治县');
INSERT INTO `jiecheng_area` VALUES ('2589', '451000', '451081', '靖西市');
INSERT INTO `jiecheng_area` VALUES ('2590', '451100', '451101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2591', '451100', '451102', '八步区');
INSERT INTO `jiecheng_area` VALUES ('2592', '451100', '451103', '平桂区');
INSERT INTO `jiecheng_area` VALUES ('2593', '451100', '451121', '昭平县');
INSERT INTO `jiecheng_area` VALUES ('2594', '451100', '451122', '钟山县');
INSERT INTO `jiecheng_area` VALUES ('2595', '451100', '451123', '富川瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2596', '451200', '451201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2597', '451200', '451202', '金城江区');
INSERT INTO `jiecheng_area` VALUES ('2598', '451200', '451203', '宜州区');
INSERT INTO `jiecheng_area` VALUES ('2599', '451200', '451221', '南丹县');
INSERT INTO `jiecheng_area` VALUES ('2600', '451200', '451222', '天峨县');
INSERT INTO `jiecheng_area` VALUES ('2601', '451200', '451223', '凤山县');
INSERT INTO `jiecheng_area` VALUES ('2602', '451200', '451224', '东兰县');
INSERT INTO `jiecheng_area` VALUES ('2603', '451200', '451225', '罗城仫佬族自治县');
INSERT INTO `jiecheng_area` VALUES ('2604', '451200', '451226', '环江毛南族自治县');
INSERT INTO `jiecheng_area` VALUES ('2605', '451200', '451227', '巴马瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2606', '451200', '451228', '都安瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2607', '451200', '451229', '大化瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2608', '451300', '451301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2609', '451300', '451302', '兴宾区');
INSERT INTO `jiecheng_area` VALUES ('2610', '451300', '451321', '忻城县');
INSERT INTO `jiecheng_area` VALUES ('2611', '451300', '451322', '象州县');
INSERT INTO `jiecheng_area` VALUES ('2612', '451300', '451323', '武宣县');
INSERT INTO `jiecheng_area` VALUES ('2613', '451300', '451324', '金秀瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('2614', '451300', '451381', '合山市');
INSERT INTO `jiecheng_area` VALUES ('2615', '451400', '451401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2616', '451400', '451402', '江州区');
INSERT INTO `jiecheng_area` VALUES ('2617', '451400', '451421', '扶绥县');
INSERT INTO `jiecheng_area` VALUES ('2618', '451400', '451422', '宁明县');
INSERT INTO `jiecheng_area` VALUES ('2619', '451400', '451423', '龙州县');
INSERT INTO `jiecheng_area` VALUES ('2620', '451400', '451424', '大新县');
INSERT INTO `jiecheng_area` VALUES ('2621', '451400', '451425', '天等县');
INSERT INTO `jiecheng_area` VALUES ('2622', '451400', '451481', '凭祥市');
INSERT INTO `jiecheng_area` VALUES ('2623', '460000', '460100', '海口市');
INSERT INTO `jiecheng_area` VALUES ('2624', '460000', '460200', '三亚市');
INSERT INTO `jiecheng_area` VALUES ('2625', '460000', '460300', '三沙市');
INSERT INTO `jiecheng_area` VALUES ('2626', '460000', '460400', '儋州市');
INSERT INTO `jiecheng_area` VALUES ('2627', '460000', '469000', '省直辖县级行政区划');
INSERT INTO `jiecheng_area` VALUES ('2628', '460100', '460101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2629', '460100', '460105', '秀英区');
INSERT INTO `jiecheng_area` VALUES ('2630', '460100', '460106', '龙华区');
INSERT INTO `jiecheng_area` VALUES ('2631', '460100', '460107', '琼山区');
INSERT INTO `jiecheng_area` VALUES ('2632', '460100', '460108', '美兰区');
INSERT INTO `jiecheng_area` VALUES ('2633', '460200', '460201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2634', '460200', '460202', '海棠区');
INSERT INTO `jiecheng_area` VALUES ('2635', '460200', '460203', '吉阳区');
INSERT INTO `jiecheng_area` VALUES ('2636', '460200', '460204', '天涯区');
INSERT INTO `jiecheng_area` VALUES ('2637', '460200', '460205', '崖州区');
INSERT INTO `jiecheng_area` VALUES ('2638', '460300', '460321', '西沙群岛');
INSERT INTO `jiecheng_area` VALUES ('2639', '460300', '460322', '南沙群岛');
INSERT INTO `jiecheng_area` VALUES ('2640', '460300', '460323', '中沙群岛的岛礁及其海域');
INSERT INTO `jiecheng_area` VALUES ('2641', '460400', '460400100', '那大镇');
INSERT INTO `jiecheng_area` VALUES ('2642', '460400', '460400101', '和庆镇');
INSERT INTO `jiecheng_area` VALUES ('2643', '460400', '460400102', '南丰镇');
INSERT INTO `jiecheng_area` VALUES ('2644', '460400', '460400103', '大成镇');
INSERT INTO `jiecheng_area` VALUES ('2645', '460400', '460400104', '雅星镇');
INSERT INTO `jiecheng_area` VALUES ('2646', '460400', '460400105', '兰洋镇');
INSERT INTO `jiecheng_area` VALUES ('2647', '460400', '460400106', '光村镇');
INSERT INTO `jiecheng_area` VALUES ('2648', '460400', '460400107', '木棠镇');
INSERT INTO `jiecheng_area` VALUES ('2649', '460400', '460400108', '海头镇');
INSERT INTO `jiecheng_area` VALUES ('2650', '460400', '460400109', '峨蔓镇');
INSERT INTO `jiecheng_area` VALUES ('2651', '460400', '460400111', '王五镇');
INSERT INTO `jiecheng_area` VALUES ('2652', '460400', '460400112', '白马井镇');
INSERT INTO `jiecheng_area` VALUES ('2653', '460400', '460400113', '中和镇');
INSERT INTO `jiecheng_area` VALUES ('2654', '460400', '460400114', '排浦镇');
INSERT INTO `jiecheng_area` VALUES ('2655', '460400', '460400115', '东成镇');
INSERT INTO `jiecheng_area` VALUES ('2656', '460400', '460400116', '新州镇');
INSERT INTO `jiecheng_area` VALUES ('2657', '460400', '460400499', '洋浦经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2658', '460400', '460400500', '华南热作学院');
INSERT INTO `jiecheng_area` VALUES ('2659', '469000', '469001', '五指山市');
INSERT INTO `jiecheng_area` VALUES ('2660', '469000', '469002', '琼海市');
INSERT INTO `jiecheng_area` VALUES ('2661', '469000', '469005', '文昌市');
INSERT INTO `jiecheng_area` VALUES ('2662', '469000', '469006', '万宁市');
INSERT INTO `jiecheng_area` VALUES ('2663', '469000', '469007', '东方市');
INSERT INTO `jiecheng_area` VALUES ('2664', '469000', '469021', '定安县');
INSERT INTO `jiecheng_area` VALUES ('2665', '469000', '469022', '屯昌县');
INSERT INTO `jiecheng_area` VALUES ('2666', '469000', '469023', '澄迈县');
INSERT INTO `jiecheng_area` VALUES ('2667', '469000', '469024', '临高县');
INSERT INTO `jiecheng_area` VALUES ('2668', '469000', '469025', '白沙黎族自治县');
INSERT INTO `jiecheng_area` VALUES ('2669', '469000', '469026', '昌江黎族自治县');
INSERT INTO `jiecheng_area` VALUES ('2670', '469000', '469027', '乐东黎族自治县');
INSERT INTO `jiecheng_area` VALUES ('2671', '469000', '469028', '陵水黎族自治县');
INSERT INTO `jiecheng_area` VALUES ('2672', '469000', '469029', '保亭黎族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2673', '469000', '469030', '琼中黎族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2674', '500000', '500100', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2675', '500000', '500200', '县');
INSERT INTO `jiecheng_area` VALUES ('2676', '500100', '500101', '万州区');
INSERT INTO `jiecheng_area` VALUES ('2677', '500100', '500102', '涪陵区');
INSERT INTO `jiecheng_area` VALUES ('2678', '500100', '500103', '渝中区');
INSERT INTO `jiecheng_area` VALUES ('2679', '500100', '500104', '大渡口区');
INSERT INTO `jiecheng_area` VALUES ('2680', '500100', '500105', '江北区');
INSERT INTO `jiecheng_area` VALUES ('2681', '500100', '500106', '沙坪坝区');
INSERT INTO `jiecheng_area` VALUES ('2682', '500100', '500107', '九龙坡区');
INSERT INTO `jiecheng_area` VALUES ('2683', '500100', '500108', '南岸区');
INSERT INTO `jiecheng_area` VALUES ('2684', '500100', '500109', '北碚区');
INSERT INTO `jiecheng_area` VALUES ('2685', '500100', '500110', '綦江区');
INSERT INTO `jiecheng_area` VALUES ('2686', '500100', '500111', '大足区');
INSERT INTO `jiecheng_area` VALUES ('2687', '500100', '500112', '渝北区');
INSERT INTO `jiecheng_area` VALUES ('2688', '500100', '500113', '巴南区');
INSERT INTO `jiecheng_area` VALUES ('2689', '500100', '500114', '黔江区');
INSERT INTO `jiecheng_area` VALUES ('2690', '500100', '500115', '长寿区');
INSERT INTO `jiecheng_area` VALUES ('2691', '500100', '500116', '江津区');
INSERT INTO `jiecheng_area` VALUES ('2692', '500100', '500117', '合川区');
INSERT INTO `jiecheng_area` VALUES ('2693', '500100', '500118', '永川区');
INSERT INTO `jiecheng_area` VALUES ('2694', '500100', '500119', '南川区');
INSERT INTO `jiecheng_area` VALUES ('2695', '500100', '500120', '璧山区');
INSERT INTO `jiecheng_area` VALUES ('2696', '500100', '500151', '铜梁区');
INSERT INTO `jiecheng_area` VALUES ('2697', '500100', '500152', '潼南区');
INSERT INTO `jiecheng_area` VALUES ('2698', '500100', '500153', '荣昌区');
INSERT INTO `jiecheng_area` VALUES ('2699', '500100', '500154', '开州区');
INSERT INTO `jiecheng_area` VALUES ('2700', '500100', '500155', '梁平区');
INSERT INTO `jiecheng_area` VALUES ('2701', '500100', '500156', '武隆区');
INSERT INTO `jiecheng_area` VALUES ('2702', '500200', '500229', '城口县');
INSERT INTO `jiecheng_area` VALUES ('2703', '500200', '500230', '丰都县');
INSERT INTO `jiecheng_area` VALUES ('2704', '500200', '500231', '垫江县');
INSERT INTO `jiecheng_area` VALUES ('2705', '500200', '500233', '忠县');
INSERT INTO `jiecheng_area` VALUES ('2706', '500200', '500235', '云阳县');
INSERT INTO `jiecheng_area` VALUES ('2707', '500200', '500236', '奉节县');
INSERT INTO `jiecheng_area` VALUES ('2708', '500200', '500237', '巫山县');
INSERT INTO `jiecheng_area` VALUES ('2709', '500200', '500238', '巫溪县');
INSERT INTO `jiecheng_area` VALUES ('2710', '500200', '500240', '石柱土家族自治县');
INSERT INTO `jiecheng_area` VALUES ('2711', '500200', '500241', '秀山土家族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2712', '500200', '500242', '酉阳土家族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2713', '500200', '500243', '彭水苗族土家族自治县');
INSERT INTO `jiecheng_area` VALUES ('2714', '510000', '510100', '成都市');
INSERT INTO `jiecheng_area` VALUES ('2715', '510000', '510300', '自贡市');
INSERT INTO `jiecheng_area` VALUES ('2716', '510000', '510400', '攀枝花市');
INSERT INTO `jiecheng_area` VALUES ('2717', '510000', '510500', '泸州市');
INSERT INTO `jiecheng_area` VALUES ('2718', '510000', '510600', '德阳市');
INSERT INTO `jiecheng_area` VALUES ('2719', '510000', '510700', '绵阳市');
INSERT INTO `jiecheng_area` VALUES ('2720', '510000', '510800', '广元市');
INSERT INTO `jiecheng_area` VALUES ('2721', '510000', '510900', '遂宁市');
INSERT INTO `jiecheng_area` VALUES ('2722', '510000', '511000', '内江市');
INSERT INTO `jiecheng_area` VALUES ('2723', '510000', '511100', '乐山市');
INSERT INTO `jiecheng_area` VALUES ('2724', '510000', '511300', '南充市');
INSERT INTO `jiecheng_area` VALUES ('2725', '510000', '511400', '眉山市');
INSERT INTO `jiecheng_area` VALUES ('2726', '510000', '511500', '宜宾市');
INSERT INTO `jiecheng_area` VALUES ('2727', '510000', '511600', '广安市');
INSERT INTO `jiecheng_area` VALUES ('2728', '510000', '511700', '达州市');
INSERT INTO `jiecheng_area` VALUES ('2729', '510000', '511800', '雅安市');
INSERT INTO `jiecheng_area` VALUES ('2730', '510000', '511900', '巴中市');
INSERT INTO `jiecheng_area` VALUES ('2731', '510000', '512000', '资阳市');
INSERT INTO `jiecheng_area` VALUES ('2732', '510000', '513200', '阿坝藏族羌族自治州');
INSERT INTO `jiecheng_area` VALUES ('2733', '510000', '513300', '甘孜藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('2734', '510000', '513400', '凉山彝族自治州');
INSERT INTO `jiecheng_area` VALUES ('2735', '510100', '510101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2736', '510100', '510104', '锦江区');
INSERT INTO `jiecheng_area` VALUES ('2737', '510100', '510105', '青羊区');
INSERT INTO `jiecheng_area` VALUES ('2738', '510100', '510106', '金牛区');
INSERT INTO `jiecheng_area` VALUES ('2739', '510100', '510107', '武侯区');
INSERT INTO `jiecheng_area` VALUES ('2740', '510100', '510108', '成华区');
INSERT INTO `jiecheng_area` VALUES ('2741', '510100', '510112', '龙泉驿区');
INSERT INTO `jiecheng_area` VALUES ('2742', '510100', '510113', '青白江区');
INSERT INTO `jiecheng_area` VALUES ('2743', '510100', '510114', '新都区');
INSERT INTO `jiecheng_area` VALUES ('2744', '510100', '510115', '温江区');
INSERT INTO `jiecheng_area` VALUES ('2745', '510100', '510116', '双流区');
INSERT INTO `jiecheng_area` VALUES ('2746', '510100', '510117', '郫都区');
INSERT INTO `jiecheng_area` VALUES ('2747', '510100', '510121', '金堂县');
INSERT INTO `jiecheng_area` VALUES ('2748', '510100', '510129', '大邑县');
INSERT INTO `jiecheng_area` VALUES ('2749', '510100', '510131', '蒲江县');
INSERT INTO `jiecheng_area` VALUES ('2750', '510100', '510132', '新津县');
INSERT INTO `jiecheng_area` VALUES ('2751', '510100', '510181', '都江堰市');
INSERT INTO `jiecheng_area` VALUES ('2752', '510100', '510182', '彭州市');
INSERT INTO `jiecheng_area` VALUES ('2753', '510100', '510183', '邛崃市');
INSERT INTO `jiecheng_area` VALUES ('2754', '510100', '510184', '崇州市');
INSERT INTO `jiecheng_area` VALUES ('2755', '510100', '510185', '简阳市');
INSERT INTO `jiecheng_area` VALUES ('2756', '510300', '510301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2757', '510300', '510302', '自流井区');
INSERT INTO `jiecheng_area` VALUES ('2758', '510300', '510303', '贡井区');
INSERT INTO `jiecheng_area` VALUES ('2759', '510300', '510304', '大安区');
INSERT INTO `jiecheng_area` VALUES ('2760', '510300', '510311', '沿滩区');
INSERT INTO `jiecheng_area` VALUES ('2761', '510300', '510321', '荣县');
INSERT INTO `jiecheng_area` VALUES ('2762', '510300', '510322', '富顺县');
INSERT INTO `jiecheng_area` VALUES ('2763', '510400', '510401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2764', '510400', '510402', '东区');
INSERT INTO `jiecheng_area` VALUES ('2765', '510400', '510403', '西区');
INSERT INTO `jiecheng_area` VALUES ('2766', '510400', '510411', '仁和区');
INSERT INTO `jiecheng_area` VALUES ('2767', '510400', '510421', '米易县');
INSERT INTO `jiecheng_area` VALUES ('2768', '510400', '510422', '盐边县');
INSERT INTO `jiecheng_area` VALUES ('2769', '510500', '510501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2770', '510500', '510502', '江阳区');
INSERT INTO `jiecheng_area` VALUES ('2771', '510500', '510503', '纳溪区');
INSERT INTO `jiecheng_area` VALUES ('2772', '510500', '510504', '龙马潭区');
INSERT INTO `jiecheng_area` VALUES ('2773', '510500', '510521', '泸县');
INSERT INTO `jiecheng_area` VALUES ('2774', '510500', '510522', '合江县');
INSERT INTO `jiecheng_area` VALUES ('2775', '510500', '510524', '叙永县');
INSERT INTO `jiecheng_area` VALUES ('2776', '510500', '510525', '古蔺县');
INSERT INTO `jiecheng_area` VALUES ('2777', '510600', '510601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2778', '510600', '510603', '旌阳区');
INSERT INTO `jiecheng_area` VALUES ('2779', '510600', '510604', '罗江区');
INSERT INTO `jiecheng_area` VALUES ('2780', '510600', '510623', '中江县');
INSERT INTO `jiecheng_area` VALUES ('2781', '510600', '510681', '广汉市');
INSERT INTO `jiecheng_area` VALUES ('2782', '510600', '510682', '什邡市');
INSERT INTO `jiecheng_area` VALUES ('2783', '510600', '510683', '绵竹市');
INSERT INTO `jiecheng_area` VALUES ('2784', '510700', '510701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2785', '510700', '510703', '涪城区');
INSERT INTO `jiecheng_area` VALUES ('2786', '510700', '510704', '游仙区');
INSERT INTO `jiecheng_area` VALUES ('2787', '510700', '510705', '安州区');
INSERT INTO `jiecheng_area` VALUES ('2788', '510700', '510722', '三台县');
INSERT INTO `jiecheng_area` VALUES ('2789', '510700', '510723', '盐亭县');
INSERT INTO `jiecheng_area` VALUES ('2790', '510700', '510725', '梓潼县');
INSERT INTO `jiecheng_area` VALUES ('2791', '510700', '510726', '北川羌族自治县');
INSERT INTO `jiecheng_area` VALUES ('2792', '510700', '510727', '平武县');
INSERT INTO `jiecheng_area` VALUES ('2793', '510700', '510781', '江油市');
INSERT INTO `jiecheng_area` VALUES ('2794', '510800', '510801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2795', '510800', '510802', '利州区');
INSERT INTO `jiecheng_area` VALUES ('2796', '510800', '510811', '昭化区');
INSERT INTO `jiecheng_area` VALUES ('2797', '510800', '510812', '朝天区');
INSERT INTO `jiecheng_area` VALUES ('2798', '510800', '510821', '旺苍县');
INSERT INTO `jiecheng_area` VALUES ('2799', '510800', '510822', '青川县');
INSERT INTO `jiecheng_area` VALUES ('2800', '510800', '510823', '剑阁县');
INSERT INTO `jiecheng_area` VALUES ('2801', '510800', '510824', '苍溪县');
INSERT INTO `jiecheng_area` VALUES ('2802', '510900', '510901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2803', '510900', '510903', '船山区');
INSERT INTO `jiecheng_area` VALUES ('2804', '510900', '510904', '安居区');
INSERT INTO `jiecheng_area` VALUES ('2805', '510900', '510921', '蓬溪县');
INSERT INTO `jiecheng_area` VALUES ('2806', '510900', '510923', '大英县');
INSERT INTO `jiecheng_area` VALUES ('2807', '510900', '510981', '射洪市');
INSERT INTO `jiecheng_area` VALUES ('2808', '511000', '511001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2809', '511000', '511002', '市中区');
INSERT INTO `jiecheng_area` VALUES ('2810', '511000', '511011', '东兴区');
INSERT INTO `jiecheng_area` VALUES ('2811', '511000', '511024', '威远县');
INSERT INTO `jiecheng_area` VALUES ('2812', '511000', '511025', '资中县');
INSERT INTO `jiecheng_area` VALUES ('2813', '511000', '511071', '内江经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2814', '511000', '511083', '隆昌市');
INSERT INTO `jiecheng_area` VALUES ('2815', '511100', '511101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2816', '511100', '511102', '市中区');
INSERT INTO `jiecheng_area` VALUES ('2817', '511100', '511111', '沙湾区');
INSERT INTO `jiecheng_area` VALUES ('2818', '511100', '511112', '五通桥区');
INSERT INTO `jiecheng_area` VALUES ('2819', '511100', '511113', '金口河区');
INSERT INTO `jiecheng_area` VALUES ('2820', '511100', '511123', '犍为县');
INSERT INTO `jiecheng_area` VALUES ('2821', '511100', '511124', '井研县');
INSERT INTO `jiecheng_area` VALUES ('2822', '511100', '511126', '夹江县');
INSERT INTO `jiecheng_area` VALUES ('2823', '511100', '511129', '沐川县');
INSERT INTO `jiecheng_area` VALUES ('2824', '511100', '511132', '峨边彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('2825', '511100', '511133', '马边彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('2826', '511100', '511181', '峨眉山市');
INSERT INTO `jiecheng_area` VALUES ('2827', '511300', '511301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2828', '511300', '511302', '顺庆区');
INSERT INTO `jiecheng_area` VALUES ('2829', '511300', '511303', '高坪区');
INSERT INTO `jiecheng_area` VALUES ('2830', '511300', '511304', '嘉陵区');
INSERT INTO `jiecheng_area` VALUES ('2831', '511300', '511321', '南部县');
INSERT INTO `jiecheng_area` VALUES ('2832', '511300', '511322', '营山县');
INSERT INTO `jiecheng_area` VALUES ('2833', '511300', '511323', '蓬安县');
INSERT INTO `jiecheng_area` VALUES ('2834', '511300', '511324', '仪陇县');
INSERT INTO `jiecheng_area` VALUES ('2835', '511300', '511325', '西充县');
INSERT INTO `jiecheng_area` VALUES ('2836', '511300', '511381', '阆中市');
INSERT INTO `jiecheng_area` VALUES ('2837', '511400', '511401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2838', '511400', '511402', '东坡区');
INSERT INTO `jiecheng_area` VALUES ('2839', '511400', '511403', '彭山区');
INSERT INTO `jiecheng_area` VALUES ('2840', '511400', '511421', '仁寿县');
INSERT INTO `jiecheng_area` VALUES ('2841', '511400', '511423', '洪雅县');
INSERT INTO `jiecheng_area` VALUES ('2842', '511400', '511424', '丹棱县');
INSERT INTO `jiecheng_area` VALUES ('2843', '511400', '511425', '青神县');
INSERT INTO `jiecheng_area` VALUES ('2844', '511500', '511501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2845', '511500', '511502', '翠屏区');
INSERT INTO `jiecheng_area` VALUES ('2846', '511500', '511503', '南溪区');
INSERT INTO `jiecheng_area` VALUES ('2847', '511500', '511504', '叙州区');
INSERT INTO `jiecheng_area` VALUES ('2848', '511500', '511523', '江安县');
INSERT INTO `jiecheng_area` VALUES ('2849', '511500', '511524', '长宁县');
INSERT INTO `jiecheng_area` VALUES ('2850', '511500', '511525', '高县');
INSERT INTO `jiecheng_area` VALUES ('2851', '511500', '511526', '珙县');
INSERT INTO `jiecheng_area` VALUES ('2852', '511500', '511527', '筠连县');
INSERT INTO `jiecheng_area` VALUES ('2853', '511500', '511528', '兴文县');
INSERT INTO `jiecheng_area` VALUES ('2854', '511500', '511529', '屏山县');
INSERT INTO `jiecheng_area` VALUES ('2855', '511600', '511601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2856', '511600', '511602', '广安区');
INSERT INTO `jiecheng_area` VALUES ('2857', '511600', '511603', '前锋区');
INSERT INTO `jiecheng_area` VALUES ('2858', '511600', '511621', '岳池县');
INSERT INTO `jiecheng_area` VALUES ('2859', '511600', '511622', '武胜县');
INSERT INTO `jiecheng_area` VALUES ('2860', '511600', '511623', '邻水县');
INSERT INTO `jiecheng_area` VALUES ('2861', '511600', '511681', '华蓥市');
INSERT INTO `jiecheng_area` VALUES ('2862', '511700', '511701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2863', '511700', '511702', '通川区');
INSERT INTO `jiecheng_area` VALUES ('2864', '511700', '511703', '达川区');
INSERT INTO `jiecheng_area` VALUES ('2865', '511700', '511722', '宣汉县');
INSERT INTO `jiecheng_area` VALUES ('2866', '511700', '511723', '开江县');
INSERT INTO `jiecheng_area` VALUES ('2867', '511700', '511724', '大竹县');
INSERT INTO `jiecheng_area` VALUES ('2868', '511700', '511725', '渠县');
INSERT INTO `jiecheng_area` VALUES ('2869', '511700', '511771', '达州经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2870', '511700', '511781', '万源市');
INSERT INTO `jiecheng_area` VALUES ('2871', '511800', '511801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2872', '511800', '511802', '雨城区');
INSERT INTO `jiecheng_area` VALUES ('2873', '511800', '511803', '名山区');
INSERT INTO `jiecheng_area` VALUES ('2874', '511800', '511822', '荥经县');
INSERT INTO `jiecheng_area` VALUES ('2875', '511800', '511823', '汉源县');
INSERT INTO `jiecheng_area` VALUES ('2876', '511800', '511824', '石棉县');
INSERT INTO `jiecheng_area` VALUES ('2877', '511800', '511825', '天全县');
INSERT INTO `jiecheng_area` VALUES ('2878', '511800', '511826', '芦山县');
INSERT INTO `jiecheng_area` VALUES ('2879', '511800', '511827', '宝兴县');
INSERT INTO `jiecheng_area` VALUES ('2880', '511900', '511901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2881', '511900', '511902', '巴州区');
INSERT INTO `jiecheng_area` VALUES ('2882', '511900', '511903', '恩阳区');
INSERT INTO `jiecheng_area` VALUES ('2883', '511900', '511921', '通江县');
INSERT INTO `jiecheng_area` VALUES ('2884', '511900', '511922', '南江县');
INSERT INTO `jiecheng_area` VALUES ('2885', '511900', '511923', '平昌县');
INSERT INTO `jiecheng_area` VALUES ('2886', '511900', '511971', '巴中经济开发区');
INSERT INTO `jiecheng_area` VALUES ('2887', '512000', '512001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2888', '512000', '512002', '雁江区');
INSERT INTO `jiecheng_area` VALUES ('2889', '512000', '512021', '安岳县');
INSERT INTO `jiecheng_area` VALUES ('2890', '512000', '512022', '乐至县');
INSERT INTO `jiecheng_area` VALUES ('2891', '513200', '513201', '马尔康市');
INSERT INTO `jiecheng_area` VALUES ('2892', '513200', '513221', '汶川县');
INSERT INTO `jiecheng_area` VALUES ('2893', '513200', '513222', '理县');
INSERT INTO `jiecheng_area` VALUES ('2894', '513200', '513223', '茂县');
INSERT INTO `jiecheng_area` VALUES ('2895', '513200', '513224', '松潘县');
INSERT INTO `jiecheng_area` VALUES ('2896', '513200', '513225', '九寨沟县');
INSERT INTO `jiecheng_area` VALUES ('2897', '513200', '513226', '金川县');
INSERT INTO `jiecheng_area` VALUES ('2898', '513200', '513227', '小金县');
INSERT INTO `jiecheng_area` VALUES ('2899', '513200', '513228', '黑水县');
INSERT INTO `jiecheng_area` VALUES ('2900', '513200', '513230', '壤塘县');
INSERT INTO `jiecheng_area` VALUES ('2901', '513200', '513231', '阿坝县');
INSERT INTO `jiecheng_area` VALUES ('2902', '513200', '513232', '若尔盖县');
INSERT INTO `jiecheng_area` VALUES ('2903', '513200', '513233', '红原县');
INSERT INTO `jiecheng_area` VALUES ('2904', '513300', '513301', '康定市');
INSERT INTO `jiecheng_area` VALUES ('2905', '513300', '513322', '泸定县');
INSERT INTO `jiecheng_area` VALUES ('2906', '513300', '513323', '丹巴县');
INSERT INTO `jiecheng_area` VALUES ('2907', '513300', '513324', '九龙县');
INSERT INTO `jiecheng_area` VALUES ('2908', '513300', '513325', '雅江县');
INSERT INTO `jiecheng_area` VALUES ('2909', '513300', '513326', '道孚县');
INSERT INTO `jiecheng_area` VALUES ('2910', '513300', '513327', '炉霍县');
INSERT INTO `jiecheng_area` VALUES ('2911', '513300', '513328', '甘孜县');
INSERT INTO `jiecheng_area` VALUES ('2912', '513300', '513329', '新龙县');
INSERT INTO `jiecheng_area` VALUES ('2913', '513300', '513330', '德格县');
INSERT INTO `jiecheng_area` VALUES ('2914', '513300', '513331', '白玉县');
INSERT INTO `jiecheng_area` VALUES ('2915', '513300', '513332', '石渠县');
INSERT INTO `jiecheng_area` VALUES ('2916', '513300', '513333', '色达县');
INSERT INTO `jiecheng_area` VALUES ('2917', '513300', '513334', '理塘县');
INSERT INTO `jiecheng_area` VALUES ('2918', '513300', '513335', '巴塘县');
INSERT INTO `jiecheng_area` VALUES ('2919', '513300', '513336', '乡城县');
INSERT INTO `jiecheng_area` VALUES ('2920', '513300', '513337', '稻城县');
INSERT INTO `jiecheng_area` VALUES ('2921', '513300', '513338', '得荣县');
INSERT INTO `jiecheng_area` VALUES ('2922', '513400', '513401', '西昌市');
INSERT INTO `jiecheng_area` VALUES ('2923', '513400', '513422', '木里藏族自治县');
INSERT INTO `jiecheng_area` VALUES ('2924', '513400', '513423', '盐源县');
INSERT INTO `jiecheng_area` VALUES ('2925', '513400', '513424', '德昌县');
INSERT INTO `jiecheng_area` VALUES ('2926', '513400', '513425', '会理县');
INSERT INTO `jiecheng_area` VALUES ('2927', '513400', '513426', '会东县');
INSERT INTO `jiecheng_area` VALUES ('2928', '513400', '513427', '宁南县');
INSERT INTO `jiecheng_area` VALUES ('2929', '513400', '513428', '普格县');
INSERT INTO `jiecheng_area` VALUES ('2930', '513400', '513429', '布拖县');
INSERT INTO `jiecheng_area` VALUES ('2931', '513400', '513430', '金阳县');
INSERT INTO `jiecheng_area` VALUES ('2932', '513400', '513431', '昭觉县');
INSERT INTO `jiecheng_area` VALUES ('2933', '513400', '513432', '喜德县');
INSERT INTO `jiecheng_area` VALUES ('2934', '513400', '513433', '冕宁县');
INSERT INTO `jiecheng_area` VALUES ('2935', '513400', '513434', '越西县');
INSERT INTO `jiecheng_area` VALUES ('2936', '513400', '513435', '甘洛县');
INSERT INTO `jiecheng_area` VALUES ('2937', '513400', '513436', '美姑县');
INSERT INTO `jiecheng_area` VALUES ('2938', '513400', '513437', '雷波县');
INSERT INTO `jiecheng_area` VALUES ('2939', '520000', '520100', '贵阳市');
INSERT INTO `jiecheng_area` VALUES ('2940', '520000', '520200', '六盘水市');
INSERT INTO `jiecheng_area` VALUES ('2941', '520000', '520300', '遵义市');
INSERT INTO `jiecheng_area` VALUES ('2942', '520000', '520400', '安顺市');
INSERT INTO `jiecheng_area` VALUES ('2943', '520000', '520500', '毕节市');
INSERT INTO `jiecheng_area` VALUES ('2944', '520000', '520600', '铜仁市');
INSERT INTO `jiecheng_area` VALUES ('2945', '520000', '522300', '黔西南布依族苗族自治州');
INSERT INTO `jiecheng_area` VALUES ('2946', '520000', '522600', '黔东南苗族侗族自治州');
INSERT INTO `jiecheng_area` VALUES ('2947', '520000', '522700', '黔南布依族苗族自治州');
INSERT INTO `jiecheng_area` VALUES ('2948', '520100', '520101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2949', '520100', '520102', '南明区');
INSERT INTO `jiecheng_area` VALUES ('2950', '520100', '520103', '云岩区');
INSERT INTO `jiecheng_area` VALUES ('2951', '520100', '520111', '花溪区');
INSERT INTO `jiecheng_area` VALUES ('2952', '520100', '520112', '乌当区');
INSERT INTO `jiecheng_area` VALUES ('2953', '520100', '520113', '白云区');
INSERT INTO `jiecheng_area` VALUES ('2954', '520100', '520115', '观山湖区');
INSERT INTO `jiecheng_area` VALUES ('2955', '520100', '520121', '开阳县');
INSERT INTO `jiecheng_area` VALUES ('2956', '520100', '520122', '息烽县');
INSERT INTO `jiecheng_area` VALUES ('2957', '520100', '520123', '修文县');
INSERT INTO `jiecheng_area` VALUES ('2958', '520100', '520181', '清镇市');
INSERT INTO `jiecheng_area` VALUES ('2959', '520200', '520201', '钟山区');
INSERT INTO `jiecheng_area` VALUES ('2960', '520200', '520203', '六枝特区');
INSERT INTO `jiecheng_area` VALUES ('2961', '520200', '520221', '水城县');
INSERT INTO `jiecheng_area` VALUES ('2962', '520200', '520281', '盘州市');
INSERT INTO `jiecheng_area` VALUES ('2963', '520300', '520301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2964', '520300', '520302', '红花岗区');
INSERT INTO `jiecheng_area` VALUES ('2965', '520300', '520303', '汇川区');
INSERT INTO `jiecheng_area` VALUES ('2966', '520300', '520304', '播州区');
INSERT INTO `jiecheng_area` VALUES ('2967', '520300', '520322', '桐梓县');
INSERT INTO `jiecheng_area` VALUES ('2968', '520300', '520323', '绥阳县');
INSERT INTO `jiecheng_area` VALUES ('2969', '520300', '520324', '正安县');
INSERT INTO `jiecheng_area` VALUES ('2970', '520300', '520325', '道真仡佬族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2971', '520300', '520326', '务川仡佬族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2972', '520300', '520327', '凤冈县');
INSERT INTO `jiecheng_area` VALUES ('2973', '520300', '520328', '湄潭县');
INSERT INTO `jiecheng_area` VALUES ('2974', '520300', '520329', '余庆县');
INSERT INTO `jiecheng_area` VALUES ('2975', '520300', '520330', '习水县');
INSERT INTO `jiecheng_area` VALUES ('2976', '520300', '520381', '赤水市');
INSERT INTO `jiecheng_area` VALUES ('2977', '520300', '520382', '仁怀市');
INSERT INTO `jiecheng_area` VALUES ('2978', '520400', '520401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2979', '520400', '520402', '西秀区');
INSERT INTO `jiecheng_area` VALUES ('2980', '520400', '520403', '平坝区');
INSERT INTO `jiecheng_area` VALUES ('2981', '520400', '520422', '普定县');
INSERT INTO `jiecheng_area` VALUES ('2982', '520400', '520423', '镇宁布依族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2983', '520400', '520424', '关岭布依族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2984', '520400', '520425', '紫云苗族布依族自治县');
INSERT INTO `jiecheng_area` VALUES ('2985', '520500', '520501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2986', '520500', '520502', '七星关区');
INSERT INTO `jiecheng_area` VALUES ('2987', '520500', '520521', '大方县');
INSERT INTO `jiecheng_area` VALUES ('2988', '520500', '520522', '黔西县');
INSERT INTO `jiecheng_area` VALUES ('2989', '520500', '520523', '金沙县');
INSERT INTO `jiecheng_area` VALUES ('2990', '520500', '520524', '织金县');
INSERT INTO `jiecheng_area` VALUES ('2991', '520500', '520525', '纳雍县');
INSERT INTO `jiecheng_area` VALUES ('2992', '520500', '520526', '威宁彝族回族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2993', '520500', '520527', '赫章县');
INSERT INTO `jiecheng_area` VALUES ('2994', '520600', '520601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('2995', '520600', '520602', '碧江区');
INSERT INTO `jiecheng_area` VALUES ('2996', '520600', '520603', '万山区');
INSERT INTO `jiecheng_area` VALUES ('2997', '520600', '520621', '江口县');
INSERT INTO `jiecheng_area` VALUES ('2998', '520600', '520622', '玉屏侗族自治县');
INSERT INTO `jiecheng_area` VALUES ('2999', '520600', '520623', '石阡县');
INSERT INTO `jiecheng_area` VALUES ('3000', '520600', '520624', '思南县');
INSERT INTO `jiecheng_area` VALUES ('3001', '520600', '520625', '印江土家族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('3002', '520600', '520626', '德江县');
INSERT INTO `jiecheng_area` VALUES ('3003', '520600', '520627', '沿河土家族自治县');
INSERT INTO `jiecheng_area` VALUES ('3004', '520600', '520628', '松桃苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('3005', '522300', '522301', '兴义市');
INSERT INTO `jiecheng_area` VALUES ('3006', '522300', '522302', '兴仁市');
INSERT INTO `jiecheng_area` VALUES ('3007', '522300', '522323', '普安县');
INSERT INTO `jiecheng_area` VALUES ('3008', '522300', '522324', '晴隆县');
INSERT INTO `jiecheng_area` VALUES ('3009', '522300', '522325', '贞丰县');
INSERT INTO `jiecheng_area` VALUES ('3010', '522300', '522326', '望谟县');
INSERT INTO `jiecheng_area` VALUES ('3011', '522300', '522327', '册亨县');
INSERT INTO `jiecheng_area` VALUES ('3012', '522300', '522328', '安龙县');
INSERT INTO `jiecheng_area` VALUES ('3013', '522600', '522601', '凯里市');
INSERT INTO `jiecheng_area` VALUES ('3014', '522600', '522622', '黄平县');
INSERT INTO `jiecheng_area` VALUES ('3015', '522600', '522623', '施秉县');
INSERT INTO `jiecheng_area` VALUES ('3016', '522600', '522624', '三穗县');
INSERT INTO `jiecheng_area` VALUES ('3017', '522600', '522625', '镇远县');
INSERT INTO `jiecheng_area` VALUES ('3018', '522600', '522626', '岑巩县');
INSERT INTO `jiecheng_area` VALUES ('3019', '522600', '522627', '天柱县');
INSERT INTO `jiecheng_area` VALUES ('3020', '522600', '522628', '锦屏县');
INSERT INTO `jiecheng_area` VALUES ('3021', '522600', '522629', '剑河县');
INSERT INTO `jiecheng_area` VALUES ('3022', '522600', '522630', '台江县');
INSERT INTO `jiecheng_area` VALUES ('3023', '522600', '522631', '黎平县');
INSERT INTO `jiecheng_area` VALUES ('3024', '522600', '522632', '榕江县');
INSERT INTO `jiecheng_area` VALUES ('3025', '522600', '522633', '从江县');
INSERT INTO `jiecheng_area` VALUES ('3026', '522600', '522634', '雷山县');
INSERT INTO `jiecheng_area` VALUES ('3027', '522600', '522635', '麻江县');
INSERT INTO `jiecheng_area` VALUES ('3028', '522600', '522636', '丹寨县');
INSERT INTO `jiecheng_area` VALUES ('3029', '522700', '522701', '都匀市');
INSERT INTO `jiecheng_area` VALUES ('3030', '522700', '522702', '福泉市');
INSERT INTO `jiecheng_area` VALUES ('3031', '522700', '522722', '荔波县');
INSERT INTO `jiecheng_area` VALUES ('3032', '522700', '522723', '贵定县');
INSERT INTO `jiecheng_area` VALUES ('3033', '522700', '522725', '瓮安县');
INSERT INTO `jiecheng_area` VALUES ('3034', '522700', '522726', '独山县');
INSERT INTO `jiecheng_area` VALUES ('3035', '522700', '522727', '平塘县');
INSERT INTO `jiecheng_area` VALUES ('3036', '522700', '522728', '罗甸县');
INSERT INTO `jiecheng_area` VALUES ('3037', '522700', '522729', '长顺县');
INSERT INTO `jiecheng_area` VALUES ('3038', '522700', '522730', '龙里县');
INSERT INTO `jiecheng_area` VALUES ('3039', '522700', '522731', '惠水县');
INSERT INTO `jiecheng_area` VALUES ('3040', '522700', '522732', '三都水族自治县');
INSERT INTO `jiecheng_area` VALUES ('3041', '530000', '530100', '昆明市');
INSERT INTO `jiecheng_area` VALUES ('3042', '530000', '530300', '曲靖市');
INSERT INTO `jiecheng_area` VALUES ('3043', '530000', '530400', '玉溪市');
INSERT INTO `jiecheng_area` VALUES ('3044', '530000', '530500', '保山市');
INSERT INTO `jiecheng_area` VALUES ('3045', '530000', '530600', '昭通市');
INSERT INTO `jiecheng_area` VALUES ('3046', '530000', '530700', '丽江市');
INSERT INTO `jiecheng_area` VALUES ('3047', '530000', '530800', '普洱市');
INSERT INTO `jiecheng_area` VALUES ('3048', '530000', '530900', '临沧市');
INSERT INTO `jiecheng_area` VALUES ('3049', '530000', '532300', '楚雄彝族自治州');
INSERT INTO `jiecheng_area` VALUES ('3050', '530000', '532500', '红河哈尼族彝族自治州');
INSERT INTO `jiecheng_area` VALUES ('3051', '530000', '532600', '文山壮族苗族自治州');
INSERT INTO `jiecheng_area` VALUES ('3052', '530000', '532800', '西双版纳傣族自治州');
INSERT INTO `jiecheng_area` VALUES ('3053', '530000', '532900', '大理白族自治州');
INSERT INTO `jiecheng_area` VALUES ('3054', '530000', '533100', '德宏傣族景颇族自治州');
INSERT INTO `jiecheng_area` VALUES ('3055', '530000', '533300', '怒江傈僳族自治州');
INSERT INTO `jiecheng_area` VALUES ('3056', '530000', '533400', '迪庆藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3057', '530100', '530101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3058', '530100', '530102', '五华区');
INSERT INTO `jiecheng_area` VALUES ('3059', '530100', '530103', '盘龙区');
INSERT INTO `jiecheng_area` VALUES ('3060', '530100', '530111', '官渡区');
INSERT INTO `jiecheng_area` VALUES ('3061', '530100', '530112', '西山区');
INSERT INTO `jiecheng_area` VALUES ('3062', '530100', '530113', '东川区');
INSERT INTO `jiecheng_area` VALUES ('3063', '530100', '530114', '呈贡区');
INSERT INTO `jiecheng_area` VALUES ('3064', '530100', '530115', '晋宁区');
INSERT INTO `jiecheng_area` VALUES ('3065', '530100', '530124', '富民县');
INSERT INTO `jiecheng_area` VALUES ('3066', '530100', '530125', '宜良县');
INSERT INTO `jiecheng_area` VALUES ('3067', '530100', '530126', '石林彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3068', '530100', '530127', '嵩明县');
INSERT INTO `jiecheng_area` VALUES ('3069', '530100', '530128', '禄劝彝族苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('3070', '530100', '530129', '寻甸回族彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3071', '530100', '530181', '安宁市');
INSERT INTO `jiecheng_area` VALUES ('3072', '530300', '530301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3073', '530300', '530302', '麒麟区');
INSERT INTO `jiecheng_area` VALUES ('3074', '530300', '530303', '沾益区');
INSERT INTO `jiecheng_area` VALUES ('3075', '530300', '530304', '马龙区');
INSERT INTO `jiecheng_area` VALUES ('3076', '530300', '530322', '陆良县');
INSERT INTO `jiecheng_area` VALUES ('3077', '530300', '530323', '师宗县');
INSERT INTO `jiecheng_area` VALUES ('3078', '530300', '530324', '罗平县');
INSERT INTO `jiecheng_area` VALUES ('3079', '530300', '530325', '富源县');
INSERT INTO `jiecheng_area` VALUES ('3080', '530300', '530326', '会泽县');
INSERT INTO `jiecheng_area` VALUES ('3081', '530300', '530381', '宣威市');
INSERT INTO `jiecheng_area` VALUES ('3082', '530400', '530401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3083', '530400', '530402', '红塔区');
INSERT INTO `jiecheng_area` VALUES ('3084', '530400', '530403', '江川区');
INSERT INTO `jiecheng_area` VALUES ('3085', '530400', '530422', '澄江县');
INSERT INTO `jiecheng_area` VALUES ('3086', '530400', '530423', '通海县');
INSERT INTO `jiecheng_area` VALUES ('3087', '530400', '530424', '华宁县');
INSERT INTO `jiecheng_area` VALUES ('3088', '530400', '530425', '易门县');
INSERT INTO `jiecheng_area` VALUES ('3089', '530400', '530426', '峨山彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3090', '530400', '530427', '新平彝族傣族自治县');
INSERT INTO `jiecheng_area` VALUES ('3091', '530400', '530428', '元江哈尼族彝族傣族自治县');
INSERT INTO `jiecheng_area` VALUES ('3092', '530500', '530501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3093', '530500', '530502', '隆阳区');
INSERT INTO `jiecheng_area` VALUES ('3094', '530500', '530521', '施甸县');
INSERT INTO `jiecheng_area` VALUES ('3095', '530500', '530523', '龙陵县');
INSERT INTO `jiecheng_area` VALUES ('3096', '530500', '530524', '昌宁县');
INSERT INTO `jiecheng_area` VALUES ('3097', '530500', '530581', '腾冲市');
INSERT INTO `jiecheng_area` VALUES ('3098', '530600', '530601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3099', '530600', '530602', '昭阳区');
INSERT INTO `jiecheng_area` VALUES ('3100', '530600', '530621', '鲁甸县');
INSERT INTO `jiecheng_area` VALUES ('3101', '530600', '530622', '巧家县');
INSERT INTO `jiecheng_area` VALUES ('3102', '530600', '530623', '盐津县');
INSERT INTO `jiecheng_area` VALUES ('3103', '530600', '530624', '大关县');
INSERT INTO `jiecheng_area` VALUES ('3104', '530600', '530625', '永善县');
INSERT INTO `jiecheng_area` VALUES ('3105', '530600', '530626', '绥江县');
INSERT INTO `jiecheng_area` VALUES ('3106', '530600', '530627', '镇雄县');
INSERT INTO `jiecheng_area` VALUES ('3107', '530600', '530628', '彝良县');
INSERT INTO `jiecheng_area` VALUES ('3108', '530600', '530629', '威信县');
INSERT INTO `jiecheng_area` VALUES ('3109', '530600', '530681', '水富市');
INSERT INTO `jiecheng_area` VALUES ('3110', '530700', '530701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3111', '530700', '530702', '古城区');
INSERT INTO `jiecheng_area` VALUES ('3112', '530700', '530721', '玉龙纳西族自治县');
INSERT INTO `jiecheng_area` VALUES ('3113', '530700', '530722', '永胜县');
INSERT INTO `jiecheng_area` VALUES ('3114', '530700', '530723', '华坪县');
INSERT INTO `jiecheng_area` VALUES ('3115', '530700', '530724', '宁蒗彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3116', '530800', '530801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3117', '530800', '530802', '思茅区');
INSERT INTO `jiecheng_area` VALUES ('3118', '530800', '530821', '宁洱哈尼族彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3119', '530800', '530822', '墨江哈尼族自治县');
INSERT INTO `jiecheng_area` VALUES ('3120', '530800', '530823', '景东彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3121', '530800', '530824', '景谷傣族彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3122', '530800', '530825', '镇沅彝族哈尼族拉祜族自治县');
INSERT INTO `jiecheng_area` VALUES ('3123', '530800', '530826', '江城哈尼族彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3124', '530800', '530827', '孟连傣族拉祜族佤族自治县');
INSERT INTO `jiecheng_area` VALUES ('3125', '530800', '530828', '澜沧拉祜族自治县');
INSERT INTO `jiecheng_area` VALUES ('3126', '530800', '530829', '西盟佤族自治县');
INSERT INTO `jiecheng_area` VALUES ('3127', '530900', '530901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3128', '530900', '530902', '临翔区');
INSERT INTO `jiecheng_area` VALUES ('3129', '530900', '530921', '凤庆县');
INSERT INTO `jiecheng_area` VALUES ('3130', '530900', '530922', '云县');
INSERT INTO `jiecheng_area` VALUES ('3131', '530900', '530923', '永德县');
INSERT INTO `jiecheng_area` VALUES ('3132', '530900', '530924', '镇康县');
INSERT INTO `jiecheng_area` VALUES ('3133', '530900', '530925', '双江拉祜族佤族布朗族傣族自治县');
INSERT INTO `jiecheng_area` VALUES ('3134', '530900', '530926', '耿马傣族佤族自治县');
INSERT INTO `jiecheng_area` VALUES ('3135', '530900', '530927', '沧源佤族自治县');
INSERT INTO `jiecheng_area` VALUES ('3136', '532300', '532301', '楚雄市');
INSERT INTO `jiecheng_area` VALUES ('3137', '532300', '532322', '双柏县');
INSERT INTO `jiecheng_area` VALUES ('3138', '532300', '532323', '牟定县');
INSERT INTO `jiecheng_area` VALUES ('3139', '532300', '532324', '南华县');
INSERT INTO `jiecheng_area` VALUES ('3140', '532300', '532325', '姚安县');
INSERT INTO `jiecheng_area` VALUES ('3141', '532300', '532326', '大姚县');
INSERT INTO `jiecheng_area` VALUES ('3142', '532300', '532327', '永仁县');
INSERT INTO `jiecheng_area` VALUES ('3143', '532300', '532328', '元谋县');
INSERT INTO `jiecheng_area` VALUES ('3144', '532300', '532329', '武定县');
INSERT INTO `jiecheng_area` VALUES ('3145', '532300', '532331', '禄丰县');
INSERT INTO `jiecheng_area` VALUES ('3146', '532500', '532501', '个旧市');
INSERT INTO `jiecheng_area` VALUES ('3147', '532500', '532502', '开远市');
INSERT INTO `jiecheng_area` VALUES ('3148', '532500', '532503', '蒙自市');
INSERT INTO `jiecheng_area` VALUES ('3149', '532500', '532504', '弥勒市');
INSERT INTO `jiecheng_area` VALUES ('3150', '532500', '532523', '屏边苗族自治县');
INSERT INTO `jiecheng_area` VALUES ('3151', '532500', '532524', '建水县');
INSERT INTO `jiecheng_area` VALUES ('3152', '532500', '532525', '石屏县');
INSERT INTO `jiecheng_area` VALUES ('3153', '532500', '532527', '泸西县');
INSERT INTO `jiecheng_area` VALUES ('3154', '532500', '532528', '元阳县');
INSERT INTO `jiecheng_area` VALUES ('3155', '532500', '532529', '红河县');
INSERT INTO `jiecheng_area` VALUES ('3156', '532500', '532530', '金平苗族瑶族傣族自治县');
INSERT INTO `jiecheng_area` VALUES ('3157', '532500', '532531', '绿春县');
INSERT INTO `jiecheng_area` VALUES ('3158', '532500', '532532', '河口瑶族自治县');
INSERT INTO `jiecheng_area` VALUES ('3159', '532600', '532601', '文山市');
INSERT INTO `jiecheng_area` VALUES ('3160', '532600', '532622', '砚山县');
INSERT INTO `jiecheng_area` VALUES ('3161', '532600', '532623', '西畴县');
INSERT INTO `jiecheng_area` VALUES ('3162', '532600', '532624', '麻栗坡县');
INSERT INTO `jiecheng_area` VALUES ('3163', '532600', '532625', '马关县');
INSERT INTO `jiecheng_area` VALUES ('3164', '532600', '532626', '丘北县');
INSERT INTO `jiecheng_area` VALUES ('3165', '532600', '532627', '广南县');
INSERT INTO `jiecheng_area` VALUES ('3166', '532600', '532628', '富宁县');
INSERT INTO `jiecheng_area` VALUES ('3167', '532800', '532801', '景洪市');
INSERT INTO `jiecheng_area` VALUES ('3168', '532800', '532822', '勐海县');
INSERT INTO `jiecheng_area` VALUES ('3169', '532800', '532823', '勐腊县');
INSERT INTO `jiecheng_area` VALUES ('3170', '532900', '532901', '大理市');
INSERT INTO `jiecheng_area` VALUES ('3171', '532900', '532922', '漾濞彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3172', '532900', '532923', '祥云县');
INSERT INTO `jiecheng_area` VALUES ('3173', '532900', '532924', '宾川县');
INSERT INTO `jiecheng_area` VALUES ('3174', '532900', '532925', '弥渡县');
INSERT INTO `jiecheng_area` VALUES ('3175', '532900', '532926', '南涧彝族自治县');
INSERT INTO `jiecheng_area` VALUES ('3176', '532900', '532927', '巍山彝族回族自治县');
INSERT INTO `jiecheng_area` VALUES ('3177', '532900', '532928', '永平县');
INSERT INTO `jiecheng_area` VALUES ('3178', '532900', '532929', '云龙县');
INSERT INTO `jiecheng_area` VALUES ('3179', '532900', '532930', '洱源县');
INSERT INTO `jiecheng_area` VALUES ('3180', '532900', '532931', '剑川县');
INSERT INTO `jiecheng_area` VALUES ('3181', '532900', '532932', '鹤庆县');
INSERT INTO `jiecheng_area` VALUES ('3182', '533100', '533102', '瑞丽市');
INSERT INTO `jiecheng_area` VALUES ('3183', '533100', '533103', '芒市');
INSERT INTO `jiecheng_area` VALUES ('3184', '533100', '533122', '梁河县');
INSERT INTO `jiecheng_area` VALUES ('3185', '533100', '533123', '盈江县');
INSERT INTO `jiecheng_area` VALUES ('3186', '533100', '533124', '陇川县');
INSERT INTO `jiecheng_area` VALUES ('3187', '533300', '533301', '泸水市');
INSERT INTO `jiecheng_area` VALUES ('3188', '533300', '533323', '福贡县');
INSERT INTO `jiecheng_area` VALUES ('3189', '533300', '533324', '贡山独龙族怒族自治县');
INSERT INTO `jiecheng_area` VALUES ('3190', '533300', '533325', '兰坪白族普米族自治县');
INSERT INTO `jiecheng_area` VALUES ('3191', '533400', '533401', '香格里拉市');
INSERT INTO `jiecheng_area` VALUES ('3192', '533400', '533422', '德钦县');
INSERT INTO `jiecheng_area` VALUES ('3193', '533400', '533423', '维西傈僳族自治县');
INSERT INTO `jiecheng_area` VALUES ('3194', '540000', '540100', '拉萨市');
INSERT INTO `jiecheng_area` VALUES ('3195', '540000', '540200', '日喀则市');
INSERT INTO `jiecheng_area` VALUES ('3196', '540000', '540300', '昌都市');
INSERT INTO `jiecheng_area` VALUES ('3197', '540000', '540400', '林芝市');
INSERT INTO `jiecheng_area` VALUES ('3198', '540000', '540500', '山南市');
INSERT INTO `jiecheng_area` VALUES ('3199', '540000', '540600', '那曲市');
INSERT INTO `jiecheng_area` VALUES ('3200', '540000', '542500', '阿里地区');
INSERT INTO `jiecheng_area` VALUES ('3201', '540100', '540101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3202', '540100', '540102', '城关区');
INSERT INTO `jiecheng_area` VALUES ('3203', '540100', '540103', '堆龙德庆区');
INSERT INTO `jiecheng_area` VALUES ('3204', '540100', '540104', '达孜区');
INSERT INTO `jiecheng_area` VALUES ('3205', '540100', '540121', '林周县');
INSERT INTO `jiecheng_area` VALUES ('3206', '540100', '540122', '当雄县');
INSERT INTO `jiecheng_area` VALUES ('3207', '540100', '540123', '尼木县');
INSERT INTO `jiecheng_area` VALUES ('3208', '540100', '540124', '曲水县');
INSERT INTO `jiecheng_area` VALUES ('3209', '540100', '540127', '墨竹工卡县');
INSERT INTO `jiecheng_area` VALUES ('3210', '540100', '540171', '格尔木藏青工业园区');
INSERT INTO `jiecheng_area` VALUES ('3211', '540100', '540172', '拉萨经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('3212', '540100', '540173', '西藏文化旅游创意园区');
INSERT INTO `jiecheng_area` VALUES ('3213', '540100', '540174', '达孜工业园区');
INSERT INTO `jiecheng_area` VALUES ('3214', '540200', '540202', '桑珠孜区');
INSERT INTO `jiecheng_area` VALUES ('3215', '540200', '540221', '南木林县');
INSERT INTO `jiecheng_area` VALUES ('3216', '540200', '540222', '江孜县');
INSERT INTO `jiecheng_area` VALUES ('3217', '540200', '540223', '定日县');
INSERT INTO `jiecheng_area` VALUES ('3218', '540200', '540224', '萨迦县');
INSERT INTO `jiecheng_area` VALUES ('3219', '540200', '540225', '拉孜县');
INSERT INTO `jiecheng_area` VALUES ('3220', '540200', '540226', '昂仁县');
INSERT INTO `jiecheng_area` VALUES ('3221', '540200', '540227', '谢通门县');
INSERT INTO `jiecheng_area` VALUES ('3222', '540200', '540228', '白朗县');
INSERT INTO `jiecheng_area` VALUES ('3223', '540200', '540229', '仁布县');
INSERT INTO `jiecheng_area` VALUES ('3224', '540200', '540230', '康马县');
INSERT INTO `jiecheng_area` VALUES ('3225', '540200', '540231', '定结县');
INSERT INTO `jiecheng_area` VALUES ('3226', '540200', '540232', '仲巴县');
INSERT INTO `jiecheng_area` VALUES ('3227', '540200', '540233', '亚东县');
INSERT INTO `jiecheng_area` VALUES ('3228', '540200', '540234', '吉隆县');
INSERT INTO `jiecheng_area` VALUES ('3229', '540200', '540235', '聂拉木县');
INSERT INTO `jiecheng_area` VALUES ('3230', '540200', '540236', '萨嘎县');
INSERT INTO `jiecheng_area` VALUES ('3231', '540200', '540237', '岗巴县');
INSERT INTO `jiecheng_area` VALUES ('3232', '540300', '540302', '卡若区');
INSERT INTO `jiecheng_area` VALUES ('3233', '540300', '540321', '江达县');
INSERT INTO `jiecheng_area` VALUES ('3234', '540300', '540322', '贡觉县');
INSERT INTO `jiecheng_area` VALUES ('3235', '540300', '540323', '类乌齐县');
INSERT INTO `jiecheng_area` VALUES ('3236', '540300', '540324', '丁青县');
INSERT INTO `jiecheng_area` VALUES ('3237', '540300', '540325', '察雅县');
INSERT INTO `jiecheng_area` VALUES ('3238', '540300', '540326', '八宿县');
INSERT INTO `jiecheng_area` VALUES ('3239', '540300', '540327', '左贡县');
INSERT INTO `jiecheng_area` VALUES ('3240', '540300', '540328', '芒康县');
INSERT INTO `jiecheng_area` VALUES ('3241', '540300', '540329', '洛隆县');
INSERT INTO `jiecheng_area` VALUES ('3242', '540300', '540330', '边坝县');
INSERT INTO `jiecheng_area` VALUES ('3243', '540400', '540402', '巴宜区');
INSERT INTO `jiecheng_area` VALUES ('3244', '540400', '540421', '工布江达县');
INSERT INTO `jiecheng_area` VALUES ('3245', '540400', '540422', '米林县');
INSERT INTO `jiecheng_area` VALUES ('3246', '540400', '540423', '墨脱县');
INSERT INTO `jiecheng_area` VALUES ('3247', '540400', '540424', '波密县');
INSERT INTO `jiecheng_area` VALUES ('3248', '540400', '540425', '察隅县');
INSERT INTO `jiecheng_area` VALUES ('3249', '540400', '540426', '朗县');
INSERT INTO `jiecheng_area` VALUES ('3250', '540500', '540501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3251', '540500', '540502', '乃东区');
INSERT INTO `jiecheng_area` VALUES ('3252', '540500', '540521', '扎囊县');
INSERT INTO `jiecheng_area` VALUES ('3253', '540500', '540522', '贡嘎县');
INSERT INTO `jiecheng_area` VALUES ('3254', '540500', '540523', '桑日县');
INSERT INTO `jiecheng_area` VALUES ('3255', '540500', '540524', '琼结县');
INSERT INTO `jiecheng_area` VALUES ('3256', '540500', '540525', '曲松县');
INSERT INTO `jiecheng_area` VALUES ('3257', '540500', '540526', '措美县');
INSERT INTO `jiecheng_area` VALUES ('3258', '540500', '540527', '洛扎县');
INSERT INTO `jiecheng_area` VALUES ('3259', '540500', '540528', '加查县');
INSERT INTO `jiecheng_area` VALUES ('3260', '540500', '540529', '隆子县');
INSERT INTO `jiecheng_area` VALUES ('3261', '540500', '540530', '错那县');
INSERT INTO `jiecheng_area` VALUES ('3262', '540500', '540531', '浪卡子县');
INSERT INTO `jiecheng_area` VALUES ('3263', '540600', '540602', '色尼区');
INSERT INTO `jiecheng_area` VALUES ('3264', '540600', '540621', '嘉黎县');
INSERT INTO `jiecheng_area` VALUES ('3265', '540600', '540622', '比如县');
INSERT INTO `jiecheng_area` VALUES ('3266', '540600', '540623', '聂荣县');
INSERT INTO `jiecheng_area` VALUES ('3267', '540600', '540624', '安多县');
INSERT INTO `jiecheng_area` VALUES ('3268', '540600', '540625', '申扎县');
INSERT INTO `jiecheng_area` VALUES ('3269', '540600', '540626', '索县');
INSERT INTO `jiecheng_area` VALUES ('3270', '540600', '540627', '班戈县');
INSERT INTO `jiecheng_area` VALUES ('3271', '540600', '540628', '巴青县');
INSERT INTO `jiecheng_area` VALUES ('3272', '540600', '540629', '尼玛县');
INSERT INTO `jiecheng_area` VALUES ('3273', '540600', '540630', '双湖县');
INSERT INTO `jiecheng_area` VALUES ('3274', '542500', '542521', '普兰县');
INSERT INTO `jiecheng_area` VALUES ('3275', '542500', '542522', '札达县');
INSERT INTO `jiecheng_area` VALUES ('3276', '542500', '542523', '噶尔县');
INSERT INTO `jiecheng_area` VALUES ('3277', '542500', '542524', '日土县');
INSERT INTO `jiecheng_area` VALUES ('3278', '542500', '542525', '革吉县');
INSERT INTO `jiecheng_area` VALUES ('3279', '542500', '542526', '改则县');
INSERT INTO `jiecheng_area` VALUES ('3280', '542500', '542527', '措勤县');
INSERT INTO `jiecheng_area` VALUES ('3281', '610000', '610100', '西安市');
INSERT INTO `jiecheng_area` VALUES ('3282', '610000', '610200', '铜川市');
INSERT INTO `jiecheng_area` VALUES ('3283', '610000', '610300', '宝鸡市');
INSERT INTO `jiecheng_area` VALUES ('3284', '610000', '610400', '咸阳市');
INSERT INTO `jiecheng_area` VALUES ('3285', '610000', '610500', '渭南市');
INSERT INTO `jiecheng_area` VALUES ('3286', '610000', '610600', '延安市');
INSERT INTO `jiecheng_area` VALUES ('3287', '610000', '610700', '汉中市');
INSERT INTO `jiecheng_area` VALUES ('3288', '610000', '610800', '榆林市');
INSERT INTO `jiecheng_area` VALUES ('3289', '610000', '610900', '安康市');
INSERT INTO `jiecheng_area` VALUES ('3290', '610000', '611000', '商洛市');
INSERT INTO `jiecheng_area` VALUES ('3291', '610100', '610101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3292', '610100', '610102', '新城区');
INSERT INTO `jiecheng_area` VALUES ('3293', '610100', '610103', '碑林区');
INSERT INTO `jiecheng_area` VALUES ('3294', '610100', '610104', '莲湖区');
INSERT INTO `jiecheng_area` VALUES ('3295', '610100', '610111', '灞桥区');
INSERT INTO `jiecheng_area` VALUES ('3296', '610100', '610112', '未央区');
INSERT INTO `jiecheng_area` VALUES ('3297', '610100', '610113', '雁塔区');
INSERT INTO `jiecheng_area` VALUES ('3298', '610100', '610114', '阎良区');
INSERT INTO `jiecheng_area` VALUES ('3299', '610100', '610115', '临潼区');
INSERT INTO `jiecheng_area` VALUES ('3300', '610100', '610116', '长安区');
INSERT INTO `jiecheng_area` VALUES ('3301', '610100', '610117', '高陵区');
INSERT INTO `jiecheng_area` VALUES ('3302', '610100', '610118', '鄠邑区');
INSERT INTO `jiecheng_area` VALUES ('3303', '610100', '610122', '蓝田县');
INSERT INTO `jiecheng_area` VALUES ('3304', '610100', '610124', '周至县');
INSERT INTO `jiecheng_area` VALUES ('3305', '610200', '610201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3306', '610200', '610202', '王益区');
INSERT INTO `jiecheng_area` VALUES ('3307', '610200', '610203', '印台区');
INSERT INTO `jiecheng_area` VALUES ('3308', '610200', '610204', '耀州区');
INSERT INTO `jiecheng_area` VALUES ('3309', '610200', '610222', '宜君县');
INSERT INTO `jiecheng_area` VALUES ('3310', '610300', '610301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3311', '610300', '610302', '渭滨区');
INSERT INTO `jiecheng_area` VALUES ('3312', '610300', '610303', '金台区');
INSERT INTO `jiecheng_area` VALUES ('3313', '610300', '610304', '陈仓区');
INSERT INTO `jiecheng_area` VALUES ('3314', '610300', '610322', '凤翔县');
INSERT INTO `jiecheng_area` VALUES ('3315', '610300', '610323', '岐山县');
INSERT INTO `jiecheng_area` VALUES ('3316', '610300', '610324', '扶风县');
INSERT INTO `jiecheng_area` VALUES ('3317', '610300', '610326', '眉县');
INSERT INTO `jiecheng_area` VALUES ('3318', '610300', '610327', '陇县');
INSERT INTO `jiecheng_area` VALUES ('3319', '610300', '610328', '千阳县');
INSERT INTO `jiecheng_area` VALUES ('3320', '610300', '610329', '麟游县');
INSERT INTO `jiecheng_area` VALUES ('3321', '610300', '610330', '凤县');
INSERT INTO `jiecheng_area` VALUES ('3322', '610300', '610331', '太白县');
INSERT INTO `jiecheng_area` VALUES ('3323', '610400', '610401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3324', '610400', '610402', '秦都区');
INSERT INTO `jiecheng_area` VALUES ('3325', '610400', '610403', '杨陵区');
INSERT INTO `jiecheng_area` VALUES ('3326', '610400', '610404', '渭城区');
INSERT INTO `jiecheng_area` VALUES ('3327', '610400', '610422', '三原县');
INSERT INTO `jiecheng_area` VALUES ('3328', '610400', '610423', '泾阳县');
INSERT INTO `jiecheng_area` VALUES ('3329', '610400', '610424', '乾县');
INSERT INTO `jiecheng_area` VALUES ('3330', '610400', '610425', '礼泉县');
INSERT INTO `jiecheng_area` VALUES ('3331', '610400', '610426', '永寿县');
INSERT INTO `jiecheng_area` VALUES ('3332', '610400', '610428', '长武县');
INSERT INTO `jiecheng_area` VALUES ('3333', '610400', '610429', '旬邑县');
INSERT INTO `jiecheng_area` VALUES ('3334', '610400', '610430', '淳化县');
INSERT INTO `jiecheng_area` VALUES ('3335', '610400', '610431', '武功县');
INSERT INTO `jiecheng_area` VALUES ('3336', '610400', '610481', '兴平市');
INSERT INTO `jiecheng_area` VALUES ('3337', '610400', '610482', '彬州市');
INSERT INTO `jiecheng_area` VALUES ('3338', '610500', '610501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3339', '610500', '610502', '临渭区');
INSERT INTO `jiecheng_area` VALUES ('3340', '610500', '610503', '华州区');
INSERT INTO `jiecheng_area` VALUES ('3341', '610500', '610522', '潼关县');
INSERT INTO `jiecheng_area` VALUES ('3342', '610500', '610523', '大荔县');
INSERT INTO `jiecheng_area` VALUES ('3343', '610500', '610524', '合阳县');
INSERT INTO `jiecheng_area` VALUES ('3344', '610500', '610525', '澄城县');
INSERT INTO `jiecheng_area` VALUES ('3345', '610500', '610526', '蒲城县');
INSERT INTO `jiecheng_area` VALUES ('3346', '610500', '610527', '白水县');
INSERT INTO `jiecheng_area` VALUES ('3347', '610500', '610528', '富平县');
INSERT INTO `jiecheng_area` VALUES ('3348', '610500', '610581', '韩城市');
INSERT INTO `jiecheng_area` VALUES ('3349', '610500', '610582', '华阴市');
INSERT INTO `jiecheng_area` VALUES ('3350', '610600', '610601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3351', '610600', '610602', '宝塔区');
INSERT INTO `jiecheng_area` VALUES ('3352', '610600', '610603', '安塞区');
INSERT INTO `jiecheng_area` VALUES ('3353', '610600', '610621', '延长县');
INSERT INTO `jiecheng_area` VALUES ('3354', '610600', '610622', '延川县');
INSERT INTO `jiecheng_area` VALUES ('3355', '610600', '610625', '志丹县');
INSERT INTO `jiecheng_area` VALUES ('3356', '610600', '610626', '吴起县');
INSERT INTO `jiecheng_area` VALUES ('3357', '610600', '610627', '甘泉县');
INSERT INTO `jiecheng_area` VALUES ('3358', '610600', '610628', '富县');
INSERT INTO `jiecheng_area` VALUES ('3359', '610600', '610629', '洛川县');
INSERT INTO `jiecheng_area` VALUES ('3360', '610600', '610630', '宜川县');
INSERT INTO `jiecheng_area` VALUES ('3361', '610600', '610631', '黄龙县');
INSERT INTO `jiecheng_area` VALUES ('3362', '610600', '610632', '黄陵县');
INSERT INTO `jiecheng_area` VALUES ('3363', '610600', '610681', '子长市');
INSERT INTO `jiecheng_area` VALUES ('3364', '610700', '610701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3365', '610700', '610702', '汉台区');
INSERT INTO `jiecheng_area` VALUES ('3366', '610700', '610703', '南郑区');
INSERT INTO `jiecheng_area` VALUES ('3367', '610700', '610722', '城固县');
INSERT INTO `jiecheng_area` VALUES ('3368', '610700', '610723', '洋县');
INSERT INTO `jiecheng_area` VALUES ('3369', '610700', '610724', '西乡县');
INSERT INTO `jiecheng_area` VALUES ('3370', '610700', '610725', '勉县');
INSERT INTO `jiecheng_area` VALUES ('3371', '610700', '610726', '宁强县');
INSERT INTO `jiecheng_area` VALUES ('3372', '610700', '610727', '略阳县');
INSERT INTO `jiecheng_area` VALUES ('3373', '610700', '610728', '镇巴县');
INSERT INTO `jiecheng_area` VALUES ('3374', '610700', '610729', '留坝县');
INSERT INTO `jiecheng_area` VALUES ('3375', '610700', '610730', '佛坪县');
INSERT INTO `jiecheng_area` VALUES ('3376', '610800', '610801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3377', '610800', '610802', '榆阳区');
INSERT INTO `jiecheng_area` VALUES ('3378', '610800', '610803', '横山区');
INSERT INTO `jiecheng_area` VALUES ('3379', '610800', '610822', '府谷县');
INSERT INTO `jiecheng_area` VALUES ('3380', '610800', '610824', '靖边县');
INSERT INTO `jiecheng_area` VALUES ('3381', '610800', '610825', '定边县');
INSERT INTO `jiecheng_area` VALUES ('3382', '610800', '610826', '绥德县');
INSERT INTO `jiecheng_area` VALUES ('3383', '610800', '610827', '米脂县');
INSERT INTO `jiecheng_area` VALUES ('3384', '610800', '610828', '佳县');
INSERT INTO `jiecheng_area` VALUES ('3385', '610800', '610829', '吴堡县');
INSERT INTO `jiecheng_area` VALUES ('3386', '610800', '610830', '清涧县');
INSERT INTO `jiecheng_area` VALUES ('3387', '610800', '610831', '子洲县');
INSERT INTO `jiecheng_area` VALUES ('3388', '610800', '610881', '神木市');
INSERT INTO `jiecheng_area` VALUES ('3389', '610900', '610901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3390', '610900', '610902', '汉滨区');
INSERT INTO `jiecheng_area` VALUES ('3391', '610900', '610921', '汉阴县');
INSERT INTO `jiecheng_area` VALUES ('3392', '610900', '610922', '石泉县');
INSERT INTO `jiecheng_area` VALUES ('3393', '610900', '610923', '宁陕县');
INSERT INTO `jiecheng_area` VALUES ('3394', '610900', '610924', '紫阳县');
INSERT INTO `jiecheng_area` VALUES ('3395', '610900', '610925', '岚皋县');
INSERT INTO `jiecheng_area` VALUES ('3396', '610900', '610926', '平利县');
INSERT INTO `jiecheng_area` VALUES ('3397', '610900', '610927', '镇坪县');
INSERT INTO `jiecheng_area` VALUES ('3398', '610900', '610928', '旬阳县');
INSERT INTO `jiecheng_area` VALUES ('3399', '610900', '610929', '白河县');
INSERT INTO `jiecheng_area` VALUES ('3400', '611000', '611001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3401', '611000', '611002', '商州区');
INSERT INTO `jiecheng_area` VALUES ('3402', '611000', '611021', '洛南县');
INSERT INTO `jiecheng_area` VALUES ('3403', '611000', '611022', '丹凤县');
INSERT INTO `jiecheng_area` VALUES ('3404', '611000', '611023', '商南县');
INSERT INTO `jiecheng_area` VALUES ('3405', '611000', '611024', '山阳县');
INSERT INTO `jiecheng_area` VALUES ('3406', '611000', '611025', '镇安县');
INSERT INTO `jiecheng_area` VALUES ('3407', '611000', '611026', '柞水县');
INSERT INTO `jiecheng_area` VALUES ('3408', '620000', '620100', '兰州市');
INSERT INTO `jiecheng_area` VALUES ('3409', '620000', '620200', '嘉峪关市');
INSERT INTO `jiecheng_area` VALUES ('3410', '620000', '620300', '金昌市');
INSERT INTO `jiecheng_area` VALUES ('3411', '620000', '620400', '白银市');
INSERT INTO `jiecheng_area` VALUES ('3412', '620000', '620500', '天水市');
INSERT INTO `jiecheng_area` VALUES ('3413', '620000', '620600', '武威市');
INSERT INTO `jiecheng_area` VALUES ('3414', '620000', '620700', '张掖市');
INSERT INTO `jiecheng_area` VALUES ('3415', '620000', '620800', '平凉市');
INSERT INTO `jiecheng_area` VALUES ('3416', '620000', '620900', '酒泉市');
INSERT INTO `jiecheng_area` VALUES ('3417', '620000', '621000', '庆阳市');
INSERT INTO `jiecheng_area` VALUES ('3418', '620000', '621100', '定西市');
INSERT INTO `jiecheng_area` VALUES ('3419', '620000', '621200', '陇南市');
INSERT INTO `jiecheng_area` VALUES ('3420', '620000', '622900', '临夏回族自治州');
INSERT INTO `jiecheng_area` VALUES ('3421', '620000', '623000', '甘南藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3422', '620100', '620101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3423', '620100', '620102', '城关区');
INSERT INTO `jiecheng_area` VALUES ('3424', '620100', '620103', '七里河区');
INSERT INTO `jiecheng_area` VALUES ('3425', '620100', '620104', '西固区');
INSERT INTO `jiecheng_area` VALUES ('3426', '620100', '620105', '安宁区');
INSERT INTO `jiecheng_area` VALUES ('3427', '620100', '620111', '红古区');
INSERT INTO `jiecheng_area` VALUES ('3428', '620100', '620121', '永登县');
INSERT INTO `jiecheng_area` VALUES ('3429', '620100', '620122', '皋兰县');
INSERT INTO `jiecheng_area` VALUES ('3430', '620100', '620123', '榆中县');
INSERT INTO `jiecheng_area` VALUES ('3431', '620100', '620171', '兰州新区');
INSERT INTO `jiecheng_area` VALUES ('3432', '620200', '620201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3433', '620300', '620301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3434', '620300', '620302', '金川区');
INSERT INTO `jiecheng_area` VALUES ('3435', '620300', '620321', '永昌县');
INSERT INTO `jiecheng_area` VALUES ('3436', '620400', '620401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3437', '620400', '620402', '白银区');
INSERT INTO `jiecheng_area` VALUES ('3438', '620400', '620403', '平川区');
INSERT INTO `jiecheng_area` VALUES ('3439', '620400', '620421', '靖远县');
INSERT INTO `jiecheng_area` VALUES ('3440', '620400', '620422', '会宁县');
INSERT INTO `jiecheng_area` VALUES ('3441', '620400', '620423', '景泰县');
INSERT INTO `jiecheng_area` VALUES ('3442', '620500', '620501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3443', '620500', '620502', '秦州区');
INSERT INTO `jiecheng_area` VALUES ('3444', '620500', '620503', '麦积区');
INSERT INTO `jiecheng_area` VALUES ('3445', '620500', '620521', '清水县');
INSERT INTO `jiecheng_area` VALUES ('3446', '620500', '620522', '秦安县');
INSERT INTO `jiecheng_area` VALUES ('3447', '620500', '620523', '甘谷县');
INSERT INTO `jiecheng_area` VALUES ('3448', '620500', '620524', '武山县');
INSERT INTO `jiecheng_area` VALUES ('3449', '620500', '620525', '张家川回族自治县');
INSERT INTO `jiecheng_area` VALUES ('3450', '620600', '620601', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3451', '620600', '620602', '凉州区');
INSERT INTO `jiecheng_area` VALUES ('3452', '620600', '620621', '民勤县');
INSERT INTO `jiecheng_area` VALUES ('3453', '620600', '620622', '古浪县');
INSERT INTO `jiecheng_area` VALUES ('3454', '620600', '620623', '天祝藏族自治县');
INSERT INTO `jiecheng_area` VALUES ('3455', '620700', '620701', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3456', '620700', '620702', '甘州区');
INSERT INTO `jiecheng_area` VALUES ('3457', '620700', '620721', '肃南裕固族自治县');
INSERT INTO `jiecheng_area` VALUES ('3458', '620700', '620722', '民乐县');
INSERT INTO `jiecheng_area` VALUES ('3459', '620700', '620723', '临泽县');
INSERT INTO `jiecheng_area` VALUES ('3460', '620700', '620724', '高台县');
INSERT INTO `jiecheng_area` VALUES ('3461', '620700', '620725', '山丹县');
INSERT INTO `jiecheng_area` VALUES ('3462', '620800', '620801', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3463', '620800', '620802', '崆峒区');
INSERT INTO `jiecheng_area` VALUES ('3464', '620800', '620821', '泾川县');
INSERT INTO `jiecheng_area` VALUES ('3465', '620800', '620822', '灵台县');
INSERT INTO `jiecheng_area` VALUES ('3466', '620800', '620823', '崇信县');
INSERT INTO `jiecheng_area` VALUES ('3467', '620800', '620825', '庄浪县');
INSERT INTO `jiecheng_area` VALUES ('3468', '620800', '620826', '静宁县');
INSERT INTO `jiecheng_area` VALUES ('3469', '620800', '620881', '华亭市');
INSERT INTO `jiecheng_area` VALUES ('3470', '620900', '620901', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3471', '620900', '620902', '肃州区');
INSERT INTO `jiecheng_area` VALUES ('3472', '620900', '620921', '金塔县');
INSERT INTO `jiecheng_area` VALUES ('3473', '620900', '620922', '瓜州县');
INSERT INTO `jiecheng_area` VALUES ('3474', '620900', '620923', '肃北蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('3475', '620900', '620924', '阿克塞哈萨克族自治县');
INSERT INTO `jiecheng_area` VALUES ('3476', '620900', '620981', '玉门市');
INSERT INTO `jiecheng_area` VALUES ('3477', '620900', '620982', '敦煌市');
INSERT INTO `jiecheng_area` VALUES ('3478', '621000', '621001', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3479', '621000', '621002', '西峰区');
INSERT INTO `jiecheng_area` VALUES ('3480', '621000', '621021', '庆城县');
INSERT INTO `jiecheng_area` VALUES ('3481', '621000', '621022', '环县');
INSERT INTO `jiecheng_area` VALUES ('3482', '621000', '621023', '华池县');
INSERT INTO `jiecheng_area` VALUES ('3483', '621000', '621024', '合水县');
INSERT INTO `jiecheng_area` VALUES ('3484', '621000', '621025', '正宁县');
INSERT INTO `jiecheng_area` VALUES ('3485', '621000', '621026', '宁县');
INSERT INTO `jiecheng_area` VALUES ('3486', '621000', '621027', '镇原县');
INSERT INTO `jiecheng_area` VALUES ('3487', '621100', '621101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3488', '621100', '621102', '安定区');
INSERT INTO `jiecheng_area` VALUES ('3489', '621100', '621121', '通渭县');
INSERT INTO `jiecheng_area` VALUES ('3490', '621100', '621122', '陇西县');
INSERT INTO `jiecheng_area` VALUES ('3491', '621100', '621123', '渭源县');
INSERT INTO `jiecheng_area` VALUES ('3492', '621100', '621124', '临洮县');
INSERT INTO `jiecheng_area` VALUES ('3493', '621100', '621125', '漳县');
INSERT INTO `jiecheng_area` VALUES ('3494', '621100', '621126', '岷县');
INSERT INTO `jiecheng_area` VALUES ('3495', '621200', '621201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3496', '621200', '621202', '武都区');
INSERT INTO `jiecheng_area` VALUES ('3497', '621200', '621221', '成县');
INSERT INTO `jiecheng_area` VALUES ('3498', '621200', '621222', '文县');
INSERT INTO `jiecheng_area` VALUES ('3499', '621200', '621223', '宕昌县');
INSERT INTO `jiecheng_area` VALUES ('3500', '621200', '621224', '康县');
INSERT INTO `jiecheng_area` VALUES ('3501', '621200', '621225', '西和县');
INSERT INTO `jiecheng_area` VALUES ('3502', '621200', '621226', '礼县');
INSERT INTO `jiecheng_area` VALUES ('3503', '621200', '621227', '徽县');
INSERT INTO `jiecheng_area` VALUES ('3504', '621200', '621228', '两当县');
INSERT INTO `jiecheng_area` VALUES ('3505', '622900', '622901', '临夏市');
INSERT INTO `jiecheng_area` VALUES ('3506', '622900', '622921', '临夏县');
INSERT INTO `jiecheng_area` VALUES ('3507', '622900', '622922', '康乐县');
INSERT INTO `jiecheng_area` VALUES ('3508', '622900', '622923', '永靖县');
INSERT INTO `jiecheng_area` VALUES ('3509', '622900', '622924', '广河县');
INSERT INTO `jiecheng_area` VALUES ('3510', '622900', '622925', '和政县');
INSERT INTO `jiecheng_area` VALUES ('3511', '622900', '622926', '东乡族自治县');
INSERT INTO `jiecheng_area` VALUES ('3512', '622900', '622927', '积石山保安族东乡族撒拉族自治县');
INSERT INTO `jiecheng_area` VALUES ('3513', '623000', '623001', '合作市');
INSERT INTO `jiecheng_area` VALUES ('3514', '623000', '623021', '临潭县');
INSERT INTO `jiecheng_area` VALUES ('3515', '623000', '623022', '卓尼县');
INSERT INTO `jiecheng_area` VALUES ('3516', '623000', '623023', '舟曲县');
INSERT INTO `jiecheng_area` VALUES ('3517', '623000', '623024', '迭部县');
INSERT INTO `jiecheng_area` VALUES ('3518', '623000', '623025', '玛曲县');
INSERT INTO `jiecheng_area` VALUES ('3519', '623000', '623026', '碌曲县');
INSERT INTO `jiecheng_area` VALUES ('3520', '623000', '623027', '夏河县');
INSERT INTO `jiecheng_area` VALUES ('3521', '630000', '630100', '西宁市');
INSERT INTO `jiecheng_area` VALUES ('3522', '630000', '630200', '海东市');
INSERT INTO `jiecheng_area` VALUES ('3523', '630000', '632200', '海北藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3524', '630000', '632300', '黄南藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3525', '630000', '632500', '海南藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3526', '630000', '632600', '果洛藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3527', '630000', '632700', '玉树藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3528', '630000', '632800', '海西蒙古族藏族自治州');
INSERT INTO `jiecheng_area` VALUES ('3529', '630100', '630101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3530', '630100', '630102', '城东区');
INSERT INTO `jiecheng_area` VALUES ('3531', '630100', '630103', '城中区');
INSERT INTO `jiecheng_area` VALUES ('3532', '630100', '630104', '城西区');
INSERT INTO `jiecheng_area` VALUES ('3533', '630100', '630105', '城北区');
INSERT INTO `jiecheng_area` VALUES ('3534', '630100', '630121', '大通回族土族自治县');
INSERT INTO `jiecheng_area` VALUES ('3535', '630100', '630122', '湟中县');
INSERT INTO `jiecheng_area` VALUES ('3536', '630100', '630123', '湟源县');
INSERT INTO `jiecheng_area` VALUES ('3537', '630200', '630202', '乐都区');
INSERT INTO `jiecheng_area` VALUES ('3538', '630200', '630203', '平安区');
INSERT INTO `jiecheng_area` VALUES ('3539', '630200', '630222', '民和回族土族自治县');
INSERT INTO `jiecheng_area` VALUES ('3540', '630200', '630223', '互助土族自治县');
INSERT INTO `jiecheng_area` VALUES ('3541', '630200', '630224', '化隆回族自治县');
INSERT INTO `jiecheng_area` VALUES ('3542', '630200', '630225', '循化撒拉族自治县');
INSERT INTO `jiecheng_area` VALUES ('3543', '632200', '632221', '门源回族自治县');
INSERT INTO `jiecheng_area` VALUES ('3544', '632200', '632222', '祁连县');
INSERT INTO `jiecheng_area` VALUES ('3545', '632200', '632223', '海晏县');
INSERT INTO `jiecheng_area` VALUES ('3546', '632200', '632224', '刚察县');
INSERT INTO `jiecheng_area` VALUES ('3547', '632300', '632321', '同仁县');
INSERT INTO `jiecheng_area` VALUES ('3548', '632300', '632322', '尖扎县');
INSERT INTO `jiecheng_area` VALUES ('3549', '632300', '632323', '泽库县');
INSERT INTO `jiecheng_area` VALUES ('3550', '632300', '632324', '河南蒙古族自治县');
INSERT INTO `jiecheng_area` VALUES ('3551', '632500', '632521', '共和县');
INSERT INTO `jiecheng_area` VALUES ('3552', '632500', '632522', '同德县');
INSERT INTO `jiecheng_area` VALUES ('3553', '632500', '632523', '贵德县');
INSERT INTO `jiecheng_area` VALUES ('3554', '632500', '632524', '兴海县');
INSERT INTO `jiecheng_area` VALUES ('3555', '632500', '632525', '贵南县');
INSERT INTO `jiecheng_area` VALUES ('3556', '632600', '632621', '玛沁县');
INSERT INTO `jiecheng_area` VALUES ('3557', '632600', '632622', '班玛县');
INSERT INTO `jiecheng_area` VALUES ('3558', '632600', '632623', '甘德县');
INSERT INTO `jiecheng_area` VALUES ('3559', '632600', '632624', '达日县');
INSERT INTO `jiecheng_area` VALUES ('3560', '632600', '632625', '久治县');
INSERT INTO `jiecheng_area` VALUES ('3561', '632600', '632626', '玛多县');
INSERT INTO `jiecheng_area` VALUES ('3562', '632700', '632701', '玉树市');
INSERT INTO `jiecheng_area` VALUES ('3563', '632700', '632722', '杂多县');
INSERT INTO `jiecheng_area` VALUES ('3564', '632700', '632723', '称多县');
INSERT INTO `jiecheng_area` VALUES ('3565', '632700', '632724', '治多县');
INSERT INTO `jiecheng_area` VALUES ('3566', '632700', '632725', '囊谦县');
INSERT INTO `jiecheng_area` VALUES ('3567', '632700', '632726', '曲麻莱县');
INSERT INTO `jiecheng_area` VALUES ('3568', '632800', '632801', '格尔木市');
INSERT INTO `jiecheng_area` VALUES ('3569', '632800', '632802', '德令哈市');
INSERT INTO `jiecheng_area` VALUES ('3570', '632800', '632803', '茫崖市');
INSERT INTO `jiecheng_area` VALUES ('3571', '632800', '632821', '乌兰县');
INSERT INTO `jiecheng_area` VALUES ('3572', '632800', '632822', '都兰县');
INSERT INTO `jiecheng_area` VALUES ('3573', '632800', '632823', '天峻县');
INSERT INTO `jiecheng_area` VALUES ('3574', '632800', '632857', '大柴旦行政委员会');
INSERT INTO `jiecheng_area` VALUES ('3575', '640000', '640100', '银川市');
INSERT INTO `jiecheng_area` VALUES ('3576', '640000', '640200', '石嘴山市');
INSERT INTO `jiecheng_area` VALUES ('3577', '640000', '640300', '吴忠市');
INSERT INTO `jiecheng_area` VALUES ('3578', '640000', '640400', '固原市');
INSERT INTO `jiecheng_area` VALUES ('3579', '640000', '640500', '中卫市');
INSERT INTO `jiecheng_area` VALUES ('3580', '640100', '640101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3581', '640100', '640104', '兴庆区');
INSERT INTO `jiecheng_area` VALUES ('3582', '640100', '640105', '西夏区');
INSERT INTO `jiecheng_area` VALUES ('3583', '640100', '640106', '金凤区');
INSERT INTO `jiecheng_area` VALUES ('3584', '640100', '640121', '永宁县');
INSERT INTO `jiecheng_area` VALUES ('3585', '640100', '640122', '贺兰县');
INSERT INTO `jiecheng_area` VALUES ('3586', '640100', '640181', '灵武市');
INSERT INTO `jiecheng_area` VALUES ('3587', '640200', '640201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3588', '640200', '640202', '大武口区');
INSERT INTO `jiecheng_area` VALUES ('3589', '640200', '640205', '惠农区');
INSERT INTO `jiecheng_area` VALUES ('3590', '640200', '640221', '平罗县');
INSERT INTO `jiecheng_area` VALUES ('3591', '640300', '640301', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3592', '640300', '640302', '利通区');
INSERT INTO `jiecheng_area` VALUES ('3593', '640300', '640303', '红寺堡区');
INSERT INTO `jiecheng_area` VALUES ('3594', '640300', '640323', '盐池县');
INSERT INTO `jiecheng_area` VALUES ('3595', '640300', '640324', '同心县');
INSERT INTO `jiecheng_area` VALUES ('3596', '640300', '640381', '青铜峡市');
INSERT INTO `jiecheng_area` VALUES ('3597', '640400', '640401', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3598', '640400', '640402', '原州区');
INSERT INTO `jiecheng_area` VALUES ('3599', '640400', '640422', '西吉县');
INSERT INTO `jiecheng_area` VALUES ('3600', '640400', '640423', '隆德县');
INSERT INTO `jiecheng_area` VALUES ('3601', '640400', '640424', '泾源县');
INSERT INTO `jiecheng_area` VALUES ('3602', '640400', '640425', '彭阳县');
INSERT INTO `jiecheng_area` VALUES ('3603', '640500', '640501', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3604', '640500', '640502', '沙坡头区');
INSERT INTO `jiecheng_area` VALUES ('3605', '640500', '640521', '中宁县');
INSERT INTO `jiecheng_area` VALUES ('3606', '640500', '640522', '海原县');
INSERT INTO `jiecheng_area` VALUES ('3607', '650000', '650100', '乌鲁木齐市');
INSERT INTO `jiecheng_area` VALUES ('3608', '650000', '650200', '克拉玛依市');
INSERT INTO `jiecheng_area` VALUES ('3609', '650000', '650400', '吐鲁番市');
INSERT INTO `jiecheng_area` VALUES ('3610', '650000', '650500', '哈密市');
INSERT INTO `jiecheng_area` VALUES ('3611', '650000', '652300', '昌吉回族自治州');
INSERT INTO `jiecheng_area` VALUES ('3612', '650000', '652700', '博尔塔拉蒙古自治州');
INSERT INTO `jiecheng_area` VALUES ('3613', '650000', '652800', '巴音郭楞蒙古自治州');
INSERT INTO `jiecheng_area` VALUES ('3614', '650000', '652900', '阿克苏地区');
INSERT INTO `jiecheng_area` VALUES ('3615', '650000', '653000', '克孜勒苏柯尔克孜自治州');
INSERT INTO `jiecheng_area` VALUES ('3616', '650000', '653100', '喀什地区');
INSERT INTO `jiecheng_area` VALUES ('3617', '650000', '653200', '和田地区');
INSERT INTO `jiecheng_area` VALUES ('3618', '650000', '654000', '伊犁哈萨克自治州');
INSERT INTO `jiecheng_area` VALUES ('3619', '650000', '654200', '塔城地区');
INSERT INTO `jiecheng_area` VALUES ('3620', '650000', '654300', '阿勒泰地区');
INSERT INTO `jiecheng_area` VALUES ('3621', '650000', '659000', '自治区直辖县级行政区划');
INSERT INTO `jiecheng_area` VALUES ('3622', '650100', '650101', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3623', '650100', '650102', '天山区');
INSERT INTO `jiecheng_area` VALUES ('3624', '650100', '650103', '沙依巴克区');
INSERT INTO `jiecheng_area` VALUES ('3625', '650100', '650104', '新市区');
INSERT INTO `jiecheng_area` VALUES ('3626', '650100', '650105', '水磨沟区');
INSERT INTO `jiecheng_area` VALUES ('3627', '650100', '650106', '头屯河区');
INSERT INTO `jiecheng_area` VALUES ('3628', '650100', '650107', '达坂城区');
INSERT INTO `jiecheng_area` VALUES ('3629', '650100', '650109', '米东区');
INSERT INTO `jiecheng_area` VALUES ('3630', '650100', '650121', '乌鲁木齐县');
INSERT INTO `jiecheng_area` VALUES ('3631', '650200', '650201', '市辖区');
INSERT INTO `jiecheng_area` VALUES ('3632', '650200', '650202', '独山子区');
INSERT INTO `jiecheng_area` VALUES ('3633', '650200', '650203', '克拉玛依区');
INSERT INTO `jiecheng_area` VALUES ('3634', '650200', '650204', '白碱滩区');
INSERT INTO `jiecheng_area` VALUES ('3635', '650200', '650205', '乌尔禾区');
INSERT INTO `jiecheng_area` VALUES ('3636', '650400', '650402', '高昌区');
INSERT INTO `jiecheng_area` VALUES ('3637', '650400', '650421', '鄯善县');
INSERT INTO `jiecheng_area` VALUES ('3638', '650400', '650422', '托克逊县');
INSERT INTO `jiecheng_area` VALUES ('3639', '650500', '650502', '伊州区');
INSERT INTO `jiecheng_area` VALUES ('3640', '650500', '650521', '巴里坤哈萨克自治县');
INSERT INTO `jiecheng_area` VALUES ('3641', '650500', '650522', '伊吾县');
INSERT INTO `jiecheng_area` VALUES ('3642', '652300', '652301', '昌吉市');
INSERT INTO `jiecheng_area` VALUES ('3643', '652300', '652302', '阜康市');
INSERT INTO `jiecheng_area` VALUES ('3644', '652300', '652323', '呼图壁县');
INSERT INTO `jiecheng_area` VALUES ('3645', '652300', '652324', '玛纳斯县');
INSERT INTO `jiecheng_area` VALUES ('3646', '652300', '652325', '奇台县');
INSERT INTO `jiecheng_area` VALUES ('3647', '652300', '652327', '吉木萨尔县');
INSERT INTO `jiecheng_area` VALUES ('3648', '652300', '652328', '木垒哈萨克自治县');
INSERT INTO `jiecheng_area` VALUES ('3649', '652700', '652701', '博乐市');
INSERT INTO `jiecheng_area` VALUES ('3650', '652700', '652702', '阿拉山口市');
INSERT INTO `jiecheng_area` VALUES ('3651', '652700', '652722', '精河县');
INSERT INTO `jiecheng_area` VALUES ('3652', '652700', '652723', '温泉县');
INSERT INTO `jiecheng_area` VALUES ('3653', '652800', '652801', '库尔勒市');
INSERT INTO `jiecheng_area` VALUES ('3654', '652800', '652822', '轮台县');
INSERT INTO `jiecheng_area` VALUES ('3655', '652800', '652823', '尉犁县');
INSERT INTO `jiecheng_area` VALUES ('3656', '652800', '652824', '若羌县');
INSERT INTO `jiecheng_area` VALUES ('3657', '652800', '652825', '且末县');
INSERT INTO `jiecheng_area` VALUES ('3658', '652800', '652826', '焉耆回族自治县');
INSERT INTO `jiecheng_area` VALUES ('3659', '652800', '652827', '和静县');
INSERT INTO `jiecheng_area` VALUES ('3660', '652800', '652828', '和硕县');
INSERT INTO `jiecheng_area` VALUES ('3661', '652800', '652829', '博湖县');
INSERT INTO `jiecheng_area` VALUES ('3662', '652800', '652871', '库尔勒经济技术开发区');
INSERT INTO `jiecheng_area` VALUES ('3663', '652900', '652901', '阿克苏市');
INSERT INTO `jiecheng_area` VALUES ('3664', '652900', '652922', '温宿县');
INSERT INTO `jiecheng_area` VALUES ('3665', '652900', '652923', '库车县');
INSERT INTO `jiecheng_area` VALUES ('3666', '652900', '652924', '沙雅县');
INSERT INTO `jiecheng_area` VALUES ('3667', '652900', '652925', '新和县');
INSERT INTO `jiecheng_area` VALUES ('3668', '652900', '652926', '拜城县');
INSERT INTO `jiecheng_area` VALUES ('3669', '652900', '652927', '乌什县');
INSERT INTO `jiecheng_area` VALUES ('3670', '652900', '652928', '阿瓦提县');
INSERT INTO `jiecheng_area` VALUES ('3671', '652900', '652929', '柯坪县');
INSERT INTO `jiecheng_area` VALUES ('3672', '653000', '653001', '阿图什市');
INSERT INTO `jiecheng_area` VALUES ('3673', '653000', '653022', '阿克陶县');
INSERT INTO `jiecheng_area` VALUES ('3674', '653000', '653023', '阿合奇县');
INSERT INTO `jiecheng_area` VALUES ('3675', '653000', '653024', '乌恰县');
INSERT INTO `jiecheng_area` VALUES ('3676', '653100', '653101', '喀什市');
INSERT INTO `jiecheng_area` VALUES ('3677', '653100', '653121', '疏附县');
INSERT INTO `jiecheng_area` VALUES ('3678', '653100', '653122', '疏勒县');
INSERT INTO `jiecheng_area` VALUES ('3679', '653100', '653123', '英吉沙县');
INSERT INTO `jiecheng_area` VALUES ('3680', '653100', '653124', '泽普县');
INSERT INTO `jiecheng_area` VALUES ('3681', '653100', '653125', '莎车县');
INSERT INTO `jiecheng_area` VALUES ('3682', '653100', '653126', '叶城县');
INSERT INTO `jiecheng_area` VALUES ('3683', '653100', '653127', '麦盖提县');
INSERT INTO `jiecheng_area` VALUES ('3684', '653100', '653128', '岳普湖县');
INSERT INTO `jiecheng_area` VALUES ('3685', '653100', '653129', '伽师县');
INSERT INTO `jiecheng_area` VALUES ('3686', '653100', '653130', '巴楚县');
INSERT INTO `jiecheng_area` VALUES ('3687', '653100', '653131', '塔什库尔干塔吉克自治县');
INSERT INTO `jiecheng_area` VALUES ('3688', '653200', '653201', '和田市');
INSERT INTO `jiecheng_area` VALUES ('3689', '653200', '653221', '和田县');
INSERT INTO `jiecheng_area` VALUES ('3690', '653200', '653222', '墨玉县');
INSERT INTO `jiecheng_area` VALUES ('3691', '653200', '653223', '皮山县');
INSERT INTO `jiecheng_area` VALUES ('3692', '653200', '653224', '洛浦县');
INSERT INTO `jiecheng_area` VALUES ('3693', '653200', '653225', '策勒县');
INSERT INTO `jiecheng_area` VALUES ('3694', '653200', '653226', '于田县');
INSERT INTO `jiecheng_area` VALUES ('3695', '653200', '653227', '民丰县');
INSERT INTO `jiecheng_area` VALUES ('3696', '654000', '654002', '伊宁市');
INSERT INTO `jiecheng_area` VALUES ('3697', '654000', '654003', '奎屯市');
INSERT INTO `jiecheng_area` VALUES ('3698', '654000', '654004', '霍尔果斯市');
INSERT INTO `jiecheng_area` VALUES ('3699', '654000', '654021', '伊宁县');
INSERT INTO `jiecheng_area` VALUES ('3700', '654000', '654022', '察布查尔锡伯自治县');
INSERT INTO `jiecheng_area` VALUES ('3701', '654000', '654023', '霍城县');
INSERT INTO `jiecheng_area` VALUES ('3702', '654000', '654024', '巩留县');
INSERT INTO `jiecheng_area` VALUES ('3703', '654000', '654025', '新源县');
INSERT INTO `jiecheng_area` VALUES ('3704', '654000', '654026', '昭苏县');
INSERT INTO `jiecheng_area` VALUES ('3705', '654000', '654027', '特克斯县');
INSERT INTO `jiecheng_area` VALUES ('3706', '654000', '654028', '尼勒克县');
INSERT INTO `jiecheng_area` VALUES ('3707', '654200', '654201', '塔城市');
INSERT INTO `jiecheng_area` VALUES ('3708', '654200', '654202', '乌苏市');
INSERT INTO `jiecheng_area` VALUES ('3709', '654200', '654221', '额敏县');
INSERT INTO `jiecheng_area` VALUES ('3710', '654200', '654223', '沙湾县');
INSERT INTO `jiecheng_area` VALUES ('3711', '654200', '654224', '托里县');
INSERT INTO `jiecheng_area` VALUES ('3712', '654200', '654225', '裕民县');
INSERT INTO `jiecheng_area` VALUES ('3713', '654200', '654226', '和布克赛尔蒙古自治县');
INSERT INTO `jiecheng_area` VALUES ('3714', '654300', '654301', '阿勒泰市');
INSERT INTO `jiecheng_area` VALUES ('3715', '654300', '654321', '布尔津县');
INSERT INTO `jiecheng_area` VALUES ('3716', '654300', '654322', '富蕴县');
INSERT INTO `jiecheng_area` VALUES ('3717', '654300', '654323', '福海县');
INSERT INTO `jiecheng_area` VALUES ('3718', '654300', '654324', '哈巴河县');
INSERT INTO `jiecheng_area` VALUES ('3719', '654300', '654325', '青河县');
INSERT INTO `jiecheng_area` VALUES ('3720', '654300', '654326', '吉木乃县');
INSERT INTO `jiecheng_area` VALUES ('3721', '659000', '659001', '石河子市');
INSERT INTO `jiecheng_area` VALUES ('3722', '659000', '659002', '阿拉尔市');
INSERT INTO `jiecheng_area` VALUES ('3723', '659000', '659003', '图木舒克市');
INSERT INTO `jiecheng_area` VALUES ('3724', '659000', '659004', '五家渠市');
INSERT INTO `jiecheng_area` VALUES ('3725', '659000', '659006', '铁门关市');
INSERT INTO `jiecheng_area` VALUES ('3726', '710000', '710100', '台北市');
INSERT INTO `jiecheng_area` VALUES ('3727', '710000', '710200', '高雄市');
INSERT INTO `jiecheng_area` VALUES ('3728', '710000', '710300', '基隆市');
INSERT INTO `jiecheng_area` VALUES ('3729', '710000', '710400', '台中市');
INSERT INTO `jiecheng_area` VALUES ('3730', '710000', '710500', '台南市');
INSERT INTO `jiecheng_area` VALUES ('3731', '710000', '710600', '新竹市');
INSERT INTO `jiecheng_area` VALUES ('3732', '710000', '710700', '嘉义市');
INSERT INTO `jiecheng_area` VALUES ('3733', '710100', '710101', '内湖区');
INSERT INTO `jiecheng_area` VALUES ('3734', '710100', '710102', '南港区');
INSERT INTO `jiecheng_area` VALUES ('3735', '710100', '710103', '中正区');
INSERT INTO `jiecheng_area` VALUES ('3736', '710100', '710104', '松山区');
INSERT INTO `jiecheng_area` VALUES ('3737', '710100', '710105', '信义区');
INSERT INTO `jiecheng_area` VALUES ('3738', '710100', '710106', '大安区');
INSERT INTO `jiecheng_area` VALUES ('3739', '710100', '710107', '中山区');
INSERT INTO `jiecheng_area` VALUES ('3740', '710100', '710108', '文山区');
INSERT INTO `jiecheng_area` VALUES ('3741', '710100', '710109', '大同区');
INSERT INTO `jiecheng_area` VALUES ('3742', '710100', '710110', '万华区');
INSERT INTO `jiecheng_area` VALUES ('3743', '710100', '710111', '士林区');
INSERT INTO `jiecheng_area` VALUES ('3744', '710100', '710112', '北投区');
INSERT INTO `jiecheng_area` VALUES ('3745', '710200', '710201', '新兴区');
INSERT INTO `jiecheng_area` VALUES ('3746', '710200', '710202', '前金区');
INSERT INTO `jiecheng_area` VALUES ('3747', '710200', '710203', '芩雅区');
INSERT INTO `jiecheng_area` VALUES ('3748', '710200', '710204', '盐埕区');
INSERT INTO `jiecheng_area` VALUES ('3749', '710200', '710205', '鼓山区');
INSERT INTO `jiecheng_area` VALUES ('3750', '710200', '710206', '旗津区');
INSERT INTO `jiecheng_area` VALUES ('3751', '710200', '710207', '前镇区');
INSERT INTO `jiecheng_area` VALUES ('3752', '710200', '710208', '三民区');
INSERT INTO `jiecheng_area` VALUES ('3753', '710200', '710209', '左营区');
INSERT INTO `jiecheng_area` VALUES ('3754', '710200', '710210', '楠梓区');
INSERT INTO `jiecheng_area` VALUES ('3755', '710200', '710211', '小港区');
INSERT INTO `jiecheng_area` VALUES ('3756', '710300', '710301', '仁爱区');
INSERT INTO `jiecheng_area` VALUES ('3757', '710300', '710302', '信义区');
INSERT INTO `jiecheng_area` VALUES ('3758', '710300', '710303', '中正区');
INSERT INTO `jiecheng_area` VALUES ('3759', '710300', '710304', '暖暖区');
INSERT INTO `jiecheng_area` VALUES ('3760', '710300', '710305', '安乐区');
INSERT INTO `jiecheng_area` VALUES ('3761', '710300', '710307', '七堵区');
INSERT INTO `jiecheng_area` VALUES ('3762', '710400', '710401', '中区');
INSERT INTO `jiecheng_area` VALUES ('3763', '710400', '710402', '东区');
INSERT INTO `jiecheng_area` VALUES ('3764', '710400', '710403', '南区');
INSERT INTO `jiecheng_area` VALUES ('3765', '710400', '710404', '西区');
INSERT INTO `jiecheng_area` VALUES ('3766', '710400', '710405', '北区');
INSERT INTO `jiecheng_area` VALUES ('3767', '710400', '710406', '北屯区');
INSERT INTO `jiecheng_area` VALUES ('3768', '710400', '710407', '西屯区');
INSERT INTO `jiecheng_area` VALUES ('3769', '710400', '710408', '南屯区');
INSERT INTO `jiecheng_area` VALUES ('3770', '710500', '710501', '中西区');
INSERT INTO `jiecheng_area` VALUES ('3771', '710500', '710502', '东区');
INSERT INTO `jiecheng_area` VALUES ('3772', '710500', '710503', '南区');
INSERT INTO `jiecheng_area` VALUES ('3773', '710500', '710504', '北区');
INSERT INTO `jiecheng_area` VALUES ('3774', '710500', '710505', '安平区');
INSERT INTO `jiecheng_area` VALUES ('3775', '710500', '710506', '安南区');
INSERT INTO `jiecheng_area` VALUES ('3776', '710600', '710601', '东区');
INSERT INTO `jiecheng_area` VALUES ('3777', '710600', '710602', '北区');
INSERT INTO `jiecheng_area` VALUES ('3778', '710600', '710603', '香山区');
INSERT INTO `jiecheng_area` VALUES ('3779', '710700', '710701', '东区');
INSERT INTO `jiecheng_area` VALUES ('3780', '710700', '710702', '西区');
INSERT INTO `jiecheng_area` VALUES ('3781', '810000', '810001', '中西區');
INSERT INTO `jiecheng_area` VALUES ('3782', '810000', '810002', '灣仔區');
INSERT INTO `jiecheng_area` VALUES ('3783', '810000', '810003', '東區');
INSERT INTO `jiecheng_area` VALUES ('3784', '810000', '810004', '南區');
INSERT INTO `jiecheng_area` VALUES ('3785', '810000', '810005', '油尖旺區');
INSERT INTO `jiecheng_area` VALUES ('3786', '810000', '810006', '深水埗區');
INSERT INTO `jiecheng_area` VALUES ('3787', '810000', '810007', '九龍城區');
INSERT INTO `jiecheng_area` VALUES ('3788', '810000', '810008', '黃大仙區');
INSERT INTO `jiecheng_area` VALUES ('3789', '810000', '810009', '觀塘區');
INSERT INTO `jiecheng_area` VALUES ('3790', '810000', '810010', '荃灣區');
INSERT INTO `jiecheng_area` VALUES ('3791', '810000', '810011', '屯門區');
INSERT INTO `jiecheng_area` VALUES ('3792', '810000', '810012', '元朗區');
INSERT INTO `jiecheng_area` VALUES ('3793', '810000', '810013', '北區');
INSERT INTO `jiecheng_area` VALUES ('3794', '810000', '810014', '大埔區');
INSERT INTO `jiecheng_area` VALUES ('3795', '810000', '810015', '西貢區');
INSERT INTO `jiecheng_area` VALUES ('3796', '810000', '810016', '沙田區');
INSERT INTO `jiecheng_area` VALUES ('3797', '810000', '810017', '葵青區');
INSERT INTO `jiecheng_area` VALUES ('3798', '810000', '810018', '離島區');
INSERT INTO `jiecheng_area` VALUES ('3799', '820000', '820001', '花地瑪堂區');
INSERT INTO `jiecheng_area` VALUES ('3800', '820000', '820002', '花王堂區');
INSERT INTO `jiecheng_area` VALUES ('3801', '820000', '820003', '望德堂區');
INSERT INTO `jiecheng_area` VALUES ('3802', '820000', '820004', '大堂區');
INSERT INTO `jiecheng_area` VALUES ('3803', '820000', '820005', '風順堂區');
INSERT INTO `jiecheng_area` VALUES ('3804', '820000', '820006', '嘉模堂區');
INSERT INTO `jiecheng_area` VALUES ('3805', '820000', '820007', '路氹填海區');
INSERT INTO `jiecheng_area` VALUES ('3806', '820000', '820008', '聖方濟各堂區');

INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (1, '顺丰速运', 'SF', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (2, '百世快递', 'HTKY', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (3, '中通快递', 'ZTO', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (4, '申通快递', 'STO', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (5, '圆通速递', 'YTO', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (6, '韵达速递', 'YD', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (7, '邮政快递包裹', 'YZPY', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (8, 'EMS', 'EMS', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (9, '天天快递', 'HHTT', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (10, '京东快递', 'JD', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (11, '优速快递', 'UC', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (12, '德邦快递', 'DBL', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (13, '宅急送', 'ZJS', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (14, '安捷快递', 'AJ', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES (15, '阿里跨境电商物流', 'ALKJWL', NULL, NULL, NULL, 1);
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('16', '安迅物流', 'AX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('17', '安邮美国', 'AYUS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('18', '亚马逊物流', 'AMAZON', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('19', '澳门邮政', 'AOMENYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('20', '安能物流', 'ANE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('21', '澳多多', 'ADD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('22', '澳邮专线', 'AYCA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('23', '安鲜达', 'AXD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('24', '安能快运', 'ANEKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('25', '澳邦国际', 'ABGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('26', '安得物流', 'ANNTO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('27', '澳德物流', 'AUODEXPRESS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('28', '转运四方', 'A4PX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('29', '八达通  ', 'BDT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('30', '百腾物流', 'BETWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('31', '北极星快运', 'BJXKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('32', '奔腾物流', 'BNTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('33', '百福东方', 'BFDF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('34', '贝海国际 ', 'BHGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('35', '八方安运', 'BFAY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('36', '百世快运', 'BTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('37', '帮帮发转运', 'BBFZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('38', '百城通物流', 'BCTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('39', '春风物流', 'CFWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('40', '诚通物流', 'CHTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('41', '传喜物流', 'CXHY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('42', '城市100', 'CITY100', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('43', '城际快递', 'CJKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('44', 'CNPEX中邮快递', 'CNPEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('45', 'COE东方快递', 'COE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('46', '长沙创一', 'CSCY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('47', '成都善途速运', 'CDSTKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('48', '联合运通', 'CTG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('49', '疯狂快递', 'CRAZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('50', 'CBO钏博物流', 'CBO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('51', '佳吉快运', 'CNEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('52', '承诺达', 'CND', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('53', '畅顺通达', 'CSTD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('54', 'D速物流', 'DSWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('55', '到了港', 'DLG ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('56', '大田物流', 'DTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('57', '东骏快捷物流', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('58', '德坤', 'DEKUN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('59', '德邦快运', 'DBLKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('60', '大马鹿', 'DML', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('61', '丹鸟物流', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('62', '东方汇', 'EST365', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('63', 'E特快', 'ETK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('64', 'EMS国内', 'EMS2', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('65', 'EWE', 'EWE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('66', 'E通速递', 'ETONG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('67', '卓志速运', 'ESDEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('68', '飞康达', 'FKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('69', '富腾达  ', 'FTD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('70', '凡宇货的', 'FYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('71', '速派快递', 'FASTGO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('72', '飞豹快递', 'FBKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('73', '丰巢', 'FBOX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('74', '飞狐快递', 'FHKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('75', '复融供应链', 'FRGYL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('76', '飞远配送', 'FYPS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('77', '凡宇速递', 'FYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('78', '丰通快运', 'FT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('79', '冠达   ', 'GD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('80', '广东邮政', 'GDEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('81', '共速达', 'GSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('82', '广通       ', 'GTONG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('83', '冠达快递', 'GDKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('84', '挂号信', 'GHX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('85', '广通速递', 'GTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('86', '高铁快运', 'GTKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('87', '迦递快递', 'GAI', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('88', '港快速递', 'GKSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('89', '高铁速递', 'GTSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('90', '黑狗冷链', 'HGLL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('91', '恒路物流', 'HLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('92', '天地华宇', 'HOAU', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('93', '鸿桥供应链', 'HOTSCM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('94', '海派通物流公司', 'HPTEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('95', '华强物流', 'hq568', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('96', '环球速运  ', 'HQSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('97', '华夏龙物流', 'HXLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('98', '河北建华', 'HBJH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('99', '汇丰物流', 'HF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('100', '华航快递', 'HHKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('101', '华翰物流', 'HHWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('102', '黄马甲快递', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('103', '海盟速递', 'HMSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('104', '华企快运', 'HQKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('105', '昊盛物流', 'HSWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('106', '鸿泰物流', 'HTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('107', '豪翔物流 ', 'HXWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('108', '合肥汇文', 'HFHW', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('109', '辉隆物流', 'HLONGWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('110', '华企快递', 'HQKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('111', '韩润物流', 'HRWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('112', '青岛恒通快递', 'HTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('113', '货运皇物流', 'HYH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('114', '好来运快递', 'HLYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('115', '皇家物流', 'HJWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('116', '海信物流', 'HISENSE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('117', '汇森速运', 'HSSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('118', '徽托邦物流', 'HTB56', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('119', '捷安达  ', 'JAD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('120', '京广速递', 'JGSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('121', '九曳供应链', 'JIUYE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('122', '急先达', 'JXD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('123', '晋越快递', 'JYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('124', '佳成国际', 'JCEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('125', '捷特快递', 'JTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('126', '精英速运', 'JYSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('127', '加运美', 'JYM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('128', '景光物流', 'JGWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('129', '佳怡物流', 'JYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('130', '京东快运', 'JDKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('131', '金大物流', 'JDWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('132', '极兔速递', 'JTSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('133', '跨越速运', 'KYSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('134', '快服务', 'KFW', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('135', '快速递物流', 'KSDWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('136', '康力物流', 'KLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('137', '快淘快递', 'KTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('138', '快优达速递', 'KYDSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('139', '跨越物流', 'KYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('140', '快8速运', 'KBSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('141', '龙邦快递', 'LB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('142', '蓝弧快递', 'LHKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('143', '乐捷递', 'LJD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('144', '立即送', 'LJS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('145', '联昊通速递', 'LHT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('146', '民邦快递', 'MB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('147', '民航快递', 'MHKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('148', '美快    ', 'MK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('149', '门对门快递', 'MDM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('150', '迈达', 'MD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('151', '闽盛快递', 'MSKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('152', '迈隆递运', 'MRDY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('153', '明亮物流', 'MLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('154', '南方传媒物流', 'NFCM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('155', '南京晟邦物流', 'NJSBWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('156', '能达速递', 'NEDA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('157', '平安达腾飞快递', 'PADTF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('158', '泛捷快递', 'PANEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('159', '品骏快递', 'PJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('160', '陪行物流', 'PXWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('161', 'PCA Express', 'PCA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('162', '全晨快递', 'QCKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('163', '全日通快递', 'QRT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('164', '快客快递', 'QUICK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('165', '全信通', 'QXT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('166', '七曜中邮', 'QYZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('167', '如风达', 'RFD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('168', '荣庆物流', 'RQ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('169', '日日顺物流', 'RRS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('170', '日昱物流', 'RLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('171', '瑞丰速递', 'RFEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('172', '赛澳递', 'SAD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('173', '苏宁物流', 'SNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('174', '圣安物流', 'SAWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('175', '晟邦物流', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('176', '上大物流', 'SDWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('177', '盛丰物流', 'SFWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('178', '速通物流', 'ST', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('179', '速腾快递', 'STWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('180', '速必达物流', 'SUBIDA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('181', '速递e站', 'SDEZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('182', '速呈宅配', 'SCZPDS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('183', '速尔快递', 'SURE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('184', '山东海红', 'SDHH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('185', '顺丰国际', 'SFGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('186', '盛辉物流', 'SHWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('187', '穗佳物流', 'SJWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('188', '三态速递', 'STSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('189', '山西红马甲', 'SXHMJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('190', '世运快递', 'SYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('191', '闪送', 'SS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('192', '盛通快递', 'STKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('193', '郑州速捷', 'SJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('194', '顺心捷达', 'SX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('195', '商桥物流', 'SQWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('196', '佳旺达物流', 'SYJWDX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('197', '速递中国', 'SENDCN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('198', '台湾邮政', 'TAIWANYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('199', '唐山申通', 'TSSTO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('200', '特急送', 'TJS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('201', '通用物流', 'TYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('202', '华宇物流', 'TDHY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('203', '通和天下', 'THTX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('204', '腾林物流', 'TLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('205', '泰捷达物流', 'TJDGJWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('206', '全一快递', 'UAPEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('207', 'UBI', 'UBI', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('208', 'UEQ Express', 'UEQ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('209', '美国快递', 'USEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('210', '万家康  ', 'WJK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('211', '万家物流', 'WJWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('212', '武汉同舟行', 'WHTZX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('213', '维普恩', 'WPE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('214', '中粮我买网', 'WM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('215', '万象物流', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('216', '微特派', 'WTP', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('217', '温通物流', 'WTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('218', '文捷航空速递', 'WJEXPRESS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('219', '迅驰物流  ', 'XCWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('220', '信丰物流', 'XFEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('221', '希优特', 'XYT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('222', '新邦物流', 'XBWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('223', '祥龙运通', 'XLYT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('224', '新杰物流', 'XJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('225', '晓毕物流', 'XIAOBI', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('226', '迅达速递', 'XDEXPRESS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('227', '源安达快递', 'YAD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('228', '远成物流', 'YCWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('229', '远成快运', 'YCSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('230', '义达国际物流', 'YDH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('231', '易达通  ', 'YDT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('232', '原飞航物流', 'YFHEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('233', '亚风快递', 'YFSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('234', '运通快递', 'YTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('235', '亿翔快递', 'YXKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('236', '运东西网', 'YUNDX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('237', '壹米滴答', 'YMDD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('238', '邮政国内标快', 'YZBK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('239', '一站通速运', 'YZTSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('240', '驭丰速运', 'YFSUYUN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('241', '余氏东风', 'YSDF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('242', '耀飞快递', 'YF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('243', '韵达快运', 'YDKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('244', '云路', 'YL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('245', '邮必佳', 'YBJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('246', '越丰物流', 'YFEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('247', '银捷速递', 'YJSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('248', '优联吉运', 'YLJY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('249', '亿领速运', 'YLSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('250', '英脉物流', 'YMWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('251', '亿顺航', 'YSH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('252', '音素快运', 'YSKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('253', '易通达', 'YTD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('254', '一统飞鸿', 'YTFH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('255', '圆通国际', 'YTOGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('256', '宇鑫物流', 'YXWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('257', '包裹/平邮/挂号信', 'YZGN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('258', '一智通', 'YZT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('259', '优拜物流', 'YBWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('260', '一路发物流', 'YLFWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('261', '云聚物流', 'YJWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('262', '增益快递', 'ZENY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('263', '中睿速递', 'ZRSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('264', '中铁快运', 'ZTKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('265', '中天万运', 'ZTWY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('266', '中外运速递', 'ZWYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('267', '澳转运', 'ZY_AZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('268', '八达网', 'ZY_BDA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('269', '贝易购', 'ZY_BYECO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('270', '赤兔马转运', 'ZY_CTM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('271', 'CUL中美速递', 'ZY_CUL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('272', 'ETD', 'ZY_ETD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('273', '风驰快递', 'ZY_FCKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('274', '风雷速递', 'ZY_FLSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('275', '皓晨优递', 'ZY_HCYD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('276', '海带宝', 'ZY_HDB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('277', '汇丰美中速递', 'ZY_HFMZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('278', '豪杰速递', 'ZY_HJSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('279', '华美快递', 'ZY_HMKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('280', '360hitao转运', 'ZY_HTAO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('281', '海淘村', 'ZY_HTCUN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('282', '365海淘客', 'ZY_HTKE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('283', '华通快运', 'ZY_HTONG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('284', '海星桥快递', 'ZY_HXKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('285', '华兴速运', 'ZY_HXSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('286', 'LogisticsY', 'ZY_IHERB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('287', '领跑者快递', 'ZY_LPZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('288', '量子物流', 'ZY_LZWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('289', '明邦转运', 'ZY_MBZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('290', '美嘉快递', 'ZY_MJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('291', '168 美中快递', 'ZY_MZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('292', '欧e捷', 'ZY_OEJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('293', '欧洲疯', 'ZY_OZF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('294', '欧洲GO', 'ZY_OZGO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('295', '全美通', 'ZY_QMT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('296', 'SCS国际物流', 'ZY_SCS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('297', 'SOHO苏豪国际', 'ZY_SOHO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('298', 'Sonic-Ex速递', 'ZY_SONIC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('299', '通诚美中快递', 'ZY_TCM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('300', 'TrakPak', 'ZY_TPAK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('301', '天天海淘', 'ZY_TTHT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('302', '天泽快递', 'ZY_TZKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('303', '迅达快递', 'ZY_XDKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('304', '信达速运', 'ZY_XDSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('305', '新干线快递', 'ZY_XGX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('306', '信捷转运', 'ZY_XJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('307', '优购快递', 'ZY_YGKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('308', '友家速递(UCS)', 'ZY_YJSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('309', '云畔网', 'ZY_YPW', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('310', '易送网', 'ZY_YSW', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('311', '中运全速', 'ZYQS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('312', '中邮物流', 'ZYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('313', '汇强快递', 'ZHQKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('314', '众通快递', 'ZTE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('315', '中通快运', 'ZTOKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('316', '中邮快递', 'ZYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('317', '芝麻开门', 'DNWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('318', '中骅物流', 'ZHWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('319', '中铁物流', 'ZTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('320', '智汇鸟', 'ZHN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('321', '众邮快递', 'ZYE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('322', '中环快递', 'ZHONGHUAN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('323', 'AAE全球专递', 'AAE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('324', 'ACS雅仕快递', 'ACS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('325', 'ADP Express Tracking', 'ADP', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('326', '安圭拉邮政', 'ANGUILAYOU', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('327', 'APAC', 'APAC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('328', 'Aramex', 'ARAMEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('329', '奥地利邮政', 'AT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('330', 'AOL（澳通）', 'AOL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('331', 'Australia Post Tracking', 'AUSTRALIA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('332', '澳邮国际', 'AUEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('333', '比利时邮政', 'BEL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('334', 'BHT快递', 'BHT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('335', '秘鲁邮政', 'BILUYOUZHE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('336', '巴西邮政', 'BR', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('337', '巴伦支快递', 'BALUNZHI', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('338', 'BETWL_Crack', 'BETWL_Crack', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('339', '败欧洲', 'BEUROPE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('340', 'BCWELT   ', 'BCWELT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('341', '笨鸟国际', 'BN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('342', '宝凯物流', 'BKWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('343', '巴伦支', 'BLZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('344', 'BNTWL_Crack', 'BNTWL_Crack', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('345', '北青小红帽', 'BQXHM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('346', '不丹邮政', 'BUDANYOUZH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('347', '邦送物流', 'BSWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('348', '波兰邮政', 'BLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('349', 'CCES快递', 'CCES', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('350', '出口易', 'CKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('351', '新配盟', 'CNXLM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('352', 'CDEK', 'CDEK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('353', '加拿大邮政', 'CA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('354', '程光物流', 'CG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('355', '递必易国际物流', 'DBYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('356', '大道物流', 'DDWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('357', '德国云快递', 'DGYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('358', '到乐国际', 'DLGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('359', 'DHL', 'DHL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('360', 'DHL德国', 'DHL_DE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('361', 'DHL(英文版)', 'DHL_EN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('362', 'DHL全球', 'DHL_GLB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('363', 'DHL Global Mail', 'DHLGM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('364', '丹麦邮政', 'DK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('365', '德创物流', 'DCWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('366', 'DHL(中国件)', 'DHL_C', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('367', 'DHL(美国)', 'DHL_USA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('368', '东红物流', 'DHWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('369', '店通快递', 'DTKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('370', '大洋物流快递', 'DYWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('371', 'DPD', 'DPD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('372', '递四方速递', 'D4PX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('373', 'DPEX', 'DPEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('374', '鼎润物流', 'DRL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('375', 'EMS国际', 'EMSGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('376', '易客满', 'EKM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('377', 'EShipper', 'ESHIPPER', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('378', 'EPS (联众国际快运)', 'EPS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('379', 'EASY-EXPRESS', 'EASYEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('380', 'EFS POST', 'EASYEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('381', '丰程物流', 'FCWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('382', '法翔速运', 'FX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('383', 'FQ', 'FQ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('384', '芬兰邮政', 'FLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('385', '方舟国际速递', 'FZGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('386', 'FEDEX联邦(国际件）', 'FEDEX_GJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('387', 'FEDEX联邦(国内件）', 'FEDEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('388', '国际e邮宝', 'GJEYB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('389', '国际邮政包裹', 'GJYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('390', 'GE2D', 'GE2D', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('391', 'GLS', 'GLS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('392', '冠泰', 'GT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('393', '欧洲专线(邮政)', 'IOZYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('394', '澳大利亚邮政', 'IADLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('395', '阿尔巴尼亚邮政', 'IAEBNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('396', '阿尔及利亚邮政', 'IAEJLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('397', '阿富汗邮政', 'IAFHYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('398', '安哥拉邮政', 'IAGLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('399', '埃及邮政', 'IAJYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('400', '阿鲁巴邮政', 'IALBYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('401', '阿联酋邮政', 'IALYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('402', '阿塞拜疆邮政', 'IASBJYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('403', '博茨瓦纳邮政', 'IBCWNYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('404', '波多黎各邮政', 'IBDLGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('405', '冰岛邮政', 'IBDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('406', '白俄罗斯邮政', 'IBELSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('407', '波黑邮政', 'IBHYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('408', '保加利亚邮政', 'IBJLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('409', '巴基斯坦邮政', 'IBJSTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('410', '黎巴嫩邮政', 'IBLNYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('411', '波兰邮政', 'IBOLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('412', '宝通达', 'IBTD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('413', '贝邮宝', 'IBYB', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('414', '德国邮政', 'IDGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('415', '危地马拉邮政', 'IWDMLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('416', '乌干达邮政', 'IWGDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('417', '乌克兰EMS', 'IWKLEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('418', '乌克兰邮政', 'IWKLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('419', '乌拉圭邮政', 'IWLGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('420', '林克快递', 'ILKKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('421', '文莱邮政', 'IWLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('422', '新喀里多尼亚邮政', 'IXGLDNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('423', '爱尔兰邮政', 'IE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('424', '夏浦物流', 'IXPWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('425', '印度邮政', 'IYDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('426', '夏浦世纪', 'IXPSJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('427', '厄瓜多尔邮政', 'IEGDEYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('428', '俄罗斯邮政', 'IELSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('429', '飞特物流', 'IFTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('430', '瓜德罗普岛邮政', 'IGDLPDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('431', '哥斯达黎加邮政', 'IGSDLJYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('432', '韩国邮政', 'IHGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('433', '互联易', 'IHLY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('434', '哈萨克斯坦邮政', 'IHSKSTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('435', '黑山邮政', 'IHSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('436', '津巴布韦邮政', 'IJBBWYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('437', '吉尔吉斯斯坦邮政', 'IJEJSSTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('438', '捷克邮政', 'IJKYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('439', '加纳邮政', 'IJNYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('440', '柬埔寨邮政', 'IJPZYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('441', '安的列斯群岛邮政', 'IADLSQDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('442', '阿根廷邮政', 'IAGTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('443', '奥兰群岛邮政', 'IALQDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('444', '阿曼邮政', 'IAMYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('445', '埃塞俄比亚邮政', 'IASEBYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('446', '爱沙尼亚邮政', 'IASNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('447', '阿森松岛邮政', 'IASSDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('448', '便利速递', 'IBLSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('449', '巴林邮政', 'IBLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('450', '百慕达邮政', 'IBMDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('451', '达方物流', 'IDFWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('452', '厄立特里亚邮政', 'IELTLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('453', '瓜德罗普岛EMS', 'IGDLPDEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('454', '俄速递', 'IGJESD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('455', '哥伦比亚邮政', 'IGLBYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('456', '格陵兰邮政', 'IGLLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('457', '科特迪瓦邮政', 'IKTDWYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('458', '卡塔尔邮政', 'IKTEYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('459', '利比亚邮政', 'ILBYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('460', '卢森堡邮政', 'ILSBYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('461', '拉脱维亚邮政', 'ILTWYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('462', '立陶宛邮政', 'ILTWYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('463', '列支敦士登邮政', 'ILZDSDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('464', '马尔代夫邮政', 'IMEDFYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('465', '孟加拉国EMS', 'IMJLGEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('466', '摩洛哥邮政', 'IMLGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('467', '毛里求斯邮政', 'IMLQSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('468', '马来西亚EMS', 'IMLXYEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('469', '马来西亚邮政', 'IMLXYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('470', '马其顿邮政', 'IMQDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('471', '马提尼克EMS', 'IMTNKEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('472', '马提尼克邮政', 'IMTNKYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('473', '墨西哥邮政', 'IMXGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('474', '南非邮政', 'INFYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('475', '挪威邮政', 'INWYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('476', '葡萄牙邮政', 'IPTYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('477', '全球快递', 'IQQKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('478', '全通物流', 'IQTWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('479', '苏丹邮政', 'ISDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('480', '萨尔瓦多邮政', 'ISEWDYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('481', '斯洛伐克邮政', 'ISLFKYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('482', '斯洛文尼亚邮政', 'ISLWNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('483', '沙特阿拉伯邮政', 'ISTALBYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('484', '土耳其邮政', 'ITEQYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('485', '泰国邮政', 'ITGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('486', '特立尼达和多巴哥EMS', 'ITLNDHDBGE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('487', '突尼斯邮政', 'ITNSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('488', '坦桑尼亚邮政', 'ITSNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('489', '乌兹别克斯坦EMS', 'IWZBKSTEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('490', '小飞龙物流', 'IXFLWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('491', '新加坡邮政', 'IXJPYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('492', '叙利亚邮政', 'IXLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('493', '匈牙利邮政', 'IXYLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('494', '印度尼西亚邮政', 'IYDNXYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('495', '伊朗邮政', 'IYLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('496', '越南邮政', 'IYNYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('497', '以色列邮政', 'IYSLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('498', '易通关', 'IYTG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('499', '燕文物流', 'IYWWL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('500', '直布罗陀邮政', 'IZBLTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('501', '克罗地亚邮政', 'IKNDYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('502', '肯尼亚邮政', 'IKNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('503', '科特迪瓦EMS', 'IKTDWEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('504', '罗马尼亚邮政', 'ILMNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('505', '摩尔多瓦邮政', 'IMEDWYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('506', '马耳他邮政', 'IMETYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('507', '尼日利亚邮政', 'INRLYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('508', '塞尔维亚邮政', 'ISEWYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('509', '塞浦路斯邮政', 'ISPLSYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('510', '乌兹别克斯坦邮政', 'IWZBKSTYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('511', '西班牙邮政', 'IXBYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('512', '新加坡EMS', 'IXJPEMS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('513', '希腊邮政', 'IXLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('514', '新西兰邮政', 'IXXLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('515', '意大利邮政', 'IYDLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('516', '英国邮政', 'IYGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('517', '亚美尼亚邮政', 'IYMNYYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('518', '智利邮政', 'IZLYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('519', '也门邮政', 'IYMYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('520', '日本邮政', 'JP', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('521', '今枫国际', 'JFGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('522', '极光转运', 'JGZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('523', '吉祥邮转运', 'JXYKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('524', '嘉里国际', 'JLDT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('525', '上海久易国际', 'JYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('526', '绝配国际速递', 'JPKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('527', '联运通', 'LYT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('528', '联合快递', 'LHKDS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('529', '新顺丰', 'NSF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('530', '荷兰邮政', 'NL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('531', 'ONTRAC', 'ONTRAC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('532', 'OCS', 'OCS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('533', '啪啪供应链', 'PAPA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('534', 'POSTEIBE', 'POSTEIBE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('535', '全球邮政', 'QQYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('536', '秦远海运', 'QYHY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('537', '瑞典邮政', 'RDSE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('538', '澳洲飞跃', 'RLG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('539', 'SKYPOST', 'SKYPOST', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('540', '林道国际', 'SHLDHY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('541', '佳惠尔', 'SYJHE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('542', '瑞士邮政', 'SWCH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('543', '首达速运', 'SDSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('544', '穗空物流', 'SK', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('545', '首通快运', 'STONG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('546', '申通快递国际单', 'STO_INTL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('547', '光线速递', 'SUNSHINE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('548', 'TNT快递', 'TNT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('549', '泰国138', 'TAILAND138', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('550', '优邦国际速运', 'UBONEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('551', 'UEX   ', 'UEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('552', 'USPS美国邮政', 'USPS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('553', '万国邮政', 'UPU', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('554', 'UPS', 'UPS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('555', '启辰国际', 'VENUCIA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('556', '中越国际物流', 'VCTRANS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('557', '星空国际', 'XKGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('558', '迅达国际', 'XD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('559', '香港邮政', 'XGYZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('560', '喜来快递', 'XLKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('561', '鑫世锐达', 'XSRD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('562', '新元国际', 'XYGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('563', '西邮寄', 'XYJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('564', 'ADLER雄鹰国际速递', 'XYGJSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('565', '日本大和运输(Yamato)', 'YAMA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('566', 'YODEL', 'YODEL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('567', '一号线', 'YHXGJSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('568', '约旦邮政', 'YUEDANYOUZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('569', '玥玛速运', 'YMSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('570', '鹰运', 'YYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('571', '易境达', 'YJD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('572', '洋包裹', 'YBG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('573', '友家速递', 'YJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('574', '爱购转运', 'ZY_AG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('575', '爱欧洲', 'ZY_AOZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('576', '澳世速递', 'ZY_AUSE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('577', 'AXO', 'ZY_AXO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('578', '贝海速递', 'ZY_BH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('579', '蜜蜂速递', 'ZY_BEE', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('580', '百利快递', 'ZY_BL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('581', '斑马物流', 'ZY_BM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('582', '百通物流', 'ZY_BT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('583', '策马转运', 'ZY_CM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('584', '宜送转运', 'ZY_ESONG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('585', '飞碟快递', 'ZY_FD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('586', '飞鸽快递', 'ZY_FG', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('587', '风行快递', 'ZY_FX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('588', '风行速递', 'ZY_FXSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('589', '飞洋快递', 'ZY_FY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('590', '皓晨快递', 'ZY_HC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('591', '海悦速递', 'ZY_HYSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('592', '君安快递', 'ZY_JA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('593', '时代转运', 'ZY_JD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('594', '骏达快递', 'ZY_JDKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('595', '骏达转运', 'ZY_JDZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('596', '久禾快递', 'ZY_JH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('597', '金海淘', 'ZY_JHT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('598', '联邦转运FedRoad', 'ZY_LBZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('599', '龙象快递', 'ZY_LX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('600', '美国转运', 'ZY_MGZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('601', '美速通', 'ZY_MST', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('602', '美西转运', 'ZY_MXZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('603', 'QQ-EX', 'ZY_QQEX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('604', '瑞天快递', 'ZY_RT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('605', '瑞天速递', 'ZY_RTSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('606', '速达快递', 'ZY_SDKD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('607', '四方转运', 'ZY_SFZY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('608', '上腾快递', 'ZY_ST', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('609', '天际快递', 'ZY_TJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('610', '天马转运', 'ZY_TM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('611', '滕牛快递', 'ZY_TN', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('612', '太平洋快递', 'ZY_TPY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('613', '唐三藏转运', 'ZY_TSZ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('614', 'TWC转运世界', 'ZY_TWC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('615', '润东国际快线', 'ZY_RDGJ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('616', '同心快递', 'ZY_TX', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('617', '天翼快递', 'ZY_TY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('618', '德国海淘之家', 'ZY_DGHT', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('619', '德运网', 'ZY_DYW', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('620', '文达国际DCS', 'ZY_WDCS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('621', '同舟快递', 'ZY_TZH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('622', 'UCS合众快递', 'ZY_UCS', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('623', '星辰快递', 'ZY_XC', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('624', '先锋快递', 'ZY_XF', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('625', '云骑快递', 'ZY_YQ', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('626', '优晟速递', 'ZY_YSSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('627', '运淘美国', 'ZY_YTUSA', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('628', '至诚速递', 'ZY_ZCSD', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('629', '增速海淘', 'ZYZOOM', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('630', '中驰物流', 'ZH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('631', '中欧快运', 'ZO', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('632', '准实快运', 'ZSKY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('633', '中外速运', 'ZWSY', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('634', '郑州建华', 'ZZJH', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('635', '中通国际', 'ZTOGLOBAL', NULL, NULL, NULL, '1');
INSERT INTO `jiecheng_express` (`id`, `name`, `code`, `key`, `ali_key`, `r_datas`, `status`) VALUES ('636', '转运四方国际快递', 'ZYSFGJ', NULL, NULL, NULL, '1');


INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('agentExplain', '{\"open\": 0, \"level\": 0, \"become\": {\"type\": 0, \"condition\": {\"apply\": {\"isShow\": 0}, \"payOrder\": {\"orderCount\": 0, \"statistics\": 0}, \"buyCommodity\": {\"commodity\": [], \"statistics\": 0}, \"explainTotal\": {\"explain\": 0, \"statistics\": 0}}}, \"profit\": {\"first\": 0, \"third\": 0, \"second\": 0}, \"isAudit\": 0, \"titleImg\": \"\", \"inPurchasing\": 0, \"relationship\": 0, \"is_show_profit\": 0}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('aliSms', '{\"accessKeyId\": \"\", \"accessSecret\": \"\"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('customerService', '{\"wechat\": \"\", \"is_open\": 0, \"wechata\": \"\"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('integralBalance', '{\"share\": {\"new\": 0, \"old\": 0}, \"is_open\": 0, \"withdraw\": 0, \"deduction\": 0.01, \"daily_limit\": 0, \"is_free_fee\": 0, \"balance_cash\": 0.1, \"balance_text\": \"余额\", \"integral_text\": \"积分\", \"withdraw_type\": [], \"agent_withdraw\": 0, \"integral_upper\": 0, \"service_charge\": 5.5, \"withdraw_limit\": 1, \"balance_setting\": 0, \"free_fee_section\": [0, 0], \"withdraw_service\": 0, \"agent_is_free_fee\": 0, \"get_integral_type\": 0, \"merchant_withdraw\": 0, \"get_integral_ratio\": 0, \"agent_withdraw_type\": [], \"withdraw_limit_cash\": \"100.00\", \"agent_service_charge\": 5.5, \"agent_withdraw_limit\": 0, \"integral_upper_limit\": 0, \"merchant_is_free_fee\": 0, \"withdraw_type_outline\": [], \"agent_free_fee_section\": [0, 0], \"agent_withdraw_service\": 0, \"merchant_withdraw_type\": [], \"merchant_service_charge\": 5.5, \"merchant_withdraw_limit\": 0, \"agent_withdraw_limit_cash\": \"100.00\", \"merchant_free_fee_section\": [0, 0], \"merchant_withdraw_service\": 0, \"agent_withdraw_type_outline\": [], \"merchant_withdraw_limit_cash\": \"100.00\", \"merchant_withdraw_type_outline\": []}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('lottery', '{\"lottery_open\": 0, \"lottery_type\": 1, \"lottery_limit\": 1, \"lottery_money\": 0, \"superposition\": 1, \"lottery_number\": 1, \"lottery_config_id\": 0, \"lottery_style\": 1}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('mallBase', '{\"logo\": \"61465115.jpg\", \"remark\": \"商城未开启\", \"is_open\": 0, \"load_pic\": \"\", \"mall_name\": \"商城名称(可修改)\", \"deal_title\": \"\", \"is_handset\": 0, \"trolley_pic\": \"\", \"deal_content\": \"\", \"sell_out_pic\": \"\", \"show_big_pic\": 0, \"customer_service_type\": 0}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('mallContact', '{\"mobile1\": \" \", \"mobile2\": \"\", \"contacts\": \" \", \"web_jsapi\": \" \"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('mallPayMode', '{\"web\": {\"ali\": 0, \"sendPay\": 0, \"recharge\": 0, \"ali_pay_mode_id\": \"\"}, \"wechat\": {\"wx\": 0, \"ali\": 0, \"sendPay\": 0, \"recharge\": 0, \"wx_pay_mode_id\": \"\", \"ali_pay_mode_id\": \"\"}, \"wechata\": {\"wx\": 0, \"sendPay\": 0, \"recharge\": 0, \"wx_pay_mode_id\": \"\"}}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('mallShare', '{\"logo\": 0, \"title\": 0, \"describe\": 1, \"share_url\": 0, \"custom_logo\": \"\", \"custom_title\": \"\", \"custom_describe\": \"\", \"custom_share_url\": \"\"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('memberLevel', '{\"list\": [{\"data\": {\"name\": \"vipmember\", \"type\": {\"card_type\": 1, \"avart_type\": 1, \"card_type_open\": 1}, \"color\": {\"bg_color\": \"#41444e\", \"card_num_color\": \"#fff\", \"progress_color\": \"#846D4D\", \"card_bg_color_f\": \"#a98550\", \"card_bg_color_l\": \"#e2cc9d\", \"card_font_color\": \"#f9efd0\", \"card_rule_color\": \"#52555e\", \"card_grade_color\": \"#FFFFFF\", \"card_round_color_f\": \"#C4A674\", \"card_round_color_l\": \"#D9C191\", \"card_rule_btn_color\": \"#f7eecf\", \"card_bg_color_f_open\": \"#a98550\", \"card_bg_color_l_open\": \"#e2cc9d\", \"card_grade_color_open\": \"#FFFFFF\"}, \"is_open\": 1, \"card_img\": \"\", \"avart_img\": \"https://cube.elemecdn.com/3/7c/3ea6beec64369c2642b92c6726f1epng.png\", \"card_img_open\": \"\"}, \"name\": \"vipmember\", \"active\": false, \"visible\": false, \"visible1\": false}, {\"data\": {\"num\": 2, \"list\": [[{\"icon\": \"static/images/btns1.png\", \"text\": \"权益文字1\", \"card_bg\": \"#019CFE\", \"linkdata\": {\"id\": \"\", \"link\": \"\", \"name\": \"\", \"type\": \"\", \"login_type\": 1}, \"card_color\": \"#fff\"}]], \"name\": \"vipinterests\", \"rows\": 1, \"type\": 1, \"title\": {\"size\": 16, \"color\": \"#000\"}, \"width\": 60, \"radius\": {\"top\": 10, \"bottom\": 10}, \"bgcolor\": \"#FFFFFF\", \"padding\": {\"lr\": 24, \"top\": 16, \"bottom\": 8}, \"textcolor\": \"#565656\", \"style_type\": 1, \"currentIndex\": 0, \"card_font_size\": 16, \"content_bgcolor\": \"#fff\", \"content_padding\": {\"lr\": 24, \"top\": 16, \"bottom\": 8}}, \"name\": \"vipinterests\", \"active\": true, \"visible\": false, \"visible1\": false}, {\"data\": {\"bg\": \"#FFFFFF\", \"p_b\": 8, \"p_t\": 8, \"r_b\": 0, \"r_t\": 0, \"list\": [], \"name\": \"vipshangpinzu\", \"p_lr\": 12, \"type\": 0, \"limit\": 20, \"order\": \"comprehensive\", \"cateid\": 0, \"t_color\": \"#000000\", \"del_price\": {\"show\": 0, \"color\": \"#969696\", \"title\": \"\"}, \"list_type\": \"two\", \"pro_price\": {\"show\": 1, \"color\": \"#FF3C29\"}, \"sales_num\": {\"show\": 0, \"color\": \"#969696\", \"title\": \"\"}, \"show_type\": \"block\", \"vip_price\": {\"show\": 0}, \"add_button\": {\"icon\": \"el-sc-gouwuche2\", \"show\": false, \"color\": \"#F56C6C\"}, \"svip_price\": {\"show\": 0}}, \"name\": \"vipshangpinzu\", \"active\": false, \"visible\": false, \"visible1\": false}], \"is_open\": 1, \"is_price\": 0, \"page_config\": {\"nav_bg\": \"#41444E\", \"bg_type\": 1, \"bg_color\": \"#fff\", \"nav_name\": \"会员中心\", \"nav_type\": \"white\", \"nav_bgimg\": \"\", \"text_color\": \"#333333\"}}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('oss', '{\"bucket\": \"\", \"is_open\": 0, \"endpoint\": \"m\", \"oss_choice\": 1, \"accessKeyId\": \"\", \"accessKeySecret\": \"\"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('rightsSetting', '{\"refund_single\": 1, \"refund_explain\": \"\", \"is_rights_apply\": 1, \"rights_apply_time\": 7}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('signIn', '{\"info\": \"<p></p>\", \"type\": 1, \"style\": {}, \"value\": \"1\", \"signIn\": false, \"pattern\": \"day\", \"special\": [], \"is_remind\": false, \"continuity\": [], \"cumulative\": []}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('tradeSetting', '{\"invoice\": [], \"is_invoice\": 0, \"deal_enhance\": 0, \"audit_evaluate\": 1, \"order_end_time\": 7, \"order_evaluate\": 1, \"order_is_close\": 0, \"receiving_time\": 7, \"is_show_evaluate\": 1, \"order_close_time\": 20, \"is_auto_receiving\": 0, \"agent_get_money_time\": 0}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('wx', '{\"URL\": \"1\", \"Token\": \"1\", \"appid\": \" \", \"appsecret\": \" \", \"EncodingAESKey\": \"2\"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('wxa', '{\"appid\": \" \", \"appsecret\": \" \"}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('web', '{\"is_open\": 0}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('group', '{\"is_open\": 1, \"close_time\": 0}');
INSERT INTO `jiecheng_base_configuration`(`type`, `configuration`) VALUES ('seckill', '{\"close_time\": 0}');
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (1, '查看', '首页管理', '首页', 'read', 1, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (2, '查看', '商品管理', '分类管理', 'read', 2, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (3, '操作 ', '商品管理', '分类管理', 'update', 2, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (4, '查看', '商品管理', '商品', 'read', 3, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (5, '操作', '商品管理', '商品', 'update', 3, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (6, '查看', '商品管理', '商品审核', 'read', 4, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (7, '操作', '商品管理', '商品审核', 'update', 4, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (8, '查看', '商品管理', '运费模板', 'read', 5, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (9, '操作', '商品管理', '运费模板', 'update', 5, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (10, '查看', '商品管理', '退货地址', 'read', 6, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (11, '操作', '商品管理', '退货地址', 'update', 6, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (12, '查看', '商品管理', '物流设置', 'read', 7, '0', b'1', '2021-06-02 14:11:06', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (13, '操作', '商品管理', '物流设置', 'update', 7, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (14, '查看', '订单管理', '订单', 'read', 8, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (15, '操作', '订单管理', '订单', 'update', 8, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (16, '发货', '订单管理', '订单', 'deliver', 8, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (17, '退款', '订单管理', '订单', 'refund', 8, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (18, '删除', '订单管理', '订单', 'delete', 8, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (19, '查看', '订单管理', '订单评论', 'read', 9, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (20, '操作', '订单管理', '订单评论', 'update', 9, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (21, '查看', '订单管理', '维权订单', 'read', 10, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (22, '操作', '订单管理', '维权订单', 'update', 10, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (23, '查看', '订单管理', '批量发货', 'read', 11, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (24, '操作', '订单管理', '批量发货', 'update', 11, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (25, '查看', '订单管理', '交易设置', 'read', 12, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (26, '操作', '订单管理', '交易设置', 'update', 12, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (27, '查看', '订单管理', '维权设置', 'read', 13, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (28, '操作', '订单管理', '维权设置', 'update', 13, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (29, '查看', 'DIY', '店铺装修', 'read', 14, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (30, '操作', 'DIY', '店铺装修', 'update', 14, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (31, '查看', 'DIY', '模板中心', 'read', 15, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (32, '操作', 'DIY', '模板中心', 'update', 15, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (33, '查看', 'DIY', '底部菜单', 'read', 16, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (34, '操作', 'DIY', '底部菜单', 'update', 16, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (35, '查看', '用户管理', '用户', 'read', 17, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (36, '操作', '用户管理', '用户', 'update', 17, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (37, '查看', '财务管理', '提现申请', 'read', 18, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (38, '操作', '财务管理', '提现申请', 'update', 18, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (39, '查看', '财务管理', '金额明细', 'read', 19, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (40, '操作', '财务管理', '金额明细', 'update', 19, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (41, '查看', '财务管理', '积分明细', 'read', 20, '0', b'1', '2021-06-02 14:11:07', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (42, '查看', '财务管理', '积分余额配置', 'read', 21, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (43, '操作', '财务管理', '积分余额配置', 'update', 21, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (44, '查看', '财务管理', '支付方式配置', 'read', 22, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (45, '操作', '财务管理', '支付方式配置', 'update', 22, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (46, '查看', '财务管理', '支付模板', 'read', 23, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (47, '操作', '财务管理', '支付模板', 'update', 23, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (48, '查看', '分销管理', '分销商', 'read', 24, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (49, '操作', '分销管理', '分销商', 'update', 24, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (50, '查看', '分销管理', '申请列表', 'read', 25, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (51, '操作', '分销管理', '申请列表', 'update', 25, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (52, '查看', '分销管理', '分销订单', 'read', 26, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (53, '查看', '分销管理', '分销设置', 'read', 27, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (54, '操作', '分销管理', '分销设置', 'update', 27, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (55, '查看', '多端管理', '多端设置', 'read', 28, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (56, '操作', '多端管理', '多端设置', 'update', 28, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (57, '查看', '系统管理', '基础设置', 'read', 29, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (58, '操作', '系统管理', '基础设置', 'update', 29, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (59, '查看', '系统管理', '分享设置', 'read', 30, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (60, '操作', '系统管理', '分享设置', 'update', 30, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (61, '查看', '系统管理', '公告管理', 'read', 31, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (62, '操作', '系统管理', '公告管理', 'update', 31, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (63, '查看', '系统管理', '联系我们', 'read', 32, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (64, '操作', '系统管理', '联系我们', 'update', 32, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (65, '查看', '系统管理', '协议管理', 'read', 33, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (66, '操作', '系统管理', '协议管理', 'update', 33, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (67, '查看', '系统管理', '角色管理', 'read', 34, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (68, '操作', '系统管理', '角色管理', 'update', 34, '0', b'1', '2021-06-02 14:11:08', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (69, '查看', '系统管理', '管理员管理', 'read', 35, '0', b'1', '2021-06-02 14:11:09', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (70, '操作', '系统管理', '管理员管理', 'update', 35, '0', b'1', '2021-06-02 14:11:09', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (71, '查看', '应用管理', '应用列表', 'read', 36, '0', b'1', '2021-06-02 14:11:09', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (72, '查看', '应用管理', '新人送礼', 'read', 37, '1', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (73, '操作', '应用管理', '新人送礼', 'update', 37, '1', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (74, '操作', '应用管理', '领取列表', 'read', 55, '1', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (75, '查看', '应用管理', '文章', 'read', 38, '2', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (76, '操作', '应用管理', '文章', 'update', 38, '2', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (77, '查看', '应用管理', '优惠券', 'read', 39, '3', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (78, '操作', '应用管理', '优惠券', 'update', 39, '3', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (79, '审核', '应用管理', '优惠券', 'audit', 39, '3', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (80, '查看', '应用管理', '消息通知', 'read', 40, '4', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (81, '操作', '应用管理', '消息通知', 'update', 40, '4', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (82, '查看', '应用管理', '签到', 'read', 41, '5', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (83, '操作', '应用管理', '签到', 'update', 41, '5', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (84, '查看', '应用管理', '积分规划', 'read', 42, '6', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (85, '操作', '应用管理', '积分规划', 'update', 42, '6', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (86, '查看', '应用管理', '消费返积分', 'read', 43, '15', b'1', '2021-06-02 14:11:09', '2022-01-25 14:30:32', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (87, '操作', '应用管理', '消费返积分', 'update', 43, '15', b'1', '2021-06-02 14:11:09', '2022-01-25 14:30:34', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (88, '查看', '应用管理', '抵扣设置', 'read', 44, '7', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (89, '操作', '应用管理', '抵扣设置', 'update', 44, '7', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (90, '查看', '应用管理', '远程附件', 'read', 45, '8', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (91, '操作', '应用管理', '远程附件', 'update', 45, '8', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (92, '查看', '应用管理', '在线客服', 'read', 46, '9', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (93, '操作', '应用管理', '在线客服', 'update', 46, '9', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (94, '查看', '应用管理', '小票助手', 'read', 47, '10', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (95, '操作', '应用管理', '小票助手', 'update', 47, '10', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (96, '查看', '应用管理', '门店', 'read', 48, '15', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (97, '操作', '应用管理', '门店', 'update', 48, '15', b'1', '2021-06-02 14:11:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (98, '查看', '应用管理', '商户', 'read', 49, '12', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (99, '操作', '应用管理', '商户', 'update', 49, '12', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (100, '查看', '应用管理', '商户申请', 'read', 50, '12', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (101, '操作', '应用管理', '商户申请', 'update', 50, '12', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (102, '查看', '应用管理', '自定义表单', 'read', 51, '13', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (103, '操作', '应用管理', '自定义表单', 'update', 51, '13', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (104, '查看', '应用管理', '快递助手', 'read', 52, '14', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (105, '操作', '应用管理', '快递助手', 'update', 52, '14', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (108, '查看', '财务管理', '商户明细', 'read', 57, '12', b'1', '2021-06-26 13:50:09', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (109, '查看', '商品管理', '关键字管理', 'read', 56, '0', b'1', '2021-06-26 13:50:57', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (110, '操作', '商品管理', '关键字管理', 'update', 56, '0', b'1', '2021-06-26 13:51:01', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (111, '查看', '财务管理', '入账详情', 'read', 58, '0', b'1', '2021-07-02 14:12:44', '2021-09-16 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (112, '查看', '应用管理', '九宫格抽奖', 'read', 59, '17', b'1', '2021-09-03 16:49:25', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (113, '操作', '应用管理', '九宫格抽奖', 'update', 59, '17', b'1', '2021-09-03 16:49:52', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (114, '查看', '应用管理', '刮刮乐抽奖', 'read', 60, '18', b'1', '2021-09-03 16:49:25', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (115, '操作', '应用管理', '刮刮乐抽奖', 'update', 60, '18', b'1', '2021-09-03 16:49:52', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (116, '操作', '应用管理', '积分商城', 'read', 61, '19', b'1', '2021-12-08 13:51:08', '2021-12-08 13:51:10', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (117, '查看', '应用管理', '积分商城', 'update', 61, '19', b'1', '2021-12-08 13:51:52', '2021-12-23 10:37:56', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (118, '查看', '应用管理', '同城配送', 'read', 62, '16', b'1', '2021-12-23 11:15:46', '2022-01-25 14:25:44', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (119, '操作', '应用管理', '同城配送', 'update', 62, '16', b'1', '2021-12-23 11:15:46', '2022-01-25 14:26:22', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (120, '查看', '应用管理', '猜你喜欢', 'read', 63, '21', b'1', '2021-12-23 16:15:46', '2022-01-25 14:28:54', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (121, '操作', '应用管理', '猜你喜欢', 'update', 63, '21', b'1', '2021-12-23 16:15:46', '2022-01-25 14:28:57', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (122, '查看', '应用管理', '会员中心', 'read', 64, '20', b'1', '2021-12-23 16:15:46', '2022-01-25 14:29:20', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (123, '操作', '应用管理', '会员中心', 'update', 64, '20', b'1', '2021-12-23 16:15:46', '2022-01-25 14:29:24', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (124, '查看', '系统管理', '頁面管理', 'read', 65, '0', b'1', '2022-02-07 14:11:09', '2022-02-07 13:33:42', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (125, '查看', '系统管理', '抽奖设置', 'read', 66, '0', b'1', '2022-01-04 17:15:05', '2022-01-04 17:15:41', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (126, '操作', '系统管理', '抽奖设置', 'update', 66, '0', b'1', '2022-01-04 17:15:05', '2022-01-04 17:15:41', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (127, '查看', '应用管理', '微信客服', 'read', 67, '22', b'1', '2022-02-26 13:00:00', '2022-02-26 16:20:24', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (128, '操作', '应用管理', '微信客服', 'update', 67, '22', b'1', '2022-02-26 13:00:00', '2022-02-26 16:20:27', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (129, '查看', 'DIY', '悬浮菜单', 'read', 68, '0', b'1', '2022-02-26 14:00:00', '2022-02-26 16:20:30', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (130, '操作', 'DIY', '悬浮菜单', 'update', 68, '0', b'1', '2022-02-26 14:00:00', '2022-02-26 16:20:33', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (131, '查看', 'DIY', '主题色', 'read', 69, '0', b'1', '2022-03-14 16:19:00', '2022-03-14 16:19:00', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (132, '操作', 'DIY', '主题色', 'update', 69, '0', b'1', '2022-03-14 16:19:00', '2022-03-14 16:19:00', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (135, '查看', '分销管理', '分销等级', 'read', 70, '0', b'1', '2022-04-12 11:24:00', '2022-04-12 11:24:00', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (136, '操作', '分销管理', '分销等级', 'update', 70, '0', b'1', '2022-04-12 11:24:00', '2022-04-12 11:24:00', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (137, '查看', '应用管理', '系统表单', 'read', 71, '23', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (138, '操作', '应用管理', '系统表单', 'update', 71, '23', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (141, '查看', '应用管理', '商品采集', 'read', 74, '26', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (142, '操作', '应用管理', '商品采集', 'update', 74, '26', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (143, '查看', '应用管理', '小程序直播', 'read', 75, '27', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (144, '操作', '应用管理', '小程序直播', 'update', 75, '27', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (145, '查看', '应用管理', '注册企业小程序', 'read', 76, '28', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (146, '操作', '应用管理', '注册企业小程序', 'update', 76, '28', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (150, '查看', '系统管理', '系统操作日志', 'read', 77, '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (151, '查看', '应用管理', '限时拼团', 'read', 78, '24', b'1', '2021-06-18 14:11:10', '2022-06-18 15:51:33', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (152, '操作', '应用管理', '限时拼团', 'update', 78, '24', b'1', '2021-06-18 14:11:10', '2022-06-18 15:51:39', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (153, '查看', '应用管理', '秒杀抢购', 'read', 79, '25', 1, '2021-07-18 14:11:10', '2022-07-18 15:51:33', NULL);
INSERT INTO `jiecheng_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (154, '操作', '应用管理', '秒杀抢购', 'update', 79, '25', 1, '2021-07-18 14:11:10', '2022-07-07 09:46:19', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (155, '查看', '应用管理', '分销达标', 'read', '80', '30', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (156, '操作', '应用管理', '分销达标', 'update', '80', '30', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (157, '查看', '应用管理', '分销考核', 'read', '81', '31', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (158, '操作', '应用管理', '分销考核', 'update', '81', '31', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (159, '查看', '应用管理', '分销考核提现', 'read', '82', '31', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (160, '操作', '应用管理', '分销考核提现', 'update', '82', '31', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (161, '查看', '应用管理', '小程序外部跳转链接', 'read', '83', '32', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (162, '操作', '应用管理', '小程序外部跳转链接', 'update', '83', '32', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (163, '查看', '应用管理', '商品预售', 'read', 84, '34', b'1', '2022-07-28 17:00:00', '2022-07-28 17:00:00', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (164, '操作', '应用管理', '商品预售', 'update', 84, '34', b'1', '2022-07-28 17:00:00', '2022-07-28 17:00:00', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (165, '查看', '系统管理', '截留口令', 'read', 85, '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (166, '操作', '系统管理', '截留口令', 'update', 85, '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (167, '查看', '应用管理', '资产转赠', 'read', '86', '35', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (168, '操作', '应用管理', '资产转赠', 'update', '86', '35', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (169, '查看', '应用管理', '充值奖励', 'read', 87, '36', b'1', '2021-06-02 14:11:10', '2022-08-03 15:20:58', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (170, '操作', '应用管理', '充值奖励', 'update', 87, '36', b'1', '2021-06-02 14:11:10', '2022-08-03 15:21:02', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('171', '查看', '系统管理', '插件列表', 'read', '88', '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('172', '操作', '系统管理', '插件列表', 'update', '88', '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('173', '查看', '系统管理', '版权', 'read', '89', '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);
INSERT INTO `jiecheng_rule` (`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('174', '操作', '系统管理', '版权', 'update', '89', '0', b'1', '2021-06-02 14:11:10', '2021-12-07 14:27:49', NULL);

INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (1, 1, '/shop/statistic/consumption_ranking', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (2, 1, '/shop/statistic/count', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (3, 1, '/shop/statistic/unified/count', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (4, 1, '/shop/statistic/index', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (5, 1, '/shop/statistic/sales_ranking', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (6, 1, '/shop/statistic/total', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (7, 2, '/madmin/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (8, 3, '/madmin/classify', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (9, 3, '/madmin/classify/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (10, 3, '/madmin/classify/level', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (11, 4, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (12, 4, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (13, 4, '/shop/freight/temp', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (14, 4, '/shop/spu/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (15, 4, '/shop/spu/sellout', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (16, 4, '/shop/integral/open', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (17, 5, '/shop/spu', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (18, 5, '/shop/spu/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (19, 5, '/shop/spu', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (20, 5, '/shop/spu', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (21, 5, '/shop/spu/inventory/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (22, 5, '/madmin/spu/sort/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (23, 6, '/madmin/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (24, 6, '/shop/freight/temp', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (25, 6, '/madmin/spu/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (26, 7, '/madmin/spu/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (27, 8, '/shop/freight/temp/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (28, 8, '/shop/freight/temp/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (29, 8, '/shop/freight/express', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (30, 9, '/shop/freight/temp', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (31, 9, '/shop/freight/temp/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (32, 9, '/shop/freight/temp/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (33, 10, '/shop/return/address/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (34, 10, '/shop/return/address/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (35, 11, '/shop/return/address', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (36, 11, '/shop/return/address/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (37, 11, '/shop/return/address/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (38, 12, '/madmin/configuration/alicloudExpress', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (39, 13, '/madmin/configuration/alicloudExpress', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (40, 13, '/madmin/configuration/alicloudExpress', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (41, 14, '/shop/receipts/printer/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (42, 14, '/shop/receipts/template/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (43, 14, '/shop/order/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (44, 14, '/shop/order/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (45, 15, '/shop/receipts/create', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (46, 16, '/shop/freight/express', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (47, 16, '/shop/order/saveOrderStatus', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (48, 16, '/shop/order/storeList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (49, 17, '/shop/order/drawback', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (50, 18, '/shop/order/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (51, 19, '/shop/evaluate/evaluateList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (52, 20, '/shop/evaluate/reply', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (53, 20, '/shop/evaluate/examine', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (54, 20, '/shop/evaluate/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (55, 21, '/shop/aftersale/afterSaleList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (56, 21, '/shop/aftersale/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (57, 21, '/shop/freight/express', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (58, 21, '/shop/order/storeList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (59, 21, '/shop/return/address/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (60, 22, '/shop/aftersale/upAfterState', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (61, 22, '/shop/order/saveOrderStatus', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (62, 22, '/shop/order/saveConsignee', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (63, 23, '/shop/order/export', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (64, 24, '/shop/order/exportData', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (65, 25, '/madmin/configuration/tradeSetting', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (66, 26, '/madmin/configuration/tradeSetting', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (67, 26, '/madmin/configuration/tradeSetting', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (68, 27, '/madmin/configuration/rightsSetting', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (69, 28, '/madmin/configuration/rightsSetting', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (70, 28, '/madmin/configuration/rightsSetting', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (71, 29, '/madmin/page/pageList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (72, 29, '/madmin/page/preview', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (73, 29, '/madmin/page/renovation', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (74, 29, '/madmin/page/getMyTemplateInfo', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (75, 29, '/madmin/page/market', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (76, 29, '/madmin/page/myTemplateList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (77, 30, '/madmin/page/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (78, 30, '/madmin/page/enable', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (79, 30, '/madmin/page/saveSource', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (80, 30, '/madmin/page/base64Up', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (81, 30, '/madmin/page/create', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (82, 30, '/madmin/page/upRenovation', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (83, 30, '/madmin/page/saveTemplate', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (84, 30, '/madmin/page/startAdvert', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (85, 30, '/madmin/page/renovation', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (86, 31, '/madmin/page/market', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (87, 31, '/madmin/page/myTemplateList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (88, 32, '/madmin/page/delMytemplate/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (89, 32, '/madmin/page/base64Up', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (90, 32, '/madmin/page/create', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (91, 32, '/madmin/page/upRenovation', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (92, 32, '/madmin/page/saveTemplate', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (93, 32, '/madmin/page/renovation', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (94, 32, '/madmin/page/getMyTemplateInfo', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (95, 33, '/madmin/fitment/bottom_menu/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (96, 33, '/madmin/fitment/bottom_menu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (97, 34, '/madmin/fitment/bottom_menu', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (98, 34, '/madmin/fitment/bottom_menu/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (99, 34, '/madmin/fitment/bottom_menu/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (100, 35, '/madmin/user/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (101, 36, '/madmin/user/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (102, 36, '/madmin/user/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (103, 36, '/shop/statistic/user/total/<user_id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (104, 36, '/madmin/user/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (105, 36, '/madmin/user/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (106, 36, '/madmin/user/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (107, 36, '/madmin/user/cash/integral/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (108, 36, '/madmin/recharge/record/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (109, 36, '/madmin/integral/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (110, 36, '/madmin/integral/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (111, 36, '/madmin/user/cash/recharge/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (112, 37, '/madmin/merchant/withdraw/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (113, 37, '/madmin/merchant/withdraw/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (114, 37, '/madmin/agent/withdraw/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (115, 37, '/madmin/agent/withdraw/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (116, 37, '/madmin/user/withdraw/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (117, 37, '/madmin/user/withdraw/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (118, 38, '/madmin/merchant/withdraw/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (119, 38, '/madmin/agent/withdraw/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (120, 38, '/madmin/user/withdraw/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (121, 38, '/madmin/merchant/withdraw/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (122, 38, '/madmin/agent/withdraw/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (123, 38, '/madmin/user/withdraw/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (124, 39, '/madmin/recharge/record/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (125, 40, '/madmin/user/cash/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (126, 40, '/madmin/user/cash/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (127, 41, '/madmin/integral/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (128, 42, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (129, 43, '/madmin/configuration/integralBalance', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (130, 43, '/madmin/configuration/integralBalance', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (131, 44, '/madmin/configuration/mallPayMode', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (132, 44, '/madmin/pay/mode', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (133, 45, '/madmin/configuration/mallPayMode', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (134, 45, '/madmin/configuration/mallPayMode', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (135, 46, '/madmin/pay/mode/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (136, 46, '/madmin/pay/mode/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (137, 47, '/madmin/pay/mode/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (138, 47, '/madmin/pay/mode/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (139, 47, '/madmin/pay/mode/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (140, 48, '/madmin/agent/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (141, 48, '/madmin/user/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (142, 48, '/madmin/agent/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (143, 49, '/madmin/agent/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (144, 49, '/madmin/agent', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (145, 49, '/madmin/agent/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (146, 49, '/madmin/agent/commission/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (147, 49, '/madmin/agent/commission/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (148, 50, '/madmin/agent/apply/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (149, 51, '/madmin/agent/apply/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (150, 51, '/madmin/agent/apply/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (151, 52, '/madmin/agent/order', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (152, 53, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (153, 54, '/madmin/configuration/agentExplain', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (154, 54, '/madmin/configuration/agentExplain', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (155, 55, '/madmin/configuration/wxa', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (156, 55, '/madmin/configuration/wx', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (157, 55, '/madmin/wxurl', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (158, 56, '/madmin/configuration/wxa', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (159, 56, '/madmin/configuration/wxa', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (160, 56, '/madmin/configuration/wx', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (161, 56, '/madmin/configuration/wx', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (162, 56, '/madmin/wxa/config/login', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (163, 56, '/madmin/wxa/config/send', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (164, 57, '/madmin/configuration/mallBase', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (165, 58, '/madmin/configuration/mallBase', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (166, 58, '/madmin/configuration/mallBase', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (167, 59, '/madmin/configuration/mallShare', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (168, 60, '/madmin/configuration/mallShare', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (169, 60, '/madmin/configuration/mallShare', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (170, 61, '/shop/notice/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (171, 61, '/shop/notice/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (172, 62, '/shop/notice/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (173, 62, '/shop/notice/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (174, 62, '/shop/notice', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (175, 63, '/madmin/configuration/mallContact', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (176, 64, '/madmin/configuration/mallContact', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (177, 64, '/madmin/configuration/mallContact', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (178, 65, '/madmin/protocol/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (179, 65, '/madmin/protocol/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (180, 66, '/madmin/protocol/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (181, 66, '/madmin/protocol/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (182, 66, '/madmin/protocol', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (183, 67, '/madmin/auth/role/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (184, 67, '/madmin/auth/role/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (185, 68, '/madmin/auth/role/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (186, 68, '/madmin/auth/role/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (187, 68, '/madmin/auth/role', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (188, 68, '/madmin/auth/rule/value', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (189, 69, '/madmin/admin/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (190, 69, '/madmin/admin/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (191, 70, '/madmin/admin', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (192, 70, '/madmin/admin/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (193, 70, '/madmin/admin/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (194, 70, '/madmin/auth/role', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (195, 71, '/madmin/addons', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (196, 72, '/madmin/new_kid_gift/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (197, 72, '/madmin/new_kid_gift/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (198, 73, '/madmin/new_kid_gift', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (199, 73, '/madmin/new_kid_gift/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (200, 73, '/madmin/new_kid_gift/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (201, 74, '/madmin/new_kid_gift/receive', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (202, 75, '/madmin/article/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (203, 75, '/madmin/article/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (204, 75, '/madmin/article/type', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (205, 75, '/madmin/article/type/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (206, 76, '/madmin/article/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (207, 76, '/madmin/article/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (208, 76, '/madmin/article', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (209, 76, '/madmin/article/type/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (210, 76, '/madmin/article/type/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (211, 76, '/madmin/article/type', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (212, 77, '/shop/coupon/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (213, 77, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (214, 77, '/shop/user/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (215, 77, '/madmin/configuration/couponExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (216, 77, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (217, 77, '/madmin/user/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (218, 78, '/shop/coupon/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (219, 78, '/shop/coupon', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (220, 78, '/shop/coupon/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (221, 78, '/shop/coupon/send/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (222, 78, '/madmin/configuration/couponExplain', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (223, 78, '/madmin/configuration/couponExplain', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (224, 79, '/madmin/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (225, 79, '/madmin/coupon/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (226, 79, '/madmin/coupon/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (227, 80, '/madmin/msg/config/scene', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (228, 80, '/madmin/msg/config', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (229, 80, '/madmin/sms/ali/template/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (230, 80, '/madmin/sms/ali/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (231, 80, '/madmin/sms/ali/template', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (232, 80, '/madmin/sms/wechat/template/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (233, 80, '/madmin/sms/wechat/template', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (234, 80, '/madmin/sms/wechat/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (235, 80, '/madmin/configuration/aliSms', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (236, 80, '/madmin/configuration/wx', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (237, 80, '/madmin/configuration/wxa', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (238, 81, '/madmin/msg/config/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (239, 81, '/madmin/msg/config', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (240, 81, '/madmin/sms/ali/template/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (241, 81, '/madmin/sms/ali/template/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (242, 81, '/madmin/sms/ali/template', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (243, 81, '/madmin/sms/wechat/template/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (244, 81, '/madmin/sms/wechat/template', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (245, 81, '/madmin/sms/wechat/template/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (246, 81, '/madmin/configuration/aliSms', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (247, 81, '/madmin/configuration/aliSms', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (248, 81, '/madmin/configuration/wx', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (249, 81, '/madmin/configuration/wx', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (250, 81, '/madmin/configuration/wxa', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (251, 81, '/madmin/configuration/wxa', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (252, 82, '/madmin/signin', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (253, 83, '/madmin/signin', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (254, 84, '/madmin/integral/share/find', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (255, 85, '/madmin/integral/share/save', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (256, 86, '/madmin/integral', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (257, 87, '/madmin/integral', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (258, 88, '/madmin/integral', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (259, 89, '/madmin/integral', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (260, 90, '/madmin/configuration/oss', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (261, 91, '/madmin/configuration/oss', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (262, 91, '/madmin/configuration/oss', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (263, 92, '/madmin/configuration/customerService', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (264, 93, '/madmin/configuration/customerService', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (265, 93, '/madmin/configuration/customerService', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (266, 94, '/shop/receipts/template/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (267, 94, '/shop/receipts/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (268, 95, '/shop/receipts/template/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (269, 95, '/shop/receipts/template/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (270, 95, '/shop/receipts/template', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (271, 95, '/shop/receipts/printer', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (272, 95, '/shop/receipts/printer/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (273, 95, '/shop/receipts/printer/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (274, 95, '/shop/receipts/printer/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (275, 95, '/shop/receipts/printer/default/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (276, 95, '/shop/receipts/task/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (277, 95, '/shop/receipts/task/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (278, 95, '/shop/receipts/task', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (279, 95, '/shop/receipts/task/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (280, 95, '/shop/receipts/task/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (281, 96, '/shop/store/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (282, 96, '/shop/store/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (283, 96, '/shop/store/assistant/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (284, 96, '/shop/store/assistant/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (285, 97, '/shop/store/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (286, 97, '/shop/store/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (287, 97, '/shop/store', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (288, 97, '/shop/store/assistant', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (289, 97, '/shop/store/assistant/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (290, 97, '/shop/store/assistant/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (291, 98, '/madmin/merchant/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (292, 98, '/madmin/merchant/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (293, 98, '/madmin/user/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (294, 99, '/madmin/merchant/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (295, 99, '/madmin/merchant/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (296, 99, '/madmin/merchant', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (297, 99, '/madmin/merchant/apply/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (298, 99, '/madmin/user/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (299, 100, '/madmin/merchant/apply/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (300, 101, '/madmin/merchant/apply/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (301, 101, '/madmin/merchant/apply/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (302, 101, '/madmin/merchant', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (303, 102, '/madmin/form/template/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (304, 102, '/madmin/form/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (305, 103, '/madmin/form/template/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (306, 103, '/madmin/form/answer/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (307, 103, '/madmin/form/template', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (308, 103, '/madmin/form/template/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (309, 104, '/shop/order/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (310, 104, '/shop/receipts/template/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (311, 104, '/shop/express_bill/template/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (312, 104, '/shop/receipts/printer/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (313, 104, '/shop/invoice', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (314, 105, '/shop/express_bill/create', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (315, 105, '/shop/invoice/print', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (316, 105, '/shop/express_bill/template/default/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (317, 105, '/shop/receipts/create', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (318, 105, '/shop/express_bill/template/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (319, 105, '/shop/express_bill/config', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (320, 105, '/shop/express_bill/config', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (321, 105, '/shop/express_bill/template', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (322, 105, '/shop/express_bill/template/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (323, 105, '/shop/express_bill/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (324, 105, '/shop/invoice', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (325, 105, '/shop/invoice/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (326, 105, '/shop/invoice/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (327, 105, '/shop/invoice/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (328, 106, '/shop/pic/picList', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (329, 106, '/shop/pic/groupList', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (330, 107, '/shop/pic/addPic', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (331, 69, '/madmin/auth/role', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (332, 68, '/madmin/auth/role', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (333, 48, '/madmin/agent/level', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (334, 104, '/shop/invoice', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (335, 108, '/madmin/user/cash/merchant', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (336, 109, '/madmin/search/keyword', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (337, 110, '/madmin/search/keyword', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (338, 110, '/madmin/search/keyword/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (339, 110, '/madmin/search/keyword/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (340, 75, '/shop/pic/addPic', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (341, 75, '/shop/pic/addGroup', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (342, 75, '/shop/pic/picList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (343, 75, '/shop/pic/groupList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (344, 75, '/shop/pic/editGroup', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (345, 75, '/shop/pic/delPic', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (346, 75, '/shop/pic/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (347, 76, '/shop/pic/addPic', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (348, 76, '/shop/pic/addGroup', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (349, 76, '/shop/pic/picList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (350, 76, '/shop/pic/groupList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (351, 76, '/shop/pic/editGroup', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (352, 76, '/shop/pic/delPic', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (353, 76, '/shop/pic/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (354, 98, '/shop/pic/addPic', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (355, 98, '/shop/pic/addGroup', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (356, 98, '/shop/pic/picList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (357, 98, '/shop/pic/groupList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (358, 98, '/shop/pic/editGroup', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (359, 98, '/shop/pic/delPic', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (360, 98, '/shop/pic/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (361, 99, '/shop/pic/addPic', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (362, 99, '/shop/pic/addGroup', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (363, 99, '/shop/pic/picList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (364, 99, '/shop/pic/groupList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (365, 99, '/shop/pic/editGroup', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (366, 99, '/shop/pic/delPic', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (367, 99, '/shop/pic/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (368, 78, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (369, 78, '/shop/configuration/<type>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (370, 78, '/shop/configuration/<type>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (371, 78, '/shop/configuration/<type>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (372, 95, '/shop/store', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (373, 105, '/madmin/page/base64up', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (374, 4, '/shop/stroe', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (375, 5, '/shop/store', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (376, 30, '/madmin/page/advert', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (377, 29, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (378, 29, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (379, 29, '/madmin/configuration/signIn', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (380, 29, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (381, 29, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (382, 29, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (383, 30, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (384, 30, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (385, 30, '/madmin/configuration/signIn', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (386, 30, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (387, 30, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (388, 30, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (389, 55, '/madmin/wxurl/preview', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (390, 59, '/madmin/page/pageList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (391, 59, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (392, 59, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (393, 59, '/madmin/configuration/signIn', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (394, 59, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (395, 59, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (396, 59, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (397, 61, '/madmin/page/pageList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (398, 61, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (399, 61, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (400, 61, '/madmin/configuration/signIn', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (401, 61, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (402, 61, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (403, 61, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (404, 53, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (405, 1, '/shop/statistic/exportUser', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (406, 1, '/shop/statistic/exportCommodity', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (407, 111, '/madmin/merchant/settlement/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (408, 34, '/madmin/configuration/integralBalance', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (409, 34, '/madmin/configuration/agentExplain', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (410, 34, '/madmin/configuration/signIn', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (411, 34, '/shop/spu/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (412, 34, '/shop/classify', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (413, 34, '/shop/coupon/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (414, 34, '/madmin/page/pageList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (415, 36, '/shop/statistic/user/total/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (416, 95, '/shop/receipts/printer/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (417, 103, '/madmin/page/base64Up', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (418, 105, '/shop/freight/express', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (419, 78, '/shop/configuration', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (420, 31, '/madmin/page/classList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (421, 30, '/madmin/page/classList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (422, 30, '/madmin/page/getTemplateInfo', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (423, 30, '/madmin/page/saveMyTemplate', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (424, 4, '/shop/spu/wechata/preview/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (425, 51, '/madmin/agent', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (426, 111, '/madmin/merchant/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (427, 29, '/madmin/form/template/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (428, 5, '/shop/spu/reaudit/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (429, 29, '/madmin/form/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (430, 56, '/madmin/wxa/config/preview', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (431, 29, '/madmin/merchant/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (432, 31, '/madmin/merchant/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (433, 29, '/madmin/merchant/commodity', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (434, 30, '/madmin/merchant/commodity', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (435, 112, '/madmin/lottery/config/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (436, 112, '/madmin/lottery/config/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (437, 113, '/madmin/lottery/config', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (438, 113, '/madmin/lottery/config/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (439, 113, '/madmin/lottery/config/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (440, 113, '/madmin/lottery/config/open/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (441, 112, '/madmin/lottery/log/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (442, 112, '/madmin/lottery/log/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (443, 114, '/madmin/lottery/receiving', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (444, 114, '/madmin/lottery/config/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (445, 115, '/madmin/lottery/config/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (446, 115, '/madmin/lottery/config', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (447, 115, '/madmin/lottery/config/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (448, 115, '/madmin/lottery/config/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (449, 115, '/madmin/lottery/config/open/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (450, 114, '/madmin/lottery/log/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (451, 114, '/madmin/lottery/log/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (452, 115, '/madmin/lottery/receiving', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (453, 117, '/madmin/integral/shop/goods/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (454, 117, '/madmin/integral/shop/goods/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (455, 117, '/madmin/integral/shop/order/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (456, 117, '/madmin/integral/shop/order/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (457, 117, '/madmin/integral/shop/classify/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (458, 117, '/madmin/integral/shop/classify/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (459, 116, '/madmin/integral/shop/goods', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (460, 116, '/madmin/integral/shop/goods/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (461, 116, '/madmin/integral/shop/goods/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (462, 116, '/madmin/integral/shop/order/refund/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (463, 116, '/madmin/integral/shop/order/deliver/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (464, 116, '/madmin/integral/shop/order/receiving/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (465, 116, '/madmin/integral/shop/order/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (466, 116, '/madmin/integral/shop/order/update/deliver/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (467, 116, '/madmin/integral/shop/goods', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (468, 116, '/madmin/integral/shop/goods/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (469, 116, '/madmin/integral/shop/goods/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (470, 116, '/madmin/integral/shop/classify', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (471, 116, '/madmin/integral/shop/classify/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (472, 118, '/shop/store/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (473, 118, '/shop/distribution/read/<store_id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (474, 119, '/shop/distribution/save', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (475, 120, '/madmin/pick/get_config', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (476, 121, '/madmin/pick/save', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (477, 122, '/madmin/membership/level/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (478, 122, '/madmin/membership/level/content/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (479, 122, '/madmin/configuration/memberLevel', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (480, 123, '/shop/pic/addPic', 'POST', '图片');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (481, 123, '/shop/pic/addGroup', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (482, 123, '/shop/pic/picList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (483, 123, '/shop/pic/groupList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (484, 123, '/shop/pic/editGroup', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (485, 123, '/shop/pic/delPic', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (486, 123, '/shop/pic/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (487, 122, '/madmin/membership/level/content/<id>', 'GET', '会员权益查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (488, 122, '/madmin/membership/record', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (489, 123, '/madmin/membership/level', 'POST', '新增');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (490, 123, '/madmin/membership/level/<id>', 'PUT', '启用／编辑');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (491, 122, '/madmin/membership/level/<id>', 'GET', '会员列表查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (492, 123, '/madmin/membership/level/<id>', 'DELETE', '删除');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (493, 123, '/madmin/configuration/memberLevel', 'PUT', '会员中心编辑');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (494, 123, '/madmin/membership/level/content/<id>', 'PUT', '会员权益编辑');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (495, 125, '/madmin/configuration/lottery', 'GET', '抽奖设置');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (496, 126, '/madmin/configuration/lottery', 'PUT', '抽奖设置保存');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (497, 35, '/madmin/membership/level/list', 'POST', '用戶詳情');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (498, 124, '/madmin/page/pageList', 'GET', '页面管理');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (499, 4, '/madmin/membership/level/list', 'POST', '用戶詳情');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (500, 127, '/madmin/wx/customer', 'GET', '客服列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (501, 127, '/madmin/wx/customer/read/<id>', 'GET', '获取客服');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (502, 127, '/madmin/wx/company', 'GET', '企业列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (503, 127, '/madmin/wx/company/customer', 'GET', '某企业客服列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (504, 127, '/madmin/wx/company/read/<id>', 'GET', '获取企业');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (505, 128, '/madmin/wx/company/save', 'POST', '新增企业');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (506, 128, '/madmin/wx/company/update', 'PUT', '更新企业');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (507, 128, '/madmin/wx/company/delete/<id>', 'DELETE', '删除企业');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (508, 128, '/madmin/wx/customer/save', 'POST', '新增客服');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (509, 128, '/madmin/wx/customer/update', 'PUT', '更新客服');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (510, 128, '/madmin/wx/customer/delete/<id>', 'DELETE', '删除客服');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (511, 129, '/madmin/float/get_config', 'GET', '悬浮菜单查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (512, 130, '/madmin/float/save', 'POST', '悬浮菜单关闭');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (513, 131, '/madmin/configuration/theme', 'GET', '主题色查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (514, 132, '/madmin/configuration/theme', 'PUT', '主题色修改');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (515, 117, '/madmin/integral/shop/classify/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (516, 55, '/madmin/configuration/web', 'GET', 'h5端查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (517, 56, '/madmin/configuration/web', 'PUT', 'h5端修改');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (518, 135, '/madmin/agent/grade/list', 'POST', '佣金等级列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (519, 135, '/madmin/agent/grade/<id>', 'GET', '查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (520, 136, '/madmin/agent/grade/<id>', 'PUT', '更新');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (521, 136, '/madmin/agent/grade/<id>', 'DELETE', '删除');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (522, 4, '/madmin/agent/grade/list', 'POST', '商品里查看佣金等级');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (523, 36, '/madmin/membership/level/user/<id>', 'PUT', '修改用户等级');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (524, 56, '/madmin/upload/license', 'POST', '上传授权文件');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (526, 137, '/madmin/order/form', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (530, 138, '/madmin/order/form', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (527, 137, '/madmin/order/form/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (531, 138, '/madmin/order/form/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (528, 138, '/madmin/order/form/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (533, 138, '/madmin/order/form/status', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (529, 138, '/madmin/order/form/answer/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (532, 138, '/madmin/order/form/answer/info/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (534, 138, '/madmin/order/form/template/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (535, 138, '/madmin/page/base64Up', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (536, 5, '/madmin/spu/name/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (537, 47, '/madmin/pay/mode', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (540, 143, '/madmin/live/good/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (541, 144, '/madmin/live/good/updateGoods/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (542, 144, '/madmin/live/good/addAudit/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (543, 144, '/madmin/live/good/resetAudit/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (544, 144, '/madmin/live/good/repeatAudit/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (545, 144, '/madmin/live/good/deleteAudit/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (546, 143, '/madmin/live/room/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (547, 144, '/madmin/live/room/getQrcode/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (548, 144, '/madmin/live/room/add', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (549, 143, '/madmin/live/room/detail/<id>', 'get', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (550, 144, '/madmin/live/room/syncRoom', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (551, 143, '/madmin/live/room/hide/<id>', 'get', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (552, 143, '/madmin/live/room/getReplay/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (553, 144, '/madmin/live/room/edit/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (554, 144, '/madmin/live/room/delete/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (555, 144, '/madmin/live/room/updateFeedPublic/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (556, 144, '/madmin/live/room/updateReplay/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (557, 144, '/madmin/live/room/goodsOnSale/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (558, 144, '/madmin/live/room/deleteRoomGoods/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (559, 144, '/madmin/live/room/addRoomGoods/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (560, 144, '/madmin/live/room/pushRoomGoods/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (561, 143, '/madmin/live/room/goods/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (563, 141, '/madmin/configuration/scrapyRead', 'GET', '采集配置查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (564, 142, '/madmin/configuration/scrapySave', 'POST', '采集配置修改');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (565, 142, '/shop/spu/scrapy', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (566, 141, '/shop/spu/scrapyLogList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (567, 146, '/madmin/weixin/save', 'POST', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (568, 145, '/madmin/configuration/WeixinAppRead', 'GET', '快速注册小程序配置查看');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (569, 146, '/madmin/configuration/WeixinAppSave', 'POST', '快速注册小程序配置修改');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (571, 29, '/madmin/live/room/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (572, 150, '/madmin/log/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (573, 15, '/shop/order/changeMoney', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (574, 14, '/shop/order/changeMoneyList', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (575, 5, '/madmin/spu/name/<id>', 'PUT', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (576, 151, '/madmin/group', 'GET', '拼团活动列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (577, 152, '/madmin/group', 'POST', '保存活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (578, 151, '/madmin/group/<id>', 'GET', '查看活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (579, 152, '/madmin/group/<id>', 'PUT', '编辑活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (580, 152, '/madmin/group/<id>', 'POST', '停止活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (581, 152, '/madmin/group/<id>', 'DELETE', '删除活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (582, 151, '/madmin/group/data', 'GET', '数据');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (583, 151, '/madmin/group/groupData', 'GET', '数据');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (584, 151, '/madmin/collage', 'GET', '团列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (585, 151, '/madmin/collage/<id>', 'GET', '团');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (586, 152, '/madmin/collage/<id>', 'POST', '手动成团');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (587, 151, '/madmin/configuration/group', 'GET', '拼团配置');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (588, 152, '/madmin/configuration/group', 'PUT', '拼团配置');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (589, 29, '/madmin/group', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (590, 29, '/madmin/group/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (592, 153, '/madmin/seckill', 'GET', '秒杀列表');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (593, 154, '/madmin/seckill', 'POST', '保存活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (594, 153, '/madmin/seckill/<id>', 'GET', '查看活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (595, 154, '/madmin/seckill/<id>', 'PUT', '编辑活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (596, 154, '/madmin/seckill/<id>', 'POST', '停止活动');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (597, 154, '/madmin/seckill/<id>', 'DELETE', '删除活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (600, '155', '/madmin/commission/activity/list', 'GET', '列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (601, '156', '/madmin/commission/activity/add', 'POST', '添加');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (602, '155', '/madmin/commission/activity/detail/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (603, '156', '/madmin/commission/activity/status', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (604, '156', '/madmin/commission/activity/delete/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (605, '155', '/madmin/commission/activity/award/list', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (606, '156', '/madmin/commission/activity/send', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (607, '156', '/madmin/commission/activity/issue', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (608, '157', '/madmin/commission/assessment/index', 'GET', '首页');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (609, '157', '/madmin/commission/assessment/list', 'GET', '列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (610, '158', '/madmin/commission/assessment/add', 'POST', '添加');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (611, '157', '/madmin/commission/assessment/detail/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (612, '158', '/madmin/commission/assessment/status', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (613, '157', '/madmin/commission/assessment/delete/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (614, '157', '/madmin/commission/assessment/order', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (615, '157', '/madmin/commission/assessment/degrade', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (616, '159', '/madmin/commission/assessment/withdraw/list', 'POST', '列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (617, '160', '/madmin/commission/assessment/withdraw/<id>', 'PUT', '添加');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (618, '159', '/madmin/commission/assessment/withdraw/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (619, '160', '/madmin/commission/assessment/withdraw/adopt/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (620, '160', '/madmin/commission/assessment/withdraw/refuse/<id>', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (621, '160', '/madmin/commission/assessment/withdraw/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (622, '161', '/madmin/wx/urllink/list', 'GET', '列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (623, '162', '/madmin/wx/urllink/add', 'POST', '添加');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (624, '161', '/madmin/wx/urllink/detail/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (625, '162', '/madmin/wx/urllink/delete/<id>', 'DELETE', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (626, 29, '/madmin/seckill', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (627, 29, '/madmin/seckill/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (628, 153, '/madmin/configuration/seckill', 'GET', '秒杀配置');
INSERT INTO `jiecheng_rule_extend`(`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (629, 154, '/madmin/configuration/seckill', 'PUT', '秒杀配置');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (630, 163, '/madmin/presell', 'GET', '预售列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (631, 164, '/madmin/presell', 'POST', '保存活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (632, 163, '/madmin/presell/<id>', 'GET', '查看活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (633, 164, '/madmin/presell/<id>', 'PUT', '编辑活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (634, 164, '/madmin/presell/<id>', 'POST', '停止活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (635, 164, '/madmin/presell/<id>', 'DELETE', '删除活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (636, 165, '/madmin/configuration/withholdPassword', 'GET', '截留口令查看');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (637, 166, '/madmin/configuration/withholdPassword', 'POST', '截留口令修改');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (638, 29, '/madmin/presell', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (639, 29, '/madmin/presell/<id>', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (640, '167', '/madmin/configuration/withholdPassword', 'GET', '资产转赠查看');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (641, '168', '/madmin/configuration/withholdPassword', 'POST', '资产转赠修改');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (642, 169, '/madmin/recharge', 'GET', '列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (643, 170, '/madmin/recharge', 'POST', '保存活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (644, 169, '/madmin/recharge/<id>', 'GET', '查看活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (645, 170, '/madmin/recharge/<id>', 'PUT', '编辑活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (646, 170, '/madmin/recharge/<id>', 'POST', '停止活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (647, 170, '/madmin/recharge/<id>', 'DELETE', '删除活动');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (648, '4', '/madmin/order/form', 'GET', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (649, 48, '/madmin/agent/grade/list', 'POST', '');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('650', '171', '/madmin/addons/list', 'GET', '插件列表');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('651', '171', '/madmin/addons/<id>', 'GET', '插件读取');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('652', '171', '/madmin/update_recode/<id>', 'GET', '插件更新日志');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('653', '172', '/madmin/addons/install', 'POST', '插件安装');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('654', '172', '/madmin/addons/uninstall', 'POST', '插件卸载');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('655', '172', '/madmin/authentication/active/addons', 'POST', '插件激活');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('656', '173', '/madmin/copyright', 'GET', '版权读取');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('657', '174', '/madmin/copyright', 'POST', '版权修改');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('658', '174', '/madmin/copyright/active', 'POST', '版权激活');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES ('659', '174', '/madmin/authentication/active/copyright', 'POST', '版权激活');
INSERT INTO `jiecheng_rule_extend` (`id`, `rule_id`, `rule_route`, `method`, `title`) VALUES (660, 14, '/madmin/order/form/answer/<id>', 'POST', '');

INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (1, '查看', '首页管理', '首页', 'read', 1, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (2, '查看', '商品管理', '商品', 'read', 2, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (3, '操作', '商品管理', '商品', 'update', 2, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (4, '查看', '商品管理', '运费模板', 'read', 3, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (5, '操作', '商品管理', '运费模板', 'update', 3, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (6, '查看', '商品管理', '退货地址', 'read', 4, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (7, '操作', '商品管理', '退货地址', 'update', 4, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (8, '查看', '订单管理', '订单', 'read', 5, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (9, '操作', '订单管理', '订单', 'update', 5, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (10, '发货', '订单管理', '订单', 'deliver', 5, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (11, '退款', '订单管理', '订单', 'refund', 5, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (12, '删除', '订单管理', '订单', 'delete', 5, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (13, '查看', '订单管理', '订单评论', 'read', 6, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (14, '操作', '订单管理', '订单评论', 'update', 6, '0', b'1', '2021-06-02 14:37:30', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (15, '查看', '订单管理', '维权订单', 'read', 7, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (16, '操作', '订单管理', '维权订单', 'update', 7, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (17, '操作', '订单管理', '批量发货', 'update', 8, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (18, '查看', '财务管理', '财务', 'read', 10, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (19, '操作', '财务管理', '财务', 'update', 10, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (20, '查看', '应用管理', '优惠券', 'read', 12, '3', b'1', '2021-06-02 14:37:31', '2021-09-16 11:08:16', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (21, '操作', '应用管理', '优惠券', 'update', 12, '3', b'1', '2021-06-02 14:37:31', '2021-09-16 11:08:18', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (22, '查看', '应用管理', '小票助手', 'read', 13, '10', b'1', '2021-06-02 14:37:31', '2021-09-16 11:08:30', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (23, '操作', '应用管理', '小票助手', 'update', 13, '10', b'1', '2021-06-02 14:37:31', '2021-09-16 11:08:37', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (25, '操作', '应用管理', '门店', 'update', 14, '11', b'1', '2021-06-02 14:37:31', '2021-09-16 11:08:41', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (26, '查看', '应用管理', '门店', 'read', 14, '11', b'1', '2021-06-02 14:37:31', '2021-09-16 11:09:01', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (27, '查看', '应用管理', '快递助手', 'read', 15, '14', b'1', '2021-06-02 14:37:31', '2021-09-16 11:09:05', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (28, '操作', '应用管理', '快递助手', 'update', 15, '14', b'1', '2021-06-02 14:37:31', '2021-09-16 11:09:07', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (29, '查看', '系统管理', '角色管理', 'read', 16, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (30, '操作', '系统管理', '角色管理', 'update', 16, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (31, '查看', '系统管理', '管理员管理', 'read', 17, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (32, '操作', '系统管理', '管理员管理', 'update', 17, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (33, '查看', '系统管理', '基本信息', 'read', 18, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (34, '操作', '系统管理', '基本信息', 'update', 18, '0', b'1', '2021-06-02 14:37:31', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (35, '查看', '财务管理', '入账详情', 'read', 19, '0', b'1', '2021-07-02 14:10:47', '2021-09-16 11:03:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (36, '查看', '应用管理', '同城配送', 'read', 20, '16', b'1', '2021-12-23 11:06:21', '2021-12-23 11:06:21', NULL);
INSERT INTO `jiecheng_shop_rule`(`id`, `name`, `group`, `resource_name`, `resource`, `level`, `up`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (37, '操作', '应用管理', '同城配送', 'update', 20, '16', b'1', '2021-12-23 11:06:21', '2021-12-23 11:06:21', NULL);
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (1, 1, '/shop/statistic/index', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (2, 1, '/shop/statistic/total', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (3, 1, '/shop/statistic/count', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (4, 1, '/shop/statistic/agent', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (5, 1, '/shop/statistic/consumption_ranking', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (6, 1, '/shop/statistic/sales_ranking', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (7, 2, '/shop/spu/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (8, 2, '/shop/spu/shop_preview', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (9, 2, '/shop/classify', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (10, 2, '/shop/freight/temp', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (11, 2, '/shop/integral/open', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (12, 2, '/shop/spu/sellout', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (13, 3, '/shop/spu', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (14, 3, '/shop/spu/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (15, 3, '/shop/spu', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (16, 3, '/shop/spu', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (17, 4, '/shop/freight/temp/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (18, 4, '/shop/freight/express', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (19, 5, '/shop/freight/temp/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (20, 5, '/shop/freight/temp/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (21, 5, '/shop/freight/temp/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (22, 5, '/shop/freight/temp', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (23, 6, '/shop/return/address/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (24, 7, '/shop/return/address/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (25, 7, '/shop/return/address/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (26, 7, '/shop/return/address/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (27, 7, '/shop/return/address', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (28, 8, '/shop/order/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (29, 8, '/shop/order/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (30, 8, '/shop/order/storeList/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (31, 9, '/shop/order/modifyOrderState', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (32, 9, '/shop/order/cancelOrder', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (33, 9, '/shop/order/orderClose', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (34, 9, '/shop/order/saveConsignee', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (35, 9, '/shop/order/export', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (36, 10, '/shop/order/saveOrderStatus', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (37, 11, '/shop/order/drawback', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (38, 12, '/shop/order/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (39, 13, '/shop/evaluate/evaluateList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (40, 14, '/shop/evaluate/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (41, 14, '/shop/evaluate/examine', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (42, 14, '/shop/evaluate/reply', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (43, 15, '/shop/aftersale/afterSaleList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (44, 15, '/shop/aftersale/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (45, 16, '/shop/aftersale/upAfterState', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (46, 17, 'shop/order/exportData', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (47, 18, '/shop/merchant/withdraw/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (48, 18, '/shop/merchant', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (49, 19, '/shop/merchant/withdraw', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (50, 20, '/shop/coupon/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (51, 20, '/shop/coupon/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (52, 20, '/shop/user/coupon/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (53, 20, '/shop/spu/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (54, 21, '/shop/coupon/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (55, 21, '/shop/coupon/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (56, 21, '/shop/coupon', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (57, 21, '/shop/coupon/send/<id>', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (58, 22, '/shop/receipts/template/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (59, 22, '/shop/receipts/printer/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (60, 23, '/shop/receipts/create', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (61, 25, '/shop/store/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (62, 25, '/shop/store/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (63, 25, '/shop/store/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (64, 25, '/shop/store/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (65, 25, '/shop/store', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (66, 26, '/shop/store/assistant/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (67, 25, '/shop/store/assistant', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (68, 26, '/shop/store/assistant/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (69, 25, '/shop/store/assistant/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (70, 25, '/shop/store/assistant/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (71, 27, '/shop/freight/express', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (72, 27, '/shop/express_bill/create', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (73, 27, '/shop/express_bill/template/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (74, 27, '/shop/receipts/template/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (75, 27, '/shop/receipts/template/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (76, 27, '/shop/receipts/printer/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (77, 27, '/shop/receipts/printer/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (78, 27, '/shop/invoice/<page>/<size>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (79, 27, '/shop/invoice/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (80, 27, '/shop/receipts/task/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (81, 27, '/shop/receipts/task/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (82, 27, '/shop/express_bill/config', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (83, 27, '/shop/order/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (84, 28, '/shop/invoice', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (85, 28, '/shop/invoice/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (86, 28, '/shop/invoice/print', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (87, 28, '/shop/receipts/create', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (88, 28, '/shop/express_bill/config', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (89, 28, '/shop/receipts/printer', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (90, 28, '/shop/receipts/printer/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (91, 28, '/shop/receipts/printer/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (92, 28, '/shop/receipts/printer/default/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (93, 28, '/shop/receipts/task', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (94, 28, '/shop/receipts/task/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (95, 28, '/shop/receipts/task/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (96, 28, '/shop/express_bill/template', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (97, 28, '/shop/express_bill/template/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (98, 28, '/shop/express_bill/template/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (99, 28, '/shop/express_bill/template/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (100, 28, '/shop/express_bill/template/default/<id>', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (101, 28, '/shop/receipts/template/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (102, 28, '/shop/receipts/template/default/<id>', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (103, 28, '/shop/receipts/template', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (104, 28, '/shop/receipts/template/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (105, 29, '/shop/auth/role/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (106, 29, '/shop/auth/rule/value', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (107, 30, '/shop/auth/role', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (108, 30, '/shop/auth/role/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (109, 30, '/shop/auth/role/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (110, 30, '/shop/auth/role/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (111, 31, '/shop/admin/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (112, 32, '/shop/admin/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (113, 32, '/shop/admin/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (114, 32, '/shop/admin/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (115, 32, '/shop/admin', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (116, 33, '/shop/merchant', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (117, 34, '/shop/merchant', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (118, 2, '/shop/store', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (119, 3, '/shop/store', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (120, 3, '/shop/spu/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (121, 2, '/shop/pic/addPic', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (122, 2, '/shop/pic/addGroup', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (123, 2, '/shop/pic/picList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (124, 2, '/shop/pic/groupList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (125, 2, '/shop/pic/editGroup', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (126, 2, '/shop/pic/delPic', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (127, 2, '/shop/pic/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (128, 3, '/shop/pic/addPic', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (129, 3, '/shop/pic/addGroup', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (130, 3, '/shop/pic/picList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (131, 3, '/shop/pic/groupList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (132, 3, '/shop/pic/editGroup', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (133, 3, '/shop/pic/delPic', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (134, 3, '/shop/pic/<id>', 'DELETE');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (135, 1, '/shop/statistic/exportUser', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (136, 1, '/shop/statistic/exportCommodity', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (137, 31, '/shop/auth/role', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (138, 32, '/shop/auth/role', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (139, 8, '/shop/receipts/template/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (140, 8, '/shop/receipts/printer/list', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (141, 35, '/shop/merchant/settlement/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (142, 21, '/shop/classify', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (143, 21, '/shop/configuration/<type>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (144, 22, '/shop/receipts/task/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (145, 23, '/shop/receipts/task/list', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (146, 23, '/shop/store', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (147, 26, '/shop/store', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (148, 27, '/shop/invoice', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (149, 10, '/shop/freight/express', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (150, 10, '/shop/order/storeList', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (151, 21, '/shop/configuration', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (152, 23, '/shop/receipts/template', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (153, 23, '/shop/receipts/printer', 'POST');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (154, 2, '/shop/spu/wechata/preview/<id>', 'GET');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (155, 3, '/shop/spu/reaudit/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (156, 3, '/shop/spu/inventory/<id>', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (157, 9, '/shop/order/changeMoney', 'PUT');
INSERT INTO `jiecheng_shop_rule_extend`(`id`, `rule_id`, `rule_route`, `method`) VALUES (158, 9, '/shop/order/changeMoneyList', 'GET');

INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (1, '订单支付通知', 1, 'OPENTM400231951', '1,2,3,4,5', 'vF-JTs4y4AlMoci3-cTy5ERkIbLbgPzemyWDDLDvrOw');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (2, '优惠券发放通知', 1, 'TM00279', '1,2,3,4,5', 'vtk_r0q64OnDGWyILLylcstHX6bcg_7GN8vYjVBYRfE');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (3, '提现成功通知', 1, 'OPENTM411207151', '1,2,3,4,5', 'CBPVrs50OGaW6cF4_smmXnKiWpEIZsNqjcnV1vb1Npk');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (4, '充值成功通知', 1, 'OPENTM417992877', '1,2,3,4,5', 'bO10CTN8TisyeajYEe135VxHQP4cIPGVrmFwqmyLprc');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (5, '积分变更通知', 1, 'OPENTM207379626', '1,2,3,4,5', '3R5DYi4tF-2PNf2bh8p7TsLkPZRdHY4Qz36FggiUjFI');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (6, '订单发货通知', 1, 'OPENTM414956350', '1,2,3,4,5', 'BSV34l5Ua4a-ZaDi0jgngNooBsWzv-bPBMrgtzC7rAg');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (7, '订单手动退款通知', 1, 'OPENTM415835502', '1,2,3,4,5', 'QEDa1ICUH2Wo6-h-v_lt1g5LJ0v67v5J_0Gt5BMvWu0');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (8, '订单付款通知', 1, 'OPENTM412822604', '1,2,3,4,5', 'vrUDMlgvZQeGBzaFOkDEQ_BW7EvY7BIJzcmfuzWzV_g');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (9, '订单售后通知', 1, 'OPENTM410195709', '1,2,3,4,5', 'ASQIH4nUs4Z-ilB23o2CH-lAbZQe-roPEbRX1ACmHGM');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (10, '订单收货通知', 1, 'OPENTM411154202', '1,2,3,4,5', 'S2gMCMjvb2yGdBLWyqxrUkVG7DbudctXO1QlVsTob3c');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (11, '订单支付通知', 2, '989', '1,2,3,4', '[\"character_string1\",\"amount2\",\"date3\",\"thing4\"]');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (12, '订单发货通知', 2, '855', '1,2,3,4,5', '[\"character_string1\",\"thing2\",\"phrase3\",\"character_string4\",\"date5\"]');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (13, '提现成功通知', 2, '2001', '1,2,3,4,5', '[\"amount1\",\"amount2\",\"thing3\",\"thing4\",\"time5\"]');
INSERT INTO `jiecheng_wx_template`(`id`, `name`, `channel`, `value`, `kid_list`, `example`) VALUES (14, '充值成功通知', 2, '4531', '1,2,3,4,5', '[\"amount1\",\"amount2\",\"thing3\",\"thing4\",\"time5\"]');


-- mall端数据初始化
-- ----------------------------
INSERT INTO `jiecheng_version` VALUES (1, '0', '1.0.0');
-- 登录账号 --
INSERT INTO `jiecheng_admin`(`id`, `mall_id`, `username`, `password`, `avatar`, `mobile`, `role_id`, `is_root`, `status`, `name`, `ac_token`, `login_ip`, `create_time`, `update_time`, `delete_time`) VALUES (1, '?mall_id?', '?admin_user?', '?admin_password?', NULL, NULL, 1, b'1', 1, '平台管理员', '0', '127.0.0.1', '?create_time?', '?create_time?', NULL);
-- mall配置 --
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'agentExplain', '{\"open\": 0, \"level\": 0, \"become\": {\"type\": 0, \"condition\": {\"apply\": {\"isShow\": 0}, \"payOrder\": {\"orderCount\": 0, \"statistics\": 0}, \"buyCommodity\": {\"commodity\": [], \"statistics\": 0}, \"explainTotal\": {\"explain\": 0, \"statistics\": 0}}}, \"profit\": {\"first\": 0, \"third\": 0, \"second\": 0}, \"isAudit\": 0, \"titleImg\": \"\", \"inPurchasing\": 0, \"relationship\": 0, \"is_show_profit\": 0}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'aliSms', '{\"accessKeyId\": \"\", \"accessSecret\": \"\"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'customerService', '{\"wechat\": \"\", \"is_open\": 0, \"wechata\": \"\"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'integralBalance', '{\"share\": {\"new\": 0, \"old\": 0}, \"is_open\": 0, \"withdraw\": 0, \"deduction\": 0.01, \"daily_limit\": 0, \"is_free_fee\": 0, \"balance_cash\": 0.1, \"balance_text\": \"余额\", \"integral_text\": \"积分\", \"withdraw_type\": [], \"agent_withdraw\": 0, \"integral_upper\": 0, \"service_charge\": 5.5, \"withdraw_limit\": 1, \"balance_setting\": 0, \"free_fee_section\": [0, 0], \"withdraw_service\": 0, \"agent_is_free_fee\": 0, \"get_integral_type\": 0, \"merchant_withdraw\": 0, \"get_integral_ratio\": 0, \"agent_withdraw_type\": [], \"withdraw_limit_cash\": \"100.00\", \"agent_service_charge\": 5.5, \"agent_withdraw_limit\": 0, \"integral_upper_limit\": 0, \"merchant_is_free_fee\": 0, \"withdraw_type_outline\": [], \"agent_free_fee_section\": [0, 0], \"agent_withdraw_service\": 0, \"merchant_withdraw_type\": [], \"merchant_service_charge\": 5.5, \"merchant_withdraw_limit\": 0, \"agent_withdraw_limit_cash\": \"100.00\", \"merchant_free_fee_section\": [0, 0], \"merchant_withdraw_service\": 0, \"agent_withdraw_type_outline\": [], \"merchant_withdraw_limit_cash\": \"100.00\", \"merchant_withdraw_type_outline\": []}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'lottery', '{\"lottery_open\": 0, \"lottery_type\": 1, \"lottery_limit\": 1, \"lottery_money\": 0, \"superposition\": 1, \"lottery_number\": 1, \"lottery_config_id\": 0, \"lottery_style\": 1}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'mallBase', '{\"logo\": \"61465115.jpg\", \"remark\": \"商城未开启\", \"is_open\": 0, \"load_pic\": \"\", \"mall_name\": \"商城名称(可修改)\", \"deal_title\": \"\", \"is_handset\": 0, \"trolley_pic\": \"\", \"deal_content\": \"\", \"sell_out_pic\": \"\", \"show_big_pic\": 0, \"customer_service_type\": 0}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'mallContact', '{\"mobile1\": \" \", \"mobile2\": \"\", \"contacts\": \" \", \"web_jsapi\": \" \"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'mallPayMode', '{\"web\": {\"ali\": 0, \"sendPay\": 0, \"recharge\": 0, \"ali_pay_mode_id\": \"\"}, \"wechat\": {\"wx\": 0, \"ali\": 0, \"sendPay\": 0, \"recharge\": 0, \"wx_pay_mode_id\": \"\", \"ali_pay_mode_id\": \"\"}, \"wechata\": {\"wx\": 0, \"sendPay\": 0, \"recharge\": 0, \"wx_pay_mode_id\": \"\"}}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'mallShare', '{\"logo\": 0, \"title\": 0, \"describe\": 1, \"share_url\": 0, \"custom_logo\": \"\", \"custom_title\": \"\", \"custom_describe\": \"\", \"custom_share_url\": \"\"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'memberLevel', '{\"list\": [{\"data\": {\"name\": \"vipmember\", \"type\": {\"card_type\": 1, \"avart_type\": 1, \"card_type_open\": 1}, \"color\": [], \"is_open\": 1, \"card_img\": \"\", \"avart_img\": \"\", \"card_img_open\": \"\"}, \"name\": \"vipmember\", \"active\": false, \"visible\": false, \"visible1\": false}, {\"data\": {\"num\": 2, \"list\": [[{\"icon\": \"/static/images/btns1.png\", \"text\": \"权益文字1\", \"card_bg\": \"#019CFE\", \"linkdata\": {\"id\": \"\", \"link\": \"\", \"name\": \"\", \"type\": \"\", \"login_type\": 1}, \"card_color\": \"#fff\"}]], \"name\": \"vipinterests\", \"rows\": 1, \"type\": 1, \"title\": {\"size\": 16, \"color\": \"#000\"}, \"width\": 60, \"radius\": {\"top\": 10, \"bottom\": 10}, \"bgcolor\": \"#FFFFFF\", \"padding\": {\"lr\": 24, \"top\": 16, \"bottom\": 8}, \"textcolor\": \"#565656\", \"style_type\": 1, \"currentIndex\": 0, \"card_font_size\": 16, \"content_bgcolor\": \"#fff\", \"content_padding\": {\"lr\": 24, \"top\": 16, \"bottom\": 8}}, \"name\": \"vipinterests\", \"active\": true, \"visible\": false, \"visible1\": false}, {\"data\": {\"bg\": \"#FFFFFF\", \"p_b\": 8, \"p_t\": 8, \"r_b\": 0, \"r_t\": 0, \"list\": [], \"name\": \"vipshangpinzu\", \"p_lr\": 12, \"type\": 0, \"limit\": 20, \"order\": \"comprehensive\", \"cateid\": 0, \"t_color\": \"#000000\", \"del_price\": {\"show\": 0, \"color\": \"#969696\", \"title\": \"\"}, \"list_type\": \"two\", \"pro_price\": {\"show\": 1, \"color\": \"#FF3C29\"}, \"sales_num\": {\"show\": 0, \"color\": \"#969696\", \"title\": \"\"}, \"show_type\": \"block\", \"vip_price\": {\"show\": 0}, \"add_button\": {\"icon\": \"el-sc-gouwuche2\", \"show\": false, \"color\": \"#F56C6C\"}, \"svip_price\": {\"show\": 0}}, \"name\": \"vipshangpinzu\", \"active\": false, \"visible\": false, \"visible1\": false}], \"is_open\": 1, \"is_price\": 0, \"page_config\": {\"nav_bg\": \"#41444E\", \"bg_type\": 1, \"bg_color\": \"#fff\", \"nav_name\": \"会员中心\", \"nav_type\": \"white\", \"nav_bgimg\": \"\", \"text_color\": \"#333333\"}}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'oss', '{\"bucket\": \"\", \"is_open\": 0, \"endpoint\": \"m\", \"oss_choice\": 1, \"accessKeyId\": \"\", \"accessKeySecret\": \"\"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'rightsSetting', '{\"refund_single\": 1, \"refund_explain\": \"\", \"is_rights_apply\": 1, \"rights_apply_time\": 7}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'signIn', '{\"info\": \" \", \"type\": 1, \"style\": {}, \"value\": \"1\", \"signIn\": false, \"pattern\": \"day\", \"special\": [], \"is_remind\": false, \"continuity\": [], \"cumulative\": []}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'tradeSetting', '{\"invoice\": [], \"is_invoice\": 0, \"deal_enhance\": 0, \"audit_evaluate\": 1, \"order_end_time\": 7, \"order_evaluate\": 1, \"order_is_close\": 0, \"receiving_time\": 7, \"is_show_evaluate\": 1, \"order_close_time\": 20, \"is_auto_receiving\": 0, \"agent_get_money_time\": 0}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'wx', '{\"URL\": \"1\", \"Token\": \"1\", \"appid\": \" \", \"appsecret\": \" \", \"EncodingAESKey\": \"2\"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'wxa', '{\"appid\": \" \", \"appsecret\": \" \"}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'web', '{\"is_open\": 0}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'group', '{\"is_open\": 1, \"close_time\": 0}', 1, '?create_time?', '?create_time?', NULL);
INSERT INTO `jiecheng_configuration`(`mall_id`, `type`, `configuration`, `status`, `create_time`, `update_time`, `delete_time`) VALUES ('?mall_id?', 'seckill', '{\"close_time\": 0}', 1, '?create_time?', '?create_time?', NULL);
-- 商户 ---
INSERT INTO `jiecheng_merchant`(`id`, `mall_id`, `user_id`, `name`, `logo`, `mobile`, `company`, `district`, `address`, `longitude`, `latitude`, `remark`, `content`, `business_license`, `indate`, `type`, `is_commodity_limit`, `commodity_limit`, `apply_id`, `audit_type`, `is_self`, `status`, `create_time`, `update_time`, `delete_time`, `customer_service_type`, `customer_service_mobile`, `username`, `password`, `wx_customer`) VALUES (1, "?mall_id?", NULL, '商城名称(可修改)', '/61465115.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, b'1', 0, 0, 0, b'1', 1, 1, '?create_time?', NULL, NULL, 1, NULL, NULL, NULL, NULL);
-- 商城装修 --

INSERT INTO `jiecheng_renovation` (`id`, `mall_id`, `merchant_id`, `page_name`, `page_type`, `status`, `small_img`, `page_key`, `qrcode`, `page_config`, `source`, `update_time`, `create_time`, `configure`) VALUES ('1', "?mall_id?", '1', '个人中心V1', '3', '2', 'https://jzwl365.oss-cn-beijing.aliyuncs.com/2021-07-26/60fe479ecb093.png', NULL, NULL, '{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}', '1,2,3', NULL, '2022-03-10 14:54:10', '{\"list\":[{\"name\":\"member\",\"active\":false,\"data\":{\"name\":\"member\",\"avart_type\":1,\"bg_type\":1,\"bg\":\"#FF3C29\",\"imgbg\":null,\"namecolor\":\"#FFFFFF\",\"finance\":{\"btns\":[0,1,2],\"numcolor\":\"#FFFFFF\",\"titlecolor\":\"#FFFFFF\"},\"sign\":{\"show\":1,\"text\":\"签到赚金币\",\"bg_color\":\"#EB4521\",\"text_color\":\"#FFFFFF\"},\"icon_right\":{\"icon\":\"\",\"color\":\"#FFFFFF\"},\"btn\":{\"show\":0,\"text_color\":\"#000000\",\"icon_color\":\"#000000\"},\"list\":[{\"icon\":\"el-sc-ziyuan1\",\"text\":\"未付款\",\"linkdata\":{\"link\":\"../order/list?tab=1\",\"type\":1,\"id\":7,\"name\":\"未付款订单\",\"login_type\":2}},{\"icon\":\"el-sc-daifahuo1\",\"linkdata\":{\"link\":\"../order/list?tab=2\",\"type\":1,\"id\":8,\"name\":\"待发货订单\",\"login_type\":2},\"text\":\"待发货\"},{\"icon\":\"el-sc-daishouhuo\",\"linkdata\":{\"link\":\"../order/list?tab=3\",\"type\":1,\"id\":9,\"name\":\"待收货订单\",\"login_type\":2},\"text\":\"待收货\"},{\"icon\":\"el-sc-pingjia-tianchong\",\"linkdata\":{\"link\":\"../order/list?tab=5\",\"type\":1,\"id\":10,\"name\":\"待评价订单\",\"login_type\":2},\"text\":\"待评价\"},{\"icon\":\"el-sc-wenzhang-copy\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"text\":\"全部订单\"}]},\"visible1\":false,\"visible\":false},{\"name\":\"bindmobile\",\"active\":false,\"data\":{\"name\":\"bindmobile\",\"title\":{\"text\":\"绑定手机号，同步全渠道订单和优惠卷\",\"color\":\"#212121\"},\"icon\":{\"url\":\"\",\"color\":\"#FF3C29\"},\"padding\":{\"top\":10,\"lr\":10,\"bottom\":10},\"radius\":{\"top\":0,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"listmenu\",\"active\":false,\"data\":{\"name\":\"listmenu\",\"left\":{\"iconcolor\":\"#F43623\",\"textcolor\":\"#212121\"},\"right\":{\"iconcolor\":\"#969696\",\"textcolor\":\"#969696\",\"icon\":\"el-sc-jiantou\"},\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":0,\"lr\":10,\"bottom\":0},\"radius\":{\"top\":0,\"bottom\":0},\"list\":[{\"icon\":\"el-sc-wenzhang-copy\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"right_text\":\"查看\",\"left_text\":\"全部订单\"}]},\"visible1\":false,\"visible\":false},{\"name\":\"icongroup\",\"active\":false,\"data\":{\"name\":\"icongroup\",\"list\":[{\"icon\":\"el-sc-qianbao3\",\"text\":\"未付款\",\"linkdata\":{\"link\":\"../order/list?tab=1\",\"type\":1,\"id\":7,\"name\":\"未付款订单\",\"login_type\":2}},{\"icon\":\"el-sc-daifahuo1\",\"linkdata\":{\"link\":\"../order/list?tab=2\",\"type\":1,\"id\":8,\"name\":\"待发货订单\",\"login_type\":2},\"text\":\"待发货\"},{\"icon\":\"el-sc-daishouhuo2\",\"linkdata\":{\"link\":\"../order/list?tab=3\",\"type\":1,\"id\":9,\"name\":\"待收货订单\",\"login_type\":2},\"text\":\"待收货\"},{\"icon\":\"el-sc-pingjia-tianchong\",\"linkdata\":{\"link\":\"../order/list?tab=5\",\"type\":1,\"id\":10,\"name\":\"待评价订单\",\"login_type\":2},\"text\":\"待评价\"},{\"icon\":\"el-sc-wancheng\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"text\":\"全部\"}],\"iconcolor\":\"#F43623\",\"textcolor\":\"#565656\",\"num\":5,\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":0,\"lr\":10,\"bottom\":0},\"radius\":{\"top\":0,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"icongroup\",\"active\":true,\"data\":{\"name\":\"icongroup\",\"list\":[{\"icon\":\"el-sc-ziyuanldpi\",\"text\":\"我的收藏\",\"linkdata\":{\"link\":\"../mine/collect/collect\",\"type\":1,\"id\":19,\"name\":\"我的收藏\",\"login_type\":2}},{\"icon\":\"el-sc-ziyuan458\",\"linkdata\":{\"link\":\"../contactus/contactus\",\"type\":1,\"id\":23,\"name\":\"联系我们\",\"login_type\":1},\"text\":\"联系我们\"},{\"icon\":\"el-sc-fenxiaoshang\",\"linkdata\":{\"link\":\"../agent/index\",\"type\":1,\"id\":13,\"name\":\"分销中心\",\"login_type\":2},\"text\":\"分销中心\"},{\"icon\":\"el-sc-jifenmingxi\",\"linkdata\":{\"link\":\"../mine/integral/integral\",\"type\":1,\"id\":17,\"name\":\"积分明细\",\"login_type\":2},\"text\":\"积分明细\"},{\"icon\":\"el-sc-caiwuliushui\",\"linkdata\":{\"link\":\"../mine/balance/balance\",\"type\":1,\"id\":20,\"name\":\"余额明细\",\"login_type\":2},\"text\":\"余额明细\"},{\"icon\":\"el-sc-kefu1\",\"linkdata\":{\"link\":\"../wxqcode/wxqcode\",\"type\":1,\"id\":27,\"name\":\"客服二维码\",\"login_type\":1},\"text\":\"在线客服\"},{\"icon\":\"el-sc-position\",\"linkdata\":{\"link\":\"../mine/address/index\",\"type\":1,\"id\":11,\"name\":\"地址列表\",\"login_type\":2},\"text\":\"收货地址\"},{\"icon\":\"el-sc-icon1\",\"linkdata\":{\"link\":\"../recharge/recharge\",\"type\":1,\"id\":18,\"name\":\"充值中心\",\"login_type\":2},\"text\":\"充值中心\"},{\"icon\":\"el-sc-yuetixian\",\"linkdata\":{\"link\":\"../mine/balance/withdraw\",\"type\":1,\"id\":21,\"name\":\"余额提现\",\"login_type\":2},\"text\":\"余额提现\"}],\"iconcolor\":\"#F43623\",\"textcolor\":\"#565656\",\"num\":4,\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":10,\"lr\":10,\"bottom\":8},\"radius\":{\"top\":10,\"bottom\":10}},\"visible1\":false,\"visible\":false}],\"list1\":[],\"page_data\":{\"page_name\":\"个人中心V1\",\"nav_name\":\"个人中心V1\",\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\"},\"page_config\":{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}}');
INSERT INTO `jiecheng_renovation` (`id`, `mall_id`, `merchant_id`, `page_name`, `page_type`, `status`, `small_img`, `page_key`, `qrcode`, `page_config`, `source`, `update_time`, `create_time`, `configure`) VALUES ('2', "?mall_id?", '1', '商品详情V1', '2', '2', 'https://jzwl365.oss-cn-beijing.aliyuncs.com/2021-07-22/60f9314273335.png', NULL, NULL, '{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}', '1,2,3', NULL, '2022-03-10 14:54:10', '{\"list\":[{\"name\":\"detail_banner\",\"active\":false,\"data\":{\"name\":\"detail_banner\",\"imglist\":[{\"url\":\"\"},{\"url\":\"\"}],\"type\":1,\"bg\":\"#FFFFFF\",\"position\":\"center\"},\"visible1\":false,\"visible\":false},{\"name\":\"detail_info\",\"active\":true,\"data\":{\"name\":\"detail_info\",\"padding\":{\"top\":0,\"bottom\":0},\"color\":{\"bg\":\"#FFFFFF\",\"title\":\"#212121\",\"desc\":\"#565656\",\"price\":\"#FF3C29\",\"del_price\":\"#969696\",\"express\":\"#565656\",\"sales\":\"#565656\",\"addr\":\"#565656\"},\"share\":{\"show\":1,\"text\":\"分享\",\"icon\":\"el-sc-fenxiang_2\",\"color\":\"#666666\"},\"collect\":{\"show\":1,\"text\":\"收藏\",\"icon\":\"el-sc-shoucang11\",\"color\":\"#E3713E\",\"bfcolor\":\"#666666\"},\"commission\":{\"show\":1,\"text\":\"\",\"icon\":\"\"},\"memberprice\":{\"show\":1,\"bg_color\":\"#F5F5F5\",\"text_color\":\"#565656\",\"text\":\"成为会员，享受会员价\",\"price_color\":\"#FF3C29\"}},\"visible1\":false,\"visible\":false},{\"name\":\"merchshop\",\"active\":false,\"data\":{\"name\":\"merchshop\",\"bg_color\":\"#FFFFFF\",\"num_color\":\"#969696\",\"title_color\":\"#212121\",\"shownum\":true,\"p_t\":10,\"p_b\":0,\"logo_type\":\"circle\",\"btn\":{\"type\":1,\"title\":\"\",\"titlecolor1\":\"#FF3C29\",\"titlecolor2\":\"#FFFFFF\",\"color\":\"#FF3C29\",\"rd\":30,\"icon\":\"el-sc-xiangyou\",\"icon_color\":\"#FF3C29\"}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_sale\",\"active\":false,\"data\":{\"name\":\"detail_sale\",\"color\":{\"title\":\"#969696\",\"text\":\"#212121\",\"coupon\":\"#FF3C29\",\"bg\":\"#FFFFFF\"},\"active\":{\"labelcolor\":\"#FF3C29\",\"textcolor\":\"#FF3C29\"},\"service\":{\"labelcolor\":\"#FF3C29\",\"textcolor\":\"#FF3C29\"},\"padding\":{\"top\":10,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_pullup\",\"active\":false,\"data\":{\"name\":\"detail_pullup\",\"bg\":\"#FFFFFF\",\"mt\":0},\"visible1\":false,\"visible\":false},{\"name\":\"detail_comment\",\"active\":false,\"data\":{\"name\":\"detail_comment\",\"padding\":{\"top\":10,\"bottom\":0},\"color\":{\"title\":\"#212121\",\"more\":\"#969696\",\"username\":\"#212121\",\"start\":\"#FF3C29\",\"evaluate\":\"#212121\",\"sku\":\"#969696\",\"bg\":\"#FFFFFF\"}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_btns\",\"active\":false,\"data\":{\"name\":\"detail_btns\",\"color\":{\"bg\":\"#FFFFFF\",\"icon\":\"#969696\",\"text\":\"#969696\",\"btn_l\":\"#212121\",\"btn_r\":\"#FF3C29\",\"badge\":\"#FF3C29\"},\"list\":[{\"icon\":\"el-sc-shouye\",\"linkdata\":{\"link\":\"../index/index\",\"type\":1,\"id\":1,\"name\":\"商城首页\",\"login_type\":1},\"text\":\"首页\"},{\"icon\":\"el-sc-cuo1\",\"text\":\"客服\",\"linkdata\":{\"link\":\"../customer/customer\",\"type\":1,\"id\":25,\"name\":\"在线客服\",\"login_type\":1}},{\"icon\":\"el-sc-gouwuche3\",\"text\":\"购物车\",\"linkdata\":{\"link\":\"../cart/cart\",\"type\":1,\"id\":5,\"name\":\"购物车\",\"login_type\":2}}]},\"visible1\":false,\"visible\":false}],\"list1\":[],\"page_data\":{\"page_name\":\"商品详情V1\",\"nav_name\":\"商品详情V1\",\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\"},\"page_config\":{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}}');
INSERT INTO `jiecheng_renovation` (`id`, `mall_id`, `merchant_id`, `page_name`, `page_type`, `status`, `small_img`, `page_key`, `qrcode`, `page_config`, `source`, `update_time`, `create_time`, `configure`) VALUES ('3', "?mall_id?", '1', '首页V1', '1', '2', 'https://cs.jc362.com/storage/52684/shop_admin/20220813/1660380577.png', 'TEMPLATE::3', NULL, '{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}', '1,2,3', '2022-08-13 17:38:08', '2022-03-10 14:54:10', '{\"list\":[{\"name\":\"searchBar\",\"active\":true,\"data\":{\"name\":\"searchBar\",\"placeholder\":\"请输入查询内容\",\"bg_type\":1,\"bg_color\":\"#FFFFFF\",\"bg_img\":\"\",\"color\":{\"text\":\"#cdcac9\",\"icon\":\"#cdcac9\",\"bg\":\"#f7f6f5\",\"border\":\"#E4E7ED\"},\"margin\":{\"top\":14,\"bottom\":14},\"padding\":{\"lr\":14},\"radius\":10},\"visible1\":false,\"visible\":false},{\"name\":\"singlePics\",\"active\":false,\"data\":{\"name\":\"singlePics\",\"imglist\":[{\"url\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb43459d5b.png\",\"linkdata\":{\"link\":\"\",\"name\":\"\",\"id\":\"\",\"type\":\"\",\"login_type\":1}}],\"margin\":{\"top\":14,\"bottom\":14,\"lr\":14,\"pic\":0},\"radius\":20},\"visible1\":false,\"visible\":false},{\"name\":\"btnGroup\",\"active\":false,\"data\":{\"name\":\"btnGroup\",\"list\":[{\"icon\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb4340ba14.png\",\"text\":\"冬季精选\",\"linkdata\":{\"link\":\"../mine/mine\",\"type\":1,\"id\":2,\"name\":\"个人中心\",\"login_type\":1}},{\"icon\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb43407bd5.png\",\"text\":\"潮鞋专区\",\"linkdata\":{\"link\":\"../mine/mine\",\"type\":1,\"id\":2,\"name\":\"个人中心\",\"login_type\":1}},{\"icon\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb4340a357.png\",\"text\":\"名牌背包\",\"linkdata\":{\"link\":\"../list/list\",\"type\":1,\"id\":4,\"name\":\"商品列表\",\"login_type\":1}},{\"icon\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb4340a7ee.png\",\"text\":\"潮牌精选\",\"linkdata\":{\"link\":\"../cart/cart\",\"type\":1,\"id\":5,\"name\":\"购物车\",\"login_type\":2}},{\"icon\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb43407771.png\",\"text\":\"全部服饰\",\"linkdata\":{\"link\":\"../index/index\",\"type\":1,\"id\":1,\"name\":\"商城首页\",\"login_type\":1}}],\"rows\":1,\"textcolor\":\"#565656\",\"num\":5,\"type\":1,\"bgcolor\":\"#FFFFFF\",\"width\":33,\"padding\":{\"top\":0,\"lr\":14,\"bottom\":14},\"radius\":{\"top\":10,\"bottom\":10}},\"visible1\":false,\"visible\":false},{\"name\":\"hot_area\",\"active\":false,\"data\":{\"name\":\"hot_area\",\"img\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb434505d3.png\",\"list\":[{\"width\":159,\"height\":113,\"x\":13,\"y\":9,\"link\":\"\",\"name\":\"\",\"id\":\"\",\"type\":1},{\"width\":153,\"height\":102,\"x\":211,\"y\":14.5,\"link\":\"\",\"name\":\"\",\"id\":\"\",\"type\":1},{\"width\":150,\"height\":109,\"x\":390,\"y\":11,\"link\":\"\",\"name\":\"\",\"id\":\"\",\"type\":1}],\"padding\":{\"top\":0,\"lr\":0,\"bottom\":0},\"radius\":{\"top\":0,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"singlePics\",\"active\":false,\"data\":{\"name\":\"singlePics\",\"imglist\":[{\"url\":\"https://jzwl365.oss-cn-beijing.aliyuncs.com/zongkong/2021-05-25/60acb434838b7.png\",\"linkdata\":{\"link\":\"\",\"name\":\"\",\"id\":\"\",\"type\":\"\",\"login_type\":1}}],\"margin\":{\"top\":12,\"bottom\":12,\"lr\":5,\"pic\":0},\"radius\":0},\"visible1\":false,\"visible\":false}],\"list1\":[],\"page_data\":{\"page_name\":\"服饰类1\",\"nav_name\":\"首页\",\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\"},\"page_config\":{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}}');
INSERT INTO `jiecheng_bottom_menu` (`id`, `mall_id`, `name`, `type`, `data`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (1, "?mall_id?", '底部菜单', '商城', '{\"name\": \"底部菜单\", \"type\": 1, \"bgcolor\": {\"checked\": \"#ffffff\", \"default\": \"#ffffff\"}, \"shownum\": 0, \"tablist\": [{\"url\": \"\", \"icon\": \"el-sc-shangchengshouye2\", \"name\": \"\", \"text\": \"首页\", \"linkdata\": {\"id\": 1, \"link\": \"../index/index\", \"name\": \"商城首页\", \"type\": 1, \"login_type\": 1}, \"icon_type\": 1, \"active_url\": \"\"}, {\"url\": \"\", \"icon\": \"el-sc-fenlei_\", \"name\": \"\", \"text\": \"分类\", \"linkdata\": {\"id\": 3, \"link\": \"../categoryList/categoryList\", \"name\": \"分类列表\", \"type\": 1, \"login_type\": 1}, \"icon_type\": 1, \"active_url\": \"\"}, {\"url\": \"\", \"icon\": \"el-sc-gouwuche1\", \"name\": \"\", \"text\": \"购物车\", \"linkdata\": {\"id\": 5, \"link\": \"../cart/cart\", \"name\": \"购物车\", \"type\": 1, \"login_type\": 2}, \"icon_type\": 1, \"active_url\": \"\"}, {\"url\": \"\", \"icon\": \"el-sc-geren3\", \"name\": \"\", \"text\": \"个人中心\", \"linkdata\": {\"id\": 2, \"link\": \"../mine/mine\", \"name\": \"个人中心\", \"type\": 1, \"login_type\": 1}, \"icon_type\": 1, \"active_url\": \"\"}], \"iconcolor\": {\"checked\": \"#FF3C29\", \"default\": \"#565656\"}, \"num_color\": \"#FF3C29\", \"show_text\": 1, \"textcolor\": {\"checked\": \"#212121\", \"default\": \"#565656\"}, \"line_color\": \"#FFFFFF\"}', b'1', '2022-08-12 09:23:19', '2022-08-12 09:23:19', NULL);


-- 权限 --
INSERT INTO `jiecheng_role`(`id`, `mall_id`, `name`, `remark`, `department_id`, `is_root`, `status`, `create_time`, `update_time`, `delete_time`) VALUES (1, "?mall_id?", '超级管理员', '拥有所有权限', 1, b'1', b'1', '?create_time?', NULL, NULL);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (1, "?mall_id?", 1, 1);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (2, "?mall_id?", 1, 2);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (3, "?mall_id?", 1, 3);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (4, "?mall_id?", 1, 4);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (5, "?mall_id?", 1, 5);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (6, "?mall_id?", 1, 6);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (7, "?mall_id?", 1, 7);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (8, "?mall_id?", 1, 8);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (9, "?mall_id?", 1, 9);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (10, "?mall_id?", 1, 10);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (11, "?mall_id?", 1, 11);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (12, "?mall_id?", 1, 12);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (13, "?mall_id?", 1, 13);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (14, "?mall_id?", 1, 14);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (15, "?mall_id?", 1, 15);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (16, "?mall_id?", 1, 16);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (17, "?mall_id?", 1, 17);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (18, "?mall_id?", 1, 18);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (19, "?mall_id?", 1, 19);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (20, "?mall_id?", 1, 20);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (21, "?mall_id?", 1, 21);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (22, "?mall_id?", 1, 22);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (23, "?mall_id?", 1, 23);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (24, "?mall_id?", 1, 24);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (25, "?mall_id?", 1, 25);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (26, "?mall_id?", 1, 26);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (27, "?mall_id?", 1, 27);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (28, "?mall_id?", 1, 28);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (29, "?mall_id?", 1, 29);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (30, "?mall_id?", 1, 30);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (31, "?mall_id?", 1, 31);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (32, "?mall_id?", 1, 32);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (33, "?mall_id?", 1, 33);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (34, "?mall_id?", 1, 34);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (35, "?mall_id?", 1, 35);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (36, "?mall_id?", 1, 36);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (37, "?mall_id?", 1, 37);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (38, "?mall_id?", 1, 38);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (39, "?mall_id?", 1, 39);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (40, "?mall_id?", 1, 40);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (41, "?mall_id?", 1, 41);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (42, "?mall_id?", 1, 42);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (43, "?mall_id?", 1, 43);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (44, "?mall_id?", 1, 44);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (45, "?mall_id?", 1, 45);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (46, "?mall_id?", 1, 46);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (47, "?mall_id?", 1, 47);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (48, "?mall_id?", 1, 48);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (49, "?mall_id?", 1, 49);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (50, "?mall_id?", 1, 50);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (51, "?mall_id?", 1, 51);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (52, "?mall_id?", 1, 52);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (53, "?mall_id?", 1, 53);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (54, "?mall_id?", 1, 54);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (55, "?mall_id?", 1, 55);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (56, "?mall_id?", 1, 56);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (57, "?mall_id?", 1, 57);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (58, "?mall_id?", 1, 58);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (59, "?mall_id?", 1, 59);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (60, "?mall_id?", 1, 60);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (61, "?mall_id?", 1, 61);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (62, "?mall_id?", 1, 62);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (63, "?mall_id?", 1, 63);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (64, "?mall_id?", 1, 64);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (65, "?mall_id?", 1, 65);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (66, "?mall_id?", 1, 66);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (67, "?mall_id?", 1, 67);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (68, "?mall_id?", 1, 68);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (69, "?mall_id?", 1, 69);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (70, "?mall_id?", 1, 70);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (71, "?mall_id?", 1, 71);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (72, "?mall_id?", 1, 72);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (73, "?mall_id?", 1, 73);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (74, "?mall_id?", 1, 74);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (75, "?mall_id?", 1, 75);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (76, "?mall_id?", 1, 76);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (77, "?mall_id?", 1, 77);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (78, "?mall_id?", 1, 78);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (79, "?mall_id?", 1, 79);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (80, "?mall_id?", 1, 80);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (81, "?mall_id?", 1, 81);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (82, "?mall_id?", 1, 82);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (83, "?mall_id?", 1, 83);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (84, "?mall_id?", 1, 84);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (85, "?mall_id?", 1, 85);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (86, "?mall_id?", 1, 86);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (87, "?mall_id?", 1, 87);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (88, "?mall_id?", 1, 88);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (89, "?mall_id?", 1, 89);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (90, "?mall_id?", 1, 90);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (91, "?mall_id?", 1, 91);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (92, "?mall_id?", 1, 92);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (93, "?mall_id?", 1, 93);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (94, "?mall_id?", 1, 94);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (95, "?mall_id?", 1, 95);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (96, "?mall_id?", 1, 96);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (97, "?mall_id?", 1, 97);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (98, "?mall_id?", 1, 98);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (99, "?mall_id?", 1, 99);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (100, "?mall_id?", 1, 100);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (101, "?mall_id?", 1, 101);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (102, "?mall_id?", 1, 102);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (103, "?mall_id?", 1, 103);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (104, "?mall_id?", 1, 104);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (105, "?mall_id?", 1, 105);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (106, "?mall_id?", 1, 108);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (107, "?mall_id?", 1, 109);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (108, "?mall_id?", 1, 110);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (109, "?mall_id?", 1, 111);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (110, "?mall_id?", 1, 112);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (111, "?mall_id?", 1, 113);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (112, "?mall_id?", 1, 114);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (113, "?mall_id?", 1, 115);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (114, "?mall_id?", 1, 116);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (115, "?mall_id?", 1, 117);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (116, "?mall_id?", 1, 118);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (117, "?mall_id?", 1, 119);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (118, "?mall_id?", 1, 120);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (119, "?mall_id?", 1, 121);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (120, "?mall_id?", 1, 122);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (121, "?mall_id?", 1, 123);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (122, "?mall_id?", 1, 124);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (123, "?mall_id?", 1, 125);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (124, "?mall_id?", 1, 126);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (125, "?mall_id?", 1, 127);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (126, "?mall_id?", 1, 128);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (127, "?mall_id?", 1, 129);
INSERT INTO `jiecheng_role_rule`(`id`, `mall_id`, `role_id`, `rule_id`) VALUES (128, "?mall_id?", 1, 130);
INSERT INTO `jiecheng_lessee` (`id`, `user_id`, `site`, `meal_id`, `addons`, `binding`, `type`, `indate`, `apply_id`, `versions`, `entry_type`, `status`, `create_time`, `update_time`, `delete_time`, `username`, `password`, `oss_size`) VALUES ('?mall_id?', NULL, '管理平台', '1', '[1, 2, 3, 5, 10, 11, 12, 13, 14, 16, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 30, 31, 32, 34, 35, 36, 4, 6, 7, 8, 9, 15, 17, 18,29, 33]', NULL, '1', NULL, '0', NULL, '[1, 2, 3]', '1', '2022-08-03 11:41:27', '2022-08-13 11:51:22', NULL, '?admin_user?', '?admin_password?', '5242880');

INSERT INTO `jiecheng_copyright` (`name`, `mobile`, `logo`, `record_number`, `token`, `url`, `link_info`, `record_number1`) VALUES ('节程商城系统开源版', '', 'https://q.jc362.com/storage/favicon.png?1637204530', '商用需要授权', '', '', NULL, '');


-- ----------------------------
-- Records of jiecheng_template
-- ----------------------------
INSERT INTO `jiecheng_template`(`id`, `page_name`, `page_type`, `small_img`, `page_key`, `page_config`, `source`, `create_time`, `update_time`, `class_name`, `configure`) VALUES (1, '商品详情V1', 2, 'https://jzwl365.oss-cn-beijing.aliyuncs.com/2021-07-22/60f9314273335.png', 'TEMPLATE::9', '{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}', '1,2', '2021-05-31 14:58:00', '2021-08-28 13:48:39', NULL, '{\"list\":[{\"name\":\"detail_banner\",\"active\":false,\"data\":{\"name\":\"detail_banner\",\"imglist\":[{\"url\":\"\"},{\"url\":\"\"}],\"type\":1,\"bg\":\"#FFFFFF\",\"position\":\"center\"},\"visible1\":false,\"visible\":false},{\"name\":\"detail_info\",\"active\":true,\"data\":{\"name\":\"detail_info\",\"padding\":{\"top\":0,\"bottom\":0},\"color\":{\"bg\":\"#FFFFFF\",\"title\":\"#212121\",\"desc\":\"#565656\",\"price\":\"#FF3C29\",\"del_price\":\"#969696\",\"express\":\"#565656\",\"sales\":\"#565656\",\"addr\":\"#565656\"},\"share\":{\"show\":1,\"text\":\"分享\",\"icon\":\"el-sc-fenxiang_2\",\"color\":\"#666666\"},\"collect\":{\"show\":1,\"text\":\"收藏\",\"icon\":\"el-sc-shoucang11\",\"color\":\"#E3713E\",\"bfcolor\":\"#666666\"},\"commission\":{\"show\":1,\"text\":\"\",\"icon\":\"\"},\"memberprice\":{\"show\":1,\"bg_color\":\"#F5F5F5\",\"text_color\":\"#565656\",\"text\":\"成为会员，享受会员价\",\"price_color\":\"#FF3C29\"}},\"visible1\":false,\"visible\":false},{\"name\":\"merchshop\",\"active\":false,\"data\":{\"name\":\"merchshop\",\"bg_color\":\"#FFFFFF\",\"num_color\":\"#969696\",\"title_color\":\"#212121\",\"shownum\":true,\"p_t\":10,\"p_b\":0,\"logo_type\":\"circle\",\"btn\":{\"type\":1,\"title\":\"\",\"titlecolor1\":\"#FF3C29\",\"titlecolor2\":\"#FFFFFF\",\"color\":\"#FF3C29\",\"rd\":30,\"icon\":\"el-sc-xiangyou\",\"icon_color\":\"#FF3C29\"}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_sale\",\"active\":false,\"data\":{\"name\":\"detail_sale\",\"color\":{\"title\":\"#969696\",\"text\":\"#212121\",\"coupon\":\"#FF3C29\",\"bg\":\"#FFFFFF\"},\"active\":{\"labelcolor\":\"#FF3C29\",\"textcolor\":\"#FF3C29\"},\"service\":{\"labelcolor\":\"#FF3C29\",\"textcolor\":\"#FF3C29\"},\"padding\":{\"top\":10,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_pullup\",\"active\":false,\"data\":{\"name\":\"detail_pullup\",\"bg\":\"#FFFFFF\",\"mt\":0},\"visible1\":false,\"visible\":false},{\"name\":\"detail_comment\",\"active\":false,\"data\":{\"name\":\"detail_comment\",\"padding\":{\"top\":10,\"bottom\":0},\"color\":{\"title\":\"#212121\",\"more\":\"#969696\",\"username\":\"#212121\",\"start\":\"#FF3C29\",\"evaluate\":\"#212121\",\"sku\":\"#969696\",\"bg\":\"#FFFFFF\"}},\"visible1\":false,\"visible\":false},{\"name\":\"detail_btns\",\"active\":false,\"data\":{\"name\":\"detail_btns\",\"color\":{\"bg\":\"#FFFFFF\",\"icon\":\"#969696\",\"text\":\"#969696\",\"btn_l\":\"#212121\",\"btn_r\":\"#FF3C29\",\"badge\":\"#FF3C29\"},\"list\":[{\"icon\":\"el-sc-shouye\",\"linkdata\":{\"link\":\"../index/index\",\"type\":1,\"id\":1,\"name\":\"商城首页\",\"login_type\":1},\"text\":\"首页\"},{\"icon\":\"el-sc-cuo1\",\"text\":\"客服\",\"linkdata\":{\"link\":\"../customer/customer\",\"type\":1,\"id\":25,\"name\":\"在线客服\",\"login_type\":1}},{\"icon\":\"el-sc-gouwuche3\",\"text\":\"购物车\",\"linkdata\":{\"link\":\"../cart/cart\",\"type\":1,\"id\":5,\"name\":\"购物车\",\"login_type\":2}}]},\"visible1\":false,\"visible\":false}],\"list1\":[],\"page_data\":{\"page_name\":\"商品详情V1\",\"nav_name\":\"商品详情V1\",\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\"},\"page_config\":{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}}');
INSERT INTO `jiecheng_template`(`id`, `page_name`, `page_type`, `small_img`, `page_key`, `page_config`, `source`, `create_time`, `update_time`, `class_name`, `configure`) VALUES (2, '个人中心V1', 3, 'https://jzwl365.oss-cn-beijing.aliyuncs.com/2021-07-26/60fe479ecb093.png', 'TEMPLATE::10',  '{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}', '1,2', '2022-01-27 13:47:50', '2022-01-22 10:03:56', NULL, '{\"list\":[{\"name\":\"member\",\"active\":false,\"data\":{\"name\":\"member\",\"avart_type\":1,\"bg_type\":1,\"bg\":\"#FF3C29\",\"imgbg\":null,\"namecolor\":\"#FFFFFF\",\"finance\":{\"btns\":[0,1,2],\"numcolor\":\"#FFFFFF\",\"titlecolor\":\"#FFFFFF\"},\"sign\":{\"show\":1,\"text\":\"签到赚金币\",\"bg_color\":\"#EB4521\",\"text_color\":\"#FFFFFF\"},\"icon_right\":{\"icon\":\"\",\"color\":\"#FFFFFF\"},\"btn\":{\"show\":0,\"text_color\":\"#000000\",\"icon_color\":\"#000000\"},\"list\":[{\"icon\":\"el-sc-ziyuan1\",\"text\":\"未付款\",\"linkdata\":{\"link\":\"../order/list?tab=1\",\"type\":1,\"id\":7,\"name\":\"未付款订单\",\"login_type\":2}},{\"icon\":\"el-sc-daifahuo1\",\"linkdata\":{\"link\":\"../order/list?tab=2\",\"type\":1,\"id\":8,\"name\":\"待发货订单\",\"login_type\":2},\"text\":\"待发货\"},{\"icon\":\"el-sc-daishouhuo\",\"linkdata\":{\"link\":\"../order/list?tab=3\",\"type\":1,\"id\":9,\"name\":\"待收货订单\",\"login_type\":2},\"text\":\"待收货\"},{\"icon\":\"el-sc-pingjia-tianchong\",\"linkdata\":{\"link\":\"../order/list?tab=5\",\"type\":1,\"id\":10,\"name\":\"待评价订单\",\"login_type\":2},\"text\":\"待评价\"},{\"icon\":\"el-sc-wenzhang-copy\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"text\":\"全部订单\"}]},\"visible1\":false,\"visible\":false},{\"name\":\"bindmobile\",\"active\":false,\"data\":{\"name\":\"bindmobile\",\"title\":{\"text\":\"绑定手机号，同步全渠道订单和优惠卷\",\"color\":\"#212121\"},\"icon\":{\"url\":\"\",\"color\":\"#FF3C29\"},\"padding\":{\"top\":10,\"lr\":10,\"bottom\":10},\"radius\":{\"top\":0,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"listmenu\",\"active\":false,\"data\":{\"name\":\"listmenu\",\"left\":{\"iconcolor\":\"#F43623\",\"textcolor\":\"#212121\"},\"right\":{\"iconcolor\":\"#969696\",\"textcolor\":\"#969696\",\"icon\":\"el-sc-jiantou\"},\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":0,\"lr\":10,\"bottom\":0},\"radius\":{\"top\":0,\"bottom\":0},\"list\":[{\"icon\":\"el-sc-wenzhang-copy\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"right_text\":\"查看\",\"left_text\":\"全部订单\"}]},\"visible1\":false,\"visible\":false},{\"name\":\"icongroup\",\"active\":false,\"data\":{\"name\":\"icongroup\",\"list\":[{\"icon\":\"el-sc-qianbao3\",\"text\":\"未付款\",\"linkdata\":{\"link\":\"../order/list?tab=1\",\"type\":1,\"id\":7,\"name\":\"未付款订单\",\"login_type\":2}},{\"icon\":\"el-sc-daifahuo1\",\"linkdata\":{\"link\":\"../order/list?tab=2\",\"type\":1,\"id\":8,\"name\":\"待发货订单\",\"login_type\":2},\"text\":\"待发货\"},{\"icon\":\"el-sc-daishouhuo2\",\"linkdata\":{\"link\":\"../order/list?tab=3\",\"type\":1,\"id\":9,\"name\":\"待收货订单\",\"login_type\":2},\"text\":\"待收货\"},{\"icon\":\"el-sc-pingjia-tianchong\",\"linkdata\":{\"link\":\"../order/list?tab=5\",\"type\":1,\"id\":10,\"name\":\"待评价订单\",\"login_type\":2},\"text\":\"待评价\"},{\"icon\":\"el-sc-wancheng\",\"linkdata\":{\"link\":\"../order/list\",\"type\":1,\"id\":6,\"name\":\"订单列表\",\"login_type\":2},\"text\":\"全部\"}],\"iconcolor\":\"#F43623\",\"textcolor\":\"#565656\",\"num\":5,\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":0,\"lr\":10,\"bottom\":0},\"radius\":{\"top\":0,\"bottom\":0}},\"visible1\":false,\"visible\":false},{\"name\":\"icongroup\",\"active\":true,\"data\":{\"name\":\"icongroup\",\"list\":[{\"icon\":\"el-sc-ziyuanldpi\",\"text\":\"我的收藏\",\"linkdata\":{\"link\":\"../mine/collect/collect\",\"type\":1,\"id\":19,\"name\":\"我的收藏\",\"login_type\":2}},{\"icon\":\"el-sc-ziyuan458\",\"linkdata\":{\"link\":\"../contactus/contactus\",\"type\":1,\"id\":23,\"name\":\"联系我们\",\"login_type\":1},\"text\":\"联系我们\"},{\"icon\":\"el-sc-fenxiaoshang\",\"linkdata\":{\"link\":\"../agent/index\",\"type\":1,\"id\":13,\"name\":\"分销中心\",\"login_type\":2},\"text\":\"分销中心\"},{\"icon\":\"el-sc-jifenmingxi\",\"linkdata\":{\"link\":\"../mine/integral/integral\",\"type\":1,\"id\":17,\"name\":\"积分明细\",\"login_type\":2},\"text\":\"积分明细\"},{\"icon\":\"el-sc-caiwuliushui\",\"linkdata\":{\"link\":\"../mine/balance/balance\",\"type\":1,\"id\":20,\"name\":\"余额明细\",\"login_type\":2},\"text\":\"余额明细\"},{\"icon\":\"el-sc-kefu1\",\"linkdata\":{\"link\":\"../wxqcode/wxqcode\",\"type\":1,\"id\":27,\"name\":\"客服二维码\",\"login_type\":1},\"text\":\"在线客服\"},{\"icon\":\"el-sc-position\",\"linkdata\":{\"link\":\"../mine/address/index\",\"type\":1,\"id\":11,\"name\":\"地址列表\",\"login_type\":2},\"text\":\"收货地址\"},{\"icon\":\"el-sc-icon1\",\"linkdata\":{\"link\":\"../recharge/recharge\",\"type\":1,\"id\":18,\"name\":\"充值中心\",\"login_type\":2},\"text\":\"充值中心\"},{\"icon\":\"el-sc-yuetixian\",\"linkdata\":{\"link\":\"../mine/balance/withdraw\",\"type\":1,\"id\":21,\"name\":\"余额提现\",\"login_type\":2},\"text\":\"余额提现\"}],\"iconcolor\":\"#F43623\",\"textcolor\":\"#565656\",\"num\":4,\"bgcolor\":\"#FFFFFF\",\"padding\":{\"top\":10,\"lr\":10,\"bottom\":8},\"radius\":{\"top\":10,\"bottom\":10}},\"visible1\":false,\"visible\":false}],\"list1\":[],\"page_data\":{\"page_name\":\"个人中心V1\",\"nav_name\":\"个人中心V1\",\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\"},\"page_config\":{\"bg_color\":\"#f3f3f3\",\"text_color\":\"#333333\",\"bg_type\":1,\"nav_bgimg\":\"\",\"nav_bg\":\"#FFFFFF\",\"nav_type\":\"black\"}}');
-- ----------------------------
-- View structure for jiecheng_agent_order_profit
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_agent_order_profit`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_agent_order_profit` AS select `ab`.`order_no` AS `order_no`,max(`ab`.`order_id`) AS `id`,if((min(`ab`.`steps`) = 0),1,2) AS `is_purchase`,min(`ab`.`status`) AS `status`,max((case when (`ab`.`steps` = 1) then `ag`.`name` when (`ab`.`steps` = 0) then `u`.`nickname` else NULL end)) AS `name1`,max(if((`ab`.`steps` = 2),`ag`.`name`,NULL)) AS `name2`,max(if((`ab`.`steps` = 3),`ag`.`name`,NULL)) AS `name3`,if((min(`ab`.`status`) <> 3),sum(if(((`ab`.`steps` in (0,1)) and (`ab`.`status` <> 3)),`ab`.`amount`,0)),sum(if((`ab`.`steps` in (0,1)),`ab`.`amount`,0))) AS `amount1`,if((min(`ab`.`status`) <> 3),sum(if(((`ab`.`steps` = 2) and (`ab`.`status` <> 3)),`ab`.`amount`,0)),sum(if((`ab`.`steps` = 2),`ab`.`amount`,0))) AS `amount2`,if((min(`ab`.`status`) <> 3),sum(if(((`ab`.`steps` = 3) and (`ab`.`status` <> 3)),`ab`.`amount`,0)),sum(if((`ab`.`steps` = 3),`ab`.`amount`,0))) AS `amount3`,min(`u`.`nickname`) AS `nickname`,min(`u`.`mobile`) AS `mobile`,max(`ab`.`commodity_id`) AS `commodity_id`,max(`ab`.`mall_id`) AS `mall_id`,max(`ab`.`update_time`) AS `update_time` from (((`jiecheng_agent_bill_temporary` `ab` left join `jiecheng_agent` `ag` on((`ag`.`id` = `ab`.`agent_id`))) left join `jiecheng_user` `u` on((`ab`.`source_user_id` = `u`.`id`))) left join `jiecheng_order` `o` on((`o`.`id` = `ab`.`order_id`))) where ((`o`.`status` <> 1) and (`o`.`status` <> 12)) group by `ab`.`order_no`;

-- ----------------------------
-- View structure for jiecheng_agent_statistics
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_agent_statistics`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_agent_statistics` AS select `la`.`id` AS `id`,`la`.`grade_id` AS `grade_id`,`la`.`mall_id` AS `mall_id`,`la`.`user_id` AS `user_id`,`u`.`nickname` AS `nickname`,`u`.`picurl` AS `picurl`,`uc`.`agent_total` AS `agent_total`,`uc`.`agent` AS `agent`,`uc`.`agent_withdraw` AS `agent_withdraw`,`uc`.`assessment_total` AS `assessment_total`,`uc`.`assessment` AS `assessment`,`uc`.`assessment_withdraw` AS `assessment_withdraw`,ifnull(`pu`.`nickname`,'总店') AS `p_nickname`,`pu`.`picurl` AS `p_picurl`,ifnull(`ls`.`count_order`,0) AS `count_order`,ifnull(`ls`.`sum_figure`,0) AS `sum_figure`,((select count(`cu`.`id`) from `jiecheng_user` `cu` where ((`cu`.`pid` = `la`.`id`) and isnull(`cu`.`agent_id`))) + ifnull((select ((count(`ca`.`id`) + count(`cca`.`id`)) + count(`ccca`.`id`)) from ((`jiecheng_agent` `ca` left join `jiecheng_agent` `cca` on(((`cca`.`pid` = `ca`.`id`) and isnull(`cca`.`delete_time`)))) left join `jiecheng_agent` `ccca` on(((`ccca`.`pid` = `cca`.`id`) and isnull(`cca`.`delete_time`)))) where ((`ca`.`pid` = `la`.`id`) and isnull(`ca`.`delete_time`))),0)) AS `team` from ((((((`jiecheng_agent` `la` left join `jiecheng_user` `u` on((`u`.`id` = `la`.`user_id`))) left join `jiecheng_user_cash` `uc` on((`uc`.`user_id` = `la`.`user_id`))) left join `jiecheng_agent` `pa` on((`pa`.`user_id` = `la`.`user_id`))) left join `jiecheng_agent` `ppa` on((`ppa`.`id` = `pa`.`pid`))) left join `jiecheng_user` `pu` on((`pu`.`id` = `ppa`.`user_id`))) left join (select distinct `a`.`user_id` AS `user_id`,`a`.`agent_id` AS `agent_id`,`ab`.`count_order` AS `count_order`,`sf`.`sum_figure` AS `sum_figure` from ((`jiecheng_agent_bill_temporary` `a` left join (select `a`.`user_id` AS `user_id`,count(`a`.`order_id`) AS `count_order` from (select `jiecheng_agent_bill_temporary`.`user_id` AS `user_id`,`jiecheng_agent_bill_temporary`.`order_id` AS `order_id`,`jiecheng_agent_bill_temporary`.`agent_id` AS `agent_id` from (`jiecheng_agent_bill_temporary` left join `jiecheng_order` `o` on((`o`.`id` = `jiecheng_agent_bill_temporary`.`order_id`))) where ((`jiecheng_agent_bill_temporary`.`status` in (0,1)) and (`o`.`status` <> 1) and (`o`.`status` <> 12)) group by `jiecheng_agent_bill_temporary`.`order_id`,`jiecheng_agent_bill_temporary`.`user_id`,`jiecheng_agent_bill_temporary`.`agent_id`) `a` group by `a`.`user_id`) `ab` on((`a`.`user_id` = `ab`.`user_id`))) left join (select `jiecheng_agent_bill_temporary`.`user_id` AS `user_id`,sum(`jiecheng_agent_bill_temporary`.`amount`) AS `sum_figure` from (`jiecheng_agent_bill_temporary` left join `jiecheng_order` `o` on((`o`.`id` = `jiecheng_agent_bill_temporary`.`order_id`))) where ((`jiecheng_agent_bill_temporary`.`status` = 0) and (`o`.`status` <> 1) and (`o`.`status` <> 12)) group by `jiecheng_agent_bill_temporary`.`user_id`) `sf` on((`a`.`user_id` = `sf`.`user_id`)))) `ls` on((`la`.`user_id` = `ls`.`user_id`))) where isnull(`la`.`delete_time`);

-- ----------------------------
-- View structure for jiecheng_agent_team
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_agent_team`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_agent_team` AS select `a`.`id` AS `id`,`a`.`mall_id` AS `mall_id`,`a`.`user_id` AS `user_id`,`a`.`pid` AS `pid`,`u`.`nickname` AS `nickname`,`u`.`picurl` AS `picurl`,`uc`.`agent_total` AS `agent_total`,`uc`.`agent_withdraw` AS `agent_withdraw`,(select count(`jiecheng_agent`.`id`) from `jiecheng_agent` where (`jiecheng_agent`.`pid` = `a`.`id`)) AS `child_agent`,`a`.`create_time` AS `create_time`,`pa`.`pid` AS `ppid`,`ppa`.`pid` AS `pppid`,`wx`.`type` AS `source` from (((((`jiecheng_agent` `a` left join `jiecheng_user_wx` `wx` on((`wx`.`user_id` = `a`.`user_id`))) left join `jiecheng_user_cash` `uc` on((`uc`.`user_id` = `a`.`user_id`))) left join `jiecheng_user` `u` on((`u`.`id` = `a`.`user_id`))) left join `jiecheng_agent` `pa` on((`pa`.`id` = `a`.`pid`))) left join `jiecheng_agent` `ppa` on((`ppa`.`id` = `pa`.`pid`))) where isnull(`a`.`delete_time`);

-- ----------------------------
-- View structure for jiecheng_commodity_fraction
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_commodity_fraction`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_commodity_fraction` AS select sum(`t`.`visit_number`) AS `visit`,sum(`t`.`sell_number`) AS `sell`,sum(`t`.`collect`) AS `collect`,`t`.`commodity_id` AS `commodity_id`,`t`.`create_time` AS `create_time`,((unix_timestamp(date_format(`t`.`create_time`,'%Y-%m-%d')) - unix_timestamp(date_format(`t`.`create_time`,'%Y-01-01'))) / 86400) AS `time` from ((select 0 AS `visit_number`,sum(`jiecheng_order_commodity`.`count`) AS `sell_number`,0 AS `collect`,`jiecheng_order_commodity`.`commodity_id` AS `commodity_id`,date_format(`jiecheng_order_commodity`.`create_time`,'%Y-%m-%d') AS `create_time` from `jiecheng_order_commodity` where (`jiecheng_order_commodity`.`status` not in (1,11,9)) group by `jiecheng_order_commodity`.`commodity_id`,`jiecheng_order_commodity`.`create_time`) union (select count(0) AS `visit_number`,0 AS `sell_number`,0 AS `collect`,replace(`jiecheng_visit`.`url`,'index/goods/','') AS `commodity_id`,date_format(`jiecheng_visit`.`create_time`,'%Y-%m-%d') AS `create_time` from `jiecheng_visit` where ((`jiecheng_visit`.`url` like 'index/goods/%') and (`jiecheng_visit`.`mode` = 'GET')) group by `jiecheng_visit`.`url`,`jiecheng_visit`.`create_time`) union (select 0 AS `visit_number`,0 AS `sell_number`,count(0) AS `collect`,`jiecheng_my_collect`.`commodity_id` AS `commodity_id`,date_format(`jiecheng_my_collect`.`create_time`,'%Y-%m-%d') AS `create_time` from `jiecheng_my_collect` group by `jiecheng_my_collect`.`commodity_id`,`jiecheng_my_collect`.`create_time`)) `t` where ((length((0 + `t`.`commodity_id`)) = length(`t`.`commodity_id`)) and (`t`.`create_time` is not null)) group by `t`.`commodity_id`,`t`.`create_time` order by `t`.`create_time` desc;

-- ----------------------------
-- View structure for jiecheng_consumption_ranking
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_consumption_ranking`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_consumption_ranking` AS select `u`.`nickname` AS `nickname`,`u`.`picurl` AS `picurl`,ifnull(`y`.`amount`,0) AS `amount`,ifnull(`y`.`order_number`,0) AS `order_number`,floor(((unix_timestamp(now()) - unix_timestamp(`u`.`update_time`)) / 86400)) AS `not_logged_in_day`,`u`.`merchant_id` AS `merchant_id`,`u`.`mall_id` AS `mall_id` from (`jiecheng_user` `u` left join (select sum(`jiecheng_order`.`money`) AS `amount`,count(0) AS `order_number`,`jiecheng_order`.`user_id` AS `user_id` from `jiecheng_order` where (`jiecheng_order`.`status` <> 1) group by `jiecheng_order`.`user_id`) `y` on((`u`.`id` = `y`.`user_id`))) order by ifnull(`y`.`amount`,0) desc;

-- ----------------------------
-- View structure for jiecheng_mall_agent_team
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_mall_agent_team`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_mall_agent_team` AS select `a`.`id` AS `id`,`a`.`mall_id` AS `mall_id`,`a`.`status` AS `status`,`a`.`user_id` AS `user_id`,(SELECT `ag`.`name` FROM `jiecheng_agent_grade` `ag` WHERE ( `ag`.`id` = `a`.`grade_id` ) AND `ag`.`mall_id` = `a`.`mall_id`) AS `grade`,`a`.`name` AS `name`,`a`.`mobile` AS `mobile`,`aa`.`create_time` AS `apply_date`,`a`.`create_time` AS `create_time`,`u`.`nickname` AS `nickname`,`u`.`picurl` AS `picurl`,`sf`.`sum_figure` AS `sum_figure`,`uc`.`agent_total` AS `agent_total`,`uc`.`agent` AS `agent`,`uc`.`agent_withdraw` AS `agent_withdraw`,ifnull(`pu`.`nickname`,'总店') AS `p_nickname`,`pu`.`picurl` AS `p_picurl`,(select count(`cu`.`pid`) from `jiecheng_user` `cu` where ((`cu`.`pid` = `a`.`id`) and (`cu`.`pid` is not null) and isnull(`cu`.`agent_id`) and (`cu`.`status` = 1) and isnull(`cu`.`delete_time`))) AS `child_user`,(select count(`ca`.`id`) from `jiecheng_agent` `ca` where ((`ca`.`pid` = `a`.`id`) and (`ca`.`status` = 1) and isnull(`ca`.`delete_time`))) AS `l1`,(select count(`cca`.`id`) from (`jiecheng_agent` `ca` left join `jiecheng_agent` `cca` on((`cca`.`pid` = `ca`.`id`))) where ((`ca`.`pid` = `a`.`id`) and (`ca`.`status` = 1) and isnull(`ca`.`delete_time`) and (`cca`.`status` = 1) and isnull(`cca`.`delete_time`))) AS `l2`,(select count(`ccca`.`id`) from ((`jiecheng_agent` `ca` left join `jiecheng_agent` `cca` on((`cca`.`pid` = `ca`.`id`))) left join `jiecheng_agent` `ccca` on((`ccca`.`pid` = `cca`.`id`))) where ((`ca`.`pid` = `a`.`id`) and (`ca`.`status` = 1) and isnull(`ca`.`delete_time`) and (`cca`.`status` = 1) and isnull(`cca`.`delete_time`) and (`ccca`.`status` = 1) and isnull(`ccca`.`delete_time`))) AS `l3` from ((((((`jiecheng_agent` `a` left join `jiecheng_user` `u` on(((`u`.`id` = `a`.`user_id`) and (`u`.`status` = 1) and isnull(`u`.`delete_time`)))) left join `jiecheng_user_cash` `uc` on((`uc`.`user_id` = `a`.`user_id`))) left join (select `jiecheng_agent_bill_temporary`.`user_id` AS `user_id`,sum(`jiecheng_agent_bill_temporary`.`amount`) AS `sum_figure` from (`jiecheng_agent_bill_temporary` left join `jiecheng_order` `o` on((`o`.`id` = `jiecheng_agent_bill_temporary`.`order_id`))) where ((`jiecheng_agent_bill_temporary`.`status` = 0) and (`o`.`status` <> 1) and (`o`.`status` <> 12)) group by `jiecheng_agent_bill_temporary`.`user_id`) `sf` on((`a`.`user_id` = `sf`.`user_id`))) left join `jiecheng_agent` `pa` on(((`pa`.`id` = `a`.`pid`) and (`pa`.`status` = 1) and isnull(`pa`.`delete_time`)))) left join `jiecheng_user` `pu` on(((`pu`.`id` = `pa`.`user_id`) and (`pu`.`status` = 1) and isnull(`pu`.`delete_time`)))) left join `jiecheng_agent_apply` `aa` on(((`aa`.`id` = `a`.`apply_id`) and (`a`.`apply_id` is not null) and (`aa`.`status` = 1)))) where isnull(`a`.`delete_time`);

-- ----------------------------
-- View structure for jiecheng_merchant_log
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_merchant_log`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_merchant_log` AS select ifnull(`t`.`money`,0) AS `money`,`t`.`text` AS `text`,`t`.`order_no` AS `order_no`,`t`.`merchant_id` AS `merchant_id`,`t`.`create_time` AS `create_time`,`t`.`type` AS `type`,`jiecheng_merchant`.`mall_id` AS `mall_id`,`jiecheng_merchant`.`name` AS `name`,`jiecheng_user`.`nickname` AS `nickname`,`jiecheng_user`.`picurl` AS `picurl` from ((((select (`jiecheng_order`.`money` - (`jiecheng_order`.`discount` + `jiecheng_order`.`discount_amount`)) AS `money`,'下单' AS `text`,`jiecheng_order`.`order_no` AS `order_no`,`jiecheng_order`.`merchant_id` AS `merchant_id`,`jiecheng_order`.`create_time` AS `create_time`,1 AS `type`,`jiecheng_order`.`user_id` AS `user_id` from `jiecheng_order` where (`jiecheng_order`.`status` > 1)) union (select `jiecheng_order_after`.`money` AS `money`,'退款' AS `text`,`jiecheng_order`.`order_no` AS `order_no`,`jiecheng_order`.`merchant_id` AS `merchant_id`,`jiecheng_order_after`.`update_time` AS `create_time`,2 AS `type`,`jiecheng_order`.`user_id` AS `user_id` from (`jiecheng_order_after` left join `jiecheng_order` on((`jiecheng_order_after`.`order_id` = `jiecheng_order`.`id`))) where (`jiecheng_order_after`.`state` in (2,5,8,11)))) `t` left join `jiecheng_merchant` on((`jiecheng_merchant`.`id` = `t`.`merchant_id`))) left join `jiecheng_user` on((`t`.`user_id` = `jiecheng_user`.`id`))) where (`jiecheng_merchant`.`is_self` = 0) order by `t`.`create_time` desc;

-- ----------------------------
-- View structure for jiecheng_merchant_settlement
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_merchant_settlement`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_merchant_settlement` AS select `o`.`id` AS `id`,`o`.`mall_id` AS `mall_id`,`o`.`merchant_id` AS `merchant_id`,`o`.`pay_id` AS `pay_id`,`jm`.`name` AS `merchant_name`,`jm`.`is_self` AS `is_self`,`o`.`order_no` AS `order_no`,`o`.`pay_no` AS `pay_no`,`o`.`user_id` AS `user_id`,`u`.`nickname` AS `nickname`,`u`.`picurl` AS `picurl`,(`o`.`money` + ifnull(`of`.`freight`,0.00)) AS `money`,`of`.`freight` AS `freight`,`o`.`status` AS `status`,`o`.`discount` AS `discount`,`c`.`name` AS `name`,`c`.`denomination` AS `denomination`,`o`.`discount_amount` AS `discount_amount`,ifnull(`oaf`.`refund`,0.00) AS `refund`,ifnull(`abtt`.`suma`,0.00) AS `agent_cash`,`abtt`.`jsonb` AS `agent_details`,`o`.`create_time` AS `create_time`,`o`.`update_time` AS `update_time`,`o`.`pay_time` AS `pay_time`,(((((`o`.`money` + ifnull(`of`.`freight`,0.00)) - `o`.`discount`) - `o`.`discount_amount`) - ifnull(`oaf`.`refund`,0.00)) - ifnull(`abtt`.`suma`,0.00)) AS `account`,(case `o`.`settlement` when 0 then '待支付' when 1 then '未结算' when 2 then '已结算' else '取消' end) AS `settlement` from (((((((`jiecheng_order` `o` left join `jiecheng_merchant` `jm` on((`jm`.`id` = `o`.`merchant_id`))) left join `jiecheng_order_freight` `of` on((`of`.`order_id` = `o`.`id`))) left join `jiecheng_user_coupon` `uc` on((`uc`.`unified_order_no` = `o`.`order_no`))) left join `jiecheng_coupon` `c` on((`c`.`id` = `uc`.`coupon_id`))) left join `jiecheng_user` `u` on((`u`.`id` = `o`.`user_id`))) left join (select `jiecheng_order_after`.`order_id` AS `order_id`,sum(`jiecheng_order_after`.`money`) AS `refund` from `jiecheng_order_after` where (isnull(`jiecheng_order_after`.`delete_time`) and (`jiecheng_order_after`.`state` in (7,8,11))) group by `jiecheng_order_after`.`order_id`) `oaf` on((`oaf`.`order_id` = `o`.`id`))) left join (select `order_commodity`.`order_no` AS `order_no`,sum(`order_commodity`.`amount`) AS `suma`,concat('[',group_concat(json_object('commodity_name',`order_commodity`.`name`,'commodity_master',`order_commodity`.`master`,'sku',`order_commodity`.`pvs_value`,'sku_pic',`order_commodity`.`pic`,'amount',`order_commodity`.`amount`) separator ','),']') AS `jsonb` from (select `temporary`.`order_no` AS `order_no`,`commodity`.`name` AS `name`,`temporary`.`amount` AS `amount`,`sku`.`pvs_value` AS `pvs_value`,`sku`.`pic` AS `pic`,`commodity`.`master` AS `master` from ((`jiecheng_agent_bill_temporary` `temporary` left join `jiecheng_commodity` `commodity` on((`temporary`.`commodity_id` = `commodity`.`id`))) left join `jiecheng_sku` `sku` on((`temporary`.`sku_id` = `sku`.`id`))) where (isnull(`temporary`.`delete_time`) and (`temporary`.`status` in (0,1)))) `order_commodity` group by `order_commodity`.`order_no`) `abtt` on((convert(`abtt`.`order_no` using utf8mb4) = `o`.`order_no`))) where (isnull(`o`.`delete_time`) and (`o`.`status` > 1) and (`o`.`settlement` > 0));

-- ----------------------------
-- View structure for jiecheng_merchant_statistics
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_merchant_statistics`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_merchant_statistics` AS select sum(`t`.`avg_money`) AS `avg_money`,sum((`t`.`total_money` - `t`.`refund_money`)) AS `turnover`,sum(`t`.`total_number`) AS `total_number`,sum(`t`.`total_pay_order_number`) AS `total_pay_order_number`,sum(`t`.`total_money`) AS `total_money`,sum(`t`.`consumer`) AS `consumer`,round(((sum(`t`.`total_money`) - sum(`t`.`refund_money`)) / sum(`t`.`consumer`)),2) AS `avg_user_money`,sum(`t`.`consumer`) AS `number`,sum(`t`.`new_user_number`) AS `new_user_number`,sum(`t`.`new_agent_number`) AS `new_agent_number`,sum((`t`.`payment_money` + `t`.`refund_money`)) AS `payment_money`,sum(`t`.`refund_number`) AS `refund_number`,sum(`t`.`refund_money`) AS `refund_money`,round((sum(`t`.`refund_number`) / sum(`t`.`total_number`)),4) AS `refund_rate`,sum(`t`.`fission_number`) AS `fission_number`,sum(`t`.`fission_visit`) AS `fission_visit`,sum(`t`.`fission_click`) AS `fission_click`,round(unix_timestamp(`t`.`create_time`),0) AS `create_time`,`t`.`mall_id` AS `mall_id`,`t`.`merchant_id` AS `merchant_id` from ((select round(avg(`jiecheng_order`.`money`),2) AS `avg_money`,sum(if((`jiecheng_order`.`status` = 1),0,`jiecheng_order`.`money`)) AS `payment_money`,count(0) AS `total_number`,sum((`jiecheng_order`.`status` in (2,3,4,5,6,7,8))) AS `total_pay_order_number`,sum(`jiecheng_order`.`money`) AS `total_money`,0 AS `avg_user_money`,0 AS `new_user_number`,0 AS `new_agent_number`,sum(`refund`.`refund_number`) AS `refund_number`,sum(truncate(`refund`.`refund_money`,2)) AS `refund_money`,count(distinct `jiecheng_order`.`user_id`) AS `consumer`,0 AS `fission_number`,0 AS `fission_visit`,0 AS `fission_click`,`jiecheng_order`.`mall_id` AS `mall_id`,date_format(`jiecheng_order`.`create_time`,'%Y-%m-%d') AS `create_time`,`jiecheng_order`.`merchant_id` AS `merchant_id` from (`jiecheng_order` left join (select count(distinct `yyyy`.`order_id`) AS `refund_number`,sum(`yyyy`.`money`) AS `refund_money`,`yyyy`.`order_id` AS `order_id` from ((select `jiecheng_order_commodity`.`order_id` AS `order_id`,`jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`mall_id` AS `mall_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`type` = 3) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d')))) union all select `jiecheng_order_after`.`order_id` AS `order_id`,`jiecheng_order_after`.`money` AS `money`,`jiecheng_order_after`.`mall_id` AS `mall_id` from `jiecheng_order_after` where ((`jiecheng_order_after`.`state` in (2,5,8,11)) and (`jiecheng_order_after`.`update_time` > date_format(now(),'%Y-%m-%d')))) `yyyy` group by `yyyy`.`order_id`) `refund` on((`jiecheng_order`.`id` = `refund`.`order_id`))) where (`jiecheng_order`.`create_time` < date_format(now(),'%Y-%m-%d')) group by `jiecheng_order`.`mall_id`,`jiecheng_order`.`create_time`,`jiecheng_order`.`merchant_id`) union (select 0 AS `avg_money`,0 AS `payment_money`,0 AS `total_number`,0 AS `total_pay_order_number`,0 AS `total_money`,0 AS `avg_user_money`,count(0) AS `new_user_number`,ifnull(sum((`jiecheng_user`.`agent_id` > 0)),0) AS `new_agent_number`,0 AS `refund_number`,0 AS `refund_money`,0 AS `consumer`,sum((`jiecheng_user`.`pid` > 0)) AS `fission_number`,0 AS `fission_visit`,0 AS `fission_click`,`jiecheng_user`.`mall_id` AS `mall_id`,date_format(`jiecheng_user`.`create_time`,'%Y-%m-%d') AS `create_time`,`jiecheng_user`.`merchant_id` AS `merchant_id` from (`jiecheng_user` left join `jiecheng_user_cash` on((`jiecheng_user_cash`.`user_id` = `jiecheng_user`.`id`))) where (`jiecheng_user`.`create_time` < date_format(now(),'%Y-%m-%d')) group by `jiecheng_user`.`mall_id`,`jiecheng_user`.`create_time`,`jiecheng_user`.`merchant_id`) union (select 0 AS `avg_money`,0 AS `payment_money`,0 AS `total_number`,0 AS `total_pay_order_number`,0 AS `total_money`,0 AS `avg_user_money`,0 AS `new_user_number`,0 AS `new_agent_number`,0 AS `refund_number`,0 AS `refund_money`,0 AS `consumer`,0 AS `fission_number`,count(distinct `jiecheng_visit`.`user_id`) AS `fission_visit`,count(distinct `jiecheng_visit`.`user_id`,`jiecheng_visit`.`url`) AS `fission_click`,`jiecheng_visit`.`mall_id` AS `mall_id`,date_format(`jiecheng_visit`.`create_time`,'%Y-%m-%d') AS `create_time`,`jiecheng_visit`.`merchant_id` AS `merchant_id` from `jiecheng_visit` where ((`jiecheng_visit`.`is_fission` = 1) and (`jiecheng_visit`.`create_time` < date_format(now(),'%Y-%m-%d'))) group by `jiecheng_visit`.`mall_id`,`jiecheng_visit`.`create_time`,`jiecheng_visit`.`merchant_id`)) `t` group by `t`.`mall_id`,`t`.`create_time`,`t`.`merchant_id`;

-- ----------------------------
-- View structure for jiecheng_merchant_total_statistics
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_merchant_total_statistics`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_merchant_total_statistics` AS select sum(`t`.`user_number`) AS `user_number`,sum(`t`.`lost_user`) AS `lost_user`,sum(`t`.`today_user_number`) AS `today_user_number`,sum(`t`.`agent_total`) AS `agent_total`,sum(`t`.`agent_amount`) AS `agent_amount`,sum(`t`.`agent_cash`) AS `agent_cash`,sum(`t`.`agent_apply`) AS `agent_apply`,sum(`t`.`agent_confirm`) AS `agent_confirm`,sum(`t`.`consignment`) AS `consignment`,sum(`t`.`consumers`) AS `consumers`,sum(`t`.`turnover`) AS `turnover`,sum(`t`.`order_number`) AS `order_number`,sum(`t`.`total_order_money`) AS `total_order_money`,sum(`t`.`refund_number`) AS `refund_number`,sum(`t`.`refund_money`) AS `refund_money`,sum(`t`.`today_order_number`) AS `today_order_number`,sum(`t`.`stay_write_off`) AS `stay_write_off`,sum(`t`.`after_sales`) AS `after_sales`,sum(`t`.`today_pay_order_number`) AS `today_pay_order_number`,sum(`t`.`today_total_order_money`) AS `today_total_order_money`,sum(`t`.`today_consumers`) AS `today_consumers`,sum(`t`.`today_refund_number`) AS `today_refund_number`,sum(`t`.`today_refund_money`) AS `today_refund_money`,sum(`t`.`goods_number`) AS `goods_number`,sum(`t`.`sold_out`) AS `sold_out`,sum(`t`.`warehouse`) AS `warehouse`,round((sum(`t`.`total_order_money`) / sum(`t`.`consumers`)),2) AS `avg_user_money`,round((sum(`t`.`today_total_order_money`) / sum(`t`.`today_order_number`)),2) AS `today_avg_order_money`,round((sum(`t`.`today_total_order_money`) / sum(`t`.`today_consumers`)),2) AS `today_avg_user_money`,round((sum(`t`.`refund_number`) / sum(`t`.`order_number`)),4) AS `today_refund_rate`,`t`.`mall_id` AS `mall_id`,`t`.`merchant_id` AS `merchant_id` from ((select `u`.`merchant_id` AS `merchant_id`,`u`.`mall_id` AS `mall_id`,count(0) AS `user_number`,sum((`u`.`update_time` < (now() + interval -(1) month))) AS `lost_user`,sum((`u`.`create_time` > date_format(now(),'%Y-%m-%d'))) AS `today_user_number`,sum((`a`.`id` is not null)) AS `agent_total`,sum(`c`.`agent_total`) AS `agent_amount`,sum(`c`.`agent`) AS `agent_cash`,sum(`d`.`number`) AS `agent_apply`,sum(`b`.`agent_confirm`) AS `agent_confirm`,0 AS `consignment`,0 AS `consumers`,0 AS `turnover`,0 AS `order_number`,0 AS `total_order_money`,0 AS `refund_number`,0 AS `refund_money`,0 AS `today_order_number`,0 AS `stay_write_off`,0 AS `after_sales`,0 AS `today_pay_order_number`,0 AS `today_total_order_money`,0 AS `today_consumers`,0 AS `today_refund_number`,0 AS `today_refund_money`,0 AS `goods_number`,0 AS `sold_out`,0 AS `warehouse` from ((((`jiecheng_user` `u` left join `jiecheng_agent` `a` on((`a`.`user_id` = `u`.`id`))) left join (select `jiecheng_agent_bill_temporary`.`user_id` AS `user_id`,sum(`jiecheng_agent_bill_temporary`.`money`) AS `money`,sum(`jiecheng_agent_bill_temporary`.`amount`) AS `agent_confirm` from `jiecheng_agent_bill_temporary` where (`jiecheng_agent_bill_temporary`.`status` = 1) group by `jiecheng_agent_bill_temporary`.`user_id`) `b` on((`b`.`user_id` = `u`.`id`))) left join `jiecheng_user_cash` `c` on((`c`.`user_id` = `u`.`id`))) left join (select `jiecheng_agent_apply`.`user_id` AS `user_id`,count(0) AS `number` from `jiecheng_agent_apply` where (`jiecheng_agent_apply`.`status` = 0) group by `jiecheng_agent_apply`.`user_id`) `d` on((`d`.`user_id` = `u`.`id`))) group by `u`.`mall_id`,`u`.`merchant_id`) union (select `jiecheng_order`.`merchant_id` AS `merchant_id`,`jiecheng_order`.`mall_id` AS `mall_id`,0 AS `user_number`,0 AS `lost_user`,0 AS `today_user_number`,0 AS `agent_total`,0 AS `agent_amount`,0 AS `agent_cash`,0 AS `agent_apply`,0 AS `agent_confirm`,sum((`jiecheng_order`.`status` = 2)) AS `consignment`,count(distinct `jiecheng_order`.`user_id`) AS `consumers`,sum(((not(`jiecheng_order`.`id` in (select `jiecheng_order_after`.`order_id` from `jiecheng_order_after` where (`jiecheng_order`.`status` in (5,8,11))))) and (`jiecheng_order`.`status` not in (1,12)))) AS `turnover`,count(0) AS `order_number`,sum(if((`jiecheng_order`.`status` in (1,11)),0,`jiecheng_order`.`money`)) AS `total_order_money`,sum(((`jiecheng_order`.`status` not in (1,12)) and (`jiecheng_order`.`create_time` > date_format(now(),'%Y-%m-%d')))) AS `today_order_number`,sum(`yyyy`.`refund_number`) AS `refund_number`,sum(truncate(`yyyy`.`refund_money`,2)) AS `refund_money`,sum(((`jiecheng_order`.`status` = 2) and (`jiecheng_order`.`order_type` = 2))) AS `stay_write_off`,sum((`jiecheng_order`.`after_status` = 1)) AS `after_sales`,sum(((`jiecheng_order`.`status` in (2,3,4,5,6,7,8)) and (`jiecheng_order`.`create_time` > date_format(now(),'%Y-%m-%d')))) AS `today_pay_order_number`,sum(if(((`jiecheng_order`.`create_time` > date_format(now(),'%Y-%m-%d')) and (`jiecheng_order`.`status` in (2,3,4,5,6,7,8))),`jiecheng_order`.`money`,0)) AS `today_total_order_money`,count(distinct if(((`jiecheng_order`.`status` in (2,3,4,5,6,7,8)) and (`jiecheng_order`.`create_time` > date_format(now(),'%Y-%m-%d'))),`jiecheng_order`.`user_id`,NULL)) AS `today_consumers`,sum(`uuuu`.`today_refund_number`) AS `today_refund_number`,sum(`uuuu`.`today_refund_money`) AS `today_refund_money`,0 AS `goods_number`,0 AS `sold_out`,0 AS `warehouse` from ((`jiecheng_order` left join (select count(distinct `aaaa`.`order_id`) AS `refund_number`,sum(`aaaa`.`money`) AS `refund_money`,`aaaa`.`order_id` AS `order_id` from (select `jiecheng_order_commodity`.`order_id` AS `order_id`,`jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`mall_id` AS `mall_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`type` = 3) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d'))) union all select `jiecheng_order_after`.`order_id` AS `order_id`,`jiecheng_order_after`.`money` AS `money`,`jiecheng_order_after`.`mall_id` AS `mall_id` from `jiecheng_order_after` where ((`jiecheng_order_after`.`state` in (2,5,8,11)) and (`jiecheng_order_after`.`update_time` > date_format(now(),'%Y-%m-%d')))) `aaaa` group by `aaaa`.`order_id`) `yyyy` on((`yyyy`.`order_id` = `jiecheng_order`.`id`))) left join (select count(distinct `ssss`.`order_id`) AS `today_refund_number`,sum(`ssss`.`money`) AS `today_refund_money`,`ssss`.`order_id` AS `order_id` from (select `jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`order_id` AS `order_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`type` = 3) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d'))) union select `jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`order_id` AS `order_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`after_status` = 2) and (`jiecheng_order_commodity`.`after_type` in (1,2)) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d')))) `ssss` group by `ssss`.`order_id`) `uuuu` on((`uuuu`.`order_id` = `jiecheng_order`.`id`))) where isnull(`jiecheng_order`.`delete_time`) group by `jiecheng_order`.`mall_id`,`jiecheng_order`.`merchant_id`) union (select `jiecheng_commodity`.`merchant_id` AS `merchant_id`,`jiecheng_commodity`.`mall_id` AS `mall_id`,0 AS `user_number`,0 AS `lost_user`,0 AS `today_user_number`,0 AS `agent_total`,0 AS `agent_amount`,0 AS `agnet_cash`,0 AS `agent_apply`,0 AS `agent_confirm`,0 AS `consignment`,0 AS `consumers`,0 AS `turnover`,0 AS `order_number`,0 AS `total_order_money`,0 AS `refund_number`,0 AS `refund_money`,0 AS `today_order_number`,0 AS `stay_write_off`,0 AS `after_sales`,0 AS `today_pay_order_number`,0 AS `today_total_order_money`,0 AS `today_consumers`,0 AS `today_refund_number`,0 AS `today_refund_money`,count(0) AS `goods_number`,sum((`jiecheng_commodity`.`total` = `jiecheng_commodity`.`sell`)) AS `sold_out`,sum((`jiecheng_commodity`.`status` = 0)) AS `warehouse` from `jiecheng_commodity` where isnull(`jiecheng_commodity`.`delete_time`) group by `jiecheng_commodity`.`mall_id`,`jiecheng_commodity`.`merchant_id`)) `t` group by `t`.`mall_id`,`t`.`merchant_id`;

-- ----------------------------
-- View structure for jiecheng_order_commodity_list_view
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_order_commodity_list_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_order_commodity_list_view` AS select `A`.`order_id` AS `order_id`,`A`.`count` AS `count`,`A`.`status` AS `status`,`A`.`sku_id` AS `sku_id`,`A`.`store_id` AS `store_id`,`A`.`money` AS `money`,`A`.`logistics_no` AS `logistics_no`,`A`.`express_company` AS `express_company`,`A`.`name` AS `name`,`A`.`master` AS `master`,`A`.`id` AS `id`,`A`.`type` AS `type`,`A`.`classify_value` AS `classify_value`,`A`.`has_sku` AS `has_sku`,`A`.`ocid` AS `ocid`,`A`.`pvs_value` AS `pvs_value`,`A`.`attach_value` AS `attach_value`,`A`.`gc_id` AS `gc_id`,`A`.`collage_id` AS `collage_id`,`A`.`money_type` AS `money_type`,`A`.`order_type` AS `order_type`,concat(`A`.`order_id`,`A`.`money_type`) AS `mid` from (select `jiecheng_order_commodity`.`order_id` AS `order_id`,`jiecheng_order_commodity`.`count` AS `count`,`jiecheng_order_commodity`.`status` AS `status`,`jiecheng_order_commodity`.`sku_id` AS `sku_id`,`jiecheng_order_commodity`.`store_id` AS `store_id`,`jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`logistics_no` AS `logistics_no`,`jiecheng_order_commodity`.`express_company` AS `express_company`,`jiecheng_commodity`.`name` AS `name`,`jiecheng_commodity`.`master` AS `master`,`jiecheng_commodity`.`id` AS `id`,`jiecheng_commodity`.`type` AS `type`,`jiecheng_commodity`.`classify_value` AS `classify_value`,`jiecheng_commodity`.`has_sku` AS `has_sku`,`jiecheng_commodity`.`type` AS `order_type`,`jiecheng_order_commodity`.`id` AS `ocid`,`jiecheng_sku`.`pvs_value` AS `pvs_value`,`jiecheng_order_commodity`.`attach_value` AS `attach_value`,`jiecheng_order_commodity`.`gc_id` AS `gc_id`,`jiecheng_order_commodity`.`collage_id` AS `collage_id`,1 AS `money_type` from ((`jiecheng_order_commodity` left join `jiecheng_commodity` on((`jiecheng_order_commodity`.`commodity_id` = `jiecheng_commodity`.`id`))) left join `jiecheng_sku` on((`jiecheng_sku`.`id` = `jiecheng_order_commodity`.`sku_id`))) union select `jiecheng_integral_order_commodity`.`order_id` AS `order_id`,`jiecheng_integral_order_commodity`.`count` AS `count`,`jiecheng_integral_order_commodity`.`status` AS `status`,`jiecheng_integral_order_commodity`.`sku_id` AS `sku_id`,0 AS `store_id`,`jiecheng_integral_order_commodity`.`money` AS `money`,`jiecheng_integral_order_commodity`.`logistics_no` AS `logistics_no`,`jiecheng_integral_order_commodity`.`express_company` AS `express_company`,`jiecheng_integral_goods`.`name` AS `name`,`jiecheng_integral_goods`.`master` AS `master`,`jiecheng_integral_goods`.`id` AS `id`,`jiecheng_integral_goods`.`type` AS `order_type`,0 AS `type`,0 AS `classify_value`,`jiecheng_integral_goods`.`has_sku` AS `has_sku`,`jiecheng_integral_order_commodity`.`id` AS `ocid`,`jiecheng_integral_sku`.`pvs_value` AS `pvs_value`,0 AS `attach_value`,0 AS `gc_id`,0 AS `collage_id`,2 AS `money_type` from ((`jiecheng_integral_order_commodity` left join `jiecheng_integral_goods` on((`jiecheng_integral_order_commodity`.`commodity_id` = `jiecheng_integral_goods`.`id`))) left join `jiecheng_integral_sku` on((`jiecheng_integral_sku`.`id` = `jiecheng_integral_order_commodity`.`sku_id`)))) `A`;

-- ----------------------------
-- View structure for jiecheng_order_list_view
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_order_list_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_order_list_view` AS select `a`.`id` AS `id`,`a`.`mall_id` AS `mall_id`,`a`.`order_no` AS `order_no`,`a`.`status` AS `status`,`a`.`money` AS `money`,`a`.`pay_time` AS `pay_time`,`a`.`address` AS `address`,`a`.`user_id` AS `user_id`,`a`.`create_time` AS `create_time`,`a`.`money_type` AS `money_type`,`a`.`discount` AS `discount`,`a`.`is_evaluate` AS `is_evaluate`,concat(`a`.`id`,`a`.`money_type`) AS `mid`,`a`.`order_type` AS `order_type`,concat(`a`.`status`,`a`.`money_type`) AS `mstatus`,`a`.`freight` AS `freight`,`a`.`has_attach` AS `has_attach`,`a`.`distribution_fee` AS `distribution_fee`,`a`.`order_from` AS `order_from` from (select `jiecheng_order`.`id` AS `id`,`jiecheng_order`.`mall_id` AS `mall_id`,`jiecheng_order`.`order_no` AS `order_no`,`jiecheng_order`.`status` AS `status`,`jiecheng_order`.`order_type` AS `order_type`,((`jiecheng_order`.`money` - ifnull(`jiecheng_order`.`discount`,0)) - ifnull(`jiecheng_order`.`discount_amount`,0)) AS `money`,`jiecheng_order`.`pay_time` AS `pay_time`,`jiecheng_order`.`user_id` AS `user_id`,`jiecheng_order`.`create_time` AS `create_time`,`jiecheng_order`.`address` AS `address`,1 AS `money_type`,`jiecheng_order`.`discount` AS `discount`,`jiecheng_order`.`is_evaluate` AS `is_evaluate`,any_value(`jiecheng_order_freight`.`freight`) AS `freight`,`jiecheng_order`.`has_attach` AS `has_attach`,`jiecheng_order`.`distribution_fee` AS `distribution_fee`,`jiecheng_order`.`order_from` AS `order_from` from (`jiecheng_order` left join `jiecheng_order_freight` on((`jiecheng_order`.`id` = `jiecheng_order_freight`.`order_id`))) where isnull(`jiecheng_order`.`delete_time`) union select `jiecheng_integral_order`.`id` AS `id`,`jiecheng_integral_order`.`mall_id` AS `mall_id`,`jiecheng_integral_order`.`order_no` AS `order_no`,`jiecheng_integral_order`.`status` AS `status`,`jiecheng_integral_order`.`order_type` AS `order_type`,`jiecheng_integral_order`.`money` AS `money`,`jiecheng_integral_order`.`pay_time` AS `pay_time`,`jiecheng_integral_order`.`user_id` AS `user_id`,`jiecheng_integral_order`.`create_time` AS `create_time`,`jiecheng_integral_order`.`address` AS `address`,2 AS `money_type`,0 AS `discount`,0 AS `is_evaluate`,0 AS `freight`,0 AS `has_attach`,0 AS `distribution_fee`,0 AS `order_from` from `jiecheng_integral_order` where isnull(`jiecheng_integral_order`.`delete_time`)) `a` order by `a`.`create_time`;

-- ----------------------------
-- View structure for jiecheng_sales_ranking
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_sales_ranking`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_sales_ranking` AS select `c`.`name` AS `name`,ifnull(`y`.`number`,0) AS `number`,ifnull(`y`.`money`,0) AS `money`,`c`.`status` AS `status`,`c`.`merchant_id` AS `merchant_id`,`c`.`mall_id` AS `mall_id` from (`jiecheng_commodity` `c` left join (select sum(`jiecheng_order_commodity`.`count`) AS `number`,sum(`jiecheng_order_commodity`.`money`) AS `money`,`jiecheng_order_commodity`.`commodity_id` AS `commodity_id` from `jiecheng_order_commodity` group by `jiecheng_order_commodity`.`commodity_id`) `y` on((`c`.`id` = `y`.`commodity_id`))) order by ifnull(`y`.`number`,0) desc;

-- ----------------------------
-- View structure for jiecheng_statistics
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_statistics`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_statistics` AS select sum(`t`.`uv`) AS `uv`,sum(`t`.`pv`) AS `pv`,sum(`t`.`avg_money`) AS `avg_money`,sum((`t`.`total_money` - `t`.`refund_money`)) AS `turnover`,sum(`t`.`total_number`) AS `total_number`,sum(`t`.`total_pay_order_number`) AS `total_pay_order_number`,sum(`t`.`total_money`) AS `total_money`,sum(`t`.`consumer`) AS `consumer`,round(((sum(`t`.`total_money`) - sum(`t`.`refund_money`)) / sum(`t`.`consumer`)),2) AS `avg_user_money`,sum(`t`.`consumer`) AS `number`,sum(`t`.`new_user_number`) AS `new_user_number`,sum((`t`.`payment_money` + `t`.`refund_money`)) AS `payment_money`,sum(`t`.`refund_number`) AS `refund_number`,round((sum(`t`.`refund_number`) / sum(`t`.`total_number`)),4) AS `refund_rate`,sum(`t`.`refund_money`) AS `refund_money`,sum(`t`.`fission_number`) AS `fission_number`,sum(`t`.`fission_click`) AS `fission_click`,`t`.`mall_id` AS `mall_id`,round(unix_timestamp(`t`.`create_time`),0) AS `create_time` from ((select 0 AS `avg_money`,0 AS `payment_money`,0 AS `total_number`,0 AS `total_pay_order_number`,0 AS `total_money`,0 AS `avg_user_money`,0 AS `new_user_number`,0 AS `refund_number`,0 AS `refund_money`,0 AS `consumer`,0 AS `fission_number`,sum(`jiecheng_total`.`pv`) AS `pv`,sum(`jiecheng_total`.`uv`) AS `uv`,0 AS `fission_click`,`jiecheng_total`.`mall_id` AS `mall_id`,date_format(`jiecheng_total`.`create_time`,'%Y-%m-%d %H:%i:%s') AS `create_time` from `jiecheng_total` group by `jiecheng_total`.`mall_id`,`jiecheng_total`.`create_time`) union select round(avg(`jiecheng_order`.`money`),2) AS `avg_money`,sum(if((`jiecheng_order`.`status` = 1),0,`jiecheng_order`.`money`)) AS `payment_money`,count(0) AS `total_number`,sum((`jiecheng_order`.`status` in (2,3,4,5,6,7,8))) AS `total_pay_order_number`,sum(if((`jiecheng_order`.`status` in (2,3,4,5,6,7,8)),`jiecheng_order`.`money`,0)) AS `total_money`,0 AS `avg_user_money`,0 AS `new_user_number`,sum(`oc`.`refund_number`) AS `refund_number`,sum(`oc`.`refund_money`) AS `refund_money`,count(distinct `jiecheng_order`.`user_id`) AS `consumer`,0 AS `fission_number`,0 AS `pv`,0 AS `uv`,0 AS `fission_click`,`jiecheng_order`.`mall_id` AS `mall_id`,date_format(`jiecheng_order`.`create_time`,'%Y-%m-%d %H:%i:%s') AS `create_time` from (`jiecheng_order` left join (select count(distinct `yyyy`.`order_id`) AS `refund_number`,truncate(sum(`yyyy`.`money`),2) AS `refund_money`,`yyyy`.`order_id` AS `order_id` from (select `jiecheng_order_commodity`.`order_id` AS `order_id`,`jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`mall_id` AS `mall_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`type` = 3) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d %H:%i:%s'))) union all select `jiecheng_order_after`.`order_id` AS `order_id`,`jiecheng_order_after`.`money` AS `money`,`jiecheng_order_after`.`mall_id` AS `mall_id` from `jiecheng_order_after` where ((`jiecheng_order_after`.`state` in (2,5,8,11)) and (`jiecheng_order_after`.`update_time` > date_format(now(),'%Y-%m-%d %H:%i:%s')))) `yyyy` group by `yyyy`.`order_id`) `oc` on((`oc`.`order_id` = `jiecheng_order`.`id`))) where (`jiecheng_order`.`create_time` < date_format(now(),'%Y-%m-%d %H:%i:%s')) group by `jiecheng_order`.`mall_id`,`jiecheng_order`.`create_time` union (select 0 AS `avg_money`,0 AS `payment_money`,0 AS `total_number`,0 AS `total_pay_order_number`,0 AS `total_money`,0 AS `avg_user_money`,count(0) AS `new_user_number`,0 AS `refund_number`,0 AS `refund_money`,0 AS `consumer`,0 AS `fission_number`,0 AS `pv`,0 AS `uv`,0 AS `fission_click`,`jiecheng_user`.`mall_id` AS `mall_id`,date_format(`jiecheng_user`.`create_time`,'%Y-%m-%d %H:%i:%s') AS `create_time` from (`jiecheng_user` left join `jiecheng_user_cash` on((`jiecheng_user_cash`.`user_id` = `jiecheng_user`.`id`))) where (`jiecheng_user`.`create_time` < date_format(now(),'%Y-%m-%d %H:%i:%s')) group by `jiecheng_user`.`mall_id`,`jiecheng_user`.`create_time`) union (select 0 AS `avg_money`,0 AS `payment_money`,0 AS `total_number`,0 AS `total_pay_order_number`,0 AS `total_money`,0 AS `avg_user_money`,0 AS `new_user_number`,0 AS `refund_number`,0 AS `refund_money`,0 AS `consumer`,count(distinct `jiecheng_visit`.`ip`) AS `fission_number`,0 AS `pv`,0 AS `uv`,count(distinct `jiecheng_visit`.`ip`,`jiecheng_visit`.`url`) AS `fission_click`,`jiecheng_visit`.`mall_id` AS `mall_id`,date_format(`jiecheng_visit`.`create_time`,'%Y-%m-%d %H:%i:%s') AS `create_time` from `jiecheng_visit` where (`jiecheng_visit`.`is_fission` = 1) group by `jiecheng_visit`.`mall_id`,`jiecheng_visit`.`create_time`)) `t` group by `t`.`mall_id`,`t`.`create_time`;
-- ----------------------------
-- View structure for jiecheng_total_statistics
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_total_statistics`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_total_statistics` AS select `uv`.`uv` AS `uv`,`uv`.`pv` AS `pv`,`t`.`user_number` AS `user_number`,`t`.`today_user_number` AS `today_user_number`,`o`.`consignment` AS `consignment`,`oall`.`order_number` AS `order_number`,`o`.`consumers` AS `consumers`,`o`.`turnover` AS `turnover`,`o`.`total_order_money` AS `total_order_money`,`t`.`agent_total` AS `agent_total`,`g`.`goods_number` AS `goods_number`,`a`.`agent_apply` AS `agent_apply`,`ab`.`agent_confirm` AS `agent_confirm`,`u`.`agent_amount` AS `agent_amount`,`u`.`agent_cash` AS `agent_cash`,`o`.`stay_write_off` AS `stay_write_off`,`o`.`after_sales` AS `after_sales`,`g`.`sold_out` AS `sold_out`,`g`.`warehouse` AS `warehouse`,`oanew`.`today_refund_number` AS `today_refund_number`,`oanew`.`today_refund_money` AS `today_refund_money`,`onew`.`today_order_number` AS `today_order_number`,`onew`.`today_pay_order_number` AS `today_pay_order_number`,`onew`.`today_avg_order_money` AS `today_avg_order_money`,`onew`.`today_total_order_money` AS `today_total_order_money`,round((`onew`.`today_total_order_money` / `onew`.`today_consumers`),2) AS `today_avg_user_money`,`onew`.`today_consumers` AS `today_consumers`,`t`.`mall_id` AS `mall_id` from (((((((((((select count(0) AS `user_number`,sum((`jiecheng_user`.`create_time` > date_format(now(),'%Y-%m-%d'))) AS `today_user_number`,sum(((`jiecheng_user`.`agent_id` > 0) and (`jiecheng_user`.`agent_id` is not null))) AS `agent_total`,`jiecheng_user`.`mall_id` AS `mall_id` from `jiecheng_user` group by `jiecheng_user`.`mall_id`)) `t` left join (select sum(`jiecheng_total`.`uv`) AS `uv`,sum(`jiecheng_total`.`pv`) AS `pv`,`jiecheng_total`.`mall_id` AS `mall_id` from `jiecheng_total` group by `jiecheng_total`.`mall_id`) `uv` on((`t`.`mall_id` = `uv`.`mall_id`))) left join (select sum(((`jiecheng_order`.`status` = 2) and (`jiecheng_order`.`order_type` <> 2))) AS `consignment`,count(distinct `jiecheng_order`.`user_id`) AS `consumers`,count(0) AS `turnover`,sum(`jiecheng_order`.`money`) AS `total_order_money`,sum(((`jiecheng_order`.`status` = 2) and (`jiecheng_order`.`order_type` = 2))) AS `stay_write_off`,sum((`jiecheng_order`.`after_status` = 1)) AS `after_sales`,`jiecheng_order`.`mall_id` AS `mall_id` from `jiecheng_order` where (isnull(`jiecheng_order`.`delete_time`) and (`jiecheng_order`.`status` not in (1,12)) and (not(`jiecheng_order`.`id` in (select `jiecheng_order_after`.`order_id` from `jiecheng_order_after` where (`jiecheng_order_after`.`state` in (5,8,11)))))) group by `jiecheng_order`.`mall_id`) `o` on((`t`.`mall_id` = `o`.`mall_id`))) left join (select count(0) AS `goods_number`,sum((`jiecheng_commodity`.`total` = `jiecheng_commodity`.`sell`)) AS `sold_out`,sum((`jiecheng_commodity`.`status` = 0)) AS `warehouse`,`jiecheng_commodity`.`mall_id` AS `mall_id` from `jiecheng_commodity` where isnull(`jiecheng_commodity`.`delete_time`) group by `jiecheng_commodity`.`mall_id`) `g` on((`t`.`mall_id` = `g`.`mall_id`))) left join (select count(0) AS `agent_apply`,`jiecheng_agent_apply`.`mall_id` AS `mall_id` from `jiecheng_agent_apply` where (isnull(`jiecheng_agent_apply`.`delete_time`) and (`jiecheng_agent_apply`.`status` = 0)) group by `jiecheng_agent_apply`.`mall_id`) `a` on((`t`.`mall_id` = `a`.`mall_id`))) left join (select sum(`jiecheng_agent_bill_temporary`.`amount`) AS `agent_confirm`,`jiecheng_agent_bill_temporary`.`mall_id` AS `mall_id` from ((`jiecheng_agent_bill_temporary` left join `jiecheng_order` `o` on((`o`.`id` = `jiecheng_agent_bill_temporary`.`order_id`))) left join `jiecheng_user` `u` on((`jiecheng_agent_bill_temporary`.`user_id` = `u`.`id`))) where ((`jiecheng_agent_bill_temporary`.`status` = 0) and (`o`.`status` in (2,3,4,5,6,7,8)) and (`u`.`agent_id` is not null)) group by `jiecheng_agent_bill_temporary`.`mall_id`) `ab` on((`t`.`mall_id` = `ab`.`mall_id`))) left join (select sum(`jiecheng_user_cash`.`agent_total`) AS `agent_amount`,sum(`jiecheng_user_cash`.`agent`) AS `agent_cash`,`jiecheng_user_cash`.`mall_id` AS `mall_id` from `jiecheng_user_cash` group by `jiecheng_user_cash`.`mall_id`) `u` on((`t`.`mall_id` = `u`.`mall_id`))) left join (select count(0) AS `today_order_number`,sum((`jiecheng_order`.`status` in (2,3,4,5,6,7,8))) AS `today_pay_order_number`,round(avg(`jiecheng_order`.`money`),2) AS `today_avg_order_money`,sum(if((`jiecheng_order`.`status` in (2,3,4,5,6,7,8)),`jiecheng_order`.`money`,0)) AS `today_total_order_money`,count(distinct `jiecheng_order`.`user_id`) AS `today_consumers`,`jiecheng_order`.`mall_id` AS `mall_id` from `jiecheng_order` where ((`jiecheng_order`.`create_time` > date_format(now(),'%Y-%m-%d')) and (`jiecheng_order`.`status` in (2,3,4,5,6,7,8))) group by `jiecheng_order`.`mall_id`) `onew` on((`t`.`mall_id` = `onew`.`mall_id`))) left join (select count(distinct `yyyy`.`order_id`) AS `today_refund_number`,truncate(sum(`yyyy`.`money`),2) AS `today_refund_money`,`yyyy`.`mall_id` AS `mall_id` from (select `jiecheng_order_commodity`.`order_id` AS `order_id`,`jiecheng_order_commodity`.`money` AS `money`,`jiecheng_order_commodity`.`mall_id` AS `mall_id` from `jiecheng_order_commodity` where ((`jiecheng_order_commodity`.`type` = 3) and (`jiecheng_order_commodity`.`update_time` > date_format(now(),'%Y-%m-%d'))) union all select `jiecheng_order_after`.`order_id` AS `order_id`,`jiecheng_order_after`.`money` AS `money`,`jiecheng_order_after`.`mall_id` AS `mall_id` from `jiecheng_order_after` where ((`jiecheng_order_after`.`state` in (2,5,8,11)) and (`jiecheng_order_after`.`update_time` > date_format(now(),'%Y-%m-%d')))) `yyyy` group by `yyyy`.`mall_id`) `oanew` on((`t`.`mall_id` = `oanew`.`mall_id`))) left join (select count(0) AS `order_number`,`jiecheng_order`.`mall_id` AS `mall_id` from `jiecheng_order` group by `jiecheng_order`.`mall_id`) `oall` on((`t`.`mall_id` = `oall`.`mall_id`)));

-- ----------------------------
-- View structure for jiecheng_user_total_count
-- ----------------------------
DROP VIEW IF EXISTS `jiecheng_user_total_count`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `jiecheng_user_total_count` AS select sum((isnull(`jiecheng_order_after`.`id`) and (`jiecheng_order`.`status` <> 12))) AS `order_number`,sum(if((isnull(`jiecheng_order_after`.`id`) and (`jiecheng_order`.`status` <> 12)),`jiecheng_order`.`money`,0)) AS `order_money`,sum((`jiecheng_order_after`.`id` is not null)) AS `refund_number`,truncate(sum(ifnull(`jiecheng_order_after`.`money`,0)),2) AS `refund_money`,`jiecheng_order`.`user_id` AS `user_id` from (`jiecheng_order` left join `jiecheng_order_after` on(((`jiecheng_order_after`.`order_id` = `jiecheng_order`.`id`) and (`jiecheng_order_after`.`state` in (2,5,8,11))))) where (`jiecheng_order`.`status` > 1) group by `jiecheng_order`.`user_id`;

-- ----------------------------
-- Procedure structure for jiecheng_user_total_count_by_time
-- ----------------------------
DROP PROCEDURE IF EXISTS `jiecheng_user_total_count_by_time`;
delimiter ;;
CREATE PROCEDURE `jiecheng_user_total_count_by_time`(IN `order_time` datetime, IN total_user_id INT)
BEGIN
  #Routine body goes here...
  SELECT
  sum((
      isnull( `jiecheng_order_after`.`id` )
    AND ( `jiecheng_order`.`status` <> 12 ))) AS `order_number`,
  sum(
  IF
  (( isnull( `jiecheng_order_after`.`id` ) AND ( `jiecheng_order`.`status` <> 12 )), `jiecheng_order`.`money`, 0 )) AS `order_money`,
  sum((
      `jiecheng_order_after`.`id` IS NOT NULL
    )) AS `refund_number`,
  TRUNCATE ( sum( ifnull( `jiecheng_order_after`.`money`, 0 )), 2 ) AS `refund_money`,
  `jiecheng_order`.`user_id` AS `user_id`
FROM
  (
    `jiecheng_order`
    LEFT JOIN `jiecheng_order_after` ON (((
          `jiecheng_order_after`.`order_id` = `jiecheng_order`.`id`
          )
        AND (
        `jiecheng_order_after`.`state` IN ( 2, 5, 8, 11 )))))
WHERE
  ( `jiecheng_order`.`status` > 1 AND `jiecheng_order`.`create_time` > order_time AND `jiecheng_order`.`user_id` = total_user_id)
GROUP BY
  `jiecheng_order`.`user_id`;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;



