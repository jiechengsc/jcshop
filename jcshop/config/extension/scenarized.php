<?php

return [
    "订单支付通知" => [
        1 => [
            'apply' => [
                "template_id_short" => "OPENTM400231951"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "订单支付成功",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "order_cash",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "order_no",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "您已支付成功,点击查看详情",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
        2 => [
            'apply' => [
                "tid" => 989,
                "kidList" => [1, 5, 2, 3, 4],
                "sceneDesc" => "购买成功通知"
            ],
            'template' => [
                "page" => "xxxxx",
                "data" => [
                    "character_string1" => [
                        "value" => "order_no",
                    ],
                    "name5" => [
                        "value" => "commodity_name",
                    ],
                    "amount2" => [
                        "value" => "order_cash",
                    ],
                    "date3" => [
                        "value" => "order_create_time",
                    ],
                    "thing4" => [
                        "flag" => true,
                        "value" => "您已支付成功,点击查看订单详情",
                    ],
                ]
            ],
        ]
    ],
    "订单发货通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM414956350"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "您购买的订单已经发货啦,正快马加鞭向您飞奔而去",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "order_no",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "order_sell_time",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "express_name",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "express_no",
                        "color" => "#000000"
                    ],
                    "keyword5" => [
                        "value" => "buyer_info",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "请保持收件手机畅通!",
                        "color" => "#000000"
                    ],
                ]
            ],
        ],
        2 => [
            'apply' => [
                "tid" => 855,
                "kidList" => [1, 2, 3, 4, 5],
                "sceneDesc" => "订单发货通知"
            ],
            'template' => [
                "page" => "xxxxx",
                "data" => [
                    "character_string1" => [
                        "value" => "order_no",
                    ],
                    "thing2" => [
                        "value" => "commodity_name",
                    ],
                    "phrase3" => [
                        "value" => "express_name",
                    ],
                    "character_string4" => [
                        "value" => "express_no",
                    ],
                    "date5" => [
                        "value" => "order_sell_time",
                    ],
                ]
            ],
        ]
    ],
    "订单手动退款通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM415835502"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "尊敬的顾客,您的退款申请已通过审核",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "commodity_refund_name",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "refund_cash",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "order_create_time",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "order_no",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "商品金额已退回至您的账户,请您及时查收.",
                        "color" => "#000000"
                    ],
                ]
            ],
        ]
    ],
    "提现成功通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM411207151"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "提现成功",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "withdraw_cash",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "withdraw_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "感谢您的使用",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
        2 => [
            'apply' => [
                "tid" => 2001,
                "kidList" => [1, 5],
                "sceneDesc" => "提现成功通知"
            ],
            'template' => [
                "page" => "xxxxx",
                "data" => [
                    "amount1" => [
                        "value" => "withdraw_cash",
                    ],
                    "time5" => [
                        "value" => "withdraw_time",
                    ],
                ]
            ],
        ]
    ],
    "充值成功通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM417992877"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "您的充值已成功到账",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "recharge_user",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "recharge_reality_cash",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "recharge_type",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "recharge_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "点击查看您的余额",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
        2 => [
            'apply' => [
                "tid" => 4531,
                "kidList" => [2, 4, 5],
                "sceneDesc" => "充值成功通知"
            ],
            'template' => [
                "page" => "xxxxx",
                "data" => [
                    "amount2" => [
                        "value" => "recharge_reality_cash",
                    ],
                    "time4" => [
                        "value" => "recharge_time",
                    ],
                    "thing5" => [
                        "flag" => true,
                        "value" => "充值成功后点击查看详情",
                    ],
                ]
            ],
        ]
    ],
    "积分变更通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM207379626"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "您的积分已变更",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "integral_user",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "integral_residue",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "integral_change",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "integral_total",
                        "color" => "#000000"
                    ],
                    "keyword5" => [
                        "value" => "integral_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "点击查看详情!",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
    ],
    "优惠券发放通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "TM00279"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "优惠券发放成功",
                        "color" => "#000000"
                    ],
                    "Apply_id" => [
                        "value" => "coupon_user",
                        "color" => "#000000"
                    ],
                    "Apply_Type" => [
                        "value" => "coupon_name",
                        "color" => "#000000"
                    ],
                    "Apply_State" => [
                        "value" => "coupon_validity",
                        "color" => "#000000"
                    ],
                    "Apply_CreateTime" => [
                        "value" => "coupon_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "点击查看详情!!!",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
    ],
    "订单付款通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM412822604"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "您有新的订单消息",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "order_no",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "order_commodity",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "order_cash",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "buyer_name",
                        "color" => "#000000"
                    ],
                    "keyword5" => [
                        "value" => "buyer_mobile",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "请及时处理订单",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
    ],
    "订单收货通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM411154202"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "您好,您有订单客户已收货",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "user_name",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "order_no",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "order_cash",
                        "color" => "#000000"
                    ],
                    "keyword4" => [
                        "value" => "order_create_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "感谢您的使用",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
    ],
    "订单售后通知" => [
        1 => [
            "apply" => [
                "template_id_short" => "OPENTM410195709"
            ],
            "template" => [
                "url" => "xxxxx",
                "data" => [
                    "first" => [
                        "value" => "订单售后通知",
                        "color" => "#000000"
                    ],
                    "keyword1" => [
                        "value" => "safeguard_no",
                        "color" => "#000000"
                    ],
                    "keyword2" => [
                        "value" => "safeguard_type",
                        "color" => "#000000"
                    ],
                    "keyword3" => [
                        "value" => "safeguard_time",
                        "color" => "#000000"
                    ],
                    "remark" => [
                        "value" => "您有新的售后订单,请尽快处理!",
                        "color" => "#000000"
                    ],
                ]
            ]
        ],
    ],
];