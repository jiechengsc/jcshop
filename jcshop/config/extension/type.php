<?php

return [
    //base_type:1:基础模板,2:自定义
    1 => ["name" => "微信模板消息", "base_type" => [1, 2]],
    2 => ["name" => "微信小程序订阅消息", "base_type" => [1]],
    3 => ["name" => "短信通知", "base_type" => [2]]
];