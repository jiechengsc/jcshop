<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */

namespace app;

use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\db\exception\PDOException;
use think\Exception;
use think\exception\ErrorException;
use think\exception\Handle;
use think\exception\HttpException;
use think\exception\HttpResponseException;
use think\exception\InvalidArgumentException;
use think\exception\RouteNotFoundException;
use think\exception\ValidateException;
use think\Response;
use Throwable;


class ExceptionHandle extends Handle
{

    protected $ignoreReport = [
        HttpException::class,
        HttpResponseException::class,
        ModelNotFoundException::class,
        DataNotFoundException::class,
        ValidateException::class,
    ];


    public function report(Throwable $exception): void
    {
       
        parent::report($exception);
    }


    public function render($request, Throwable $e): Response
    {

       
        if ($e instanceof ValidateException) {
            return json(['error_msg'=>$e->getError()], HTTP_NOTACCEPT);
        }

       
        if ($e instanceof HttpException) {
            return json(['error_msg' => $e->getMessage()], $e->getStatusCode());
        }

       
        if ($e instanceof PDOException) {
            return json(['error_msg' => $e->getMessage()], HTTP_INVALID);
        }

       
        if ($e instanceof \PDOException) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }

       
        if ($e instanceof RouteNotFoundException) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }

       
        if ($e instanceof InvalidArgumentException) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }

       
        if ($e instanceof ErrorException) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }

       
        if ($e instanceof Exception) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }

        if ($e instanceof \Error) {
            return json(['error_msg' => $e->getMessage()], $e->getCode());
        }


       
        return parent::render($request, $e);
    }
}
