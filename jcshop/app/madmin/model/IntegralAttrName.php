<?php


namespace app\madmin\model;


class IntegralAttrName extends BaseModel
{
    public function attrValue()
    {
        return $this->hasMany(IntegralAttrValue::class,'attr_name_id','id')->field('id,value,attr_name_id')
            ->hidden(['attr_name_id']);
    }
}