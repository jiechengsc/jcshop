<?php


namespace app\madmin\model;

class IntegralSku extends BaseModel
{
    public function inventory(){
        return $this->hasOne(IntegralSkuInventory::class,'sku_id','id');
    }
}