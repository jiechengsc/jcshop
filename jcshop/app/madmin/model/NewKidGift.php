<?php


namespace app\madmin\model;


use think\Model;

class NewKidGift extends Model
{
    public function GetCouponDataAttr($value,$data){
        return Coupon::field('id,name')
            ->where('id','IN',json_decode($data['coupon_list'],true))
            ->select();
    }

    public function UserNicknamePicurl(){
        return $this->hasOne(User::class,'id','user_id')
            ->bind(['nickname','picurl']);
    }

    public function Config(){
        return $this->hasOne(NewKidGiftConfig::class,'id','new_kid_gift_id')
            ->bind(['name','config']);
    }
}