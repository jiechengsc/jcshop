<?php
declare (strict_types = 1);

namespace app\madmin\model;

use think\Exception;
use think\Model;
use think\model\concern\SoftDelete;

class Collage extends BaseModel
{
    protected $globalScope = ['mallId'];

    public function collageItem()
    {
        return $this->hasMany(CollageItem::class, 'collage_id', 'id');
    }

    public function commodity()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id');
    }

    public function groupCommodity()
    {
        return $this->belongsTo(GroupCommodity::class, 'gc_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function orderCommodity()
    {
        return $this->hasOne(OrderCommodity::class, 'id', 'oc_id');
    }
}
