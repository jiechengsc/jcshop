<?php


namespace app\madmin\model;


use think\Model;

class IntegralOrderCommodity extends Model
{
    public function goods(){
        return $this->hasOne(IntegralGoods::class,'id','commodity_id');
    }
    public function sellPrice(){
        return $this->hasOne(IntegralSkuInventory::class,'id','sku_id')
            ->bind(['sell_price']);
    }
    public function pvsValue(){
        return $this->hasOne(IntegralSku::class,'id','sku_id')
            ->bind(['pvs_value']);
    }
}