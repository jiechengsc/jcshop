<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class AssettransferLog extends BaseModel
{
    public static function write($write_data)
    {

        $data = [
            'create_time' => date("Y-m-d H:i:s", time())
        ];
        $data = array_merge($data, $write_data);
        $model = new self();
        $model->save($data);
        return $model;
    }

    //赠送者用户信息
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')->alias('user');
    }

    //被赠用户信息
    public function giftedUser()
    {
        return $this->hasOne(User::class, 'id', 'gifted_user_id')->alias('gifte_user');
    }

}
