<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class AgentBillTemporary extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }
    public function order()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未结算',
            1 => '已结算',
            2 => '已删除',
            3 => '已退款'
        ];
        return $status[$data['status']];
    }

    public function scopeSearch(Query $query,$data)
    {

        $query = $query->alias('a')
            ->join('order o', 'a.order_id=o.id', 'left')
            ->with(['agent','user','source'])
            ->where('o.status', 'in', [2, 3, 4, 5, 6, 7, 8])
            ->where('a.status', '<', 2)->order('o.update_time DESC');
        foreach ($data as $k=>$v){
            if($k=="start_time" ){
                $query->where('create_time','>',$v);
            }elseif($k=="end_time"){
                $query->where('create_time','<',$v);
            } else{
                if(!in_array($k,['page','size'])){
                    $query = $query->where('a.'.$k, $v);
                }
            }

        }

        return $query;
    }
    public function agent()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
    public function source()
    {
        return $this->hasOne(User::class, 'id', 'source_user_id');
    }

}
