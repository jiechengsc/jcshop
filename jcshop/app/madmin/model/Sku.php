<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

class Sku extends BaseModel
{

    protected $autoWriteTimestamp = false;

    public function __construct(array $data = [])
    {
        parent::__construct($data);

    }

    public function inventory()
    {
        return $this->hasOne(SkuInventory::class,'sku_id','id');
    }

}
