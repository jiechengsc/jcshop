<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class IntegralOrder extends Model
{
    use SoftDelete;
    protected $autoWriteTimestamp = 'datetime';
    public function getStatusTextAttr($value,$data) : string
    {
        $text = [
            0 => '待支付',
            1 => '待发货',
            2 => '待收货',
            3 => '已收货',
            4 => '已关闭'
        ];
        return $text[$data['status']];
    }

    public function getTypeTextAttr($value,$data) : string{
        $text = [
            1 => '快递',
            2 => '自提'
        ];
        return $text[$data['type']];
    }
    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }
    public function orderCommodity(){
        return $this->hasOne(IntegralOrderCommodity::class, 'order_id', 'id');
    }
    public function store(){
        return $this->hasOne(Store::class, 'id', 'store_id');
    }
}