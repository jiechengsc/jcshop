<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class FormTemplate extends Model
{
    use SoftDelete;
    protected $json = ['config'];
    protected $jsonAssoc = true;
}