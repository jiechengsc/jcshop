<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\model;

use think\Exception;
use app\madmin\validate\SeckillValidate;
use think\model\concern\SoftDelete;

class ActivityRecharge extends BaseModel
{
    use SoftDelete;
    protected $autoWriteTimestamp = 'datetime';
//    protected $globalScope = "mallId";

    public function __construct($data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        $status = [
            0 => '未开始',
            1 => '进行中',
            2 => '已结束'
        ];
        return $status[$data['status']];
    }

}
