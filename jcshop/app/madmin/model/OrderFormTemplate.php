<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class OrderFormTemplate extends Model
{
    use SoftDelete;
    protected $json = ['config'];
    protected $jsonAssoc = true;

    public function answers(){
        return $this->hasMany(OrderFormAnswer::class,'template_id','id');
    }
}