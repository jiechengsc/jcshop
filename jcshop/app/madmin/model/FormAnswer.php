<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class FormAnswer extends Model
{
    use SoftDelete;
    protected $json = ['answer'];
    protected $jsonAssoc = true;
    public function User(){
        return $this->hasOne(User::class,'id','user_id')
            ->field('id,nickname,picurl');
    }

}