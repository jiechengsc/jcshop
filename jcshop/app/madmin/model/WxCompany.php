<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\model\concern\SoftDelete;

class WxCompany extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function customer()
    {
        return $this->hasMany(WxCustomer::class, 'company_id', 'id');
    }
}
