<?php
declare (strict_types = 1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;

class MerchantApply extends BaseModel
{

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'申请中',1=>'通过',2=>'不通过'];
        return $status[$data['status']];
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')->bind(['user_name'=>'nickname']);
    }
}
