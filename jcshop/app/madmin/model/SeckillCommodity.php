<?php
namespace app\madmin\model;

class SeckillCommodity extends BaseModel
{

//    protected $globalScope = "mallId";

    public function __construct($data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        $status = [
            0 => '未开始',
            1 => '进行中',
            2 => '已结束'
        ];
        return $status[$data['status']];
    }


    public function goods()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id');
    }

    public function sku()
    {
        return $this->hasMany(SeckillCommoditySku::class, "sc_id", 'id');
    }
}
