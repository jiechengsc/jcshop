<?php


namespace app\madmin\model;


use think\Model;

class LiveGood extends Model
{
    public function goods(){
        return $this->hasOne(Goods::class,'id','goods_id')->field([                'id',
            'merchant_id',
            'type',
            'name',
            'subtitle',
            'master',
            'classify_id',
            'total',
            'sell',
            'original_price',
            'sell_price',
            'min_price',
            'max_price',
            'sort',
            'is_virtual',
            'virtual_sell',
            'is_distribution',
            'distribution',
            'has_sku',
            'audit',
            'status',
            'create_time',
            'update_time']);
    }
}