<?php
declare (strict_types = 1);

namespace app\madmin\model;

use think\Exception;
use think\Model;
use think\model\concern\SoftDelete;

class CollageItem extends BaseModel
{
    protected $globalScope = ['mallId'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
