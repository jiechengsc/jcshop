<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class LinkClick extends \think\Model
{
    #use SoftDelete;
}
