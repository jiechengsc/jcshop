<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model;


use think\Model;

class VisitHistory extends Model
{
    public function searchCreateTimeAttr($query,$value,$data){
        $query->whereBetweenTime('create_time', $value[0], $value[1]);
    }
    public function getCreateTimeAttr($value,$data){
        return date('Y-m-d H:i:s',$value);
    }
}