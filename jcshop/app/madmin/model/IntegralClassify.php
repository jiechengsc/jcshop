<?php


namespace app\madmin\model;


use think\model\concern\SoftDelete;

class IntegralClassify extends BaseModel
{
    protected $autoWriteTimestamp = 'datetime';
    use SoftDelete;
}