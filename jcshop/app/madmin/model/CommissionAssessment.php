<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class CommissionAssessment extends BaseModel
{
    use SoftDelete;


    /**
     * 累计发放佣金
     * @param $id int 分销商id
     * @param $mid
     * @return float 获取的佣金总额
     */
    public static function getCommissions($where)
    {
        $commission = Db::name("agent_bill_temporary")
            ->where('delete_time', null)
            ->where('is_assessment', 1)
            #->where('assessment_status', 1)
            ->where($where)
            ->sum("commission");
        return $commission;
    }

    /**
     * 已提现考核佣金
     * @param $id int 分销商id
     * @param $mid
     * @return float 获取的佣金总额
     */
    public static function getAssessmentDraw($where)
    {
        $draws = Db::name("commission_assessment_withdraw")
            ->field("withdraw")
            ->where('status', 1)
            ->where('delete_time', null)
            ->where($where)
            ->sum('withdraw');
        return $draws ?? 0;
    }



    /**
     * 累计订单数
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getOrderCount($where)
    {
        $count = Db::name("agent_bill_temporary")
//            ->Distinct(true)->field('order_no')
            ->where('status', 1)
            ->where('delete_time', null)
            ->where('is_assessment', 1)
            ->group('order_no')
            ->where($where)
            ->count();
        return $count;
    }
    /**
     * 订单列表
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getOrderList($where)
    {
        $count = Db::name("agent_bill_temporary")
//            ->Distinct(true)->field('order_no')
            ->where('status', 1)
            ->where('delete_time', null)
            #->where('steps',0)
            ->where('is_assessment', 1)
            ->where('assessment_status', 0)
            ->where($where)
            ->select();
        return $count;
    }
    /**
     * 分销商人数
     * @return int
     */
    public static function getAgents($where)
    {
        $agents = Db::name("agent")
            ->where('status', 1)
            ->where('delete_time', null)
            ->where($where)
            ->count();
        return $agents;
    }

    /**
     * 获得阶梯佣金人数
     * @param $where
     * @return int
     */
    public static function getAssessmentAgents($where)
    {
        $count = Db::name("agent_bill_temporary")
//            ->Distinct(true)->field('user_id')
            ->where('status', 1)
            ->where('delete_time', null)
            ->where('is_assessment', 1)
            ->where($where)
            ->group('user_id')
            ->count();
        return $count;
    }

    /**
     * 未达标降价人次
     * @return int
     */
    public static function getDegrades($where)
    {
        $count = Db::name("commission_assessment_degrade")
            ->where($where)
            ->group('user_id')
            ->count();
        return $count;
    }

    //额外阶梯佣金排行
    public static function getAssessmentCommission($where)
    {
        $page = request()->get('page',1);
        $size =request()->get('size',20);
        $commission = AgentBillTemporary::with(['agent','order','user'])
            ->where('delete_time', null)
            ->where('is_assessment', 1)
//            ->where('steps',0)
            ->whereRaw('(steps=0 AND is_self=1) or (is_self=0)') #避免自购出现计算多条问题
//            ->where('assessment_status', 1)
            ->where($where)
            ->fieldRaw("agent_id,user_id,order_id,sum(commission) as commission_count,sum(money) as money_count,count(*) as order_count")
            ->group(['agent_id'])
            ->paginate(['page' => $page, 'list_rows' => $size])->toArray();

        return $commission;
    }
    /**
     * 下级用户消费金额
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getPayment($where)
    {
        $money = Db::name("agent_bill_temporary")
//            ->where('agent_id', $id)
            ->where('status', 1)
            ->where('is_assessment', 1)
            ->where('delete_time', null)
            ->where($where)
            ->group('order_no')
            ->sum("money");
        return $money;
    }
    public static function agents($mid, $activity)
    {
        $agent_query = Agent::where(['mall_id' => $mid]);
        $level_type = $activity->level_type;

        if ($level_type == 1) {
            $levels = json_decode($activity->levels, true);
            if (!empty($levels)) {
                $agent_query->where('grade_id', 'in', $levels);
            }
        }
        $agents = $agent_query->select();
        return $agents;
    }
    public function close(){
        
    }
}
