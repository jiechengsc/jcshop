<?php


namespace app\madmin\model;


use think\Model;

class LiveLog extends BaseModel
{
    public static function write($title,$desc,$room_id=0){
        global $user;
        $data=[
            'admin_id'=>$user->id ?? 0,
            'mall_id'=>$user->mall_id ?? 0,
            'title'=>$title,
            'room_id'=>$room_id ?? 0,
            'desc'=>json_encode($desc,JSON_UNESCAPED_UNICODE),
            'create_time'=>date("Y-m-d H:i:s",time())
        ];
        $model= new self();
        $model->save($data);
        return $model;
    }
}