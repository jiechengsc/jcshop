<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\model\concern\SoftDelete;

class WxCustomer extends BaseModel
{
//    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function company()
    {
        return $this->hasOne(WxCompany::class, 'id', 'company_id');
    }
}
