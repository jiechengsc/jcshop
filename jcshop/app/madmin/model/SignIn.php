<?php


namespace app\madmin\model;


use think\Model;

class SignIn extends Model
{
    public function UserNicknamePicurl(){
        return $this->hasOne(User::class,'id','user_id')
            ->bind(['nickname','picurl']);
    }
}