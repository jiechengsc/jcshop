<?php
namespace app\madmin\model;

use app\command\Group;

class GroupCommodity extends BaseModel
{

//    protected $globalScope = "mallId";

    public function __construct($data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        $status = [
            0 => '未开始',
            1 => '进行中',
            2 => '已结束'
        ];
        return $status[$data['status']];
    }


    public function goods()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id');
    }

    public function sku()
    {
        return $this->hasMany(GroupCommoditySku::class, "gc_id", 'id');
    }

    /**
     * 处理拼团中
     * @param $now
     * @throws
     */
    public static function finishCollage($gc_id)
    {
        $Group = new Group();
        $gc = GroupCommodity::find($gc_id);
        $Group->setCollage(date("Y-m-d H:i:s", strtotime("-1 day", strtotime($gc['end_time']))), $gc_id);
    }
}
