<?php
namespace app\madmin\model;


class PresellCommoditySku extends BaseModel
{

    public function __construct($data = [])
    {
        parent::__construct($data);
    }

    public function sku()
    {
        return $this->hasOne(Sku::class, 'id', 'sku_id')->bind(['pvs_value']);
    }
}
