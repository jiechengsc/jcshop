<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;

class PayMode extends BaseModel
{
    protected $autoWriteTimestamp = false;

    protected $json = ['configuration'];

    protected $jsonAssoc = true;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['mode'])){
            self::append(['mode_text']);
            self::append(['type_text']);
        }
    }

    public function getModeTextAttr($value,$data)
    {
        $mode = [
            1 => '微信',
            2 => '支付宝',
            3=> '新微信支付'
        ];
        return $mode[$data['mode']];
    }

    public function getTypeTextAttr($value,$data)
    {
        if (!isset($data['mode']))
            return null;
        switch ($data['mode']){
            case 1:
                $type = [
                    1 => '微信支付',
                    2 => '微信支付子商户',
                    3 => '借用微信支付',
                    4 => '借用微信子商户',
                ];
                break;
            case 2:
                $type = [
                    1 => '支付宝2.0',
                ];
                break;
            case 3:
                $type = [
                    1 => '微信支付',
                    2 => '微信支付子商户',
                    3 => '借用微信支付',
                    4 => '借用微信子商户',
                ];
                break;
        }

        return $type[$data['type']];
    }

}
