<?php


namespace app\madmin\model;


use think\Model;

class IntegralSkuInventory extends Model
{
    public function inventory()
    {
        return $this->hasOne(IntegralSkuInventory::class,'sku_id','id');
    }
}