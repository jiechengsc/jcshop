<?php


namespace app\madmin\model;


class Coupon extends BaseModel
{

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id')
            ->bind(['merchant_name' => 'name', 'merchant_logo' => "logo", 'merchant_is_self' => "is_self"]);
    }

}