<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class IntegralGoods extends Model
{
    use SoftDelete;
    protected $json = ['sku_value','slideshow'];
    protected $jsonAssoc = true;
    public function getStatusTextAttr($value,$data){
        $status = [
            0 => '上架',
            1 => '下架'
        ];
        return $status[$data['status']];
    }

    public function sku(){
        return $this->hasMany(IntegralSku::class,'commodity_id','id');
    }
}