<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class CommissionAssessmentOrder extends BaseModel
{
    use SoftDelete;

    public static function write($write_data)
    {
        global $user;
        $data = [
            'admin_id' => $user->id ?? 0,
            'mall_id' => $user->mall_id ?? 0,
            'create_time' => date("Y-m-d H:i:s", time())
        ];
        $data = array_merge($data, $write_data);
        $model = new self();
        $model->save($data);
        return $model;
    }
}
