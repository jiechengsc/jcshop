<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class CommissionActivity extends BaseModel
{
    #use SoftDelete;

    /**
     * 获取分销商的绑定用户
     * @param $id int 分销商id
     * @param $mid
     * @return int 下级用户人数
     */
    public static function getUsers($id, $where)
    {
        $users = Db::name("user")
            ->where('pid', $id)
            ->whereNull('agent_id')
//            ->where('mall_id', $mid)
            ->where($where)
            ->where('delete_time', null)
            ->count();
        return $users;
    }

    /**
     * 下级用户消费金额
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getPayment($id, $where)
    {
        $money = Db::name("agent_bill_temporary")
            ->where($where)
            ->where('agent_id', $id)
            ->where('status', 1)
//            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->group('order_no')
            ->sum("money");
        return $money;
    }

    /**
     * 下级分销商人数
     * @param $id int 分销商id
     * @param $mid
     * @return int
     */
    public static function getAgents($id, $where)
    {
        $agents = Db::name("agent")
            ->where("pid", $id)
            ->where('status', 1)
            ->where('delete_time', null)
            ->where($where)
            ->count();
        return $agents;
    }

    /**
     * 下级用户订单数
     * @param $id int 分销商id
     * @param $mid
     * @return float
     */
    public static function getOrderCount($id, $where)
    {
        $count = Db::name("agent_bill_temporary")
            #->Distinct(true)->field('order_no')
            ->where('agent_id', $id)
            ->where('status', 1)
//            ->where('mall_id', $mid)
            ->where('delete_time', null)
            ->where($where)
            ->group('order_no')
            ->count();
        return $count;
    }


    public static function agents($mid, $activity)
    {
        $agent_query = Agent::where(['mall_id' => $activity->mall_id]);
        $level_type = $activity->level_type;

        if ($level_type == 1) {
            $levels = json_decode($activity->levels, true);
            if (!empty($levels)) {
                $agent_query->where('grade_id', 'in', $levels);
            }
        }
        $agents = $agent_query->select();
        return $agents;
    }

    public static function getAwardRole($activity)
    {
        $condition = json_decode($activity->condition, true);
        $award_stage = json_decode($activity->award_stage, true);
        $award_role = [];

        #只支持三级
        $arr = ["first" => 0, 'second' => 1, 'third' => 2];
        #先筛选出开启条件的规则
        foreach ($condition as $k => $value) {
            if ($value != 1) {
                continue;
            }
            foreach ($award_stage as $stage_k => $stage_v) {
                $key = $arr[$stage_k];
                $award_role[$key] = $stage_v;
            }
        }
        return $award_role;
    }
}
