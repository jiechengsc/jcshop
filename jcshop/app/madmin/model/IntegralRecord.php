<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model;


use think\Model;

class IntegralRecord extends Model
{
    public function orderNo(){
        return $this->hasOne(Order::class,'id','order_id')
            ->bind(['order_no']);
    }

    public function userNicknamePicurl(){
        return $this->hasOne(User::class,'id','user_id')
            ->bind(['nickname','picurl']);
    }

    public function getTypeTextAttr($value,$data) : string{
        $array = [
            1 => '消费获得',
            2 => '使用',
            3 => '分享获得',
            4 => '后台增加',
            5 => '后台减少',
            6 => '签到',
            7 => '新人领取',
            8 => '抽奖获得',
            9 => '抽奖消耗',
            10 => '积分商品',
            11 => '订单退款',
            12 => '开通会员等级',
            13 => '分销达标奖励',
            14 => '资产转赠增加',
            15 => '资产转赠减少'
        ];
        return $array[$data['type']];
    }
}