<?php


namespace app\madmin\model;


use think\Model;
use think\model\concern\SoftDelete;

class OrderFormAnswer extends Model
{
    use SoftDelete;
    protected $json = ['answer'];
    protected $jsonAssoc = true;
    public function user(){
        return $this->hasOne(User::class,'id','user_id')
            ->field('id,nickname,picurl');
    }
    public function order(){
        return $this->hasOne(Order::class,'id','order_id');
    }
}