<?php
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class CommissionAssessmentDegrade extends BaseModel
{
    use SoftDelete;
    
    public static function write($write_data)
    {

        $data = [
            'create_time' => date("Y-m-d H:i:s", time())
        ];
        $data = array_merge($data, $write_data);
        $model = new self();
        $model->save($data);
        return $model;
    }
    public function agent()
    {
        return $this->hasOne(Agent::class, 'id', 'agent_id');
    }

    public function agentGrade()
    {
        return $this->hasOne(AgentGrade::class, 'id', 'level');
    }
    public function agentOldGrade()
    {
        return $this->hasOne(AgentGrade::class, 'id', 'old_level');
    }
}
