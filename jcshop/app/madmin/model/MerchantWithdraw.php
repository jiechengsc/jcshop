<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class MerchantWithdraw extends BaseModel
{
    use SoftDelete;

    protected $json = ['card'];

    protected $jsonAssoc = true;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '申请中',
            1 => '通过',
            2 => '取消'
        ];
        return $status[$data['status']];
    }

    public function merchant()
    {
        return $this->hasOne(Merchant::class, 'id', 'merchant_id')->bind(['merchant_name' => 'name']);
    }

    public function scopeMerchantSearch(Query $query,$data)
    {

        $query = $query->alias('mw')
            ->join('merchant m', 'm.id = mw.merchant_id', 'left');

        if (isset($data['begin_date'], $data['end_date'])){
            $query->whereBetweenTime('mw.create_time',$data['begin_date'] . " 00:00:00", $data['end_date'] . "23:59:59");
            unset($data['begin_date'],$data['end_date']);
        }
        if (isset($data['merchant_name'])){
            $query = $query->where('m.name', 'like', '%' . $data['merchant_name'] . '%');
            unset($data['merchant_name']);
        }
        foreach ($data as $k=>$v){
            $query = $query->where('mw.'.$k, $v);
        }
        return $query;

    }

}
