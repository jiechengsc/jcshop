<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\model\view;


use app\madmin\model\BaseModel;

class AgentTeam extends BaseModel
{

    protected $table = 'jiecheng_agent_team';

    public function superior()
    {
        return $this->hasOne(AgentTeam::class,"id","pid")
            ->bind(['p_name'=>"nickname","p_picurl" => "picurl"]);
    }

}