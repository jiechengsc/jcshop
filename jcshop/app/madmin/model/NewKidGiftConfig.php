<?php


namespace app\madmin\model;


use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

class NewKidGiftConfig extends Model
{
    use SoftDelete;
    protected $json = ['config'];
    protected $jsonAssoc = true;
    public function GetCouponDataAttr($value,$data){
        return Db::name('coupon')
            ->where('id','IN',$data['config']['coupon'])
            ->select();
    }
}