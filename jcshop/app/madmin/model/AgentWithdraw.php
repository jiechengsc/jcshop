<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class AgentWithdraw extends BaseModel
{
    use SoftDelete;
    //

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '审核中',
            1 => '通过',
            2 => '取消',
        ];
        return $status[$data['status']];
    }

    public function agent()
    {
        return $this->hasOne(Agent::class,'id','agent_id')->bind(['agent_name'=>'name']);
    }


    public function userId(){
        return $this->hasOne(Agent::class,'id','agent_id')->bind(['user_id']);
    }

    public function scopeAgentSearch(Query $query,$data)
    {
        $query = $query->alias('aw')
            ->join('agent a', 'a.id = aw.agent_id', 'left');

        if (isset($data['begin_date'], $data['end_date'])){
            $query->whereBetweenTime('aw.create_time',$data['begin_date'] . " 00:00:00", $data['end_date'] . "23:59:59");
            unset($data['begin_date'],$data['end_date']);
        }
        if (isset($data['agent_name'])){
            $query = $query->where('a.name', 'like', '%' . $data['agent_name'] . '%');
            unset($data['agent_name']);
        }
        foreach ($data as $k=>$v){
            $query = $query->where('aw.'.$k, $v);
        }
        return $query;
    }

}
