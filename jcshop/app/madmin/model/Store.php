<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\madmin\model;

use think\db\Query;
use think\Model;

class Store extends BaseModel
{

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['audit'])){
            self::append(['audit_text']);
        }
    }

    public function getAuditTextAttr($value,$data)
    {
        $status = [0=>'审核中',1=>'审核通过'];
        return $status[$data['audit']];
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class,'merchant_id','id');
    }

}
