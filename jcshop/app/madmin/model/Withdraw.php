<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\model;

use think\db\Query;
use think\model\concern\SoftDelete;

class Withdraw extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '申请中',
            1 => '通过',
            2 => '取消'
        ];
        return $status[$data['status']];
    }

    public function getCardAttr($value,$data){
        if(!empty($value))
            return json_decode($value,true);
        else
            return $value;

    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function userNickname()
    {
        return $this->hasOne(User::class,'id','user_id')->bind(['nickname']);
    }

    public function scopeUserSearch(Query $query,$data)
    {

        $query = $query->alias('uw')
            ->join('user u', 'u.id = uw.user_id', 'left');

        if (isset($data['begin_date'], $data['end_date'])){
            $query->whereBetweenTime('uw.create_time',$data['begin_date'] . " 00:00:00", $data['end_date'] . "23:59:59");
            unset($data['begin_date'],$data['end_date']);
        }
        if (isset($data['nickname'])){
            $query = $query->where('u.nickname', 'like', '%' . $data['nickname'] . '%');
            unset($data['nickname']);
        }
        foreach ($data as $k=>$v){
            $query = $query->where('uw.'.$k, $v);
        }
        return $query;

    }

}
