<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
use app\madmin\validate\AgentApplyValidate;
use app\madmin\validate\AgentBillTemporaryValidate;
use app\madmin\validate\AgentValidate;
use app\madmin\validate\AgentWithdrawValidate;
use app\madmin\validate\AgentGradeValidate;

use app\madmin\validate\ArticleTypeValidate;
use app\madmin\validate\ArticleValidate;

use app\madmin\validate\CommodityValidate;
use app\madmin\validate\ConfigurationValidate;
use app\madmin\validate\Department;
use app\madmin\validate\ExpressValidate;
use app\madmin\validate\Merchant;
use app\madmin\validate\MerchantApplyValidate;
use app\madmin\validate\MerchantWithdrawValidate;
use app\madmin\validate\Operation;
use app\madmin\validate\PayModeValidate;
use app\madmin\validate\RechargeRecordValidate;
use app\madmin\validate\RoleValidate;
use app\madmin\validate\RuleValidate;
use app\madmin\validate\StoreValidate;
use app\madmin\validate\UserCashValidate;
use app\madmin\validate\UserCouponValidate;
use app\madmin\validate\UserValidae;
use app\madmin\validate\WxaConfigValidate;
use app\madmin\validate\AdminValidate;
use app\madmin\validate\GroupValidate;
use app\madmin\validate\SeckillValidate;
use app\madmin\validate\PresellValidate;
use app\madmin\validate\RechargeValidate;

//Route::rest('create',['POST','/list','findAll']);
//Route::resource("user","sadmin/user");    资源路由 存在无法使用验证器问题
Route::any('', 'index/index');

//登录操作
Route::group("operation", function () {
    Route::post('login', 'login')
        ->validate(Operation::class, "login");
    Route::any('logout', 'logout');
})->prefix('operation/');

//前台用户
Route::group("user", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(UserValidae::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(UserValidae::class, 'update');
    Route::delete(':id', 'delete');
    Route::get('total/:id', 'total');
})->prefix('user/')->pattern(['id' => '\d+']);

//前台用户金额
Route::group("user/cash", function () {
    Route::post('list', 'findAll')
        ->validate(UserCashValidate::class, 'list');
    Route::post('merchant', 'merchantfindAll');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(UserCashValidate::class, 'update');
    Route::put('recharge/:id', 'recharge');
    Route::put('integral/:id', 'integral');
    Route::delete(':id', 'delete');
})->prefix('user_cash/')->pattern(['id' => '\d+']);

//用户优惠券
Route::group("user/coupon", function () {
    Route::post('list', 'findAll')
        ->validate(UserCouponValidate::class);
})->prefix('user_coupon/')->pattern(['id' => '\d+']);

//前台用户金额记录
Route::group("recharge/record", function () {
    Route::post('list', 'findAll')
        ->validate(RechargeRecordValidate::class, 'list');
    Route::delete(':id', 'delete');
})->prefix('recharge_record/')->pattern(['id' => '\d+']);

//后台用户
Route::group("admin", function () {
    Route::post('list', 'findAll')
        ->validate(AdminValidate::class, 'list');
    Route::post('', 'save')
        ->validate(AdminValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put('selfUp/:id', 'selfUp');
    Route::put(':id', 'update')
        ->validate(AdminValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('admin/')->pattern(['id' => '\d+']);

//部门
Route::group("auth/department", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(Department::class, "save");
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(Department::class, "update");
    Route::delete(':id', 'delete');
})->prefix('department/')->pattern(['id' => '\d+']);

//商户申请
Route::group("merchant/apply", function () {
    Route::post('list', 'findAll')
        ->validate(MerchantApplyValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(MerchantApplyValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('merchant_apply/')->pattern(['id' => '\d+']);

//商户
Route::group("merchant", function () {
    Route::post('commodity', 'getMerchantAndCommodityByIds');
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(Merchant::class, "save");
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(Merchant::class, "update");
    Route::delete(':id', 'delete');
})->prefix('merchant/')->pattern(['id' => '\d+']);

//商户门店
Route::group("merchant/store", function () {
    Route::post('list', 'findAll')
        ->validate(StoreValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(StoreValidate::class, 'update');
})->prefix('store/')->pattern(['id' => '\d+']);

//商户商品
Route::group("spu", function () {
    Route::post('list', 'findAll')
        ->validate(CommodityValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(CommodityValidate::class, 'update');
    Route::put('sort/:id', 'updateSort');
    Route::put('name/:id', 'updateGoodsName');
})->prefix('commodity/')->pattern(['id' => '\d+']);

//商户提现记录
Route::group("merchant/withdraw", function () {
    Route::post('list', 'findAll')
        ->validate(MerchantWithdrawValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(MerchantWithdrawValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('merchant_withdraw/')->pattern(['id' => '\d+']);

//推广员申请
Route::group("agent/apply", function () {
    Route::post('list', 'findAll')
        ->validate(AgentApplyValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(AgentApplyValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('agent_apply/')->pattern(['id' => '\d+']);

//推广员
Route::group("agent", function () {
    Route::post('list', 'findAll')
        ->validate(AgentValidate::class, 'list');
    Route::post('', 'save')
        ->validate(AgentValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(AgentValidate::class, 'update');
    Route::delete(':id', 'delete');
    Route::post('order', 'orderList')
        ->validate(AgentValidate::class, 'order');
    Route::get('level', 'AgentLevel');
    Route::get('rank', 'rank');
    Route::get('grade/index', 'AgentGrade/index');
})->prefix('agent/')->pattern(['id' => '\d+']);

// 分销等级
Route::group('agent/grade', function () {
    Route::post('list', 'findAll');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('agent_grade/')->pattern(['id' => '\d+']);

//推广员佣金记录
Route::group("agent/commission", function () {
    Route::post('list', 'findAll');
    Route::get(':id', 'read');
    Route::put(':id', 'update')->validate(AgentBillTemporaryValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('agent_bill_temporary/')->pattern(['id' => '\d+']);

//推广员提现记录
Route::group("agent/withdraw", function () {
    Route::post('list', 'findAll')
        ->validate(AgentWithdrawValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(AgentWithdrawValidate::class, 'update');
    Route::delete(':id', 'delete');
    Route::get('adopt/:id', 'adopt');
    Route::get('refuse/:id', 'refuse');
})->prefix('agent_withdraw/')->pattern(['id' => '\d+']);


//快递
Route::group("express", function () {
    Route::get('', 'index');
    Route::post('', 'save')
        ->validate(ExpressValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('express/')->pattern(['id' => '\d+']);

//文章分类
Route::group("article/type", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(ArticleTypeValidate::class, 'save');
    Route::put(':id', 'update')
        ->validate(ArticleTypeValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('article_type/')->pattern(['id' => '\d+']);

//文章
Route::group("article", function () {
    Route::post('list', 'findAll')
        ->validate(ArticleValidate::class, 'list');
    Route::post('', 'save')
        ->validate(ArticleValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('article/')->pattern(['id' => '\d+']);

//协议
Route::group("protocol", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('protocol/')->pattern(['id' => '\d+']);

//商品分类
Route::group("classify", function () {
    Route::put('level', 'level');
    Route::get('', 'index');
    Route::post('', 'save');
//        ->validate(app\madmin\validate\ClassifyValidate::class, 'save');
    Route::delete(':id', 'delete');
})->prefix('classify/')->pattern(['id' => '\d+']);

//权限角色
Route::group("auth/role", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(RoleValidate::class, 'list');
    Route::post('', 'save')
        ->validate(RoleValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(RoleValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('role/')->pattern(['id' => '\d+']);

//权限规则
Route::group("auth/rule", function () {
    Route::get('', 'index');
    Route::get('value', 'findAllValue');
    Route::post('list', 'findAll')
        ->validate(RuleValidate::class, 'list');
    Route::post('', 'save')
        ->validate(RuleValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(RuleValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('rule/')->pattern(['id' => '\d+']);

//插件
Route::group("addons", function () {
    Route::get('', 'index');
    Route::get('list', 'list');
    Route::get(':id', 'read');
    Route::post("install",'install');
    Route::post("uninstall",'uninstall');
    Route::get("download",'download');
    Route::get('update_recode/:id','updateRecordList');
})->prefix('addons/')->pattern(['id' => '\d+']);

Route::group("authentication", function () {
    Route::post("active/addons", 'addonsRemoteAuthorization');
    Route::post("active/copyright", 'copyrightRemoteAuthorization');
})->prefix('authentication/');

//上传接口
Route::group("upload", function () {
    //这里校验上传接口会有问题
//    Route::post('images','images')->validate(app\sadmin\validate\Upload::class,'images');
    Route::post('images', 'images');
    Route::post('license', 'licenseTxt');
})->prefix('upload/');

//导出excel
Route::group("excel", function () {
    Route::get('agent', 'agentExport');
    Route::get('user', 'userExport');
})->prefix('excel/');

//阿里短信模板
Route::group("sms/ali/template", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('ali_sms_template/')->pattern(['id' => '\d+']);

//微信模板
Route::group("sms/wechat/template", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('wechat_template/')->pattern(['id' => '\d+']);

//消息发送配置
Route::group("msg/config", function () {
    Route::get('', 'index');
    Route::post('', 'save');
    Route::put(':id', 'update');
    Route::get('scene', 'scene');
    Route::get('wxTemplate', 'wxTemplate');
})->prefix('msg_config/')->pattern(['id' => '\d+']);

//支付方式模板
Route::group("pay/mode", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(PayModeValidate::class);
    Route::get(':id', 'read');
    Route::post(':id', 'update')
        ->validate(PayModeValidate::class);
    Route::delete(':id', 'delete');
})->prefix('pay_mode/')->pattern(['id' => '\d+']);

//配置文件
Route::group("configuration", function () {
    Route::post('mallBase', 'mallBaseSave')
        ->validate(ConfigurationValidate::class, 'mallBaseSave');
    Route::get('mallBase', 'mallBaseRead');
    Route::put('mallBase', 'mallBaseUpdate')
        ->validate(ConfigurationValidate::class, 'mallBaseUpdate');
    Route::post('mallShare', 'mallShareSave');
    Route::get('mallShare', 'mallShareRead');
    Route::put('mallShare', 'mallShareUpdate');
    Route::post('mallContact', 'mallContactSave');
    Route::get('mallContact', 'mallContactRead');
    Route::put('mallContact', 'mallContactUpdate');
    Route::post('tradeSetting', 'tradeSettingSave')
        ->validate(ConfigurationValidate::class, 'tradeSettingSave');
    Route::get('tradeSetting', 'tradeSettingRead');
    Route::put('tradeSetting', 'tradeSettingUpdate')
        ->validate(ConfigurationValidate::class, 'tradeSettingUpdate');
    Route::post('rightsSetting', 'rightsSettingSave')
        ->validate(ConfigurationValidate::class, 'rightsSettingSave');
    Route::get('rightsSetting', 'rightsSettingRead');
    Route::put('rightsSetting', 'rightsSettingUpdate')
        ->validate(ConfigurationValidate::class, 'rightsSettingUpdate');
    Route::post('integralBalance', 'integralBalanceSave')
        ->validate(ConfigurationValidate::class, 'integralBalanceSave');
    Route::get('integralBalance', 'integralBalanceRead');
    Route::put('integralBalance', 'integralBalanceUpdate')
        ->validate(ConfigurationValidate::class, 'integralBalanceUpdate');
    Route::post('mallPayMode', 'mallPayModeSave')
        ->validate(ConfigurationValidate::class, 'mallPayModeSave');
    Route::get('mallPayMode', 'mallPayModeRead');
    Route::put('mallPayMode', 'mallPayModeUpdate')
        ->validate(ConfigurationValidate::class, 'mallPayModeUpdate');
    Route::post('remitSetting', 'remitSettingSave')
        ->validate(ConfigurationValidate::class, 'remitSettingSave');
    Route::get('remitSetting', 'remitSettingRead');
    Route::put('remitSetting', 'remitSettingUpdate')
        ->validate(ConfigurationValidate::class, 'remitSettingUpdate');
    Route::post('aliSms', 'aliSmsSave')
        ->validate(ConfigurationValidate::class, 'aliSmsSave');
    Route::get('aliSms', 'aliSmsRead');
    Route::put('aliSms', 'aliSmsUpdate')
        ->validate(ConfigurationValidate::class, 'aliSmsUpdate');
    Route::post('wx', 'wxSave')
        ->validate(ConfigurationValidate::class, 'wxSave');
    Route::get('wx', 'wxRead');
    Route::put('wx', 'wxUpdate')
        ->validate(ConfigurationValidate::class, 'wxUpdate');
    Route::post('wxa', 'wxaSave');
    Route::get('wxa', 'wxaRead');
    Route::put('wxa', 'wxaUpdate');
    Route::post('exoressBird', 'exoressBirdSave')
        ->validate(ConfigurationValidate::class, 'exoressBirdSave');
    Route::get('exoressBird', 'exoressBirdRead');
    Route::put('exoressBird', 'exoressBirdUpdate')
        ->validate(ConfigurationValidate::class, 'exoressBirdUpdate');
    Route::post('couponExplain', 'couponExplainSave');
    Route::get('couponExplain', 'couponExplainRead');
    Route::put('couponExplain', 'couponExplainUpdate');
    Route::post('agentExplain', 'agentExplainSave')
        ->validate(ConfigurationValidate::class, 'agentExplainSave');
    Route::get('agentExplain', 'agentExplainRead');
    Route::put('agentExplain', 'agentExplainUpdate')
        ->validate(ConfigurationValidate::class, 'agentExplainUpdate');
    Route::post('customerService', 'customerServiceSave');
    Route::get('customerService', 'customerServiceRead');
    Route::put('customerService', 'customerServiceUpdate');
    Route::post('signIn', 'signInSave');
    Route::get('signIn', 'signInRead');
    Route::put('signIn', 'signInUpdate');


    Route::post('alicloudExpress', 'alicloudExpressSave');
    Route::get('alicloudExpress', 'alicloudExpressRead');
    Route::put('alicloudExpress', 'alicloudExpressUpdate');

    Route::post('oss', 'ossSave');
    Route::get('oss', 'ossRead');
    Route::put('oss', 'ossUpdate');
    Route::post('lottery', 'lotterySave');
    Route::get('lottery', 'lotteryRead');
    Route::put('lottery', 'lotteryUpdate');

    Route::post('memberLevel', 'memberLevelSave');
    Route::get('memberLevel', 'memberLevelRead');
    Route::put('memberLevel', 'memberLevelUpdate');

    Route::get('theme', 'themeRead');
    Route::put('theme', 'themeUpdate');

    Route::get('web', 'webRead');
    Route::put('web', 'webUpdate');

    #采集商品

    Route::get('scrapyRead', 'scrapyRead');
    Route::post('scrapySave', 'scrapySave');
    # 快速注册企业小程序
    Route::get('WeixinAppRead', 'WeixinAppRead');
    Route::post('WeixinAppSave', 'WeixinAppSave');

    Route::get('group', 'groupRead');
    Route::put('group', 'groupUpdate');

    Route::get('seckill', 'seckillRead');
    Route::put('seckill', 'seckillUpdate');

    #截留口令
    Route::get('withholdPassword', 'withholdPasswordRead');
    Route::post('withholdPassword', 'withholdPasswordSave');
    #资金转赠
    Route::get('Assettransfer', 'AssettransferRead');
    Route::post('Assettransfer', 'AssettransferSave');

})->prefix('configuration/')->pattern(['type' => '[a-zA-Z]+']);

//微信校验地址
Route::group("wxurl", function () {
    Route::get('', 'index');
    Route::get('check', 'check');
    Route::get('preview', 'preview');
})->prefix('wx_url/')->pattern(['type' => '[a-zA-Z]+']);


Route::group('visit', function () {
    Route::get('', 'getVisitData');
//    Route::post('','getVisitList');
})->prefix('VisitHistory/');

//小程序上传接口
Route::group("wxa/config", function () {
    Route::get('login', 'login');
    Route::get('is_preview', 'islogin');
    Route::get('preview', 'preview');
    Route::get('send', 'sendFile')->validate(WxaConfigValidate::class);
})->prefix('wxa_config/')->pattern(['id' => '\d+']);

//底部菜单
Route::group("fitment/bottom_menu", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('bottom_menu/')->pattern(['id' => '\d+']);

//装修
Route::group('page', function () {
    Route::post('text', 'text');
    Route::post('create', 'createPage');
    Route::post('startAdvert', 'startAdvert');
    Route::get('pageList', 'pageList');
    Route::get('renovation', 'renovation');
    Route::get('defaultTemplate', 'defaultTemplate');
    Route::get('myTemplateList', 'myTemplateList');
    Route::delete('delMytemplate/:id', 'delMytemplate');
    Route::delete(':id', 'delPage');
    Route::get('preview', 'preview');
    Route::get('getMyTemplateInfo', 'getMyTemplateInfo');
    Route::get('myTemplate', 'myTemplate');
    Route::get('market', 'market');
    Route::get('getTemplateInfo', 'getTemplateInfo');
    Route::get('advert', 'advert');
    Route::get('classList', 'classList');
    Route::get('setDefaultTemplate', 'setDefaultTemplate');
    Route::put('upRenovation', 'upRenovation');
    Route::post('base64Up', 'base64Up');
    Route::post('createPublicTemplate', 'createPublicTemplate');
    Route::post('saveTemplate', 'saveTemplate');
    Route::post('enable', 'enable');
    Route::put('saveSource', 'saveSource');
    Route::put('saveMyTemplate', 'saveMyTemplate');
    Route::post('shop_preview', 'shop_preview');
})->prefix('page/')->pattern(['id' => '\d+']);
//积分
Route::group('integral', function () {
    Route::post('list', 'list')
        ->validate(app\madmin\validate\IntegralValidate::class, 'list');
    Route::get('', 'config_find');
    Route::put('', 'config_update')
        ->validate(app\madmin\validate\IntegralValidate::class, 'update');
    Route::group('share', function () {
        Route::get('find', 'find_share');
        Route::put('save', 'save_share');
    });
    Route::get('open', 'open');
})->prefix('integral/');
//订单
Route::group('order', function () {
    Route::post('drawback', 'order/drawback');
})->prefix('order/');

//搜索关键字
Route::group("search/keyword", function () {
    Route::get('', 'index');
    Route::post('', 'save');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('search_keyword/')->pattern(['id' => '\d+']);

//小程序跳转
Route::group("wxa/jump", function () {
    Route::get('', 'index');
    Route::post('', 'save');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('jump_wxa/')->pattern(['id' => '\d+']);

//用户提现记录
Route::group("user/withdraw", function () {
    Route::post('list', 'findAll');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('withdraw/')->pattern(['id' => '\d+']);

//自定义表单
Route::group('form', function () {
    Route::group('template', function () {
        Route::post('', 'create');
        Route::get(':id', 'read');
        Route::post('list', 'list');
        Route::put(':id', 'update');
        Route::delete(':id', 'delete');
    });
    Route::group('answer', function () {
        Route::get(':id', 'answerRead');
        Route::post(':id', 'answerList');
    });
})->prefix('form/');
//签到
Route::group('signin', function () {
    Route::get('', 'config');
    Route::put('', 'update');
    Route::post(':page/:size', 'list');
})->prefix('sign_in/');
//新人礼物
Route::group('new_kid_gift', function () {
    Route::post('receive', 'receive_list');
    Route::get('receive/:id', 'receive_read');
    Route::get(':id', 'read');
    Route::post('', 'create')
        ->validate(app\madmin\validate\NewKidGiftValidate::class, 'create');
    Route::put(':id', 'update')
        ->validate(app\madmin\validate\NewKidGiftValidate::class, 'update');
    Route::delete(':id', 'delete');
    Route::post('list', 'list');
})->prefix('new_kid_gift/');

//优惠券
Route::group("coupon", function () {
    Route::post('list', 'findAll');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
})->prefix('coupon/')->pattern(['id' => '\d+']);

//商户详情
Route::group("merchant/settlement", function () {
    Route::post('list', 'findAll');
})->prefix('merchant_settlement/')->pattern(['id' => '\d+']);

//评论
Route::group("evaluate", function () {
    Route::get('evaluateList', 'evaluateList');
})->prefix('evaluate/')->pattern(['id' => '\d+']);

//平台
Route::group("lessee", function () {
    Route::get('entry', 'entry');
})->prefix('lessee/')->pattern(['id' => '\d+']);

//会员
Route::group("membership", function () {
    //等级
    Route::group("level", function () {
        Route::put('user/:id', 'setUserLevel');
        Route::post('list', 'findAll');
        Route::post('', 'save');
        Route::get(':id', 'read');
        Route::put(':id', 'update');
        Route::delete(':id', 'delete');
        Route::group('content', function () {
            Route::post('list', 'contentList');
            Route::get(':id', 'contentRead');
            Route::post('', 'contentSave');
            Route::put(':id', 'contentUpdate');
            Route::delete(':id', 'contentDelete');
        });
    })->prefix('membership_level/');

    //记录
    Route::group("record", function () {
        Route::get('', 'index');
        Route::get(':id', 'read');
    })->prefix('membership_record/');

})->pattern(['id' => '\d+']);

//抽奖
Route::group('lottery', function () {
    Route::group('config', function () {
        Route::get(':id', 'config_read');
        Route::post('list', 'config_list')
            ->validate(\app\madmin\validate\LotteryValidate::class, 'list');
        Route::post('', 'config_save')
            ->validate(\app\madmin\validate\LotteryValidate::class, 'save');
        Route::put(':id', 'config_update')
            ->validate(\app\madmin\validate\LotteryValidate::class, 'update');
        Route::delete(':id', 'config_delete');
        Route::post('open/:id', 'config_open');
    });
    Route::group('log', function () {
        Route::get(':id', 'lottery_log_read');
        Route::post('list', 'lottery_log_list');
    });
    Route::post('deliver', 'lottery_goods_deliver');
    Route::post('receiving', 'lottery_goods_receiving');
})->prefix('lottery/');
Route::group('integral/shop', function () {
    Route::group('classify', function () {
        Route::post('list', 'classifyList');
        Route::post('', 'classifyCreate');
        Route::put(':id', 'classifyUpdate');
        Route::get(':id', 'classifyFind');
        Route::delete(':id', 'classifyDelete');
    });
    Route::group('goods', function () {
        Route::post('list', 'goodsList');
        Route::post('', 'goodsCreate');
        Route::get(':id', 'goodsFind');
        Route::put(':id', 'goodsUpdate');
        Route::delete(':id', 'goodsDelete');
    });
    Route::group('order', function () {
        Route::post('list', 'orderList');
        Route::post('refund/:id', 'orderRefund');
        Route::get(':id', 'orderFind');
        Route::put('deliver/:id', 'orderDeliver');
        Route::put('receiving/:id', 'orderReceiving');
        Route::put('update/deliver/:id', 'orderDeliverUpdate');
        Route::delete(':id', 'orderDelete');
    });
})->prefix('integral_shop/');
//推荐商品
Route::group("pick", function () {
    Route::get('get_config', 'getConfig');
    Route::post('save', 'save');
    Route::get('get_grounding_goods', 'getGroundingGoods');
})->prefix('pick/')->pattern(['id' => '\d+']);

//悬浮菜单
Route::group("float", function () {
    Route::get('get_config', 'getConfig');
    Route::post('save', 'save');
})->prefix('float_menu/')->pattern(['id' => '\d+']);

// 企业（客服）
Route::group("wx", function () {
    Route::group("company", function () {
        Route::post('save', 'save');
        Route::put('update', 'update');
        Route::delete('delete/:id', 'delete');
        Route::get('', 'findAll');
        Route::get('read/:id', 'read');
        Route::get('customer', 'getCs');
    })->prefix("wx_company/")->pattern(['id' => '\d+']);

    Route::group("customer", function () {
        Route::post('save', 'save');
        Route::put('update', 'update');
        Route::delete('delete/:id', 'delete');
        Route::get('', 'findAll');
        Route::get('read/:id', 'read');
    })->prefix("wx_customer/")->pattern(['id' => '\d+']);

})->pattern(['id' => '\d+']);

//基础版权
Route::group("copyright", function () {
    Route::get('', 'index');
    Route::post('', 'save');
    Route::post('/active','active');
})->prefix('copyright/');
//下单表单
Route::group("order/form", function () {
    Route::get('', 'list');
    Route::post('', 'create');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
    Route::post('status', 'status');
    Route::get('answer/:id', 'answerList');
    Route::get('template/:id', 'templateAnswerList');
    Route::get('answer/info/:id', 'answerInfo');
})->prefix('order_form/')->pattern(['id' => '\d+']);

// 拼团
Route::group('group', function () {
    Route::get('', 'index')->validate(GroupValidate::class, 'index');
    Route::post('', 'save')->validate(GroupValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')->validate(GroupValidate::class, 'update');
    Route::post(':id', 'stop');
    Route::delete(':id', 'delete');
    Route::get('data', 'data');
    Route::get('groupData', 'dataForGroup');
})->prefix('group/')->pattern(['id' => '\d+']);

Route::group('collage', function () {
    Route::get('', 'index');
    Route::get(':id', 'read');
    Route::post(':id', 'update');
})->prefix('collage/')->pattern(['id' => '\d+']);


#直播商品审核部分
Route::group("live/good", function () {
    Route::get('list', 'list');
    Route::post('updateGoods/:id', 'updateGoods');
    Route::post('addAudit/:id', 'addAudit');
    Route::post('resetAudit/:id', 'resetAudit');
    Route::post('repeatAudit/:id', 'repeatAudit');
    Route::post('deleteAudit/:id', 'deleteAudit');
})->prefix('live_good/')->pattern(['id' => '\d+']);

#直播间
Route::group("live/room", function () {
    Route::get('list', 'list');
    Route::get('getQrcode/:id', 'getQrcode');
    Route::post('add', 'add');
    Route::get('detail/:id', 'detail');
    Route::post('syncRoom', 'syncRoom');
    Route::get('hide/:id', 'hide');
    Route::get('getReplay/:id', 'getReplay');
    Route::post('edit/:id', 'edit');
    Route::post('delete/:id', 'delete');
    Route::post('updateFeedPublic/:id', 'updateFeedPublic');
    Route::post('updateReplay/:id', 'updateReplay');
    Route::post('goodsOnSale/:id', 'goodsOnSale');
    Route::post('deleteRoomGoods/:id', 'deleteRoomGoods');
    Route::post('addRoomGoods/:id', 'addRoomGoods');
    Route::post('pushRoomGoods/:id', 'pushRoomGoods');
})->prefix('live_room/')->pattern(['id' => '\d+']);

#通过审核的商品库
Route::group("live/room/goods", function () {
    Route::get('list', 'list');

})->prefix('live_room_goods/')->pattern(['id' => '\d+']);

Route::group("weixin", function () {
    Route::post('listen_event/:id', 'listen_event');
    Route::post('listen_msg/:id/:appid', 'listen_msg');
    Route::post('save', 'save')->validate(\app\madmin\validate\WeixinAppValidate::class, 'save');
})->prefix('weixin/')->pattern(['id' => '\d+']);

#mall端跟shop端日志
Route::group("log", function () {
    Route::get('list', 'list');
})->prefix('log/')->pattern(['id' => '\d+']);

#分销商业绩考核
Route::group("commission/assessment", function () {
    #考核
    Route::get('index', 'index');
    Route::get('list', 'list');
    Route::get('detail/:id', 'detail');
    Route::post('add', 'add');
    Route::post('status', 'status');
    Route::delete('delete/:id', 'delete');
    #订单记录
    Route::get('order', 'order');
    #降级记录
    Route::get('degrade', 'degrade');
})->prefix('commission_assessment/')->pattern(['id' => '\d+']);

#分销商达标
Route::group("commission/activity", function () {
    #活动管理
    Route::get('list', 'list');
    Route::post('add', 'add');
    Route::get('detail/:id', 'detail');
    Route::post('status', 'status');
    Route::delete('delete/:id', 'delete');
    #奖励记录
    Route::get('award/list', 'award');
    Route::post('send', 'send');
    Route::post('issue', 'issue');
})->prefix('commission_activity/')->pattern(['id' => '\d+']);

//分销商考核提现记录
Route::group("commission/assessment/withdraw", function () {
    Route::post('list', 'findAll')
        ->validate(AgentWithdrawValidate::class, 'list');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(AgentWithdrawValidate::class, 'update');
    Route::delete(':id', 'delete');
    Route::post('adopt/:id', 'adopt');
    Route::post('refuse/:id', 'refuse');
})->prefix('commission_assessment_withdraw/')->pattern(['id' => '\d+']);

#小程序外部跳转链接
Route::group("wx/urllink", function () {
    #活动管理
    Route::get('list', 'list');
    Route::post('add', 'add');
    Route::get('detail/:id', 'detail');
    Route::delete('delete/:id', 'delete');
})->prefix('url_link/')->pattern(['id' => '\d+']);

// 秒杀
Route::group('seckill', function () {
    Route::get('', 'index')->validate(SeckillValidate::class, 'index');
    Route::post('', 'save')->validate(SeckillValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')->validate(SeckillValidate::class, 'update');
    Route::post(':id', 'stop');
    Route::delete(':id', 'delete');
})->prefix('seckill/')->pattern(['id' => '\d+']);

// 预售
Route::group('presell', function () {
    Route::get('', 'index')->validate(PresellValidate::class, 'index');
    Route::post('', 'save')->validate(PresellValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')->validate(PresellValidate::class, 'update');
    Route::post(':id', 'stop');
    Route::delete(':id', 'delete');
})->prefix('presell/')->pattern(['id' => '\d+']);

//资金转赠日志
Route::group("assettransfer/log", function () {
    Route::get('list', 'list');
})->prefix('assettransfer_log/')->pattern(['id' => '\d+']);

// 充值奖励
Route::group('recharge', function () {
    Route::get('', 'index')->validate(RechargeValidate::class, 'index');
    Route::post('', 'save')->validate(RechargeValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')->validate(RechargeValidate::class, 'update');
    Route::post(':id', 'stop');
    Route::delete(':id', 'delete');
})->prefix('recharge/')->pattern(['id' => '\d+']);