<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Agent;
use app\madmin\model\AgentBillTemporary as admin;
use app\madmin\model\UserCash;
use app\utils\TrimData;
use think\db\Query;
use think\Exception;

class AgentBillTemporaryService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        global $user;
        $admin = admin::withoutGlobalScope(['mallId'])->scope('Search', $data)
            ->where("a.mall_id", $user->mall_id)
            ->field('a.*');
        $list=$admin->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);
        $model = admin::where(['id'=>$id,'update_time'=>$update_time])->lock(true)->find();
        if(!$model)
            throw new Exception("请刷新重试");
        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);

        $agent = Agent::find($model->agent_id);
        if ($model->getData("status") == 1 && $data['status'] ==3 && !empty($admin)){
            $update = UserCash::where(['user_id' => $agent->user_id])
                ->dec("agent_total", (double)$model->cash)
                ->dec("agent", (double)$model->cash)
                ->update();
            if (empty($update))
                throw new \PDOException("取消失败");
        } elseif($model->getData("status") == 0 && $data['status'] ==1 && !empty($admin)){
            $update = UserCash::where(['user_id' => $agent->user_id])
                ->inc("agent_total", (double)$model->cash)
                ->inc("agent", (double)$model->cash)
                ->update();
            if (empty($update))
                throw new \PDOException("通过失败");
        }
        return [HTTP_CREATED, $admin];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }



}
