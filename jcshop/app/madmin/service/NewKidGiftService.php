<?php


namespace app\madmin\service;


use app\madmin\model\NewKidGift;
use app\madmin\model\NewKidGiftConfig AS model;
use think\Exception;
use think\facade\Db;

class NewKidGiftService
{
    private $user;
    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    /**
     * 创建新手礼物配置
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function create(array $data) : array{
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->whereBetweenTime('start_time',$data['start_time'],$data['end_time'])
            ->count();
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->whereBetweenTime('end_time',$data['start_time'],$data['end_time'])
            ->count();
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->where('start_time','<=',$data['start_time'])
            ->where('end_time','>=',$data['end_time'])
            ->count();
        $count = array_sum($count);
        if ($count > 0)
            throw new Exception('时间段内已经有活动了',HTTP_INVALID);
        $data['mall_id'] = $this->user['mall_id'];
        $data = model::create($data);
        if (empty($data->id))
            throw new Exception('创建失败',HTTP_INVALID);
        return [HTTP_SUCCESS,$data];
    }

    /**
     * 更新配置
     * @param int $id
     * @param array $data
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(int $id, array $data) : array{
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->whereBetweenTime('start_time',$data['start_time'],$data['end_time'])
            ->where('id','<>',$id)
            ->count();
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->whereBetweenTime('end_time',$data['start_time'],$data['end_time'])
            ->where('id','<>',$id)
            ->count();
        $count[] = model::where('mall_id',$this->user->mall_id)
            ->where('start_time','<=',$data['start_time'])
            ->where('end_time','>=',$data['end_time'])
            ->where('id','<>',$id)
            ->count();
        $count = array_sum($count);
        if ($count > 0)
            throw new Exception('时间段内已经有活动了',HTTP_INVALID);
        $model = model::find($id);
        if (empty($model))
            throw new Exception('活动不存在',HTTP_INVALID);
        $model->save($data);
        return [HTTP_SUCCESS,model::find($id)];
    }

    /**
     * 删除配置
     * @param int $id
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function delete(int $id) : array{
        $data = model::find($id);
        if (empty($data))
            throw new Exception('活动不存在',HTTP_INVALID);
        $data->delete();
        return [HTTP_SUCCESS,'删除成功'];
    }

    /**
     * 配置列表
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function list(int $page, int $size) : array{
        $data = input('post.');
        $list = model::field('id,name,start_time,end_time')
            ->where($this->where($data))
            ->where('mall_id',$this->user['mall_id'])
            ->paginate(['page' => $page,'list_rows' => $size]);
        return [HTTP_SUCCESS,$list];
    }

    private function where(array $data) : array{
        $where = [];
        !empty($data['name']) && $where[] = ['name','like',$data['name'].'%'];
        !empty($data['start_start_time']) && $where[] = ['start_time','>=',$data['start_start_time']];
        !empty($data['end_start_time']) && $where[] = ['start_time','<=',$data['end_start_time']];
        !empty($data['start_end_time']) && $where[] = ['end_time','>=',$data['start_end_time']];
        !empty($data['end_end_time']) && $where[] = ['end_time','<=',$data['end_end_time']];
        return $where;
    }

    /**
     * 配置详情
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id) : array{
        $data = model::find($id)
            ->append(['coupon_data']);
        return [HTTP_SUCCESS,$data];
    }

    /**
     * 新手领取列表
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function receive_list(int $page, int $size) : array{
        $list = NewKidGift::where('mall_id',$this->user['mall_id'])
            ->with(['UserNicknamePicurl','Config'])
            ->paginate(['page' => $page,'list_rows' => $size]);
        return [HTTP_SUCCESS,$list];
    }

    /**
     * 新手领取详情
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function receive_read(int $id){
        $data = NewKidGift::with(['UserNicknamePicurl','Config'])
            ->find($id)
            ->append(['coupon_data']);
        return [HTTP_SUCCESS,$data];
    }
}