<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);
namespace app\madmin\service;
use app\madmin\model\Commodity as admin;
use app\madmin\model\Merchant;
use app\utils\TrimData;
use think\Exception;
class CommodityService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = admin::field(
            [
                'id',
                'merchant_id',
                'type',
                'name',
                'subtitle',
                'master',
                'classify_id',
                'total',
                'sell',
                'original_price',
                'sell_price',
                'min_price',
                'max_price',
                'sort',
                'is_virtual',
                'virtual_sell',
                'is_distribution',
                'distribution',
                'has_sku',
                'audit',
                'status',
                'create_time',
                'update_time'
            ])->with(['merchant'])->where('merchant_id', '<>', 1);
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'begin_date', 'end_date']);
        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        if (!empty($model->has_sku))
            $model = admin::with(['sku', 'sku.inventory'])->find($id);
        return [HTTP_SUCCESS, $model];
    }

    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);

        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);

        return [HTTP_CREATED, $admin];
    }
    public function updateGoodsName(int $id ,$data){
//        $update_time = $data['update_time'];
//        unset($data['update_time']);
        $merchantId = admin::where(['id'=>$id])
            ->lock(true)
            ->value("merchant_id");
        if (empty($merchantId)){
            throw new Exception("请刷新重试", HTTP_NOTACCEPT);
        }
        $isSelf = Merchant::where("id", $merchantId)->value("is_self");
        if (empty($isSelf)) {
            throw new Exception("请选择自营商店的商品", HTTP_NOTACCEPT);
        }
        $commodity = admin::update(["name" => $data['name']], ['id' => $id]);
        return [HTTP_CREATED, $commodity];
    }
    public function updateSort(int $id, $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);

        $merchantId = admin::where(['id'=>$id,"update_time"=>$update_time])
            ->lock(true)
            ->value("merchant_id");
        if (empty($merchantId)){
            throw new Exception("请刷新重试", HTTP_NOTACCEPT);
        }
        $isSelf = Merchant::where("id", $merchantId)->value("is_self");
        if (empty($isSelf)) {
            throw new Exception("请选择自营商店的商品", HTTP_NOTACCEPT);
        }
        $commodity = admin::update(["sort" => $data['sort']], ['id' => $id]);
        return [HTTP_CREATED, $commodity];
    }

}
