<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use think\facade\Db;

class LesseeService
{

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function entry()
    {
        global $user;
        $entryList = Db::table("jiecheng_lessee")
            ->where("id", $user->mall_id)
            ->value("entry_type");
        if ($entryList)
            $entryList = json_decode($entryList, true);
        return [HTTP_SUCCESS, $entryList];
    }

}
