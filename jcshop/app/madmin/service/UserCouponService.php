<?php
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\UserCoupon as admin;
use app\utils\TrimData;


/**
 * 优惠券服务
 * @package app\madmin\service
 */
class UserCouponService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,user_id,coupon_id,order_id,unified_order_no,used_type,type,status,create_time,update_time');
        $admin = TrimData::searchDataTrim($admin, $data, ['unified_order_no', 'begin_date', 'end_date']);

        $list = $admin->where($data)
            ->with(['coupon','user'])
            ->paginate(['page' => $page, 'list_rows' => $size]);

        return [HTTP_SUCCESS, $list];
    }

}
