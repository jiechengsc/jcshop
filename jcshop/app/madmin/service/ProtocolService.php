<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Protocol as admin;
use app\utils\TrimData;
use think\Exception;

class ProtocolService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,type,title,subhead,content,status,create_time,update_time');
        $admin = TrimData::searchDataTrim($admin, $data, ['title', 'begin_date', 'end_date']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        if (isset($data['status'],$data['type']) && $data['status'] == 1){
            admin::where(['status' => 1,'type'=> $data['type']])->update(['status' =>0]);
        }
        $model = admin::create($data);
        return [HTTP_CREATED, $model];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);
        $model = admin::find($id);
        if (empty($model)){
            throw new Exception("数据不存在",HTTP_NOTACCEPT);
        }

        if (isset($data['type']) && $data['type'] == $model->type){
            if ($model->status != 1 && isset($data['status']) && $data['status'] == 1) {
                admin::where(['status' => 1,'type'=> $data['type']])->update(['status' =>0]);
            }
        }elseif(isset($data['type']) && $data['type'] != $model->type){
            if (isset($data['status']) && $data['status'] == 1) {
                admin::where(['status' => 1,'type'=> $data['type']])->update(['status' =>0]);
            }
        }elseif ($model->status != 1 && isset($data['status']) && $data['status'] == 1){
            admin::where(['status' => 1,'type'=> $model->type])->update(['status' =>0]);
        }
        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);
        return [HTTP_CREATED, $admin];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

}
