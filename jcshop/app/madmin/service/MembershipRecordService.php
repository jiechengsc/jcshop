<?php
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Coupon;
use app\madmin\model\MembershipRecord as admin;

class MembershipRecordService
{

    private const fields = [
        '*'
    ];

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index(int $page,int $size)
    {
        global $user;
        $list = admin::field(self::fields)
            ->where('mall_id',$user->mall_id)
            ->paginate(['page' => $page,'list_rows' => $size]);
        if (!$list->isEmpty()) {
            $list = $list->toArray();
            foreach ($list['data'] as $k => $v) {
                if (!empty($v['is_present_coupon'])) {
                    $list['data'][$k]["present_coupon"] = Coupon::where(['id' => explode(",", $v['present_coupon'])])
                        ->field('*')
                        ->select();
                }
            }
        }
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::with(['present'])->field(self::fields)
            ->find($id);
        return [HTTP_SUCCESS, $model];
    }

}
