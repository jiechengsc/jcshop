<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;

use app\madmin\model\Merchant;
use app\madmin\model\OrderEvaluate;

class EvaluateService
{
    /**获取评论列表
     * @param array $data
     * @return array
     */
    public function evaluateList(array $data)
    {
        $res = OrderEvaluate::where($this->bulidData($data))
            ->alias('a')
            ->join('user u', 'a.user_id = u.id', 'LEFT')
            ->join('order o', 'a.order_id = o.id', 'LEFT')
            ->join('commodity c', 'a.commodity_id = c.id', 'LEFT')
            ->field('
            a.content,a.img_url,a.score,a.create_time,a.remake,a.reply,a.state,
            u.nickname,u.picurl,o.order_no,c.name,c.master,a.id
            ')
            ->order('a.state','asc')
            ->paginate(['page' => $data['page'], 'list_rows' => $data['size']]);
        return [HTTP_SUCCESS, $res];
    }

    private function bulidData(array $data)
    {
        global $user;
        $where = [];
        $where[] = ['a.mall_id', '=', $user->mall_id];
        $res = Merchant::where(['is_self'=>1])->find();
        if(!empty($data['is_self']) && $data['is_self'] == 1) {
           $where[] = ['o.merchant_id','=',$res['id']];
        }
        if (!empty($data['is_self']) && $data['is_self'] == 2){
            $where[] = ['o.merchant_id','<>',$res['id']];
        }
        !empty($data['status']) && $where[] = ['a.state', '=', $data['status']];
        !empty($data['name']) && $where[] = ['u.nickname|u.name', '=', $data['name']];
        !empty($data['order_no']) && $where[] = ['o.order_no', '=', $data['order_no']];
        !empty($data['cname']) && $where[] = ['c.name', 'like', '%' . $data['cname'] . "%"];
        return $where;
    }
}