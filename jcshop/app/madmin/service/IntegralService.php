<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;

use app\madmin\model\Configuration;
use app\madmin\model\IntegralRecord;


class IntegralService
{
    private $user;
    public function __construct(){
        global $user;
        $this->user = $user;
    }

    /**
     * 列表
     * @param array $data
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function list(array $data, int $page = 1, int $size = 10) : array{

        $list = IntegralRecord::where($this->where($data))
            ->where('status',1)
            ->where('mall_id',$this->user['mall_id'])
            ->with(['orderNo','userNicknamePicurl'])
            ->order('create_time DESC')
            ->append(['type_text'])
            ->paginate(['page' => $page, 'list_rows' => $size]);

        return [HTTP_SUCCESS,$list];
    }

    /**
     * 条件
     * @param array $data
     * @return array
     */
    private function where(array $data) : array{
        $where = [];
        !empty($data['user_id']) && $where[] = ['user_id','=',$data['user_id']];
        !empty($data['start_date']) && $where[] = ['create_time','>=',$data['start_date']];
        !empty($data['end_date']) && $where[] = ['create_time','<=',$data['start_date']];
        if(isset($data['type']) && $data['type'] == 0) $where[] = ['type','in',[1,3,4,6,7,8,11]];
        if(isset($data['type']) && $data['type'] == 1) $where[] = ['type','in',[2,5,9,10]];
        return $where;
    }

    /**
     * 修改配置
     * @param int $is_open
     * @param int $deduction
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function configUpdate(int $is_open, float $deduction,float $get_integral_ratio,int $get_integral_type) : array{
        $config = Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->find()
            ->configuration;
        $config['is_open'] = $is_open;
        $config['deduction'] = $deduction;
        $config['get_integral_type'] = $get_integral_type;
        $config['get_integral_ratio'] = $get_integral_ratio;
        Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->save(['configuration' => $config]);
        return [HTTP_SUCCESS,$config];
    }

    /**
     * 获取配置
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function configFind() : array{
        $config = Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->find();
        if (empty($config)) {
            Configuration::create([
                'type' => 'integralBalance',
                'mall_id' => $this->user['mall_id'],
                'configuration' => Configuration::$integralBalance,
                'create_time' => date('Y-m-d H:i:s'),
                'status' => 1
            ]);
            return [
                HTTP_SUCCESS,[
                    'is_open' => 0,
                    'deduction' => 0.01,
                    'get_integral_type' => 1,
                    'get_integral_ratio' => 0.1
                ]
            ];
        }
        $config = $config->configuration;
        return [
            HTTP_SUCCESS,[
                'is_open' => $config['is_open'] ?? 0,
                'deduction' => $config['deduction'] ?? 0.01,
                'get_integral_type' => $config['get_integral_type'] ?? 0,
                'get_integral_ratio' => $config['get_integral_ratio'] ?? 0
            ]
        ];
    }

    /**
     *获取分享设置
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function findShare() : array{
        $config = Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->find();
        if (empty($config)) {
            Configuration::create([
                'type' => 'integralBalance',
                'mall_id' => $this->user['mall_id'],
                'configuration' => Configuration::$integralBalance,
                'create_time' => date('Y-m-d H:i:s'),
                'status' => 1
            ]);
            $config = Configuration::where('type','integralBalance')
                ->where('mall_id',$this->user['mall_id'])
                ->find()
                ->configuration;
        }else{
            $config = $config->configuration;
            if(empty($config['daily_limit']))
                $config['daily_limit'] = 0;
            if(empty($config['share']))
                $config['share'] = ["old" => 0,'new' => 0];
        }
        return [HTTP_SUCCESS,['daily_limit' => $config['daily_limit'],'share' => $config['share']]];
    }

    /**
     *配置分享设置
     * @param int $new 新用户分享
     * @param int $old
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function saveShare(int $new, int $old,int $daily_limit) : array{
        $config = Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->find()
            ->configuration;
        $config['share'] = ['old' => $old,'new' => $new];
        $config['daily_limit'] = $daily_limit;
        Configuration::where('type','integralBalance')
            ->where('mall_id',$this->user['mall_id'])
            ->save(['configuration' => $config]);
        return [HTTP_SUCCESS,['daily_limit' => $config['daily_limit'],'share' => $config['share']]];
    }
}