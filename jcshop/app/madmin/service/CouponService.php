<?php
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Coupon as admin;
use app\utils\TrimData;


/**
 * 优惠券服务
 * @package app\madmin\service
 */
class CouponService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = admin::field("*")->with(['merchant']);
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'begin_date', 'end_date']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);

        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);
        return [HTTP_CREATED, $admin];
    }

}
