<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;

use think\db\exception\PDOException;
use think\Exception;
use think\exception\ErrorException;
use think\facade\Db;

class ServiceFactory
{

    public static function getResult(String $class,$method,...$argsData)
    {
        Db::startTrans();
        try {
            $class      = new \ReflectionClass("app\madmin\service\\".$class);
            $instance   = $class->newInstance();
            $method     = $class->getMethod($method);
            $method->setAccessible(true);
            $parameters = $method->getParameters();
            $args       = array_combine($parameters, $argsData);
            $result     = $method->invokeArgs($instance,$args);
            Db::commit();
            return $result;
        } catch (\ReflectionException $e) {
            Db::rollback();
            return [$e->getCode(), $e->getMessage()];
        } catch (PDOException $e) {
            Db::rollback();
            return [HTTP_INVALID, $e->getMessage()];
        } catch (ErrorException $e) {
            Db::rollback();
            return [$e->getCode(), $e->getMessage()];
        } catch (Exception $e){
            Db::rollback();
            return [$e->getCode(), $e->getMessage()];
        }

    }

}