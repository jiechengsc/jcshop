<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\service;


use app\madmin\model\Commodity;
use app\madmin\model\RecommendMenu;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class FloatMenuService
{
    /**
     * @param array $data
     * @param array $res
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getConfig(array $res)
    {
        $find = RecommendMenu::where('mall_id', $res['mall_id'])->find();
        return [HTTP_SUCCESS, $find['content'] ?? null];
    }

    /**
     * @param array $data
     * @param array $res
     * @return array
     * @throws DbException
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     */
    public function save(array $data, array $res)
    {
        $data['mall_id'] = $res['mall_id'];
        $find = RecommendMenu::where('mall_id', $data['mall_id'])->find();
        if (empty($find))
            $create = RecommendMenu::create($data);
        else
            $create = RecommendMenu::where('mall_id', $data['mall_id'])->update($data);
        return [HTTP_SUCCESS, $create];
    }

}