<?php
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Store as admin;
use app\utils\TrimData;
use think\model\Relation;

class StoreService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = admin::field('id,merchant_id,name,mobile,district,address,longitude,latitude,remark,audit,create_time,update_time')
            ->with(['merchant' => function (Relation $query) {
                $query->bind(['merchant_name' => 'name']);
            }] );
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'mobile', 'begin_date', 'end_date']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::with(['merchant'])->find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);

        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);

        return [HTTP_CREATED, $admin];
    }

}
