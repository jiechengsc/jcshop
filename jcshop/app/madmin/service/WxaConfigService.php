<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Configuration;
use app\madmin\model\WechataPlugins;
use GuzzleHttp\Client;
use think\Exception;
use think\facade\Config;

class WxaConfigService
{

    private $client, $base, $user;


    public function __construct()
    {
        $this->client = new Client();
        $this->base = Config::get('base');

        global $user;
        $this->user = $user;
    }


    public function login()
    {
        $url = request()->domain() . "/admin/wxa/config/login";
        $response = $this->client->get($url);
        if ($response->getStatusCode() !== HTTP_SUCCESS) {
            return [HTTP_NOTACCEPT, json_decode($response->getBody()->getContents(), true)];
        }
        return [HTTP_SUCCESS, json_decode($response->getBody()->getContents(), true)];
    }
    public function islogin()
    {
        $url = request()->domain() . "/admin/wxa/config/islogin";
        $response = $this->client->get($url);
        if ($response->getStatusCode() !== HTTP_SUCCESS) {
            return [HTTP_NOTACCEPT, json_decode($response->getBody()->getContents(), true)];
        }
        return [HTTP_SUCCESS, json_decode($response->getBody()->getContents(), true)];
    }
    public function preview(string $plugins)
    {
        $wxaConfiguration = Configuration::where(['type' => "wxa"])->value("configuration");
        if (!$wxaConfiguration)
            throw new Exception("请完善小程序配置", HTTP_NOTACCEPT);
        $wxaConfiguration = json_decode($wxaConfiguration, true);

        $url = request()->domain() . "/admin/wxa/config/preview";
        $options = [
            'json' =>
                [
                    'appid' => $wxaConfiguration['appid'],
                    'baseUrl' => request()->domain(),
                    'mallId' => $this->user->mall_id,
                    'plugins' => $plugins
                ]
        ];



        $response = $this->client->post($url, $options);

        $retData = json_decode($response->getBody()->getContents(), true);
        if (!isset($retData['error_msg'])){
            if (empty($plugins) || $plugins == '0'){
                $plugins = '';
            }
            $wechata = WechataPlugins::find();
            if ($wechata){
                $wechata->plugins = $plugins;
                $wechata->save();
            }else{
                WechataPlugins::create(['plugins' => $plugins]);
            }
        }

        return [200, $retData];
    }

    public function sendFile(array $data)
    {
        $url = request()->domain() . "/admin/wxa/config/send";
        $options = [
            'json' =>
                [
                    'version' => $data['version'],
                    'desc' => $data['desc'],
                ]
        ];
        $response = $this->client->post($url, $options);
        $res = $response->getBody()->getContents();
        return [200, json_decode($res, true)];
    }

}
