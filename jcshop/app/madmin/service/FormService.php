<?php


namespace app\madmin\service;

use app\madmin\model\FormTemplate as template;
use app\madmin\model\FormAnswer AS answer;
use think\Exception;

class FormService
{
    protected $user;
    public function __construct(){
        global $user;
        $this->user = $user;
    }

    /**
     * 创建自定义表单模板
     * @param string $title
     * @param string $config
     * @param int $tries_limit
     * @return array
     */
    public function create(string $title, string $config, int $tries_limit,string $image) : array{
        $template = template::create([
            'title' => $title,
            'config' => $config,
            'image' => $image,
            'tries_limit' => $tries_limit,
            'create_time' => date('Y-m-d H:i:s'),
            'update_time' => date('Y-m-d H:i:s'),
            'merchant_id' => $this->user['merchant_id'],
            'status' => 1
        ]);
        if (!empty($template->id))
            return [HTTP_SUCCESS,$template];
        return [HTTP_INVALID,'创建表单失败'];
    }

    /**
     * 更新自定义模板
     * @param int $id
     * @param string $title
     * @param string $config
     * @param int $tries_limit
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(int $id, array $data)  : array {
        $template = template::find($id);
        if (empty($template))
            throw new Exception('模板不存在',HTTP_INVALID);
        $template->save($data);
        return [HTTP_SUCCESS,template::find($id)];
    }

    /**
     * 删除自定义模板
     * @param int $id
     * @return array
     * @throws Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function delete(int $id) : array{
        $template = template::find($id);
        if (empty($template))
            throw new Exception('模板不存在',HTTP_INVALID);
        $is_bool = $template->delete();
        if ($is_bool)
            return [HTTP_SUCCESS,'删除成功'];
        else
            return [HTTP_INVALID,'删除失败'];
    }

    /**
     * 自定义表单列表
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(int $page, int $size) : array{
        $list = template::field('id,title,image')
            ->where('merchant_id',$this->user['merchant_id'])
            ->order('create_time DESC')
            ->paginate(['page' => $page,'list_rows' => $size]);
        return [HTTP_SUCCESS,$list];
    }

    /**
     *
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id) : array{
        $read = template::find($id);
        return [HTTP_SUCCESS,$read];
    }

    /**
     * 答案列表
     * @param int $template_id
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function answerList(int $template_id, int $page, int $size) : array{
        $list = answer::where('template_id',$template_id)
            ->hidden(['delete_time'])
            ->with(['User'])
            ->order('create_time DESC')
            ->paginate(['page' => $page,'list_size' => $size]);
        return [HTTP_SUCCESS,$list];
    }
}