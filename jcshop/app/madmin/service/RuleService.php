<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\Addons;
use app\madmin\model\MallShopRule;
use app\madmin\model\Rule;
use app\madmin\model\Rule as admin;
use app\shop_admin\model\Rule as shopRule;

class RuleService
{

    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $list = admin::where('status', 1)->field('id,name,group,resource_name,resource,action,method')
            ->select();
        return [HTTP_SUCCESS, $list];
    }

    public function findAllValue()
    {
        $addonsService = new AddonsService();
        list($code, $addons_list) = $addonsService->list();
        $is_not_active_addons = [];
        $is_active_addons=[];
        foreach ($addons_list as $addons) {
            if (!$addons['is_active']) {
                $is_not_active_addons[] = $addons['id'];
            }else{
                $is_active_addons[]=$addons['id'];
            }
        }

        //$is_active_addons[]=0;
//        $addonsList = Addons::where(['mall_id' => $this->user->mall_id])->column('addons_id');
//
//        $addonsList[] = 0;

        $ruleList = admin::where('status', 1)
//            ->where('up','not in',$is_not_active_addons)
            ->field('id,name,group,resource_name')
            ->select()->toArray();
        $ruleData = $this->clearRuleList($ruleList);

        return [HTTP_SUCCESS, ['rule' => $ruleData]];
    }

    private function clearRuleList($ruleList)
    {
        $data = [];
        $a = 0;
        foreach ($ruleList as $k => $v) {
            $data[$v['group']][$v['resource_name']][$a]['id'] = $v['id'];
            $data[$v['group']][$v['resource_name']][$a]['action'] = $v['name'];
            $a++;
        }
        $ruleData = [];
        $i = 0;
        foreach ($data as $k => $v) {
            $ruleData[$i]['group'] = $k;
            $j = 0;
            foreach ($v as $x => $y) {
                $ruleData[$i]['resourceList'][$j]['resource'] = $x;
                $ruleData[$i]['resourceList'][$j]['actionList'] = array_values($y);
                $j++;
            }
            $i++;
        }

        return $ruleData;

    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
//        $data =array_filter($data,function ($val){
//            if (is_array($val) || (is_string($val) && "" !=trim($val))){
//                return true;
//            }
//            return false;
//        });

        $list = admin::where($data)
            ->field('id,name,group,resource_name,resource,action,method,status,create_time,update_time')
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        $model = admin::create($data);
        return [HTTP_CREATED, $model];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);
        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);
        return [HTTP_CREATED, $admin];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

}
