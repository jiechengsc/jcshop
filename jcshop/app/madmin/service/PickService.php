<?php


namespace app\madmin\service;


use app\madmin\model\Commodity;
use app\madmin\model\RecommendGoods;
use think\db\exception\DataNotFoundException;
use think\db\exception\DbException;
use think\db\exception\ModelNotFoundException;

class PickService
{
    /**
     * @param array $data
     * @param array $res
     * @return array
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     */
    public function getConfig(array $res)
    {
        $find = RecommendGoods::where('mall_id', $res['mall_id'])->find();
        return [HTTP_SUCCESS, $find['content'] ?? null];
    }

    /**
     * @param array $data
     * @param array $res
     * @return array
     * @throws DbException
     * @throws DataNotFoundException
     * @throws ModelNotFoundException
     */
    public function save(array $data, array $res)
    {
        $data['mall_id'] = $res['mall_id'];
        $find = RecommendGoods::where('mall_id', $data['mall_id'])->find();
        if (empty($find))
            $create = RecommendGoods::create($data);
        else
            $create = RecommendGoods::where('mall_id', $data['mall_id'])->update($data);
        return [HTTP_SUCCESS, $create];
    }

    /**
     * @param array $data
     * @return array
     * @throws DbException
     */
    public function getGroundingGoods(array $data)
    {
        $res = Commodity::where('status', 1);
        if ($data['type']) $res->order('sell', 'desc');
        $res->paginate(['page' => $data['page'], 'list_rows' => $data['size']]);
        return [HTTP_SUCCESS, $res];
    }


}