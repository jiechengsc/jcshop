<?php
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\AssettransferLog as admin;
use app\utils\TrimData;

class AssettransferLogService
{

    /**
     * @param array $data
     * @param int $page
     * @param int $size
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function list(int $page = 1, int $size = 10)
    {

        $where = [

        ];

        $admin = admin::field("*")->with(['user', 'gifted_user']);


        $admin = TrimData::searchDataTrim($admin, $where, ['title', 'begin_date', 'end_date', 'begin_time']);

        //thinkphp內部haswhere的bug 暂时只支持其中一种
        if (input('keyword')) {
            $keyword = input('keyword');
            $admin->haswhere('user', function ($query) use ($keyword) {
                $where[] = ['id|mobile|nickname', 'like', '%' . $keyword . '%'];
                $query->where($where);
            });
        } elseif (input('gifted_keyword')) {
            $keyword = input('gifted_keyword');
            $admin->haswhere('giftedUser', function ($query) use ($keyword) {

                $where[] = ['id|mobile|nickname', 'like', '%' . $keyword . '%'];
                $query->where($where);
            });
        }
        if (input('create_time')) {
            $admin->where($admin->getTable() . '.create_time', '>', input('create_time'));
        }
        if (input('money_start')) {
            $admin->where($admin->getTable() . '.money', '>', input('money_start'));
        }
        if (input('money_end')) {
            $admin->where($admin->getTable() . '.money', '<', input('money_end'));
        }
        if (input('type')){
            $admin->where($admin->getTable() . '.type', '=', input('type', 0));
        }

        $list = $admin->order($admin->getTable() . '.id', 'desc')->paginate(['page' => $page, 'list_rows' => $size]);


        return [HTTP_SUCCESS, $list];
    }
}
