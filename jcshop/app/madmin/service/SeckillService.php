<?php

namespace app\madmin\service;

use think\Exception;
use think\facade\Db;
use app\madmin\model\Seckill;
use app\madmin\model\SeckillCommodity;

class SeckillService
{

    private function buildWhere(array $data)
    {
        $where = [];
        !empty($data['status']) && $where[] = ['status', 'in', $data['status']];
        return $where;
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index($page = 1, $size = 10, $search = [])
    {
        $list = Seckill::where($this->buildWhere($search))->order('status ASC')->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     * @throws
     */
    public function save(array $data)
    {
        $check = Seckill::check($data);
        try {
            Db::startTrans();
            $group = Seckill::create($check['group']);
            foreach ($check['commodity'] as $key => $value) {
                $commodity = $group->commodity()->save($value);
                if ($value['has_sku'] == 1) {
                    $commodity->sku()->saveAll($value['skus']);
                }
            }
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            throw new Exception($e->getMessage(), HTTP_NOTACCEPT);
        }
        return [HTTP_CREATED, $group];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = Seckill::with(['commodity' => function ($sql) {
            $sql->with(['goods', 'sku' => function ($query) {
                $query->with('sku');
            }]);
        }])->find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     * @throws
     */
    public function update(int $id, array $data)
    {
        try {
            Db::startTrans();
            $rules = json_decode(Seckill::find($id)['rules'], true);
            $rules['is_show'] = $data['is_show'];
            $update = [
                'rules' => json_encode($rules),
                'end_time' => $data['end_time']
            ];
            $goods_update = [
                'show_time' => null,
                'end_time' => $data['end_time']
            ];
            if ($data['is_show'][0] == 1) {
                $goods_update['show_time'] = $data['is_show'][1];
            }
            // 过期
            if ($data['end_time'] < date("Y-m-d H:i:s", time())) {
                $update['status'] = 2;
                $goods_update['status'] = 2;
            }
            Seckill::update($update, ['id' => $id]);
            SeckillCommodity::update($goods_update, ['seckill_id' => $id]);
            Db::commit();
        } catch (Exception $e) {
            Db::rollback();
            throw new Exception("发生错误！", HTTP_NOTACCEPT);
        }
        return [HTTP_CREATED, "更新成功！"];
    }

    /**
     * @param $id
     * @return array
     * @throws
     */
    public function stop($id)
    {
        try {
            Seckill::update([
                'close_time' => date('Y-m-d H:i:s', time()),
                'status' => 2
            ], ['id' => $id]);
            $res = SeckillCommodity::update(['status' => 2], ['seckill_id' => $id]);
        } catch (Exception $e) {
            Db::rollback();
            throw new Exception("发生错误！", HTTP_NOTACCEPT);
        }
        return [HTTP_CREATED, $res];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        Seckill::update(['status' => 2], ['id' => $id]);
        SeckillCommodity::update(['status' => 2], ['seckill_id' => $id]);
        Seckill::destroy($id);
        return HTTP_NOCONTEND;
    }

}
