<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\service;

use app\madmin\model\PayMode as admin;
use app\utils\TrimData;
use think\facade\Cache;
use think\Model;

class PayModeService
{

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index()
    {
        $list = admin::field('id,name,mode,type')->select();
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,name,mode,type');
        $admin = TrimData::searchDataTrim($admin, $data, ['name']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        $model = admin::create($data);
        Cache::set(checkRedisKey('WXPAY:OPTIONS'),$model->toArray(),0);
        self::hidePem($model);
        return [HTTP_CREATED, $model];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::find($id);
        self::hidePem($model);
        return [HTTP_SUCCESS, $model];
    }

    private function hidePem(Model &$model)
    {
        empty($model->cert) ? $model->cert = false : $model->cert = true;
        empty($model->wxkey) ? $model->wxkey = false : $model->wxkey = true;
        empty($model->M_cert) ? $model->M_cert = false : $model->M_cert = true;
        empty($model->M_wxkey) ? $model->M_wxkey = false : $model->M_wxkey = true;
        empty($model->rootca) ? $model->rootca = false : $model->rootca = true;

    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        if (isset($data['status']) && $data['status'] == 1) {
            admin::update(['status' => 0]);
        }
        $admin = admin::where('id', $id)
            ->save($data);
        if (isset($data['status']) && $data['status'] == 1) {
            Cache::set(checkRedisKey('WXPAY:OPTIONS'),admin::find($id)->toArray(),0);
        }
        return [HTTP_CREATED, $admin];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

}
