<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class AgentBillTemporaryValidate extends Validate
{
    protected $rule = [
        'status' => "in:1,2",
        'update_time|更新时间' => 'require|dateFormat:Y-m-d H:i:s'
    ];

    protected $message = [
        'status.integer' => "更新有误",
    ];

    protected $scene = [
        'update' => ['update_time', 'status'],


    ];

}