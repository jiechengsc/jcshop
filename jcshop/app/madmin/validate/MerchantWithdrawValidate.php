<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class MerchantWithdrawValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'merchant_id' => 'integer',
        'merchant_name' => 'max:8',
        'status' => 'integer',
        'begin_date' => 'dateFormat:Y-m-d',
        'end_date' => 'dateFormat:Y-m-d',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'merchant_id.integer' => '商户错误',
        'merchant_name.max' => '商户名限制再8个字符之内',
        'status.integer' => '状态错误',
        'update_time' => '更新时间不正确'
    ];
    protected $scene = [
        'list' => ['page','size','merchant_id','merchant_name','status'],
        'update' => ['update_time','status']
    ];
}