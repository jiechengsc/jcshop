<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class ArticleTypeValidate extends Validate
{

    protected $rule = [
        'name|名称' => 'require|min:1|max:16',
        'status' => 'integer'
    ];

    protected $message = [
        'status.integer' => '状态错误'
    ];

    protected $scene = [
        'save' => ['name', 'status']
    ];

    public function sceneUpdate()
    {
        return $this->only(['name', 'status'])
            ->remove('name', 'require');
    }

}