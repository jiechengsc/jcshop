<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class MembershipLevelValidate extends Validate
{
    protected $rule = [
        'level|会员等级' => 'require|number|between:1,100',
        'indate|有效时长' => 'require|number',
        'discount|会员折扣' => 'require|between:0,9.9',
    ];

    protected $scene = [
        'save' => ['level', 'indate', 'discount'],
    ];

    public function sceneUpdate()
    {
        return $this->remove([
            'level' => 'require',
            'indate' => 'require',
            'discount' => 'require'
        ]);
    }
}