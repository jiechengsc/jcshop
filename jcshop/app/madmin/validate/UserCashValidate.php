<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class UserCashValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'begin_date' => 'dateFormat:Y-m-d',
        'end_date' => 'dateFormat:Y-m-d',
        'status' => 'number',
        'user_id' => 'integer',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'begin_date' => '开始时间必须是日期',
        'end_date' => '结束时间必须是日期',
        'status' => '状态错误',
        'update_time' => '更新时间不正确'
    ];
    protected $scene = [
        'list' => ['page','size','begin_date','end_date','status','user_id'],
        'update' => ['update_time','status']
    ];
}