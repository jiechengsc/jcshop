<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class NewKidGiftValidate extends Validate
{
    protected $rule = [
        'name' => 'require',
        'start_time' => 'require|dateFormat:Y-m-d H:i:s',
        'end_time' => 'require|dateFormat:Y-m-d H:i:s',
        'config' => 'require',
        'status' => 'in:1,2'
    ];
    protected $message = [
        'name' => '姓名不能为空',
        'start_time' => '开始时间不合规范',
        'end_time' => '结束时间不合规范',
        'config' => '配置不能为空',
        'status' => '状态错误'
    ];
    protected $scene = [
        'create' => ['name','start_time','end_time','config','status'],
        'update' => ['status','start_time','end_time']
    ];
}