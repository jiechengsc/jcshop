<?php
namespace app\madmin\validate;

use think\Validate;

class SeckillValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',

        'title' => 'require|max:20',
        'start_time' => 'require|date',
        'end_time' => 'require|date',
        'client_type' => 'require',
        'rules' => 'require',
        'commodity' => 'require',

        'commodity_id' => 'require',
        'activity_stock' => 'require',
        'activity_price' => 'require',
        'has_sku' => 'require',

        'is_show' => 'require',
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',

        'title.require' => '名称未上传',
        'title.max' => '名称超过最大限定20个字符',
        'start_time.require' => '开始时间必填',
        'end_time.require' => '结束时间必填',
        'client_type.require' => '渠道必选',

        'commodity_id' => '商品必填',
        'activity_stock' => '库存必填',
        'activity_price' => '秒杀价必填',
    ];
    protected $scene = [
        'index' => ['page', 'size'],
        'save' => ['title', 'start_time', 'end_time', 'client_type', 'rules', 'commodity'],
        'update' => ['is_show', 'end_time'],
        'commodity' => ['commodity_id', 'activity_stock', 'activity_price', 'has_sku'],
    ];
}