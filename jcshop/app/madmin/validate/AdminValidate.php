<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class AdminValidate extends Validate
{
    protected $rule = [
        'page|页数' => 'require|integer',
        'size|条数' => 'require|integer',
        'begin_date|开始时间' => 'dateFormat:Y-m-d',
        'end_date|结束时间' => 'dateFormat:Y-m-d',
        'username|账号' => 'require|min:6|max:32',
        'password|密码' => 'require|min:6|max:32',
        'mobile|手机号' => 'mobile',
        'name|姓名' => 'chsDash',
        'role_id|角色' => 'require|integer',
        'status' => "integer",
        'update_time|更新时间' => 'require|dateFormat:Y-m-d H:i:s'
    ];

    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'role_id.integer' => "角色选择有误",
        'status.integer' => "管理员状态有误",
    ];

    protected $scene = [
        'list' => ['page', 'size', 'begin_date', 'end_date', 'name', 'mobile'],
        'save' => ['username', 'password', 'name', 'mobile', 'role_id', 'status'],
        'update' => ['update_time', 'username', 'password'],


    ];

    public function sceneUpdate()
    {
        return $this->only([
            'update_time',
            'username',
            'password',
            'status',
            'role_id',
            'name',
            'mobile'
        ])->remove([
            'username' => 'require',
            'password' => 'require',
            'role_id' => 'require'
        ]);
    }
}