<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class AgentValidate extends Validate
{
    protected $rule = [
        'page' => 'require',
        'size' => 'require',
        'user_id' => 'require|integer',
        'pid' => 'require|integer',
        'name' => 'require|max:32',
        'mobile' => 'require|mobile',
        'apply_id' => 'integer',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s',
        'status' => 'require|integer'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'user_id' => '关联用户不能为空',
        'pid' => '上级必须关联',
        'name.require' => '姓名必须填写',
        'name.max' => '姓名限定在32个字符以内',
        'apply_id' => '申请ID格式错误',
        'mobile' => '手机号码必须填写正确',
        'update_time' => '更新时间不正确',
        'status' => '状态不正确'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['user_id','pid','name','mobile'],
        'update' => ['update_time','status'],
        'order' => ['page','size']
    ];
}