<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class LotteryValidate extends Validate
{
    protected $rule = [
        'start_time' => 'require|dateFormat:Y-m-d H:i:s',
        'end_time' => 'require|dateFormat:Y-m-d H:i:s',
        'config' => 'require',
        'style' => 'require|in:1,2',
        'limit' => 'require|integer',
        'type' => 'require|in:1,2',
        'number' => 'require',
        'integral' => 'integer'
    ];
    protected $message = [
        'start_time' => '开始时间不正确',
        'end_time' => '结束时间不正确',
        'type' => '抽奖类型错误',
        'config' => '配置参数不能为空',
        'style' => '样式参数不正确',
        'number' => '数量不正确',
        'limit' => '上限不正确',
        'integral' => '消耗积分不正确'
    ];
    protected $scene = [
        'list' => ['style'],
        'save' => ['start_time','end_time','config','style','limit','type','integral'],
        'update' => ['start_time','end_time','config','style','limit','type','integral']
    ];
}