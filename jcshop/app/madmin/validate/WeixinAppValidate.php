<?php
declare (strict_types=1);

namespace app\madmin\validate;

use think\Validate;

class WeixinAppValidate extends Validate
{
    protected $rule = [
        'name|公司名称' => 'require',
        'code|企业代码' => 'require',
        'code_type|企业代码类型' => 'require|integer',
        'legal_persona_wechat|法人微信号' => 'require',
        'legal_persona_name|法人姓名' => 'require',
    ];

    protected $scene = [
        'save' => ['name','code','code_type','legal_persona_wechat','legal_persona_name'],
    ];
}