<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\validate;


use think\Validate;

class StatisticsValidate extends Validate
{
    protected $rule = [
        'field' => 'require|in:avg_money,total_number,total_money,avg_user_money,number,new_user_number',
        'key' => 'require|in:turnover,consumer,money',
        'date_format'   => 'in:day,month,year',
        'begin_date'    => 'dateFormat:Y-m-d',
        'end_date'      => 'dateFormat:Y-m-d'
    ];
    protected $message = [
        'field.require'              => '显示字段必选',
        'field.in'                   => '显示字段不在规范内',
        'date_format'              => '日期格式错误',
        'begin_date.dateFormat'    => '请传入正确的时间戳',
        'end_date.dateFormat'      => '请传入正确的时间戳',
    ];
    protected $scene = [
        'count' => ['field','begin_date','end_date','date_format'],
        'chart' => ['key','begin_date','end_date','date_format']
    ];
}