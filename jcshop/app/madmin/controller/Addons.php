<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\facade\Db;
use think\Response;


class Addons extends BaseAction
{
    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    public function list(){
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    //安装插件
    public function install(){
        $addons_id=request()->post('addons_id', 0);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);

        if (trim($msg) == "token无效" or strpos($msg, "该域名未授权") !== false){
            Db::name("addons")->where(['addons_id'=>$addons_id])->update(['token'=>'']);
            return json(['msg' => "激活码已失效,请重新激活"], $code);
        }
        return json(['msg' => $msg], $code);
    }
    //卸载插件
    public function uninstall(){
        $addons_id = request()->post('addons_id', 0);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$addons_id);
        return json(['msg' => $msg], $code);
    }
    //插件安装更新日志
    public function updateRecordList($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$id);
        return json(['msg' => $msg], $code);
    }
    public function download(){
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return Response::create($msg, 'file')->name('')->isContent(false)->expire(180);
        return download($msg);
    }
}
