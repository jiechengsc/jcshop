<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use app\utils\Wechat;
use app\utils\Weixin;
use think\App;

class AssettransferLog extends BaseAction
{
    //日志列表
    public function list()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }

}