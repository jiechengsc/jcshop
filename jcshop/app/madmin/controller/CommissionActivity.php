<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use app\utils\UrlLink;
use app\utils\Wechat;
use app\utils\Weixin;
use think\App;

class CommissionActivity extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 30);
        if (!$check)
            throw new \Exception("请先激活分销达标插件", HTTP_NOTACCEPT);
    }

    //列表
    public function list()
    {
//        $url_info=parse_url("../detail/detail?id=263&id2=248");
//        dd($url_info);
//        exit();
//        $query=[
//            'mall_id=32684'
//        ];
//        if(isset($url_info['query'])){
//            $arr=array_values(explode("&",$url_info['query']));
//            $query=array_merge($query,$arr);
//        }
//        implode("&",$query);
//
//
//        $data=[
//          'path'=>'pages/index/index',
//            'query'=>'mall_id=52684',
//            'expire_type'=>1,
//            'expire_interval'=>30,
//
//        ];
//        $d=UrlLink::generate_urllink($data);


        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }

    //添加
    public function add()
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    //详情
    public function detail($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 删除
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 变更状态
     * @param $id
     * @return \think\response\Json
     */
    public function status()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    //发放奖励
    public function send()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    //奖励列表
    public function award()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }
    //发放奖励
    public function issue()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}