<?php
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
class Store extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 11);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }
    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }
    public function update(int $id)
    {
        $data = $this->request->put();

        $data = array_intersect_key($data, ['audit' => 0, 'update_time' => 0, 'audit_remark' => 0]);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }
    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}
