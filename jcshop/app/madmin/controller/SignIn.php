<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class SignIn extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (empty($this->user))
            throw new Exception('请先登录',HTTP_UNAUTH);
        $this->is_bool = \app\utils\Addons::check($this->user['mall_id'],5);
    }


    public function config(){
        if (!$this->is_bool)return json(['msg' => [],HTTP_SUCCESS]);
        $config = \app\madmin\model\Configuration::where('type','signIn')->where('mall_id',$this->user['mall_id'])->find();
        if (empty($config))$config = \app\madmin\model\Configuration::create([
            'status' => 1,
            'configuration' => \app\madmin\model\Configuration::$signIn,
            'create_time' => date('Y-m-d H:i:s'),
            'type' => 'signIn',
            'mall_id' => $this->user['mall_id']
        ]);
        return json([
            'msg' => $config
        ],HTTP_SUCCESS);
    }



    public function update() : Json{
        if (!$this->is_bool)return json(['msg' => [],HTTP_SUCCESS]);
        $config = input('put.configuration');
        list($code,$data) = ServiceFactory::getResult('SignInService','update',$config);
        return json(['msg' => $data],$code);
    }


    public function list(int $page, int $size){
        if (!$this->is_bool)return json(['msg' => [],HTTP_SUCCESS]);
        $data = input('post.');
        list($code,$list) = ServiceFactory::getResult('SignInService','list',$page,$size,$data);
        return json(['msg' => $list],$code);
    }
}