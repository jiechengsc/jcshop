<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use app\utils\Wechat;
use app\utils\Weixin;
use think\App;

class LiveRoom extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 27);
        if (!$check)
            throw new \Exception("请先激活小程序直播插件", HTTP_NOTACCEPT);
    }
    public function rollback(){

    }

    //直播间列表
    public function list()
    {
//        $weixin= new Weixin();
//        $weixin->test();
//        die('33');

//        $file=public_path("storage/shop_admin/20220308")."11281b0e6adfebbd24049f8f9249fe58.png";
//        $d=compress($file,['width' => 800, 'height' => 600, 'fixed' => true]);
//        dd($d);

        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }

    //获取二维码
    public function getQrcode($id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$id);
        return json(['msg' => $msg], $code);
    }

    //添加直播间
    public function add()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    //直播间详情
    public function detail($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    //同步直播间
    public function syncRoom()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    //是否隐藏
    public function hide($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function getReplay($id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    /**
     * 编辑直播间
     * @param $id
     * @return \think\response\Json
     */
    public function edit($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 删除直播间
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 开启/关闭直播间官方收录
     * @param $id
     * @return \think\response\Json
     */
    public function updateFeedPublic($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 开启/关闭回放功能
     * @param $id
     * @return \think\response\Json
     */
    public function updateReplay($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 开启/关闭回放功能
     * @param $id
     * @return \think\response\Json
     */
    public function goodsOnSale($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 删除直播间商品
     * @param $id
     * @return \think\response\Json
     */
    public function deleteRoomGoods($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
    /**
     * 添加商品到直播间
     * @param $id
     * @return \think\response\Json
     */
    public function addRoomGoods($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
    /**
     * 推送商品
     * @param $id
     * @return \think\response\Json
     */
    public function pushRoomGoods($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}