<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;

class LiveRoomGoods extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 27);
        if (!$check)
            throw new \Exception("请先激活小程序直播插件", HTTP_NOTACCEPT);
    }
    //获取商品库列表
    public function list()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

}