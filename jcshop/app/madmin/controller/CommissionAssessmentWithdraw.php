<?php
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;

class CommissionAssessmentWithdraw extends BaseAction
{
    
    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function update(int $id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $this->request->put());
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code((int)$code);
    }

    public function adopt(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult('AgentWithdrawService', 'adopt', $id);
        return json(['msg' => $msg], $code);
    }

    public function refuse(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult('AgentWithdrawServuce', 'refuse', $id);
        return json(['msg' => $msg], $code);
    }
}
