<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use app\Request;
use app\utils\Addons;
use think\App;

class LiveGood extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 27);
        if (!$check)
            throw new \Exception("请先激活小程序直播插件", HTTP_NOTACCEPT);
    }
    //i7lL2fnEehsd7vDnN8bY2nqSyU6mybkTb1R5Njn8g4PJAlCvtHG2shJIO_9jJ7p6
    //添加审核
    public function list()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    //更新商品
    public function updateGoods($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
    //添加审核
    public function addAudit($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
    //撤销审核
    public function resetAudit($id)
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
    //重新审核
    public function repeatAudit($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$id);
        return json(['msg' => $msg], $code);
    }
    //删除商品审核
    public function deleteAudit($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$id);
        return json(['msg' => $msg], $code);
    }
}