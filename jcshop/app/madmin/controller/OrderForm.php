<?php


namespace app\madmin\controller;


use app\madmin\model\OrderFormAnswer as answer;
use app\madmin\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class OrderForm extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function create(): Json
    {
        $title = input('title', '');
        $config = input('config', '');
        $source = input('source', 0);
        $image = input('image', '');
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'create', $title, $config, $source, $image);
        return json(['msg' => $data], $code);
    }
    public function status(): Json
    {
        $data = input('post.');
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'status', $data);
        return json(['msg' => $data], $code);
    }
    public function update($id): Json
    {
        $data = input('put.');
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'update', $id, $data);
        return json(['msg' => $data], $code);
    }

    public function delete($id): Json
    {
        list($code, $message) = ServiceFactory::getResult(class_basename($this) . 'Service', 'delete', $id);
        return json(['msg' => $message], $code);
    }

    public function list(): Json
    {
        $page = input('page',1);
        $size = input('size',10);
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'findAll', $page, $size);
        return json(['msg' => $data], $code);
    }

    public function read(int $id): Json
    {
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'read', $id);
        return json(['msg' => $data], $code);
    }

    public function answerList(int $id): Json
    {

        $page = input('page',1);
        $size = input('size',10);
        list($code, $list) = ServiceFactory::getResult(class_basename($this) . 'Service', 'answerList', $id, $page, $size);
        return json(['msg' => $list], $code);
    }
    public function templateAnswerList(int $id): Json
    {
        $page = input('page',1);
        $size = input('size',10);
        list($code, $list) = ServiceFactory::getResult(class_basename($this) . 'Service', 'templateAnswerList', $id, $page, $size);
        return json(['msg' => $list], $code);
    }

    public function answerInfo(int $id):Json{
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', 'answerInfo', $id);
        return json(['msg' => $data], $code);
    }
}