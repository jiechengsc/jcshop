<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;

class Lottery extends BaseAction
{
    protected $class_name = 'LotteryService';

    public function config_read(int $id){
        list($code,$config) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $config],$code);
    }


    public function config_list(){
        $data = input('post.');
        $page = input('page');
        $size = input('size');
        list($code,$config) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$data,$page,$size);
        return json(['msg' => $config],$code);
    }

    public function config_save(){
        $data = input('post.');
        list($code,$config) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$data);
        return json(['msg' => $config],$code);
    }

    public function config_update(int $id){
        $data = input('put.');
        list($code,$config) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id,$data);
        return json(['msg' => $config],$code);
    }


    public function config_delete(int $id){
        list($code,$config) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $config],$code);
    }


    public function config_open(int $id){
        $status = input('status');
        list($code,$bool) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id,$status);
        return json(['msg' => $bool],$code);
    }

    public function lottery_log_list(){
        $page = input('page');
        $size = input('size');
        $data = input('post.');
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$page,$size,$data);
        return json(['msg' => $list],$code);
    }
    
    public function lottery_log_read(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function lottery_goods_receiving(){
        $ids = input('ids');
        list($code,$msg) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$ids);
        return json(['msg' => $msg],$code);
    }


    public function lottery_goods_deliver(){
        $id = input('id');
        $logistics = input('logistics');
        $express_code = input('express_code');
        list($code,$msg) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id,$logistics,$express_code);
        return json(['msg' => $msg],$code);
    }
}