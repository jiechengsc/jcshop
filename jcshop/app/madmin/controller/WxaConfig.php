<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\LesseeService;
use app\madmin\service\ServiceFactory;
use app\utils\RedisUtil;
use think\App;
use think\Exception;
use think\facade\Cache;
class WxaConfig extends BaseAction
{

    private $ip;

    public function __construct(App $app)
    {
        parent::__construct($app);
        $lesseeService = new LesseeService();
        list($code, $entry) = $lesseeService->entry();
        if (empty($entry) || !in_array(1, $entry)) {
            throw new Exception("不支持该端操作", HTTP_NOTACCEPT);
        }

        $this->ip = getIp();
        if (Cache::store('redis')->has("WXACONFIG_IP")) {
            $wxaUsedIp = Cache::store('redis')->get("WXACONFIG_IP");
            if (request()->action() !== 'login' && $this->ip !== $wxaUsedIp) {
                throw new Exception("二维码失效，请重新扫码操作", HTTP_NOTACCEPT);
            }
        }
    }
    public function login()
    {
        if (Cache::store('redis')->has("WXACONFIG_FLAG")) {
            $redisUtil = new RedisUtil();
            $ttl = $redisUtil->ttl("WXACONFIG_FLAG");
            return json(["error_msg" => "队列中，预计等待{$ttl}秒"], HTTP_NOTACCEPT);
        }
        Cache::store('redis')->set("WXACONFIG_FLAG", 1, 120);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        Cache::store('redis')->set("WXACONFIG_IP", $this->ip, 1000);
        return json($msg, $code);
    }
    public function islogin()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json($msg, $code);
    }

    public function preview()
    {
        if (Cache::store('redis')->has("WXACONFIG_PREVIEW"))
            return json(["error_msg" => "服务器繁忙,请稍后再试"], HTTP_NOTACCEPT);
        Cache::store('redis')->set("WXACONFIG_PREVIEW", 1, 120);
        $plugins = input('get.plugins', '');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $plugins);
        Cache::store('redis')->delete("WXACONFIG_PREVIEW");

        if (isset($msg['error_msg']) && $msg['error_msg'] == "插件未授权使用") {
            throw new Exception("插件未授权使用", HTTP_NOTACCEPT);
        }

        return json($msg, $code);
    }
    public function sendFile()
    {
        if (Cache::store('redis')->has("WXACONFIG_SEND"))
            return json(["error_msg" => "服务器繁忙,请稍后再试"], HTTP_NOTACCEPT);
        Cache::store('redis')->set("WXACONFIG_SEND", 1, 120);
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__
            , $data);
        if ($code === HTTP_SUCCESS) {
            Cache::store('redis')->delete("WXACONFIG_FLAG");
        }
        Cache::store('redis')->delete("WXACONFIG_SEND");
        Cache::store('redis')->delete("WXACONFIG_IP");
        return json($msg, $code);
    }

}
