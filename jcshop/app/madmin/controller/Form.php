<?php


namespace app\madmin\controller;



use app\madmin\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class Form extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $is_bool = \app\utils\Addons::check($this->user['mall_id'],13);
        if (!$is_bool)
            throw new Exception('请先激活自定义表单插件',HTTP_NOTACCEPT);
    }

    public function create() : Json{
        $title = input('title','');
        $config = input('config','');
        $tries_limit = input('tries_limit','');
        $image = input('image');
        list($code,$data) = ServiceFactory::getResult('FormService','create',$title,$config,$tries_limit,$image);
        return json(['msg' => $data],$code);
    }

    public function update($id) : Json{
        $data = input('put.');
        list($code,$data) = ServiceFactory::getResult('FormService','update',$id,$data);
        return json(['msg' => $data],$code);
    }

    public function delete($id) :Json{
        list($code,$message) = ServiceFactory::getResult('FormService','delete',$id);
        return json(['msg' => $message],$code);
    }

    public function list() : Json{
        $page = input('page');
        $size = input('size');
        list($code,$data) = ServiceFactory::getResult('FormService','findAll',$page,$size);
        return json(['msg' => $data],$code);
    }
    
    public function read(int $id) : Json{
        list($code,$data) = ServiceFactory::getResult('FormService','read',$id);
        return json(['msg' => $data],$code);
    }

    public function answerList(int $id) : Json{
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult('FormService','answerList',$id,$page,$size);
        return json(['msg' => $list],$code);
    }

}