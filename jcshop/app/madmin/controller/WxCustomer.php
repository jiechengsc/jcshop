<?php
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\AddonsService;
use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;

/**
 * 客服控制器
 * @package app\madmin\controller
 */
class WxCustomer extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 22);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }


    /**
     * 获取客服列表
     * showdoc
     * @catalog 客服管理/客服
     * @title 获取列表
     * @description 获取列表的接口
     * @method post
     * @url /wxcompany/list?page=:page&size=:size
     * @header Authorization 必选 string 设备token
     * @param page 必选 string 页数
     * @param size 必选 string 行数
     * @param name 可选 string 昵称
     * @param mobile 可选 string 手机
     * @param company 可选 string 公司
     * @param audit_type 可选 int 是否免审核:0:不免,1:免审核
     * @param status 可选 string 状态
     * @param begin_date 可选 date 开始时间区间:2020-01-01
     * @param end_date 可选 date 结束时间区间:2020-01-02
     * @return {"msg":{"total":1,"per_page":10,"current_page":1,"last_page":1,"data":[{"id":1,"user_id":1,"name":"测试客服","mobile":"13511122233","company":"惊蛰","district":"浙江省 温州市 鹿城区","address":"xx路","longitude":116.397128,"latitude":39.916527,"remark":"备注","content":"详细介绍","business_license":"http:\/\/123","indate":0,"type":0,"status":1,"status_text":"正常","create_time":"2020-08-03 13:06:06","update_time":"2020-08-03 13:06:06"}]}}
     * @return_param msg json 对象信息,没有为空
     * @return_param total int 总记录数
     * @remark 这里是备注信息
     * @number 8
     */
    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    /**
     * 保存客服
     * showdoc
     * @catalog 客服管理/客服
     * @title 保存客服
     * @description 保存客服的接口
     * @method post
     * @url /merchant
     * @header Authorization 必选 string 设备token
     * @param user_id 必选 int 用户id
     * @param name 必选 string 客服名
     * @param mobile 必选 string 手机号
     * @param company 必选 string 公司
     * @param district 必选 string 所在区域
     * @param address 必选 string 地址
     * @param longitude 必选 double 经度
     * @param latitude 必选 double 纬度
     * @param business_license 必选 string 营业执照
     * @param type 必选 int 0:单门店,1:多门店
     * @param indate 必选 string 有效期时间戳,0,永久有效
     * @param remark 可选 string 备注
     * @param content 可选 string 详细介绍
     * @param is_commodity_limit 必选 int 是否限制商品发布数量:0:不限制,1:限制
     * @param commodity_limit 可选 int 限制商品发布数量
     * @param audit_type 可选 int 是否免审核:0:不免(默认),1:免审核
     * @param status 可选 int 状态
     * @param apply_id 可选 int 申请id
     * @param update_time 可选 datetime 更新时间
     * @return {"msg":{"model":{"user_id":1,"name":"测试客服_1","mobile":"13511122237","company":"惊蛰_1","district":"浙江省 温州市 鹿城区","address":"xx路","longitude":116.397128,"latitude":39.916527,"remark":"备注","content":"详细介绍","business_license":"http:\/\/123","indate":0,"type":0,"apply_id":0,"audit_type":0,"status":1,"create_time":"2020-12-25 15:11:04","update_time":"2020-12-25 15:11:04","id":"15"},"secret":{"username":"PDqEMeSl","password":"QOzOxqJVeYdz"}}}
     * @return_param msg json 返回信息
     * @return_param model json 新增对象信息
     * @return_param secret json 账号密码,该账号密码后续不显示,妥善保管
     * @number 9
     */
    public function save(Request $request)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $request->post());
        return json(['msg' => $msg], $code);
    }

    /**
     * 获取客服
     * showdoc
     * @catalog 客服管理/客服
     * @title 获取客服
     * @description 获取客服的接口
     * @method get
     * @url /merchant/:id
     * @header Authorization 必选 string 设备token
     * @param id 必选 int 客服id
     * @return {"msg":{"id":1,"user_id":1,"name":"测试客服","logo":null,"mobile":"13511122234","company":"惊蛰","district":"浙江省 温州市 鹿城区","address":"xx路","longitude":116.397128,"latitude":39.916527,"remark":"备注","content":"详细介绍","business_license":"http:\/\/123","indate":0,"type":0,"is_commodity_limit":0,"commodity_limit":0,"apply_id":0,"audit_type":0,"status":1,"create_time":"2020-08-10 11:37:38","update_time":"2020-08-15 14:23:27","delete_time":null,"user_name":"aaa","cash":{"total":"10000.00","recharge":"8897.78","withdraw":"1102.22"},"status_text":"正常"}}
     * @return_param msg string 对象信息,报错则为错误信息
     * @number 10
     */
    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 更新客服
     * showdoc
     * @catalog 客服管理/客服
     * @title 更新客服
     * @description 更新客服的接口
     * @method put
     * @url /merchant/:id
     * @header Authorization 必选 string 设备token
     * @param id 必选 int 客服id
     * @param update_time 必选 datetime 更新时间
     * @param user_id 可选 int 用户id
     * @param name 可选 string 客服名
     * @param mobile 可选 string 手机号
     * @param company 可选 string 公司
     * @param district 可选 string 所在区域
     * @param address 可选 string 地址
     * @param longitude 可选 double 经度
     * @param latitude 可选 double 纬度
     * @param business_license 可选 string 营业执照
     * @param type 可选 int 0:单门店,1:多门店
     * @param indate 可选 string 有效期时间戳,0,永久有效
     * @param remark 可选 string 备注
     * @param content 可选 string 详细介绍
     * @param audit_type 可选 int 是否免审核:0:不免,1:免审核
     * @param status 可选 int 状态
     * @return {"msg":"影响了1条数据"}
     * @return_param msg string 对象信息,报错则为错误信息
     * @number 11
     */
    public function update(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $this->request->put());
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "成功"], $code);
    }

    /**
     * 删除客服
     * showdoc
     * @catalog 客服管理/客服
     * @title 删除客服
     * @description 删除客服的接口
     * @method delete
     * @url /merchant/:id
     * @header Authorization 必选 string 设备token
     * @param :id 必选 int 客服id
     * @return null
     * @return_param msg string 对象信息,报错则为错误信息
     * @number 12
     */
    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code($code);
    }
}
