<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\Request;
use think\response\Json;

class Agent extends BaseAction
{

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function save(Request $request)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $request->post());
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function update(int $id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $this->request->put());
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code($code);
    }

    public function orderList() : Json{
        $order_no = input('post.order_no','');
        $status = input('status',-1);
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult('AgentService','order',$page,$size,$order_no,$status);
        return json(['msg' => $list],$code);
    }

    public function AgentLevel()
    {
        $user_id = input('user_id');
        $level = input('level');
        $page = input('page');
        $size = input('size');
        list($code, $list) = ServiceFactory::getResult('AgentService', 'LevelList', $user_id, $level, $page, $size);
        return json(['msg' => $list], $code);
    }

    public function rank()
    {
        list($code, $list) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $list], $code);
    }
}
