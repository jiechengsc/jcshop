<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use think\App;
use think\Exception;

class Integral extends BaseAction
{

    public function list(){
        $page = input('page');
        $size = input('size');

        $data = input('post.',[]);
        list($code,$result) = ServiceFactory::getResult('IntegralService','list',$data,$page,$size);

        return json(['msg' => $result],$code);
    }

    public function config_update(){
        $is_bool = \app\utils\Addons::check($this->user['mall_id'],15) || \app\utils\Addons::check($this->user['mall_id'],7);
        if (!$is_bool)
            throw new Exception('请先激活积分相关插件',HTTP_NOTACCEPT);
        $is_open = input('is_open');
        if(input('deduction')<0.01){
            throw new Exception('最低抵扣不能低于0.01',HTTP_NOTACCEPT);
        }
        $deduction = floatval(1/input('deduction'))/100;


        $get_integral_ratio = input('get_integral_ratio');
        $get_integral_type = input('get_integral_type');
        list($code,$result) = ServiceFactory::getResult('IntegralService','configUpdate',$is_open,$deduction,$get_integral_ratio,$get_integral_type);
        return json(['msg' => $result],$code);
    }


    public function config_find(){

        $is_bool = \app\utils\Addons::check($this->user['mall_id'],15) || \app\utils\Addons::check($this->user['mall_id'],7);
        if (!$is_bool)
            throw new Exception('请先激活积分相关插件',HTTP_NOTACCEPT);
        list($code,$result) = ServiceFactory::getResult('IntegralService','configFind');


        $a=floatval(1/$result['deduction'])*100/10000;

        $result['deduction']=number_format($a,2);
        return json(['msg' => $result],$code);
    }


    public function save_share(){
        $is_bool = \app\utils\Addons::check($this->user['mall_id'],6);
        if (!$is_bool)
            throw new Exception('请先激活积分规划插件',HTTP_NOTACCEPT);
        $old = input('old',0);
        $new = input('new',0);
        $daily_limit = input('daily_limit',0);
        list($code,$result) = ServiceFactory::getResult('IntegralService','saveShare',$new,$old,$daily_limit);
        return json(['msg' => $result],$code);
    }


    public function find_share(){
        $is_bool = \app\utils\Addons::check($this->user['mall_id'],6);
        if (!$is_bool)
            throw new Exception('请先激活积分规划插件',HTTP_NOTACCEPT);
        list($code,$result) = ServiceFactory::getResult('IntegralService','findShare');
        return json(['msg' => $result],$code);
    }
}