<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;

class IntegralShop extends BaseAction
{
    protected $className = 'IntegralShopSerivce';

    public function classifylist(){
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__);
        return json(['msg' => $list],$code);
    }

    public function classifyFind(int $id){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function classifyCreate(){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,input('post.'));
        return json(['msg' => $data],$code);
    }

    public function classifyUpdate(int $id){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id,input('put.'));
        return json(['msg' => $data],$code);
    }

    public function classifyDelete(int $id){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }
    
    public function goodsList()
    {
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $list],$code);
    }

    public function goodsFind(int $id)
    {
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function goodsCreate(){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,input('post.'));
        return json(['msg' => $data],$code);
    }

    public function goodsUpdate(int $id){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id,input('put.'));
        return json(['msg' => $data],$code);
    }

    public function goodsDelete(int $id){
        $is_bool = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $is_bool ? '成功' : '失败'],$is_bool ? HTTP_SUCCESS : HTTP_NOTACCEPT);
    }

    public function orderList(){
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $list],$code);
    }

    public function orderFind(int $id){
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $list],$code);
    }

    public function orderRefund(int $id){
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $list],$code);
    }

    public function orderDeliver(int $id){
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$id,input('put.'));
        return json(['msg' => $list],$code);
    }

    public function orderReceiving(int $id){
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $list],$code);
    }

    public function orderDelete(int $id){
        $is_bool = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $is_bool ? '成功' : '失败'],$is_bool ? HTTP_SUCCESS : HTTP_NOTACCEPT);
    }

    public function orderDeliverUpdate(int $id){
        $data = input('put.');
        ServiceFactory::getResult($this->className,__FUNCTION__,$id,$data);
        return json(['msg' => '成功'],HTTP_SUCCESS);
    }
    public function skuList()
    {
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult($this->className,__FUNCTION__,$page,$size);
        return json(['msg' => $list],$code);
    }

    public function skuFind(int $id){
        list($code,$data) = ServiceFactory::getResult($this->className,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }
}