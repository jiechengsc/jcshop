<?php


namespace app\madmin\controller;


use app\madmin\model\CommissionAssessment;
use app\madmin\model\Link;
use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use app\utils\Wechat;
use app\utils\Weixin;
use think\App;

class UrlLink extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $time = date("Y-m-d H:i:s");
        #自动过期
        $over_where = [
            ['expired_time', '<', $time],
            ['status', 'in', [0]]
        ];
        Link::update(['status' => 1], $over_where);
        if(isset($this->user->mall_id)){
            $check = Addons::check($this->user->mall_id, 32);
            if (!$check)
                throw new \Exception("请先激活小程序外部跳转链接插件", HTTP_NOTACCEPT);
        }

    }

    //列表
    public function list()
    {

        $page = input('page', 1);
        $size = input('size', 10);

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }

    //添加
    public function add()
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    //详情
    public function detail($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    /**
     * 删除
     * @param $id
     * @return \think\response\Json
     */
    public function delete($id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }
}