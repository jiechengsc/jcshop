<?php
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;


class MembershipRecord extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }


    public function index(int $page,int $size)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,$page,$size);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}
