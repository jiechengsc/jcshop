<?php
namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\App;
use app\utils\Addons;
use think\Exception;

class Presell extends BaseAction
{
    /**
     * Group constructor.
     * @param App $app
     * @throws Exception
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 34);
        if (!$check)
            throw new Exception("请先激活预售", HTTP_NOTACCEPT);
    }

    public function index()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        $search = $this->request->get('search') ?? [];
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size, $search);
        return json(['msg' => $data], $code);
    }


    public function save()
    {
        $data = input('post.');
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $data], $code);
    }


    public function read(int $id)
    {
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $data], $code);
    }

    public function update($id)
    {
        $data = input('put.');
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id, $data);
        return json(['msg' => $data], $code);
    }

    public function stop($id)
    {
        list($code, $data) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $data], $code);
    }

    public function delete($id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }

}