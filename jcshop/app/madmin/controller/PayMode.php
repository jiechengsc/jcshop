<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use think\exception\InvalidArgumentException;
use think\facade\Filesystem;
use think\Request;


class PayMode extends BaseAction
{


    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }



    public function save(Request $request)
    {

        $data = $request->post();

        $files = $request->file();
        if ($data['mode'] == 1) {

            if (!isset($files['cert'], $files['wxkey']))
                return json(['msg' => '配置文件必须上传'], HTTP_NOTACCEPT);


            $fileList['cert'] = $files['cert'];
            $fileList['wxkey'] = $files['wxkey'];

            if (isset($files['rootca'])) $fileList['rootca'] = $files['rootca'];

            try {
                validate([
                    'cert' => 'filesize:102400|fileExt:pem',
                    'wxkey' => 'filesize:102400|fileExt:pem',

                    'rootca' => 'filesize:102400|fileExt:pem'
                ])->check($fileList);
            } catch (\Exception $e) {
                return json(['msg' => $e->getMessage()], HTTP_NOTACCEPT);
            }

            $savename = [];
            foreach ($fileList as $k => $file) {
                $savename[$k] = "storage/" . Filesystem::disk('local')->putFile(md5('wxcert'), $file, 'sha1');
            }

            if ($data['type'] == 1) {
                if (!isset($data['configuration'],
                    $data['configuration']['AppId'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 2) {
                if (!isset($data['configuration'],
                    $data['configuration']['M_AppId'],
                    $data['configuration']['M_MchId'],
                    $data['configuration']['AppId'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 3) {
                if (!isset($data['configuration'],
                    $data['configuration']['AppId'],
                    $data['configuration']['AppSecret'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 4) {
                if (!isset($data['configuration'],
                    $data['configuration']['M_AppId'],
                    $data['configuration']['M_MchId'],
                    $data['configuration']['AppId'],
                    $data['configuration']['AppSecret'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            }

            $data = array_merge($data, $savename);

        } elseif ($data['mode'] == 2) {
            if (!isset($data['configuration'],
                $data['configuration']['AppId'],
                $data['configuration']['check'],
                $data['configuration']['public'],
                $data['configuration']['private']))
                return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            $data['M_cert'] = $files['M_cert'];
            $data['M_wxkey'] = $files['M_wxkey'];
        }

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);

    }


    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function update(int $id)
    {

        $data = request()->post();

        list($code, $model) = ServiceFactory::getResult(class_basename($this) . 'Service',
            'read', $id);
        if (HTTP_SUCCESS != $code)
            throw new InvalidArgumentException("请求id有误",HTTP_NOTACCEPT);

        if ($data['mode'] == 1 || $data['mode']==3) {

            if ($data['type'] == 1) {
                if (!isset($data['configuration'],
                    $data['configuration']['AppId'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 2) {
                if (!isset($data['configuration'],
                    $data['configuration']['M_AppId'],
                    $data['configuration']['M_MchId'],
                    $data['configuration']['AppId'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 3) {
                if (!isset($data['configuration'],
                    $data['configuration']['AppId'],
                    $data['configuration']['AppSecret'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            } elseif ($data['type'] == 4) {
                if (!isset($data['configuration'],
                    $data['configuration']['M_AppId'],
                    $data['configuration']['M_MchId'],
                    $data['configuration']['AppId'],
                    $data['configuration']['AppSecret'],
                    $data['configuration']['MchId'],
                    $data['configuration']['APIKEY']))
                    return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
            }

            $files = request()->file();
            if ($model->mode != 1 && $model->mode != 3){
                if (!isset($files['cert'], $files['wxkey']))
                    return json(['msg' => '配置文件必须上传'], HTTP_NOTACCEPT);

            }

            if (!empty($files) && sizeof($files) > 0){
                try {
                    validate([
                        'cert' => 'filesize:102400|fileExt:pem',
                        'wxkey' => 'filesize:102400|fileExt:pem',

                        'rootca' => 'filesize:102400|fileExt:pem'
                    ])->check($files);
                } catch (\Exception $e) {
                    return json(['msg' => $e->getMessage()], HTTP_NOTACCEPT);
                }
                $savename = [];
                foreach ($files as $k => $file) {
                    $savename[$k] = "storage/" . Filesystem::disk('local')->putFile(md5('wxcert'), $file, 'sha1');
                }

                $data = array_merge($data, $savename);
            }


        } elseif ($data['mode'] == 2) {
            if (!isset($data['configuration'],
                $data['configuration']['AppId'],
                $data['configuration']['check'],
                $data['configuration']['public'],
                $data['configuration']['private']))
                return json(['msg' => '请完善配置信息'], HTTP_NOTACCEPT);
                $data['cert'] = null;
                $data['wxkey'] = null;
                $data['rootca'] = null;
        }

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code($code);
    }

}
