<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use Psr\SimpleCache\InvalidArgumentException;
use think\App;
use think\facade\Cache;

class Pick extends BaseAction
{

    public function getConfig()
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, json_decode($this->user, true));
        return json(['msg' => $msg], $code);
    }


    public function save()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data, json_decode($this->user, true));
        return json(['msg' => $msg], $code);
    }


    public function getGroundingGoods()
    {
        $data['page'] = input('page', 1);
        $data['size'] = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }
}