<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\madmin\controller;

use app\madmin\service\ServiceFactory;
use app\Oss;
use Psr\SimpleCache\InvalidArgumentException;
use think\facade\Cache;

class Page extends BaseAction
{

    public function pageList()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

    public function createPage()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function delPage(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }


    public function preview()
    {
        $get = input('get.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $msg], $code);
    }


    public function renovation()
    {
        $get = input('get.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $msg], $code);
    }


    public function upRenovation()
    {
        $put = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $put);
        return json(['msg' => $msg], $code);
    }

    public function myTemplate()
    {
        $get = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $msg], $code);
    }


    public function market()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

    public function getTemplateInfo()
    {
        $id = input('get.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    public function saveTemplate()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function myTemplateList()
    {
        $get = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $msg], $code);
    }


    public function getMyTemplateInfo()
    {
        $id = input('get.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function delMytemplate(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }


    public function base64Up()
    {
        $post = input('post.images');
        list($code, $msg) = ServiceFactory::getResult('UploadService', "upBase64Img", $post);
        return json(['msg' => $msg], $code);
    }

    public function createPublicTemplate()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function enable()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function saveSource()
    {
        $data = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function saveMyTemplate()
    {
        $data = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function startAdvert()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }

    public function advert()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function defaultTemplate()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function classList()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function text()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function setDefaultTemplate()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}