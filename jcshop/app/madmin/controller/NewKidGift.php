<?php


namespace app\madmin\controller;


use app\madmin\service\ServiceFactory;
use think\App;
use think\Exception;
class NewKidGift extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        global $user;
        $is_bool = \app\utils\Addons::check($user['mall_id'],1);
        if (!$is_bool)
            throw new Exception('请先激活新人送礼插件',HTTP_NOTACCEPT);
    }


    public function create(){
        $data = input('post.');
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','create',$data);
        return json(['msg' => $data],$code);
    }


    public function update(int $id){
        $data = input('put.');
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','update',$id,$data);
        return json(['msg' => $data],$code);
    }


    public function delete(int $id){
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','delete',$id);
        return json(['msg' => $data],$code);
    }


    public function list(int $page, int $size){
        list($code,$list) = ServiceFactory::getResult('NewKidGiftService','list',$page,$size);
        return json(['msg' => $list],$code);
    }

    public function read(int $id){
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','read',$id);
        return json(['msg' => $data],$code);
    }


    public function receive_list(){
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult('NewKidGiftService','receive_list',$page,$size);
        return json(['msg' => $list],$code);
    }


    public function receive_read(int $id){
        list($code,$data) = ServiceFactory::getResult('NewKidGiftService','receive_read',$id);
        return json(['msg' => $data],$code);
    }
}