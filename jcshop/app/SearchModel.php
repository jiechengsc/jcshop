<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app;


use think\db\Query;

/**
 * 模糊搜索模型
 * @package app
 */
class SearchModel extends \think\Model
{

    public function searchTradeAttr(Query $query,$value,$data)
    {
        $query->where('trade','like',"%".$value."%");
    }

    public function searchPayNoAttr(Query $query,$value,$data)
    {
        $query->where('pay_no','like',"%".$value."%");
    }

    public function searchUnifiedOrderNoAttr(Query $query,$value,$data)
    {
        $query->where('unified_order_no','like',"%".$value."%");
    }

    public function searchSerialNumAttr(Query $query,$value,$data)
    {
        $query->where('serial_num','like',"%".$value."%");
    }

    public function searchNicknameAttr(Query $query, $value, $data)
    {
        $query->where('nickname','like',"%".$value."%");
    }

    public function searchCompanyAttr(Query $query,$value,$data)
    {
        $query->where('company','like',"%".$value."%");
    }

    public function searchAgentNameAttr(Query $query,$value,$data)
    {
        $query->where('agent_name','like',"%".$value."%");
    }

    public function searchTitleAttr(Query $query, $value, $data)
    {
        $query->where('title', 'like', "%" . $value . "%");
    }

    /**
     * 姓名模糊查询
     * @param Query $query
     * @param $value
     * @param $data
     */
    public function searchNameAttr(Query $query, $value, $data)
    {
        $query->where('name', 'like', "%" . $value . "%");
    }

    /**
     * 手机模糊查询
     * @param Query $query
     * @param $value
     * @param $data
     */
    public function searchMobileAttr(Query $query, $value, $data)
    {
        $query->where('mobile', 'like', "%" . $value . "%");
    }

    /**
     * 创建时间模糊查询
     * @param Query $query
     * @param $value
     * @param $data
     */
    public function searchCreateTimeAttr(Query $query, $value, $data)
    {
        $query->whereBetweenTime('create_time', $value[0], $value[1]);
    }

}