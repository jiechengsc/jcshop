<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class ShopConfigurationValidate extends Validate
{
    protected $rule = [
        'type' => 'require|max:32',
        'configuration' => 'require',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'type.require' => '类型必须上传',
        'configuration.require' => '配置信息未上传',
        'update_time.require' => '更新时间不正确',
        'update_time.dateFormat' => '更新时间必须是日期格式'
    ];
    protected $scene = [
        'save' => ['type','configuration'],
        'update_time' => ['update_time']
    ];
}