<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class RoleValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'require|max:32',
        'remark' => 'max:64',
        'department_id' => 'integer',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s',
        'rule' => 'require'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'name.require' => '角色名未上传',
        'name.max' => '角色名称超过最大限定32个字符',
        'remark' => '说明必须限定在64字符之内',
        'department_id' => '部门错误',
        'update_time.require' => '更新时间必须上传',
        'update_time.dateFormat' => '更新时间不正确',
        'rule.require' => '规则必须上传'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['name','remark','department_id'],
        'update' => ['update_time','department_id','remark','status','rule']
    ];
}