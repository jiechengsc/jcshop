<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\shop_admin\validate;

use think\Validate;

class Operation extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
	protected $rule = [
        'username'      => 'require|length:4,32',
        'name'          => 'require|length:2,16|chs',
        'password'      => 'require|length:4,32',
        'mobile'        => 'require|mobile',
        'update_time'   => 'dateFormat:Y-m-d H:i:s',
        'begin_date'    => 'dateFormat:Y-m-d',
        'end_date'      => 'dateFormat:Y-m-d'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'username.require'          => '账号必须填写',
        'username.length'           => '账号只能有4-32个字符组成',
        'username.alphaDash'        => '账号只能使用字母和数字，下划线_及破折号-',
        'name.require'              => '姓名必须填写',
        'name.length'               => '账号只能有2-16个字符组成',
        'name.alphaDash'            => '账号只能使用汉字',
        'password.require'          => '密码必须填写',
        'password.length'           => '密码只能有4-32个字符组成',
        'password.alphaDash'        => '密码只能使用字母和数字，下划线_及破折号-',
        'mobile.requrie'            => '手机号必填',
        'mobile.mobile'             => '请输入正确的手机号',
        'update_time.dateFormat'    => '请传入正确的时间戳',
        'begin_date.dateFormat'     => '请传入正确的时间戳',
        'end_date.dateFormat'       => '请传入正确的时间戳'
    ];

    protected $scene = [
        'login'   => ['username','password'],
        'save'    => ['username','password','mobile'],
    ];

    public function sceneUpdate()
    {
        return $this->only(['username', 'password', 'mobile', 'update_time', 'begin_date', 'end_date'])
            ->remove('password','require');
    }
}
