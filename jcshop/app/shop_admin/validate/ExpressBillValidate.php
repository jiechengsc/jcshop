<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class ExpressBillValidate extends Validate
{
    protected $rule = [
        'order_id' => 'require|array',
        'order_commodity_id' => 'require|array',
        'template_id' => 'require|integer'
    ];
    protected $message = [
        'order_id.require' => '缺少订单参数',
        'order_id.array' => '订单参数错误',
        'order_commodity_id.require' => '缺少订单商品参数',
        'order_commodity_id.array' => '订单商品参数格式错误',
        'template_id.require' => '模板未选择',
        'template_id.integral' => '模板格式错误'
    ];
    protected $scene = [
        'create' => ['order_commodity_id','template_id']
    ];
}