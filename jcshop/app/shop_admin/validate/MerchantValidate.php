<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class MerchantValidate extends Validate
{
    protected $rule = [
        'mobile' => 'require',
        'business_license' => 'require',
        'address' => 'require',
        'logo' => 'require',
        'images' => 'require|array'
    ];
    protected $message = [
        'mobile' => '手机号码不能为空',
        'business_license' => '营业执照不能为空',
        'address' => '地址不能为空',
        'logo' => 'logo不能为空',
        'images' => '客服数据上传错误'
    ];
    protected $scene = [
        'update' => ['mobile','business_license','address','logo','content'],
        'saveCustomerService' => ['images']
    ];
}