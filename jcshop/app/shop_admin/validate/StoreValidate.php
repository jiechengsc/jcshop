<?php


namespace app\shop_admin\validate;


use think\Validate;

class StoreValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'require|max:32|chsAlphaNum',
        'mobile' => 'require|mobile',
        'district' => 'require|max:64',
        'address' => 'require',
        'longitude' => 'require',
        'latitude' => 'require'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'name.require' => '门店名必须传',
        'name.max' => '门店名限定在32个字符内',
        'name.chsAlphaNum' => '门店名不的出现特殊字符',
        'mobile.require' => '门店电话必须填写',
        'mobile.mobile' => '门店电话格式错误',
        'district.require' => '区域必须填写',
        'district.max' => '区域限定再64个字符内',
        'address' => '地址必须填写',
        'longitude.require' => '经度必须填写',
//        'longitude.float' => '经度错误',
        'latitude.require' => '纬度必须填写',
//        'latitude.float' => '纬度错误'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['name','mobile','district','address','longitude','latitude']
    ];
}