<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class StatisticsValidate extends Validate
{
    protected $rule = [
//        'field' => 'require|in:avg_money,total_number,total_money,avg_user_money,number,new_user_number',
        // 'key' => 'require|in:turnover,consumer,money,refund_number,refund_money',
//        'date_format'   => 'in:day,month,year',
//        'begin_date'    => 'require|dateFormat:Y-m-d H:i:s',
//        'end_date'      => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
//        'field.require'            => '显示字段必选',
//        'field.in'                 => '显示字段不在规范内',
        // 'key.require'              => '显示字段必选',
        // 'key.in'                   => '显示字段不在规范内',
//        'date_format'              => '日期格式错误',
//        'begin_date.require'       => '开始时间必填',
//        'end_date.require'         => '结束时间必填',
//        'begin_date.dateFormat'    => '请传入正确的时间戳',
//        'end_date.dateFormat'      => '请传入正确的时间戳'
    ];
    protected $scene = [
//        'total' => ['begin_date','end_date'],
//        'count' => ['begin_date','end_date','date_format'],
//        'transaction_total' => ['begin_date','end_date'],
        // 'transaction_chart' => ['key']
    ];
}