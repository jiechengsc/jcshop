<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\validate;


use think\Validate;

class CommodityValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'name' => 'require|max:32',
        'type' => 'require|integer',
        'subtitle' => 'require|max:32',
        'master' => 'require',
        'slideshow' => 'require',
        'classify_id' => 'require',
        'classify_value' => 'require',
        'ft_type' => 'require|integer',
        'total' => 'integer',
        'sell_price' => 'require|float',
        'is_original_price' => 'require|integer',
        'content' => 'require',
        'has_parameter' => 'require|integer',
        'is_virtual' => 'require|integer',
        'is_distribution' => 'require|integer',
        'is_base_distribution' => 'require|integer',
        'has_sku' => 'require|integer',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'name.require' => '商品名必须填写',
        'name.max' => '商品名称必须限定再32个字符内',
        'type.require' => '商品类型必须选择',
        'type.integer' => '商品类型格式错误',
        'subtitle.require' => '副标题必须填写',
        'subtitle.max' => '副标题限制在32个字符内',
        'master.require' => '主图必须上传',
        'slideshow.require' => '轮播图必须上传',
        'classify_id.require' => '分类必须选择',
        'classify_value.require' => '分类值必须填写',
        'ft_type.require' => '运费类型必须填写',
        'ft_type.integer' => '运费类型错误',
        'total.require' => '总量必须填写',
        'total.integer' => '总量必须是数值',
        'sell_price.require' => '售价必须填写',
        'sell_price.float' => '售价必须是数字',
        'is_original_price.require' => '是否开启原价',
        'is_original_price.integer' => '是否开启原价错误',
        'content.require' => '商品详情必须填写',
        'has_parameter.require' => '是否拥有参数',
        'has_parameter.integer' => '是否拥有参数错误',
        'is_virtual.require' => '是否开启虚拟销量',
        'is_virtual.integer' => '是否开启虚拟销量格式错误',
        'is_distribution.require' => '是否参与分销',
        'is_distribution.integer' => '是否参与分销格式错误',
        'is_base_distribution.require' => '启用全局配置',
        'is_base_distribution.integer' => '启用全局配置错误',
        'has_sku.require' => '是否sku对象',
        'has_sku.integer' => '是否sku对象错误',
        'update_time' => '更新时间错误'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['name','type','subtitle','master','slideshow','classify_id','classify_value','ft_type','total','sell_price','is_original_price','content','has_parameter','is_virtual','is_distribution','is_base_distribution','has_sku'],
        'update' => ['ft_type','update_time']
    ];
}