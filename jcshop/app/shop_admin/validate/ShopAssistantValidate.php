<?php


namespace app\shop_admin\validate;


use think\Validate;

class ShopAssistantValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'user_id' => 'require|integer',
        'store_id' => 'require|integer',
        'serial_num' => 'require|max:16',
        'update_time' => 'require|dateFormat:Y-m-d H:i:s'
    ];
    protected $message = [
        'page' => '页数必须上传',
        'size' => '显示数量必须上传',
        'user_id.require' => '用户未选择',
        'user_id.integer' => '用户选择错误',
        'store_id.require' => '门店未选择',
        'store_id.integer' => '门店选择错误',
        'serial_num.require' => '店员编号必须上传',
        'serial_num.alphaNum' => '店员编号不能包含特殊字符',
        'serial_num.max' => '店员编号不能超过16个字符',
        'update_time' => '更新时间不正确'
    ];
    protected $scene = [
        'list' => ['page','size'],
        'save' => ['user_id','store_id','serial_num'],
        'update' => ['user_id','store_id','serial_num','update_time'],
    ];
}