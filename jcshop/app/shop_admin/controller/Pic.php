<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;
use app\shop_admin\service\ServiceFactory;
use Psr\SimpleCache\InvalidArgumentException;

class Pic extends BaseAction
{
    public function picList()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data, $data['page'], $data['size']);
        return json(['msg' => $msg], $code);
    }
    public function addPic()
    {
        $file = request()->file();
        $group_id = input('post.pic_group_id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $file, $group_id);
        return json(['msg' => $msg], $code);
    }
    public function groupList()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
    public function addGroup()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }
    public function delGroup(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }
    public function editGroup()
    {
        $data = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }
    public function delPic(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return response()->code($code);
    }
    
    public function ossConfig()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function getUrl()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}