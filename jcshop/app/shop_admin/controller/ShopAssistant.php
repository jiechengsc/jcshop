<?php
declare (strict_types = 1);

namespace app\shop_admin\controller;

use app\shop_admin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;

class ShopAssistant extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 11);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }

    
    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function save(Request $request)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $request->post());
        return json(['msg' => $msg], $code);
    }

    
    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }


    
    public function update(int $id)
    {

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $this->request->put());
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    
    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code($code);
    }
}
