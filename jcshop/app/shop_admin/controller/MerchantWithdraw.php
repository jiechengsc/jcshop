<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class MerchantWithdraw extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
    }
    public function total()
    {
        $amount = input('put.amount');
        $remark = input('put.remark');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $amount, $remark);
        return json(['msg' => $msg], $code);
    }
    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }
    public function save()
    {
        $data = input('post.');
        $remark = input('post.remark');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data, $remark);
        return json(['msg' => $msg], $code);
    }

}