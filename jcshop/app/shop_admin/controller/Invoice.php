<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;

class Invoice extends BaseAction
{

    public function create(){
        global $user;
        $name = input('name');
        $config = input('config');
        $title = input('title');
        $express_id = input('express_id');
        $logo = input('logo');
        list($code,$result) = ServiceFactory::getResult('InvoiceService','create',$config,$title,$name,$express_id,$logo,$user->merchant_id);
        return json(['msg' => $result],$code);
    }


    public function update(){
        $id = input('id');
        $name = input('name');
        $config = input('config');
        $title = input('title');
        $express_id = input('express_id');
        $logo = input('logo');
        list($code,$result) = ServiceFactory::getResult('InvoiceService','update',$id,$config,$title,$name,$express_id,$logo);
        return json(['msg' => $result],$code);
    }

    public function delete(){
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('InvoiceService','delete',$id);
        return json(['msg' => $result],$code);
    }


    public function list(){
        $page = input('page');
        $size = input('size');
        global $user;
        list($code,$result) = ServiceFactory::getResult('InvoiceService','list',$page,$size,$user->merchant_id);
        return json(['msg' => $result],$code);
    }


    public function read(){
        $id = input('id');
        list($code,$result) = ServiceFactory::getResult('InvoiceService','read',$id);
        return json(['msg' => $result],$code);
    }


    public function print(){
        $order_id = input('order_id',[]);
        $order_commodity_id = input('order_commodity_id',[]);
        $template_id = input('template_id');
        list($code,$result) = ServiceFactory::getResult('InvoiceService','print',$template_id,$order_id,$order_commodity_id);
        return json(['msg' => $result],$code);
    }
}