<?php
declare (strict_types=1);

namespace app\shop_admin\controller;

use app\shop_admin\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;


class AcceptRecord extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->user->mall_id, 11);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

}
