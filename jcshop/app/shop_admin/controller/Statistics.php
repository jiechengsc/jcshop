<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;


use app\shop_admin\service\ServiceFactory;
use think\response\Json;

class Statistics extends BaseAction
{
    protected $data;
    public function initialize(){
        global $user;
        $this->data = $user;
    }


    public function index() : Json{
        list($code,$select) = ServiceFactory::getResult('StatisticsService','IndexData');
        return json(['msg' => $select],$code);
    }


    public function total(){
        $begin_date = input('begin_date');
        $end_date = input('end_date');
        list($code,$result) = ServiceFactory::getResult('StatisticsService','CountData',$begin_date,$end_date);
        return json(['msg' => $result],$code);
    }


    public function count(){
        $begin_date = input('begin_date',null);
        $end_date = input('end_date',null);
        $date_format = input('date_format','day');
        $field = input('field');
        list($code,$result) = ServiceFactory::getResult('StatisticsService','StatisticsData',$field,$begin_date,$end_date,$date_format);
        return json(['msg' => $result],$code);
    }


    public function consumption_ranking(){
        list($code,$result) = ServiceFactory::getResult('StatisticsService','ConsumptionRanking');
        return json(['msg' => $result],$code);
    }

    public function sales_ranking(){
        list($code,$result) = ServiceFactory::getResult('StatisticsService','SalesRanking');
        return json(['msg' => $result],$code);
    }

    public function goods_ranking(){
        $page = input('page',1);
        $size = input('size',10);
        list($code,$result) = ServiceFactory::getResult('StatisticsService','GoodsRanking',$page,$size);
        return json(['msg' => $result],$code);
    }


    public function user_ranking(){
        $page = input('page',1);
        $size = input('size',10);
        $nickname = input('nickname','');
        list($code,$result) = ServiceFactory::getResult('StatisticsService','UserRanking',$page,$size,$nickname);
        return json(['msg' => $result],$code);
    }

    public function unified_count(){
        list($code,$result) = ServiceFactory::getResult('StatisticsService','unified_count');
        return json(['msg' => $result],$code);
    }

    public function user_total(int $user_id){
        list($code,$result) = ServiceFactory::getResult('StatisticsService','userTotal',$user_id);
        return json(['msg' => $result],$code);
    }



    public function exportUser()
    {
        list($code,$result) = ServiceFactory::getResult('StatisticsService',__FUNCTION__);
        return json(['msg' => $result],$code);
    }


    public function exportCommodity()
    {
        list($code,$result) = ServiceFactory::getResult('StatisticsService',__FUNCTION__);
        return json(['msg' => $result],$code);
    }
}