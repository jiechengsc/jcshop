<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\controller;

use app\shop_admin\service\ServiceFactory;
use think\App;
class Recommend extends BaseAction
{
    private $is_bool = true;
    public function __construct(App $app)
    {
        global $user;
        parent::__construct($app);
//        $this->is_bool = \app\utils\Addons::check($user->mall_id,10);
    }
    public function list(){
        list($code,$list) = ServiceFactory::getResult('RecommendService','list');
        return json(['msg' => $list],$code);
    }
    public function read(int $id){
        list($code,$data) = ServiceFactory::getResult('RecommendService','read',$id);
        return json(['msg' => $data],$code);
    }
    public function save(){
        list($code,$data) = ServiceFactory::getResult('RecommendService','save');
        return json(['msg' => $data],$code);
    }
    public function update(int $id){
        $data = input('put.');
        list($code,$data) = ServiceFactory::getResult('RecommendService','update',$id,$data);
        return json(['msg' => $data],$code);
    }
    public function delete(int $id){

    }
}