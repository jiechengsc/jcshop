<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\model;

use think\Model;
use think\model\concern\SoftDelete;

/**
 * 优惠券
 * @package app\madmin\model
 */
class UserCoupon extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未使用',
            1 => '已使用',
            2 => '过期'
        ];
        return $status[$data['status']];
    }

    public function coupon()
    {
        return $this->hasOne(Coupon::class,'id','coupon_id')->hidden(['delete_time']);
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id')->hidden(['agent_id','merchant_id','delete_time']);
    }

}
