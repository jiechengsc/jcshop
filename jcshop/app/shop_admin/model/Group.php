<?php
namespace app\shop_admin\model;

use app\SearchModel;
use think\Model;

class Group extends SearchModel
{
    public function groupCommodity()
    {
        return $this->hasMany(Commodity::class, 'commodity_id', 'id');
    }
}