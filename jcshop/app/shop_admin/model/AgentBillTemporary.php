<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;

use app\index\model\OrderAgentProfit;
use think\Model;

class AgentBillTemporary extends Model
{
    /**
     * 根据订单删除分销代理费数据
     * @param int $order_id
     */
    public static function orderDel(int $order_id) : void{
        self::where('order_id',$order_id)
            ->delete();
    }

    /**
     * 根据订单商品删除分销代理费数据
     * @param array $order_commodity_id
     */
    public static function orderCommodityDel(array $order_commodity_id) : void{
        self::where('order_commodity_id','IN',$order_commodity_id)
            ->update(['status' => 3,'update_time' => date('Y-m-d H:i:s')]);
    }
}