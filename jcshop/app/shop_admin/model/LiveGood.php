<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use app\utils\LiveErrorCode;
use app\utils\LiveGoods;
use think\facade\Cache;
use think\Model;

class LiveGood extends Model
{
    /**
     * 变更状态
     */
    public static function changeStatus(){

        $where=[
            'status'=>0
        ];
        $count=LiveGood::where($where)->count();
        for($i=0;$i<$count;$i=$i+20){
            $live_goods=LiveGood::where($where)->limit($i,20)->select()->toArray();
            $goods_ids=array_values(array_column($live_goods,"room_goods_id"));
            $data=LiveGoods::getGoodsWarehouse(['goods_ids'=>$goods_ids]);
            $err_msg = LiveErrorCode::getError($data);
            if (!empty($err_msg)) {
                return [HTTP_NOTACCEPT, $err_msg];
            }
            if (!empty($data['goods'])){
                foreach($data['goods'] as $item=>$value){
                    LiveGood::update(['status'=>$value['audit_status']],['room_goods_id'=>$value['goods_id']]);
                }
            }
        }
        #10分钟实时更新状态
        Cache::store('redis')->set("live_goods_status_change",1,60*10);
        return [HTTP_SUCCESS, "成功"];
    }
}