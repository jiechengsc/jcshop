<?php
namespace app\shop_admin\model;

use app\SearchModel;
use think\Model;

class SeckillCommodity extends SearchModel
{
    public function commodity()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id')->bind(['name']);
    }

    public function seckill()
    {
        return $this->belongsTo(Seckill::class, 'seckill_id', 'id')->bind(['title']);
    }
}