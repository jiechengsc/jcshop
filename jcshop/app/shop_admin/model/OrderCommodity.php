<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;
use app\SearchModel;
use think\facade\Db;

class OrderCommodity extends SearchModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['after_type'])){
            self::append(['after_type_text']);
        }
    }
    public function skuPvsValue(){
        return $this->hasOne(Sku::class,'id','sku_id')->bind(['pvs_value']);
    }

    public function commodity()
    {
        return $this->belongsTo(Commodity::class,'commodity_id','id');
    }

    public function orderAfter()
    {
        return $this->hasOneThrough(OrderAfter::class,AfterOrderCommodity::class,'ocid','id','id','order_after_id');
    }
    public function commodityName(){
        return $this->hasOne(Commodity::class,'id','commodity_id')->bind(['name']);
    }

    public function orderData(){
        return $this->hasOne(Order::class,'id','order_id')->bind(['order_no,address,consignee,iphone']);
    }

    public function orderUserAddress(){
        return $this->hasOneThrough(UserAddress::class,Order::class,'id','id','order_id','user_address_id');
    }

    public function getStatusAttr($value, $data)
    {
        //订单状态: 订单状态: 1-待支付 2-待发货 3-已发货 4-已完结 5-已评价   6-删除 7-已收货 8-维权中 9-已维权 10-维权驳回 11-已关闭
        $state = [1=>"待支付",2=>"待发货",3=>"已发货",4=>"已完结",5=>"已评价",6=>"删除",7=>"已收货",8=>"维权中",9=>"已维权",10=>"维权驳回",11=>"已关闭", 13 => '开团中', 14 => '待发货', 15 => '拼团关闭'];
        return $state[$value];
    }

    public function getIsPrintAttr($is_bool){
        if ($is_bool == 0)return '未打印';
        return '已打印';
    }

    public function getStatusDataAttr($value,$data){
        return $data['status'];
    }

    public function getAfterTypeTextAttr($value,$data)
    {
        $arr = [1=>"仅退款",2=>"退货退款",3=>"换货"];
        return $arr[$data['after_type']];
    }
}