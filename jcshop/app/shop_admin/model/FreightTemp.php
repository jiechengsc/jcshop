<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\shop_admin\model;

use think\model\concern\SoftDelete;

class FreightTemp extends BaseModel
{

    use SoftDelete;


    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'禁用',1=>'正常'];
        return $status[$data['status']];
    }

    public function carryModeNum()
    {
        return $this->hasMany(CarryModeNum::class,'ft_id','id');
    }

    public function carryModeWeight()
    {
        return $this->hasMany(CarryModeWeight::class,'ft_id','id');
    }

}
