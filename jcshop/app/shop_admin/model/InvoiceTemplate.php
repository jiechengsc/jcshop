<?php


namespace app\shop_admin\model;


use think\Model;
use think\model\concern\SoftDelete;

class InvoiceTemplate extends Model
{
    use SoftDelete;
    protected $json = ['config'];
    protected $jsonAssoc = true;
    public function expressName(){
        return $this
            ->hasOne(Express::class,'id','express_id')
            ->field('id,name AS express_name');
    }
}