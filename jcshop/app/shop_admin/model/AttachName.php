<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types = 1);

namespace app\shop_admin\model;

use app\index\model\OrderCommodityAttach;
use app\SearchModel;

class AttachName extends SearchModel
{

    protected $autoWriteTimestamp = false;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

//    public function classify()
//    {
//        return $this->belongsTo(Classify::class,'id','classify_id')->bind(['classify_name'=>'name']);
//    }
//
//    public function attachValue()
//    {
//        return $this->hasMany(AttachValue::class,'attr_name_id', 'id')->field('id,value,attr_name_id')
//            ->hidden(['attr_name_id']);
//    }

}
