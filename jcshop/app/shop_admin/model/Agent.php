<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


class Agent extends BaseMallModel
{
    /**连接用户表
     * @return \think\model\relation\HasOne
     */
    public function User(){
        return $this->hasOne('User','id','user_id')
            ->bind(['nickname','picurl']);
    }
    public function UserAgent(){
        return $this->hasOne('UserCash','user_id','user_id')
            ->bind(['agent']);
    }
}