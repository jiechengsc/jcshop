<?php


namespace app\shop_admin\model;


use think\Model;
use think\model\concern\SoftDelete;

class ReceiptsTask extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    use SoftDelete;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }
}