<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;

use app\SearchModel;
use think\facade\Db;

class Order extends SearchModel
{
    protected $readonly = ['order_no', 'pay_no'];


    public function buildOrderNo($id)
    {
        $order_no = $id . $this->msectime() . rand(10000, 99999);
        return $order_no;
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function userAddress()
    {
        return $this->belongsTo(UserAddress::class, 'user_address_id', 'id');
    }

    public function orderAfter()
    {
        return $this->hasMany(OrderAfter::class,'order_id','id')->visible(['state']);
    }

    public function orderCommodity()
    {
        return $this->hasMany(OrderCommodity::class, 'order_id', 'id');
    }
    public function orderCommoditys()
    {
        return $this->hasMany(OrderCommodity::class, 'order_id', 'id')
            ->alias('y')
            ->join('commodity c', 'y.commodity_id=c.id', 'LEFT')
            ->join('sku k', 'y.sku_id=k.id', 'LEFT')
            ->join('sku_inventory s', 's.sku_id=k.id', 'LEFT')
            ->join('after_order_commodity t', 'y.id=t.ocid', 'LEFT')
            ->group('IFNULL(t.ocid,y.id)')
            ->order('y.id asc')
            ->field('y.order_id,y.count,y.status,y.sku_id,y.store_id,y.id,y.after_type,t.order_after_id,y.id AS order_commodity_id,y.after_status,y.is_print,
                c.name,c.master,c.id as cid,c.type,c.classify_value,c.has_sku,y.price as sell_price,y.is_evaluate,y.logistics_no,y.express_company,
                k.pvs_value,c.type AS order_type,y.attach_value,y.commodity_id,y.collage_id');
    }

    public function orderFreight()
    {
        return $this->hasOne(OrderFreight::class, 'order_id', 'id');
    }

    public function manyCommodity()
    {
        return $this->hasManyThrough(Commodity::class, OrderCommodity::class, 'order_id', 'id', 'id', 'commodity_id');
    }

    public function pay()
    {
        return $this->belongsTo(PayType::class, 'pay_id', 'id');
    }

    public function getPayIdAttr($name)
    {
        //支付方式 1-余额 2-微信 3-支付宝
        $pay = [1 => "余额", 2 => "微信", 3=>"支付宝",4=>"后台支付"];
        return $pay[$name];
    }

    public function freight()
    {
        return $this->hasOne(OrderFreight::class,'order_id','id')->bind(['freight']);
    }

    public function getStatusAttr($name, $data)
    {
        //订单状态: 1-待支付 2-待发货 3-已发货 4-已完结 5-已评价   6-删除 7-部分发货 8-已收货 9-部分收货 10-维权中 11-维权完成 12-已关闭
        $state = [
            1 => '待支付',
            2 => '待发货',
            3 => '已发货',
            4 => '已完结',
            5 => '已评价',
            6 => '删除',
            7 => '部分发货',
            8 => '已收货',
            9 => '部分收货',
            10 => '维权中',
            11 => '维权完成',
            12 => '已关闭',

            13 => '开团中',
            14 => '待发货',
            15 => '拼团关闭'
        ];

        return $state[$name];
    }

    public function getStatusDataAttr($value,$data){
        return $data['status'];
    }

    function msectime()
    {
        list($msec, $sec) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }

}