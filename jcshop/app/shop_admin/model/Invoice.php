<?php


namespace app\shop_admin\model;


use app\SearchModel;
use think\Model;

class Invoice extends SearchModel
{
    public function getStatusAttr($name)
    {
        //1-未审核 2-已通过 3-已驳回
        $pay = [1 => "未审核", 2 => "已通过", 3=>"已驳回"];
        return $pay[$name];
    }

    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function order()
    {
        return $this->belongsTo(Order::class,'order_id','id');
    }
}