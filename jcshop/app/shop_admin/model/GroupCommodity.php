<?php
namespace app\shop_admin\model;

use app\SearchModel;
use think\Model;

class GroupCommodity extends SearchModel
{
    public function commodity()
    {
        return $this->hasOne(Commodity::class, 'id', 'commodity_id')->bind(['name']);
    }

    public function group()
    {
        return $this->belongsTo(Group::class, 'group_id', 'id')->bind(['title']);
    }
}