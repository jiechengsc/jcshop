<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;
use app\SearchModel;

class OrderEvaluate extends SearchModel
{
    public function getImgUrlAttr($name)
    {
        $imgs = [];
        !empty($name) && $imgs = json_decode($name, true);
        return $imgs;
    }

    public function getStateAttr($name)
    {
        //1-未审核 2-已审核 3-驳回
        $arr = [1 => "未审核", 2 => "已审核", 3 => "驳回"];
        return $arr[$name];
    }
}