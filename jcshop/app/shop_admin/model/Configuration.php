<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use think\db\Query;
use think\Model;

class Configuration extends Model
{
    protected $globalScope = ['mallId'];
    protected $json = ['configuration'];
    protected $jsonAssoc = true;
    protected static $mallId;
   public function __construct(array $data = [])
    {
        parent::__construct($data);
        global $user;
        if (!empty($user)) {
            self::$mallId = $user->mall_id;
        }
    }

    public function scopeMallId(Query $query)
    {
        $query->where('mall_id', self::$mallId);
    }
}