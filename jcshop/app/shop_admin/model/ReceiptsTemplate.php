<?php


namespace app\shop_admin\model;


use think\Model;
use think\model\concern\SoftDelete;

class ReceiptsTemplate extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    use SoftDelete;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }
}