<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use think\db\Query;
use think\model\concern\SoftDelete;

class Admin extends BaseModel
{
    use SoftDelete;

    protected $table = 'jiecheng_shop_admin';

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            $this->append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'禁用',1=>'正常'];
        return $status[$data['status']];
    }

    /**
     * 关联角色
     * @return \think\model\relation\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id')->bind(['role_name' => 'name']);
    }


}