<?php
declare (strict_types = 1);

namespace app\shop_admin\model;

use think\db\Query;
use think\model\concern\SoftDelete;

class ShopAssistant extends BaseModel
{

    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [0=>'禁用',1=>'正常'];
        return $status[$data['status']];
    }

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id')->bind(["user_name"=>"nickname"]);
    }

    public function store()
    {
        return $this->belongsTo(Store::class,'store_id','id')->bind(['store_name'=>'name']);
    }

}
