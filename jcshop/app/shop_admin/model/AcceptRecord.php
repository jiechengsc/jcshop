<?php
declare (strict_types=1);

namespace app\shop_admin\model;



class AcceptRecord extends BaseModel
{

    protected $autoWriteTimestamp = false;

    protected $json = ['commodity'];

    protected $jsonAssoc = true;

}
