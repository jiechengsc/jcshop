<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\model;


use think\model\concern\SoftDelete;

class MerchantWithdraw extends BaseModel
{
    use SoftDelete;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])) {
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        $array = [0 => '申请中', 1 => '通过', 2 => '取消'];
        $text = empty($array[$data['status']]) ? '' : $array[$data['status']];
        return $text;
    }
}