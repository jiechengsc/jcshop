<?php
namespace app\shop_admin\model;

use app\SearchModel;
use think\Model;

class Seckill extends SearchModel
{
    public function seckillCommodity()
    {
        return $this->hasMany(Commodity::class, 'commodity_id', 'id');
    }
}