<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\middleware;

use app\madmin\model\Log;
use think\Response;

class TokenAfter
{
    /**
     * 全局添加token
     *
     * @param \think\Request $request
     * @param \Closure $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $response = $next($request);


        Log::write($response,'shop');
        // 添加中间件执行代码
        global $uri;

        $dischargedArr = ['operation/login', 'operation/logout', 'license/index'];
        if (in_array($uri, $dischargedArr)) {
            unset($GLOBALS['token'], $GLOBALS['user'], $GLOBALS['uri']);
            return $response;
        }

        unset($GLOBALS['token'], $GLOBALS['user'], $GLOBALS['uri'], $GLOBALS['flag']);
        return $response;
    }
}
