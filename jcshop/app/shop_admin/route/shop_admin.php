<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use app\shop_admin\validate\CommodityValidate;
use app\shop_admin\validate\CouponValidate;
use app\shop_admin\validate\FreightTempValidate;
use app\shop_admin\validate\RoleValidate;
use app\shop_admin\validate\RuleValidate;
use app\shop_admin\validate\ShopAssistantValidate;
use app\shop_admin\validate\StoreValidate;
use think\facade\Route;

// 达达回调
Route::any('dada_call_back', 'DaDa/index');
//Route::rest('create',['POST','/list','findAll']);
//Route::resource("user","sadmin/user");    资源路由 存在无法使用验证器问题
Route::get('', 'index/index');
//登录操作
Route::group("operation", function () {
    Route::post('login', 'login');
    Route::any('logout', 'logout');
})->prefix('operation/');

//后台用户
Route::group("admin", function () {
    Route::post('list', 'findAll')
        ->validate(app\shop_admin\validate\AdminValidate::class, 'list');
    Route::post('', 'save')
        ->validate(app\shop_admin\validate\Operation::class, "save");
    Route::get(':id', 'read');
    Route::put('selfUp/:id', 'selfUp');
    Route::put(':id', 'update')
        ->validate(app\shop_admin\validate\Operation::class, "update");
    Route::delete(':id', 'delete');
})->prefix('admin/')->pattern(['id' => '\d+']);

//部门
Route::group("auth/department", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(app\shop_admin\validate\Department::class, "save");
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(app\shop_admin\validate\Department::class, "update");
    Route::delete(':id', 'delete');
})->prefix('department/')->pattern(['id' => '\d+']);

//门店
Route::group("store", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(StoreValidate::class, 'list');
    Route::post('', 'save')
        ->validate(StoreValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('store/')->pattern(['id' => '\d+']);

//店员
Route::group("store/assistant", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(ShopAssistantValidate::class, 'list');
    Route::post('', 'save')
        ->validate(ShopAssistantValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(ShopAssistantValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('shop_assistant/')->pattern(['id' => '\d+']);

//快递
Route::group("freight/express", function () {
    Route::get('', 'index');
    Route::get(':id', 'read');
})->prefix('express/')->pattern(['id' => '\d+']);

//运费模板
Route::group("freight/temp", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(FreightTempValidate::class, 'list');
    Route::post('', 'save')
        ->validate(FreightTempValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('freight_temp/')->pattern(['id' => '\d+']);

//运费模板
Route::group("freight/area", function () {
    Route::get('[:areaCode]', 'index');
})->prefix('area/')->pattern(['areaCode' => '\d+']);

//商品分类
Route::group("classify", function () {
    Route::get('', 'index');
})->prefix('classify/')->pattern(['id' => '\d+']);

//spu
Route::group("spu", function () {
    Route::get('', 'index');
    Route::post('ids', 'findByIds');
    Route::post('sellout', 'findSellOut')
        ->validate(CommodityValidate::class, 'list');
    Route::post('list', 'findAll')
        ->validate(CommodityValidate::class, 'list');
    Route::post('', 'save')
        ->validate(CommodityValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(CommodityValidate::class, 'update');
    Route::put('reaudit/:id', 'reaudit');
    Route::put('inventory/:id', 'inventory');
    Route::put('', 'shelf');
    Route::post('shop_preview', 'shop_preview');
    Route::delete('', 'delete');
    Route::get('wechata/preview/:id', 'wechataPreview');
    Route::get('changeLiveGoodStatus','changeLiveGoodStatus');
    Route::post('scrapy','scrapy');
    Route::get('scrapyLogList','scrapyLogList');
})->prefix('commodity/')->pattern(['id' => '\d+']);


//权限角色
Route::group("auth/role", function () {
    Route::get('', 'index');
    Route::post('list', 'findAll')
        ->validate(RoleValidate::class, 'list');
    Route::post('', 'save')
        ->validate(RoleValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(RoleValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('role/')->pattern(['id' => '\d+']);

//权限规则
Route::group("auth/rule", function () {
    Route::get('', 'index');
    Route::get('value', 'findAllValue');
    Route::post('list', 'findAll')
        ->validate(RuleValidate::class, 'list');
    Route::post('', 'save')
        ->validate(RuleValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(RuleValidate::class, 'update');
    Route::delete(':id', 'delete');
})->prefix('rule/')->pattern(['id' => '\d+']);


//上传接口
Route::group("upload", function () {
    //这里校验上传接口会有问题
//    Route::post('images','images')->validate(app\sadmin\validate\Upload::class,'images');
    Route::post('images', 'images');
})->prefix('upload/');

//配置文件
Route::group("configuration", function () {
    Route::post('', 'save');
//        ->validate(\app\shop_admin\validate\ShopConfigurationValidate::class,'save');
    Route::get(':type', 'read');
    Route::put(':type', 'update');
//        ->validate(\app\shop_admin\validate\ShopConfigurationValidate::class,'update');
})->prefix('shop_configuration/')->pattern(['type' => '[a-zA-Z]+']);
//统计
Route::group('statistic', function () {
    Route::any('index', 'index');
    Route::get('exportUser', 'exportUser');
    Route::get('exportCommodity', 'exportCommodity');
    Route::post('total', 'total')
        ->validate(app\shop_admin\validate\StatisticsValidate::class, 'total');
    Route::post('count', 'count')
        ->validate(app\shop_admin\validate\StatisticsValidate::class, 'count');
    Route::post('sales_ranking', 'sales_ranking');
    Route::post('consumption_ranking', 'consumption_ranking');
    Route::post('statistics', 'statistics');
    Route::group('user', function () {
        Route::get('total/:user_id', 'user_total');
    });
    Route::group('unified', function () {
        Route::post('count', 'unified_count');
    });
})->prefix('Statistics/');
//小票打印
Route::group('receipts', function () {
    Route::post('create', 'create');
    Route::group('template', function () {
        Route::get('list', 'template_list');
        Route::get(':id', 'template_find');
        Route::post('default/:id', 'template_default');
        Route::post('', 'template_create')
            ->validate(app\shop_admin\validate\ReceiptsValidate::class, 'create');
        Route::put(':id', 'template_update')
            ->validate(app\shop_admin\validate\ReceiptsValidate::class, 'update');
        Route::delete(':id', 'template_delete');
    });
    Route::group('printer', function () {
        Route::get('list', 'printer_list');
        Route::get('default/:id', 'printer_default');
        Route::get(':id', 'printer_find');
        Route::post('', 'printer_insert')
            ->validate(app\shop_admin\validate\ReceiptsValidate::class, 'insert');
        Route::put(':id', 'printer_update');
        Route::delete(':id', 'printer_delete');
    });
    Route::group('task', function () {
        Route::get(':id', 'task_find');
        Route::post('list', 'task_list');
        Route::post('', 'task_create')
            ->validate(app\shop_admin\validate\ReceiptsValidate::class, 'task_create');
        Route::put(':id', 'task_update');
        Route::delete(':id', 'task_delete');
    });
})->prefix('Receipts/');
//电子面单
Route::group('express_bill', function () {
    Route::post('create', 'create_bill')
        ->validate(app\shop_admin\validate\ExpressBillValidate::class, 'create');
    Route::get('config', 'getConfig');
    Route::post('config', 'setConfig');
    Route::group('template', function () {
        Route::post('', 'create_template');
        Route::put(':id', 'update_template');
        Route::delete(':id', 'delete_template');
        Route::post('list', 'list_template');
        Route::get(':id', 'find_template');
        Route::post('default/:id', 'default_template');
    });
})->prefix('ExpressBill/');

//核销记录
Route::group("accept/record", function () {
    Route::post('list', 'findAll');
})->prefix('accept_record/')->pattern(['id' => '\d+']);

//订单
Route::group("order", function () {
    Route::post('list', 'orderList');
    Route::post('cancelOrder', 'cancelOrder');
    Route::post('exportData', 'exportData');
    Route::put('save', 'save')
        ->validate(app\shop_admin\validate\OrderValidate::class, 'save');
    Route::post('drawback', 'order/drawback');
    Route::post('localDelivery', 'localDelivery');
    Route::post('dadaPay', 'dadaPay');
    Route::get('dadaQuery', 'dadaQuery');
    Route::get('export', 'export');
    Route::get('read', 'read');
    Route::get('storeList', 'storeList');
    Route::get('excelDownload', 'excelDownload');
    Route::get('expressDownload', 'expressDownload');
    Route::get('getDistribution', 'getDistribution');
    Route::get(':id', 'orderInfo');
    Route::delete(':id', 'orderDel');
    Route::put('saveOrderStatus', 'saveOrderStatus');
    Route::put('orderClose', 'orderClose');
    Route::put('modifyOrderState', 'modifyOrderState');
    Route::put('saveConsignee', 'saveConsignee')
        ->validate(app\shop_admin\validate\OrderValidate::class, 'saveConsignee');
    Route::put('changeMoney', 'changeMoney');
    Route::get('changeMoneyList', 'changeMoneyList');

})->prefix('order/')->pattern(['id' => '\d+']);

// 达达配送
Route::group("DaDa", function () {
    Route::post('pay', 'pay');
    Route::post('formalCancel', 'formalCancel');
    Route::get('query', 'query');
    Route::get('cityCode', 'list');
    Route::get('reasons', 'reasons');
})->prefix('da_da/');

//退货地址
Route::group("return/address", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('return_address/')->pattern(['id' => '\d+']);

//公告
Route::group("notice", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('notice/')->pattern(['id' => '\d+']);

//评论
Route::group("evaluate", function () {
    Route::delete(':id', 'evaluateDel');
    Route::get('evaluateList', 'evaluateList');
    Route::post('examine', 'examine');
    Route::post('reply', 'reply');
})->prefix('evaluate/');

//售后
Route::group('aftersale', function () {
    Route::get('afterSaleList', 'afterSaleList');
    Route::get('getAfterAddress', 'getAfterAddress');
    Route::get(':id', 'afterInfo');
    Route::put('upAfterState', 'upAfterState');
})->prefix('after_sale/');
//日志
Route::group('journal', function () {
    Route::get('readLog', 'readLog');
})->prefix('journal/');

//图传
Route::group('pic', function () {
    Route::post('addPic', 'addPic');
    Route::post('addGroup', 'addGroup');
    Route::get('picList', 'picList');
    Route::get('groupList', 'groupList');
    Route::get('ossConfig', 'ossConfig');
    Route::put('editGroup', 'editGroup');
    Route::delete('delPic', 'delPic');
    Route::delete(':id', 'delGroup');
    Route::get('getUrl', 'getUrl');
})->prefix('pic/')->pattern(['id' => '\d+']);

//优惠券
Route::group("coupon", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save')
        ->validate(CouponValidate::class, 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update')
        ->validate(CouponValidate::class, 'update');
    Route::delete(':id', 'delete');
    Route::post('send/:id', 'send');
})->prefix('coupon/')->pattern(['id' => '\d+']);

//用户优惠券
Route::group("user/coupon", function () {
    Route::post('list', 'findAll');
    Route::post('', 'save');
    Route::get(':id', 'read');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
})->prefix('user_coupon/')->pattern(['id' => '\d+']);

//商户提现
Route::group('merchant/withdraw', function () {
    //提现:余额
    Route::put('total', 'total');
    Route::post('list', 'findAll');
    Route::post('', 'save');
})->prefix('merchant_withdraw/');

//商户详情
Route::group("merchant", function () {
    Route::get('', 'read');
    Route::put('', 'update')
        ->validate(\app\shop_admin\validate\MerchantValidate::class, 'update');
    Route::post('customer_service', 'saveCustomerService')
        ->validate(\app\shop_admin\validate\MerchantValidate::class, 'saveCustomerService');
})->prefix('merchant/')->pattern(['id' => '\d+']);
//发票
Route::group('invoice/manage', function () {
    Route::get('invoiceList', 'invoiceList');
    Route::put('examine', 'examine');
    Route::get('read', 'read');
})->prefix('InvoiceManage/');
//创建发货单模板
Route::group('invoice', function () {
    Route::get('', 'list');
    Route::get(':id', 'read');
    Route::post('', 'create');
    Route::put(':id', 'update');
    Route::delete(':id', 'delete');
    Route::post('print', 'print');
})->prefix('invoice/');


//积分
Route::group('integral',function (){
    Route::get('open','open');
})->prefix('integral/');

//插件
Route::group("addons", function () {
    Route::get('', 'index');
})->prefix('addons/');

//商户详情
Route::group("merchant/settlement", function () {
    Route::post('list', 'findAll');
})->prefix('merchant_settlement/')->pattern(['id' => '\d+']);

//同城配送  Distribution
Route::group("distribution", function () {
    Route::get('read', 'read');
    Route::get('transport', 'transport');
    Route::post('save', 'save');
    Route::post('upDistributionType', 'upDistributionType');
    Route::post('saveMyContact', 'saveMyContact');
    Route::get('getJurisdiction', 'getJurisdiction');
    Route::get('getMyContact', 'getMyContact');
})->prefix('distribution/')->pattern(['id' => '\d+']);


//平台
Route::group("lessee", function () {
    Route::get('entry', 'entry');
})->prefix('lessee/')->pattern(['id' => '\d+']);

// 微信客服
Route::get('wx/cs','wx_cs/index');