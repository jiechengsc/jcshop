<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use app\shop_admin\model\Classify as admin;
use app\shop_admin\model\Commodity;
use think\facade\Cache;

class ClassifyService
{

    /**
     * 获取列表
     * @return array
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        global $user;
        Cache::store("redis")->has(checkRedisKey("MALL:CLASSIFY_LEVEL")) ?
            $level = Cache::store("redis")->get(checkRedisKey("MALL:CLASSIFY_LEVEL")) :
            $level = 2;
        if (Cache::store("redis")->has(checkRedisKey("MALL:CLASSIFY"))) {
            $list = Cache::store("redis")->get(checkRedisKey("MALL:CLASSIFY"));
        } else {
//            动态生成with
            $max = $level;
            $with = '';
            if ($max > 0) {
                $with .= "children";
                if ($max > 1) {
                    for ($i = 1; $i < $max; $i++)
                        $with .= ".children";
                }
            }

            $list = admin::where('pid', 0)
                ->field('id,name,img,pid')
                ->where('mall_id',$user['mall_id'])
                ->with($with)
                ->select();
            Cache::store("redis")->set(checkRedisKey("MALL:CLASSIFY"),$list);
        }
//        $list = Commodity::field("GROUP_CONCAT(DISTINCT classify_id SEPARATOR ',') AS name")->select();
        return [HTTP_SUCCESS, ["data" => $list]];
    }
}
