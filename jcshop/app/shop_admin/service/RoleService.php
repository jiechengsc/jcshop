<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use app\shop_admin\model\Role as admin;
use app\shop_admin\model\RoleRule;
use app\utils\TrimData;
use think\db\Query;
use think\Exception;

class RoleService
{

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $list = admin::where('status', 1)
            ->field('id,name')
            ->select();
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
//        $data =array_filter($data,function ($val){
//            if (is_array($val) || (is_string($val) && "" !=trim($val))){
//                return true;
//            }
//            return false;
//        });

        $admin = admin::field('id,name,remark,department_id,is_root,status,create_time,update_time');
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'begin_date', 'end_date']);
        $list = $admin->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {
        $ruleList = $data['rule'];
        unset($data['rule'], $data['is_root']);
        $role = admin::create($data);
        foreach ($ruleList as $k => $v) {
            RoleRule::create(['role_id' => $role->id, 'rule_id' => $v]);
        }
        return [HTTP_CREATED, $role];
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::with([
            'rule' => function (Query $query) {
                $query->where('status', 1)->visible(['id']);
            }
        ])->find($id);
        if ($model) {
            $model = $model->toArray();
            $model['rule'] = array_column($model['rule'], 'id');
        }
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time'],$data['is_root']);
        $this->getAdmin($id);

        $ruleList = (array)$data['rule'];
        unset($data['rule']);

        RoleRule::where(['role_id' => $id])->delete();
        foreach ($ruleList as $k => $v) {
            RoleRule::create(['role_id' => $id, 'rule_id' => $v]);
        }
        admin::where('update_time', $update_time)
            ->where(['id' => $id, 'is_root' => 0])
            ->save($data);

        return [HTTP_CREATED, 1];
    }

    private function getAdmin($id)
    {
        $admin = admin::find($id);
        if (1 === $admin->is_root){
            throw new Exception("初始账号不能操作",HTTP_NOTACCEPT);
        }
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        $this->getAdmin($id);
        admin::destroy(function ($query) use ($id) {
            $query->where(['id' => $id,'is_root' =>0]);
        });
        return HTTP_NOCONTEND;
    }

}
