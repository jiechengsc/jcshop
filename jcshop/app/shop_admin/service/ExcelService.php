<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\service;

use \PhpOffice\PhpSpreadsheet\IOFactory;
use \PhpOffice\PhpSpreadsheet\Spreadsheet;

class ExcelService
{
    /**
     * 导出订单excel
     * @param string $fileName 文件名
     * @param string $title 工作表名
     * @param array $headArr 第一栏标题
     * @param array $data 导出数据 二维数组
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    function excelExport($fileName, $title, $headArr = [], $data = [])
    {
        $fileName .= "-" . date("YmdHi", time()) . ".xls";
        $objPHPExcel = new Spreadsheet();
        $objSheet = $objPHPExcel->getActiveSheet();
        $objSheet->setTitle($title);
        $key = ord("A"); // 设置表头
        foreach ($headArr as $v) {
            $colum = chr($key);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colum)->setAutoSize(true);
            $objSheet->setCellValue($colum . '1', $v);
            $key += 1;
        }
        $column = 2;
        foreach ($data as $key => $rows) { // 行写入
            $span = ord("A");
            $oc = $rows['orderCommodity'];
            unset($rows['orderCommodity']);
                foreach ($rows as $keyName => $value) { // 列写入
                    $objSheet->setCellValue(chr($span) . $column, $value);
                    $span++;
                }
            if ($span > 78 ){
                foreach ($oc as $k=>$v){
                    $span1 = ord("O");
                    foreach ($v as $vv){
                        $objSheet->setCellValue(chr($span1) . $column, $vv);
                        $span1++;
                    }
                   if ($k < count($oc)-1) $column++;
                }

            }
            $column++;
        }
        $fileName = iconv("utf-8", "gb2312", $fileName); // 重命名表
        $objPHPExcel->setActiveSheetIndex(0); // 设置活动单指数到第一个表,所以Excel打开这是第一个表
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$fileName");
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xls');
        $objWriter->save('php://output'); // 文件通过浏览器下载
        exit();
    }

    /**导出普通Excel
     * @param $fileName
     * @param $title
     * @param array $headArr
     * @param array $data
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     */
    public function excel($fileName, $title, $headArr = [], $data = [])
    {
        $fileName .= "-" . date("YmdHi", time()) . ".xls";
        $objPHPExcel = new Spreadsheet();
        $objSheet = $objPHPExcel->getActiveSheet();
        $objSheet->setTitle($title);
        $key = ord("A"); // 设置表头
        foreach ($headArr as $v) {
            $colum = chr($key);
            $objPHPExcel->getActiveSheet()->getColumnDimension($colum)->setAutoSize(true);
            $objSheet->setCellValue($colum . '1', $v);
            $key += 1;
        }
        $column = 2;
        foreach ($data as $key => $rows) { // 行写入
            $span = ord("A");
            foreach ($rows as $keyName => $value) { // 列写入
                $objSheet->setCellValue(chr($span) . $column, $value);
                $span++;
            }
            $column++;
        }
        $fileName = iconv("utf-8", "gb2312", $fileName); // 重命名表
        $objPHPExcel->setActiveSheetIndex(0); // 设置活动单指数到第一个表,所以Excel打开这是第一个表
        header('Content-Type: application/vnd.ms-excel');
        header("Content-Disposition: attachment;filename=$fileName");
        header('Cache-Control: max-age=0');
        $objWriter = IOFactory::createWriter($objPHPExcel, 'Xls');
        $objWriter->save('php://output'); // 文件通过浏览器下载
        exit();
    }
}