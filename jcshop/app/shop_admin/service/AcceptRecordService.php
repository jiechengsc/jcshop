<?php
declare (strict_types=1);

namespace app\shop_admin\service;

use app\shop_admin\model\AcceptRecord as admin;
use app\utils\TrimData;

class AcceptRecordService
{

    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field(['order_no','commodity','assistant','store','accept_time']);
        $admin = TrimData::searchDataTrim($admin, $data, ['order_no', 'store', 'begin_date', 'end_date']);

        $list = $admin->where($data)
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

}
