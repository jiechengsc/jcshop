<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use app\shop_admin\model\Area;
use app\shop_admin\model\CarryModeNum;
use app\shop_admin\model\CarryModeWeight;
use app\shop_admin\model\FreightTemp as admin;
use app\utils\TrimData;
use think\facade\Cache;

class FreightTempService
{

    /**
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function index()
    {
        $list = admin::field('id,name')->select();
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = admin::field('id,name,express_id,carry_mode_type,no_carry,no_carry_value,is_default,sort,status,create_time,update_time')
            ->with(['carryModeNum', 'carryModeWeight']);
        $admin = TrimData::searchDataTrim($admin, $data, ['name', 'begin_date', 'end_date']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 保存数据
     * @param array $data
     * @return array
     */
    public function save(array $data)
    {

//        $this->noCarryValue($data);

        $model = admin::create($data);

        if ($data['carry_mode_type'] === 0)
            $model->carryModeNum()->saveAll($data['carryModeNum']);
        elseif ($data['carry_mode_type'] === 1)
            $model->carryModeWeight()->saveAll($data['carryModeWeight']);
        Cache::store('redis')->tag('FT')->clear();

        return [HTTP_CREATED, $model];
    }

    private function noCarryValue(&$data)
    {
        if (isset($data['no_carry']) && !empty($data['no_carry'])) {
            $areaCode = explode(",", $data['no_carry']);
            $areaName = Area::where(['area_code' => $areaCode])->column('name');
            $data['no_carry_value'] = implode(",", $areaName);
        }
    }

    /**
     * 读取一条数据
     * @param int $id
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function read(int $id)
    {
        $model = admin::with(['carryModeNum', 'carryModeWeight'])->find($id);
        return [HTTP_SUCCESS, $model];
    }


    /**
     * 更新数据
     * @param int $id
     * @param array $data
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time']);

        $temp = admin::where(['update_time' => $update_time])->lock(true)->find($id);

        if (empty($temp))
            return [HTTP_CREATED, 0];

        if (isset($data['is_default']) && $data['is_default'] == 1)
            admin::update(['is_default' => 0], ['is_default' => 1]);

//        $this->noCarryValue($data);

        $temp->carry_mode_type == 0 ?
            CarryModeNum::where('ft_id', $temp->id)->delete() :
            CarryModeWeight::where('ft_id', $temp->id)->delete();

        if ($data['carry_mode_type'] === 0)
            $temp->carryModeNum()->saveAll($data['carryModeNum']);
        elseif ($data['carry_mode_type'] === 1)
            $temp->carryModeWeight()->saveAll($data['carryModeWeight']);
        admin::update($data, ['id' => $id]);
        Cache::store('redis')->tag('FT')->clear();
        return [HTTP_CREATED, 1];
    }

    /**
     * @param $id
     * @return int
     */
    public function delete($id)
    {
        admin::destroy($id);
        Cache::store('redis')->tag('FT')->clear();
        return HTTP_NOCONTEND;
    }

}
