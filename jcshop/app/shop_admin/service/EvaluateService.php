<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\shop_admin\service;

use app\shop_admin\model\OrderEvaluate;

class EvaluateService
{
    /**获取评论列表
     * @param array $data
     * @return array
     */
    public function evaluateList(array $data)
    {
        $res = OrderEvaluate::where($this->bulidData($data))
            ->alias('a')
            ->join('user u', 'a.user_id = u.id', 'LEFT')
            ->join('order o', 'a.order_id = o.id', 'LEFT')
            ->join('commodity c', 'a.commodity_id = c.id', 'LEFT')
            ->field('
            a.content,a.img_url,a.score,a.create_time,a.remake,a.reply,a.state,
            u.nickname,u.picurl,o.order_no,c.name,c.master,a.id
            ')
            ->order('a.id desc')
            ->paginate(['page' => $data['page'], 'list_rows' => $data['size']]);
        return [HTTP_SUCCESS, $res];
    }

    /**审核通过或驳回
     * @param array $data
     * @return array
     */
    public function examine(array $data)
    {
        $id = $data['id'];
        unset($data['id']);
        $res = OrderEvaluate::update($data, ['id' => $id]);
        return [HTTP_SUCCESS, $res];
    }

    /**掌柜回复
     * @param array $data
     * @return array
     */
    public function reply(array $data)
    {
        $id = $data['id'];
        unset($data['id']);
        $res = OrderEvaluate::update($data, ['id' => $id]);
        return [HTTP_SUCCESS, $res];
    }

    /** 评论删除
     * @param int $id
     * @return array
     */
    public function evaluateDel(int $id)
    {
        OrderEvaluate::destroy($id);
        return HTTP_NOCONTEND;
    }

    private function bulidData(array $data)
    {
        global $user;
        $where = [];
        $where[] = ['o.merchant_id','=',$user->merchant_id];
        !empty($data['status']) && $where[] = ['a.state', '=', $data['status']];
        !empty($data['name']) && $where[] = ['u.nickname|u.name', '=', $data['name']];
        !empty($data['order_no']) && $where[] = ['o.order_no', '=', $data['order_no']];
        !empty($data['cname']) && $where[] = ['c.name', 'like', '%' . $data['cname'] . "%"];
        return $where;
    }
}