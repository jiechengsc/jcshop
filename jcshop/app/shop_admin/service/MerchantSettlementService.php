<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\shop_admin\service;

use app\shop_admin\model\view\MerchantSettlement as admin;
use app\utils\TrimData;
use think\facade\Db;

class MerchantSettlementService
{

    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    /**
     * 查询列表
     * @param int $page
     * @param int $size
     * @param array $data
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        Db::query("SET SESSION group_concat_max_len=102400;");
        $admin = admin::field("*");
        $admin = TrimData::searchDataTrim($admin, $data, ['order_no', 'pay_no', 'begin_date', 'end_date']);

        $list = $admin->where($data)
            ->order('id','desc')
            ->hidden(['mall_id','merchant_id'])
            ->paginate(['page' => $page, 'list_rows' => $size]);
        Db::query("SET SESSION group_concat_max_len=1024;");
        return [HTTP_SUCCESS, $list];
    }

}
