<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);


namespace app\shop_admin\service;

use app\madmin\model\WxCustomer;
use app\madmin\model\WxCompany;

class WxCsService
{
    /**
     * 查询列表
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index(int $page = 1, int $size = 10)
    {
        $list = WxCustomer::paginate(['page' => $page, 'list_rows' => $size]);
        foreach ($list as $key => $value) {
            $list[$key]['corp_id'] = WxCompany::find($value['company_id'])['corp_id'];
        }
        return [HTTP_SUCCESS, $list];
    }

}
