<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


use think\facade\Env;
use think\facade\Db;

// 这是系统自动生成的公共文件
function createToken()
{
    $str = md5(uniqid(md5(microtime(true)), true));  //生成一个不会重复的字符串
    $str = sha1($str);  //加密
    return $str;
}

function retArr($code, $message = '', $data = array())
{
    if (!is_numeric($code)) {
        return '';
    }

    return array(
        'code' => $code,
        'message' => $message,
        'data' => $data
    );
}

function timediff($starttime, $endtime)
{
    $timediff = $endtime - $starttime;
    $days = intval($timediff / 86400);
    $remain = $timediff % 86400;
    $hours = intval($remain / 3600);
    $remain = $remain % 3600;
    $mins = intval($remain / 60);
    $secs = $remain % 60;
    $res = array("day" => $days, "hour" => $hours, "min" => $mins, "sec" => $secs);
    return $res;
}

/*
$array:需要排序的数组
$keys:需要根据某个key排序
$sort:倒叙还是顺序
*/
function arraySort($array, $keys, $sort = 'asc')
{
    $newArr = $valArr = array();
    foreach ($array as $key => $value) {
        $valArr[$key] = $value[$keys];
    }
    ($sort == 'asc') ? asort($valArr) : arsort($valArr);//先利用keys对数组排序，目的是把目标数组的key排好序
    reset($valArr); //指针指向数组第一个值
    foreach ($valArr as $key => $value) {
        $newArr[$key] = $array[$key];
    }
    return $newArr;
}

function checkRedisKey($key)
{
    global $user;
    return "MALL:" . $user->mall_id . ":" . $key;
}
function encode(){
    $keys=[
        'time'=>time(),
        'name'=>"jinzhe",
        'domain'=>request()->host()
    ];
    return authcode(json_encode($keys),"jinzhe");
}

function checkAddons($id)
{
    $lessee = json_decode(Db::name("lessee")->where(['status' => 1])->value('addons'));
    if (is_array($lessee) && in_array($id, $lessee)) {
        return true;
    }
    return false;
}
