<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\command;

use think\console\Command;
use think\console\Input;
use think\console\Output;

abstract class BaseCommand extends Command
{

    protected function setDb(Input $input, Output $output)
    {
        $this->executeAction($input, $output);
    }

    protected abstract function executeAction(Input $input, Output $output);

}