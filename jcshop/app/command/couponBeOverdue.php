<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\command;

use think\console\Input;
use think\console\Output;
use think\facade\Db;

class couponBeOverdue extends BaseCommand
{
    protected function configure()
    {
        // 指令配置
        $this->setName('coupon_be_overdue')
            ->setDescription('优惠券过期');
    }
    protected function execute(Input $input, Output $output){
        $this->setDb($input, $output);
    }

    protected function executeAction(Input $input, Output $output)
    {
        sleep(1);
        Db::name('user_coupon')
            ->alias('a')
            ->join('coupon c','c.id=a.coupon_id','left')
            ->where('a.status',0)
            ->where('c.time_limit_type',0)
            ->where('DATE_FORMAT(DATE_ADD(`a`.`create_time`, INTERVAL `c`.`limit_time` DAY), \'%Y-%m-%d 00:00:00\') < now(0)')
            ->update([
                'a.status' => 2,
                'a.update_time' => date('Y-m-d H:i:s')
            ]);
        Db::name('user_coupon')
            ->alias('a')
            ->join('coupon c','c.id=a.coupon_id','left')
            ->where('a.status',0)
            ->where('time_limit_type',1)
            ->where('limit_indate_end','<',date('Y-m-d H:i:s'))
            ->update([
                'a.status' => 2,
                'a.update_time' => date('Y-m-d H:i:s')
            ]);
    }
}
