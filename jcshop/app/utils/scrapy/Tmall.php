<?php

namespace app\utils\scrapy;


use app\shop_admin\service\CommodityService;

class Tmall extends Base
{
    public $apiUrl = "/tmall/detail";
    #https://chaoshi.detail.tmall.com/item.htm?spm=a230r.1.14.34.3a5073dbM75EJv&id=655712287964&ns=1&abbucket=16
    #
    public function getSku($result)
    {
        $item = $result['data']['item'];
        $price = $item['priceRange'] ?? 0;
        if (empty($item['props'])) {
            return ['sku' => [], 'total' => 0, 'has_sku' => 0, 'sku_value' => [], 'has_attach' => 0, 'has_parameter' => 0, 'sell_price' => $price, 'original_price' => $price, 'max_price' => $price, 'min_price' => $price];
        }

        $return_data = [];

        $skus = $item['sku'];
        $props = $item['props'];
        $has_sku = empty($skus) ? 0 : 1;
        $sku_props = [];
        $sku_type_search = [];
        $sku_type_replace = [];
        $prices = [];
        foreach ($props as $k => $prop) {
            $attrValue = [];
            foreach ($prop['values'] as $item => $value) {
                $attr = ['name' => $prop['name'], "value" => $value['name']];
                array_push($attrValue, $attr);
            }
            $arr = [
                'name' => $prop['name'],
                'has_pic' => ($k == count($props) - 1) ? 1 : 0,
                'attrValue' => $attrValue
            ];
            array_push($sku_props, $arr);
            array_push($sku_type_search, $prop['name']);
            array_push($sku_type_replace, $prop['name'] . ":");
        }
        $shop_sku = [];
        $quantity_total = 0; #总库存
        #整理出所有图片异步下载,不然单个下载性能过低
        $sku_images = [];

        foreach ($skus as $sku) {
            if (!empty($sku['image'])) {
                $sku_images[] = $sku['image'];
            }
        }

        #异步下载
        $sku_images = images_download($sku_images);

        foreach ($skus as $sku) {
            if (empty($sku['skuName'])) {
                $quantity_total = $sku['quantity'];
                continue;
            }

            $shop_sku_v = [
                "pvs" => str_replace(";", ",", str_replace($sku_type_search, $sku_type_replace, $sku['skuName'])), #"颜色:红色,尺码:L",
                "weight" => 0,
                "pic" => isset($sku['image']) ? (!empty($sku_images[$sku['image']]) ? $sku_images[$sku['image']] : '') : '',
                "inventory" => [
                    "original_price" => $sku['price'],
                    "sell_price" => $sku['price'],
                    "total" => $sku['quantity']
                ]
            ];
            array_push($prices, $sku['price']);
            array_push($shop_sku, $shop_sku_v);
        }

        if (count($prices) > 0) {
            $return_data['max_price'] = max($prices);
            $return_data['min_price'] = min($prices);
            $avg_price = number_format(array_sum($prices) / count($prices), 2);
            $return_data['sell_price'] = $avg_price;
            $return_data['original_price'] = $avg_price;
        }

        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $quantity_total, 'has_sku' => $has_sku, 'has_attach' => 1, 'has_parameter' => 0]);
        return $return_data;
    }

    public function getDescHtml($item)
    {
        $html = "";
        $images = $item['descImgs'] ?? [];


        $img_arr = images_download($images);

        foreach ($img_arr as $image) {
            $url = $this->oss_url . '/' . $image;
            $html .= "<p><img class='wscnph' style='display: block; margin-left: auto; margin-right: auto;' src='$url'></p>";
        }
        return $html;
    }


    public function scrapy()
    {

//        $goods_id=$this->getId();
//        if(!$goods_id){
//            return '解析异常';
//        }

//        $tmall_path = public_path() . "tmall1.json";
//        $result = json_decode(file_get_contents($tmall_path), true);
        $result = $this->getData();

        $item = $result['data']['item'];
        $images = $item['images'];

        $img_arr = images_download($images);
        $slideshows = [];
        foreach ($img_arr as $image) {
            array_push($slideshows, ['url' => $image]);
        }

        $data = [

            'content' => $this->getDescHtml($item),
            'mall_id' => $this->mall_id,
            'master' => !empty($slideshows) ? $slideshows[0]['url'] : '',
            'name' => mb_substr($item['title'], 0, 32),
            'slideshow' => $slideshows, #轮播图
            'subtitle' => empty($item['subTitle']) ? mb_substr($item['title'], 0, 32) : mb_substr($item['subTitle'], 0, 32), #副标题,
        ];

        #写入数据
        $data = array_merge($data, $this->getSku($result));

        $this->write($data);
        return [HTTP_SUCCESS, "success"];
    }
}