<?php

namespace app\utils\scrapy;


use app\shop_admin\service\CommodityService;

class Jd extends base
{
    public $apiUrl="/jd/detail";

    #https://item.jd.com/10052971560774.html
    public function getSku($result)
    {
        $return_data = [];
        $item = $result['data']['item'];
        $skus = $item['sku'];
        $props = $item['saleProp'];
        $has_sku = empty($skus) ? 0 : 1;
        $sku_props = [];
        $sku_type_search = [];
        $sku_type_replace = [];
        $prices = [];
        $types=[];
        $sku_images = [];
        foreach ($skus as $sku) {
            if (!empty($sku['imagePath'])) {
                $sku_images[] = "https://img14.360buyimg.com/n1/".$sku['imagePath'];
            }
        }
        foreach ($props as $k => $prop) {

            $attrValue = [];
            if (empty($item['skuProps'][$k])){
                continue;
            }
            foreach ($item['skuProps'][$k] as $item1 => $value) {
                $attr = ['name' => $prop, "value" => $value];
                array_push($attrValue, $attr);
                $types[$prop][]=$value;
            }
            $arr = [
                'name' => $prop,
                'has_pic' => empty($sku_images) ? 0 : (($k == count($props) - 1) ? 1 : 0),
                'attrValue' => $attrValue
            ];
            array_push($sku_props, $arr);


        }
        $shop_sku = [];
        $quantity_total = 0; #总库存
        #整理出所有图片异步下载,不然单个下载性能过低

        #jfs/t1/132562/24/23053/127623/629458caEf82b34ab/1a93039e4b6bccf5.jpg



        #异步下载
        $sku_images = images_download($sku_images);
        $types_keys_arr=array_keys($types);
        foreach ($skus as $sku) {
            $pvs_arr=[];
            for ($i=1; $i<=count($types) ;$i++){
                if(empty($sku[$i])){
                    continue;
                }
                $pvs_arr[]=$types_keys_arr[$i-1].":".$sku[$i];
            }
            $pvs_str=implode(",",$pvs_arr);


            $shop_sku_v = [
                "pvs" => $pvs_str, #"颜色:红色,尺码:L",
                "weight" => 0,
                "pic" => !empty($sku['imagePath']) ? $sku_images["https://img14.360buyimg.com/n1/".$sku['imagePath']] ?? '' : '',
                "inventory" => [
                    "original_price" => $sku['originalPrice'],
                    "sell_price" => $sku['price'],
                    "total" => $sku['stockState'] ?? 0
                ]
            ];
            array_push($prices, $sku['price']);
            array_push($shop_sku, $shop_sku_v);
        }

        if (count($prices) > 0) {
            $return_data['max_price'] = max($prices);
            $return_data['min_price'] = min($prices);
            $avg_price = number_format(array_sum($prices) / count($prices), 2);
            $return_data['sell_price'] = $item['price'] ?? 0;
            $return_data['original_price'] = $item['originalPrice'] ?? 0;
        }
        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $quantity_total, 'has_sku' => $has_sku, 'has_attach' => 1, 'has_parameter' => 0]);

        return $return_data;
    }

    public function getDescHtml($item)
    {
        $html = "";
        $images = $item['descImgs'] ?? [];


        $img_arr = images_download($images);

        foreach ($img_arr as $image) {
            $url = $this->oss_url . '/' . $image;
            $html .= "<p><img class='wscnph' style='display: block; margin-left: auto; margin-right: auto;' src='$url'></p>";
        }
        return $html;
    }

    public function scrapy()
    {

        $result=$this->getData();

        $item = $result['data']['item'];
        $images = $item['images'];

        $img_arr = images_download($images);
        $slideshows = [];
        foreach ($img_arr as $image) {
            array_push($slideshows, ['url' => $image]);
        }

        $data = [
            'content' => $this->getDescHtml($item),
            'mall_id' => $this->mall_id,
            'master' => !empty($slideshows) ? $slideshows[0]['url'] : '',
            'name' => mb_substr($item['name'],0,32),
            'slideshow' => $slideshows, #轮播图
            'subtitle' => mb_substr($item['name'],0,32), #副标题,
        ];

        #写入数据
        $data = array_merge($data, $this->getSku($result));

        $this->write($data);
        return [HTTP_SUCCESS,"success"];
    }
}