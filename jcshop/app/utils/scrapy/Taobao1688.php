<?php

namespace app\utils\scrapy;


use app\shop_admin\service\CommodityService;

class Taobao1688 extends Base
{
    public $apiUrl="/alibaba/detail";
    #https://detail.1688.com/offer/653860986118.html?spm=a262eq.12572798.jsczf959.1.67c52fb1urLydi&&scm=1007.30832.181565.0&pvid=b082e713-1e3a-4ad9-aefc-60a1c1d3a137&object_id=653860986118&scm2=1007.30657.177495.0&pvid2=6bc4a9e8-e0b5-426b-96a5-5d3dcfeaeeac&trackInfo=0_653860986118_0.0678695_0.0_0.0_0.0__1294152

    public function getTypeName($types,$value){


        foreach($types as $name=>$values){
            foreach($values as $v){
                if($v==$value){
                    return $name;
                }
            }
        }
    }
    public function getSku($result)
    {
        $return_data = [];



        $item = $result['data'];
        $price=0;
        if (!empty($item['showPriceRanges'])){
            $price=$item['showPriceRanges'][0]['price'];
        }


        $skus = $item['skuMap'];
        $props = $item['skuProps'];
        $has_sku = empty($skus) ? 0 : 1;
        $sku_props = [];
        $types=[];
        $prices = [];
        foreach ($props as $k => $prop) {
            $attrValue = [];
            foreach ($prop['value'] as $item => $value) {
                $attr = ['name' => $prop['prop'], "value" => $value['name']];
                $types[$prop['prop']][]=$value['name'];
                array_push($attrValue, $attr);
            }
            $arr = [
                'name' => $prop['prop'],
                'has_pic' => ($k == count($props) - 1) ? 1 : 0,
                'attrValue' => $attrValue
            ];
            array_push($sku_props, $arr);

        }


        $shop_sku = [];
        $quantity_total = 0; #总库存
        #整理出所有图片异步下载,不然单个下载性能过低
//        $sku_images = [];
//        foreach ($skus as $sku) {
//            if (!empty($sku['image'])) {
//                $sku_images[] = $sku['image'];
//            }
//        }
//
//        #异步下载
//        $sku_images = images_download($sku_images);

        foreach ($skus as $sku => $attr_value) {
            $a=explode(";",$sku);
            $pvs_arr=[];
            foreach($a as $k1=>$v1){
                $v1=str_replace(["&gt","&lt"],"",$v1);
                $pvs_arr[]=$this->getTypeName($types,$v1).":".$v1;
            }

            $shop_sku_v = [
                "pvs" => implode(",",$pvs_arr),
                "weight" => 0,
                "pic" => '',
                "inventory" => [
                    "original_price" => $attr_value['price'] ?? $price,
                    "sell_price" => $attr_value['price'] ?? $price,
                    "total" => 100
                ]
            ];
            $quantity_total+=100;
            array_push($prices, $attr_value['price'] ?? $price);
            array_push($shop_sku, $shop_sku_v);
        }

        if (count($prices) > 0) {
            $return_data['max_price'] = max($prices);
            $return_data['min_price'] = min($prices);
            $avg_price = number_format(array_sum($prices) / count($prices), 2);
            $return_data['sell_price'] = $avg_price;
            $return_data['original_price'] = $avg_price;
        }
        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $quantity_total, 'has_sku' => $has_sku, 'has_attach' => 1, 'has_parameter' => 0]);

        return $return_data;
    }

    public function getDescHtml($item)
    {
        $html = "";
        $images = $item['descImgs'] ?? [];


        $img_arr = images_download($images);

        foreach ($img_arr as $image) {
            $url = $this->oss_url . '/' . $image;
            $html .= "<p><img class='wscnph' style='display: block; margin-left: auto; margin-right: auto;' src='$url'></p>";
        }
        if (empty($html)) {
            $html .= "<p>无详情</p>";
        }
        return $html;
    }


    public function scrapy()
    {
//        $goods_id=$this->getId();
//        if(!$goods_id){
//            return '解析异常';
//        }
//        $tmall_path = public_path() . "1688_1.json";
//        $result = json_decode(file_get_contents($tmall_path), true);
        $result = $this->getData();
        $item = $result['data'];
        $images = $item['images'];

        $img_arr = images_download($images);
        $slideshows = [];
        foreach ($img_arr as $image) {
            array_push($slideshows, ['url' => $image]);
        }
        $data = [
            'content' => $this->getDescHtml($item),
            'mall_id' => $this->mall_id,
            'master' => !empty($slideshows) ? $slideshows[0]['url'] : '',
            'name' => mb_substr($item['title'],0,32),
            'slideshow' => $slideshows, #轮播图
            'subtitle' => mb_substr($item['title'],0,32), #副标题,

        ];


        #$sku_data = ['sku' => [], 'total' => 0, 'has_sku' => 0, 'sku_value' => [], 'has_attach' => 0, 'has_parameter' => 0];
        $data = array_merge($data,$this->getSku($result));

        $this->write($data);
        return [HTTP_SUCCESS, "success"];
    }
}