<?php

namespace app\utils\scrapy;


use app\shop_admin\service\CommodityService;

class Pdd extends base
{
    public $apiUrl="/pdd/detail";

    #https://mobile.pinduoduo.com/goods.html?goods_id=349418420304&_oak_gallery=https%3A%2F%2Fimg.pddpic.com%2Fmms-material-img%2F2022-04-13%2F48e0e066-8866-48ff-a58c-96a5cf5cc627.png&_x_query=%E6%89%8B%E6%9C%BA&refer_page_el_sn=99369&refer_rn=&refer_page_name=search_result&refer_page_id=10015_1653894845418_7atesqqv0w&refer_page_sn=10015
    #https://mobile.pinduoduo.com/goods.html?goods_id=356544759512&_oc_trace_mark=199&_oc_adinfo=eyJzY2VuZV9pZCI6MH0%3D&_oc_refer_ad=1&_oak_gallery=https%3A%2F%2Ft16img.yangkeduo.com%2Fgarner-api%2F4b9f01f273582d10c33ea665613cc869.jpeg&_x_query=%E6%95%B0%E7%A0%81&refer_page_el_sn=99369&refer_rn=&refer_page_name=search_result&refer_page_id=10015_1653894894625_i9bo37fu34&refer_page_sn=10015
    public function getSku($result)
    {
        $return_data = [];
        $item = $result['data']['item'];

        $skus = $item['skus'];

        $has_sku = empty($skus) ? 0 : 1;
        $sku_props = [];
        $prices = [];
        $types=[];
        $shop_sku = [];
        #整理出所有图片异步下载,不然单个下载性能过低
        $sku_images = [];

        $props=[];
        $sku_props=[];
        foreach ($skus as $sku) {
            if (!empty($sku['thumbUrl'])) {
                $sku_images[] = $sku['thumbUrl'];
            }
            foreach($sku['specs'] as $item1=>$value){
                $props[$value['spec_key']][]=$value['spec_value'];
            }
        }
        $n=1;

        foreach($props as $k=>$sku_key){
            $sku_v= array_unique($sku_key);
            $attrValue=[];
            foreach($sku_v as $v){
                $attr=['name' => $k, "value" => $v];
                array_push($attrValue,$attr);
            }
            array_push($sku_props,[
                'name' => $k,
                'has_pic' => $n==count($props) ? 1 : 0,
                'attrValue' => $attrValue
            ]);
            $n=$n+1;
        }

        $quantity_total = 0; #总库存
        #异步下载
        $sku_images = images_download($sku_images);
        foreach ($skus as $k => $sku) {
            $pvs_arr=[];
            foreach($sku['specs'] as $item1=>$value){
                $pvs_arr[]=$value['spec_key'].":".$value['spec_value'];
            }
            $pvs_str=implode(",",$pvs_arr);
            $shop_sku_v = [
                "pvs" => $pvs_str, #"颜色:红色,尺码:L",
                "weight" => 0,
                "pic" => isset($sku_images[$sku['thumbUrl']]) ? $sku_images[$sku['thumbUrl']] : '',
                "inventory" => [
                    "original_price" => $sku['normalPrice'],
                    "sell_price" => $sku['normalPrice'],
                    "total" => $sku['quantity'] ?? 0
                ]
            ];
            $quantity_total=$quantity_total+$sku['quantity'];
            array_push($prices, $sku['normalPrice']);
            array_push($shop_sku, $shop_sku_v);
        }

        if (count($prices) > 0) {
            $return_data['max_price'] = max($prices);
            $return_data['min_price'] = min($prices);
            $avg_price = number_format(array_sum($prices) / count($prices), 2);
            $return_data['sell_price'] = $item['price'] ?? 0;
            $return_data['original_price'] = $item['originalPrice'] ?? 0;
        }
        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $quantity_total, 'has_sku' => $has_sku, 'has_attach' => 1, 'has_parameter' => 0]);

        return $return_data;
    }

    public function getDescHtml($item)
    {
        $html = "";
        $images = array_column($item['detail'],'url');

        $img_arr = images_download($images);

        foreach ($img_arr as $image) {
            $url = $this->oss_url . '/' . $image;
            $html .= "<p><img class='wscnph' style='display: block; margin-left: auto; margin-right: auto;' src='$url'></p>";
        }
        return $html;
    }


    public function scrapy()
    {

        $result = $this->getData();
        $item = $result['data']['item'];

        $images = array_merge([$item['thumbUrl']],$item['banner']);

        $img_arr = images_download($images);
        $slideshows = [];
        foreach ($img_arr as $image) {
            array_push($slideshows, ['url' => $image]);
        }

        $data = [
            'content' => $this->getDescHtml($item),
            'mall_id' => $this->mall_id,
            'master' => !empty($slideshows) ? $slideshows[0]['url'] : '',
            'name' => mb_substr($item['goodsName'],0,32),
            'slideshow' => $slideshows, #轮播图
            'subtitle' => mb_substr($item['goodsName'],0,32), #副标题,
        ];

        #写入数据
        $data = array_merge($data, $this->getSku($result));
        $data['min_price']=$item['minNormalPrice'] ?? $data['min_price'];
        $data['max_price']=$item['maxGroupPrice'] ?? $data['max_price'];

        $this->write($data);
        return [HTTP_SUCCESS,"success"];
    }
}