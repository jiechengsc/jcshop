<?php

namespace app\utils\scrapy;


use app\shop_admin\service\CommodityService;

class Suning extends Base
{
    public $apiUrl="/suning/detail";
    #https://product.suning.com/0070688515/12362165160.html?safp=d488778a.13701.productWrap.10&safc=prd.0.0&safpn=10007
    #https://product.suning.com/0070243074/103780262.html?safp=d488778a.13701.productWrap.10&safc=prd.0.0&safpn=10007
    public function getSku($result)
    {
        $price=$result['data']['price'];
        if (isset($result['data']['passSubList'])) {
            return $this->getSku2($result);
        } elseif (isset($result['data']['ItemClusterDisplayVO'])) {
            return $this->getSku1($result);
        } else {
            return ['sku' => [], 'total' => 0, 'has_sku' => 0, 'sku_value' => [], 'has_attach' => 0, 'has_parameter' => 0,'max_price'=>$price,'min_price'=>$price,'original_price'=>$price,'sell_price'=>$price];
        }
    }

    # suning 接口获取过来的数据 sku有多类暂时只适配2种
    public function getSku1($result)
    {
        $item = $result['data']['ItemClusterDisplayVO'];
        #这好像是调用关系
        $clusterMap = $item['clusterMap'];
        $price = $result['data']['price'] ?? 0;
        #第一层
        $colorList = $item['colorList'];
        #第二层
        $versionList = $item['versionList'];
        $colors = [];
        $attr_type = "";
        $attrValue = [];
        $sku_props = [];
        $attrValue = [];
        $shop_sku = [];
        if (!empty($versionList)){
            foreach ($versionList as $version_k => $version_v) {
                $colors[$version_v['characterValueId']] = ['type' => $version_v['characterName'], 'value' => $version_v['characterValueName']];
                $attrValue[] = ['name' => $version_v['characterName'], 'value' => $version_v['characterValueName']];
                $attr_type = $version_v['characterName'];
            }
            array_push($sku_props, [
                'name' => $attr_type,
                'has_pic' => 0,
                'attrValue' => $attrValue
            ]);
        }

        $total = 0;
        $attrValue = [];
        foreach ($colorList as $item1 => $value1) {
            $colors[$value1['characterValueId']] = ['type' => $value1['characterName'], 'value' => $value1['characterValueName']];
            if(isset($clusterMap[$value1['characterValueId']])){
                foreach ($clusterMap[$value1['characterValueId']] as $item2 => $value2) {

                    $version_arr = $colors[$item2];
                    $pvs_str = $value2['characterName'] . ":" . $value2['characterValueName'] . "," . $version_arr['type'] . ":" . $version_arr['value'];
                    $shop_sku_v = [
                        "pvs" => $pvs_str, #"颜色:红色,尺码:L",
                        "weight" => 0,
                        "pic" => '',
                        "inventory" => [
                            "original_price" => $price,
                            "sell_price" => $price,
                            "total" => 1#$sku['quantity']
                        ]
                    ];
                    array_push($shop_sku, $shop_sku_v);

                }
            }else{
                $pvs_str=$value1['characterName'].":".$value1['characterValueName'];
                $shop_sku_v = [
                    "pvs" => $pvs_str, #"颜色:红色,尺码:L",
                    "weight" => 0,
                    "pic" => '',
                    "inventory" => [
                        "original_price" => $price,
                        "sell_price" => $price,
                        "total" => 1#$sku['quantity']
                    ]
                ];
                array_push($shop_sku, $shop_sku_v);
            }
            $total = $total + 1;

            $attrValue[] = ['name' => $value1['characterName'], 'value' => $value1['characterValueName']];
            $attr_type = $value1['characterName'];
        }
        array_push($sku_props, [
            'name' => $attr_type,
            'has_pic' => 0,
            'attrValue' => $attrValue
        ]);
        $return_data['max_price'] = $price;
        $return_data['min_price'] = $price;
        $return_data['sell_price'] = $price;
        $return_data['original_price'] = $price;

        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $total, 'has_sku' => 1, 'has_attach' => 1, 'has_parameter' => 0]);

        return $return_data;

    }

    public function getSku2($result)
    {
        $return_data = [];
        $item = $result['data']['passSubList'];
        $has_sku = empty($item) ? 0 : 1;
        $sku_props = [];
        $shop_sku = [];
        $prices = [];
        $k = [];
        $types = [];
        foreach ($item as $k => $prop) {
            foreach ($prop as $k1 => $v1) {
                $attrValue = [];
                foreach ($prop[$k1] as $item1 => $value1) {
                    $attr = ['name' => $value1['characterDisplayName'], "value" => $value1['characterValueDisplayName']];
                    array_push($attrValue, $attr);
                    $types[$value1['characterDisplayName']][] = $value1['characterValueDisplayName'];

                }
                $arr = [
                    'name' => $k1,
                    'has_pic' => 0,
                    'attrValue' => $attrValue
                ];
                array_push($sku_props, $arr);
            }
            break;
        }
        $skus = $result['data']['charPartNumbers'][0];
        $price = $result['data']['price'] ?? 0;
        $total = 0;
        foreach ($skus as $sku) {
            $pvs = $sku['itemName'];
            $arr = explode(" ", $pvs);
            $pvs_arr = [];
            for ($i = 1; $i < count($arr); $i++) {
                foreach ($types as $k2 => $v2) {
                    if (in_array($arr[$i], $v2)) {
                        $pvs_arr[] = $k2 . ":" . $arr[$i];
                    }
                }
            }

            $pvs_str = implode(",", $pvs_arr);
            $shop_sku_v = [
                "pvs" => $pvs_str, #"颜色:红色,尺码:L",
                "weight" => 0,
                "pic" => '',
                "inventory" => [
                    "original_price" => $price,
                    "sell_price" => $price,
                    "total" => 1#$sku['quantity']
                ]
            ];
            $total = $total + 1;

            array_push($shop_sku, $shop_sku_v);
        }


        $return_data['max_price'] = $price;
        $return_data['min_price'] = $price;
        $return_data['sell_price'] = $price;
        $return_data['original_price'] = $price;

        $return_data = array_merge($return_data, ['sku' => $shop_sku, 'sku_value' => $sku_props, 'total' => $total, 'has_sku' => $has_sku, 'has_attach' => 1, 'has_parameter' => 0]);

        return $return_data;
    }

    public function getDescHtml($item)
    {


        $html = "";
        $desc = $item['desc'];

        preg_match_all("&\<img  src2=\"(.*?)\" alt=\"\" />&", $desc, $re);
        if (empty($re[1])) {
            return '';
        }
        $images = $re[1];
        $img_arr = images_download($images);

        foreach ($img_arr as $image) {
            $url = $this->oss_url . '/' . $image;
            $html .= "<p><img class='wscnph' style='display: block; margin-left: auto; margin-right: auto;' src='$url'></p>";
        }
        return $html;
    }




    public function scrapy()
    {
//        $goods_id=$this->getId();
//        if(!$goods_id){
//            return '解析异常';
//        }
//        $tmall_path = public_path() . "suning3.json";
//        $result = json_decode(file_get_contents($tmall_path), true);
        $result = $this->getData();

        $item = $result['data'];
        $images = $item['images'];

        $img_arr = images_download($images);
        $slideshows = [];
        foreach ($img_arr as $image) {
            array_push($slideshows, ['url' => $image]);
        }


        $data = [
            'content' => $this->getDescHtml($item),
            'mall_id' => $this->mall_id,
            'master' => !empty($slideshows) ? $slideshows[0]['url'] : '',
            'name' => mb_substr($item['title'], 0, 32),
            'slideshow' => $slideshows, #轮播图
            'subtitle' => mb_substr($item['title'], 0, 32), #副标题,
        ];

        #写入数据
        $data = array_merge($data, $this->getSku($result));

        $this->write($data);
        return [HTTP_SUCCESS, "success"];
    }
}