<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */

namespace app\utils;

use think\facade\Cache;
use think\facade\Db;
use think\facade\Env;

class Addons
{

    /**
     * 校验插件是否可用
     * @param $mallId int 商城id
     * @param $addons int 插件id
     * @return bool true:允许|false:不允许
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function check1($mallId, $addons)
    {

        $profiles = Env::get("profiles");
        if ($profiles === 'dev') {
            return true;
        }

        if (Cache::store('redis')->has("use_addons_list_" . $mallId)) {
            $idList = Cache::store('redis')->get("use_addons_list_" . $mallId);
        } else {
            $idList = Db::table("jiecheng_lessee")
                ->where('id', $mallId)
                ->value('addons');
            if (!$idList) {
                Cache::store('redis')->set("use_addons_list_" . $mallId, null);
                return false;
            }
            $idList = json_decode($idList, true);
            Cache::store('redis')->set("use_addons_list_" . $mallId, $idList);
//            $idList = Db::table("jiecheng_addons")
//                ->where(['mall_id' => $mallId])
//                ->column('addons_id');
//            Cache::store('redis')->set("use_addons_list_" . $mallId, $idList);
        }

        if (!in_array($addons, $idList))
            return false;

        if (Cache::store('redis')->has("addons_license_{$mallId}_{$addons}")) {
            $license = Cache::store('redis')->get("addons_license_{$mallId}_{$addons}");
        } else {
            $license = Db::table("jiecheng_saas_license")
                ->where(['name' => "addons", 'addons_id' => $addons])
                ->value('license');
            if (empty($license))
                return false;
            Cache::store('redis')->set("addons_license_{$mallId}_{$addons}", $license);
        }

        $str = Cache::store('redis')->get($_SERVER['HTTP_HOST'] . "GLOBAL_STR");
        $data = [
            "user" => $str,
            "addons" => $addons,
        ];
        sort($data);
        if ($license != hash('sha256', json_encode($data)))
            return false;

        return true;
    }
    /**
     * 校验插件是否可用
     * @param $mallId int 商城id
     * @param $addons int 插件id
     * @return bool true:允许|false:不允许
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public static function check($mallId, $addons)
    {
        return true;
        $profiles = Env::get("profiles");
        if ($profiles === 'dev') {
            return true;
        }
        if (Cache::store('redis')->has("use_addons_list_".$mallId)) {

            $idList = Cache::store('redis')->get("use_addons_list_".$mallId);

        } else {
  
            $idList = Db::table("jiecheng_lessee")
                ->where('id', $mallId)
                ->value('addons');

            if (!$idList) {
                Cache::store('redis')->set("use_addons_list_" . $mallId, null,60);
                return false;
            }
            $idList = json_decode($idList, true);
            Cache::store('redis')->set("use_addons_list_" . $mallId, $idList,120);
//            $idList = Db::table("jiecheng_addons")
//                ->where(['mall_id' => $mallId])
//                ->column('addons_id');
//            Cache::store('redis')->set("use_addons_list_" . $mallId, $idList);
        }

        if (!in_array($addons, $idList))
            return false;

        // if (Cache::store('redis')->has("addons_license_{$mallId}_{$addons}")) {
        //     $license = Cache::store('redis')->get("addons_license_{$mallId}_{$addons}");
        // } else {
        //     $license = Db::table("jiecheng_saas_license")
        //         ->where(['name' => "addons", 'addons_id' => $addons])
        //         ->value('license');
        //     if (empty($license))
        //         return false;
        //     Cache::store('redis')->set("addons_license_{$mallId}_{$addons}", $license);
        // }

        // $str = Cache::store('redis')->get($_SERVER['HTTP_HOST'] . "GLOBAL_STR");
        // $data = [
        //     "user" => $str,
        //     "addons" => $addons,
        // ];
        // sort($data);
        // if ($license != hash('sha256', json_encode($data)))
        //     return false;

        return true;
    }
}