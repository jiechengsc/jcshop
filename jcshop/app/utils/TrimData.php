<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\utils;

use think\db\Query;
use think\Model;

class TrimData
{

    /**
     * 整理查询字段条件到模型
     * @param $admin
     * @param array $data
     * @param array $fields
     * @return mixed
     */
    public static function searchDataTrim($admin,array &$data,array $fields):Query
    {
        if (isset($data['n_load'])) {
            unset($data['n_load']);
        }
        if (isset($data['begin_date'], $data['end_date']) && in_array('begin_date',$fields) && in_array('end_date',$fields)){

            $admin->withSearch(['create_time'],['create_time'=>[$data['begin_date'] . " 00:00:00", $data['end_date'] . "23:59:59"]]);
            unset($data['begin_date'],$data['end_date']);
        }

        $fields = array_diff($fields,["begin_date","end_date"]);
        if ($fields) {
            foreach ($fields as $v){
                if (isset($data[$v])){
                    $admin->withSearch([$v], [$v => $data[$v]]);
                }
            }
            $data = array_diff_key($data, array_flip($fields));
        }

        return $admin;
    }
}