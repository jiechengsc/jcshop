<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\utils;

use app\index\model\Order;
use app\index\model\User;
use think\facade\Db;

/*
errcode 的合法值

值	说明	最低版本
-1	系统繁忙，请稍后重试
40002	暂无生成权限（非个人主体小程序无权限）
40013	生成权限被封禁
85079	小程序未发布
40165	参数 path 填写错误
40212	参数 query 填写错误
85401	参数expire_time填写错误，时间间隔大于1分钟且小于31年
85402	参数env_version填写错误
44990	生成URL Link频率过快（超过200次/秒）
44993	单天生成Scheme+URL Link数量超过上限50万
*/

class UrlLink
{

    /**
    获取小程序 URL Link，适用于短信、邮件、网页、微信内等拉起小程序的业务场景。目前仅针对国内非个人主体的小程序开放，详见获取 URL Link
     */
    public static function generate_urllink(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxa/generate_urllink?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }
        return $result;
    }
    /**
    获取小程序 URL Link，适用于短信、邮件、网页、微信内等拉起小程序的业务场景。目前仅针对国内非个人主体的小程序开放，详见获取 URL Link
     */
    public static function generate_scheme(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxa/generatescheme?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }
        return $result;
    }
}