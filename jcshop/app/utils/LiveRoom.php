<?php


namespace app\utils;

use app\index\model\Order;
use app\index\model\User;
use think\facade\Db;

class LiveRoom
{

    /**
     * 创建直播间
     * 直播间管理接口，是小程序直播提供给开发者对直播房间进行批量操作的接口能力。 开发者可以创建直播间、获取直播间信息、获取直播间回放以及往直播间导入商品
     */
    public static function create(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/create?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 获取直播间列表
     * 调用此接口获取直播间列表及直播间信息 | 调用接口获取已结束直播间的回放源视频（一般在直播结束后10分钟内生成，源视频无评论等内容）
     */
    public static function getLiveInfo(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 添加商品
     * 调用接口往指定直播间导入已入库的商品
     */
    public static function addGoods(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/addgoods?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 获取分享二维码
     * @param array $data
     * @return \Exception|mixed
     */
    public static function getShareCode(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('get', 'https://api.weixin.qq.com/wxaapi/broadcast/room/getsharedcode?access_token=' . $token . '&' . http_build_query($data));
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 获取直播间回放
     * 调用此接口获取直播间列表及直播间信息 | 调用接口获取已结束直播间的回放源视频（一般在直播结束后10分钟内生成，源视频无评论等内容）
     */
    public static function getReplay(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxa/business/getliveinfo?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }


    /**
     * 删除直播间
     */
    public static function deleteRoom(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/deleteroom?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 编辑直播间
     */
    public static function editRoom(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/editroom?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 获取直播间推流地址
     */
    public static function getPushUrl(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/getpushurl?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 开启/关闭直播间官方收录
     */
    public static function updateFeedPublic(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/updatefeedpublic?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 开启/关闭回放功能
     */
    public static function updateReplay(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/room/updatereplay?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 上下架商品
     */
    public static function goodsOnSale(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/goods/onsale?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 删除在直播间的商品
     */
    public static function delInRoomGoods(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/goods/deleteInRoom?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

    /**
     * 推送商品
     */
    public static function pushGoods(array $data)
    {
        try {
            $token = Wechat::getAccessToken();
            $result = wx_request('post', 'https://api.weixin.qq.com/wxaapi/broadcast/goods/push?access_token=' . $token, $data,
                [
                    'Content-Type' => 'application/json'
                ]
            );
        } catch (\Exception $exception) {
            $result = $exception;
        }

        return $result;
    }

}