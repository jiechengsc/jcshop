<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class CommissionAssessmentWithdraw extends BaseAction
{
    private $service_name = 'CommissionAssessmentWithdrawService';
    private $agent_id = 0;
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $this->user = \app\index\model\User::find($this->user['id']);
        $this->agent = \app\index\model\Agent::where('user_id',$this->user->id)->find();
        if (empty($this->agent))
            throw new Exception("请先升级为推广员", HTTP_INVALID);
    }


    public function total()
    {
        $amount = input('put.amount');
        $remark = input('put.remark');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $amount, $remark);
        return json(['msg' => $msg], $code);
    }


    public function weixin(){
        $amount = input('put.amount');
        $remark = input('remark',null);
        list($code, $msg) = ServiceFactory::getResult($this->service_name, 'wx', $amount,$remark ?? '');
        return json(['msg' => $msg], $code);
    }

    public function save(): Json
    {
        $amount = input('amount');
        $remark = input('remark');
        list($code, $withdraw) = ServiceFactory::getResult($this->service_name, 'save', $this->user['id'], $amount, $remark);
        return json(['msg' => $withdraw], $code);
    }

    public function findAll(): Json
    {
        $page = input('page');
        $size = input('size');
        $status = input('status',null);
        list($code, $list) = ServiceFactory::getResult($this->service_name, 'findAll', $this->user['id'],$status, $page, $size);
        return json(['msg' => $list], $code);
    }


    public function read(): Json
    {
        $id = input('id');
        list($code, $info) = ServiceFactory::getResult($this->service_name, 'read', $id);
        return json(['msg' => $info], $code);
    }

    public function apply(){
        $data = input('post.');
        list($code,$data) = ServiceFactory::getResult($this->service_name,'apply',(array)$data);
        return json(['msg' => $data],$code);
    }
}