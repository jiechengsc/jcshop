<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\model\Configuration;
use app\index\service\ServiceFactory;
use app\utils\RedisUtil;
use think\App;
use think\Exception;
use app\index\model\Order;


class Payment extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
    }

    public function pay()
    {
        $orderList = input("post.orderList");
        if (is_null($orderList)) {
            throw new Exception("订单号", HTTP_INVALID);
        }
        $orderList = explode(',', $orderList);
        $res = Order::with('orderCommodity')->where('order_no', $orderList[0])->find();
        foreach ($res['orderCommodity']->toArray() as $key => $value) {
            $redis = new RedisUtil();
            $commodityData = $redis->get(checkRedisKey('COMMODITY:' . $value['commodity_id']));
            if (empty($commodityData)) {
                Order::update(['status' => 12], ['order_no' => $orderList]);
                return json(['msg' => "商品不存在，订单已失效！"], HTTP_NOTACCEPT);
            }
        }

        
        $type = input('post.type');
        switch ($type) {
            case 1:
                $method = "balance";
                break;
            case 2:
                $method = "wxpay";
                break;
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", $method, $orderList);
        return json(['msg' => $msg], $code);
    }


    public function recharge()
    {
        $money = input("post.money");
        if (empty($money)) {
            return json(['msg' => "金额不能为空"], HTTP_NOTACCEPT);
        }
        $money = intval(bcmul((string) number_format(floatval(input("post.money")), 2,".",""), (string)100));
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__,
            $money);
        return json(['msg' => $msg], $code);
    }


    public function membership()
    {
        $memberLevelConfig = Configuration::where(['type' => 'memberLevel'])->value('configuration');
        if (!$memberLevelConfig) {
            throw new Exception("未开启会员等级", HTTP_NOTACCEPT);
        }
        $memberLevelConfig = json_decode($memberLevelConfig, true);
        if (empty($memberLevelConfig['is_open'])) {
            throw new Exception("未开启会员等级", HTTP_NOTACCEPT);
        }

        $level = input("post.level");
        $type = input('post.type');
        switch ($type) {
            case 1:
                $method = "membershipBalance";
                break;
            case 2:
                $method = "membershipWxpay";
                break;
            default:
                $method = "membershipWxpay";
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . "Service", $method,
            $level);
        return json(['msg' => $msg], $code);
    }

}
