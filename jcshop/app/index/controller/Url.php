<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


class Url
{
    public static $CODE_URL = 'https://api.weixin.qq.com/sns/jscode2session?';
    public static $LOCAL_URL = 'https://open.weixin.qq.com/connect/oauth2/authorize?';
    public static $OAUTH_URL = 'https://api.weixin.qq.com/sns/userinfo?access_token=';
    public static $TOKEN = 'https://api.weixin.qq.com/cgi-bin/token?';
    public static $Gz = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=";
    public static $ACCESS_TOKEN = "https://api.weixin.qq.com/sns/oauth2/access_token?";
    public static $USERINFO_URL = 'https://api.weixin.qq.com/cgi-bin/user/info?';
    public static $REFRRESH_TOKEN = "https://api.weixin.qq.com/sns/oauth2/refresh_token?";
    public static $OAUTH_URL1 = 'https://api.weixin.qq.com/sns/oauth2/access_token?';
    public static $FOLLOW = "https://api.weixin.qq.com/cgi-bin/user/get?access_token=";

}