<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\App;
use think\Exception;


class WxTool extends BaseAction
{


    public function sign()
    {
        if ($this->baseIn !== 'wechat')
            throw new Exception("不支持调用", HTTP_INVALID);
        $frontUrl = input("post.front");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $frontUrl);
        return json(['msg' => $msg], $code);
    }

    public function subscribeMessage($type)
    {
        if ($this->baseIn !== 'wechata') {
            throw new Exception("不支持调用", HTTP_INVALID);
        }
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $type);
        return json(['msg' => $msg], $code);
    }


    public function getSubcribeMessageResult()
    {
        if ($this->baseIn !== 'wechata')
            throw new Exception("不支持调用", HTTP_INVALID);
        $data = input("post.data");
        $data = json_decode($data, true);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    /**
     * 检查多端配置
     * @return \think\response\Json
     */
    public function check()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}
