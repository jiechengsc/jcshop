<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;

class Invoice extends BaseAction
{

    public function create()
    {
        $post = input('post.');
        global $mid, $user;
        $post['mall_id'] = $mid;
        $post['user_id'] = $user['id'];
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $result], $code);
    }


    public function editInvoice()
    {
        $data = input('put.');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $result], $code);
    }


    public function info($id)
    {
        //$get = input('get.id');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $result], $code);
    }

 
    public function list()
    {
        $get = input('get.');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $result], $code);
    }

    public function riseDel($id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return is_array($code) ? json(['msg' => $code[1]], $code[0]) : response()->code($code);
    }

    public function setRiseDefault()
    {
        $id = input('post.id');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $result], $code);
    }

    public function riseRead()
    {
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $result], $code);
    }

    public function riseList()
    {
        $get = input('get.');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $result], $code);
    }

    public function riseCreate()
    {
        $post = input('post.');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $post);
        return json(['msg' => $result], $code);
    }


    public function riseUp()
    {
        $put = input('put.');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $put);
        return json(['msg' => $result], $code);
    }

    public function riseGain()
    {
        $id = input('get.id');
        list($code, $result) = ServiceFactory::getResult(class_basename($this) . "Service", __FUNCTION__, $id);
        return json(['msg' => $result], $code);
    }

}