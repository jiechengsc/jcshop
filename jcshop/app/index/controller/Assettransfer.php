<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;


class Assettransfer extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->mid, 35);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function sendAmount()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function sendIntegral()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function userList()
    {

        $page = $this->request->get('page', 1);
        $size = $this->request->get('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data ?? [], $page, $size);
        return json(['msg' => $msg], $code);
    }
}
