<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;

class Accept extends BaseAction
{


    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);

        $check = Addons::check($this->mid, 11);
        if (!$check)
            throw new Exception("请先激活门店插件", HTTP_NOTACCEPT);
    }

    public function index()
    {
        $data = input("get.");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    public function confirm()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    public function wxaQr()
    {
        global $baseIn;
        if ($baseIn !== "wechata"){
            throw new Exception("错误",HTTP_NOTACCEPT);
        }
        $wxa = input('get.wxa');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $wxa);
        return json(['msg' => $msg], $code);
    }

    public function LotteryGoodsList(){
        $ids = input('ids');
        list($code,$list) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$ids);
        return json(['msg' => $list],$code);
    }

    public function LotteryConfirm(){
        $ids = input('ids');
        list($code,$msg) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,$ids);
        return json(['msg' => $msg],$code);
    }

    public function integralOrderList(){
        $ids = input('ids');
        list($code,$msg) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,(int)$ids);
        return json(['msg' => $msg],$code);
    }

    public function integralOrderConfirm(){
        $ids = input('ids');
        list($code,$msg) = ServiceFactory::getResult(class_basename($this).'Service',__FUNCTION__,(int)$ids);
        return json(['msg' => $msg],$code);
    }
}