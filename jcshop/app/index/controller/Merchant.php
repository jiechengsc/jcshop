<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;


class Merchant extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->mid, 12);
        $url1 = request()->pathinfo();
        $str = substr($url1, strripos($url1, '/') + 1);
        $url = is_numeric($str) ? substr($url1, 0, strripos($url1, "/")) : $url1;
        if (!$check && $url != 'merchant/commodity')
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }


    public function getMerchantAndCommodityByIds()
    {
        $check = Addons::check($this->mid, 12);
        if (!$check)
            return json(['msg' => []], HTTP_SUCCESS);
        $ids = input("post.ids");
        $ids = json_decode($ids, true);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $ids);
        return json(['msg' => $msg], $code);
    }

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}
