<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;

class Distribution extends BaseAction
{

    public function store()
    {
        $post = input('post.');
        $post['mid'] = request()->header('mid');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }
}