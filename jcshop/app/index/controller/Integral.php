<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;

class Integral extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        if (empty($this->user))
            throw new Exception('请先登录',HTTP_UNAUTH);
    }


    public function share_integral(){
        $is_bool = Addons::check($this->mid,6);
        if (!$is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $user_id = input('user_id','');
        list($code,$result) = ServiceFactory::getResult('IntegralService','share_integral',$user_id,$this->user['id']);
        return json(['msg' => $result],$code);
    }

    public function integral_list(){
        $is_bool = Addons::check($this->mid,7);
        if (!$is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $post = input('post.');
        $page = input('page');
        $size = input('size');
        list($code,$result) = ServiceFactory::getResult('IntegralService','list',$this->user['id'],$post,$page,$size);
        return json(['msg' => $result],$code);
    }

    public function integral_judge(){
        $is_bool = Addons::check($this->mid,7);
        if (!$is_bool)return json(['msg' => []],HTTP_SUCCESS);
        $commodity = input('commodity','');
        $total_price = input('total_price',0);
        $coupon = input('coupon',[]);
        list($code,$result) = ServiceFactory::getResult('IntegralService','integral_judge',$commodity,$this->user['id'],$total_price,$coupon,$this->mid);
        return json(['msg' => $result],$code);
    }
}