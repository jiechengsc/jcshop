<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;

use app\index\model\CommodityTotal;
use app\index\service\ServiceFactory;
use app\index\util\Redis;
use think\Exception;

class Commodity extends BaseAction
{


    public function findByIds()
    {
        $ids = input("post.ids");
        if (empty($ids)) {
            throw new Exception("id必传",HTTP_NOTACCEPT);
        }
        $ids = json_decode($ids, true);
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $ids);
        return json(['msg' => $msg], $code);

    }

    public function goodsList()
    {

        $data = input('post.');
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function goodsInfo()
    {
        $post = input('id');
        CommodityTotal::where(['commodity_id'=>$post])->inc('click')->save();
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }

    public function inventory($commodityId)
    {
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $commodityId);
        return json(['msg' => $msg], $code);
    }

    public function evaluate()
    {
        $get = input('get.');
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $get);
        return json(['msg' => $msg], $code);
    }

    public function getManyGoods()
    {
        $data = input('post.cids');
        $type = input("post.type");
        $js = is_array($data)?$data:json_decode($data,true);
        list($code,$msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $js, $type);
        return json(['msg' => $msg], $code);
    }

    public function commodity(int $commodity)
    {
        list($code,$commission) = ServiceFactory::getResult('CommissionService', 'commodityDistribution', $commodity);
        return json(['msg' => $commission],$code);
    }
}