<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;

class LiveRoom extends BaseAction
{

    //直播间列表
    public function list()
    {
        $page = input('page', 1);
        $size = input('size', 10);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $page, $size);
        return json(['msg' => $msg], $code);
    }
}