<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\App;
use app\utils\Addons;

/**
 * 微信客服控制器
 * @package app\sadmin\controller
 */
class WxCs extends BaseAction
{
    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->mid, 22);
        if (!$check)
            throw new Exception("请先激活微信客服插件", HTTP_NOTACCEPT);
    }

    /**
     * 微信客服
     * showdoc
     * @catalog 微信/工具
     * @title 微信客服
     * @description 微信客服的接口
     * @method get
     * @url /wx/cs/index
     * @header Authorization 必选 string 设备token
     * @return
     * @return_param msg json 对象信息,没有为空
     * @remark 这里是备注信息
     * @number 1
     */
    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    /**
     * 获取客服信息
     * @param int $id
     * @return \think\response\Json
     */
    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $id);
        return json(['msg' => $msg], $code);
    }
}
