<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;


class UserCoupon extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $check = Addons::check($this->mid, 3);
        $url1 = request()->pathinfo();
        $str = substr($url1, strripos($url1, '/') + 1);
        $url = is_numeric($str) ? substr($url1, 0, strripos($url1, "/")) : $url1;
        if (!$check && $url != 'coupon/commodity')
            throw new Exception("请先激活优惠券插件", HTTP_NOTACCEPT);
    }

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        $status = $this->request->post('status');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $status, $page, $size);
        return json(['msg' => $msg], $code);
    }

    public function findByCommodity()
    {
        $check = Addons::check($this->mid, 3);
        if (!$check)
            return json(['msg' => [],HTTP_SUCCESS]);
        $data = input("post.data");
        $data = json_decode($data, true);
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

    public function findByOrder($order)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $order);
        return json(['msg' => $msg], $code);
    }

    public function findAllByOrder()
    {
        $orderList = input("post.orderList");

        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $orderList);
        return json(['msg' => $msg], $code);
    }

    public function save(Request $request)
    {
        $couponId = $request->post("coupon_id");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $couponId);
        return json(['msg' => $msg], $code);
    }
    
    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}
