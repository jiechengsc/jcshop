<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\App;
use think\Exception;
use think\Request;


class AgentApply extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user) {
            throw new Exception('请先登录', HTTP_UNAUTH);
        }
        $agent_id = \app\index\model\User::where('id',$this->user['id'])->value('agent_id');
        if (!empty($agent_id)) {
            throw new Exception('已经是分销商,无需再发起申请', HTTP_NOTACCEPT);
        }
    }

    public function save(Request $request)
    {
        $postData = $request->post();
        $data = [
            "name" => $postData['name'],
            "mobile" => $postData['mobile']
        ];
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }


    public function read()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function update(int $id)
    {
        $putData = $this->request->put();
        $data = [];
        isset($putData['name']) && $data['name'] = $putData['name'];
        isset($putData['mobile']) && $data['mobile'] = $putData['mobile'];
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }

    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code((int)$code);
    }

}
