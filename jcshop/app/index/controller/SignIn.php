<?php


namespace app\index\controller;
use app\index\service\ServiceFactory;
use think\App;
use think\Exception;
class SignIn extends BaseAction
{
    private $is_bool;
    public function __construct(App $app)
    {
        parent::__construct($app);
        global $mid;
        $this->is_bool = \app\utils\Addons::check($mid,5);
    }


    public function sign(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        list($code,$message) = ServiceFactory::getResult('SignInService','sign',$this->user);
        return json(['msg' => $message],$code);
    }

    public function list(){
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $start_time = input('start_time');
        $end_time = input('end_time');
        list($code,$list) = ServiceFactory::getResult('SignInService','findAll',$this->user,$start_time,$end_time);
        return json(['msg' => $list],$code);
    }

    public function config(){
        global $mid;
        if (!$this->is_bool)return json(['msg' => []],HTTP_SUCCESS);
        return json([
            'msg' => json_decode(\app\index\model\Configuration::where('type','SignIn')
                ->where('mall_id',$mid)
                ->value('configuration'))
        ],HTTP_SUCCESS);
    }
}