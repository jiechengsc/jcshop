<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;

class Verification extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user)
            throw new Exception("请先登录", HTTP_UNAUTH);
        $check = Addons::check($this->mid, 11);
        if (!$check)
            throw new Exception("请先激活门店插件", HTTP_NOTACCEPT);
    }


    public function index()
    {
        $orderId = input("get.order");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $orderId);
        return json(['msg' => $msg], $code);
    }


    public function qrcode()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }

}