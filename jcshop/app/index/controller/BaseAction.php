<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;

use think\App;
use think\Exception;
use think\facade\Cache;
use think\facade\Db;
use think\facade\Env;

class BaseAction extends \app\BaseController
{
    protected $data;

    protected $user;

    protected $baseIn;

    protected $profiles;

    protected $mid;


    public function __construct(App $app)
    {
        parent::__construct($app);
        $url = request()->pathinfo();
        $dischargedArr = [
            'notify/commodity',
            'notify/recharge',
        ];
        if (!in_array(strtolower($url), $dischargedArr)) {

            global $mid;

            $mid = !array_key_exists('mid', request()->header()) ? input('mall_id') : request()->header('mid');
            if (isset($mid))
                $this->mid = $mid;
            $isOpen = Cache::get(checkRedisKey("MALL_IS_OPEN"));

            $this->profiles = Env::get("profiles");

            if (empty($isOpen)  && $this->profiles !== "dev") {
                if (request()->pathinfo() !== "configuration/mallBase" || !request()->isGet()) {
                    $configuration = \app\index\model\Configuration::where(['type' => "mallBase"])->value("configuration");
                    if (!$configuration)
                        throw new Exception("商城未开启,请稍等!!!", HTTP_NOTACCEPT);
                    $configuration = json_decode($configuration, true);
                    if (!isset($configuration['is_open']) || empty($configuration['is_open']))
                        throw new Exception($configuration['remark'], HTTP_ACCEPTED);
                }
            }

            if ($this->request->action() === 'findAll') {
                $this->data = array_filter($this->request->post(), 'filterArr');
            }

            //获取登陆数据
            global $user, $js_user;
            global $baseIn;
            if ("pro" === $this->profiles) {
                $userType = request()->header('UserType');
                $entryUrl = [
                    'login/getBaseInfo',
                    'login/getUserOpenId',
                    'fitment/float_menu',
                    'wx/tool/check',
                ];
                $user = $this->user = $js_user;
                $uri = request()->pathinfo();
                if ($userType != 3 && !in_array($uri, $entryUrl)) {
                    $entryType = Db::table('jiecheng_lessee')
                        ->where(['status' => 1, 'id' => $mid])
                        ->whereNull('delete_time')
                        ->value('entry_type');
                    if (empty($entryType)) {
                        throw new Exception("暂不支持该端登录!!!", HTTP_NOTACCEPT);
                    }

                    $entryType = json_decode($entryType, true);

                    if (!in_array($userType, $entryType)) {
                        throw new Exception("暂不支持该端登录!!!", HTTP_NOTACCEPT);
                    }
                }

                switch ($userType) {
                    case 1:
                        $this->baseIn = $baseIn = "wechata";
                        break;
                    case 2:
                        $this->baseIn = $baseIn = "wechat";
                        break;
                    case 3:
                        $this->baseIn = $baseIn = "web";
                        break;
                    default:
                        $this->baseIn = $baseIn = "wechat";
                }
            } elseif ("dev" === $this->profiles) {
                //模拟登陆数据
                $mid = 27174;
                $user = \app\index\model\User::find(32464972);
                if (empty($user)) {
                    throw new Exception("本地获取用户失败");
                }
                $this->user = $user = $user->toArray();
                $this->baseIn = $baseIn = "wechat";
            }
        }
    }
}