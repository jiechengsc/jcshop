<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;


class Coupon extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->mid, 3);
        if (!$check)
            throw new Exception("请先激活优惠券插件", HTTP_NOTACCEPT);
    }


    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $page, $size);
        return json(['msg' => $msg], $code);
    }


    public function findById()
    {
        $ids = input("post.ids");
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $ids);
        return json(['msg' => $msg], $code);
    }


    public function findByMerchant($merchantId)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $merchantId);
        return json(['msg' => $msg], $code);
    }


    public function findByCommodity($commodityId)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $commodityId);
        return json(['msg' => $msg], $code);
    }

    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

}
