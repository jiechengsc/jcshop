<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\App;
use think\Exception;
use think\Request;
use app\utils\Addons;

class MerchantApply extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        if (!$this->user) {
            throw new Exception('请先登录', HTTP_UNAUTH);
        }

        $check = Addons::check($this->mid, 12);
        if (!$check)
            throw new Exception("请先激活商户插件", HTTP_NOTACCEPT);
    }


    public function save(Request $request)
    {
        $postData = $request->post();
        $data = [
            "logo" => $postData['logo'],
            "name" => $postData['name'],
            "mobile" => $postData['mobile'],
            "business_license" => $postData['business_license'],
        ];
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $data);
        return json(['msg' => $msg], $code);
    }


    public function read()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function update(int $id)
    {
        $putData = $this->request->put();
        $data = [];
        isset($putData['logo']) && $data['logo'] = $putData['logo'];
        isset($putData['name']) && $data['name'] = $putData['name'];
        isset($putData['mobile']) && $data['mobile'] = $putData['mobile'];
        isset($putData['business_license']) && $data['business_license'] = $putData['business_license'];
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id, $data);
        if ($code != HTTP_CREATED) {
            return json(['msg' => $msg], $code);
        }
        return json(['msg' => "影响了 {$msg} 条数据"], $code);
    }


    public function delete(int $id)
    {
        $code = ServiceFactory::getResult(class_basename($this) . 'Service',
            __FUNCTION__, $id);
        return response()->code((int)$code);
    }

}
