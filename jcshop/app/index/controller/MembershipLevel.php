<?php
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use app\index\model\Configuration;

class MembershipLevel extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        global $mid;
        $check = Addons::check($mid, 20);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);

        $memberLevelConfig = Configuration::where(['type' => 'memberLevel'])->value('configuration');
        if (!$memberLevelConfig) {
            throw new Exception("未开启会员等级", HTTP_NOTACCEPT);
        }
        $memberLevelConfig = json_decode($memberLevelConfig, true);
        if (empty($memberLevelConfig['is_open'])) {
            throw new Exception("未开启会员等级", HTTP_NOTACCEPT);
        }
    }


    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }


    public function read(int $id)
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $id);
        return json(['msg' => $msg], $code);
    }

    public function getLevelData(){
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

}
