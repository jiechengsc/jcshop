<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;

class WeixinApp extends BaseAction
{

    //创建小程序接口
    public function save()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}