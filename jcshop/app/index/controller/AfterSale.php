<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\controller;


use app\index\service\ServiceFactory;

class AfterSale extends BaseAction
{

    public function apply()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);
        return json(['msg' => $msg], $code);
    }


    public function submitExpress()
    {
        $data = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);

        return json(['msg' => $msg], $code);
    }

    public function afterDetails()
    {
        $data = input('get.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);

        return json(['msg' => $msg], $code);
    }


    public function fillExpress()
    {
        $data = input('get.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $data);

        return json(['msg' => $msg], $code);
    }

    public function upImg()
    {
        $files = request()->file('image');
        list($code, $msg) = ServiceFactory::getResult('UpdateImgService', "images", $files);
        return json(['msg' => $msg], $code);
    }


    public function confirmGoods()
    {
        $put = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $put);
        return json(['msg' => $msg], $code);
    }

    public function modifyExpress()
    {
        $put = input('put.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $put);
        return json(['msg' => $msg], $code);
    }

    public function cancelOrder()
    {
        $post = input('post.id');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }

    public function againApply()
    {
        $post = input('post.');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__, $post);
        return json(['msg' => $msg], $code);
    }

}