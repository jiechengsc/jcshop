<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\Request;


class ArticleType extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
        $check = Addons::check($this->mid, 2);
        if (!$check)
            throw new Exception("请先激活插件", HTTP_NOTACCEPT);
    }


    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }

    public function findAll()
    {
        $page = $this->request->get('page');
        $size = $this->request->get('size');
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__,
            $this->data, $page, $size);
        return json(['msg' => $msg], $code);
    }
}
