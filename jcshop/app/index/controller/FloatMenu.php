<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\controller;

use app\index\service\ServiceFactory;

/**
 * 悬浮菜单控制器
 * @package app\sadmin\controller
 */
class FloatMenu extends BaseAction
{

    /**
     * 列表
     * showdoc
     * @catalog 页面装修/悬浮菜单
     * @title 获取
     * @description 获取的接口
     * @method get
     * @url /fitment/bottom_menu
     * @header Authorization 必选 string 设备token
     * @return {"msg":{"name":"底部菜单","type":1,"bgcolor":{"checked":"#FF3C29","default":"#565656"},"shownum":0,"tablist":[{"icon":"el-sc-shouye2","link":"","name":"","text":"首页","type":1},{"icon":"el-sc-shouye2","link":"","name":"","text":"首页","type":1},{"icon":"el-sc-shouye2","link":"","name":"","text":"首页","type":1},{"icon":"el-sc-shouye2","link":"","name":"","text":"首页","type":1}],"iconcolor":{"checked":"#FFFFFF","default":"#FFFFFF"},"num_color":"#FF3C29","textcolor":{"checked":"#212121","default":"#565656"},"line_color":"#FF3C29"}}
     * @return_param msg json 对象信息,没有为空
     * @return_param total int 总记录数
     * @remark 这里是备注信息
     * @number 8
     */
    public function index()
    {
        list($code, $msg) = ServiceFactory::getResult(class_basename($this) . 'Service', __FUNCTION__);
        return json(['msg' => $msg], $code);
    }
}
