<?php


namespace app\index\controller;

use app\index\service\ServiceFactory;
use think\App;
use think\Exception;
use think\response\Json;

class IntegralShop extends BaseAction
{
    protected $class_name = 'IntegralShopSerivce';
    public function __construct(App $app)
    {
        parent::__construct($app);
    }


    public function classList(){
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__);
        return json(['msg' => $list],$code);
    }

    public function goodsList(){
        $page = input('page');
        $size = input('size');
        list($code,$list) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $list],$code);
    }

    public function goodsFind(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }


    public function orderCreate() {
        $id = input('id');
        $sku_id = input('sku_id',null);
        $number = input('number');
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,input('post.'),$id,$number,$sku_id);
        return json(['msg' => $data],$code);
    }

    public function orderFind(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function orderList() {
        $page = input('page');
        $size = input('size');
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$page,$size,input('post.'));
        return json(['msg' => $data],$code);
    }

    public function orderPay(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function orderReceiving(int $id){
        list($code, $data) = ServiceFactory::getResult($this->class_name, __FUNCTION__, $id);
        return json(['msg' => $data], $code);
    }

    public function orderQrcode(int $id){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__,$id);
        return json(['msg' => $data],$code);
    }

    public function storeList(){
        list($code,$data) = ServiceFactory::getResult($this->class_name,__FUNCTION__);
        return json(['msg' => $data],$code);
    }
}