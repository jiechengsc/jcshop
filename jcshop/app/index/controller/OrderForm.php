<?php


namespace app\index\controller;


use app\index\service\ServiceFactory;
use app\Request;
use app\utils\Addons;
use think\App;
use think\Exception;
use think\response\Json;

class OrderForm extends BaseAction
{

    public function __construct(App $app)
    {
        parent::__construct($app);
    }

    public function getTemplates(Request $request) : Json{
        $ids=$request->get('ids',[]);
        list($code,$data) = ServiceFactory::getResult('OrderFormService','getTemplates',$ids);
        return json(['msg' => $data],$code);
    }

    public function getOrderForm(Request $request) :Json{
        list($code,$data) = ServiceFactory::getResult('OrderFormService','getOrderForm');
        return json(['msg' => $data],$code);
    }
}