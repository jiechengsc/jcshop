<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\validate;

use think\Validate;

class WithdrawValidate extends Validate
{
    protected $rule = [
        'trade|订单号' => 'alphaDash',
        'pay_no|流水号' => 'alphaDash',
        'amount|金额' => 'require|float|between:1,100000',
        'is_online|是否线上' => 'require|integer',
    ];

    protected $scene = [
        'findAll' => ['trade','pay_no'],
        'save' => ['amount','is_online'],
    ];
}