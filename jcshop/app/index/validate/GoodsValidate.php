<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use think\Validate;

class GoodsValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'	=>	['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'merchant_id' => 'require|number',
        'page'        =>'require|number',
        'size'        =>'require|number'
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'	=>	'错误信息'
     *
     * @var array
     */
    protected $message = [
        'merchant_id.require'       => '商户ID必须传',
        'merchant_id.number'        => '商户ID只能是数字组成',
        'page.requrie'              => '页码必传',
        'page.number'               => '页码只能是数字',
        'size.requrie'              => '行数必传',
        'size.number'               => '行数必须是数字'

    ];

    protected $scene = [
        'goodsList'   => ['merchant_id','page','size'],
    ];

}