<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use app\index\model\Order;
use think\db\exception\DataNotFoundException;
use think\db\exception\ModelNotFoundException;
use think\exception\HttpException;
use think\Validate;

class OrderValidate extends Validate
{
    /**
     * 定义验证规则
     * 格式：'字段名'    =>    ['规则1','规则2'...]
     *
     * @var array
     */
    protected $rule = [
        'type'  => 'require|number',
        'user_address_id'   => 'require|number',
        'commoditys'    => 'require|array',
        'id'    => 'checkCurrentUser',
        'commodity_id'  => 'require|number',
        'num'   => 'require|number',
    ];

    /**
     * 定义错误信息
     * 格式：'字段名.规则名'    =>    '错误信息'
     *
     * @var array
     */
    protected $message = [
        'type.require'  => '字段type必须传',
        'type.number'   => 'type只能是数字',
        'user_address_id.require'   => '用户地址id必填',
        'user_address_id.number'    => '地址ID只能是数字',
        'commoditys.requrie'    => '商品数组必须传',
        'id'    => '订单ID必传',
        'commodity_id.require'  => '商品id必填',
        'commodity_id.number'   => '商品数量为整数',
        'num.require'   => '数量必填',
        'num.number'    => '数量为整数',
    ];

    protected $scene = [
        'evaluate' => ['id'],
        'city_freight' => ['user_address_id', 'commodity_id', 'num'],
        'good_city_freight' => ['user_address_id', 'commoditys'],
    ];

    /**数据权限校验
     * @param int $oid
     * @return string
     * @throws DataNotFoundException
     * @throws DbException
     * @throws ModelNotFoundException
     * @throws \think\db\exception\DbException
     */
    public function checkCurrentUser($value, $rule, $data=[])
    {
        global $user;
        dd($value);
        $order = Order::find($value);
        if ($user['id'] != $order['user_id']) return $rule = "您无权查看...";
    }

}