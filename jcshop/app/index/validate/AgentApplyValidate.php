<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\validate;


use think\Validate;

class AgentApplyValidate extends Validate
{
    protected $rule = [
        'name' => 'require|max:32',
        'mobile' => 'require|mobile'
    ];
    protected $message = [
        'name.require' => '姓名不能为空',
        'mobile' => '手机号码必须填写正确'
    ];
    protected $scene = [
        'save' => ['name','mobile']
    ];
}