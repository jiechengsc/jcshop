<?php


namespace app\index\validate;


use think\Validate;

class AssettransferValidate extends Validate
{
    protected $rule = [
        'page' => 'require|integer',
        'size' => 'require|integer',
        'amount' => 'require|number|egt:0.01',
    ];
    protected $message = [
        'id' => '必传',
        'page' => '页数未传或不为整数',
        'size' => '每页数量未传或不未整数',
        'amount' => '金额必须上传且大于0.01',
        'gifted_user_id' =>'被赠用户ID必传'
        //'withdraw' => '提现金额必须上传且大于100',
        //'type' => '类型错误',
    ];
    protected $scene = [
        'findAll' => ['page', 'size'],
        'read' => ['id'],
        'amount' => ['amount','gifted_user_id'],

    ];
}