<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\validate;

use think\Validate;

class AgentValidate extends Validate
{
    protected $rule = [
        'page' => 'require',
        'size' => 'require',
        'commodity' => 'require|integer',
        'sku' => 'integer'
    ];

    protected $message = [
        'id.require' => '必传',
        'user_id.require' => '必传',
        'page.require' => '页数必须上传',
        'size.require' => '每页显示条数必须上传'
    ];
    protected $scene = [
        'read' => ['id'],
        'findAll' => ['page', 'size'],
        'commodity' => ['commodity','sku']
    ];
}