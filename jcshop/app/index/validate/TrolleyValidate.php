<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\validate;

use think\Validate;

class TrolleyValidate extends Validate
{
    protected $rule = [
        'type|收货类型' => 'require|in:1,2',
        'commodityId|商品' => 'require|integer',
        'sku|sku' => 'integer',
        'store_id' => 'integer',
        'num' => 'require|integer'

    ];

    protected $message = [
        'sku.integer' => "请选择sku",
        'store_id.integer' => "请选择门店"
    ];

    protected $scene = [
        'save' => [
            'type',
            'commodityId',
            'sku',
            'store_id',
            'num'
        ],
    ];

    public function sceneUpdate()
    {
        return $this->only([
            'type',
            'commodityId',
            'sku',
            'store_id',
            'num'
        ])->remove([
            'type'=>'require',
        ]);
    }
}