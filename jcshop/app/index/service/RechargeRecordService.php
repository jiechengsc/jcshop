<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\RechargeRecord as admin;
use app\utils\TrimData;

class RechargeRecordService
{


    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('*');
        $admin = TrimData::searchDataTrim($admin, $data, ['trade', 'pay_no', 'begin_date', 'end_date']);

        $list = $admin->where($data)->where('id', '>', 1)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

}
