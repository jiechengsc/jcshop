<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Classify;
use app\index\model\Commodity;
use app\index\model\Coupon as admin;
use app\index\model\UserCoupon;
use app\utils\TrimData;

class CouponService
{

    public function findAll(int $page = 1, int $size = 10)
    {
        $list = admin::where(['get_type' => 0, 'status'=>1])
            ->with(['merchant'])
            ->hidden(['create_time', 'update_time', 'delete_time', 'status', 'used'])
            ->where('case when `time_limit_type` = 1 then now() < `limit_indate_end` else `limit_indate_end` is null end')
            ->where('is_show','=',1)
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    public function findById($ids)
    {
        $ids = json_decode($ids,true);
        $list = admin::where(['id'=>$ids,'status'=>1])
            ->with(['merchant'])
            ->hidden(['create_time', 'update_time', 'delete_time', 'status', 'used'])
            ->select();
        return [HTTP_SUCCESS, $list];
    }

    public function findByMerchant($merchantId)
    {
        $list = admin::where(['get_type' => 2, 'merchant_id' => $merchantId,'status'=>1])
            ->with(['merchant'])
            ->select()
            ->hidden(['create_time', 'update_time', 'delete_time', 'status', 'used']);
        return [HTTP_SUCCESS, $list];
    }

    public function findByCommodity($commodityId)
    {

        $commodity = Commodity::find($commodityId);

        $list = admin::where(['get_type' => 2,'is_show'=>1,'merchant_id' => $commodity->merchant_id,'status'=>1])
            ->with(['merchant'])
            ->select()
            ->hidden(['create_time', 'update_time', 'delete_time', 'status', 'used']);

        if (empty($list)){
            return [HTTP_SUCCESS, null];
        }

        foreach ($list as $k => $v) {
            switch ($v->commodity_limit_type) {
                case 1:
                    $commodityList = explode(",", $v->commodity_limit);
                    if (!in_array($commodityId, $commodityList))
                        unset($list[$k]);
                    break;
                case 2:
                    $commodityList = explode(",", $v->commodity_limit);
                    if (in_array($commodityId, $commodityList))
                        unset($list[$k]);
                    break;
                case 3:
                    $allowList = explode(",", $v->commodity_limit);
                    $allowList = array_map(function ($var) {
                        return explode("->", $var);
                    }, $allowList);
                    $explode = explode("->", $commodity->classify_id);
                    $flag = $this->arrayMatch($explode, $allowList);
                    if (!$flag)
                        unset($list[$k]);
                    break;
            }
        }
        $list = array_values($list->toArray());
        return [HTTP_SUCCESS, $list];
    }

    private function arrayMatch($explode, $allowList)
    {
        $flag = true;
        foreach ($allowList as $index => $item) {
            $diff = array_diff($item, $explode);
            if (empty($diff)) {
                $flag = false;
                break;
            }
        }
        return $flag;
    }

    public function read(int $id)
    {
        $model = admin::where(['id'=>$id,'status'=>1])->find();
        if (!$model) {
            return [HTTP_SUCCESS, null];
        }
        if ($model->commodity_limit_type == 1 && !empty($model->commodity_limit)){
            $commodityIds = explode(",", $model->commodity_limit);
            $commodityList = Commodity::where(['id' => $commodityIds])->field("id,name")->select();
            $model->commodity_limit = $commodityList;
        }elseif($model->commodity_limit_type == 2 && !empty($model->commodity_limit)){
            $commodityIds = explode(",", $model->commodity_limit);
            $commodityList = Commodity::where(['id' => $commodityIds])->field("id,name")->select();
            $model->commodity_limit = $commodityList;
        }elseif($model->commodity_limit_type == 3 && !empty($model->commodity_limit)){
            $classifyIdList = explode(",", $model->commodity_limit);
            $classifyIds = array();
            foreach($classifyIdList as $classifyId) {
                $explode = explode("->", $classifyId);
                end($explode);
                $classifyIds[] = array_shift($explode);
            }
            $classifyList = Classify::where(['id' => $classifyIds])->field("id,name")->select();
            $model->commodity_limit = $classifyList;
        }

        $model = $model->hidden(['create_time', 'update_time', 'delete_time', 'status', 'used']);
        return [HTTP_SUCCESS, $model];
    }
}
