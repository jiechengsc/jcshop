<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Article as admin;
use app\utils\TrimData;

class ArticleService
{


    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,type,title,subhead,headimg,status,create_time,update_time');
        $admin = TrimData::searchDataTrim($admin, $data, ['title', 'begin_date', 'end_date']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }

}
