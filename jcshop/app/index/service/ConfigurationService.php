<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Configuration;

use app\index\model\Configuration as admin;
use app\utils\Addons;
use think\facade\Db;

class ConfigurationService
{


    public function read(string $type)
    {
        $model = admin::where(['status' => 1])->field('configuration')->find($type);
        if (!$model)
            return [HTTP_NOTACCEPT, "配置不存在"];

        return [HTTP_SUCCESS, $model->configuration];
    }

    /**
     * 截留口令
     */
    public function withholdPassword()
    {
        global $mid;
        $type = 'withholdPassword';
        $is_bool = Addons::check($mid, 33);
        if ($is_bool) {
            $config = Configuration::where('mall_id', $mid)->where('type', $type)->find();
        }
//        if (empty($config)) {
//            $less = Db::name('lessee')->where('id', $mid)->find();
//
//            if (!empty($less['withhold_password_switch'])) {
//                $config = Db::name('saas_configuration')->where('type', $type)->find();
//
//            }
//        }
        $configuration = [
            'status' => 0,
            'configuration' => [
                'content' => ''
            ],
            'content' => ''
        ];

        if (!empty($config['configuration'])) {
            if (is_array($config['configuration'])) {
                $configuration = ['status' => $config['status'] ?? 0, 'content' => $config['configuration']['content'] ?? '', 'configuration' => $config['configuration']];
            } else {
                try {
                    $configuration = json_decode($config['configuration'],true);
                    $configuration = ['status' => $config['status'] ?? 0, 'content' => $configuration['content'] ?? '', 'configuration' => $config['configuration']];
                } catch (\Exception $e) {
                }
            }
        }
        if(isset($configuration['status']) && $configuration['status']==0){
            $configuration['content']='';
        }

        return [HTTP_SUCCESS, $configuration];
    }

    public function ossConfig(): array
    {
        global $mid;
        $url = request()->server("REQUEST_SCHEME") . '://' . $_SERVER['HTTP_HOST'];
        $con = Configuration::where('mall_id', $mid)->where('type', 'oss')->find();

        if (!empty($con)) {
            $con = $con['configuration'];
            if ($con['oss_choice']==1){
                $url = request()->server("REQUEST_SCHEME") . '://' . $_SERVER['HTTP_HOST'];
            }else {
                if (isset($con['ali'])) {
                    switch ($con['is_open']) {
                        case 0:
                            $url = request()->server("REQUEST_SCHEME") . '://' . $_SERVER['HTTP_HOST'];
                            break;
                        case 1:
                            $is_customize = oss_is_customize($con['ali']['endpoint']);
                            $url = $is_customize ? $con['ali']['endpoint'] : "https://" . $con['ali']['bucket'] . "." . str_replace("https://", "", $con['ali']['endpoint']);
                            break;
                        case 2:
                            $url = $con['qcloud']['endpoint'];
                            break;
                    }
                }
            }
        }else{
            $url = request()->server("REQUEST_SCHEME") . '://' . $_SERVER['HTTP_HOST'];
        }
        return [HTTP_SUCCESS, $url];
    }

}
