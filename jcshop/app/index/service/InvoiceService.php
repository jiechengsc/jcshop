<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\service;


use app\index\model\Invoice;
use app\index\model\Order;
use app\index\model\Rise;
use think\Exception;
use think\facade\Db;

class InvoiceService
{
    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }


    public function create(array $data)
    {
        global $mid, $user;
        $data['mall_id'] = $mid;
        $find = Invoice::where('order_id', $data['order_id'])->find();

        if ($find) throw new Exception('该订单已开过发票', HTTP_UNAUTH);
        $order = Db::name('order')->find($data['order_id']);
        if (in_array($order['status'], [1, 6]) || in_array($order['after_status'], [1])) throw new Exception('该订单状态不支持开票', HTTP_UNAUTH);

        if (isset($data['rise_id']) && $data['rise_id']!="undefined") {
            if ($data['is_default']) Rise::where('user_id', $this->user['id'])->update(['is_default' => 0]);
            Rise::update($data, ['id' => $data['rise_id']]);
        } else {
            Rise::create($data);
        }

        unset($data['is_default']);
        $create = Invoice::create($data);
        return [HTTP_SUCCESS, $create];
    }


    public function editInvoice(array $data)
    {
        if (empty($data['id'])) throw new Exception('ID不能为空！', HTTP_INVALID);
        $id = $data['id'];
        $data['status'] = 1;
        $data['remake'] = null;
        unset($data['id']);
        $up = Invoice::update($data, ['id'=>$id],[
            'mailbox','is_default','money','remake','invoice_type',
            'rise','status','bank_account','deposit_bank','phone','address','duty_paragraph'
        ]);
        return [HTTP_SUCCESS, $up];
    }


    public function info($id)
    {
        $info = Invoice::with(['user'])->find($id);
        return [HTTP_SUCCESS, $info];
    }

    public function list(array $data)
    {
        global $user;
        $data['user_id']=$user['id'];

        $res = Invoice::where($this->buildWhere($data))
            ->order('id desc')
            ->paginate(['page' => $data['page'], 'list_rows' => $data['size']]);

        return [HTTP_SUCCESS, $res];
    }


    public function riseDel(int $id)
    {
        $find = Rise::where('is_default', 1)->find($id);
        if ($find) throw new Exception('默认抬头不能删除', HTTP_UNAUTH);
        Rise::destroy($id);
        return HTTP_NOCONTEND;
    }


    public function setRiseDefault($id)
    {
        Rise::where('is_default', 1)->where('user_id', $this->user['id'])->update(['is_default' => 0]);
        Rise::update(['is_default' => 1], ['id' => $id]);
        return [HTTP_SUCCESS, '成功!'];
    }


    public function riseRead()
    {
        $res = Rise::where('is_default', 1)->where('user_id', $this->user['id'])->find();
        return [HTTP_SUCCESS, $res];
    }

    public function riseGain(int $id)
    {
        $res = Rise::where('user_id', $this->user['id'])->find($id);
        $res->hidden(['mall_id', 'create_time', 'update_time', 'delete_time']);
        return [HTTP_SUCCESS, $res];
    }


    public function riseList(array $data)
    {
        $res = Rise::where('user_id', $this->user['id'])
            ->order('is_default desc')
            ->paginate(['page' => $data['page'], 'list_rows' => $data['size']]);
        return [HTTP_SUCCESS, $res];
    }


    public function riseCreate(array $data)
    {
        global $mid;
        $data['mall_id'] = $mid;
        $data['user_id'] = $this->user['id'];
        if ($data['is_default'] == 1) Rise::where(['is_default' => 1, 'user_id' => $this->user['id']])->update(['is_default' => 0]);
        $create = Rise::create($data);
        return [HTTP_SUCCESS, $create];
    }


    public function riseUp(array $data)
    {
        $id = $data['id'];
        unset($data['id']);
        if ($data['is_default'] == 1) Rise::where(['is_default' => 1, 'user_id' => $this->user['id']])->update(['is_default' => 0]);
        $up = Rise::update($data, ['id' => $id]);
        return [HTTP_SUCCESS, $up];
    }

    public function buildWhere(array $data)
    {
        $where = [];
        global $user;
        !empty($data['invoice_type']) && $where[] = ['invoice_type', '=', $data['invoice_type']];
        !empty($data['user_id']) && $where[] = ['user_id', '=', $user['id']];
        return $where;
    }
}