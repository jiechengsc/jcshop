<?php
declare (strict_types=1);


namespace app\index\service;

use app\index\model\WxCompany;
use app\index\model\WxCustomer;

class WxCsService
{

    /**
     * 查询列表
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function index(int $page = 1, int $size = 10)
    {
        $list = WxCustomer::paginate(['page' => $page, 'list_rows' => $size]);
        foreach ($list as $key => $value) {
            $list[$key]['corp_id'] = WxCompany::find($value['company_id'])['corp_id'];
        }
        return [HTTP_SUCCESS, $list];
    }

    /**
     * 获取客服信息
     * @param int $id
     * @return array
     * @throws \think\db\exception\DbException
     */
    public function read(int $id)
    {
        $item = WxCustomer::find($id);
        $item['corp_id'] = WxCompany::find($item['company_id'])['corp_id'];
        return [HTTP_SUCCESS, $item];
    }

}
