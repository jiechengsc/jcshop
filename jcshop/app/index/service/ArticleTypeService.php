<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\ArticleType as admin;
use app\utils\TrimData;

class ArticleTypeService
{


    public function index()
    {
        $list = admin::where('status', 1)->select();
        return [HTTP_SUCCESS, $list];
    }


    public function findAll(array $data, int $page = 1, int $size = 10)
    {

        $admin = admin::field('id,name,status');
        $admin = TrimData::searchDataTrim($admin, $data, ['name']);

        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

}
