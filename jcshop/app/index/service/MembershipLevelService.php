<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Coupon;
use app\index\model\MembershipLevel;
use app\index\model\MembershipLevel as admin;

class MembershipLevelService
{

    private const fields = [
        'level',
        'name',
        'indate',
        'discount',
        'is_cash_upgrade',
        'reach_cash',
        'is_buy_upgrade',
        'buy_price',
        'is_present_money',
        'present_money',
        'is_present_integer',
        'present_integer',
        'is_present_coupon',
        'present_coupon',
        'title',
        'content',
        'id'
    ];

    public function index()
    {
        $list = admin::field(self::fields)->with(['info'])->select();

        return [HTTP_SUCCESS, $list];
    }

    public function read(int $id)
    {
        $model = admin::field(self::fields)
            ->where(['level' =>$id])
            ->with(['info'])
            ->find();
        return [HTTP_SUCCESS, $model];
    }

    public function getLevelData() : array{
        global $user;
        if (empty($user->level))
            $where = ['level' => 1];
        else
            $where = ['level' => $user->level];
        $level = MembershipLevel::where($where)
            ->with(['info'])
            ->find();
        return [HTTP_SUCCESS,$level];
    }
}
