<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Store as admin;
use app\utils\TrimData;
use think\model\Relation;

class StoreService
{


    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        $admin = admin::field('id,merchant_id,name,mobile,district,address,longitude,latitude,remark')
            ->with(['merchant'=>function(Relation $query){
                $query->bind(['merchant_name'=>'name']);
            }]);
        $admin = TrimData::searchDataTrim($admin, $data, ['name']);

        unset($data['name']);
        $list = $admin->where($data)->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }


    public function read(int $id)
    {
        $model = admin::field('id,merchant_id,name,mobile,district,address,longitude,latitude,remark')
            ->with(['merchant'=>function(Relation $query){
                $query->bind(['merchant_name'=>'name']);
            }])->find($id);
        return [HTTP_SUCCESS, $model];
    }

}
