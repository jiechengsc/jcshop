<?php


namespace app\index\service;


use app\index\model\Configuration as config;
use app\index\model\IntegralRecord as integral;
use app\index\model\RechargeRecord as amount;
use app\index\model\SignIn;
use app\index\model\SignIn as model;
use app\index\model\UserCash as cash;
use think\Exception;

class SignInService
{

    public function sign($user) : array{
        global $mid;
        $config = config::where('type', 'signIn')
            ->where('mall_id',$mid)
            ->value('configuration');
        $config = json_decode($config);
        if ($config->signIn == false)
            throw new Exception('签到功能未开启',HTTP_INVALID);
        $count = model::where('user_id',$user['id'])
            ->where('create_time',date('Y-m-d'))
            ->count();
        if ($count > 0)
            throw new Exception('今日已经签到', HTTP_INVALID);

        return $this->not_special($config, $user);

    }

    private function not_special($config, $user) : array{
        $value = ['integral' => 0,'amount' => 0];
        $field = $config->type == 1 ? 'integral' : 'amount';
        $value[$field] = $config->value;
        $continuity = 0;
        if (!empty($config->continuity)){
            $continuity = model::where('user_id', $user['id'])
                ->where('create_time',date('Y-m-d',time()-86400))
                ->value('continuity');
            empty($continuity) && $continuity = 0;
            $continuity++;
            foreach ($config->continuity AS $key => $item) {
                $field = $item->type == 1 ? 'integral' : 'amount';
                if ($item->loop == true) {
                    if ($item->day / $continuity == 0) $value[$field] += $item->value;
                } else {
                    if ($item->day == $continuity) $value[$field] += $item->value;
                }
            }
        }
        if (!empty($config->cumulative)){
            $cumulative = model::where('user_id', $user['id'])->count();
            foreach ($config->cumulative AS $index => $item) {
                $field = $item->type == 1 ? 'integral' : 'amount';
                if ($item->day == $cumulative) $value[$field] += $item->value;
            }
        }
        $user = cash::where('user_id', $user['id'])
            ->lock(true)
            ->find();
        model::create([
            'user_id' => $user['user_id'],
            'mall_id' => $user['mall_id'],
            'integral' => $value['integral'],
            'amount' => $value['amount'],
            'create_time' => date('Y-m-d'),
            'continuity' => $continuity
        ]);
        $this->integral($user,$value['integral']);
        $this->amount($user,$value['amount']);
        return [HTTP_SUCCESS,'签到成功'];
    }
    
    private function integral(cash $user, int $value) : void{
        if (empty($value))return;
        $user->save([
            'integral' => bcadd($user->integral , $value),
            'integral_total' => bcadd($user->integral_total , $value),
            'integral_withdraw' => bcadd($user->integral_withdraw , $value),
            'update_time' => date('Y-m-d H:i:s')
        ]);
        integral::create([
            'user_id' => $user->user_id,
            'mall_id' => $user->mall_id,
            'order_id' => 0,
            'commodity_id' => 0,
            'sku_id' => 0,
            'number' => 0,
            'type' => 6,
            'status' => 1,
            'integral' => $value,
            'create_time' => date('Y-m-d H:i:s'),
            'update_time' => date('Y-m-d H:i:s')
        ]);
    }

    private function amount(cash $user, float $value) : void{
        if (empty($value))return;
        $user->save([
            'total' => bcadd($user->total , $value),
            'update_time' => date('Y-m-d H:i:s')
        ]);
        amount::create([
            "user_id" => $user->user_id,
            'mall_id' => $user->mall_id,
            "base_in" => "admin",
            "source_type" => 7,
            "source" => "签到获得".$value,
            "type" => 0,
            "money" => $value,
            "remark" => '签到',
            "status" => 1
        ]);
    }


    public function findAll($user,string $start_time,string $end_time) : array{
        $list = SignIn::where('user_id',$user['id'])
            ->where('create_time','>=',$start_time)
            ->where('create_time','<=',$end_time)
            ->order('create_time DESC')
            ->select();
        $count = SignIn::where('user_id',$user['id'])->count();
        $continuity = SignIn::where('user_id',$user['id'])
            ->where('create_time',date('Y-m-d'))
            ->value('continuity');
        if (empty($continuity))
            $continuity = SignIn::where('user_id',$user['id'])
                ->where('create_time',date('Y-m-d',time()-86400))
                ->value('continuity');
        if (empty($continuity))
            $continuity = 0;
        return [HTTP_SUCCESS,['list' => $list,'count' => $count,'continuity' => $continuity]];
    }
}