<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\UserCard as admin;
use app\utils\TrimData;

class UserCardService
{

    private $user;
    private $mid;
    public function __construct()
    {
        global $user,$mid;
        $this->user = $user;
        $this->mid = $mid;
    }

    public function findAll(int $page = 1, int $size = 10)
    {
        $list = admin::paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }


    public function save(array $data)
    {
        $data['user_id'] = $this->user['id'];
        $data['mall_id'] = $this->mid;
        $model = admin::create($data);
        return [HTTP_CREATED, $model];
    }

    public function read(int $id)
    {
        $model = admin::find($id);
        return [HTTP_SUCCESS, $model];
    }


    public function update(int $id, array $data)
    {
        $update_time = $data['update_time'];
        unset($data['update_time'],$data['user_id']);
        $admin = admin::where('update_time', $update_time)
            ->where('id', $id)
            ->save($data);
        return [HTTP_CREATED, $admin];
    }

    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

}
