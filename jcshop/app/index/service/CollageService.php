<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Classify;
use app\index\model\Collage;
use app\index\model\Commodity;
use app\index\model\Group;
use app\index\model\OrderCommodity;

class CollageService
{

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    public function index(int $page = 1, int $size = 10, array $data = [])
    {
        $data['status'] = 1;
        $list = Collage::where($data)
            ->with(['collageItem', 'commodity'])
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

    public function read(int $id)
    {
        Collage::finishCollage($id);
        $model = Collage::with(['commodity', 'collageItem' => function($sql) {
                $sql->with(['user']);
            }, 'groupCommodity' => function ($sql) {
                $sql->with(['sku']);
            }, 'user'])->find($id);
        $gc_id = OrderCommodity::find($model['oc_id'])["gc_id"];
        $order = OrderCommodity::alias('a')
            ->join('order o', 'a.order_id = o.id', 'right')
            ->field('a.order_id')
            ->where('a.gc_id', $gc_id)
            ->where('o.user_id', $this->user['id'])
            ->where('a.collage_id', $id)
            ->find();
        $model['order_id'] = $order['order_id'];
        return [HTTP_SUCCESS, $model];
    }

    public function group(int $id)
    {
        $model = Group::with(['commodity' => function ($sql) {
            $sql->with(['commodity', 'sku' => function ($query) {
                $query->with('sku');
            }]);
        }])->where('status', 'in', '0,1')->find($id);
        $model = is_null($model) ? "" : changTime($model);
        return [HTTP_SUCCESS, $model];
    }
}
