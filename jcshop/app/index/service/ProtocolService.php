<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Protocol as admin;
use app\utils\TrimData;

class ProtocolService
{


    public function read(int $type)
    {
        $model = admin::where(['type' => $type])->field("title,subhead,content")->find();
        return [HTTP_SUCCESS, $model];
    }

}
