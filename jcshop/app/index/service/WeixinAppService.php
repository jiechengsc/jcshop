<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Article as admin;
use app\madmin\model\Commodity;
use app\madmin\model\LiveLog;
use app\madmin\model\LiveRoom;
use app\utils\LiveErrorCode;
use app\utils\TrimData;
use app\utils\Weixin;
use app\utils\WeixinApp;
use think\facade\Cache;

class WeixinAppService
{

    public function save()
    {
        $data = request()->post();
        $res = WeixinApp::create(json_encode($data, JSON_UNESCAPED_UNICODE));
        if(!empty($res['errmsg'])){
            return [HTTP_NOTACCEPT, $res['errmsg']];
        }
        return [HTTP_SUCCESS, "成功"];
    }
}
