<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\BottomMenu as admin;
use app\utils\TrimData;

class BottomMenuService
{

    public function index()
    {

        $data = admin::where(['status' => 1])->field('data')->find();
        if ($data) {
            $data = $data->toArray();
            return [HTTP_SUCCESS, $data['data']];
        }
        return [HTTP_SUCCESS, $data];
    }

}
