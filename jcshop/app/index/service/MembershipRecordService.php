<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\MembershipRecord as admin;

class MembershipRecordService
{

    private const fields = [
        '*'
    ];


    public function index()
    {
        $list = admin::field(self::fields)->select();

        return [HTTP_SUCCESS, $list];
    }

    public function read(int $id)
    {
        $model = admin::with(['present'])->field(self::fields)
            ->find($id);
        return [HTTP_SUCCESS, $model];
    }

}
