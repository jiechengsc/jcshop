<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\service;

use app\index\model\Notice as admin;
use app\utils\TrimData;

class NoticeService
{


    public function findAll(int $merchantId)
    {
        $where=['status'=>1];
        if(!empty($merchantId)){
            $where['merchant_id']=$merchantId;
        }
        $limit=request()->get('limit',5);

        $list = admin::field('id,type,title,sort,status')
            ->where($where)->limit($limit)->order('sort,id')->select();

        return [HTTP_SUCCESS, $list];
    }


    public function read(int $id,int $merchantId)
    {
        $where=['status'=>1];
        if(!empty($merchantId)){
            $where['merchant_id']=$merchantId;
        }
        $model = admin::where($where)->field('title,type,content')->find($id);
        return [HTTP_SUCCESS, $model];
    }

}
