<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\AcceptRecord as admin;
use app\utils\TrimData;

class AcceptRecordService
{

    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }

    public function findAll(array $data, int $page = 1, int $size = 10)
    {
        unset($data['page']);
        unset($data['size']);
        $admin = admin::field(['order_no','commodity','assistant','store','accept_time']);
        $admin = TrimData::searchDataTrim($admin, $data, ['order_no', 'store', 'begin_date', 'end_date']);

        $list = $admin->where($data)
            ->where(['assistant_user_id'=>$this->user['id']])
            ->paginate(['page' => $page, 'list_rows' => $size]);
        return [HTTP_SUCCESS, $list];
    }

}
