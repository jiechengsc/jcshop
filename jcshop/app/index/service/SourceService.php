<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
namespace app\index\service;

use app\index\model\Configuration;
use think\Exception;

class SourceService
{

    public function judge(string $type)
    {
            $t = $this->decide($type);
            if (!$t) throw new Exception("您请求的信息有误!");
            $res = Configuration::where('type',$t)->value('configuration');
            $js = json_decode($res,true);
            $js['type'] = $t;
            return  $js;
    }

    public function decide($type)
    {
        switch ($type)
        {
            case 1:
                return 'wxa'; //小程序
            case 2:
                return 'wx'; //公众号
            default:
                false;
        }
    }

}