<?php
declare (strict_types=1);

namespace app\index\service;

use app\index\model\MerchantApply as admin;
use think\Exception;

class MerchantApplyService
{

    private $user;

    public function __construct()
    {
        global $user;
        $this->user = $user;
    }


    public function save(array $data)
    {

        $apply = admin::find(['user_id' => $this->user['id'], 'status' => 0]);
        if ($apply)
            throw new Exception("已有申请", HTTP_NOTACCEPT);

        $data['user_id'] = $this->user['id'];

        global $mid;
        $data['mall_id'] = $mid;

        $model = admin::create($data);
        return [HTTP_CREATED, $model];
    }


    public function read()
    {
        global $mid;
        $model = admin::field(['id', 'logo', 'name', 'mobile', 'business_license', 'create_time', 'status', 'remark'])
            ->order("id", "desc")
            ->where(['user_id' => $this->user['id'], 'mall_id' => $mid])
            ->find();
        return [HTTP_SUCCESS, $model];
    }

    public function update(int $id, array $data)
    {
        global $mid;
        $admin = admin::where(['id' => $id, 'user_id' => $this->user['id'], 'status' => 0, 'mall_id' => $mid])
            ->save($data);
        return [HTTP_CREATED, $admin];
    }

    public function delete($id)
    {
        admin::destroy($id);
        return HTTP_NOCONTEND;
    }

}
