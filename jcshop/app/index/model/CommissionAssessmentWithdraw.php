<?php


namespace app\index\model;


use app\SearchModel;
use think\Model;
use think\model\concern\SoftDelete;

class CommissionAssessmentWithdraw extends SearchModel
{
    use SoftDelete;
    protected $autoWriteTimestamp = 'datetime';
    protected $json = ['card'];
    protected $jsonAssoc = true;
    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])) {
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value, $data)
    {
        $array = [0 => '申请中', 1 => '通过', 2 => '取消'];
        $text = empty($array[$data['status']]) ? '' : $array[$data['status']];
        return $text;
    }
}