<?php
declare (strict_types=1);

namespace app\index\model;

use app\SearchModel;
use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;
use think\facade\Db;

class CommissionActivity extends SearchModel
{
    #use SoftDelete;
}
