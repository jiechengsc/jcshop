<?php


namespace app\index\model;


use think\Model;

class IntegralSku extends Model
{
    public function inventory(){
        return $this->hasOne(IntegralSkuInventory::class,'sku_id','id')->bind(['total','sell','sell_price']);
    }
}