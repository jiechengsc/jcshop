<?php
declare (strict_types=1);

namespace app\index\model;

use app\SearchModel;
use think\db\Query;

class MerchantApply extends SearchModel
{
    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }
}
