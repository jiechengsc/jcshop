<?php
declare (strict_types = 1);

namespace app\index\model;

use app\SearchModel;

class PresellCommoditySku extends SearchModel
{

    public static function getSku($pc_id, $sku)
    {
        $sku = PresellCommoditySku::where('pc_id', $pc_id)
            ->where('sku_id', $sku)
            ->find();
        return $sku;
    }

    public function sku()
    {
        return $this->hasOne(Sku::class, 'id', 'sku_id')->bind(['pvs_value']);
    }

}
