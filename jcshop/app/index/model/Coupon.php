<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use think\db\Query;
use think\model\concern\SoftDelete;


class Coupon extends BaseModel
{
    use SoftDelete;

    protected $globalScope = ['status', 'mallId', 'audit'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function scopeAudit(Query $query)
    {
        $query->where('audit', 1);
    }

    public function searchNameAttr(Query $query, $value, $data)
    {
        $query->where('name', 'like', "%" . $value . "%");
    }

    public function merchant()
    {
        return $this->belongsTo(Merchant::class, 'merchant_id', 'id')
            ->bind(['merchant_name' => 'name', 'merchant_logo' => "logo", 'merchant_is_self' => "is_self"]);
    }

}
