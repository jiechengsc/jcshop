<?php


namespace app\index\model;


use think\facade\Db;
use think\Model;
use think\model\concern\SoftDelete;

class NewKidGiftConfig extends Model
{
    protected $json = ['config'];
    protected $jsonAssoc = true;
    use SoftDelete;
    public function GetCouponDataAttr($value,$data){
        return Db::name('coupon')
            ->where('id','IN',$data['config']['coupon'])
            ->select();
    }
}