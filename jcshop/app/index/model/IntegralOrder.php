<?php


namespace app\index\model;


use think\Model;

class IntegralOrder extends Model
{
    public function orderCommodity()
    {
        return $this->hasOne(IntegralOrderCommodity::class, 'order_id', 'id');
    }
    public function store(){
        return $this->hasOne(Store::class, 'id', 'store_id');
    }
}