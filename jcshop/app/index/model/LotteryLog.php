<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use think\Model;

class LotteryLog extends Model
{
    protected $autoWriteTimestamp = 'datetime';
    public static function getInfo(int $type,$value) : ?string {
        switch ($type){
            case 1:
                return '谢谢惠顾';
                break;
            case 2:
                return $value;
            case 3:
                return $value;
            case 4:
                $name = \think\facade\Db::name('commodity')->where('id',$value['commodity_id'])->value('name');
                if (!empty($value['sku_id'])) {
                    $pvs_value = \think\facade\Db::name('sku')
                        ->where('id', $value['sku_id'])
                        ->value('pvs_value');
                    $name .= '('.$pvs_value.')';
                }
                return $name;
            case 5:
                $name = \think\facade\Db::name('coupon')->where('id',$value['coupon_id'])->value('name');
                return $name;
            default:
                return '下次好运';
        }
    }

    public function Prize(){
        return $this->hasOne(LotteryPrize::class,'uniqid','uniqid');
    }

    public function Logistics(){
        return $this->hasOne(LotteryLogistics::class,'lottery_log_id','id');
    }
}