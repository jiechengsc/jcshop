<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use app\SearchModel;
use think\db\Query;
use think\helper\Str;
use think\Model;
use think\model\concern\SoftDelete;

class User extends SearchModel
{
    protected $autoWriteTimestamp = 'datetime';
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }


    public function userCash()
    {
        return $this->hasOne(UserCash::class, 'user_id', 'id');
    }

    public function agentCash()
    {
        return $this->hasOne(UserCash::class, 'user_id', 'id')
            ->bind(['agent_total', 'agent', 'agent_withdraw']);
    }

    public function userWx()
    {
        return $this->hasOne(UserWx::class, 'user_id', 'id');
    }

    public static function onBeforeInsert(Model $model)
    {
        $model->id = random_int(10000000,99999999);
        $user = User::find($model->id);
        if ($user)
            self::onBeforeInsert($model);
    }

}
