<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class BalanceBill extends Model
{
    use SoftDelete;
    protected $autoWriteTimestamp = 'datetime';
    public function scopeUserId(Query $query)
    {
        global $user;
        $query->where('user_id','=', $user['id']);
    }
}