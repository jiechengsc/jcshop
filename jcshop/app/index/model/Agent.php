<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use app\madmin\model\AgentGrade;
use think\model\concern\SoftDelete;

class Agent extends BaseModel
{
    use SoftDelete;

    public function user(){
        return $this->hasOne(User::class,'id','user_id');
    }

    public function userAgent(){
        return $this->hasOne(User::class,'id','user_id')
            ->field("id,nickname,picurl,grade_id")->hidden(['id']);
    }

    public function superior()
    {
        return $this->hasOne(Agent::class,"id","pid")
            ->field('id,user_id,name,pid,grade_id');
    }

    public function grade()
    {
        return $this->hasOne(AgentGrade::class, 'id', 'grade_id');
    }
}