<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;

use app\SearchModel;
use think\db\Query;
use think\Model;

class BaseModel extends SearchModel
{
    protected static $mallId;
    protected $globalScope = ['status', 'mallId'];

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        global $mid;
        if (!empty($mid)) {
            self::$mallId = $mid;
        }else if (isset($data['mall_id'])) {
            self::$mallId = $data['mall_id'];
        }
    }

    public function scopeMallId(Query $query)
    {

        global $m_id;

        if(!empty($m_id)){
            self::$mallId=$m_id;
        }
        $query->where('mall_id', self::$mallId)->fetchSql();
    }

    public static function onBeforeInsert(Model $model)
    {

        $model->mall_id = self::$mallId;
    }

    public static function onAfterInsert(Model $model)
    {
        unset($model->mall_id);
    }

    public function scopeStatus(Query $query)
    {
        $query->where('status', 1);
    }
}