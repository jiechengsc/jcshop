<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;


use think\Model;

class LiveGood extends Model
{
    public function goods(){
        return $this->hasOne(Goods::class,'id','goods_id')->field([                'id',
            'merchant_id',
            'type',
            'name',
            'subtitle',
            'master',
            'classify_id',
            'total',
            'sell',
            'original_price',
            'sell_price',
            'min_price',
            'max_price',
            'sort',
            'is_virtual',
            'virtual_sell',
            'is_distribution',
            'distribution',
            'has_sku',
            'audit',
            'status',
            'create_time',
            'update_time']);
    }
}