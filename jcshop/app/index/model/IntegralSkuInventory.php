<?php


namespace app\index\model;


use think\Model;

class IntegralSkuInventory extends Model
{
    public function getNowLevelPriceAttr($value,$data) :?float{
        global $user;
        if (empty($data['level_price']))return null;
        $value = json_decode($data['level_price'],true);
        if (empty($value))return null;
        if(!empty($user['level'])) {
            if ($value['type'] == 1) {
                foreach ($value['price'] AS $key => $item){
                    if($item['level'] == $user['level']){
                        if ($item['type'] == 1)
                            return (float)bcmul((string)$item['price'],'1',2);
                        else
                            return (float)bcmul((string)$item['price'],(string)$data['sell_price'],2);
                    }
                }
            }
            return null;
        }else{
            return null;
        }
    }
}