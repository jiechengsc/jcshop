<?php
declare (strict_types=1);

namespace app\index\model;

use think\db\Query;
use think\Model;
use think\model\concern\SoftDelete;

class Article extends BaseModel
{
    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
        if (isset($data['status'])){
            self::append(['status_text']);
        }
    }

    public function getStatusTextAttr($value,$data)
    {
        $status = [
            0 => '未启用',
            1 => '正常'
        ];
        return $status[$data['status']];
    }

}
