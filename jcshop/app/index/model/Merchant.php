<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */
declare (strict_types=1);

namespace app\index\model;

use think\db\Query;
use think\model\concern\SoftDelete;

class Merchant extends BaseModel
{

    use SoftDelete;

    public function __construct(array $data = [])
    {
        parent::__construct($data);
    }

    public function searchNameAttr(Query $query, $value, $data)
    {
        $query->where('name', 'like', "%" . $value . "%");
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id')->bind(['user_name' => 'name']);
    }

    public function store()
    {
        return $this->hasMany(Store::class, 'merchant_id', 'id')
            ->visible(['id', 'name', 'mobile', 'district', 'address', 'longitude', 'latitude']);
    }

    public function commodity()
    {
        return $this->hasMany(Commodity::class,'merchant_id','id');
    }

    public function getCustomerServiceAttr($value,$data){
        if($data['customer_service_type'] == 2){
            return $data['customer_service_mobile'];
        }elseif($data['customer_service_type'] == 3){
            $images = MerchantCustomerService::where('merchant_id',$data['id'])
                ->field('merchant_id,url')
                ->select()
                ->toArray();
            return array_column($images,'url');
        }else{
            return '';
        }
    }
}
