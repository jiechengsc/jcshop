<?php


namespace app\index\model;


use app\index\model\MembershipLevelContent;
use app\index\model\MembershipLevelContentRelation;
use think\Model;

class LiveRoom extends Model
{
//    public function goods(){
//        return $this->hasMany(LiveGood::class,'id','user_id')
//            ->bind(['nickname','picurl']);
//    }
    public function liveGoods()
    {
        return $this->belongsToMany(LiveGood::class, LiveRoomHasGood::class, 'room_goods_id', 'room_id');
    }
}