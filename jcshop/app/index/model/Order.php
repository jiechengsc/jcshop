<?php
/**
 * | 节程 [ 节程赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 *  | Copyright (c) 2020~2029 温州惊蛰网络科技有限公司 All rights reserved.
 * +----------------------------------------------------------------------
 *  | Licensed 节程并不是自由软件，未经许可不能去掉节程相关版权
 * +----------------------------------------------------------------------
 */


namespace app\index\model;

use app\SearchModel;
use think\Model;

class Order extends FatherModel
{
    protected $readonly = ['order_no'];
    public function withTrashedData(bool $withTrashed)
    {
        $this->withTrashed = $withTrashed;
        return $this;
    }
    public function buildOrderNo($id)
    {
        $order_no = $id . $this->msectime() . rand(10000, 99999);
        return $order_no;
    }

    function msectime()
    {
        list($msec, $sec) = explode(' ', microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }

    //运费表
    public function freight()
    {
        return $this->hasOne(OrderFreight::class, 'order_id', 'id');
    }

    public function orderCommodity()
    {
        return $this->hasMany(OrderCommodity::class, 'order_id', 'id');
    }

    public function OrderCommodityAttach()
    {
        return $this->hasMany(OrderCommodityAttach::class, 'order_id', 'id');
    }

    public function collage()
    {
        return $this->hasMany(Collage::class, 'order_id', 'id');
    }

    public function userAddress()
    {
        return $this->hasOne(UserAddress::class, 'user_address_id', 'id')
            ->bind(['id', 'address']);
    }

    public function orderCommodityHidden()
    {
        return $this->hasMany(OrderCommodity::class, 'order_id', 'id')
            ->field('id,order_id,commodity_id,count,money,discount');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function invoice()
    {
        return $this->hasOne(Invoice::class, 'order_id', 'id');
    }

    public function after()
    {
        return $this->hasMany(OrderAfter::class, 'order_id', 'id');
    }
    //订单form
    public function form(){
        return $this->hasOne(OrderFormAnswer::class,'order_id','id')->where(['source'=>0])->hidden(['create_time','delete_time','update_time','order_id','template_id']);
    }

}