<?php
declare (strict_types = 1);

namespace app\index\model;

class Presell extends BaseModel
{
    protected $globalScope = ['mallId'];

    public function commodity()
    {
        return $this->hasMany(PresellCommodity::class, 'presell_id', 'id');
    }


}
