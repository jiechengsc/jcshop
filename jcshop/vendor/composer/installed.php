<?php return array(
    'root' => array(
        'pretty_version' => '1.0.0+no-version-set',
        'version' => '1.0.0.0',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => NULL,
        'name' => 'topthink/think',
        'dev' => true,
    ),
    'versions' => array(
        'adbario/php-dot-notation' => array(
            'pretty_version' => '2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../adbario/php-dot-notation',
            'aliases' => array(),
            'reference' => 'eee4fc81296531e6aafba4c2bbccfc5adab1676e',
            'dev_requirement' => false,
        ),
        'alibabacloud/client' => array(
            'pretty_version' => '1.5.31',
            'version' => '1.5.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../alibabacloud/client',
            'aliases' => array(),
            'reference' => '19224d92fe27ab8ef501d77d4891e7660bc023c1',
            'dev_requirement' => false,
        ),
        'aliyuncs/oss-sdk-php' => array(
            'pretty_version' => 'v2.4.2',
            'version' => '2.4.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../aliyuncs/oss-sdk-php',
            'aliases' => array(),
            'reference' => '0c9d902c33847c07efc66c4cdf823deaea8fc2b6',
            'dev_requirement' => false,
        ),
        'bacon/bacon-qr-code' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../bacon/bacon-qr-code',
            'aliases' => array(),
            'reference' => 'f73543ac4e1def05f1a70bcd1525c8a157a1ad09',
            'dev_requirement' => false,
        ),
        'clagiordano/weblibs-configmanager' => array(
            'pretty_version' => 'v1.5.0',
            'version' => '1.5.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clagiordano/weblibs-configmanager',
            'aliases' => array(),
            'reference' => '8802c7396d61a923c9a73e37ead062b24bb1b273',
            'dev_requirement' => false,
        ),
        'danielstjules/stringy' => array(
            'pretty_version' => '3.1.0',
            'version' => '3.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../danielstjules/stringy',
            'aliases' => array(),
            'reference' => 'df24ab62d2d8213bbbe88cc36fc35a4503b4bd7e',
            'dev_requirement' => false,
        ),
        'dasprid/enum' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../dasprid/enum',
            'aliases' => array(),
            'reference' => '5abf82f213618696dda8e3bf6f64dd042d8542b2',
            'dev_requirement' => false,
        ),
        'endroid/qr-code' => array(
            'pretty_version' => '3.9.7',
            'version' => '3.9.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../endroid/qr-code',
            'aliases' => array(),
            'reference' => '94563d7b3105288e6ac53a67ae720e3669fac1f6',
            'dev_requirement' => false,
        ),
        'ezyang/htmlpurifier' => array(
            'pretty_version' => 'v4.13.0',
            'version' => '4.13.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ezyang/htmlpurifier',
            'aliases' => array(),
            'reference' => '08e27c97e4c6ed02f37c5b2b20488046c8d90d75',
            'dev_requirement' => false,
        ),
        'guzzlehttp/guzzle' => array(
            'pretty_version' => '6.5.5',
            'version' => '6.5.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/guzzle',
            'aliases' => array(),
            'reference' => '9d4290de1cfd701f38099ef7e183b64b4b7b0c5e',
            'dev_requirement' => false,
        ),
        'guzzlehttp/promises' => array(
            'pretty_version' => '1.4.1',
            'version' => '1.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/promises',
            'aliases' => array(),
            'reference' => '8e7d04f1f6450fef59366c399cfad4b9383aa30d',
            'dev_requirement' => false,
        ),
        'guzzlehttp/psr7' => array(
            'pretty_version' => '1.8.2',
            'version' => '1.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../guzzlehttp/psr7',
            'aliases' => array(),
            'reference' => 'dc960a912984efb74d0a90222870c72c87f10c91',
            'dev_requirement' => false,
        ),
        'khanamiryan/qrcode-detector-decoder' => array(
            'pretty_version' => '1.0.5.2',
            'version' => '1.0.5.2',
            'type' => 'library',
            'install_path' => __DIR__ . '/../khanamiryan/qrcode-detector-decoder',
            'aliases' => array(),
            'reference' => '04fdd58d86a387065f707dc6d3cc304c719910c1',
            'dev_requirement' => false,
        ),
        'league/flysystem' => array(
            'pretty_version' => '1.1.5',
            'version' => '1.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem',
            'aliases' => array(),
            'reference' => '18634df356bfd4119fe3d6156bdb990c414c14ea',
            'dev_requirement' => false,
        ),
        'league/flysystem-cached-adapter' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/flysystem-cached-adapter',
            'aliases' => array(),
            'reference' => 'd1925efb2207ac4be3ad0c40b8277175f99ffaff',
            'dev_requirement' => false,
        ),
        'league/mime-type-detection' => array(
            'pretty_version' => '1.7.0',
            'version' => '1.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../league/mime-type-detection',
            'aliases' => array(),
            'reference' => '3b9dff8aaf7323590c1d2e443db701eb1f9aa0d3',
            'dev_requirement' => false,
        ),
        'maennchen/zipstream-php' => array(
            'pretty_version' => '2.1.0',
            'version' => '2.1.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../maennchen/zipstream-php',
            'aliases' => array(),
            'reference' => 'c4c5803cc1f93df3d2448478ef79394a5981cc58',
            'dev_requirement' => false,
        ),
        'markbaker/complex' => array(
            'pretty_version' => '2.0.3',
            'version' => '2.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/complex',
            'aliases' => array(),
            'reference' => '6f724d7e04606fd8adaa4e3bb381c3e9db09c946',
            'dev_requirement' => false,
        ),
        'markbaker/matrix' => array(
            'pretty_version' => '2.1.3',
            'version' => '2.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../markbaker/matrix',
            'aliases' => array(),
            'reference' => '174395a901b5ba0925f1d790fa91bab531074b61',
            'dev_requirement' => false,
        ),
        'mtdowling/jmespath.php' => array(
            'pretty_version' => '2.6.1',
            'version' => '2.6.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mtdowling/jmespath.php',
            'aliases' => array(),
            'reference' => '9b87907a81b87bc76d19a7fb2d61e61486ee9edb',
            'dev_requirement' => false,
        ),
        'myclabs/php-enum' => array(
            'pretty_version' => '1.8.3',
            'version' => '1.8.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../myclabs/php-enum',
            'aliases' => array(),
            'reference' => 'b942d263c641ddb5190929ff840c68f78713e937',
            'dev_requirement' => false,
        ),
        'phpoffice/phpspreadsheet' => array(
            'pretty_version' => '1.18.0',
            'version' => '1.18.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpoffice/phpspreadsheet',
            'aliases' => array(),
            'reference' => '418cd304e8e6b417ea79c3b29126a25dc4b1170c',
            'dev_requirement' => false,
        ),
        'phpseclib/phpseclib' => array(
            'pretty_version' => '2.0.33',
            'version' => '2.0.33.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpseclib/phpseclib',
            'aliases' => array(),
            'reference' => 'fb53b7889497ec7c1362c94e61d8127ac67ea094',
            'dev_requirement' => false,
        ),
        'picqer/php-barcode-generator' => array(
            'pretty_version' => 'v2.2.0',
            'version' => '2.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../picqer/php-barcode-generator',
            'aliases' => array(),
            'reference' => '7df93b40099e5fefad055543320a36b80dccda05',
            'dev_requirement' => false,
        ),
        'psr/cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'd11b50ad223250cf17b86e38383413f5a6764bf8',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '8622567409010282b7aeebe4bb841fe98b58dcaf',
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
            'dev_requirement' => false,
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
            'dev_requirement' => false,
        ),
        'psr/http-message' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'dev_requirement' => false,
        ),
        'ralouphie/getallheaders' => array(
            'pretty_version' => '3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../ralouphie/getallheaders',
            'aliases' => array(),
            'reference' => '120b605dfeb996808c31b6477290a714d356e822',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
            'dev_requirement' => false,
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'reference' => 'a603e5701bd6e305cfc777a8b50bf081ef73105e',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-idn' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-idn',
            'aliases' => array(),
            'reference' => '65bd267525e82759e7d8c4e8ceea44f398838e65',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/property-access' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-access',
            'aliases' => array(),
            'reference' => '098681253076af7070df7d9debe5f75733eea189',
            'dev_requirement' => false,
        ),
        'symfony/property-info' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/property-info',
            'aliases' => array(),
            'reference' => '0f42009150679a7a256eb6ee106401af5d974ed2',
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v5.3.3',
            'version' => '5.3.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'reference' => 'bd53358e3eccec6a670b5f33ab680d8dbe1d4ae1',
            'dev_requirement' => false,
        ),
        'symfony/var-dumper' => array(
            'pretty_version' => 'v4.4.27',
            'version' => '4.4.27.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/var-dumper',
            'aliases' => array(),
            'reference' => '391d6d0e7a06ab54eb7c38fab29b8d174471b3ba',
            'dev_requirement' => true,
        ),
        'symfony/yaml' => array(
            'pretty_version' => 'v2.8.52',
            'version' => '2.8.52.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/yaml',
            'aliases' => array(),
            'reference' => '02c1859112aa779d9ab394ae4f3381911d84052b',
            'dev_requirement' => false,
        ),
        'topthink/framework' => array(
            'pretty_version' => 'v6.0.9',
            'version' => '6.0.9.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/framework',
            'aliases' => array(),
            'reference' => '0b5fb453f0e533de3af3a1ab6a202510b61be617',
            'dev_requirement' => false,
        ),
        'topthink/think' => array(
            'pretty_version' => '1.0.0+no-version-set',
            'version' => '1.0.0.0',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => NULL,
            'dev_requirement' => false,
        ),
        'topthink/think-captcha' => array(
            'pretty_version' => 'v3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-captcha',
            'aliases' => array(),
            'reference' => '1eef3717c1bcf4f5bbe2d1a1c704011d330a8b55',
            'dev_requirement' => false,
        ),
        'topthink/think-helper' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-helper',
            'aliases' => array(),
            'reference' => 'f98e3ad44acd27ae85a4d923b1bdfd16c6d8d905',
            'dev_requirement' => false,
        ),
        'topthink/think-migration' => array(
            'pretty_version' => 'v3.0.3',
            'version' => '3.0.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-migration',
            'aliases' => array(),
            'reference' => '5717d9e5f3ea745f6dbfd1e30b4402aaadff9a79',
            'dev_requirement' => false,
        ),
        'topthink/think-multi-app' => array(
            'pretty_version' => 'v1.0.14',
            'version' => '1.0.14.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-multi-app',
            'aliases' => array(),
            'reference' => 'ccaad7c2d33f42cb1cc2a78d6610aaec02cea4c3',
            'dev_requirement' => false,
        ),
        'topthink/think-orm' => array(
            'pretty_version' => 'v2.0.44',
            'version' => '2.0.44.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-orm',
            'aliases' => array(),
            'reference' => '5d3d5c1ebf8bfccf34bacd90edb42989b16ea409',
            'dev_requirement' => false,
        ),
        'topthink/think-throttle' => array(
            'pretty_version' => 'v1.3.0',
            'version' => '1.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-throttle',
            'aliases' => array(),
            'reference' => 'c63afa6a66ffdac03e37b1124b5120040ef7cea8',
            'dev_requirement' => false,
        ),
        'topthink/think-trace' => array(
            'pretty_version' => 'v1.4',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../topthink/think-trace',
            'aliases' => array(),
            'reference' => '9a9fa8f767b6c66c5a133ad21ca1bc96ad329444',
            'dev_requirement' => true,
        ),
        'zoujingli/wechat-developer' => array(
            'pretty_version' => 'v1.2.31',
            'version' => '1.2.31.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../zoujingli/wechat-developer',
            'aliases' => array(),
            'reference' => '0cf699c725f69d66657a50e60d22f71c9e5a5e16',
            'dev_requirement' => false,
        ),
    ),
);
